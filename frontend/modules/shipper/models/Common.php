<?php

namespace frontend\modules\shipper\models;

use Yii;
use common\models\Common as Comm;
class Common{

// pagination controls...
const PURCHASE_HISTORY= 15;// pagination limit...
const COMMENT_LIST= 15;// pagination limit...
const COIN_HISTORY=30;
const VOLUME_LIMIT=9;
const VOLUME_SEARCH_LIMIT=15;
const GENRE_LIMIT=15;
const PUBLISHER_VOLUME_LIMIT=15;
const PUBLISHERS_LIMIT=15;
const FREE_COMICS_LIMIT=15;
const COMIC_SEARCH_LIMIT=15;
const MYCOLLECTION_COMICS_LIMIT=15;     



 const HTTP_SUCCESS= 200;
 const HTTP_NON_AUTHORATIVE =  203;
 const HTTP_NO_CONTENT =  204;
 const HTTP_UNAUTHORIZED =  401;
 const HTTP_BAD_REQUEST =  400;
 const HTTP_PAYMENT_REQUIRED =  402;
 const HTTP_CONFLICT =  409;
 const HTTP_UNPROCESSABLE =  422;
 const HTTP_TOO_MANY_REQUEST =  429;
 const CUSTOM_MAILNOTVERIFIED =  909; // custom error for inactive,blocked user or record...
 const CUSTOM_INACTIVE =  910; // custom error for inactive,blocked user or record...
 const CUSTOM_SERVER_ERROR =  901; // custom error MAIL FAILED OR ANY OTHER SERVER ERROR...


private static $pt_stoken,$cache;



static public function My(){
    
    return new self();
    
}

/** Encode array to json object
 *  remove element key no and return Mobile
 *  Relavant Dictionary
 *  @params $array
 *  @return JSON Object Mobile Dictionary
 **/  
public function JSONEncode($array,$array_format=true){
    if($array_format) $array = self::My()->JSONArrayFormat($array);
      return json_encode($array,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
}
/** Convert array to relavant JSON
 * format.remove index from array
 * @params $array array
 * @return $array formated array
 **/  
public function JSONArrayFormat($array){
    $array = array_values($array);
    return $array;
}
/** media path images in which directory
 * @return media path
 **/ 
public function mediapath(){
   return Comm::getUrlMediaPath();  
}
/* get thumbnail image by given size..
 *?
 */
public function Imagepath($model,$image,$context=false){
   $image = is_object($model)?$model->$image:$model[$image]; 
   return Comm::getMediaPath($model,$image,$context); 
}
public function pathWithoutImage($model,$context){
   return Comm::onlyMediaPath($model,$context); 
}
private function httpStatusCode(){
    $rs ='REQUEST_SUCCESS_';
    return $codes =[
        'HTTP_SUCCESS'=>200,
        $rs.'NO_CONTENT'=>204,
        'REQUEST_ACCEPTED'=>202,
        'NOT_FOUND'=>404,
    ];
}

/* send response data to api response
 *@params (int) status_code,(string|array) $message,(array) $response 
 *@returns json | throws Exception
 */
public function send($status_code,$response=[],$message=null){ 
   $ack                = 'error';
   $success            = ['200','204'];
   if(in_array($status_code,$success)) 
   $ack                = 'success';// && $message = ['success'=>$message];
   else $message       = ['errors'=>$message];    
   $json['status_code']= $status_code;
   $json['ack']        = $ack;
   //$json['message']    = $message;
   $json['response']   = $response; die(Common::JSONEncode($json,0)); 
}

public function createIndex($collection,$fileds,$options=null){
     $vol = Yii::$app->mongodb->getCollection($collection);
     $vol->createIndex($fileds,['background'=>true]);
 }
/* store cache data */    
 public function cacheIt($name,$data){
    $cache_dir = realpath('../frontend/runtime')."/servicescache";    
    is_dir($cache_dir) or mkdir($cache_dir,755);      
    if($data && is_array($data)){
       $data ='<?php return \''. self::JSONEncode($data,0) .'\';';        
       $name = md5($name); 
       file_put_contents($cache_dir."/".$name.".pct",$data);
    }
    self::$cache[$name] =$data; 
    if(count(self::$cache)>5)
       array_shift(self::$cache);    
          
 }
/* get cache data */    
 public function cacheResult($name){
     $cache_dir = realpath('../frontend/runtime')."/servicescache"; 
     $name = md5($name).".pct";
   try{
      if(file_exists($cache_dir."/".$name) && filemtime($cache_dir."/".$name)>time()-60*30){    
         if(!$data  =@self::$cache[$name]){
             $data = include_once($cache_dir."/".$name); 
         }          
        if($data){              
        die($data);
       }  
      }
   }catch(yii\base\ErrorException $e){}
    catch(Exception $e){ }
  
 }
    
 public function deleteCache(){
    $cache_dir = realpath('../frontend/runtime')."/servicescache";
    chmod($cache_dir,0777); 
    Comm::DelAllMedia($cache_dir); 
 }

    
    
}

