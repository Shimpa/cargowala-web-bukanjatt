<?php
namespace frontend\modules\shipper\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\AdminSettings;
use common\models\Bids;
use common\models\Bookings;
use common\models\Categories;
use common\models\Drivers;
use common\models\Faq;
use common\models\Membership;
use common\models\Payments;
use common\models\TaxTerms;
use common\models\Topics;
use common\models\Transactions;
use common\models\TruckLanes;
use common\models\Trucks;
use common\models\User;
use common\models\Users;
use common\models\Vehicles;
use common\models\Common as CommonModel;
use frontend\modules\common\models\Common;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

class UserController extends Controller{

    /**
    * @inheritdoc
    */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['register', 'login', 'create-profile', 'get-user-profile', 'update-profile', 'forgot-password', 'change-number', 'update-password',  'resend-email-token', 'get-otp', 'verify-otp', 'as-company', 'as-agent', 'get-categories', 'get-sub-categories', 'get-trucks', 'get-data', 'book-truck', 'repeat-order', 'reschedule-order', 'track-shipment', 'get-bids', 'view-bids', 'reject-bid', 'accept-bid', 'remove-shipment', 'cancel-shipment', 'view-shipments', 'view-assigned-list', 'get-bid-detail', 'save-payment', 'delete-media', 'view-assigned-drivers', 'get-faq-topics', 'get-faq-list', 'process-payment', 'citrus-response'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'register' => ['post'],
                    'login' => ['post'],
                    'create-profile' => ['post'],
                    'get-user-profile' => ['post'],
                    'update-profile' => ['post'],
                    'forgot-password' => ['post'],
                    'change-number' => ['post'],
                    'update-password' => ['post'],
                    'resend-email-token' => ['post'],
                    'get-otp' => ['post'],
                    'verify-otp' => ['post'],
                    'as-company' => ['post'],
                    'as-agent' => ['post'],
                    'get-categories' => ['post'],
                    'get-sub-categories' => ['post'],
                    'get-trucks' => ['post'],
                    'get-data' => ['post'],
                    'book-truck' => ['post'],
                    'repeat-order' => ['post'],
                    'reschedule-order' => ['post'],
                    'track-shipment' => ['post'],
                    'get-bids' => ['post'],
                    'view-bids' => ['post'],
                    'reject-bid' => ['post'],
                    'accept-bid' => ['post'],
                    'remove-shipment' => ['post'],
                    'cancel-shipment' => ['post'],
                    'view-shipments' => ['post'],
                    'view-assigned-list' => ['post'],
                    'get-bid-detail' => ['post'],
                    'add-payments' => ['post'],
                    'delete-media' => ['post'],
                    'view-assigned-drivers' => ['post'],
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
	public function beforeAction($action) {
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
    }
    
    public function actionIndex(){
        die("@Access Denied");
    }
    
    public function actions() {
      return [
        'auth' => [
          'class' => 'yii\authclient\AuthAction',
          'successCallback' => [$this, 'oAuthSuccess'],
        ],
      ];
    }

    /**
    * Function to Update/Change User Profile Data
    *
    * @Required params username, password
    * @return boolean|yii\web\Response
    * Url : http://cargowala.com/dev/frontend/web/index.php?r=trucker%2Fuser%2Flogin
    *
    */  	
    public function actionLogin(){
        (empty(Yii::$app->request->post('username')) && empty(Yii::$app->request->post('password'))) and
		Common::send(Common::HTTP_BAD_REQUEST);	
		$model = Users::findOne(['contact.email'=>Yii::$app->request->post('username')]);
		(empty($model) && empty(Yii::$app->request->post('social'))) and Common::send(Common::HTTP_NO_CONTENT);
        $isValidPassword  =  $isActive = false;
		$isSocial = !empty(Yii::$app->request->post('social'));
		$isTrucker = true;
		if(!empty($model)){
			if(($model->status == 'Inactive') && !empty($model->hash_token)) Common::send(Common::CUSTOM_MAILNOTVERIFIED);
			if($model->status == 'Inactive') Common::send(Common::CUSTOM_INACTIVE);
			$isValidPassword  = $model->password ===md5(Yii::$app->request->post('password'));
			$isActive = !$model->status == 0;
			$isTrucker = ($model->role == 21 || $model->role == 22 || $model->role == 3);  // if Shipper user try to login
		}
		if($isSocial) $isActive = true;
		if((!empty($model) && $isValidPassword && $isActive) || (!empty($model) && $isSocial )){
			$model->load(['Users' => $model->oldAttributes]);
			$model->device = [
				'token' => Yii::$app->request->post('mobile_tokens'),
				'status' => [
					'code' => (int)1,
					'text' => 'true',
				],
			];
			$model->save(false);
		}
		switch(true){
			case (!$isTrucker): // if Trucker user try to login
				Common::send(Common::HTTP_UNAUTHORIZED); 
			break;
			case (!$isSocial && !$isValidPassword && !$isTrucker):
				Common::send(Common::HTTP_BAD_REQUEST); 
			break;
			case (!$isSocial  && !$isActive && (!empty($model)  && !empty($model->hash_token))):
				Common::send(Common::CUSTOM_MAILNOTVERIFIED);
			break;
			case ((!$isSocial && !$isActive) || ($isSocial && !empty($model) &&  !$isActive)):
				Common::send(Common::CUSTOM_INACTIVE);
			break;
			case ((!empty($model) && $isValidPassword && $isActive) || (!empty($model) && $isSocial )):
				Common::send(Common::HTTP_SUCCESS,$this->getProfile($model));
			break;	   
			default:
			 $this->socialRegister();	
			break;	
		}
    }
	
	private function socialRegister(){
		in_array(Yii::$app->request->post('social'),['facebook','google']) or Common::send(Common::HTTP_BAD_REQUEST);
		$model =  new Users();
		//$model->scenario = Users::SCENARIO_SHIPPER_APISOCIALREG;
		$model->load(['Users' => [
			'email' =>Yii::$app->request->post('username'),
			'status' =>1,
			'role' =>21,
			'password' =>md5(time()),
			// 'hash_token' =>md5(mt_rand(time(),time())+Yii::$app->request->post('username')).":".time(),
			'hash_token' => '',
			'social' => [
				'provider' => Yii::$app->request->post('social'),
				'uid' => Yii::$app->request->post('uid'),
			],
			'device' => [
				'token' => Yii::$app->request->post('mobile_tokens'),
				'status' => [
					'code' => (int)1,
					'text' => 'true',
				],
			],
		]]);
		//$model->validate() or  Common::send(Common::HTTP_BAD_REQUEST);
		$this->uploadImage($model);
		$model->save(false) and Common::send(Common::HTTP_SUCCESS,$this->getProfile($model)); 
	}

    /**
    * Function to Update/Change User Profile Data
    *
    * @Required params username, password
    * @return boolean|yii\web\Response
    * Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fregister
    *
    */  
	public function actionRegister(){
		(empty(Yii::$app->request->post('username')) || empty(Yii::$app->request->post('password'))) and 
		Common::send(Common::HTTP_BAD_REQUEST);
		empty(Yii::$app->request->post('mobile_tokens')) and Common::send(Common::HTTP_UNPROCESSABLE);
		$model = Users::findOne(['contact.email'=>Yii::$app->request->post('username')]);
		if(!empty($model)) Common::send(Common::HTTP_CONFLICT);
		else $model =  new Users();
		$model->scenario = Users::SCENARIO_SHIPPER_APICREATE;
		$model->load(['Users'=>[
			'email' =>Yii::$app->request->post('username'),
			'status' =>0,
			'role' =>21,
			'password' =>md5(Yii::$app->request->post('password')),
			'hash_token' =>md5(mt_rand(time(),time())+Yii::$app->request->post('username')).":".time(),
		]]); 
		$model->social = [
			'provider' => 'mobile',
			'uid' => '',
		];
		$model->device = [
			'token' => Yii::$app->request->post('mobile_tokens'),
			'status' => [
				'code' => (int)1,
				'text' => 'true',
			]
		];
		//$model->validate() or Common::send(Common::HTTP_CONFLICT);
		$model->save(false) && $model->sendVerificationMail();
		Common::send(Common::HTTP_SUCCESS,$this->getProfile($model));		
	}
	
	public function actionCreateProfile(){
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		//$model->scenario = Users::SCENARIO_SHIPPER_APICREATEPROFILE;
		$posted = Yii::$app->request->post();
		unset($posted['password']);
		unset($posted['status']);
		unset($posted['hash_token']);
		unset($posted['mobile_tokens']);
		unset($posted['creadted_on']);
		unset($posted['modified_on']);
		unset($posted['email']);
		$posted['mobile_number'] = !empty(Yii::$app->request->post('mobile_no')) ? Yii::$app->request->post('mobile_no') : '';
		$posted['alternate_number'] = !empty(Yii::$app->request->post('alternate_number')) ? Yii::$app->request->post('alternate_number') : '';
		$posted['registered_city'] = !empty(Yii::$app->request->post('business')['register_city']) ? Yii::$app->request->post('business')['register_city'] : '';
		$posted['registered_state'] = !empty(Yii::$app->request->post('business')['register_state']) ? Yii::$app->request->post('business')['register_state'] : '';
		$posted['registered_pincode'] = !empty(Yii::$app->request->post('business')['register_pin']) ? Yii::$app->request->post('business')['register_pin'] : '';
		// $posted['service_taxno'] = !empty(Yii::$app->request->post('business')['business_service_taxno']) ? Yii::$app->request->post('business')['business_service_taxno'] : '';
		$posted['office_pincode'] = !empty(Yii::$app->request->post('business')['office_pin']) ? Yii::$app->request->post('business')['office_pin'] : '';
		$posted['landline'] = !empty(Yii::$app->request->post('business')['landline_number']) ? Yii::$app->request->post('business')['landline_number'] : '';
		unset($posted['business']['register_city']);
		unset($posted['business']['register_state']);
		unset($posted['business']['register_pin']);
		unset($posted['business']['office_pin']);
		unset($posted['business']['landline_number']);
		// unset($posted['business']['business_service_taxno']);
        $posted+= $posted['business'];
        $posted+= $posted['company_type'];
        $posted+= $posted['business_type'];		
		$model->load(['Users'=>$posted]);
		//$model->validate() or  Common::send(Common::HTTP_BAD_REQUEST);
		$model->status = $model->oldAttributes['status'];
		$model->plocation = 'dashboard';
		$model->role = Yii::$app->request->post('account_type');		
		if(!empty(Yii::$app->request->post('image_base64'))) $this->uploadImage($model);
		$model->save(false) and Common::send(Common::HTTP_SUCCESS,$this->getProfile($model)); 
	}
    
    /**
    * Function to Forgot Password
    *
    * @Required params email
    * @return boolean|yii\web\Response
    *
    */    
	public function actionForgotPassword(){
		$model = Users::findOne(['contact.email'=>Yii::$app->request->post('email'), 'status' => 1]);
        $has_token = md5(mt_rand(time(),time())+Yii::$app->request->post('email')).":".time();
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        $model->password = '';
        $model->hash_token = $has_token;
		$model->status = $model->oldAttributes['status'];
        $model->save(false) and $model->resetPasswordEmail($model);        
        Common::send(Common::HTTP_SUCCESS);
	}
    
    /**
    * Function to Update/Change Password
    *
    * @Required params _id, oldpassword, newpassword
    * @return boolean|yii\web\Response
    *
    */     
	public function actionUpdatePassword(){
        empty(Yii::$app->request->post('newpassword')) and Common::send(Common::HTTP_BAD_REQUEST);
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model  = Users::findOne(['_id' => Yii::$app->request->post('_id'), 'password' => md5(Yii::$app->request->post('oldpassword'))]);
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        $model->password = md5(Yii::$app->request->post('newpassword'));
		$model->status = $model->oldAttributes['status'];
        $model->save(false) and Common::send(Common::HTTP_SUCCESS);
	}
    
    /**
    * Function to Update/Change User Profile Data
    *
    * @Required params _id, firstname, lastname, pancard, business[business_service_taxno]
    * @return boolean|yii\web\Response
    *
    */     
	public function actionUpdateProfile(){
        ( empty(Yii::$app->request->post('firstname')) || empty(Yii::$app->request->post('lastname')) ) and Common::send(Common::HTTP_BAD_REQUEST);
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
        $posted = Yii::$app->request->post();
		unset($posted['password']);
		unset($posted['status']);
		unset($posted['hash_token']);
		unset($posted['mobile_tokens']);
		unset($posted['creadted_on']);
		unset($posted['modified_on']);
		unset($posted['email']);
		$posted['registered_city'] = !empty(Yii::$app->request->post('business')['register_city']) ? Yii::$app->request->post('business')['register_city'] : '';
		$posted['registered_state'] = !empty(Yii::$app->request->post('business')['register_state']) ? Yii::$app->request->post('business')['register_state'] : '';
		$posted['registered_pincode'] = !empty(Yii::$app->request->post('business')['register_pin']) ? Yii::$app->request->post('business')['register_pin'] : '';
		// $posted['service_taxno'] = !empty(Yii::$app->request->post('business')['business_service_taxno']) ? Yii::$app->request->post('business')['business_service_taxno'] : '';
		$posted['office_pincode'] = !empty(Yii::$app->request->post('business')['office_pin']) ? Yii::$app->request->post('business')['office_pin'] : '';
		$posted['landline'] = !empty(Yii::$app->request->post('business')['landline_number']) ? Yii::$app->request->post('business')['landline_number'] : '';
		unset($posted['business']['register_city']);
		unset($posted['business']['register_state']);
		unset($posted['business']['register_pin']);
		unset($posted['business']['office_pin']);
		unset($posted['business']['landline_number']);
		// unset($posted['business']['business_service_taxno']);
		// echo '<pre>'; print_r($posted['business']); die;
        $posted+= $posted['business'];
		$model->load(['Users'=>$posted]);
        $model->status = $model->oldAttributes['status'];
		$model->save(false);
        if(!empty(Yii::$app->request->post('image_base64'))) $this->uploadImage($model);
        Common::send(Common::HTTP_SUCCESS, $this->actionGetUserProfile($model->_id->__toString())); 
	}
	
    /**
    * Function to Register an individual user as a Company and updates company details as well on later stages
    *
    * @Required params _id
    * @return boolean|yii\web\Response 
    * Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fas-company
    */     
	public function actionAsCompany(){
        ( empty(Yii::$app->request->post('_id')) ) and Common::send(Common::HTTP_BAD_REQUEST);
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
        $posted = Yii::$app->request->post();
		unset($posted['password']);
		unset($posted['status']);
		unset($posted['hash_token']);
		unset($posted['mobile_tokens']);
		unset($posted['creadted_on']);
		unset($posted['modified_on']);
		unset($posted['email']);
		$posted['registered_city'] = !empty(Yii::$app->request->post('business')['register_city']) ? Yii::$app->request->post('business')['register_city'] : '';
		$posted['registered_state'] = !empty(Yii::$app->request->post('business')['register_state']) ? Yii::$app->request->post('business')['register_state'] : '';
		$posted['registered_pincode'] = !empty(Yii::$app->request->post('business')['register_pin']) ? Yii::$app->request->post('business')['register_pin'] : '';
		// $posted['service_taxno'] = !empty(Yii::$app->request->post('business')['business_service_taxno']) ? Yii::$app->request->post('business')['business_service_taxno'] : '';
		$posted['office_pincode'] = !empty(Yii::$app->request->post('business')['office_pin']) ? Yii::$app->request->post('business')['office_pin'] : '';
		$posted['landline'] = !empty(Yii::$app->request->post('business')['landline_number']) ? Yii::$app->request->post('business')['landline_number'] : '';
		unset($posted['business']['register_city']);
		unset($posted['business']['register_state']);
		unset($posted['business']['register_pin']);
		unset($posted['business']['office_pin']);
		unset($posted['business']['landline_number']);
		// unset($posted['business']['business_service_taxno']);
        $posted+= $posted['business'];
        $posted+= $posted['company_type'];
        $posted+= $posted['business_type'];
		$model->load(['Users'=>$posted]);
        $model->status = $model->oldAttributes['status'];
        $model->account_type = 22; // for Company Type
		$model->role = 22; // for Company Type
		$model->save(false);
        Common::send(Common::HTTP_SUCCESS, $this->actionGetUserProfile($model->_id->__toString())); 
	}
    
    /**
    * Function to Register an individual user as an Agent and updates his/her details as well on later stages
    *
    * @Required params _id
    * @return boolean|yii\web\Response 
    * Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fas-agent
    */     
	public function actionAsAgent(){
        ( empty(Yii::$app->request->post('_id')) ) and Common::send(Common::HTTP_BAD_REQUEST);
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
        $posted = Yii::$app->request->post();
		unset($posted['password']);
		unset($posted['status']);
		unset($posted['hash_token']);
		unset($posted['mobile_tokens']);
		unset($posted['creadted_on']);
		unset($posted['modified_on']);
		unset($posted['email']);
		$posted['registered_city'] = !empty(Yii::$app->request->post('business')['register_city']) ? Yii::$app->request->post('business')['register_city'] : '';
		$posted['registered_state'] = !empty(Yii::$app->request->post('business')['register_state']) ? Yii::$app->request->post('business')['register_state'] : '';
		$posted['registered_pincode'] = !empty(Yii::$app->request->post('business')['register_pin']) ? Yii::$app->request->post('business')['register_pin'] : '';
		// $posted['service_taxno'] = !empty(Yii::$app->request->post('business')['business_service_taxno']) ? Yii::$app->request->post('business')['business_service_taxno'] : '';
		$posted['office_pincode'] = !empty(Yii::$app->request->post('business')['office_pin']) ? Yii::$app->request->post('business')['office_pin'] : '';
		$posted['landline'] = !empty(Yii::$app->request->post('business')['landline_number']) ? Yii::$app->request->post('business')['landline_number'] : '';
		unset($posted['business']['register_city']);
		unset($posted['business']['register_state']);
		unset($posted['business']['register_pin']);
		unset($posted['business']['office_pin']);
		unset($posted['business']['landline_number']);
		// unset($posted['business']['business_service_taxno']);
        $posted+= $posted['business'];
		$model->load(['Users'=>$posted]);
        $model->status = $model->oldAttributes['status'];
        $model->account_type = 3; // For Agent Type
		$model->role = 3; // for Company Type
        $model->business_type = '';
        $model->company_type = '';
		$model->save(false);
        Common::send(Common::HTTP_SUCCESS, $this->actionGetUserProfile($model->_id->__toString())); 
	}
	
    /**
    * Function to Update/Change Mobile Number
    *
    * @Required params:  _id, mobile_number
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fchange-number
    * @return boolean|yii\web\Response
    *
    */
	public function actionChangeNumber(){
        (empty(Yii::$app->request->post('mobile_number')) && empty(Yii::$app->request->post('_id')) )and Common::send(Common::HTTP_BAD_REQUEST);
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$otp   = abs(ceil(mt_rand((time()*2),time()*3)));
		$otp   = substr($otp,0,6);
		$mno   = substr(Yii::$app->request->post('mobile_number'), -4,10);
		$model->otp = [
			'otp_type' => 'Change Mobile Number',
			'otp_no'   => $otp,
			'otp_for'   => Yii::$app->request->post('mobile_number'),
			'validity' => time()
		];
		$model->alternate_number = Yii::$app->request->post('alternate_number');
        $model->status = $model->oldAttributes['status'];
		$model->save(false);
		date_default_timezone_set('Asia/Kolkata');
		$name = empty($model->name['firstname']) ? '' : $model->name['firstname'].' '.empty($model->name['lastname']) ? '' : $model->name['lastname'];
		$msg  = "Hello ".$name. ", Your one time password for change mobile number on cargowala";
		$msg .= " is " . $otp . ". This can be used only once and is valid for next 4 hours from " . date('Y-m-d h:i:s');
		$pth  = "http://121.242.224.32:8080/smsapi/httpapi.jsp?username=" . urlencode(Yii::$app->params['smsusername']);		
	    $pth .= "&password=" . urlencode(Yii::$app->params['smspassword']);
	    $pth .= "&from=" . urlencode(Yii::$app->params['smssenderid']);
	    $pth .= "&to=" . urlencode(Yii::$app->request->post('mobile_number'));
	    $pth .= "&text=" . urlencode($msg);
	    // $pth .= "&coding=0";
		$res = file_get_contents($pth);
		Common::send(Common::HTTP_SUCCESS);        
    }
	
    /**
    * Function to get Main Category list
    *
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-categories
    * @return boolean|yii\web\Response
    *
    */
	public function actionGetCategories(){
		$model  = Categories::findAll( ['parent_id' => '', 'status' => 1 ] );
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        foreach($model as $key => $val){
            $details[] = [
                'id' => $id = $val->_id->__toString(),
                'name' => $id = $val->name,
            ];
        }
        Common::send(Common::HTTP_SUCCESS, $details ); 
    }
    
    /**
    * Function to get sub categories list on the bases of parent category 
    *
    * @Required params cat_id
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-sub-categories
    * @return boolean|yii\web\Response
    *
    */
	public function actionGetSubCategories(){
        ( empty(Yii::$app->request->post('cat_id')) ) and Common::send(Common::HTTP_BAD_REQUEST);        
		$model  = Categories::findAll( ['parent_id' => Yii::$app->request->post('cat_id'), 'status' => 1 ] );
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        foreach($model as $key => $val){
            $details[] = [
                'id' => $id = $val->_id->__toString(),
                'name' => $id = $val->name,
            ];
        }
        Common::send(Common::HTTP_SUCCESS, $details ); 
    }
    
    /**
    * Function to get Trucks list on the base of truck category
    *
    * @Required params truck_type
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-trucks
    * @return boolean|yii\web\Response
    *
    */
	public function actionGetTrucks(){
        ( empty(Yii::$app->request->post('truck_type')) ) and Common::send(Common::HTTP_BAD_REQUEST);        
		$model = Trucks::find()->where(['truck_type' => Yii::$app->request->post('truck_type') , 'status' => 1])->orderBy('loading_capacity ASC')->all();
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        foreach($model as $key => $val){
            $details[] = [
                'id' => $id = $val->_id->__toString(),
                'name' => $id = $val->name. '( '.$val->loading_capacity.' Ton )',
            ];
        }
        Common::send(Common::HTTP_SUCCESS, $details ); 
    }	
	
   /**
    * Function to Resend Email Token
    *
    * @Required params _id, oldpassword, newpassword
    * @return boolean|yii\web\Response
    *
    */     
	public function actionResendEmailToken(){
        empty(Yii::$app->request->post('email')) and Common::send(Common::HTTP_BAD_REQUEST);
        $has_token = md5(mt_rand(time(),time())+Yii::$app->request->post('email')).":".time();
		$model  = Users::findOne(['contact.email' => Yii::$app->request->post('email')]);
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        if($model->status == 'Active') Common::send(Common::CUSTOM_INACTIVE);
        $model->hash_token = $has_token;
		$model->status = $model->oldAttributes['status'];
        $model->save(false) and $model->resetPasswordTokenEmail($model);        
        Common::send(Common::HTTP_SUCCESS);        
	}
    
	public function actionGetOtp(){
		// $model  = Users::findOne(['contact.mobile_number'=>Yii::$app->request->post('mobile_no')]);
		// empty($model) or Common::send(Common::HTTP_CONFLICT);
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model->scenario = Users::SCENARIO_SHIPPER_APIMOBILEOTP;
		$model->mobile_number = Yii::$app->request->post('mobile_no');
		$model->validate() or Common::send(Common::HTTP_BAD_REQUEST);
		$model->mobile_number  = null;
		$otp   = abs(ceil(mt_rand((time()*2),time()*3)));
		$otp   = substr($otp,0,6);
		$mno   = substr(Yii::$app->request->post('mobile_no'),-4,10);
		$model->otp = [
			'otp_type' => 'create_profile',
			'otp_no'   => $otp,
			'otp_for'   => Yii::$app->request->post('mobile_no'),
			'validity' => time()
		];
		$model->status = $model->oldAttributes['status'];
		$model->save(false);
		date_default_timezone_set('Asia/Kolkata');
		$name = empty($model->name['firstname']) ? '' : $model->name['firstname'].' '.empty($model->name['lastname']) ? '' : $model->name['lastname'];
		$msg  = "Hello ".$name. ", Your One time password for cargowala shipper account";		
		$msg .= " on XXX XXX " . $mno;
		$msg .= " is " . $otp . ". This can be used only once and is valid for next 4 hours from " . date('Y-m-d h:i:s');
		$pth  = "http://121.242.224.32:8080/smsapi/httpapi.jsp?username=" . urlencode(Yii::$app->params['smsusername']);		
	    $pth .= "&password=" . urlencode(Yii::$app->params['smspassword']);
	    $pth .= "&from=" . urlencode(Yii::$app->params['smssenderid']);
	    $pth .= "&to=" . urlencode(Yii::$app->request->post('mobile_no'));
	    $pth .= "&text=" . urlencode($msg);
	    // $pth .= "&coding=0";
		$res = file_get_contents($pth);
		Common::send(Common::HTTP_SUCCESS);
	}
	
	public function actionVerifyOtp(){
		preg_match('/^[0-9]{6,6}$/',Yii::$app->request->post('otp')) or Common::send(Common::HTTP_BAD_REQUEST);
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model  = Users::findOne(['_id'=>Yii::$app->request->post('_id'),'otp.otp_no'=>Yii::$app->request->post('otp')]);
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$validTill = time()*60*60*4;
		if($model->otp['validity']<=$validTill){
			$model->mobile_number = $model->otp['otp_for'];
			$model->otp = [
				// 'otp_type' =>'' ,
				// 'otp_no' => '',
				// 'otp_for' => '',
				// 'validity' => ''
				'verified' =>(int)1,
			];
			$model->status = $model->oldAttributes['status'];
			$model->save(false) and Common::send(Common::HTTP_SUCCESS);	
		}else	Common::send(Common::HTTP_UNPROCESSABLE);	
	}

	private function getProfile($user){ 
		return [
			'_id' => $user->_id->__toString(),
			'firstname' => empty($user->name['firstname'])?'':$user->name['firstname'],
			'lastname'  => empty($user->name['lastname'])?'':$user->name['lastname'],
			'email' => empty($user->contact['email'])?'':$user->contact['email'],
			'mobile_no' => empty($user->contact['mobile_number'])?'':$user->contact['mobile_number'],
			'alternate_number' => empty($user->contact['alternate_number'])?'':$user->contact['alternate_number'],
			'user_type' => ($user->role == '3') ? 'Agent' : 'Shipper',
			// 'image' => empty($user->image)?'':CommonModel::getMediaPath($user,$user->image),
			'image' => empty($user->image)?'':$user->image,
			'has_cp' => empty($user->has_cp) ? '' : $user->has_cp,
			'security_token' => md5($user->contact['email'] . $user->password . 'cargowala-security-token'),			  
		];
	}
             
    /**
    * Function to Get User Profile Detail
    *
    * @Required params : _id
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-user-profile
    * @return boolean|yii\web\Response
    *
    */    
	public function actionGetUserProfile($user_id = NULL){
		$model = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
        $id = empty(Yii::$app->request->post('_id')) ? $user_id : Yii::$app->request->post('_id');
        // $model  = Users::findOne(['_id' => Yii::$app->request->post('_id')]);
        // empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$userData = [
			'_id'       => $model->_id->__toString(),
			'firstname' => empty($model->name['firstname']) ? '' : $model->name['firstname'],
			'lastname'  => empty($model->name['lastname']) ? '' : $model->name['lastname'],
			'email'     => empty($model->contact['email']) ? '' : $model->contact['email'],
			'mobile_no' => empty($model->contact['mobile_number']) ? '' : $model->contact['mobile_number'],
			'alternate_number' => empty($model->contact['alternate_number']) ? '' : $model->contact['alternate_number'],
			'user_type' => ($model->role == '3') ? 'Agent' : 'Shipper',
			'image'     => empty($model->image) ? '' : $model->image,
			'has_cp'    => empty($model->has_cp) ? '' : $model->has_cp,
			'business_name' => empty($model->business['name']) ? '' : $model->business['name'],
			'pancard' => empty($model->pancard) ? '' : $model->pancard,
			'account_type' => empty($model->account_type) ? '' : $model->account_type,
			'company_type' => empty($model->company_type) ? '' : $model->company_type,
			'business_type' => empty($model->business_type) ? '' : $model->business_type,
			'registered_address' => empty($model->business['registered_address']) ? '' : $model->business['registered_address'],
			'office_address' => empty($model->business['office_address']) ? '' : $model->business['office_address'],
			'landline_number' => empty($model->business['landline']) ? '' : $model->business['landline'],
			'register_city' => empty($model->business['registered_city']) ? '' : $model->business['registered_city'],
			'register_state' => empty($model->business['registered_state']) ? '' : $model->business['registered_state'],
			'register_pin' => empty($model->business['registered_pincode']) ? '' : $model->business['registered_pincode'],
			'office_city' => empty($model->business['office_city']) ? '' : $model->business['office_city'],
			'office_state' => empty($model->business['office_state']) ? '' : $model->business['office_state'],
			'office_pin' => empty($model->business['office_pincode']) ? '' : $model->business['office_pincode'],
			'business_service_taxno' => empty($model->business['service_taxno']) ? '' : $model->business['service_taxno'],
			'security_token' => md5($model->contact['email'].$model->password.'cargowala-security-token'),
		];
		Common::send(Common::HTTP_SUCCESS, $userData);
	}
	
	public function actionGetData(){
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$categories  = Categories::find( ['status' => 1] )->orderBy('parent_id')->all();
		$trucks  = Trucks::find( ['status' => 1] )->orderBy('loading_capacity ASC')->all();
        (empty($categories) &&  empty($trucks)) and Common::send(Common::HTTP_NO_CONTENT);
		if(!empty($categories)){
			foreach($categories as $key => $val){
				$key = 'sub-categories';
                $skey = $val->parent_id;
				if($val->parent_id == ''){
                    $key = 'parent';
                    $skey = 'self';
                }
				$id = $val->_id->__toString();
				$data['Categories'][$key][$skey][] = [
					'id' => $id,
					'name' => $val->name,
					//'parent' => $val->parent_id,
				];
			}
		}
		if(!empty($trucks)){
			foreach($trucks as $key => $val){
				$key = 'Above 15 ton';
				if($val->truck_type == 'Small Trucks') $key = 'Below 5 ton';
				if($val->truck_type == 'Medium Trucks') $key = '5 ton to 15 ton';
				$data['Trucks'][$key][] = [
					'id' => $val->_id->__toString(),
					'name' => $val->name.' ( '.$val->loading_capacity.' ton ) ',
				];
			}
		}
        Common::send(Common::HTTP_SUCCESS, $data ); 
    }

    /**
    * Function to Book truck / shipment
    *
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fbook-truck
    * @return boolean|yii\web\Response
    *
    */
	public function actionBookTruck(){
        $userData = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model  = new Bookings;
        $prefix = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 3);
        $middle = substr(str_shuffle('1234567890'), 0, 7);
        $suffix = substr(str_shuffle('AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'), 0, 2);
        $model->shipment_id = $prefix.$middle.$suffix;
		$model->load(['Bookings' => Yii::$app->request->post()]);
        $model->status = $lane_exist = 0; // Pending
		$priority_delivery_charge = number_format((float) 0, 2, '.', '');
		$service_action = true;
		if(Yii::$app->request->post('action') == 'get_data'){ $service_action = false; $model->shipment_id = '';  $redirect = 'Load Summary Screen'; }
		if(Yii::$app->request->post('action') == 'save_payment') $redirect = 'Payment Screen';
		if(Yii::$app->request->post('action') == 'cash_onloading') $redirect = 'Shipments Screen';
		if(Yii::$app->request->post('action') == 'save_bid_board') $redirect = 'Bid Board Screen';
		if($userData->has_cp == 'Yes'){ $model->shipment_id = $prefix.$middle.$suffix; $service_action = true; }
        $model->shipment_url = Yii::$app->request->post('shipment_url');
        $model->shipper_id = new \MongoId(Yii::$app->request->post('_id'));
        $truck_model = Trucks::findOne(['_id'=>Yii::$app->request->post('ttype')]);
        empty($truck_model) and Common::send(Common::HTTP_NO_CONTENT);
        $truck_name = $truck_model->name;
        $truck_ton = $truck_model->loading_capacity;
        $priority_delivery = Yii::$app->request->post('priority_delivery');
		$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
        $per_ton_price = $adminsettings->lane_rate;
		$ins_cal = "00.00"; $is_insured = 'No';
		if(Yii::$app->request->post('insurance_type') == '1')
        $ins_cal = number_format((float)Yii::$app->request->post('invoice_amount') * $adminsettings->insurance_rate / 100, 2, '.', '');
		$truckLane  = TruckLanes::find()->where([ 'and', ['like', 'source', Yii::$app->request->post('lp_name')], ['like', 'source_state', Yii::$app->request->post('lp_state')], ['like', 'destination', Yii::$app->request->post('up_name')], ['like', 'destination_state', Yii::$app->request->post('up_state')] ])->orderBy('created_on DESC')->one();
		$weight = number_format((float)Yii::$app->request->post('overall_weight') * 1/1000, 2, '.', '');
		$shipment_weight = $truck_ton;
		if($weight  >= $truck_ton) $shipment_weight = $weight;
		if(!empty($truckLane->_id)){ $per_ton_price = $truckLane->price_per_ton; $lane_exist = '1'; }
        if(Yii::$app->request->post('load_type')){ // Full Load case
            // $load_fare =  number_format((float)$per_ton_price * $shipment_weight * ceil(Yii::$app->request->post('est_distance')) * Yii::$app->request->post('quantity'), 2, '.', '');
            $load_fare =  number_format((float)$per_ton_price * $shipment_weight * ceil(Yii::$app->request->post('est_distance')), 2, '.', '');
            if($priority_delivery) $priority_delivery_charge = number_format((float)$load_fare * $adminsettings->priority_delivery_rate / 100, 2, '.', '');
        }else{ // Partial Load case
            $load_fare = number_format((float)$weight * $per_ton_price * ceil(Yii::$app->request->post('est_distance')), 2, '.', '');
        }
		// $load_fare = number_format((float)$load_fare * ceil(Yii::$app->request->post('est_distance')), 2, '.', '');
        $overall_total = $sub_total = number_format((float)$load_fare + $priority_delivery_charge + $ins_cal, 2, '.', '');
        $taxes = TaxTerms::find()->where(['status' => 1])->orderBy('priority')->all();
        if(!empty($taxes)){
			foreach($taxes as $tax){
				$count = 0;
				if($tax->ttype == 'Percentage'){
					$total[] = [
						'name' =>  $tax->name. ' @ rate of '.$tax->rate. ' %',
						'rate' => number_format((float)$sub_total * $tax->rate/100, 2, '.', ''),
						//'Tax Type' => $tax->type. ' @ rate of '.$tax->rate,
					];
				}else{
					$total[] = [
						'name' =>  $tax->name.' - '.$tax->rate.' fixed amount',
						'rate' => number_format((float)$tax->rate, 2, '.', ''),
						//'Tax Type' => $tax->type.' - '.$tax->rate,
					];
				}
				$overall_total = number_format((float)$sub_total + $total[$count]['rate'], 2, '.', '');
				$count++;
			}
		}
		$km_price = number_format((float)$load_fare / ceil(Yii::$app->request->post('est_distance')), 2, '.', '');
		// $pton_price = number_format((float)$load_fare / ceil(Yii::$app->request->post('est_distance')), 2, '.', '');
        $return = [
            'shipment_id' => $model->shipment_id,
            'shipment_weight' => Yii::$app->request->post('overall_weight'),
            'lane_exist' => (string)$lane_exist,
            'load_type' => Yii::$app->request->post('load_type') ? 'Full Load' : 'Partial Load',
            'from_city' => Yii::$app->request->post('lp_city'),
            'from_state' => Yii::$app->request->post('lp_state'),
            'from_address' => Yii::$app->request->post('lp_address'),
            'to_city' => Yii::$app->request->post('up_city'),
            'to_state' => Yii::$app->request->post('up_state'),
            'to_address' => Yii::$app->request->post('up_address'),
            'shipment_url' => empty($model->shipment_url) ? '' : $model->shipment_url,
            'priority_delivery' => $priority_delivery ? 'Yes' : 'No',
            'transit_date' => Yii::$app->request->post('t_date'),
            'transit_time' => Yii::$app->request->post('t_time'),
            'truck_category' => Yii::$app->request->post('category'),
            'truck_name' => $truck_name .'(' .$truck_ton. ' Ton)',
            'truck_quantity' => Yii::$app->request->post('quantity'),
            'today_price' => $per_ton_price,
            'load_fare' => (string)$load_fare,
            'distance_fare' => (string)$km_price,
            'insurance' => (string)$ins_cal,
            'priority_delivery_charges' => (string)$priority_delivery_charge,
            'sub_total' => (string)$sub_total,
            'taxes' => empty($total) ? '' : $total,
            'redirect_to' => empty($redirect) ? '' : $redirect,
            'has_cp' => $userData->has_cp,
            'service_taxno' => empty($userData->business['service_taxno']) ? '' :$userData->business['service_taxno'],
            'pancard' => empty($userData->pancard) ? '' : $userData->pancard,
        ];
		$model->shipment_detail = $return;
		$return['txn_id'] = '';
		if($service_action){
			$name = $userData->name['firstname'].' '.$userData->name['lastname'];
			if(Yii::$app->request->post('insurance_type') == '1' || Yii::$app->request->post('insurance_type') == '2') $is_insured = 'Yes';
			$model->shipment_weight = $shipment_weight;
			if(Yii::$app->request->post('action') != 'save_payment'){
				$model->shipment_type = [
					'code' => '5',
					'text' => 'Bid Load Board',
				];
				$subject = "CargoWala ! New order placed on Bid Load Board";
				$message = "Hello ".$name.",\nYour order with the reference number ".$model->shipment_id." has been placed successfully on our bid board on ".date('j M Y H:i a').". You can checkout the bids placed by the truckers on your mobile app/Web panel Bid load Board section.";
				if(!empty($userData->contact['email'])) CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
				if(!empty($userData->contact['mobile_number'])) CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message);
			}
			$model->priority_delivery_charges = $priority_delivery_charge;
			$model->invoicing_type = Yii::$app->request->post('invoicing_type');
			$model->overall_total = $overall_total;
			$model->save(false);
			$insurance_image = '';
			if(!empty(Yii::$app->request->post('insurance_image'))) $insurance_image = $this->processBase64($model, Yii::$app->request->post('insurance_image'), 'insurance_');
			$model->insurance = [
				'amount' => $ins_cal,
				'insured_option' => Yii::$app->request->post('insurance_type'),
				'image' => $insurance_image,
				'text' => $is_insured,
			];
			if(!empty(Yii::$app->request->post('items'))){
				foreach(json_decode(Yii::$app->request->post('items')) as $key => $val){
					foreach($val as $ky => $vl){
						if(!empty($vl->invoice_image)) $vl->invoice_image = $this->processBase64($model, $vl->invoice_image, 'invoice_'.$ky);
						// if(!empty($vl->insurance_image)) $vl->insurance_image = $this->processBase64($model, $vl->insurance_image, 'insurance_'.$ky);
						if(!empty($vl->parent_category)) $vl->parent_category = new \MongoId($vl->parent_category);
						if(!empty($vl->sub_category)) $vl->sub_category = new \MongoId($vl->sub_category);						
					}
					$model->items = $val; $model->save(false);
				}
			}
			if(Yii::$app->request->post('action') == 'save_payment' || Yii::$app->request->post('action') == 'cash_onloading'){
				$model->save(false);
				if(Yii::$app->request->post('action') == 'cash_onloading'){
					$model->shipment_type = [
						'code' => '1',
						'text' => 'Regular Load Board',
					];
					$subject = "CargoWala ! New order placed on Regular Load Board";
					$message = "Hello ".$name.",\nYour order with the reference number ".$model->shipment_id." has been placed successfully on ".date('j M Y H:i a').". You choose the Cash on Loading payment option for the mentioned order. We will notify once we find an appropriate trucking partner for your load delivery.";
					if(!empty($userData->contact['email'])) CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
					if(!empty($userData->contact['mobile_number'])) CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message);
				}
				$return['txn_id'] = $this->actionSaveTransactions($model->_id, $model->shipper_id, Yii::$app->request->post('action'), Yii::$app->request->post('payments'), Yii::$app->request->post('total_amount'));
			}
		}
		Common::send(Common::HTTP_SUCCESS, $return);
    }

	/*
	* Function to 
	*
	*/
	private function sendShipmentNotifications($user_id = NULL, $shipment_id = NULL, $amount = NULL, $trans_id = NULL, $paymentMode = NULL,$currency = NULL){
		$userData = Users::find()->where( ['_id' => new \MongoId($user_id) ])->one();
		$shipment = Bookings::find()->where( ['_id' => new \MongoId($shipment_id) ])->one();
		$shipment->shipment_type = [
			'code' => '1',
			'text' => 'Regular Load Board',
		];
		$shipment->save(false);
		$name = $userData->name['firstname'].' '.$userData->name['lastname'];
		$subject = "CargoWala ! Payment successfully completed  for the order number ".$shipment->shipment_id;
		$message = "Hello ".$name.",\n Your payment of ".$currency." ".$amount." for the order with the reference number ".$shipment->shipment_id." has been completed successfully on ".date('j M Y H:i a').". Your order is successfully placed on Regular Load Board. We will notify once we find an appropriate trucking partner for your load delivery.";
		if(!empty($userData->contact['email'])) CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
		if(!empty($userData->contact['mobile_number'])) CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message);
		return true;
	}
	
    /**
    * Function to Repeat order from the History
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Frepeat-order
	* Params : _id, security_token, repeat= 'true', t_time, t_date, shipment_id, action = 'get_data', invoice_image, invoice_amount, 
    * @return boolean|yii\web\Response
    *
    */
	public function actionRepeatOrder(){
        $userData = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$shipment = Bookings::find()->where([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ])->one();
		empty($shipment) and Common::send(Common::HTTP_NO_CONTENT);
		$model = new Bookings();
        $prefix = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 3);
        $middle = substr(str_shuffle('1234567890'), 0, 7);
        $suffix = substr(str_shuffle('AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz'), 0, 2);
		$model->load(['Bookings' => $shipment->oldAttributes]);
        $model->shipment_id = $prefix.$middle.$suffix;
        $model->vehicle = $model->trucker_id = $model->shipment_type = $model->start_date = $model->end_date = '';
        $model->status = $lane_exist = 0; // Pending
		$model->loading = $shipment->oldAttributes['loading'];
		$model->unloading = $shipment->oldAttributes['unloading'];
		$model->truck = $shipment->oldAttributes['truck'];
		$model->repeat = Yii::$app->request->post('repeat');
		$model->t_date = Yii::$app->request->post('t_date');
		$model->t_time = Yii::$app->request->post('t_time');
		$model->est_distance = $shipment->oldAttributes['transit']['est_distance'];
		$model->est_time = $shipment->oldAttributes['transit']['est_time'];
		$var = $model->transit;
		$var['start_date'] = $var['end_date'] = '';
		$model->transit = $var;		
		$priority_delivery_charge = number_format((float) 0, 2, '.', '');
		$service_action = true;
		if(Yii::$app->request->post('action') == 'get_data'){ $service_action = false; $model->shipment_id = '';  $redirect = 'Load Summary Screen'; }
		if(Yii::$app->request->post('action') == 'save_payment') $redirect = 'Payment Screen';
		if(Yii::$app->request->post('action') == 'save_bid_board') $redirect = 'Bid Board Screen';
		if(Yii::$app->request->post('action') == 'cash_onloading') $redirect = 'Shipments Screen';
		if($userData->has_cp == 'Yes'){ $model->shipment_id = $prefix.$middle.$suffix; $service_action = true; }
        $truck_model = Trucks::findOne(['_id' => new \MongoId($shipment->oldAttributes['truck']['ttype']), 'status' => 1]);
        empty($truck_model) and Common::send(Common::HTTP_UNPROCESSABLE);
        $truck_name = $truck_model->name;
        $truck_ton = $truck_model->loading_capacity;
        $priority_delivery = $shipment->oldAttributes['truck']['priority_delivery'];
		$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
        $per_ton_price = $adminsettings->lane_rate;
		$ins_cal = "00.00"; $is_insured = 'No';
		if($shipment->oldAttributes['insurance']['insured_option'] == '1')
        $ins_cal = number_format((float)Yii::$app->request->post('invoice_amount') * $adminsettings->insurance_rate / 100, 2, '.', '');
		$truckLane  = TruckLanes::find()->where([ 'and', ['like', 'source', $shipment->oldAttributes['loading']['lp_address'] ], ['like', 'source_state', $shipment->oldAttributes['loading']['lp_state'] ], ['like', 'destination', $shipment->oldAttributes['unloading']['up_address'] ], ['like', 'destination_state', $shipment->oldAttributes['unloading']['up_state'] ] ])->orderBy('created_on DESC')->one();
		$weight = number_format((float) $shipment->oldAttributes['overall_weight'] * 1/1000, 2, '.', '');
		$shipment_weight = $truck_ton;
		if($weight  >= $truck_ton) $shipment_weight = $weight;
		if(!empty($truckLane->_id)){ $per_ton_price = $truckLane->price_per_ton; $lane_exist = '1'; }
        /* if($shipment->oldAttributes['truck']['load_type']){ // Full Load case
            $load_fare =  number_format((float)$per_ton_price * $shipment_weight * Yii::$app->request->post('quantity'), 2, '.', '');
            if($priority_delivery) $priority_delivery_charge = number_format((float)$load_fare * $adminsettings->priority_delivery_rate / 100, 2, '.', '');
        }else{ // Partial Load case
            $load_fare = number_format((float)$weight * $per_ton_price, 2, '.', '');
        } */
        if($shipment->oldAttributes['truck']['load_type']){ // Full Load case
            // $load_fare =  number_format((float)$per_ton_price * $shipment_weight * ceil($shipment->oldAttributes['transit']['est_distance']) * $shipment->oldAttributes['truck']['quantity'], 2, '.', '');
            $load_fare =  number_format((float)$per_ton_price * $shipment_weight * ceil($shipment->oldAttributes['transit']['est_distance']), 2, '.', '');
            if($priority_delivery) $priority_delivery_charge = number_format((float)$load_fare * $adminsettings->priority_delivery_rate / 100, 2, '.', '');
        }else{ // Partial Load case
            $load_fare = number_format((float)$weight * $per_ton_price * ceil($shipment->oldAttributes['transit']['est_distance']), 2, '.', '');
        }		
        $overall_total = $sub_total = number_format((float)$load_fare + $priority_delivery_charge + $ins_cal, 2, '.', '');
        $taxes = TaxTerms::find()->where(['status' => 1])->orderBy('priority')->all();
        if(!empty($taxes)){
			foreach($taxes as $tax){
				$count = 0;
				if($tax->ttype == 'Percentage'){
					$total[] = [
						'name' =>  $tax->name. ' @ rate of '.$tax->rate. ' %',
						'rate' => number_format((float)$sub_total * $tax->rate/100, 2, '.', ''),
						//'Tax Type' => $tax->type. ' @ rate of '.$tax->rate,
					];
				}else{
					$total[] = [
						'name' =>  $tax->name.' - '.$tax->rate.' fixed amount',
						'rate' => number_format((float)$tax->rate, 2, '.', ''),
						//'Tax Type' => $tax->type.' - '.$tax->rate,
					];
				}
				$overall_total = number_format((float)$sub_total + $total[$count]['rate'], 2, '.', '');
				$count++;				
			}
		}
		$km_price = number_format((float)$load_fare / ceil($shipment->oldAttributes['transit']['est_distance']), 2, '.', '');
        $return = [
            'shipment_id' => $model->shipment_id,
            'shipment_weight' => $shipment->oldAttributes['overall_weight'],
            'lane_exist' => (string)$lane_exist,
            'load_type' => $shipment->oldAttributes['truck']['load_type'] ? 'Full Load' : 'Partial Load',
            'from_city' => $shipment->oldAttributes['loading']['lp_city'],
            'from_state' => $shipment->oldAttributes['loading']['lp_state'],
            'from_address' => $shipment->oldAttributes['loading']['lp_address'],
            'to_city' => $shipment->oldAttributes['unloading']['up_city'],
            'to_state' => $shipment->oldAttributes['unloading']['up_state'],
            'to_address' => $shipment->oldAttributes['unloading']['up_address'],
            'shipment_url' => empty($model->shipment_url) ? '' : $model->shipment_url,
            'priority_delivery' => $priority_delivery ? 'Yes' : 'No',
            'transit_date' => Yii::$app->request->post('t_date'),
            'transit_time' => Yii::$app->request->post('t_time'),
            'truck_category' => $shipment->oldAttributes['truck']['category'],
            'truck_name' => $truck_name .'(' .$truck_ton. ' Ton)',
            'truck_quantity' => $shipment->oldAttributes['truck']['quantity'],
            'today_price' => $per_ton_price,
            'load_fare' => (string)$load_fare,
            'distance_fare' => (string)$km_price,
            'insurance' => (string)$ins_cal,
            'priority_delivery_charges' => (string)$priority_delivery_charge,
            'sub_total' => (string)$sub_total,
            'taxes' => empty($total) ? '' : $total,
            'redirect_to' => empty($redirect) ? '' : $redirect,
            'has_cp' => $userData->has_cp,
            'service_taxno' => empty($userData->business['service_taxno']) ? '' :$userData->business['service_taxno'],
            'pancard' => empty($userData->pancard) ? '' : $userData->pancard,
        ];
		$model->shipment_detail = $return;
		if($service_action){
			$name = $userData->name['firstname'].' '.$userData->name['lastname'];
			if($shipment->oldAttributes['insurance']['insured_option'] == '1' || $shipment->oldAttributes['insurance']['insured_option'] == '2') $is_insured = 'Yes';
			$model->shipment_weight = $shipment_weight;
			if(Yii::$app->request->post('action') != 'save_payment'){
				$model->shipment_type = [
					'code' => '5',
					'text' => 'Bid Load Board',
				];
				$subject = "CargoWala ! New order placed on Bid Load Board";
				$message = "Hello ".$name.",\nYour order with the reference number ".$model->shipment_id." has been placed successfully on our bid board on ".date('j M Y H:i a').". You can checkout the bids placed by the truckers on your mobile app/Web panel Bid load Board section.";
				if(!empty($userData->contact['email'])) CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
				if(!empty($userData->contact['mobile_number'])) CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message);
			}		
			$model->priority_delivery_charges = $priority_delivery_charge;
			$model->invoicing_type = $shipment->oldAttributes['invoicing_type'];
			$model->overall_total = $overall_total;
			$model->save(false);
			$insurance_image = '';
			/* if(!empty($shipment->oldAttributes['insurance']['image'])){
				// $insurance_image = $this->copyImage($shipment, $shipment->oldAttributes['insurance']['image'], $model->shipment_id, $model);
				$insurance_image = $this->copyImage($shipment, $shipment->oldAttributes['insurance']['image'], $model->shipment_id, $model);
			} */
			$model->insurance = [
				'amount' => $ins_cal,
				'insured_option' => $shipment->oldAttributes['insurance']['insured_option'],
				'image' => $insurance_image,
				'text' => $is_insured,
			];
			if(!empty(Yii::$app->request->post('invoice_image'))) $invoice_image = $this->processBase64($shipment, Yii::$app->request->post('invoice_image'), 'invoice_');
			if(!empty($shipment->oldAttributes['items'])){
				foreach($shipment->oldAttributes['items'] as $key => $val){
					if(!empty($val['invoice_image'])) $val['invoice_image'] = $invoice_image;
					if(!empty($val['parent_category'])) $val['parent_category'] = new \MongoId($val['parent_category']);
					if(!empty($val['sub_category'])) $val['sub_category'] = new \MongoId($val['sub_category']);		
					$items[] = $val; $model->items = $items; 
				}
			}
			if(Yii::$app->request->post('action') == 'save_payment' || Yii::$app->request->post('action') == 'cash_onloading'){
				$model->save(false);
				if(Yii::$app->request->post('action') == 'cash_onloading'){
					$model->shipment_type = [
						'code' => '1',
						'text' => 'Regular Load Board',
					];
					$subject = "CargoWala ! New order placed on Regular Load Board";
					$message = "Hello ".$name.",\nYour order with the reference number ".$model->shipment_id." has been placed successfully on ".date('j M Y H:i a').". You choose the Cash on Loading payment option for the mentioned order. We will notify once we find an appropriate trucking partner for your load delivery.";
					if(!empty($userData->contact['email'])) CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
					if(!empty($userData->contact['mobile_number'])) CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message);
				}
				$return['txn_id'] = $this->actionSaveTransactions($model->_id, $model->shipper_id, Yii::$app->request->post('action'), Yii::$app->request->post('payments'), Yii::$app->request->post('total_amount'));
			}				
		}
		Common::send(Common::HTTP_SUCCESS, $return);
    }

	/**
    * Function to Accept Trucker's Bid on the Bid Load Board
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Freschedule-order
	* Params : _id, security_token, shipment_id, t_date, t_time
    * @return boolean|yii\web\Response
    *
    */	
	public function actionRescheduleOrder(){
		$userData = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$shipment  = Bookings::findOne([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')), 'status' =>[0,6,8] ]);
		empty($shipment) and Common::send(Common::HTTP_NO_CONTENT);
		// if!($shipment->status == 0 || $shipment->status == 6) Common::send(Common::HTTP_UNPROCESSABLE); // this can't be reschedule
		$shipment->t_date = Yii::$app->request->post('t_date');
		$shipment->t_time = Yii::$app->request->post('t_time');
		$return = $shipment->shipment_detail;
		$return['transit_date'] = Yii::$app->request->post('t_date');
		$return['transit_time'] = Yii::$app->request->post('t_time');
		$shipment->load(['Bookings' => $shipment->oldAttributes]);
		$shipment->shipment_detail = $return;
		/* $name = $userData->name['firstname'].' '.$userData->name['lastname'];		
		$subject = "CargoWala ! New order placed on Regular Load Board";
		$message = "Hello ".$name.",\nYou have changed the Time of your order with reference number ".$shipment->shipment_id." from <<Time>> to <<Time>> We will notify the Trucker and the Driver about the same. In case, you can also get in touch with the Trucker at <<XXXX>> and Driver at <<XXXX>> You can also contact us at <<XXXX>> for any further queries.";
		CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
		CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message); */
		$shipment->save(false) and Common::send(Common::HTTP_SUCCESS, $return);
	}
	
	/**
    * Function to Track Shipments which are In-Transit or Completed
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Ftrack-shipment
	* Params : _id, security_token, shipment_id,
    * @return boolean|yii\web\Response
    *
    */	
	public function actionTrackShipment(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model = Bookings::find()->where([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')), 'status' =>[4,5]])->one();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$return = '';
		if(!empty($model->vehicle)){
			foreach($model->vehicle as $key => $val){
				$truck = $driver = '';
				if(!empty($val['id'])) $truck = Vehicles::find()->where([ '_id' => $val['id']])->one();
				if(!empty($val['driver'])) $driver = Drivers::find()->where([ '_id' => $val['driver']])->one();
				$started_at  = [
					'data_for' => 'start_point',
					'address' => empty($model->loading['lp_address']) ? '' : $model->loading['lp_address'],
					'city' => empty($model->loading['lp_city']) ? '' : $model->loading['lp_city'],
					'state' => empty($model->loading['lp_state']) ? '' : $model->loading['lp_state'],
					'latitude' => empty($model->loading['lp_latitude']) ? '' : $model->loading['lp_latitude'],
					'longitude' => empty($model->loading['lp_longitude']) ? '' : $model->loading['lp_longitude'],
					'start_time' => empty($model->transit['start_date']) ? '' : $model->transit['start_date'],
					'end_time' => empty($model->transit['end_date']) ? '' : $model->transit['end_date'],
				];
				$ended_at  = [  // if the shipment is In-Transit 
					'data_for' => 'end_point',
					'address' => '',
					'city' => '',
					'state' => '',
					'latitude' => '',
					'longitude' => '',
					'start_time' => '',
					'end_time' => '',
				];
				if($model->status == 5){ // Completed Shipments
					$ended_at = [
						'data_for' => 'end_point',
						'address' => empty($model->unloading['up_address']) ? '' : $model->unloading['up_address'],
						'city' => empty($model->unloading['up_city']) ? '' : $model->unloading['up_city'],
						'state' => empty($model->unloading['up_state']) ? '' : $model->unloading['up_state'],
						'latitude' => empty($model->unloading['up_latitude']) ? '' : $model->unloading['up_latitude'],
						'longitude' => empty($model->unloading['up_longitude']) ? '' : $model->unloading['up_longitude'],
						'start_time' => empty($model->transit['start_date']) ? '' : $model->transit['start_date'],
						'end_time' => empty($model->transit['end_date']) ? '' : $model->transit['end_date'],						
					];
				}
				$return['location'] = [$started_at, $ended_at];
				$start_on = $end_at = '';
				$current_location = [
					'latitude' => '',
					'longitude' => '',
					'last_updated' => '',
				];
				$truckStatus = 'not started';
				if($val['status'] == 1){
					$truckStatus = 'In-Transit';
					$start_on = $val['start_on'];
					$current_location = [
						'latitude' => empty($driver->location) ? '' : $driver->location['lat'],
						'longitude' => empty($driver->location) ? '' : $driver->location['lng'],
						'last_updated' => empty($driver->location) ? '' : $driver->location['updated_on'],
					];
				}
				if($val['status'] == 2){
					$start_on = $val['start_on'];
					$end_at = $val['end_at'];
					$truckStatus = 'Completed';
					$current_location = [
						'latitude' => $val['location']['lat'],
						'longitude' => $val['location']['lng'],
						'last_updated' => $end_at,
					];					
				}
				if($model->status == 4){ // In-transit
					$return ['truck'][] = [
						'vehicle_number' => empty($truck->registration['vehicle_number']) ? '' : $truck->registration['vehicle_number'],
						'status' => $truckStatus,
						'start_on' => $start_on,
						'end_at' => $end_at,
						'current_location' => $current_location,
					];
				}
			}
		}
 		Common::send(Common::HTTP_SUCCESS, $return); 		
	}
		
    /**
    * Function to load user's Bid on the Bid Load board
    *
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-bids
    * @return boolean|yii\web\Response
    *
    */	
	public function actionGetBids(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$offset = empty(Yii::$app->request->post('next_records')) ? 0 : Yii::$app->request->post('next_records');
		$limit = empty(Yii::$app->request->post('limit')) ? 4 : Yii::$app->request->post('limit');				
		// fetch only shipments which are posted by the logged-in user and which are not expired and whose status is not Accepted
		$model = Bookings::find()->select(['shipment_detail'])->where(['>=', 'transit.t_expdatetime', time()])->andWhere(['shipper_id' => $userModel->_id, 'status' => 0, 'shipment_type.code' => '5'])->orderBy('transit.t_expdatetime')->offset($offset)->limit($limit)->all();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		foreach($model as  $key=>$val){
			// $res = Bids::findAll([ 'shipment_id' => new \MongoId($val->_id) ]);
			$res = Bids::find()->where([ 'shipment_id' => new \MongoId($val->_id), 'status' => 0])->all();
			$counter = 0;
			if(!empty($res)) $counter = count($res);
			$shipments [] = [
				'id' => $val->_id->__toString(),
				'shipment_id' => $val->shipment_detail['shipment_id'],
				'load_type' => $val->shipment_detail['load_type'],
				'from_city' => $val->shipment_detail['from_city'],
				'from_state' => $val->shipment_detail['from_state'],
				'from_address' => $val->shipment_detail['from_address'],
				'to_city' => $val->shipment_detail['to_city'],
				'to_state' => $val->shipment_detail['to_state'],
				'to_address' => $val->shipment_detail['to_address'],
				'priority_delivery' => $val->shipment_detail['priority_delivery'],
				'transit_date' => $val->shipment_detail['transit_date'],
				'transit_time' => $val->shipment_detail['transit_time'],
				'truck_category' => $val->shipment_detail['truck_category'],
				'truck_name' => $val->shipment_detail['truck_name'],
				'truck_quantity' => $val->shipment_detail['truck_quantity'],
				'insurance_amount' => $val->shipment_detail['insurance'],
				'bids_quantity' => $counter,
			];
		}
		Common::send(Common::HTTP_SUCCESS, $shipments);
	}
	
	/**
    * Function to load Trucker's Bid list on particular shipment
    *
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fview-bids
	* Params : _id, security_token, shipment_id
    * @return boolean|yii\web\Response
    *
    */
	public function actionViewBids(){
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model = Bids::find()->where( ['shipment_id' => new \MongoId(Yii::$app->request->post('shipment_id')), 'status' => 0 ])->with('user')->All();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		// echo '<pre>'; print_r($model); die;
		foreach($model as  $key=>$val){
			$truckerName = $val->user->name['firstname'].' '.$val->user->name['lastname'];
			$bidData[] = [
				'_id' => $val->_id->__toString(),
				'shipment_id' => $val->shipment_id->__toString(),
				'trucker_name' => empty($val->user->name) ? '' : $truckerName,
				'business_name' => empty($val->user->business['name']) ? $truckerName : $val->user->business['name'],
				'price' => empty($val->price) ? '' : number_format((float)$val->price, 2, '.', ''),
				'description' => empty($val->description) ? '' : $val->description,
				'rating' => 3.5,
			];
		}
		Common::send(Common::HTTP_SUCCESS, $bidData);		
	}
	
	/**
    * Function to Reject Trucker's Bid on the Bid Load Board
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Freject-bid
	* Params : _id, security_token, bid_id
    * @return boolean|yii\web\Response
    *
    */
	public function actionRejectBid(){
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		// $model  = Bids::findOne( ['shipment_id' => new \MongoId(Yii::$app->request->post('shipment_id')), 'trucker_id' => new \MongoId(Yii::$app->request->post('_id')) ]);
		$model  = Bids::findOne( ['_id' => new \MongoId(Yii::$app->request->post('bid_id')) ]);
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$model->status = 2; // Rejected
		$this->updateTransactions('reject', false, $model->trucker_id, Yii::$app->request->post('bid_id'));
		$model->save() and Common::send(Common::HTTP_SUCCESS);
	}
	
	/*
    * Function to Accept Trucker's Bid on the Bid Load Board
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Faccept-bid
	* Params : _id, security_token, bid_id, shipment_id
    * @return boolean|yii\web\Response
    *
    */
	private function actionAcceptBid($shipment_id = NULL, $bid_id = NULL, $userModel){
		$shipment_id = empty(Yii::$app->request->post('shipment_id')) ? $shipment_id : Yii::$app->request->post('shipment_id');
		$bid_id = empty(Yii::$app->request->post('bid_id')) ? $bid_id : Yii::$app->request->post('bid_id');
		$model  = Bids::find()->where( ['_id' => new \MongoId($bid_id) ])->with('user')->one(); // Trucker Name
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$shipment = Bookings::find()->where( ['_id' => new \MongoId($shipment_id) ])->one();
		empty($shipment) and Common::send(Common::HTTP_NO_CONTENT);
		$truckerName = $model->user->name['firstname'].' '.$model->user->name['lastname'];
		$shipperName = $userModel->name['firstname'].' '.$userModel->name['lastname'];
		$subject = "CargoWala ! Order #".$shipment->shipment_id." is accepted by Shipper on Bid Load Board.";
		$shipperMessage = "Hello ".$shipperName.",\nYou have accepted the bid of ".$truckerName." for Rs. of ".$model->price.", Your Order will be Shipped on ".$shipment->transit['t_date']." at ".$shipment->transit['t_time'].". Please provide below mentioned Documents to the Driver at the Time of Pickup \n1. Builty\n 2. Invoices of the Items\n 3. Photo Copy Of your Pan Card\n 4. Photo Copy of the Inc (If Any)\n Please contact our support for further queries 98XXXXXX82.";
		$truckerMessage = "Hello ".$truckerName.",\nYour bid for Rs. of ".$model->price." has been accepted by the Shipper ".$shipperName.". You need to ship the order on ".$shipment->transit['t_date']." at ".$shipment->transit['t_time'].". Please provide below mentioned Documents to the Driver at the Time of Pickup \n1. Builty\n 2. Invoices of the Items\n 3. Photo Copy Of your Pan Card\n 4. Photo Copy of the Inc (If Any)\n Please contact our support for further queries 98XXXXXX82.";
		CommonModel::sendEmailNotification($userModel->contact['email'], $subject, $shipperMessage);
		CommonModel::sendSMSNotification($userModel->contact['mobile_number'], $shipperMessage);
		CommonModel::sendEmailNotification($model->user->contact['email'], $subject, $truckerMessage);
		CommonModel::sendSMSNotification($model->user->contact['mobile_number'], $truckerMessage);
		$this->updateTransactions('accept', $shipment_id, $model->trucker_id, false);
		$model->status = 1; $model->save(false);
		$shipment->status = 6;
		$shipment->trucker_id = new \MongoId($model->trucker_id);
		$shipment->save(false) and Common::send(Common::HTTP_SUCCESS);
	}
	
	// Function to Update trucker Transactions 
	private function updateTransactions($type, $shipment_id, $trucker_id, $bid_id){
		if($type == 'accept'){
			$bids = Bids::findAll([ 'shipment_id' => new \MongoId($shipment_id), 'trucker_id' => ['$ne'=>new \MongoId($trucker_id)] ]);
		}elseif($type == 'remove'){
			$bids = Bids::findAll([ 'shipment_id' => new \MongoId($shipment_id) ]);
		}else{ // reject
			$bids = Bids::findAll( ['_id' => new \MongoId($bid_id) ]);
		}
		if(empty($bids)) return true;
		foreach($bids as $row){
			// $transModel = Transactions::findOne( ['user_id' => new \MongoId($row->trucker_id), 'shipment_id' => new \MongoId($row->shipment_id)]);
			// if(empty($transModel))
			// if($transModel->ttype['code'] == 1){ // if transaction is used from Free Transactions
				$truckerModel = Users::findOne( ['_id' => new \MongoId($row->trucker_id) ]);				
				$truckerModel->load(['Users' => $truckerModel->oldAttributes]);
				$truckerModel->updateCounters(['membership.transactions' => 1]);
				$truckerModel->save(false);
			// }
			}
		}
	
    /**
    * Function to Remove Trucker's Bid  from the Bid Load Board
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fremove-shipment
	* Params : _id, security_token, shipment_id
    * @return boolean|yii\web\Response
    *
    */	
	public function actionRemoveShipment(){
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		// Bids::deleteAll([ 'shipment_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ]);
		$shipment  = Bookings::findOne( ['_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ]);
		empty($shipment) and Common::send(Common::HTTP_NO_CONTENT);
		$this->updateTransactions('remove', Yii::$app->request->post('shipment_id'), false, false);
		$shipment->status = 7;  // Removed By Shipper
		$shipment->save(false) and Common::send(Common::HTTP_SUCCESS);
	}	
	
    /**
    * Function to Remove Trucker's Bid  from the Bid Load Board
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fcancel-shipment
	* Params : _id, security_token, shipment_id
    * @return boolean|yii\web\Response
    *
    */	
	public function actionCancelShipment(){
		$userData = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		// Bids::deleteAll([ 'shipment_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ]);
		$model  = Bookings::findOne( ['_id' => new \MongoId(Yii::$app->request->post('shipment_id'))] );
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		if(!empty($model->vehicle)){
			foreach($model->vehicle as  $val){
				if(!empty($val['id'])){
					$vehicleModel = Vehicles::findOne(['_id' => new \MongoId($val['id'])]);
					if(!empty($vehicleModel) && $vehicleModel->truck_status){
						$vehicleModel->load(['Vehicles' => $vehicleModel->oldAttributes]);										
						$vehicleModel->truck_status = 0; // Un-Assign
						$vehicleModel->save(false);
					}
				}
				if(!empty($val['driver'])){
					$driverModel = Drivers::findOne(['_id' => new \MongoId($val['driver'])]);
					if(!empty($driverModel) && $driverModel->driver_status){
						$driverModel->load(['Drivers' => $driverModel->oldAttributes]);								
						$driverModel->driver_status = 0; // Un-Assign
						$driverModel->save(false);
					}
				}
			}
		}
		$this->updateTransactions('remove', Yii::$app->request->post('shipment_id'), false, false);
		$model->status = 2; // Cancelled By Shipper
		/* $name = $userData->name['firstname'].' '.$userData->name['lastname'];		
		$subject = "CargoWala ! New order placed on Regular Load Board";
		$message = "Hello ".$name.",\nYou have changed the Time of your order with reference number ".$model->shipment_id." from <<Time>> to <<Time>> We will notify the Trucker and the Driver about the same. In case, you can also get in touch with the Trucker at <<XXXX>> and Driver at <<XXXX>> You can also contact us at <<XXXX>> for any further queries.";
		CommonModel::sendEmailNotification($userData->contact['email'], $subject, $message);
		CommonModel::sendSMSNotification($userData->contact['mobile_number'], $message); */
		// Dear <<Shipper's Name>>, Your order with reference number <<XXX>> has been cancelled. We are sorry for the inconvinience caused. In the mean time, we are finding a new Trucker for your order. For any further queries contact us at <<XXXX>>.(E-mail, Sms, Push)
		$model->save(false) and Common::send(Common::HTTP_SUCCESS);
	}
	
    /**
    * Function to load user's shipments on the bases of their Status
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fview-shipments
	* Params : _id, security_token, starts_from, limit, type = active / history
    * @return boolean|yii\web\Response
    *
    */	
	public function actionViewShipments(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$this->checkShipments(); // Function to change the shipment status to Expire and Un-Assign the Vehicles and Drivers Assigned		
		$offset = empty(Yii::$app->request->post('starts_from')) ? 0 : Yii::$app->request->post('starts_from');
		$limit = empty(Yii::$app->request->post('limit')) ? 10 : Yii::$app->request->post('limit');
		// fetch only shipments which are posted by the logged-in user and which are not expired and whose status is not Accepted
		if(Yii::$app->request->post('type') == 'active')
		$model = Bookings::find()->where([ 'and', ['shipper_id' => $userModel->_id], [ 'or', ['status' =>[0,1,4,6,8]] ] ])->orderBy('transit.t_expdatetime')->offset($offset)->limit($limit)->with('truckers')->all();
		// $model = Bookings::find()->where([ 'and', ['shipper_id' => $userModel->_id], [ 'or', ['status' =>[4,6,8]] ] ])->andWhere(['>=', 'transit.t_expdatetime', time()])->orderBy('transit.t_expdatetime')->offset($offset)->limit($limit)->with('truckers')->all();
		else $model = Bookings::find()->where([ 'and', ['shipper_id' => $userModel->_id], [ 'or',['status' =>[2,3,5,7]]] ])->orderBy('transit.t_expdatetime desc')->offset($offset)->limit($limit)->with('truckers')->all();
		$trucker_name = $trucker_number = $trucker_alternate_number = $trucker_business_name = '' ;
		$trucker_rating = 0.0;
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		foreach($model as  $key=>$val){
			$shipment_status = '';
			if($val->status == 1) $shipment_status = 'Cancelled By Trucker';
			if($val->status == 2) $shipment_status = 'Cancelled By Shipper';
			if($val->status == 3) $shipment_status = 'Expired';
			if($val->status == 5) $shipment_status = 'Completed';
			if($val->status == 6 || $val->status == 8) $shipment_status = 'Pending';
			if($val->status == 7) $shipment_status = 'In Complete';
			if($val->status == 4){
				$shipment_status = 'In Transit';
				$trucker_name = $val->truckers->firstname .' '. $val->truckers->lastname;
				$trucker_number = $val->truckers->mobile_number;
				$trucker_alternate_number = $val->truckers->mobile_number;
				$trucker_business_name = $val->truckers->mobile_number;
				$trucker_rating = '3.5';
			}
			$shipments [] = [
				'id' => $val->_id->__toString(),
				'shipment_id' => $val->shipment_detail['shipment_id'],
				'load_type' => $val->shipment_detail['load_type'],
				'from_city' => $val->shipment_detail['from_city'],
				'from_state' => $val->shipment_detail['from_state'],
				'from_address' => $val->shipment_detail['from_address'],
				'to_city' => $val->shipment_detail['to_city'],
				'to_state' => $val->shipment_detail['to_state'],
				'to_address' => $val->shipment_detail['to_address'],
				'shipment_url' => empty($val->shipment_detail['shipment_url']) ? '' : $val->shipment_detail['shipment_url'],
				'priority_delivery' => $val->shipment_detail['priority_delivery'],
				'transit_date' => $val->shipment_detail['transit_date'],
				'transit_time' => $val->shipment_detail['transit_time'],
				'truck_category' => $val->shipment_detail['truck_category'],
				'truck_name' => $val->shipment_detail['truck_name'],
				'truck_quantity' => $val->shipment_detail['truck_quantity'],
				'shipment_status' => $shipment_status,
				'trucker_name' => $trucker_name,
				'trucker_number' => $trucker_number,
				'trucker_alternate_number' => $trucker_alternate_number,
				'trucker_business_name' => $trucker_business_name,
				'trucker_rating' => $trucker_rating,
				'today_date' => date('j M Y'),
				'current_time' => time(),
				'exp_time' => $val->transit['t_expdatetime'],				
			];
		}
		Common::send(Common::HTTP_SUCCESS, $shipments);
	}
	
   /**
    * Function to Assign Drivers to the particular Shipments
    *
    * @Required params:  _id, security_token, shipment_id
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fview-assigned-list
    * @return boolean|yii\web\Response
    *
    */
	public function actionViewAssignedList(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model = Bookings::find()->where([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ])->one();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		if(!empty($model->vehicle)){
			foreach($model->vehicle as $key => $val){
				$truck = $driver = '';
				if(!empty($val['id'])) $truck = Vehicles::find()->where([ '_id' => $val['id']])->one();
				if(!empty($val['driver'])) $driver = Drivers::find()->where([ '_id' => $val['driver']])->one();
				$consignee [] = [
					'vehicle_id' => empty($val['id']) ? '' : $val['id']->__toString(),
					'vehicle_num' => empty($truck->registration['vehicle_number']) ? '' : $truck->registration['vehicle_number'],
					'driver_id' => empty($val['driver']) ? '' : $val['driver']->__toString(),
					'driver_name' => empty($driver->name['firstname']) ? '' : $driver->name['firstname'].' '.$driver->name['lastname'],
					'driver_num' => empty($driver->mobile_no) ? '' : $driver->mobile_no,
					'driver_pic' => empty($driver->image) ? '' : $driver->image	
				];
			}			
		}else{
			for($i = 1; $i <= $model->shipment_detail['truck_quantity']; $i++){
				$consignee [] = [ 'vehicle_id' => '', 'vehicle_num' => '', 'driver_id' => '', 'driver_name' => '', 'driver_num' => '', 'driver_pic' => ''];
			}
		}
 		Common::send(Common::HTTP_SUCCESS, $consignee); 
    }	
	
    /**
    * Function to load user's Bid detail by the bid_id
    *
	* Url : http://cargowala.com/frontend/web/index.php?r=shipper%2Fuser%2Fget-bid-detail
	* Params : _id, security_token, shipment_id
    * @return boolean|yii\web\Response
    *
    */
	public function actionGetBidDetail(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model = Bookings::find()->where([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ])->one();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		foreach($model->items as $key => $val){
			if(!empty($val['invoice_image'])) $val['invoice_image'] = CommonModel::getMediaPath($model, $val['invoice_image']);
			$itemsarr[] = $val;
		}
		$shipments [] = [
			'shipment_id' => $model->shipment_detail['shipment_id'],
			'load_type' => $model->shipment_detail['load_type'],
			'from_address' => $model->shipment_detail['from_address'],
			'to_address' => $model->shipment_detail['to_address'],
			'priority_delivery' => $model->shipment_detail['priority_delivery'],
			'transit_date' => $model->shipment_detail['transit_date'],
			'transit_time' => $model->shipment_detail['transit_time'],
			'truck_category' => $model->shipment_detail['truck_category'],
			'truck_name' => $model->shipment_detail['truck_name'],
			'truck_quantity' => $model->shipment_detail['truck_quantity'],
			'overall_weight' => $model->overall_weight,
			'items_arr' => $itemsarr,
			'insurance_option' => empty($model->insurance['insured_option']) ? '' : $model->insurance['insured_option'],
			'insurance_image' => empty($model->insurance['image']) ? '' : CommonModel::getMediaPath($model, $model->insurance['image']),
		];
		Common::send(Common::HTTP_SUCCESS, $shipments);
	}	
    
	private function uploadImage($model){
		$path = CommonModel::mkdir($model::PARENTDIR . $model::CHILDDIR . "-" . $model->_id, 0755, true);
		if(Yii::$app->request->post('image_url')){
		   $image = file_get_contents(Yii::$app->request->post('image_url'));
		   $image_name = basename(Yii::$app->request->post('image_url'));
		   $ext   = array_pop(@explode(".",$image_name)); 	
		   $image_name = 'image_' . time() . '.jpg';
		   file_put_contents($path . DS . $image_name, $image);
		   $model->image = empty($model->image)?$image_name:basename($model->image);
		}elseif(Yii::$app->request->post('image_base64')){
            $image = str_replace('data:image/png;base64,', '', Yii::$app->request->post('image_base64'));
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $image_name = 'image_' . time() . '.jpg';
            if(!empty($model->oldAttributes['image'])){
                $oldImage = empty($model->oldAttributes['image']) ? '' : $model->oldAttributes['image'];
                CommonModel::DelFile($path.DS.$oldImage);
            }
		    file_put_contents($path . DS . $image_name, $image);		
            //$model->image = empty($model->image)?$image_name:basename($model->image);
            $model->image = $image_name;
            $model->save(false);
	    }
    }
	
    /**
    * Function to authenticate user on the bases of his/her user_id and security_token
    */
    private function authenticateUser(){
        (empty(Yii::$app->request->post('_id')) || empty(Yii::$app->request->post('security_token')) )and Common::send(Common::HTTP_BAD_REQUEST);
        $model  = Users::findOne(['_id'=>Yii::$app->request->post('_id')]);
		empty($model) and Common::send(Common::CUSTOM_INACTIVE); // if User not found use code(404) "HTTP_NOT_FOUND"
        if($model->status == 'Inactive') Common::send(Common::CUSTOM_INACTIVE);
        $security_token = md5($model->contact['email'].$model->password.'cargowala-security-token');
		Yii::$app->request->post('security_token');
        if($security_token == Yii::$app->request->post('security_token')) return $model; // if verified
        Common::send(Common::HTTP_NOT_ACCEPTABLE);
    }
    
    /**
    * Function to Process image from  base64 string and save it to the particular folder.
    */    
    private function processBase64($model, $img, $imgName){
		$path = CommonModel::mkdir($model::PARENTDIR . $model::CHILDDIR . "-" . $model->_id, 0755, true);
        $image = str_replace('data:image/png;base64,', '', $img);
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image = base64_decode($image);
        $image_name = $imgName.time().'.jpg';
        file_put_contents($path . DS . $image_name, $image);
        return $image_name;
    }

    /**
    * Function to Copy image from the particular folder to the temporary location.
    */    
    private function copyImage($model, $img, $imgName, $shipment){
		$path = CommonModel::mkdir($model::PARENTDIR . $model::CHILDDIR . "-" . $model->_id, 0755, true);
		// $dest = CommonModel::mkdir('/tmp/shipments-'. $shipment->shipment_id, 0755, true);
		$dest = CommonModel::mkdir($model::PARENTDIR . $model::CHILDDIR . "-" . $shipment->_id, 0755, true);
		$src = $path.'/'.$img; 
		$imagePath = $dest.'/'.$imgName.'.jpg';
		copy($src, $imagePath); return $imgName.'.jpg';
    }

    public function actionSavePayment($shipment_id = NULL, $user_id = NULL){
		$this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
        $model = Payments::findOne(['shipment_id' => $shipment_id]);
		if(empty($model)) $model = new Payments();
		$model->load(['Payments' => Yii::$app->request->post('payment')]);
		$model->shipment_id =  new \MongoId($shipment_id);
		$model->payer_id = $user_id; $model->ptype = 0; // Partial Payment
		if(!empty($model)){
			$results = $model->transactions;
			$results[] = [
				't_id' => md5($user_id. md5(time())),
				'part' => empty(Yii::$app->request->post('payment')['part']) ? NULL : Yii::$app->request->post('payment')['part'],
				'amount' => empty(Yii::$app->request->post('payment')['amount']) ? NULL : Yii::$app->request->post('payment')['amount'],
				'ttype' => empty(Yii::$app->request->post('payment')['ttype']) ? 0 : Yii::$app->request->post('payment')['ttype'],
				'mode' => empty(Yii::$app->request->post('payment')['mode']) ? NULL : Yii::$app->request->post('payment')['mode'],
				'remarks' => empty(Yii::$app->request->post('payment')['remarks']) ? NULL : Yii::$app->request->post('payment')['remarks'],
				'tstatus' => empty(Yii::$app->request->post('payment')['tstatus']) ? NULL : Yii::$app->request->post('payment')['tstatus'],
			];
			$model->outstanding  = number_format( (float)$model->outstanding - Yii::$app->request->post('payment')['amount'], 2, '.', '' );
		}
		if(Yii::$app->request->post('payment')['amount'] >= Yii::$app->request->post('payment')['sub_total']) $model->ptype = 1;
		$model->transactions =  $results;
				$model->save(false);
				$transaction = new Transactions();
				$transaction->payment_id = new \MongoId($model->_id);
				$transaction->amount = $model->amount;
				$transaction->user_id = new \MongoId($user_id);
				$transaction->shipment_id = new \MongoId($model->shipment_id);
				$transaction->status = ['code' => '1', 'text' => 'success' ]; // will be changed later on
				$transaction->ttype = [ 'code' => '1', 'text' => 'free' ];
				$transaction->debug = '';
				$transaction->save(false);
		
		return true;
    }
	
	public function actionAddPayments_20_10_2016($shipment_id = NULL, $user_id = NULL, $action = NULL){
		$shipment_id = Yii::$app->request->post('shipment_id');
		$user_id = Yii::$app->request->post('user_id');
		$action = Yii::$app->request->post('action');
        $model = Payments::findOne(['shipment_id' => new \MongoId($shipment_id)]);
		if(empty($model)){
			$model = new Payments();
			$model->payment_type = 0; // 0 Partial Payment, 1 Full Payment
			$model->outstanding  = (double) number_format( (float)Yii::$app->request->post('payment')['total'] - Yii::$app->request->post('payment')['amount'], 2, '.', '');
			if(Yii::$app->request->post('payment')['amount'] >= Yii::$app->request->post('payment')['total']){
				$model->payment_type = 1;
				$model->status = 0;
			} 
		}else{
			if(Yii::$app->request->post('payment')['amount'] >= $model->outstanding){
				$model->payment_type = 1;
				$model->status = 0;
			}
			$model->outstanding  = (double) number_format( (float)$model->outstanding - Yii::$app->request->post('payment')['amount'], 2, '.', '' );
		}
		$model->load(['Payments' => Yii::$app->request->post('payment')]);
		$model->shipment_id = new \MongoId($shipment_id);
		$model->payer_id = new \MongoId($user_id);
		if(!empty($model)){
			$results = $model->transactions;
			$results[] = [
				'id' => md5($user_id. md5(time())),
				'instalment' => empty(Yii::$app->request->post('payment')['part']) ? '' : (int)Yii::$app->request->post('payment')['part'],
				'amount' => empty(Yii::$app->request->post('payment')['amount']) ? '' : (double)Yii::$app->request->post('payment')['amount'],
				'mode' => empty(Yii::$app->request->post('payment')['mode']) ? '' : (int)Yii::$app->request->post('payment')['mode'],
				'remarks' => empty(Yii::$app->request->post('payment')['remarks']) ? '' : Yii::$app->request->post('payment')['remarks'],
				'status' => 0, // 0 pending, 1 Success 
			];
		}
		$model->transactions = $results;
		$model->total = (double)Yii::$app->request->post('payment')['total'];
		$model->save(false);
		if($action == 'save_payment'){
			$transaction = new Transactions();
			$transaction->payment_id = new \MongoId($model->_id);
			$transaction->amount = (double)Yii::$app->request->post('payment')['amount'];
			$transaction->user_id = new \MongoId($user_id);
			$transaction->status = ['code' => '1', 'text' => 'SUCCESS' ]; // will be changed later on
			$transaction->ttype = ['code' => '1', 'text' => 'Spent By Shipper'];
			$transaction->debug = '';
			$transaction->save(false);			
		}
		return true;
    }
	
	/*
	*	Function to hit the Citrus payment Gateway API
	*	URL: http://cargowala.com/dev/frontend/web/index.php?r=shipper/user/process-payment
	*	Params : txn_id
	*/
	public function actionProcessPayment(){
		$access_key = "6Y5LPDN3TVJ99YSJDDKN"; //put your own access_key - found in admin panel     
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76"; //put your own secret_key - found in admin panel     
		// $return_url = 'http://'.$_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		$return_url = 'http://'.$_SERVER['HTTP_HOST'].'/dev/frontend/web/index.php?r=shipper/user/citrus-response'; // need to change url in case of accept bid
		$vanity_url = "mmes2u0c7r";
		$txn_id = empty($_REQUEST['txn_id']) ? time() . rand(10000,99999) : $_REQUEST['txn_id'];
		$value = isset($_REQUEST["amount"]) ? $_REQUEST["amount"] : ''; //Charge amount is in INR by default
		$data_string = "merchantAccessKey=" .$access_key. "&transactionId=" .$txn_id. "&amount=" . $value;
		$signature = hash_hmac('sha1', $data_string, $secret_key);
		$amount = array('value' => $value, 'currency' => 'INR');
		$bill = array(
			'merchantTxnId' => $txn_id,
			'amount' => $amount,
			'requestSignature' => $signature,
			'merchantAccessKey' => $access_key,
			'returnUrl' => $return_url,
			'vanityUrl' => $vanity_url,
		);
		echo json_encode($bill);
	}
	
	// URL :  http://cargowala.com/dev/frontend/web/index.php?r=shipper/user/citrus-response
	public function actionCitrusResponse(){
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76";
		$data = array();
		$json_object = array();
		foreach ($_REQUEST as $name => $value) {
			$data[$name] = $value;                   
		}
		$verification_data =  $data['TxId']. $data['TxStatus']. $data['amount']. $data['pgTxnNo']. $data['issuerRefNo']. $data['authIdCode']. $data['firstName']. $data['lastName']. $data['pgRespCode']. $data['addressZip'];
		$signature = hash_hmac('sha1', $verification_data, $secret_key);
		if ($signature == $data['signature']){
			$transaction = Transactions::findOne(['debug.transaction_id' => (int)$data['TxId']]);
			if(!empty($transaction)){
				$transaction->status = ['code' => '0', 'text' => 'FAIL' ];
				$transaction->debug = $_REQUEST;
				$transaction->save(false);
				if( $data['TxStatus'] == 'SUCCESS'){
					$transaction->status = ['code' => '1', 'text' => 'SUCCESS' ];
					$model = Payments::findOne(['_id' => new \MongoId($transaction->payment_id)]);
					if(!empty($model)){
						$shipment_id = $model->shipment_id->__toString();
						$payer_id = $model->payer_id->__toString();
						$model->status = 1;
						$trans = [];
						foreach($model->transactions as $trkey => $trval){
							if($trval['id'] == (int)$data['TxId']){
								$trval['status'] = 1;
								$model->save(false);
							}
							$trans[] = $trval;
							if($trval['status']  == 0) $model->status = 0;
						}
						$model->transactions = $trans;
						$model->save(false);
						$transaction->save(false);
						// Call to notifications function
						$this->sendShipmentNotifications($payer_id, $shipment_id, $data['amount'], $data['pgTxnNo'], $data['paymentMode'], $data['currency']);
					}
				}
			}
			$json_object = json_encode($data);
		}else {
			$response_data = array("Error" => "Transaction Failed", "Reason" => "Signature Verification Failed");
			$json_object = json_encode($response_data);
		}
		return $this->render('/payment_citrus', [
			'responseData' => $_REQUEST,
		]);		
		echo '<pre>Data '; print_r($data); die('citrusPay');
		Common::send(Common::HTTP_SUCCESS, $json_object);
	}

	// URL :  http://cargowala.com/dev/frontend/web/index.php?r=shipper/user/trans-test
	public function actionTransTest(){
		// $id = new \MongoId($transaction->payment_id);
		$tx_id = "147740418866093";
		$id = new \MongoId("580f661ce1ec610b778b46e5");
		$model = Payments::findOne(['_id' => $id,'transactions.id'=>$tx_id]);
		if(!empty($model)){
			echo '<pre>Model'; print_r($model->transactions);
			$shipment_id = $model->shipment_id->__toString();
			$payer_id = $model->payer_id->__toString();
			$model->status = 1;
			$trans = [];
			foreach($model->transactions as $trkey => $trval){
				if($trval['id'] == (int)$tx_id){
					$trval['status'] = 1;
					$model->save(false);
				}
				$trans[] = $trval;
				if($trval['status']  == 0) $model->status = 0;
			}
			$model->transactions = $trans;
			$model->save(false);
			// Call to notifications function
			$this->sendShipmentNotifications($payer_id, $shipment_id, $data['amount'], $data['pgTxnNo'], $data['paymentMode'], $data['currency']);
		}
	}
	
	/**
    * Function to Save Payments option to the particular shipment
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fsave-transactions
	* params :  _id, security_token, shipment_id, bid_id,  action = 'cash_onloading/save_payment', total_amount, payments, service_for = 'accept-bid'
    * @return boolean|yii\web\Response
    *
    */	
	public function actionSaveTransactions($shipment_id = NULL, $user_id = NULL, $action = NULL, $payments = NULL, $total_amount = NULL, $service_for = NULL, $bid_id = NULL){
		$service_for = empty(Yii::$app->request->post('service_for')) ? $service_for : Yii::$app->request->post('service_for');
		if($service_for == 'accept-bid') $userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$shipment_id = empty(Yii::$app->request->post('shipment_id')) ? $shipment_id : Yii::$app->request->post('shipment_id');
		$user_id = empty(Yii::$app->request->post('_id')) ? $user_id : Yii::$app->request->post('_id');
		$action = empty(Yii::$app->request->post('action')) ? $action : Yii::$app->request->post('action');
		$payments = empty(Yii::$app->request->post('payments')) ? $payments : Yii::$app->request->post('payments');
		$total_amount = empty(Yii::$app->request->post('total_amount')) ? $total_amount : Yii::$app->request->post('total_amount');
		$bid_id = empty(Yii::$app->request->post('bid_id')) ? $bid_id : Yii::$app->request->post('bid_id');
        $model = Payments::findOne(['shipment_id' => new \MongoId($shipment_id)]);
		if(empty($model)){$model = new Payments();}
		$model->payment_type = 1;
		$model->status  = $model->outstanding  = 0;
		$model->total = (double)$total_amount;
		$model->shipment_id = new \MongoId($shipment_id);
		$model->payer_id = new \MongoId($user_id);
		if(!empty($payments)){
			$transaction_id = '';
			foreach(json_decode($payments) as $key => $val){
				foreach($val as $ky => $vl){
					$vl->id = time().rand(10000,99999);
					if(!empty($vl->instalment)) $vl->instalment = (int)$vl->instalment;
					if(!empty($vl->amount)) $vl->amount = (double)$vl->amount;
					if(!empty($vl->mode)) $vl->mode = (int)$vl->mode;
					if(!empty($vl->remarks)) $vl->remarks;
					$vl->status = 0;
				}
				$model->transactions = $val; $model->save(false);
			}
			$model->save(false);
			if($action == 'save_payment'){
				$transaction = new Transactions();
				foreach($model->transactions as $trkey => $trval){
					if($trval->mode == 2){
						$transaction->payment_id = new \MongoId($model->_id);
						$transaction->amount = (double)$trval->amount;
						$transaction->user_id = new \MongoId($user_id);
						$transaction->status = ['code' => '0', 'text' => 'PENDING' ]; // will be changed later on
						$transaction->ttype = ['code' => '1', 'text' => 'Spent By Shipper'];
						$transaction->debug = ['transaction_id' => (int)$trval->id];
						$transaction->save(false);
						$transaction_id = $model->transactions[0]->id;
					}
				}
			}
			if($service_for == 'accept-bid') $this->actionAcceptBid($shipment_id, $bid_id, $userModel);
			return $transaction_id;
		}
    }

    /**
    * Function to show the list of active, available & not assigned drivers corresponding to the trucker
    *
    * @Required params:  _id, security_token, starts_from, limit
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fpayment-test
    * @return boolean|yii\web\Response
    *
    */
	public function actionPaymentTest(){
		
		$content = "some text here Add this to the file\n some text here Add this to the file\n some text here Add this to the file\n some text here Add this to the file\n ";
		// chmod ($_SERVER['DOCUMENT_ROOT'] . "/dev/newfile.txt", 0777);
		$fp = fopen($_SERVER['DOCUMENT_ROOT'] . "/dev/newfile.txt","wb");
		fwrite($fp,$content);
		fclose($fp);
		die('as');
		// $myfile = fopen('newfile.txt', "w") or die("Unable to open file!");
		
		$filename = 'newfile.txt';
		$somecontent = "Add this to the file\n";

		// Let's make sure the file exists and is writable first.
		// if (is_writable($filename)) {

			// In our example we're opening $filename in append mode.
			// The file pointer is at the bottom of the file hence
			// that's where $somecontent will go when we fwrite() it.
			if (!$handle = fopen($filename, 'a')) {
				 echo "Cannot open file ($filename)";
				 exit;
			}

			// Write $somecontent to our opened file.
			if (fwrite($handle, $somecontent) === FALSE) {
				echo "Cannot write to file ($filename)";
				exit;
			}

			echo "Success, wrote ($somecontent) to file ($filename)";

			fclose($handle);

/* } else {
    echo "The file $filename is not writable";
} */
		
		$access_key = "6Y5LPDN3TVJ99YSJDDKN"; //put your own access_key - found in admin panel     
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76"; //put your own secret_key - found in admin panel     
		$return_url = 'http://'.$_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		$notify_url = 'http://'.$_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		$vanity_url = "mmes2u0c7r";
		$txn_id = time() . rand(10000,99999);
		$value = isset($_REQUEST["amount"]) ? $_REQUEST["amount"] : '5.00'; //Charge amount is in INR by default
		$data_string = "merchantAccessKey=" .$access_key. "&transactionId=" .$txn_id. "&amount=" . $value;
		$signature = hash_hmac('sha1', $data_string, $secret_key);
		$amount = array('value' => $value, 'currency' => 'INR');
		$bill = array(
			'merchantTxnId' => $txn_id,
			'amount' => $amount,
			'requestSignature' => $signature,
			'merchantAccessKey' => $access_key,
			'returnUrl' => $return_url,
			'notifyUrl' => $notify_url,
			'vanityUrl' => $vanity_url,
		);
		echo json_encode($bill);
		// return $this->render('/payment_citrus');
	}	
	
	// Function to delete all Media from the specific folder
	// http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fdelete-media
	public function actionDeleteMedia(){
		// $model = new Bookings();
		// $path = CommonModel::mkdir($model::PARENTDIR,0755,true);
		$path = CommonModel::mkdir('/tmp/',0755,true);
		CommonModel::DelAllMedia($path); return;
        $model->delete() and Common::send(Common::HTTP_SUCCESS);
    }
	
	/**
    * Function to Assign Drivers to the particular Shipments
    *
    * @Required params:  _id, security_token, shipment_id
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fview-assigned-drivers
    * @return boolean|yii\web\Response
    *
    */
	public function actionViewAssignedDrivers(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$model = Bookings::find()->where([ '_id' => new \MongoId(Yii::$app->request->post('shipment_id')) ])->with('drivers')->one();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		// for($i = 1; $i <= $model->shipment_detail['truck_quantity']; $i++){
			// $consignee [] = [
				// 'srno' => $i,
				// 'vehicle_num' => $i,
				// 'driver_name' => $i,
				// 'driver_num' => $i,
			// ];
		// } 
		echo '<pre>'; print_r($model); die('hre');
		// $model->vehicle = $vehicle;
		// $model->save(false);
		Common::send(Common::HTTP_SUCCESS, $consignee); 
    }
	
	private function checkShipments(){
		$model = Bookings::find()->where([ 'status' =>[0,1,2,6,7,8] ])->andWhere(['<=', 'transit.t_expdatetime', time()])->orderBy('transit.t_expdatetime')->all();
		if(empty($model)) return true;
		$assignCounter = 0;
		foreach($model as $key => $data){
			$statusArr = [1,2]; // Cancelled by Trucker / Shipper
			if(in_array($data->status, $statusArr)){
				if(!empty($data->vehicle)){
					foreach($data->vehicle as  $val){
						if(!empty($val['id'])){
							$vehicleModel = Vehicles::findOne(['_id' => new \MongoId($val['id'])]);
							if(!empty($vehicleModel) && $vehicleModel->truck_status){
								$vehicleModel->load(['Vehicles' => $vehicleModel->oldAttributes]);										
								$vehicleModel->truck_status = 0; // Un-Assign
								$vehicleModel->save(false);
							}
						}
						if(!empty($val['driver'])){
							$driverModel = Drivers::findOne(['_id' => new \MongoId($val['driver'])]);
							if(!empty($driverModel) && $driverModel->driver_status){
								$driverModel->load(['Drivers' => $driverModel->oldAttributes]);								
								$driverModel->driver_status = 0; // Un-Assign
								$driverModel->save(false);
							}
						}
					}
				}
			}
			$statArr = [0,8]; //Pending, Incompleted or Not-Confirmed Shipments
			if(in_array($data->status, $statArr)){
				$data->load(['Bookings' => $data->oldAttributes]);
				$data->status = 3; $data->save(false);
			}
			if($data->status == 6 || $data->status == 7){ // Pending / Accepted / Incomplete shipments
				if(!empty($data->vehicle)){
					foreach($data->vehicle as  $val){
						if( !empty($val['id']) && !empty($val['driver']) )	$assignCounter++;
					}
				}
				if($data->shipment_detail['truck_quantity'] != $assignCounter){
					if(!empty($data->vehicle)){
						foreach($data->vehicle as  $val){
							if(!empty($val['id'])){
								$vehicleModel = Vehicles::findOne(['_id' => new \MongoId($val['id'])]);
								if(!empty($vehicleModel) && $vehicleModel->truck_status){
									$vehicleModel->load(['Vehicles' => $vehicleModel->oldAttributes]);										
									$vehicleModel->truck_status = 0; // Un-Assign
									$vehicleModel->save(false);
								}
							}
							if(!empty($val['driver'])){
								$driverModel = Drivers::findOne(['_id' => new \MongoId($val['driver'])]);
								if(!empty($driverModel) && $driverModel->driver_status){
									$driverModel->load(['Drivers' => $driverModel->oldAttributes]);								
									$driverModel->driver_status = 0; // Un-Assign
									$driverModel->save(false);
								}
							}
						}
					}
					$data->load(['Bookings' => $data->oldAttributes]);
					$data->status = 3; $data->save(false);					
				}
			}
		}
	}

	/**
    * Function to Get User Profile Detail
    *
    * @Required params : _id, security_token, shipment_id
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fcalculate-commision
    * @return boolean|yii\web\Response
    *
    */    
	private function calculateCommision($shipment_id, $data){
		$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
  		$package = $this->getTruckerMembershipInfo($data); // fetch trucker's current Membership Package
		if($package['is_expired'] == 'false'){
			if($package['transactions'] > 0){
				$amount = 00.00;
			}else{
				$model = Bookings::findOne([ '_id' => new \MongoId($shipment_id) ]);
				empty($model) and Common::send(Common::HTTP_NO_CONTENT);
				foreach($adminsettings->commision as $com){
					$amount = $com['rate'];
					if($com['distance'] > $model->transit['est_distance']){
						$amount = $com['rate']; break;
					}
				}
			}
		}
		return $amount;
	}

	private function getTruckerMembershipInfo($model){
		if($model->membership['plan_type'] != "Free"){
			$mem_data = Membership::findOne([ '_id' => $model->membership['id']]);
			$package = [
				'membership' => 'Paid',
				'plan_code' => $mem_data['mtype']['code'],
				'name' => $mem_data['mtype']['text'],
				'transactions' => $model->membership['transactions'],
				'expiry_date' => date('d M, Y', $model->membership['expiry_date']),
				'timeStamp' => $model->membership['expiry_date'],
				'is_expired' => (time() > $model->membership['expiry_date']) ? 'true' : 'false',
			];
		}else{
			$package = [
				'membership' => 'Free',
				'plan_code' => 0,
				'name' => 'Free',
				'transactions' => $model->membership['transactions'],
				'expiry_date' => '',
				'timeStamp' => '',
				'is_expired' => 'false',
			];			
		}
		return $package;
	}
	
    /**
    * Function to load FAQ Section Topics Listing
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fget-faq-topics
	* Params : _id, security_token
    * @return boolean|yii\web\Response
    *
    */	
	public function actionGetFaqTopics(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$offset = empty(Yii::$app->request->post('next_records')) ? 0 : Yii::$app->request->post('next_records');
		$limit = empty(Yii::$app->request->post('limit')) ? 4 : Yii::$app->request->post('limit');
		date_default_timezone_set('Asia/Kolkata');
		$model = Topics::find()->where(['status' => [1]])->all();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		foreach($model as  $key=>$val){
			$en_title =  $val->title['en'];
			$en_desc =  $val->description['en'];
			$hi_title =  $val->title['hi'];
			$hi_desc =  $val->description['hi'];
			$topics [] = [
				'_id' => $val->_id->__toString(),
				'english_title' => $en_title,
				'english_desc' => $en_desc,
				'hindi_title' => $hi_title,
				'hindi_desc' => $hi_desc,
			];
		}
		Common::send(Common::HTTP_SUCCESS, $topics);
	}	
	
    /**
    * Function to load FAQ Section based on Topics
    *
	* Url : http://cargowala.com/dev/frontend/web/index.php?r=shipper%2Fuser%2Fget-faq-list
	* Params : _id, security_token, topic_id
    * @return boolean|yii\web\Response
    *
    */	
	public function actionGetFaqList(){
		$userModel = $this->authenticateUser(); // To Authenticate user on the bases of _id and security token;
		$offset = empty(Yii::$app->request->post('next_records')) ? 0 : Yii::$app->request->post('next_records');
		$limit = empty(Yii::$app->request->post('limit')) ? 4 : Yii::$app->request->post('limit');
		date_default_timezone_set('Asia/Kolkata');
		$model = Faq::find()->where(['topic' => new \MongoId(Yii::$app->request->post('topic_id')), 'status' => [1]])->all();
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		foreach($model as  $key=>$val){
			$en_question =  $val->question['en'];
			$en_answer =  $val->answer['en'];
			$hi_question =  $val->question['hi'];
			$hi_answer =  $val->answer['hi'];
			$faqs [] = [
				'_id' => $val->_id->__toString(),
				'english_question' => $en_question,
				'english_answer' => $en_answer,
				'hindi_question' => $hi_question,
				'hindi_answer' => $hi_answer,
			];
		}
		Common::send(Common::HTTP_SUCCESS, $faqs);
	}	
	
}