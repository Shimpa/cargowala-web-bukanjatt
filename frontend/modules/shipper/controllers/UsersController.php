<?php
namespace frontend\modules\shipper\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;
use common\models\Users;
use frontend\modules\shipper\models\Common;
use common\models\Common as Common1;

use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

class UsersController extends Controller{

    /**
    * @inheritdoc
    */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['register', 'login', 'create-profile', 'forgot-password', 'update-password',  'resend-email-token', 'get-otp', 'validate-otp'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout'              => ['post'],
                    'login'               => ['post'],
                    'register'            => ['post'],
                    'create-profile'      => ['post'],
                    'forgot-password'     => ['post'],
                    'update-password'     => ['post'],
                    'get-otp'             => ['post'],
                    'validate-otp'        => ['post'],
                ],
            ],
        ];
    }
    
	public function beforeAction($action) {
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
    }
    
    public function actionIndex(){
        die("@Access Denied");
    }
    
    public function actions() {
      return [
        'auth' => [
          'class' => 'yii\authclient\AuthAction',
          'successCallback' => [$this, 'oAuthSuccess'],
        ],
      ];
    }

    public function actionLogin(){
        (empty(Yii::$app->request->post('username')) || empty(Yii::$app->request->post('password'))) and
		 Common::send(Common::HTTP_BAD_REQUEST);	
		$model = Users::findOne(['contact.email'=>Yii::$app->request->post('username')]);
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$isValidPassword  = $model->password ===md5(Yii::$app->request->post('password'));
		$isActive         = !$model->status ==0;
		switch(true){
			case (empty(Yii::$app->request->post('social')) && !$isValidPassword && ($model->role !==2)):
				Common::send(Common::HTTP_BAD_REQUEST); 
			break;
			case (empty(Yii::$app->request->post('social')) && !$isActive && !empty($model->hash_token)):
				Common::send(Common::CUSTOM_MAILNOTVERIFIED);
			break;
			case ((empty(Yii::$app->request->post('social')) && !$isActive) || (!empty(Yii::$app->request->post('social')) && !$isActive)):
				Common::send(Common::CUSTOM_INACTIVE);
			break;
			case ((!empty($model) && $isValidPassword && $isActive) || (!empty($model) && !empty(Yii::$app->request->post('social')))):
				Common::send(Common::HTTP_SUCCESS,$this->getProfile($model));
			break;	   
			default:
			 $this->socialRegister();	
			break;	
		}
    }

	private function socialRegister(){
		in_array(Yii::$app->request->post('social'),['facebook','google']) or                                                        Common::send(Common::HTTP_BAD_REQUEST);
		$model =  new Users();
		$model->scenario = Users::SCENARIO_SHIPPER_APICREATE;
		$model->load(['Users'=>[
			'email'    =>Yii::$app->request->post('username'),
			'status'   =>1,
			'role'     =>2
		]]);
	    $model->validate(); print_r($model->getErrors());die;
	}

	/* register user */
	public function actionRegister(){
		(empty(Yii::$app->request->post('username')) || empty(Yii::$app->request->post('password'))) and 
		 Common::send(Common::HTTP_BAD_REQUEST);
		empty(Yii::$app->request->post('mobile_tokens')) and Common::send(Common::HTTP_UNPROCESSABLE);
		$model  = new Users;
		$model->scenario = Users::SCENARIO_SHIPPER_APISOCIALREG;
		$model->load(['Users'=>[
			'email'        =>Yii::$app->request->post('username'),
			'status'       =>0,
			'role'         =>2,
			'hash_token'   =>md5(mt_rand(time(),time())+Yii::$app->request->post('username')).":".time(),
		]]);
		$model->validate() or Common::send(Common::HTTP_CONFLICT);
		$model->save(false) && $model->sendVerificationMail();
		Common::send(Common::HTTP_SUCCESS,$this->getProfile($model));		
	}

	public function actionCreateProfile(){
		$model  = Users::findOne(['_id'=>Yii::$app->request->post('_id')]);
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$model->scenario = Users::SCENARIO_SHIPPER_APICREATEPROFILE;
		$posted = Yii::$app->request->post();
		unset($posted['password']);
		unset($posted['status']);
		unset($posted['hash_token']);
		unset($posted['mobile_tokens']);
		unset($posted['creadted_on']);
		unset($posted['modified_on']);
		unset($posted['email']);
		$posted['mobile_number'] = Yii::$app->request->post('mobile_no');
		$model->load(['Users'=>$posted]);
		$model->validate() or  Common::send(Common::HTTP_BAD_REQUEST);
		$this->uploadImage($model);
		$model->save(false) and Common::send(Common::HTTP_SUCCESS,$this->getProfile($model)); 
	}
    
    /**
    * Function to Forgot Password
    *
    * @Required params email
    * @return boolean|yii\web\Response
    *
    */    
	public function actionForgotPassword(){
		$model  = Users::findOne(['contact.email'=>Yii::$app->request->post('email')]);
        $has_token = md5(mt_rand(time(),time())+Yii::$app->request->post('email')).":".time();
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        $model->password = '';
        $model->hash_token = $has_token;
        $model->save(false) and $model->resetPasswordEmail($model);        
        Common::send(Common::HTTP_SUCCESS);
	}
    
    /**
    * Function to Update/Change Password
    *
    * @Required params _id, oldpassword, newpassword
    * @return boolean|yii\web\Response
    *
    */     
	public function actionUpdatePassword(){
        empty(Yii::$app->request->post('newpassword')) and Common::send(Common::HTTP_BAD_REQUEST);
		$model  = Users::findOne(['_id' => Yii::$app->request->post('_id'), 'password' => md5(Yii::$app->request->post('oldpassword'))]);
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        $model->password = md5(Yii::$app->request->post('newpassword'));
        $model->save(false) and Common::send(Common::HTTP_SUCCESS);
	}
    
   /**
    * Function to Resend Email Token
    *
    * @Required params _id, oldpassword, newpassword
    * @return boolean|yii\web\Response
    *
    */     
	public function actionResendEmailToken(){
        empty(Yii::$app->request->post('email')) and Common::send(Common::HTTP_BAD_REQUEST);
        $has_token = md5(mt_rand(time(),time())+Yii::$app->request->post('email')).":".time();
		$model  = Users::findOne(['contact.email' => Yii::$app->request->post('email')]);
        empty($model) and Common::send(Common::HTTP_NO_CONTENT);
        if($model->status == 'Active') Common::send(Common::CUSTOM_INACTIVE);
        $model->hash_token = $has_token;
        $model->save(false) and $model->resetPasswordTokenEmail($model);        
        Common::send(Common::HTTP_SUCCESS);        
	}
    
	public function actionGetOtp(){
		$model  = Users::findOne(['contact.mobile_number'=>Yii::$app->request->post('mobile_no')]);
		empty($model) or Common::send(Common::HTTP_CONFLICT);
		$model  = Users::findOne(['_id'=>Yii::$app->request->post('_id')]);
		empty($model) and Common::send(Common::HTTP_BAD_REQUEST);
		$model->scenario = Users::SCENARIO_SHIPPER_APIMOBILEOTP;
		$model->mobile_number = Yii::$app->request->post('mobile_no');
		$model->validate() or Common::send(Common::HTTP_BAD_REQUEST);
		$model->mobile_number  = null;
		$otp   = abs(ceil(mt_rand((time()*2),time()*3)));
		$otp   = substr($otp,0,6);
		$mno   = substr(Yii::$app->request->post('mobile_no'),-4,10);
		$model->otp = [
			'otp_type' => 'create_profile',
			'otp_no'   => $otp,
			'validity' => time()
		];
		$model->save(false);
		$msg  = "One Time Password for Cargowala Registration";
		$msg .= " on XXX XXX " . $mno;
		$msg .= " is " . $otp . " This can be used only once and is valid for next 4 hours from " . date('Y-m-d');
	    $pth  = "http://136.243.19.2/http-api.php?username=" . urlencode(Yii::$app->params['smsusername']);
	    $pth .= "&password=" . urlencode(Yii::$app->params['smspassword']);
	    $pth .= "&senderid=" . urlencode(Yii::$app->params['smssenderid']);
	    $pth .= "&route=1&number=" . urlencode($mno);
	    $pth .= "&message=" . urlencode($msg);
		file_get_contents($pth);
		Common::send(Common::HTTP_SUCCESS);
	}

	public function actionValidateOtp(){
		preg_match('/^[0-9]{6,6}$/',Yii::$app->request->post('otp')) or Common::send(Common::HTTP_BAD_REQUEST);
		$model  = Users::findOne(['_id'=>Yii::$app->request->post('_id'),'otp.otp_no'=>Yii::$app->request->post('otp')]);
		empty($model) and Common::send(Common::HTTP_NO_CONTENT);
		$validTill = time()*60*60*4;
		if($model->otp['validity']<=$validTill){
		   $model->otp = [
			   'otp_type' =>'' ,
			'otp_no'   => '',
			'validity' => ''
		   ];
		   $model->save(false) and Common::send(Common::HTTP_SUCCESS);	
		}else
 		   Common::send(Common::HTTP_UNPROCESSABLE);	
	}
                  
	private function getProfile($user){ 
		return [
		      '_id'       => $user->_id->__toString(),
		      'firstname' => empty($user->name['firstname'])?'':$user->name['firstname'],
			  'lastname'  => empty($user->name['lastname'])?'':$user->name['lastname'],
			  'email'     => empty($user->contact['email'])?'':$user->contact['email'],
			  'mobile_no' => empty($user->contact['mobile_number'])?'':$user->contact['mobile_number'],
			  'image'     => empty($user->image)?'':Common::ImagePath($user,'image'),	
		];	
	}
                  
	private function uploadImage($model){
		$path = Common1::mkdir($model::PARENTDIR . $model::CHILDDIR . "-" . $model->_id, 0755, true);
		if(Yii::$app->request->post('image_url')){
		   $image = file_get_contents(Yii::$app->request->post('image_url'));
		   $image_name = basename(Yii::$app->request->post('image_url'));
		   $ext   = array_pop(@explode(".",$image_name)); 	
		   file_put_contents($path . DS . $image_name, $image);
		   $image_name = 'image_' . time() . '.jpg';
		   $model->image = empty($model->image)?$image_name:basename($model->image);
		}elseif(Yii::$app->request->post('image_base64')){
            $image = str_replace('data:image/png;base64,', '', Yii::$app->request->post('image_base64'));
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $image_name = 'image_' . time() . '.jpg';
		    file_put_contents($path . DS . $image_name, $image);		
            $model->image = empty($model->image)?$image_name:basename($model->image);
	    }
    }
    
}