<?php
namespace frontend\modules\shipper\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use common\models\Users;
use common\models\Common;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

class DefaultController extends Controller{

    /**
    * @inheritdoc
    */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'update', 'dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex(){
        return $this->redirect(['login']);
    }
    
    public function actions() {
      return [
        'auth' => [
          'class' => 'yii\authclient\AuthAction',
          'successCallback' => [$this, 'oAuthSuccess'],
        ],
      ];
    }

    
    public function actionLogin(){
		$this->layout = 'login';
        if (!\Yii::$app->user->isGuest){
            return $this->render('index');
        }
        $model = new LoginForm();
        $model->isAdmin = false;
        if($model->load(Yii::$app->request->post()) && $model->login()){
			return $this->redirect(['dashboard']);
        }else{
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    /**
    * This function will be triggered when user is successfuly authenticated using some oAuth client.
    *
    * @param yii\authclient\ClientInterface $client
    * @return boolean|yii\web\Response
    */
    public function oAuthSuccess($client) {
        // get user data from client
        $userAttributes = $client->getUserAttributes();
        // do some thing with user data. for example with $userAttributes['email']
        $this->authenticateUser($userAttributes);
    }
    
    public function authenticateUser($userData){
        $model = new LoginForm();
        $res = Users::find()->where(['contact.email' => $userData['email']])->one();
        if(!empty($res->attributes['_id'])){
            if($model->socialLogin($userData['email'])){
                if(empty($res->attributes['contact']['mobile_number'])){
                    return $this->redirect(['update', 'id' => (string)$res->attributes['_id']]);
                }
                return $this->redirect(['dashboard']);
            }
        }else{
            $model = new Users();
            $model->hash_token = empty($userData['email']) ? NULL : md5($userData['email'].time());
			$model->role = 2; // for Shipper
            $model->firstname = $userData['first_name'];
            $model->lastname = $userData['last_name'];
            $model->email = $userData['email'];
            $model->status = 1;
			$model->save(false);
            $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
			Yii::$app->session['shipper_id']  = $model->_id->__toString();
            $fid = $userData['id'];
            $img = file_get_contents('https://graph.facebook.com/'.$fid.'/picture?type=large');
            $filename  = $path.'/'.time().'.jpg';
            file_put_contents($filename, $img);
            $model->image = basename($filename);
            $model->save(false);
			return $this->redirect(['business-address']);
        }
    }
    
    public function actionDashboard(){
        // echo '<pre>'; print_r($this->goHome()); die('Home URL');
        return $this->render('index');
    }
    
    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->redirect(['login']);
    }

    /**
    * Updates an existing Trucks model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    /**
    * Finds the Trucks model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Trucks the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout(){
        return $this->render('about');
    }    
    
    /**
    * Signs user up.
    *
    * @return mixed
    */
    public function actionSignup(){
        $model = new Users();
		$model->scenario  = Users::SCENARIO_TRUCKER_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->hash_token = empty($model->email) ? NULL : md5($model->email.time());
			$model->role = 2; // for Shipper
			$model->save() && $model->sendVerificationMail();
			$this->uploadImage($model,'image');
			Yii::$app->session['shipper_id']  = $model->_id->__toString();
			return $this->redirect(['business-address']);
        }else{ 
            return $this->render('signup', [
                'model' => $model,
            ]);	
		}
    }
    
	public function actionBusinessAddress(){
		if(empty(Yii::$app->session['shipper_id']))
		   return $this->redirect(['signup']);	
		$model = Users::findOne(['_id'=>Yii::$app->session['shipper_id']]);
		$model->scenario  = Users::SCENARIO_TRUCKER_ADDRESS;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->image = basename($model->image);
			unset($model->password);
			$model->save() && $this->uploadImage($model,'business_logo');
			Yii::$app->session->setFlash('email_confirm','Email Verification  link sent on registed email. Please confirm your email before login.');
			return $this->redirect(['login']);
        }else{
            return $this->render('business-address', [
                'model' => $model,
            ]);	
		}
	}
    
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $key){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $key);
        if(!empty($image)){                    
            if($key == "business_logo"){
			   (empty($model->oldAttributes['business'][$key]) && $filename = time().".".$image->extension) || $filename = $model->oldAttributes['business'][$key];
			}else{
			   (empty($model->oldAttributes[$key]) && $filename = time().".".$image->extension) || $filename = $model->oldAttributes[$key];				
			}
            $model->$key  = $filename;
            $image->saveAs($path.DS.$model->$key);
            unset($model->password);
            $model->save(false);
	    }
    }
    
	/* get indian states list ..*/
	public function actionStatesList(){
		$states = States::find()->select(['_id','name'])->where(['status'=>1])->all();
		$states = array_map(create_function('$ar',' return ["_id"=>$ar->_id->__toString(),"name"=>$ar->name];'),$states);
		Common::encodeJSON($states,false);
	}
	
    /**
    * Requests password reset.
    *
    * @return mixed
    */
    public function actionRequestPasswordReset(){
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
    * Resets password.
    *
    * @param string $token
    * @return mixed
    * @throws BadRequestHttpException
    */
    public function actionResetPassword($token){
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	/* validate user by email check user email is active or fake*/
	public function actionValidateEmail(){
		$token = yii::$app->request->get('token');
        $model = Users::find()->where(['hash_token' => (string) $token])->one();
		$model->image = basename($model->image);
        unset($model->password);
        if (!empty($model)) {
            $model->hash_token = '';
            $model->status = 1;
            if ($model->save(false)) {
                echo '<h2>You have successfully confirmed your mail.</h2>';
            }
        } else {
            echo '<h2>You have already confirmed your email or invalid token.</h2>';
        }
        die;
	}    
    
}