<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shipper';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-4 col-xs-12 col-sm-5 pull-right text-center pR0 mT20 " >
    <div class="bg-primary pT10 pL10 pB10 bdrr">
        <div class="row">
            <div class="col-md-4 col-xs-12 pL20">
                <img src="<?=Yii::$app->user->getImage()?>" class="img-circle img-responsive "/>
                <div class="fnt13 mT10 editlink text-left txtc mL15">
                    <a href="<?=Url::to(['/shipper/default/update','id'=>Yii::$app->user->getId()])?>"><div class="edit hidden-xs hidden-sm"></div>Edit profile</a>
                </div>
            </div>  
            <div class="col-md-8 col-xs-12 mTx20">
                <h3 class="mT0">Welcome, <?=Yii::$app->user->getName()?></h3>
                <h4><?=Yii::$app->user->getCompany()?></h4>
                <div class="btn-logout pull-right mT20 fnt13 mR20">
                    <?=Html::beginForm(['/shipper/default/logout'], 'post').Html::submitButton('<div class="logout"></div>Logout</a></div>',['class' => '']).Html::endForm()?>
                </div>
            </div>
        </div>
    </div>
    <br style="clear:both;"/>  
</div>