<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="row">
  <div class="col-md-6 col-md-offset-3 mT30">
   <div class="bg-success pT15 pB10 text-center">
   <h3>Sign in to your account now</h3>
   </div> 
<div class="bg-primary pB5 text-center login">
<form>  
<div class="row">
<div class="col-md-4 col-md-offset-2 mT40 bdrr">
      <a href="#" class="icon-button facebook"><i class="fa fa-facebook"></i><span></span></a>
      <p class="mT10 facebook">Facebook</p>
</div>
<div class="col-md-4 mT40">      
      <a href="#" class="icon-button google-plus"><i class="fa fa-google"></i><span></span></a>
      <p class="mT10 google">Google</p>
</div>
</div>
<div class="row">
  <div class="col-md-8 col-md-offset-2 mT20">
      <p class="or"> or login with your email id </p>
       <span class="input input--hoshi">
          <input class="input__field input__field--hoshi" type="text" id="input-4" />
          <label class="input__label input__label--hoshi input__label--hoshi-color-1" for="input-4">
            <span class="input__label-content input__label-content--hoshi">Enter your email id</span>
          </label>
        </span>
        <span class="input input--hoshi">
          <input class="input__field input__field--hoshi" type="password" id="input-6" />
          <label class="input__label input__label--hoshi input__label--hoshi-color-3" for="input-6">
            <span class="input__label-content input__label-content--hoshi">Enter password</span>
          </label>
        </span>
        <div class="row">
        <div class="col-md-6">
        <div class="checkbox"><label><input type="checkbox"><span> Remember me</span></label></div>
        </div>
        <div class="col-md-6 mT10 text-right pR30">
        <a href="#">Forgot Password ? </a>
        </div>
        </div>
         <div class="row">
      <div class="col-md-12 text-center">
        <p class="text-danger"><span> * </span>This is an error</p>
      </div>
    </div>
        <button type="button" class="btn btn-default hvr-sweep-to-right mT10"> LOGIN </button>
        <div class="row">
          <div class="col-md-12 mT20">
          <p class="mB5">Don't have an account yet? <a href ="#"> Register here</a></p>
           </div>
        </div>    
</div>
</div>
</form>
    </div>  
  </div> 
</div>
</div>