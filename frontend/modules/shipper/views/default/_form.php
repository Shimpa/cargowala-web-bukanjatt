<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['id' => 'form-address','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="col-md-5 col-xs-12 text-center mT40 mB50 pR02">
    <div class="bg-primary p20 pull-left w100 signup2">
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Edit Comapany details</p>
		<div class="row text-left text-primary">
		<div class="col-md-6 "><label>PAN card number</label></div>
		
		<div class="col-md-6 "><label>Service tax number</label></div>
		</div>
		<div class="row"?>
	<?= $form->field($model, 'business_pancard',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Pan No']) ?>	
		<?= $form->field($model, 'business_service_taxno',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Service tax number']) ?>	
			
		</div>	
   <div class="row text-left text-primary">
		<div class="col-md-12 "><label>Company name</label>
		</div>
		</div>
		<div class="row">
	<?= $form->field($model, 'business_name',['options' => ['class' =>''],'template' => '<div class="input-group mB20"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Company name']) ?>
		</div>		
	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Landline number</label>
		</div>
		</div>
		<div class="row">
   <?= $form->field($model, 'landline_number',['options' => ['class' =>''],'template' => '<div class="input-group mB20"><span class="input-group-addon" id="basic-addon1"><img src="images/phone.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Landline No.']) ?>
		</div>
		<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Office address</label>
		</div>
		</div>
	<div class="row">	
    <?= $form->field($model, 'office_address',['options' => ['class' =>''],'template' => '<div class="input-group2 mB20">{input}</div>'])->textArea(['placeholder'=>'Office address']) ?>
	</div>	
    <div class="row text-left text-primary">
		<div class="col-md-12 "><label>Registered address</label>
		</div>
		</div>
	<div class="row">	
	<?= $form->field($model, 'registered_address',['options' => ['class' =>''],'template' => '<div class="input-group2 mB20">{input}</div>'])->textArea(['placeholder'=>'Registered address']) ?>
	</div>
	
		
     <div class="row">
    <div class="form-group mB30 col-md-4 col-sm-4 text-left" id="image-view1"> 
	<label for="users-business_logo">
	<?php if(empty($model->business_logo)):?>
	<img src="images/profile-image.png" class="img-responsive"/>
	<?php else : ?>	
	<img src="<?=Common::onlyMediaPath($model).$model->business_logo?>" class="img-responsive"/>  
	<?php endif; ?>
	</label>
	</div>
    <div class="form-group mB30 col-md-8 col-sm-8">
    <p class="text-left fnt13 text-primary">Select your Company Logo</p>
	<span id="name-show1" ></span> 	
    <?= $form->field($model, 'business_logo',['options' => ['class' =>'hidden']])->fileInput(['autofocus' => true,'class'=>'','image-view'=>'#image-view1 img','onchange'=>"document.querySelector('#name-show1').innerHTML = this.value.split('\\\').pop()"])->label(false) ?>
    </div>
  </div>
   
    </div>  
 </div>

</div>
</div>     


    <div class="col-md-4  text-left pR0 mT40 mB50">
    <div class="bg-primary p20 pull-left w100">
    <div class="row signup2">
    <div class="col-md-12">
    <p class="text-primary mT20">Edit user details</p>
    
	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>First name</label>
		</div>
		</div>
	<div class="row">
    <?= $form->field($model, 'firstname',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'First name']) ?>
	</div>
	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Last name</label>
		</div>
		</div>	
	<div class="row">	
    <?= $form->field($model, 'lastname',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Last name']) ?>
    </div>

	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Password</label>
		</div>
		</div>	
    <div class="row">
    <?= $form->field($model, 'password',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/lock.png"></span>{input}</div>'])->passwordInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Password']) ?>
		</div>
	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Confirm password</label>
		</div>
		</div>	
		<div class="row">
    <?= $form->field($model, 'confirm_password',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/lock.png"></span>{input}</div>'])->passwordInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Confirm password']) ?>
    </div>
		
	
		
    <div class="row">
    <div class="form-group mB30 col-md-4 col-sm-4 text-left" id="image-view">
	<label for="users-image">
	<?php if(empty($model->image)):?>
	<img src="images/profile-image.png" class="img-responsive"/>
	<?php else : ?>	
	<img src="<?=Common::onlyMediaPath($model).$model->image?>" class="img-responsive"/>  
	<?php endif; ?>	
     
	</label>	
    </div>
    <div class="form-group mB30 col-md-8 col-sm-8">
    <p class="text-left fnt13 text-primary">Select your profile picture</p>
	<span id="name-show" ></span>
    <?= $form->field($model, 'image',['options' => ['class' =>'hidden']])->fileInput(['autofocus' => true,'class'=>'','image-view'=>'#image-view img','onchange'=>"$('#name-show').html(this.value.split('\\\').pop())"])->label(false) ?>
    </div>
  </div>
    </div>
    </div>  
</div>
<div class="row">
<div class="col-md-10 col-md-offset-1 text-center">
	<p id="cus-error" ></p>
	
	</div>
</div>		
<div class="text-center">
<?= Html::submitButton('Update', ['class' => 'btn btn-default mL20 mT20', 'name' => 'signup-button']) ?>	
</div>
</div>
  
 
<?php ActiveForm::end(); ?> 
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){    
var msg ={};
$("#form-address").on("afterValidateAttribute", function(event, attribute, messages) {
    event.preventDefault();
	var hasError = messages.length !== 0;
	msg[attribute.name]=messages[0] || false;
    $("#cus-error").html(getError());
    return hasError;
});
function getError(){
   for(var m in msg){
     if(msg[m])
	   return msg[m];
   }
   return null;
}
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>






