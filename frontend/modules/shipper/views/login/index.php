<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ShipperSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shippers');
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [

          [
           'label' => Yii::t('app', 'Create  Shippers'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],       


];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box shipper-index">
        <div class="box-header mtb10">
         
                  <h3 class="box-title"><?= Html::encode($this->title) ?> List</h3>
                 <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Create Shipper'), ['create'], ['class' => 'btn btn-success']) ?>

         </p>
    
                </div>    
                </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'name',
            'contact',
            'otp',
            'password',
            // 'mobile_tokens',
            // 'hash_token',
            // 'image',
            // 'account_type',
            // 'company_type',
            // 'business_type',
            // 'business',
            // 'pancard',
            // 'status',
            // 'role',
            // 'social',
            // 'created_on',
            // 'modified_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>