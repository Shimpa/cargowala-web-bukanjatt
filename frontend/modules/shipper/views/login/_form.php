<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Shipper */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body shipper-form">
  <div class="row">
    <?php $form = ActiveForm::begin(); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'name') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'contact') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'otp') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'password')->passwordInput() ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'mobile_tokens') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'hash_token') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'image') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'account_type') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'company_type') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'business_type') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'business') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'pancard') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'status') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'role') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'social') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'created_on') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'modified_on') ?>

    </div>         
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>    
