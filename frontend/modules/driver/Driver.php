<?php
namespace frontend\modules\driver;

class Driver extends \yii\base\Module{
    public $controllerNamespace = 'frontend\modules\driver\controllers';

    public function init(){
        parent::init();
    }
}