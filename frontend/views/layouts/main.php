<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Metas -->
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,300' rel='stylesheet' type='text/css'>
        <?php $this->head() ?>
    </head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <?php
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Home', 'url' => 'http://www.cargowala.com'];
            //$menuItems[] = ['label' => 'About', 'url' => ['/site/about']];
            if (Yii::$app->controller->module->id == 'shipper'){
                $menuItems[] = ['label' => 'Signup', 'url' => ['default/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['default/login']];
            }else{
                $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];                
            }
        } elseif (Yii::$app->controller->module->id == 'shipper'){
            $menuItems[] = ['label' => 'Dashboard', 'url' => ['default/index']];
            $menuItems[] = ['label' => 'Place Bid', 'url' => ['default/index']];
        } else {
            $menuItems[] = ['label' => 'Dashboard', 'url' => ['/trucker/index']];
            $menuItems[] = ['label' => 'Add Vehicle', 'url' => ['/vehicles/create']];
            $menuItems[] = ['label' => 'Add Driver', 'url' => ['/drivers/create']];
            $menuItems[] = ['label' => 'Logout','linkOptions' => ['data-method' => 'post'], 'url' => ['/site/logout']];
        }
        echo Nav::widget([
            'options' => ['class' => 'nav navbar-nav '],
            'items' => $menuItems,
        ]); ?>	
        <ul class="nav navbar-nav navbar-right list-inline mT5 pR20">
            <li class="follow">Follow Us On : </li>
            <li><a href="#" class="facebook"></a></li>
            <li><a href="#" class="twitter"></a></li>
            <li><a href="#" class="google"></a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 mT20 txtc">
                <img src="images/logo.png" class="img-responsive"/>
            </div> 	
            <?= $content ?>
        </div>
    </div>
    <div class="footer">
        <div class="container-fluid">
            <div class="row mT10">
                <div class="col-md-6 col-sm-6">
                    <p>&copy; Copyright <?= date('Y') ?>. Cargowala. All rights reserved</p>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="list-inline text-right">  
                    <li><a href="#">Terms &amp; Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>	
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>