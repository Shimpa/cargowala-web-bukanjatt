<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */
/* @var $form yii\widgets\ActiveForm */
?>

 <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="col-md-5 text-center pR0 mT20 mB20">
    <div class="bg-primary p20 pull-left w100 mB20">   
   
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please add vehicle details</p>
    <div class="row">
	<?= $form->field($model, 'type',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/truck.png"></span>{input}</div>'])->dropDownList(Common::getTruckCategory(),['class'=>'form-control regtxt','prompt'=>'Select Truck Category']) ?>
	
	<?= $form->field($model, 'type',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/truck.png"></span>{input}</div>'])->dropDownList($model::getTruckList(),['class'=>'form-control regtxt','prompt'=>'Select Truck']) ?>	
		
		

		
	<?= $form->field($model, 'type',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/truck.png"></span>{input}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Truck type']) ?>
	
		
	<?= $form->field($model, 'rc_number',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Truck / RC number']) ?>	


    </div>
    <div class="row">
	<?= $form->field($model, 'permit_expiry_date',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'RC Expiry Date']) ?>		
	
	<?= $form->field($model, 'number',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Permit number']) ?>	
		
		

	<?= $form->field($model, 'renew_date',['options' => ['class' =>''],'template' => '<div class="input-group mB20 col-md-6"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Permit expiry date']) ?>	

    </div>
    <div class="row">
    <div class="input-group mB20 col-md-6">
    <span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span><input type="email" class="form-control regtxt" id="exampleInputEmail1" placeholder="Permit issue date">
    </div>
    <div class="input-group mB20 col-md-6">
    <span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span><input type="email" class="form-control regtxt" id="exampleInputEmail1" placeholder="Permit expiry date">
    </div>
    </div>
    <div class="row">
    <div class="form-group mB30 col-md-6">
    <p class="text-left fnt13 text-primary">Upload vehicle permit image</p>
    <img src="images/image2.png" class="img-responsive"/>
    <input type="file" id="exampleInputFile" class="mT20">
    </div>
    <div class="form-group mB30 col-md-6">
    <p class="text-left fnt13 text-primary">Upload vehicle RC image</p>
    <img src="images/image2.png" class="img-responsive"/>
    <input type="file" id="exampleInputFile" class="mT20">
    </div>
    </div>
</div>
</div>
</div>
</div>

<div class="col-md-5 text-center  mT20 mB20">
<div class="bg-primary pL20 pR20 pull-left w100">
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please enter vehicle insurance details</p>
    <div class="row">
    <div class="input-group mB20 col-md-6">
    <span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span><input type="email" class="form-control regtxt" id="exampleInputEmail1" placeholder="Insurance valid from">
    </div>
    <div class="input-group mB20 col-md-6">
    <span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span><input type="email" class="form-control regtxt" id="exampleInputEmail1" placeholder="Insurance valid upto">
    </div>
    </div>
    <div class="row">
    <div class="form-group mB30 col-md-6">
    <p class="text-left fnt13 text-primary">Upload vehicle insurance image</p>
    <img src="images/image2.png" class="img-responsive"/>
  </div>
    <div class="form-group mB30 col-md-6">
    <input type="file" id="exampleInputFile" class="mT20">
    </div>
    </div>
    </div>  
</div>
</div>    

    <div class="bg-primary pL20 pR20 pull-left w100 mT20 text-left minh2">
    <div class="row">
    <div class="col-md-12 mT20">
    <div class="row">
      <div class="col-md-5">
      <p class="text-primary">Please select states</p>
      </div>
      <div class="col-md-7">
        <input type="email" class="form-control addstate w80 pull-left" id="exampleInputEmail1" placeholder="Add state">
        <button type="submit" class="btn btn-primary w20 pull-left">Add</button>
      </div>
      
      </div>  
    </div>
    
    <div class="row">
      <div class="col-md-12 pL20 mT10">
        <ul class="list-inline mL20" aria-labelledby="dropdownMenu1">
        <li> <input type="checkbox"> All</li>
        <li> <input type="checkbox"> Punjab</li>
        <li> <input type="checkbox"> Haryana</li>
        <li> <input type="checkbox"> Himachal pradesh</li>
        <li> <input type="checkbox"> Delhi</li>
        <li> <input type="checkbox"> Uttar pradesh</li>
      </ul>
      </div>
    </div>
   
    </div>  
</div>
</div> 
<div class="row">
<div class="col-md-4 col-md-offset-3">
<a href="home-login.html" class="btn btn-default col-md-offset-5 mB50">Add vehicle</a>
</div>
</div>
<?php ActiveForm::end(); ?> 

















<div class="box-body vehicles-form">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
     <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'truck_category')->dropDownList(Common::getTruckCategory(),['class'=>'form-control', 'prompt'=>'Select Truck Category']); ?>
        </div>
        
        <div class="col-md-6">
            <?php echo $form->field($model, 'type')->dropDownList($model::getTruckList(),['class'=>'form-control', 'prompt'=>'Select Truck']); ?>
        </div>
     </div>

     <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'loading_capacity') ?>
        </div>
         
        <div class="col-md-6">
            <?= $form->field($model, 'vehicle_number'); //->textInput(['readonly' => !$model->isNewRecord ]); ?>
        </div>
     </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'state') ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'number') ?>
        </div>        
    </div>    
    
    <div class="row">
        <div class="col-md-6">  
        <?php if(Yii::$app->controller->action->id == "update"){
            if(!empty($model["permit"]["pic"])){
                $img = Common::getMediaPath($model, $model["permit"]["pic"]);
                echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';
            }
        }?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pic')->fileInput(); ?>
        </div>       
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'permit_issue_date')->textInput(['readonly' => 'true']);  ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'permit_expiry_date')->textInput(['readonly' => 'true']); ?>
        </div>
    </div>

    <div class="row">
      <div class="col-md-6">  
        <?php if(Yii::$app->controller->action->id == "update"){
            if(!empty($model["registration"]["rc_pic"])){
                $img = Common::getMediaPath($model, $model["registration"]["rc_pic"]);
                echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';
            }
        }?>
      </div>
    </div>
        
    <div class="row">
      <div class="col-md-6">
          <?= $form->field($model, 'rc_pic')->fileInput(); ?>
      </div>

      <div class="col-md-6">
          <?= $form->field($model, 'rc_number') ?>
      </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'issue_date')->textInput(['readonly' => 'true']); ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'renew_date')->textInput(['readonly' => 'true']); ?>
        </div>
    </div>
    
    <div class="row">
      <div class="col-md-6">  
        <?php if(Yii::$app->controller->action->id == "update"){
            if(!empty($model["insurance"]["image"])){
                $img = Common::getMediaPath($model, $model["insurance"]["image"]);
                echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';
            }
        }?>
      </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'image')->fileInput(); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    var availableStates = [
        "Punjab",
        "Haryana",
        "Himachal Pradesh",
        "Jammu & Kashmir",
        "Rajsthan",
        "Uttar Pradesh",
    ];
    
    function split( val ) {
      return val.split( /,\s*/ );
    }
    
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#vehicles-state" ).bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      }).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableStates, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });    
    
    
    $('#vehicles-permit_issue_date, #vehicles-issue_date').daterangepicker({
        singleDatePicker: true,
        autoclose: true,
    });

    $('#vehicles-permit_issue_date, #vehicles-issue_date').on('change',function(){
        $('#vehicles-permit_expiry_date, #vehicles-renew_date').daterangepicker({
            singleDatePicker: true,
            autoclose: true,
        }).data('datepicker').setStartDate(new Date(this.value))
    });
    
    /*jQuery(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        //console.log(this);
        jQuery(this).lightbox();
    });*/
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>