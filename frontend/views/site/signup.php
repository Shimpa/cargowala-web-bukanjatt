<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Register as a Trucker';
$this->params['breadcrumbs'][] = $this->title;
?>
   <div class="col-md-5 col-md-offset-1  col-sm-8 col-sm-offset-2  text-center pR0 mT20 mB30">
    <div class="bg-primary p20 pull-left w100">
    <h2 class="mB20"><?= Html::encode($this->title) ?></h2>

    <div class="row signup2">
    <div class="col-md-12">
    <p class="text-primary">Please enter the user details</p>
	<?php $form = ActiveForm::begin(['id' => 'form-signup','options' => ['enctype' => 'multipart/form-data']]); ?>	
    <div class="row ">
		
    <?= $form->field($model, 'firstname',['options' => ['class' =>''],'template' => '<div class="col-md-6">
	<div class="row"><div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1"><img src="images/user.png"></div>{input}</div></div>{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'First name']) ?>
		
 
    
  <?= $form->field($model, 'lastname',['options' => ['class' =>''],'template' => '<div class="col-md-6">
	<div class="row"><div class="input-group col-md-12"><div class="input-group-addon" id="basic-addon1"><img src="images/user.png"></div>{input}</div></div>{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Last name']) ?>
    
	
	</div>	
	<div class="row">	
	<?= $form->field($model, 'email',['options' => ['class' =>'email-unique'],'template' => '<div class="col-md-12"><div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/email.png"></span>{input}</div></div>{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Email']) ?>	
   
		</div>
    <div class="row">
	<?= $form->field($model, 'password',['options' => ['class' =>''],'template' => '<div class="col-md-6">
	<div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/lock.png"></span>{input}</div></div>{error}</div>'])->passwordInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Password']) ?>	
    <?= $form->field($model, 'confirm_password',['options' => ['class' =>''],'template' => '<div class="col-md-6">
	<div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/lock.png"></span>{input}</div></div>{error}</div>'])->passwordInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Confirm password']) ?>
    

    </div>
	
	
		
    <div class="row">
    <div class="form-group mB30 col-md-4 col-sm-4 text-left" id="image-view">
	<label for="users-image">
    <img src="images/profile-image.png" class="img-responsive"/>
    		
    </label> 
	</div>
    <div class="form-group mB30 col-md-8 col-sm-8">
    <p class="text-left fnt13 text-primary">Select your profile picture <br/>(image size should not exceed 1MB)</p>
	<span id="name-show" ></span> 	
    <?= $form->field($model, 'image',['options' => ['class' =>'']])->fileInput(['autofocus' => true,'class'=>'hidden','image-view'=>'#image-view img','onchange'=>"$('#name-show').html(this.value.split('\\\').pop())"])->label(false) ?>
    </div>
  </div>
    </div>
    <p class="fnt13">By clicking next, I agree to the <a href="#" class="text-primary">Terms &amp; Conditions</a> and <a href="#" class="text-primary">Privacy Policy</a> of CargoWala</p>
    	<div class="row">
<div class="col-md-10 col-md-offset-1 text-center">
	<p id="cus-error" ><?= empty($model->getErrors()['email'][0])?null:$model->getErrors()['email'][0];?></p>
	
	</div>
</div>
	<?= Html::submitButton('Next', ['class' => 'btn btn-default mB20', 'name' => 'signup-button']) ?>	
    <p>Already have an account? <a href="<?=Url::to(['site/login'])?>" class="register" >Login Here</a></p>
    </div>  
    
   <?php ActiveForm::end(); ?>  



</div>
</div> 






