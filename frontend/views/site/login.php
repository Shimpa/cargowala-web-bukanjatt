<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bg1">
<div class="container-fluid">
<div class="row">
    <div class="col-md-6 col-xs-12 col-sm-6 mT20 txtc">
        <img src="images/logo.png" class="img-responsive"/>
        <h1 class="mT60">Transforming the way India delivers. Trucking shall be a pleasure! That's our promise.</h1>

    </div> 
    <div class="col-md-3 col-xs-12 col-sm-6 pull-right text-center pR0">
    <div class="bg-primary bgprimaryrad p40 pull-left w100  pT30">
    <h2 class="mB20">Login as a Trucker</h2>
   	<div class="row">
      <div class="col-md-10 col-md-offset-1 text-center">

	
	 </div>
    </div>

    <?php if(isset($type)){ ?>    
        <div class="row">
            <div class="col-lg-12">
                <?php if(Yii::$app->session->hasFlash($type)){ ?>
                    <div class="alert alert-<?php echo $type; ?>" role="alert">
                        <?= Yii::$app->session->getFlash($type) ?>
                    </div>
                <?php }?>
            </div>
        </div>
    <?php }?>
    
    <div class="row">
    <div class="col-md-12 mT-10">
    <p class="mB40 mT20">Login with your email id</p>
    <?php $form = ActiveForm::begin(['id' => 'form-login']); ?>
	<?= $form->field($model, 'username',['options' => ['class' =>'email-unique'],'template' => '<div class="input-group mB20"><span class="input-group-addon" id="basic-addon1"><img src="images/email.png"></span>{input}</div><div class="c-error" >{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Email']) ?>
	
	<?= $form->field($model, 'password',['options' => ['class' =>'email-unique'],'template' => '<div class="input-group mB30"><span class="input-group-addon" id="basic-addon1"><img src="images/lock.png"></span>{input}</div><div class="c-error" >{error}</div>'])->passwordInput(['class'=>'form-control regtxt','placeholder'=>'Password']) ?>	

    <div class="checkbox fnt13  mB20">
    <label><input type="checkbox"><span> Remember me</span></label>
    <a href="<?=Url::to(['site/request-password-reset'])?>" class="mL30">Forgot Password ? </a>
    </div>
	
	<?= Html::submitButton('Login', ['class' => 'btn btn-default mB50 mT30', 'name' => 'login-button']) ?>	
    <?php ActiveForm::end(); ?>  

    <p>Don't have an account ? </p>
    <a href="<?=Url::to(['site/signup'])?>" class="register" >Register Here</a>

    

  </div>  
</div>
</div>    
</div>
</div>
</div>
</div>

<?php 
	if(Yii::$app->session->hasFlash('email_confirm')):
 ?>	   
 <div class="container">
  <!-- Trigger the modal with a button -->
  <button type="button" id="openpop" class="btn btn-info btn-lg hide" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registred Successfully</h4>
        </div>
        <div class="modal-body">
          <p><?=Yii::$app->session->getFlash('email_confirm')?></p>
        </div>
        <div class="modal-footer">
          <button type="button" id="close-pop" class="btn btn-default" data-dismiss="modal">OK</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div> 
 
<?php endif; ?>
<?php 
$JS = <<<SCRIPT
$('#openpop').click();
setTimeout(function(){
 $('#close-pop').click();
},3000)

SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>



