<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Register as a Trucker';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
  <!-- Trigger the modal with a button -->
  <button type="button" id="openpop" class="btn btn-info btn-lg hide" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registred Successfully</h4>
        </div>
        <div class="modal-body">
          <p>Email Verification link sent on registed email.Please confirm your email before login.</p>
        </div>
        <div class="modal-footer">
          <a href="<?=Url::to(['site/login'])?>">OK</a>
        </div>
      </div>
      
    </div>
  </div>
  
</div>   
<?php 
$JS = <<<SCRIPT
$('#openpop').click();

SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>











