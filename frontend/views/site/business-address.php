<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\States;

$this->title = 'Register as a Trucker';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-4 col-md-offset-1 text-center pR0 mT20 mB30">
    <div class="bg-primary p20 pull-left w100">
   
    <?php $form = ActiveForm::begin(['id' => 'form-address','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please enter the Comapany details</p>
    
	<?= $form->field($model, 'business_pancard',['options' => ['class' =>''],'template' => '<div class="col-md-12 p0"><div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div></div>{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Pan No']) ?>
		<?= $form->field($model, 'business_service_taxno',['options' => ['class' =>''],'template' => '<div class="col-md-12 p0"><div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div></div>{error}</div>'])->textInput(['autofocus' => true,'class'=>'form-control regtxt','placeholder'=>'Service tax number']) ?>	
	
	<?= $form->field($model, 'business_name',['options' => ['class' =>''],'template' => '<div class="col-md-12 p0"><div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Company name']) ?>
	
	<?= $form->field($model, 'landline_number',['options' => ['class' =>''],'template' => '<div class="col-md-12 p0"><div class="row"><div class="input-group col-md-12"><span class="input-group-addon" id="basic-addon1"><img src="images/phone.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','validate'=>'landline','placeholder'=>'Landline No. (eg.01725035560)']) ?>
		
	<?= $form->field($model, 'office_address',['options' => ['class' =>''],'template' => '<div class="input-group2 mB20">{input}{error}</div>'])->textArea(['placeholder'=>'Office address']) ?>
	
		<?= $form->field($model, 'registered_address',['options' => ['class' =>''],'template' => '<div class="input-group2 mB20">{input}{error}</div>'])->textArea(['placeholder'=>'Registered address']) ?>	
     
	<div class="row">
		<div class="col-md-12 text-left states">
		<label>Select states of operation</label>	
	 <ul class="list-unstyled text-left" id="states-list">
	  <li>Loading...</li>	 
      
	</ul>
	<?php $error =empty($model->getErrors()['states'][0])?NULL:$model->getErrors()['states'][0];	?>	
	</div>
	</div>	 


    <div class="row">
    <div class="form-group mB30 col-md-4 col-sm-4 text-left" id="image-view">
	<label for="users-business_logo">
    <img src="images/profile-image.png" class="img-responsive"/>
    		
    </label>	
	</div>
    <div class="form-group mB30 col-md-8 col-sm-8">
    <p class="text-left fnt13 text-primary">Select your Company Logo<br/>(logo size should not exceed 1MB)</p>
		<span id="name-show" ></span> 
    <?= $form->field($model, 'business_logo',['options' => ['class' =>'']])->fileInput(['autofocus' => true,'class'=>'hidden','image-view'=>'#image-view img','onchange'=>"$('#name-show').html(this.value.split('\\\').pop())"])->label(false) ?>
    </div>
  </div>
	<div class="row">
<div class="col-md-10 col-md-offset-1 text-center">
	<p id="cus-error" > <?=$error?></p>
	
	</div>
</div>	
	<?= Html::submitButton('Register', ['class' => 'btn btn-default', 'name' => 'signup-button']) ?>	
    </div>  
     <?php ActiveForm::end(); ?>    



</div>
</div>    
<?php 
$JS = <<<SCRIPT
var selected = '{$model->states}';
$(document).ready(function(){
  $.getJSON('?r=site/states-list',function(res){
     var html ='';
	 for(var i in res){
	    html+='<li><input type="checkbox" id="'+res[i]._id+'"  name="Users[states][]" value="'+res[i]._id+'">';
		html+='<label for="'+res[i]._id+'">'+res[i].name+'</label></li>'; 
	 }   
  $("#states-list").html(html);        
  });

});
var msg ={};
$("#form-address").on("afterValidateAttribute", function(event, attribute, messages) {
    event.preventDefault();
	var hasError = messages.length !== 0;
	msg[attribute.name]=messages[0] || false;
    $("#cus-error").html(getError());
    return hasError;
});
function getError(){
   for(var m in msg){
     if(msg[m])
	   return msg[m];
   }
   return null;
}

SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>











