<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password" style='margin:10% 30%'>
    <h1><?= Html::encode($this->title); ?></h1>
    <?php if(isset($type)){ ?>    
        <div class="row">
            <div class="col-lg-12">
                <?php if(Yii::$app->session->hasFlash($type)){ ?>
                    <div class="alert alert-<?php echo $type; ?>" role="alert">
                        <?= Yii::$app->session->getFlash($type) ?>
                    </div>
                <?php }?>
            </div>
        </div>
    <?php }?>
    <p>Please choose your new password:</p>
    <br/>
    <div class="row">
        <div class="col-lg-9">
            <?php $form = ActiveForm::begin(['id' => 'password-reset-form']); ?>
                <div class="row">
                    <?php echo $form->field($model, 'password')->passwordInput(['autofocus' => true]); ?>
                </div>

                <div class="row">
                    <?php echo $form->field($model, 'confirm_password')->passwordInput(['autofocus' => true]); ?>
                </div>            
            
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>