<?php
use yii\helpers\Html;
use common\models\Common;
use common\models\Vehicles;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php 
	if(Yii::$app->session->hasFlash('flash_msg')):
 ?>
<div class="flash-msg">
  <?=Yii::$app->session->getFlash('flash_msg')?>
</div>
<?php endif; ?>   
  <div class="col-md-4 col-xs-12 col-sm-5 pull-right text-center pR0 mT20 " >
  <div class="bg-primary pT10 pL10 pB10 bdrr">
    <div class="row">
      <div class="col-md-4 col-xs-12 text-center pL20">
        <img src="<?=Yii::$app->user->getImage()?>" class="img-circle img-responsive "/>
        <div class="fnt13 mT10 editlink text-left txtc mL15"><a href="<?=Url::to(['trucker/update','id'=>Yii::$app->user->getId()])?>"><div class="edit hidden-xs hidden-sm"></div>Edit profile</a></div>
      </div>  
      <div class="col-md-8 col-xs-12 mTx20">
        <h3 class="mT0">Welcome, <?=Yii::$app->user->getName()?></h3>
        <h4><?=Yii::$app->user->getCompany()?></h4>
        
       
         <div class="btn-logout pull-right mT20 fnt13 mR20">
		 <div class="logout hide"></div></div>
	   </div>
      </div>
      </div>


   </div> 
	<br style="clear:both;"/>  

</div>
  <div class="row mT20 mB50">
    <div class="col-md-5 col-md-offset-2">
		

		<div class="bg-primary pT10">
	<div class="row mB20">
		<div class="col-md-7">
		<h2 class="sub-header pL20">Your Trucks</h2>	
		</div>
		<div class="col-md-4 mT20">
			<a href="<?=Url::to(['vehicles/create'])?>" class="btn btn-default btn-default2"><span class="glyphicon glyphicon-plus"></span> Add Truck</a>
		</div>
		  </div>	  
      
          <div class="table-responsive truckers-none ">
              <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
          //  '_id',
            ['attribute'=>'truck_type','header'=>'Truck Type', 'filter'=>Vehicles::getTruckList()],
            ['attribute'=>'vehicle_number','header'=>'Vehicle Number'],
            'number',
            //'registration',
            // 'insurance',
            // 'owner_id',
            //['attribute' => 'status', 'filter' => [0=>'Inactive', 1=>'Active']],
            // 'created_on',
            // 'modified_on',

            ['class' => 'yii\grid\ActionColumn',
			'header'=>'Actions',
            'template' => '{update} {delete}',
			'buttons' => [

            //view button
            'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Edit'),
                            //'class'=>'btn btn-primary btn-xs',                                  
                ]);
            },
        ],

        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'update')
                return Url::to(['vehicles/update','id'=>$model->_id->__toString()]);
			if ($action === 'delete')
                return Url::to(['vehicles/delete','id'=>$model->_id->__toString()]);		         

		}
        ],
        ],
    ]); ?>
          </div>
          </div>
    </div>  
    <div class="col-md-5">
      <div class="bg-primary pT10 ">
      <div class="row mB20">
		<div class="col-md-7">
		<h2 class="sub-header pL20">Your Drivers</h2>	
		</div>
		<div class="col-md-4 mT20">
			<a href="<?=Url::to(['drivers/create'])?>" class="btn btn-default btn-default2"><span class="glyphicon glyphicon-plus"></span> Add Driver</a>
		</div>
		  </div>
          <div class="table-responsive truckers-none">
   <?= GridView::widget([
        'dataProvider' => $drivers_dataProvider,
        'filterModel' => $drivers_searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'_id',
            'name.firstname',
            'licence.number',
            'permit.number',
            //'mobile_no',
            //'image',
            // 'truck_type',
            // 'permit',
            // 'address',
            // 'licence',
            // 'status',
            // 'created_on',
            // 'modified_on',

                  ['class' => 'yii\grid\ActionColumn',
			'header'=>'Actions',
            'template' => '{update} {delete}',
			'buttons' => [

            //view button
            'update' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Edit'),
                            //'class'=>'btn btn-primary btn-xs',                                  
                ]);
            },
        ],

        'urlCreator' => function ($action, $model, $key, $index) {
            if ($action === 'update')
                return Url::to(['drivers/update','id'=>$model->_id->__toString()]);
			if ($action === 'delete')
                return Url::to(['drivers/delete','id'=>$model->_id->__toString()]);
				
          
		}
        ],
        ],
    ]); ?>
          </div>
    </div> 
  </div>
  </div> 