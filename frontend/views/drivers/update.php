<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Drivers */

$this->title = 'Update Drivers: ' . ' ' . $model->name['firstname'];
$this->params['breadcrumbs'][] = ['label' => 'Drivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name['firstname'], 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="drivers-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
