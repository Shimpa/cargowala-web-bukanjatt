<?php
use yii\helpers\Url;
use common\models\Common;
global $image_view;
?>
<div di="orbycam<?=$image_view?>">
<hr/>
<span>OR</span>
<a href="javascript:;" id="clicker<?=$image_view?>" data-toggle="modal" data-target="#mm<?=$image_view?>">Take Picture</a>
</div>
	<!-- Modal -->
<div id="mm<?=$image_view?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Take a Picture</h4>
      </div>
      <div class="modal-body" id="camerahtml<?=$image_view?>">
        <p>Some text in the modal.</p>
		  
      </div>
	  <div id="upload_results">
		</div>	
      <div class="modal-footer camfooter">
        <button type="button" class="btn btn-default" onclick="take_snapshot(this,'<?=$image_view?>');" >Capture</button>
		<button type="button" class="btn btn-default" onclick="resethtml(this);">Reset</button>
      </div>
    </div>

  </div>
</div>

<?php 
$JS = <<<SCRIPT
var nvagt  = navigator.userAgent;
var ostype = nvagt.indexOf("Android") || nvagt.indexOf("iPhone OS"); 
if(ostype!=-1){
  $("#orbycam{$image_view}").css("display","none");
}
//var camhtml =document.getElementById("camerahtml{$image_view}");
 $('#mm{$image_view}').on('show.bs.modal', function () {
   setcamHtml(this);
  $("div.camfooter button").each(function(k,v){
		$(v).removeAttr('data-val').removeAttr("data-dismiss");
	 
  });
})
SCRIPT;
$this->registerJs($JS,$this::POS_END);

