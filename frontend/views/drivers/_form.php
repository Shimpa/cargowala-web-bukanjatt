<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Common;
global $image_view;

/* @var $this yii\web\View */
/* @var $model common\models\Drivers */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="col-md-5 text-center pR0 mT20 mB20">
    <div class="bg-primary p20 pull-left w100 mB20">
   
   
    <div class="signup2 mT20">
   
    <p class="text-left text-primary">Please add driver details</p>
		
    <div class="row">
	<div class="col-md-6 text-left text-primary">
	<label>First name</label>	
    <?= $form->field($model, 'firstname',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'First Name']) ?>
		</div>
	<div class="col-md-6 text-left text-primary">	
	<label>Last name</label>	
	<?= $form->field($model, 'lastname',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/user.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Last Name']) ?>	
		</div>	

    </div>
		
		
		
		
		
    <div class="row">
	<div class="col-md-6 text-left text-primary">		
	<label>Date of birth<small> (mm/dd/yyyy)</small></label>	
	<?= $form->field($model, 'date_of_birth',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Date of birth']) ?>
		</div>
	<div class="col-md-6 text-left text-primary">		
	<label>Mobile number</label>		
	<?= $form->field($model, 'mobile_no',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/phone.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Mobile number(9807656780)']) ?>
    </div>
		</div>
	
		
		
	<div class="row">
	<div class="col-md-6 text-left text-primary">			
	<label>Driving license number<br><small> DL/--/--</small></label>	
	<?= $form->field($model, 'licence_number',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Driving license number']) ?>
		</div>	
	<div class="col-md-6 text-left text-primary">		
	<label>Driving license expiry date<small> (mm/dd/yyyy)</small></label>	
	<?= $form->field($model, 'licence_expiry',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'DL expiry date']) ?>	
		</div>
    </div>
		

		
		
		
		
    <div class="row">
    <div class="form-group mB30 col-md-6" id="image-view4">
    <p class="text-left fnt13 text-primary">Upload driver photo<br> (Image size should not exceed 1 MB)</p>
	<label for="drivers-image"> 	
	<?php if($model->image): ?>	
    <img src="<?=$model->image?>" class="img-responsive"/>
	<?php else: ?>
	<img src="images/profile-image.png" class="img-responsive"/>	
	<?php endif;?>	
	</label>	
    <span id="name-show3" ></span> 
    <?= $form->field($model, 'image')->fileInput(['onchange'=>"$('#name-show3').html(this.value.split('\\\').pop())",'accept'=>"image/*",'capture'=>"camera",'class'=>'hidden','image-view'=>'#image-view4 img'])->label(false); ?>
    
	<?php $image_view = "image-view4"; echo  $this->render('webcam');?>
	</div>
    <div class="form-group mB30 col-md-6" id="image-view3">
    <p class="text-left fnt13 text-primary">Upload driving license image<br> (Image size should not exceed 1 MB)</p>
	<label for="drivers-licence_image"> 	
	<?php if($model->licence_image): ?>	
    <img src="<?=$model->licence_image?>" class="img-responsive"/>
	<?php else: ?>
	<img src="images/profile-image.png" class="img-responsive"/>	
	<?php endif;?>	
	</label>	
    <span id="name-show4" ></span>
    <?= $form->field($model, 'licence_image')->fileInput(['onchange'=>"$('#name-show4').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view3 img'])->label(false); ?>
    <?php $image_view = "image-view3"; echo  $this->render('webcam');?>
	</div>
    </div>
</div>
</div>

		    <div class="bg-primary pL20 pR20 pull-left w100 text-left minh2 trucktype">
    <div class="row">
    <div class="col-md-12 mT20">
      <p class="text-primary">Please select truck type</p>
    </div>
    
    <div class="row">
      <div class="col-md-12 mL20 mT10">
		<?= $form->field($model, 'truck_type')->checkboxList(Common::getTruckCategory())->label(false) ?> 
 
      </div>
    </div>
   
    </div>  
</div>
</div>



<div class="col-md-4 text-center mT20 mB20">
<div class="bg-primary pL20 pR20 pull-left w100 signup2">
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please enter commercial permit details</p>
	
		
		
		
	
    <div class="row">
	<div class="col-md-12 text-left text-primary">	
	<label>Commercial permit number</label>	
	<?= $form->field($model, 'permit_number',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'CP number']) ?>
		</div>	
		<div class="col-md-12 text-left text-primary">	
		<label>Permit expiry date<small> (mm/dd/yyyy)</small></label>	
	<?= $form->field($model, 'permit_expiry',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'CP expirt date']) ?>	

    </div>
		
	

		
		
    <div class="row">
    <div class="form-group mB30 col-md-12" id="image-view2">
    <p class="text-center fnt13 text-primary">Upload commercial permit image<br> (Image size should not exceed 1 MB)</p>
	<label for="drivers-permit_image"> 	
	<?php if($model->permit_image): ?>	
    <img src="<?=$model->permit_image?>" class="img-responsive"/>
	<?php else: ?>
	<img src="images/profile-image.png" class="img-responsive"/>	
	<?php endif;?>	
	</label>	

  </div>
    <div class="form-group mB30 col-md-6">
	<span id="name-show2" ></span>		
    <?= $form->field($model, 'permit_image')->fileInput(['onchange'=>"$('#name-show2').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view2 img'])->label(false); ?>
    </div>
    </div>
    </div>  
</div>
</div>    
	</div>

<div class="bg-primary pL20 pR20 pull-left w100 mT20 signup2">
    <div class="row">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please enter driver address details</p>
		
		
				
		</div>	
    <div class="row">
    <div class="col-md-12 text-left text-primary">
	<label>Permanent address</label>	
	<?= $form->field($model, 'driver_address',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div></div>{error}</div>'])->textArea(['placeholder'=>'Permanent address']) ?>	
		</div>
		<div class="col-md-12 text-left text-primary">
		<label>Aadhar card number</label>	
	<?= $form->field($model, 'adhaar_no',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Adhaar number']) ?>	
		</div>
    </div>

    <div class="row">
    <div class="form-group mB30 col-md-6" id="image-view">
    <p class="text-left fnt13 text-primary">Upload address proof photo<br> (Image size should not exceed 1 MB)</p>
	<label for="drivers-address_image"> 	
	<?php if($model->address_image): ?>	
    <img src="<?=$model->address_image?>" class="img-responsive"/>
	<?php else: ?>
	<img src="images/profile-image.png" class="img-responsive"/>	
	<?php endif;?>	
	</label>
	<span id="name-show0" ></span>	
    <?= $form->field($model, 'address_image')->fileInput(['onchange'=>"$('#name-show0').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view img'])->label(false); ?>
    </div>
    <div class="form-group mB30 col-md-6" id="image-view1">
    <p class="text-left fnt13 text-primary">Upload adhaar card photo<br> (Image size should not exceed 1 MB)</p>
	<label for="drivers-adhaar_image"> 	
	<?php if($model->adhaar_image): ?>	
    <img src="<?=$model->adhaar_image?>" class="img-responsive"/>
	<?php else: ?>
	<img src="images/profile-image.png" class="img-responsive"/>	
	<?php endif;?>	
	</label>	
    <span id="name-show1" ></span>	
    <?= $form->field($model, 'adhaar_image')->fileInput(['onchange'=>"$('#name-show1').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view1 img'])->label(false); ?>
    </div>
    </div>
    </div>  
</div>
</div> 

<p>&nbsp;</p>
<div class="row">
<div class="col-md-5 col-md-offset-3 text-center">
	<p id="cus-error" ><?php echo empty(array_shift($model->getErrors())[0])?'':array_shift($model->getErrors())[0];?></p>
	</div>
</div>

<div class="row">
<div class="col-md-5 col-md-offset-3 text-center cus">
	<p> By  clicking below button you ensure that the above provided details are true and best to my knowledge. </a></p>
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add driver') : Yii::t('app', 'Update vehicle'), ['class' => 'btn btn-default mB50']) ?>	
<!--<a href="<?=Url::to(['trucker/index'])?>" class="btn btn-default mL20 mT20" >Cancel</a> -->
</div>
</div>
<?php ActiveForm::end(); ?>



<?php 
$path = Common::getUrlMediaPath()."tmp/";
$url  = Url::to(['site/uploadwebcam']);
$JS = <<<SCRIPT
var cam_img_path = "{$path}";
var uploadimageurl = "{$url}";
$(document).ready(function(){


    
    $('#drivers-date_of_birth').daterangepicker({
        singleDatePicker: true,
        autoclose: true,
	   //"minDate": new Date(Date.now()-(1000*60*60*60*12*12*58)),
	   "maxDate": new Date(Date.now()-(1000*60*60*60*12*12*18)),
	   "startDate": new Date(Date.now()-(1000*60*60*60*12*12*18)),
		showDropdowns: true
    });
	$('#drivers-licence_expiry').daterangepicker({
        singleDatePicker: true,
		//"minDate": new Date(Date.now()-(1000*60*60*60*12*12*25)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });
	 $('#drivers-permit_expiry').daterangepicker({
        singleDatePicker: true,
		//"minDate": new Date(Date.now()-(1000*60*60*60*12*12*5)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });

});
//webcam.set_api_url( uploadimageurl );
webcam.set_quality( 90 ); // JPEG quality (1 - 100)
webcam.set_shutter_sound( false ); // play shutter click sound
//webcam.set_hook( 'onComplete', 'showthumb');

webcam.set_hook( 'onLoad', 'my_completion_handler1');
 var iid = '',thisobj="",imgview="";
 function setcamHtml(obj){
   $(".modal-body embed").attr("id",'');
   var camhtmlobj = webcam.get_html(320, 240);
   iid  ="activewebcam_movie";
   var mdl = obj.querySelector('.modal-body')
   thisobj =mdl;
   $(mdl).html(camhtmlobj).find('embed').attr('id',"activewebcam_movie");
   webcam.set_hook( 'onError', 'my_completion_handler1'); 
 }

function take_snapshot(obj,img_view){

   if(obj.classList.contains('capturedone')){
	  obj.classList.remove("capturedone");
	  obj.innerHTML =obj.getAttribute("data-val");
	  obj.removeAttribute("data-val");
	  obj.setAttribute('data-dismiss',"modal");
	}else{
	  obj.setAttribute('data-val',obj.innerHTML);
	  obj.innerHTML = "Done";
	  obj.classList.add("capturedone");
	// take snapshot and upload to server
	
		 //document.getElementById('upload_results').innerHTML = '<h1>Uploading...</h1>';
		
		 //document.getElementById(fileid).previousElementSibling.value = data_url;
		 webcam.snap(uploadimageurl,function(dta_url){
		  var imgg    = document.querySelector("#"+img_view+" img");
		       imgg.src=cam_img_path+dta_url;
		    var fileid = imgg.parentElement.getAttribute("for");
		    document.getElementById(fileid).previousElementSibling.value =dta_url;
		 },undefined,iid);   
	  			
	}			      
}


function my_completion_handler1(msg){
	   if(msg)
	      thisobj.innerHTML = '<p id="camerror">'+msg+'</p>';
	}
 function resethtml(obj){
 if(document.getElementById("camerror")==undefined){
    //camhtml.innerHTML = "";
	var firstbtn = obj.previousElementSibling;
	firstbtn.classList.remove('capturedone');
	if(firstbtn.getAttribute('data-val'))
	   firstbtn.innerHTML = firstbtn.getAttribute('data-val');
	firstbtn.removeAttribute('data-val');
	webcam.reset(iid);
    document.getElementById('upload_results').innerHTML = "";
 }
 }	
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>
