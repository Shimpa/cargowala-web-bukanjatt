<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */

$this->title = Yii::t('app', 'Create Vehicles');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehicles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [  

          [
           'label' => Yii::t('app', 'Manage  Vehicles'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],         
];
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


