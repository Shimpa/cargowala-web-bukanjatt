<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="col-md-5 text-center pR0 mT20 mB20">
    <div class="bg-primary p20 pull-left w100 mB20">   
   
    <div class="row  signup2">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please add vehicle details</p>
		
    <div class="row">
	<div class="col-md-6 text-left text-primary">	
	<label>Truck category</label>	
	<?= $form->field($model, 'truck_category',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/truck.png"></span>{input}</div>{error}</div></div>'])->dropDownList(Common::getTruckCategory(),['class'=>'form-control regtxt','prompt'=>'Select Truck Category']) ?>
		</div>
		<div class="col-md-6 text-left text-primary">
		<label>Truck type</label>	
	<?= $form->field($model, 'truck_type',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/truck-type.png"></span>{input}</div>{error}</div></div>'])->dropDownList([],['class'=>'form-control regtxt','prompt'=>'Select Truck Category First']) ?>		
		</div>	
		<div class="row">
		
		<div class="col-md-6"></div>	
		
		</div>
		<div class="col-md-6 text-left text-primary">
		<label>Vehicle / RC number</label>
	<?= $form->field($model, 'vehicle_number',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div>{error}</div></div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Vehicle / RC number']) ?>
		</div>	
		<div class="col-md-6 text-left text-primary">
		<label>RC expiry date <small> (mm/dd/yyyy)</small></label>
	<?= $form->field($model, 'rc_expiry_date',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div>{error}</div></div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'RC Expiry Date']) ?>		
		</div>
		
	

    </div>
	<div class="row">
	<div class="col-md-6 text-left text-primary">
	<label>Permit number</label>	
	<?= $form->field($model, 'number',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/card.png"></span>{input}</div>{error}</div></div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Permit number']) ?>	
	</div>	
		
	<div class="col-md-6 text-left text-primary">	
	<label>Permit expiry date <small> (mm/dd/yyyy)</small></label>	
	<?= $form->field($model, 'permit_expiry_date',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div>{error}</div></div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Permit expiry date']) ?>	
		</div>	
    </div>
	
		
    <div class="row">
    <div class="form-group mB30 col-md-5" id="image-view">
    <p class="text-center fnt13 text-primary">Upload vehicle permit image<br> (Image size should not exceed 1MB)</p>
	<label for="vehicles-pic"> 
	
	 <?php if(!empty($model->permit["pic"])){
                $img = Common::getMediaPath($model, $model->permit["pic"]);
                echo '<image src='.$img.' class="img-responsive"  data-toggle="lightbox" />';
            }else{?>
	 <img src="images/profile-image.png" class="img-responsive"/>
	<?php }?>	
    </label>
	 <span id="name-show2" ></span> 	
     <?= $form->field($model, 'pic')->fileInput(['onchange'=>"$('#name-show2').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view img'])->label(false); ?>
    </div>
    <div class="form-group mB30 col-md-5 col-md-offset-1" id="image-view1">
    <p class="text-center fnt13 text-primary">Upload vehicle RC image<br> (Image size should not exceed 1MB)</p>
	<label for="vehicles-rc_pic"> 	
     <?php  if(!empty($model["registration"]["rc_pic"])){
                $img = Common::getMediaPath($model, $model["registration"]["rc_pic"]);
                echo '<image src='.$img.' class="img-responsive"  data-toggle="lightbox" />';
        }else{?>
	 <img src="images/profile-image.png" class="img-responsive"/>
	<?php }?>
	<span id="name-show3" ></span> 	
       <?= $form->field($model, 'rc_pic')->fileInput(['onchange'=>"$('#name-show3').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>"#image-view1 img"])->label(false); ?>
    </div>
    </div>
</div>
</div>
</div>
</div>

<div class="col-md-4 text-center mT20 mB20">
<div class="bg-primary pL20 pR20 pull-left w100">
    <div class="row signup2">
    <div class="col-md-12 mT20">
    <p class="text-left text-primary">Please enter vehicle insurance details</p>
	<div class="row text-left text-primary">
		<div class="col-md-12 "><label>Insurance valid upto <small> (mm/dd/yyyy)</small></label></div>
		
		
		</div>	
    <div class="row">
	<?= $form->field($model, 'renew_date',['options' => ['class' =>''],'template' => '<div class="col-md-12"><div class="row"><div class="input-group npad"><span class="input-group-addon" id="basic-addon1"><img src="images/date.png"></span>{input}</div></div>{error}</div>'])->textInput(['class'=>'form-control regtxt','placeholder'=>'Insurance valid upto']) ?>	

    </div>

		
    <div class="row">
    <div class="form-group col-md-12" id="image-view2">
    <p class="text-center fnt13 text-primary pL15">Upload vehicle insurance image<br> (Image size should not exceed 1MB)</p>
	  <label for="vehicles-image">   	
      <?php if(empty($model["insurance"]["image"])){ ?>
		<img src="images/profile-image.png" class="img-responsive"/>
	  <?php }else{	
                $img = Common::getMediaPath($model, $model["insurance"]["image"]);
                echo '<image src='.$img.' class="img-responsive"  data-toggle="lightbox" />';
            }
       ?>
		</label> 
	<span id="name-show1" ></span> 	
  </div>
    
		
   <?= $form->field($model, 'image')->fileInput(['onchange'=>"$('#name-show1').html(this.value.split('\\\').pop())",'class'=>'hidden','image-view'=>'#image-view2 img'])->label(false); ?>
		
    
    </div>
		
    </div>  
</div>
</div>    


</div> 
<div class="row">
<div class="col-md-10 col-md-offset-1 text-center">
	<p id="cus-error" ></p>
	
	</div>
</div>	
<div class="row">
	
	<?php  ?>
<div class="col-md-4 col-md-offset-3 text-center mT20">
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add vehicle') : Yii::t('app', 'Update vehicle'), ['class' => 'btn btn-default col-md-offset-5 mB50']) ?>	

</div>
</div>
<?php ActiveForm::end(); ?> 
<?php 
$JS = <<<SCRIPT
 var vehicle = '{$model->truck_type}';
$(document).ready(function(){
        


    $('#vehicles-truck_category').change(function(){
        var siteUrl = '?r=vehicles/gettrucks';
        var type = $(this).val();
		 $('#vehicles-truck_type').html('<option>Loading...</option>');
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type,truck:vehicle},
            success: function(msg){
                $('#vehicles-truck_type').html(msg);
            }
        });
    });
    
 console.log(vehicle);
    if(vehicle!==null)
	  $('#vehicles-truck_category').trigger('change');
        
    var availableStates = [
        "Punjab",
        "Haryana",
        "Himachal Pradesh",
        "Jammu & Kashmir",
        "Rajsthan",
        "Uttar Pradesh",
    ];
    
    function split( val ) {
      return val.split( /,\s*/ );
    }
    
    function extractLast( term ) {
      return split( term ).pop();
    }
 
   /* $( "#vehicles-state" ).bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      }).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableStates, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });    
    */
    
    $('#vehicles-renew_date').daterangepicker({
        singleDatePicker: true,
		showDropdowns: true,
        autoclose: true,
    });
	$('#vehicles-rc_expiry_date').daterangepicker({
        singleDatePicker: true,
		showDropdowns: true,
        autoclose: true,
    });
	 $('#vehicles-permit_expiry_date').daterangepicker({
        singleDatePicker: true,
		showDropdowns: true,
        autoclose: true,
    });


    
    /*jQuery(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        //console.log(this);
        jQuery(this).lightbox();
    });*/
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>