<?php
namespace frontend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Vehicles;
use common\models\Users;
use common\models\VehiclesSearch;
use common\models\DriversSearch;
use common\models\Common;
use common\models\Trucks;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * VehiclesController implements the CRUD actions for Vehicles model.
 */
class VehiclesController extends Controller
{
    
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'gettrucks', 'gettruckload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all Vehicles models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new VehiclesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$drivers_searchModel = new DriversSearch();
        $drivers_dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'drivers_searchModel' => $drivers_searchModel,
            'dataProvider' => $dataProvider,
            'drivers_dataProvider' => $drivers_dataProvider,
        ]);
    }

    /**
    * Displays a single Vehicles model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vehicles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Vehicles();
        $model->scenario = Vehicles::SCENARIO_CREATE;
		$model->status = 1;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            $this->uploadImage($model, true);
			Users::emailNotifications("driver_added");
			Yii::$app->session->setFlash('flash_msg',"Vehicle detail added successfully.");
			return $this->redirect(['trucker/index']);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }        
    }

    /**
    * Updates an existing Vehicles model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->scenario = Vehicles::SCENARIO_UPDATE;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->uploadImage($model, false); // call to upload function.
            $model->save();
			Yii::$app->session->setFlash('flash_msg',"Vehicle detail updated successfully.");
            return $this->redirect(['trucker/index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }        
    }

    /**
    * Deletes an existing Vehicles model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        $record = $this->findModel($id);
        $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            Common::DelAllMedia($path);
        $record->delete();
		Yii::$app->session->setFlash('flash_msg',"Vehicle record deleted successfully.");
        return $this->redirect(['trucker/index']);
    }

    /**
    * Finds the Vehicles model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Vehicles the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Vehicles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = '';
        if(!empty($_FILES['Vehicles']['name'])){
            foreach($_FILES['Vehicles']['name'] as $key => $val){
                $prefix = 'registration';
                if($key == 'pic'){
                    $prefix = 'permit';
                }else if($key == 'image'){
                    $prefix = 'insurance';
                }
                if(!empty($val))
                    $image = UploadedFile::getInstance($model, $key);
                if(!empty($image)){
                    if(!$stat){
                        $filename = $model->oldAttributes[$prefix][$key];
                        if($image)
                            Common::DelFile($path.DS.$filename);
                    }
                    $model->$prefix  = $prefix.'_'.$key.".".$image->extension;
                    $filename = $model->$prefix;
                    $image->saveAs($path.DS.$filename);
                    // Common::SiteStardardImageSizesResize($path.DS.$model->image);
                    $model->$prefix = [$key => empty($filename) ? NULL : $filename];
                    if($stat) $model->save(false);
                }elseif(empty($image) && !$stat){ //if images are not Changed/Uploaded again
                    if(!empty($model->oldAttributes[$prefix][$key])) $filename = $model->oldAttributes[$prefix][$key];
                    $model->$prefix = [$key => empty($filename) ? NULL : $filename];
                    $model->save(false);
                }
            }
        }
        return true;
    }
    
    public function actionGettrucks(){
        $type  = Yii::$app->request->post('type');
        $truck = Yii::$app->request->post('truck');
        $res = Trucks::find()->where(['truck_type' => $type, 'status' => 1])->orderBy('loading_capacity ASC')->all();

        $data = '';
        $data .= "<option value = ''>Select Truck</option>";
        foreach($res as $key => $val){
            $id = $val->_id->__toString();
            $name = $val->name;
			$selected = $truck==$id?"selected='selected'":null;
            $data .= "<option {$selected} value= ".$id.">".$name."   ( ".$val->loading_capacity." ton ) </option>";
        }
        die($data);
    }
    
    public function actionGettruckload(){
        $type = Yii::$app->request->post('type');
        $res = Trucks::find()->where(['_id' => $type])->one();
        die($res->attributes['loading_capacity']);
    }

}