<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Drivers;
use common\models\Users;
use common\models\DriversSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Common;
use yii\web\UploadedFile;


/**
 * DriversController implements the CRUD actions for Drivers model.
 */
class DriversController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''], //Put the actions here which you don't want to authenticate
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','create','update','delete'], //Put the actions here which you want to authenticate
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Drivers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DriversSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Drivers model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Drivers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Drivers();
        //$model->load(Yii::$app->request->post()); 
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->save(); 
			$this->uploadImage($model,'image');
			$this->uploadImage($model,'licence_image');
			$this->uploadImage($model,'permit_image');
			$this->uploadImage($model,'address_image');
			$this->uploadImage($model,'adhaar_image');
			Users::emailNotifications("driver_added");
			Yii::$app->session->setFlash('flash_msg',"Driver detail added successfully.");
            return $this->redirect(['trucker/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Drivers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->uploadImage($model,'image');
			$this->uploadImage($model,'licence_image');
			$this->uploadImage($model,'permit_image');
			$this->uploadImage($model,'address_image');
			$this->uploadImage($model,'adhaar_image');
			Yii::$app->session->setFlash('flash_msg',"Driver detail updated successfully.");
            return $this->redirect(['trucker/index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Drivers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $record = $this->findModel($id);
        $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            Common::DelAllMedia($path);
        $record->delete();
        Yii::$app->session->setFlash('flash_msg',"Driver record deleted successfully.");
        return $this->redirect(['trucker/index']);
    }
	 /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $index){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $index);
		$this->moveFiles($model);
        if(!empty($image)){
		   $filename       = empty($model->$index)
			                 ?time() . "{$index}.".$image->extension:
			                  basename($model->$index);	
           $model->$index  = $filename;
           $image->saveAs($path.DS.$model->$index);
           $model->save(false);
	    }
	}
	private function moveFiles($model){ 
	  $path     = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true)."/";
	  $tmpath   = Common::Mediapath()."/tmp/";
	  foreach(['image','licence_image'] as $image){
		if(file_exists($tmpath.$model->$image)){
		   if(@copy($tmpath.$model->$image,$path.$model->$image))
			  @unlink($tmpath.$model->$image);  
		}  
	  }	
	}
	
	
	
	
	

    /**
     * Finds the Drivers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Drivers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drivers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
