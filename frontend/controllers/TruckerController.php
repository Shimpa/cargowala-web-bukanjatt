<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Users;
use common\models\Common;
use common\models\VehiclesSearch;
use common\models\DriversSearch;
use common\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * TruckerController implements the CRUD actions for Users model.
 */
class TruckerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''], //Put the actions here which you don't want to authenticate
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index','update'], //Put the actions here which you want to authenticate
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ], 
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VehiclesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$drivers_searchModel = new DriversSearch();
        $drivers_dataProvider = $drivers_searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'drivers_searchModel' => $drivers_searchModel,
            'drivers_dataProvider' => $drivers_dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$model->scenario = Users::SCENARIO_TRUCKER_UPDATE;
        unset($model->password); 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->uploadImage($model,'image');
			$this->uploadImage($model,'business_logo');
			Yii::$app->session->setFlash('flash_msg',"Record updated successfully.");
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
	 /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $key){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $key);
        if(!empty($image)){                    
            if($key=="business_logo"){
			   (empty($model->oldAttributes['business'][$key]) && $filename = time().".".$image->extension) ||                             $filename = $model->oldAttributes['business'][$key];
			}else{
			   (empty($model->oldAttributes[$key]) && $filename = time().".".$image->extension) ||                             $filename = $model->oldAttributes[$key];				
			}        
                    $model->$key  = $filename;
                    $image->saveAs($path.DS.$model->$key);
			        unset($model->password);
                    $model->save(false);
	    }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('flash_msg',"Record deleted successfully.");
        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
