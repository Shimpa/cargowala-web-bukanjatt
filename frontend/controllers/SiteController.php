<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\Users;
use common\models\User;
use common\models\Common;
use common\models\States;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class SiteController extends Controller{
    
    public function beforeAction($action){
        if($action->id == 'uploadwebcam'|| $action->id == 'l0dc0mrl_'){
            return true;
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
	/**
	* @inheritdoc
	*/
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup', 'validate-email', 'paddress', 'site-states', 'content-page', 'bill-generator'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','business-address','uploadwebcam'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'content-page' => ['post'],
                ],
            ],
        ];
    }
	
	public function actionBillGenerator(){
		$access_key = "6Y5LPDN3TVJ99YSJDDKN"; //put your own access_key - found in admin panel     
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76"; //put your own secret_key - found in admin panel     
		$return_url = $_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		// $return_url = "https://sandbox.citruspay.com/mmes2u0c7r"; // SandBox Url.
		$txn_id = time() . rand(10000,99999);    
		$value = isset($_REQUEST["amount"]) ? $_REQUEST["amount"] : ''; //Charge amount is in INR by default
		// $value = 1550; //Charge amount is in INR by default
		$data_string = "merchantAccessKey=" .$access_key. "&transactionId=" .$txn_id. "&amount=" . $value;    
		$signature = hash_hmac('sha1', $data_string, $secret_key);    
		$amount = array('value' => $value, 'currency' => 'INR');    
		$bill = array(
			'merchantTxnId' => $txn_id,      
			'amount' => $amount,        
			'requestSignature' => $signature,         
			'merchantAccessKey' => $access_key,        
			'returnUrl' => $return_url
		);     
		echo json_encode($bill);
	}

    /**
    * @inheritdoc
    */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex(){
        return $this->redirect(['site/login']);
		return $this->render('index');
    }

	/**
	* Logs in a user.
	*
	* @return mixed
	*/
    public function actionLogin(){
		$this->layout = "login";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['trucker/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

	/**
	* Logs out the current user.
	*
	* @return mixed
	*/
    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }

	/**
	* Displays contact page.
	*
	* @return mixed
	*/
    public function actionContact(){
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

	/**
	* Displays about page.
	*
	* @return mixed
	*/
	public function actionAbout(){
        return $this->render('about');
    }

    /**
    * Signs user up.
    *
    * @return mixed
    */
    public function actionSignup(){
        $model = new Users();
		$model->scenario  = Users::SCENARIO_TRUCKER_CREATE;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->hash_token = empty($model->email) ? NULL : md5($model->email.time());
			$model->role = 1;
			$model->save() && $model->sendVerificationMail();
			$this->uploadImage($model,'image');
			Yii::$app->session['trucker_id']  = $model->_id->__toString();
			return $this->redirect(['site/business-address']);
        }else{ 
            return $this->render('signup', [
                'model' => $model,
            ]);	
		}
    }
    
	public function actionBusinessAddress(){
		if(empty(Yii::$app->session['trucker_id']))
		   return $this->redirect(['site/signup']);	
		$model = Users::findOne(['_id'=>Yii::$app->session['trucker_id']]);
		$model->scenario  = Users::SCENARIO_TRUCKER_ADDRESS;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->image = basename($model->image);
			unset($model->password);
			$model->save() && $this->uploadImage($model,'business_logo');
			Yii::$app->session->setFlash('email_confirm','Email Verification  link sent on registed email.Please confirm your email before login.');
			return $this->redirect(['site/login']);
        }else{
            return $this->render('business-address', [
                'model' => $model,
            ]);	
		}
	}
  /*
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $key){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $key);
        if(!empty($image)){                    
            if($key=="business_logo"){
			   (empty($model->oldAttributes['business'][$key]) && $filename = time().".".$image->extension) || $filename = $model->oldAttributes['business'][$key];
			}else{
			   (empty($model->oldAttributes[$key]) && $filename = time().".".$image->extension) || $filename = $model->oldAttributes[$key];				
			}
            $model->$key  = $filename;
            $image->saveAs($path.DS.$model->$key);
            unset($model->password);
            $model->save(false);
	    }
    }
    
	/* get indian states list ..*/
	public function actionStatesList(){
		$states = States::find()->select(['_id','name'])->where(['status'=>1])->all();
		$states = array_map(create_function('$ar',' return ["_id"=>$ar->_id->__toString(),"name"=>$ar->name];'),$states);
		Common::encodeJSON($states,false);
	}
	
    /**
    * Requests password reset.
    *
    * @return mixed
    */
    public function actionRequestPasswordReset(){
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
    * Resets password.
    *
    * @param string $token
    * @return mixed
    * @throws BadRequestHttpException
    */
    public function actionResetPassword($token){
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	/* validate user by email check user email is active or fake*/
    //Needs to be change (redirect to Login Page When New Design Layout is done)
	public function actionValidateEmail(){
		$this->layout = "login";
        $model = new LoginForm();
        // $model = Users::find()->where(['_id' => yii::$app->request->get('id')])->one();
        $dataModel = Users::find()->where(['hash_token' => (string)yii::$app->request->get('token')])->one();
        if (!empty($dataModel)) {
            $dataModel->image = basename($dataModel->image);
            unset($dataModel->password);
            $dataModel->hash_token = '';
            $dataModel->status = 1;
            if($dataModel->save(false)){
                $type = 'success';
                Yii::$app->session->setFlash($type, 'You have successfully verified your email address!');
            }
        }else{
            $type = 'danger';
            Yii::$app->session->setFlash($type, 'You have already verified your email address or invalid token!');
        }
        return $this->render('validate_email', [
            'model' => $model,
            'type' => $type,
        ]);
	}	
    
	/* Reset User's Password using User Id and user hash_token */
	public function actionPasswordReset(){
        $model = Users::find()->where(['_id' => yii::$app->request->get('id')])->one();
        $model->scenario = Users::SCENARIO_RESETPASSWORD;
        $type = '';
        if(!empty($_POST['Users']['password'])){
            $dataModel = Users::find()->where(['_id' => yii::$app->request->get('id'), 'hash_token' => (string)yii::$app->request->get('token')])->one();
            if(!empty($dataModel)){
                $dataModel->hash_token = '';
				$dataModel->load(['Users' => $dataModel->oldAttributes]);
                $dataModel->password = md5($_POST['Users']['password']);
                if($dataModel->save(false)){
                    $type = 'success';
                    Yii::$app->session->setFlash($type, 'You have successfully changed your password!');
                }
            } else{
                $type = 'danger';
                Yii::$app->session->setFlash($type, 'You have already reset your password or invalid token!');
            } 
        }
        return $this->render('passwordReset', [
            'model' => $model,
            'type' => $type,
        ]);
	}
    
	public function actionUploadwebcam(){
	    $fp   = file_get_contents('php://input');
		$name = time().".jpg";
		$path = Common::Mediapath()."/tmp/".$name;
		file_put_contents($path,$fp);
		die($name);
	}
	
    public function actionContentPage(){
        $pageData = '';
        $model = CmsPages::find()->where(['alias' => Yii::$app->request->post('alias'), 'status'=>1])->one();
        empty($model) and ModuleCommon::send(ModuleCommon::HTTP_NO_CONTENT);
        $lang = Yii::$app->request->post('lang');
        if(!empty($lang)){
            if(array_key_exists($lang, $model->content)){
                $pageData = [
                    'title' => $model->title[$lang],
                    'content' => $model->content[$lang],
                    'alias' => $model->alias,
                ];
            }else{
                $pageData = [
                    'title' => $model->title['en'],
                    'content' => $model->content['en'],
                    'alias' => $model->alias,
                ];             
            }
        }
        ModuleCommon::send(ModuleCommon::HTTP_SUCCESS, $pageData); 
    }


}
