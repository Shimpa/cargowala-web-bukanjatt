<?php
class Test{

	/* public function BillGenerator(){
		$access_key = "6Y5LPDN3TVJ99YSJDDKN"; //put your own access_key - found in admin panel     
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76"; //put your own secret_key - found in admin panel     
		$return_url = $_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		// $return_url = "https://sandbox.citruspay.com/mmes2u0c7r"; // SandBox Url.
		$txn_id = time() . rand(10000,99999);    
		$value = isset($_REQUEST["amount"]) ? $_REQUEST["amount"] : ''; //Charge amount is in INR by default
		// $value = 1550; //Charge amount is in INR by default
		$data_string = "merchantAccessKey=" .$access_key. "&transactionId=" .$txn_id. "&amount=" . $value;    
		$signature = hash_hmac('sha1', $data_string, $secret_key);    
		$amount = array('value' => $value, 'currency' => 'INR');    
		$bill = array(
			'merchantTxnId' => $txn_id,      
			'amount' => $amount,        
			'requestSignature' => $signature,         
			'merchantAccessKey' => $access_key,        
			'returnUrl' => $return_url
		);     
		echo json_encode($bill);
	} */
	
	public function BillGenerator(){
		$access_key = "6Y5LPDN3TVJ99YSJDDKN"; //put your own access_key - found in admin panel     
		$secret_key = "e43f840797fd9ae0956d5191263a56559bcc0a76"; //put your own secret_key - found in admin panel     
		$return_url = "http://".$_SERVER['HTTP_HOST'].'/dev/citruspay.php'; //put your own return_url.php here.
		//$notify_url = "https://salty-plateau-1529.herokuapp.com/notifyUrl.sandbox.php"; //put your own notify_url.php here.
		$ruleName = $_REQUEST["ruleName"];
		$operation = $_REQUEST["dpOperation"];
		$altered_amount_value = $_REQUEST["alteredAmount"];
		$txn_id = time() . rand(10000,99999);
		if (!empty($_REQUEST["txn_id"])) {
			$txn_id = $_REQUEST["txn_id"];
		}
		$value = $_REQUEST["amount"]; //Charge amount is in INR by default
		$data_string = "merchantAccessKey=" . $access_key . "&transactionId=" .$txn_id . "&amount=" . $value;
		$data_string_dp = ""; 
		$customParams = array('param1' => "1000", 'param2' => "CitrusTestSDK");
		if (strcmp($operation, "searchAndApply") == 0) {
			$data_string_dp = $value."INR".$txn_id;
		} else if (strcmp($operation, "calculatePricing") == 0) {
			$data_string_dp = $ruleName.$value."INR".$txn_id;
		} else if (strcmp($operation, "validateRule") == 0) {
			$data_string_dp = $value.$altered_amount_value.$ruleName.$txn_id;
		}
		$signature = hash_hmac('sha1', $data_string, $secret_key);
		$dpsignature = hash_hmac('sha1', $data_string_dp, $secret_key);
		$amount = array('value' => $value, 'currency' => 'INR');
		$alteredAmout = array('value' => $altered_amount_value, 'currency' => 'INR');
		$bill = array(
			'merchantTxnId' => $txn_id, 
			'amount' => $amount, 
			'requestSignature' => $signature, 
			'merchantAccessKey' => $access_key, 
			'returnUrl' => $return_url,
			'notifyUrl' => $notify_url,
			'customParameters' => $customParams,
			'alteredAmout' => $alteredAmout,
			'dpSignature'=> $dpsignature
		);
		echo json_encode($bill);
	}
		
}

$className = new Test();
$className->BillGenerator('120');
?>