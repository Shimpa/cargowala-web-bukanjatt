<?php
$vendorDir  =dirname(dirname(__DIR__)) . '/vendor';
return array (
  'yiisoft/yii2-mongodb-master' => 
  array (
    'name' => 'yiisoft/yii2-mongodb-master',
    'version' => '~2.0.0',
    'alias' => 
    array (
      '@yii/mongodb' => $vendorDir . '/yiisoft/yii2-mongodb-master',
    ),
  ),
   /* 'yiisoft/authclient' => 
  array (
    'name' => 'yiisoft/authclient',
    'version' => '~2.0.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/authclient',
    ),
  ),*/
  'yiisoft/yii2-jui-master' => 
  array (
    'name' => 'yiisoft/yii2-jui-master',
    'version' => '~2.0.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui-master',
    ),
  ),
  'yiisoft/yii2-admin-2.2' => 
  array (
    'name' => 'yiisoft/yii2-admin-2.2',
    'version' => '~2.0.0',
    'alias' => 
    array (
      '@yii/yii2admin' => $vendorDir . '/yiisoft/yii2-admin-2.2',
    ),
  ),
  'yiisoft/yii2-tcpdf' => 
  array (
    'name' => 'yiisoft/yii2-tcpdf',
    'version' => '~2.0.0',
    'alias' => 
    array (
      '@cinghie/tcpdf' => $vendorDir . '/yiisoft/yii2-tcpdf',
    ),
  ),	

);