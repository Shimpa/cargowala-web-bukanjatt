<?php
$vendorDir = dirname(dirname(__DIR__)) . '/vendor';
return [
    'vendorPath' => $vendorDir,
    'name'=>'CargoWala',
    'extensions' => array_merge(
        require($vendorDir . '/yiisoft/extensions.php'),
        require(__DIR__ . '/extensions.php')      
    ),
    'language'=>'en',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		// Yii2 TCPDF
        'tcpdf' => [
            'class' => 'cinghie\tcpdf\TCPDF',
        ],
        /*'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '../../common/messages',
                    // 'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                     'class' => 'yii\authclient\clients\GoogleOAuth',
                     'clientId' => '808194064157-hj0gjur32eqbps36pgsnvdoqsn09rl6c.apps.googleusercontent.com',
                     'clientSecret' => 'PL_8ZinKufA1dFoULN4SSaYk',
                 ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '940644399366146',
                    'clientSecret' => '4789308401f6d588e5a908fcde5528e5',
                ],
            ],
        ],*/
        'urlManagerFrontend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '/testrepo/frontend/web',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],        
    ],
    'modules' => [
		'gii' => [
			'class' => 'yii\gii\Module',
			'generators' => [
				'mongoDbModel' => [
					'class' => 'yii\mongodb\gii\model\Generator'
				]
			],
		],
	],
];