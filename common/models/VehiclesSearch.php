<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Vehicles;

/**
 * VehiclesSearch represents the model behind the search form about `common\models\Vehicles`.
 */
class VehiclesSearch extends Vehicles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[['truck_category', 'truck_type', 'vehicle_number', 'loading_capacity', 'pic', 'pnumber', 'permit_issue_date', 'permit_expiry_date','rc_expiry_date', 'owner_id', 'created_by', 'status', 'truck_status', 'is_available', 'created_on', 'modified_on', 'issue_date', 'renew_date', 'rc_pic', 'rc_number', 'compliance'], 'safe'],			
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vehicles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $is_available = $this->is_available;
        if(isset($this->is_available) && ($this->is_available != '')) $is_available = (int)$this->is_available;
        $truck_category = $this->truck_category;
        if(isset($this->truck_category) && ($this->truck_category != '')) $truck_category = (int)$this->truck_category;
        $truck_status = $this->truck_status;
        if(isset($this->truck_status) && ($this->truck_status != '')) $truck_status = (int)$this->truck_status;
        $query->andFilterWhere(['like', '_id', $this->_id])
			->andFilterWhere(['truck_category' => $truck_category])
			->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'vehicle_number', $this->vehicle_number])
            // ->andFilterWhere(['like', 'permit.state', $this->state])
            // ->andFilterWhere(['like', 'permit.number', $this->number])
            ->andFilterWhere(['like', 'registration', $this->registration])
            ->andFilterWhere(['like', 'insurance', $this->insurance])
            ->andFilterWhere(['like', 'owner_id', $this->owner_id])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['is_available' => $is_available])
            ->andFilterWhere(['truck_status' => $truck_status]);
            // ->andFilterWhere(['like', 'created_on', $this->created_on])
            // ->andFilterWhere(['like', 'modified_on', $this->modified_on]);
        return $dataProvider;
    }
}
