<?php
namespace common\models;
use Yii;
use common\models\Common;

/**
* This is the model class for collection "drivers".
*
* @property \MongoId|string $_id
* @property mixed $name
* @property mixed $date_of_birth
* @property mixed $mobile_no
* @property mixed $image
* @property mixed $truck_type
* @property mixed $permit
* @property mixed $address
* @property mixed $licence
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/
class Drivers extends \yii\mongodb\ActiveRecord{
    const PARENTDIR = '/drivers';
    const CHILDDIR = '/driver';
	
	public $licence_number, $licence_expiry, $licence_image, $permit_number, $permit_expiry, $permit_image, $firstname, $lastname, $driver_address,$driver_city, $driver_state, $driver_pin_code, $address_image, $adhaar_no, $adhaar_image, $lat, $lng, $place, $updated_on, $update_location, $dob;
	
	const SCENARIO_ADMINCREATE = 'admin_create';
	const SCENARIO_ADMINUPDATE = 'admin_update';
	
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'drivers'];
    }

    /**
     * @inheritdoc
     */
    public function attributes(){
        return [
            '_id',
            'name',
            'mobile_no',
           // 'username',
            'password',
            'image',
            'truck_type',
            'permit',
            'address',
            'licence',
            'status',
            'driver_status',
            'is_available',
			'location',
            'owner_id',
            'created_by',
            'compliance',
            'date_of_birth',
            'created_on',
            'modified_on',
            'device',
            'firstname',
            'lastname',
        ];
    }
	
    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADMINCREATE] = ['firstname', 'lastname', 'image', 'date_of_birth', 'driver_address', 'driver_city', 'driver_state', 'driver_pin_code',  'mobile_no', 'truck_type', 'password', 'adhaar_image', 'adhaar_no', 'owner_id', 'licence_number', 'licence_expiry', 'licence_image', 'is_available', 'status'];
        $scenarios[self::SCENARIO_ADMINUPDATE] = ['firstname', 'lastname', 'date_of_birth', 'driver_address', 'driver_city', 'driver_state', 'driver_pin_code',  'mobile_no', 'truck_type',  'adhaar_no', 'owner_id', 'licence_number', 'licence_expiry', 'is_available', 'status'];
        return $scenarios;
    } 		

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['date_of_birth', 'firstname', 'lastname', 'image', 'date_of_birth', 'driver_address', 'driver_city', 'driver_state', 'driver_pin_code',  'mobile_no', 'truck_type', 'adhaar_image', 'adhaar_no', 'owner_id', 'licence_number', 'licence_expiry', 'licence_image', 'is_available', 'status', 'password'], 'required'],
			[['firstname','lastname'], 'string', 'length' => [2, 16]],
			[['driver_city', 'driver_state'], 'string', 'length' => [2, 40]],
			[['licence_number'], 'string', 'length' => [4, 25]],
			[['permit_number'], 'string', 'length' => [10, 30]],
			[['adhaar_no'], 'string', 'length' => [12, 12]],
			 [['mobile_no'], 'unique'],
			[['mobile_no'], 'string', 'length' => [10, 13]],
			[['mobile_no','adhaar_no'], 'number'],
			[['driver_pin_code'], 'integer','message'=>'Pincode must be numeric value'],
            [['driver_pin_code'], 'string', 'length' => [6,8]],
			[['driver_pin_code'], 'match', 'pattern' => '/^[0-9]{6,8}$/'],
			[['firstname', 'lastname'], 'match', 'pattern' => '/^[a-z A-Z]+$/','message'=>"Only alphabets are allowed in name"],
			[['driver_address'], 'string', 'length' => [5, 120]],
			[['permit_image'], 'required','when'=>function($model){
				return !empty($model->permit_image);
			},'whenClient' => "function (attribute, value) {
				return ($('#drivers-permit_image').get(0).files.length ==0 && $('#drivers-permit_image').prev('input').val()=='');
			}"],
			[['licence_image'], 'required','when'=>function($model){
				return !empty($model->licence_image);
			},'whenClient' => "function (attribute, value) {
			return ($('#drivers-licence_image').get(0).files.length ==0 && $('#drivers-licence_image').prev('input').val()=='');
			}"],
			[['image'], 'required','when'=>function($model){
				return !empty($model->image);
			},'whenClient' => "function (attribute, value) {
			return ($('#drivers-image').get(0).files.length ==0 && $('#drivers-image').prev('input').val()=='');
			}"],
			[['adhaar_image'], 'required','when'=>function($model){
				return !empty($model->adhaar_image);
			},'whenClient' => "function (attribute, value) {
				return ($('#drivers-adhaar_image').get(0).files.length ==0 && $('#drivers-adhaar_image').prev('input').val()=='');
			}"],
			[['address_image'], 'required','when'=>function($model){
				return !empty($model->address_image);
			},'whenClient' => "function (attribute, value) {
				return ($('#drivers-address_image').get(0).files.length ==0 && $('#drivers-address_image').prev('input').val()=='');
			}"],
			[['image', 'permit_image', 'adhaar_image', 'address_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg'],	
			[['name', 'licence_number', 'licence_expiry', 'permit_number', 'permit_expiry',	'firstname', 'lastname', 'driver_address', 'driver_city', 'driver_state', 'driver_pin_code', 'adhaar_no',  'mobile_no', 'password', 'image', 'truck_type', 'adhaar_image', 'permit', 'address', 'licence', 'status', 'driver_status', 'is_available', 'owner_id', 'created_on', 'modified_on', 'compliance','date_of_birth', 'dob', 'device'], 'safe']
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeType(){
        return [
            'integer' => ['status', 'driver_status', 'is_available', 'mobile_no', 'truck_type']
		];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            '_id' => 'ID',
            'name' => 'Name',
            'date_of_birth' => 'Date Of Birth',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'mobile_no' => 'Mobile Number',
            'image' => 'Image',
            'truck_type' => 'Truck Type',
            'permit' => 'Permit',
            'address' => 'Address',
            'licence' => 'Licence',
            'status' => 'Status',
            'created_by' => 'Owner',
            'compliance' => 'Compliance',
            'created_on' => 'Created On',
            'modified_on' => 'Modified On',
        ];
    }
	 
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			// echo '<pre>'; print_r($this); die('sa');
			$this->name    = [
				'firstname' => $this->firstname,
				'lastname' => $this->lastname,				
			];
			if($this->image) $this->image = basename($this->image);
			else $this->image = basename(@$this->oldAttributes['image']);
			if($this->adhaar_image) $this->adhaar_image = basename($this->adhaar_image);
			else $this->adhaar_image = basename(@$this->oldAttributes['address']['adhaar_image']);
			$this->licence = [
				'number' => $this->licence_number,
				// 'expiry' => $this->licence_expiry,
				'expiry' => Common::convertDate($this->licence_expiry),
				'image' => basename($this->licence_image),  
			];
			/* $this->permit  = [
			   'number'=>$this->permit_number,
			   'expiry'=>$this->permit_expiry,
			   'image' =>basename($this->permit_image), 	                
			]; */
			$this->address = [
			   'address'=>$this->driver_address,
			   'city'=>$this->driver_city,
			   'state'=>$this->driver_state,
			   'pin_code'=>$this->driver_pin_code,
			   'address_image'=>basename($this->address_image),
			   'adhaar_no'=>$this->adhaar_no,
			   'adhaar_image'=>basename($this->adhaar_image),
    		];
			if(!empty($this->update_location)){
				$this->location = [
					'lat' => empty($this->lat) ? "" : $this->lat,
					'lng' => empty($this->lng) ? "" : $this->lng,
					'place' => empty($this->place) ? "" : $this->place,
					'updated_on' => time()*1000,
				];
			}
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if($this->isNewRecord){
				$id = !empty(Yii::$app->request->post('_id')) ? Yii::$app->request->post('_id') :  Yii::$app->user->getId();
				$this->created_by =  new \MongoId($id);
				if(!empty(Yii::$app->request->post('_id'))) $this->owner_id = new \MongoId($id);
				if(!empty($this->owner_id)) $this->owner_id = new \MongoId($this->owner_id);
			}else{
				$this->modified_on = Common::currentTimeStamp();
				$this->created_by =  $this->oldAttributes['created_by'];
				$this->owner_id =  $this->oldAttributes['owner_id'];
			}		
			if(!empty($this->dob)) $this->date_of_birth = Common::convertDate($this->dob);
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();      
		$this->firstname = @$this->name['firstname'];
		$this->lastname = @$this->name['lastname'];
		$this->image = empty($this->image)? null : Common::onlyMediaPath($this) . $this->image; 
		$this->licence_number = @$this->licence['number'];
		$this->licence_expiry = @Common::showDateFrontend($this->licence['expiry']);
		$this->licence_image = empty($this->licence['image'])? null: Common::onlyMediaPath($this) . $this->licence['image'];  
		/* $this->permit_number = @$this->permit['number'];  
		$this->permit_expiry = @$this->permit['expiry'];  
		$this->permit_image = empty($this->permit['image'])?null: Common::onlyMediaPath($this) . $this->permit['image'];   */
		$this->driver_address = @$this->address['address'];
		$this->driver_city = @$this->address['city'];
		$this->driver_state = @$this->address['state'];
		$this->lat = @$this->location['lat'];
		$this->lng = @$this->location['lng'];
		$this->place = @$this->location['place'];
		$this->updated_on = @$this->location['updated_on'];
		$this->driver_pin_code = @$this->address['pin_code'];
		$this->owner_id = Common::getOwnerName($this->owner_id);		
		$this->address_image = empty($this->address['address_image'])?null: Common::onlyMediaPath($this) . $this->address['address_image'];
		$this->adhaar_no = @$this->address['adhaar_no'];
		$this->adhaar_image = empty($this->address['adhaar_image'])?null: Common::onlyMediaPath($this) . $this->address['adhaar_image'];
		// $this->created_by = Common::getOwner($this->created_by);
		$this->modified_on = @Common::showDate($this->modified_on);
		$this->created_on = @Common::showDate($this->created_on);
		$this->date_of_birth = @Common::showDateFrontend($this->date_of_birth);
		$this->dob = @Common::showDateFrontend($this->date_of_birth);
        if(Yii::$app->controller->action->id != "update"){
            $this->status = $this->status ? 'Active' : 'Inactive';
            if($this->driver_status == 0) $this->driver_status =  'Not Assigned'; 
            if($this->driver_status == 1) $this->driver_status =  'Assigned'; 
            if($this->driver_status == 2) $this->driver_status =  'In-Transit'; 
            $this->is_available = $this->is_available ? 'Available' : 'Not Available'; 
        }
    }

	public function getName($id){
		$truck =Trucks::find()->select(['name'])->where(['_id'=>$id])->one();
		return empty($truck->name)?null:$truck->name;
	}
	
	public function getOwner(){
        return $this->hasOne(Users::className(), ['_id' => 'owner_id']);
    }
	/* get driver name and no by id */
	public function getNameandNo($id){
		try{
		$driver = Drivers::find()->select(['name','mobile_no'])->where(['_id'=>$id])->one();
		if(empty($driver))return;	
		return $driver->name['firstname']. " ".$driver->name['lastname']."<br/> Ph No.".
			   $driver->mobile_no;
		}catch(Exception $e){}
	}
	
}