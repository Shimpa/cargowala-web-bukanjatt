<?php
namespace common\models;
use Yii;
use yii\helpers\ArrayHelper;
/**
* This is the model class for collection "topics".
* @property \MongoId|string $_id
* @property mixed $title
* @property mixed $description
* @property mixed $created_on
* @property mixed $modified_on
* @property mixed $status
*/
class Topics extends \yii\mongodb\ActiveRecord{
    public $en_title;
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
		return [Yii::$app->mongodb->defaultDatabaseName, 'topics'];
    }

	/**
	* @inheritdoc
	*/
    public function attributes(){
        return [
            '_id',
            'title',
            'description',
            'created_on',
            'modified_on',
            'status',
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
             'integer' => ['status']
        ];
    }	
	
	/**
	* @inheritdoc
	*/
	public function rules(){
        return [
            [['title', 'description', 'status'], 'required'],
            [['title', 'description', 'created_on', 'modified_on', 'status'], 'safe']
        ];
    }

	/**
	* @inheritdoc
	*/
	public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();		
            return true;
        }else{
            return false;
        }
		// echo '<pre>'; print_r($insert); die('beforeSave');
    }	
	
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id != "update")
            $this->status = $this->status ? 'Active' : 'Inactive';
        $this->en_title = empty($this->title['en']) ? '' : $this->title['en'];
        // $this->description = empty($this->description['en']) ? '' : $this->description['en'];
        $this->created_on = Common::showDate($this->created_on);
        $this->modified_on = Common::showDate($this->modified_on);
    }

    public static  function getTopics(){
        $res = Topics::find()->select(['_id','title'])->where(['status'=>1])->all();
        list($id,$name) = ['_id','en_title'];
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);
    }
	
	
	/* Function to get the Ownner Name*/
    public function getTopicName($id){
        $result = Topics::findOne($id);
        if($result['en_title']) return $result['en_title'];
            return $id = '';
    }	
}