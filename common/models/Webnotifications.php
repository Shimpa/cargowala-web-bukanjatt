<?php
namespace common\models;

use Yii;

/**
* This is the model class for collection "webnotifications".
*
* @property \MongoId|string $_id
* @property mixed $message
* @property mixed $created_on
* @property mixed $read
* @property mixed $can_view
*/
class Webnotifications extends \yii\mongodb\ActiveRecord{
	
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'webnotifications'];		
    }

	/**
	* @inheritdoc
	*/
    public function attributes(){
        return [
            '_id',
            'message',
            'created_on',
            'read',
            'can_view',
        ];
    }

	/**
	* @inheritdoc
	*/
    public function rules(){
        return [
            [['message', 'created_on', 'read', 'can_view'], 'safe']
        ];
    }

	/**
	* @inheritdoc
	*/
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'message' => Yii::t('app', 'Message'),
            'created_on' => Yii::t('app', 'Created On'),
            'read' => Yii::t('app', 'Read'),
            'can_view' => Yii::t('app', 'Can View'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->created_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
	
    public function afterFind(){
        parent::afterFind();
		$this->created_on = Common::showDate($this->created_on);
    }	
	
}