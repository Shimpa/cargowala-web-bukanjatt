<?php
namespace common\models;
use Yii;

/**
 * This is the model class for collection "queue".
 *
 * @property \MongoId|string $_id
 * @property mixed $member
 * @property mixed $message
 * @property mixed $status
 * @property mixed $action
 * @property mixed $subject
 * @property mixed $priority
 */
class Queue extends \yii\mongodb\ActiveRecord{

    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'queue'];
    }
	
    /**
     * @inheritdoc
     */
    public function attributes(){
        return [
            '_id',
            'member',
            'message',
            'status',
            'action',
            'subject',
            'priority',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member', 'message', 'status', 'action', 'subject', 'priority'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['status', 'priority']
        ];
    }	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'member' => Yii::t('app', 'Member'),
            'message' => Yii::t('app', 'Message'),
            'status' => Yii::t('app', 'Status'),
            'action' => Yii::t('app', 'Action'),
            'subject' => Yii::t('app', 'Subject'),
            'priority' => Yii::t('app', 'Priority'),
        ];
    }
}
