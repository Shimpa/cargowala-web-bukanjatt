<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaxTerms;

/**
 * TaxTermsSearch represents the model behind the search form about `common\models\TaxTerms`.
 */
class TaxTermsSearch extends TaxTerms
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'name', 'ttype', 'rate', 'priority', 'created_by', 'status', 'created_on', 'modified_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaxTerms::find();

        /* $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    '_id' => SORT_DESC,
                ]
            ],           
        ]); */
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $ttype = $this->ttype;
        if(isset($this->ttype) && ($this->ttype != '')) $ttype = (int)$this->ttype;
        $query->andFilterWhere(['like', '_id', $this->_id])
            //->andFilterWhere(['like', 'term', $this->term])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'rate', $this->rate])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['ttype' => $ttype])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on]);
        return $dataProvider;
    }
}
