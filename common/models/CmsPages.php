<?php
namespace common\models;
use common\models\Languages;
use Yii;

/**
* This is the model class for collection "cms_pages".
*
* @property \MongoId|string $_id
* @property mixed $title
* @property mixed $alias
* @property mixed $content
* @property mixed $image
* @property mixed $created_by
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/

class CmsPages extends \yii\mongodb\ActiveRecord{
    public $en_title;
    /**
     * @inheritdoc
     */
	public static function collectionName(){
		return [Yii::$app->mongodb->defaultDatabaseName, 'cms_pages'];
    }

    /**
     * @inheritdoc
     */
    public function attributes(){
        return [
            '_id',
            'title',
            'alias',
            'content',
            'image',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['title', 'content', 'status'], 'required'],
			// [['title'], 'unique', 'targetAttribute' => ['title.en', 'title.hi.']],
            [['title', 'alias', 'content', 'image', 'created_by', 'status', 'created_on', 'modified_on'], 'safe'],
        ];
    }
    
    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [ 'integer' => 'status' ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'alias' => Yii::t('app', 'Alias'),
            'content' => Yii::t('app', 'Content'),
            'image' => Yii::t('app', 'Image'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->alias = preg_replace("/[^a-zA-Z ]/", '', $this->title['en']);
            $this->alias = str_replace(' ','-',strtolower($this->alias));
			if($this->isNewRecord){
				$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
				$this->created_by = new \MongoId(Yii::$app->user->getId());				
			}else{
				$this->modified_on = Common::currentTimeStamp();
			}			
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id != "update"){
			$this->status = $this->status ? 'Active' : 'Inactive';
			$this->en_title = empty($this->title['en']) ? '' : $this->title['en'];
			//$this->content = empty($this->content['en']) ? '' : $this->content['en'];
			$this->created_by = Common::getOwner($this->created_by);
			$this->created_on = Common::showDate($this->created_on);
			$this->modified_on = Common::showDate($this->modified_on);
		}
    }

    public function getLanguages(){
       return $languages = Languages::find()->select(['_id','language_code','language_name'])->where(['status'=>1])->all();
    }
    
}