<?php
namespace common\models;
use common\models\Common;
use Yii;
/**
 * This is the model class for collection "trucks".
 *
 * @property \MongoId|string $_id
 * @property mixed $truck_type
 * @property mixed $name
 * @property mixed $desc
 * @property mixed $loading_capacity
 * @property mixed $created_by
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class Trucks extends \yii\mongodb\ActiveRecord{
    
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'trucks'];
    }

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'truck_type',
            'name',
            'desc',
            'loading_capacity',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['truck_type', 'name', 'loading_capacity', 'status'], 'required'],
			[['name'], 'unique', 'targetAttribute' => 'name'],
            [['loading_capacity'], 'number'],
            [['loading_capacity'], 'string', 'length' => [1, 4]],
            [['truck_type', 'name', 'desc', 'loading_capacity', 'created_by', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['status', 'truck_type'],
            // 'float' => ['loading_capacity'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'truck_type' => Yii::t('app', 'Truck Type'),
            'name' => Yii::t('app', 'Truck Name'),
            'desc' => Yii::t('app', 'Description'),
            'loading_capacity' => Yii::t('app', 'Loading Capacity ( in Tons )'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			$this->created_by = empty($this->created_by) ? new \MongoId(Yii::$app->user->getId()) : $this->oldAttributes['created_by'];
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id != "update"){
            $this->truck_type = Common::getTruckTypes($this->truck_type);
            $this->status = $this->status ? 'Active' : 'Inactive';
        }
        $this->created_by = Common::getOwner($this->created_by);
        //$this->desc = empty($this->desc) ? 'N-A' : $this->desc;
        $this->created_on = Common::showDate($this->created_on);
        $this->modified_on = Common::showDate($this->modified_on);
    }
	
	public function getName($id){
		$truck =Trucks::find()->select(['name'])->where(['_id'=>$id])->one();
		return empty($truck->name)?null:$truck->name;
	}
	
}