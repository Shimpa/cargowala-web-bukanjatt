<?php
namespace common\models;
use Yii;
/**
* This is the model class for collection "users".
*
* @property \MongoId|string $_id
* @property mixed $name
* @property mixed $contact
* @property mixed $password
* @property mixed $hash_token
* @property mixed $image
* @property mixed $account_type
* @property mixed $company_type
* @property mixed $business_type
* @property mixed $business
* @property mixed $account
* @property mixed $pancard
* @property mixed $status
* @property mixed $role
* @property mixed $social
* @property mixed $created_on
* @property mixed $modified_on
*
*/
class Users extends \yii\mongodb\ActiveRecord{
    const PARENTDIR = '/users';
    const CHILDDIR = '/user';    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';    
    const SCENARIO_TRUCKER_CREATE = 'trucker_create';
    const SCENARIO_TRUCKER_ADDRESS = 'trucker_address';
    const SCENARIO_TRUCKER_UPDATE = 'trucker_update';
    const SCENARIO_AGENT_UPDATE = 'agent_update';
    const SCENARIO_TRUCKER_VIEW = 'trucker_view';
    const SCENARIO_TRUCKER_ADMINCREATE = 'trucker_admincreate';
    const SCENARIO_TRUCKER_ADMINUPDATE = 'trucker_adminupdate';
    const SCENARIO_SHIPPER_ADMINCREATE = 'shipper_admincreate';
    const SCENARIO_SHIPPER_ADMINUPDATE = 'shipper_adminupdate';
    const SCENARIO_SHIPPER_APICREATE   = 'shipper_apicreate';
    const SCENARIO_SHIPPER_APICREATEPROFILE   = 'shipper_apicreateprofile';
    const SCENARIO_SHIPPER_APIMOBILEOTP   = 'shipper_apimobileotp';
    const SCENARIO_SHIPPER_APISOCIALREG   = 'shipper_apisocialregister';
    const SCENARIO_RESETPASSWORD   = 'password_reset';
    public $confirm_password;
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'users'];
    }
    
    public $firstname, $lastname, $email, $mobile_number, $alternate_number, $business_name, $registered_address, $office_address, $landline, $business_pancard, $business_logo, $uname, $states, $business_service_taxno, $registered_city, $registered_state, $registered_pincode, $office_city, $office_state, $office_pincode, $c_type, $c_others, $b_type, $b_others, $ac_number, $holder_name, $bank_name, $branch_address, $branch_code, $ifsc_code,$checkbox;

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'name',
            'contact',
            'otp',
            'password',
            'mobile_tokens',
            'hash_token',
            'image',
            'account_type',
            'company_type',
            'business_type',
            'business',
            'account',
            'pancard',
            'status',
            'states',
            'role',
            'has_cp',
            'social',
            'created_on',
            'modified_on',
            'plocation',
            'compliance',
            'membership',
            'firstname',
            'lastname',
            'email',
            'mobile_number',
            'device',
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['firstname', 'lastname', 'email', 'password', 'mobile_number', 'has_cp', 'compliance', 'account_type', 'company_type', 'business_type', 'pancard'];
        $scenarios[self::SCENARIO_UPDATE] = ['firstname', 'lastname', 'email', 'mobile_number', 'has_cp', 'compliance', 'account_type', 'company_type', 'business_type', 'pancard', 'has_cp', 'business_name', 'landline', 'registered_address', 'registered_city', 'registered_state' , 'registered_pincode', 'office_address', 'office_city' , 'office_state', 'office_pincode', 'status'];
		 $scenarios[self::SCENARIO_SHIPPER_ADMINCREATE] = ['firstname', 'lastname', 'email', 'password', 'mobile_number', 'has_cp', 'compliance', 'account_type', 'company_type', 'business_type', 'pancard'];
        $scenarios[self::SCENARIO_SHIPPER_ADMINUPDATE] = ['firstname', 'lastname', 'email', 'mobile_number', 'has_cp', 'compliance', 'account_type', 'company_type', 'business_type', 'pancard'];
		$scenarios[self::SCENARIO_TRUCKER_CREATE]  = ['firstname', 'lastname', 'email', 'password','role', 'confirm_password', 'image', 'mobile_number', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'business_logo', 'business_service_taxno', 'registered_city', 'registered_state', 'registered_pincode', 'office_city', 'office_state', 'office_pincode', 'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code'];
		$scenarios[self::SCENARIO_TRUCKER_ADDRESS] = ['business_pancard', 'business_name', 'office_address', 'landline', 'registered_address', 'states', 'business_logo', 'business_service_taxno'];
		$scenarios[self::SCENARIO_TRUCKER_UPDATE] = ['_id','firstname', 'lastname',  'mobile_number','role', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'pancard', 'business_service_taxno', 'registered_city', 'registered_state', 'registered_pincode', 'office_city', 'office_state', 'office_pincode', 'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code', 'status'];
		$scenarios[self::SCENARIO_AGENT_UPDATE] = ['_id','firstname', 'lastname',  'mobile_number', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'pancard', 'business_logo', 'business_service_taxno', 'registered_city', 'registered_state', 'registered_pincode', 'office_city', 'office_state', 'office_pincode', 'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code', 'status', 'has_cp'];
		$scenarios[self::SCENARIO_TRUCKER_VIEW] = ['_id','firstname', 'lastname', 'image', 'mobile_number','role', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'business_logo', 'business_service_taxno', 'registered_city', 'registered_state', 'registered_pincode', 'office_city', 'office_state', 'office_pincode', 'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code'];
        $scenarios[self::SCENARIO_TRUCKER_ADMINCREATE]  = ['firstname', 'lastname', 'email', 'mobile_number', 'password', 'status', 'business_service_taxno', 'business_pancard', 'business_name', 'landline', 'registered_address','office_address'];
        $scenarios[self::SCENARIO_TRUCKER_ADMINUPDATE]  = ['firstname', 'lastname', 'email', 'mobile_number', 'status', 'business_pancard', 'business_service_taxno', 'business_name', 'landline', 'registered_address','office_address'];
		//api scenario...
		$scenarios[self::SCENARIO_SHIPPER_APICREATE]  = ['email','password','role','status','hash_token'];
		$scenarios[self::SCENARIO_SHIPPER_APISOCIALREG]  = ['email','role','status','hash_token'];
		$scenarios[self::SCENARIO_SHIPPER_APICREATEPROFILE]  = ['firstname', 'lastname', 'mobile_number', 'account_type'];
		$scenarios[self::SCENARIO_SHIPPER_APIMOBILEOTP]  = ['mobile_number'];
		$scenarios[self::SCENARIO_RESETPASSWORD]  = ['password', 'confirm_password'];
        return $scenarios;
    }    
    
    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'landline', 'account_type', 'company_type', 'business_type', 'status', 'role', 'confirm_password', 'states',  'pancard', 'password', 'has_cp', 'registered_address', 'registered_city', 'registered_state' , 'registered_pincode', 'office_address', 'office_city' , 'office_state', 'office_pincode', 'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code'], 'required'],
            [['mobile_number', 'alternate_number', 'landline'], 'integer','message'=>'Number must be numeric value like 9876543210, 01239876542'],
            [['email'], 'email'],
			[['firstname', 'lastname', 'holder_name', 'bank_name'], 'match', 'pattern' => '/^[a-z A-Z]+$/','message'=>"Only alphabets are allowed in name"],
            //[['password'], 'checkpassword'],
            /*[['password'], 'required', 'on'=>['SCENARIO_CREATE', 'SCENARIO_TRUCKER_CREATE', 'SCENARIO_TRUCKER_ADMINCREATE']],
            [['password'], 'checkpassword', 'on'=>['SCENARIO_UPDATE', 'SCENARIO_TRUCKER_UPDATE', 'SCENARIO_TRUCKER_ADMINUPDATE']],*/
            [['confirm_password'], 'compare', 'compareAttribute' => 'password'],
            [['email'], 'unique','targetAttribute'=>'contact.email'],
			[['firstname','lastname', 'holder_name'], 'string', 'length' => [2, 25]],
			//[['password'], 'string', 'length' => [6, 20]],
			[['pancard','business_pancard'], 'string', 'length' => [10, 10]],
			/*[['business_service_taxno'], 'required', 'when'=>function($model){
				return !(strlen($model->business_service_taxno)<15);
			},'whenClient' => "function (attribute, value) {
            console.log(attribute);
                return ($(attribute.input).val().length<15);
            }",'message'=>'Business Service Taxno should contain at least 15 characters.'],*/
			/*[['password'], 'required', 'when'=>function($model){
				// return (strlen($model->password)<6);
				return !($this->isNewRecord);
			},'whenClient' => "function (attribute, value) {
                // console.log(attribute);
                return ($(attribute.input).val().length > 6 || $(attribute.input).val().length < 15);
            }",'message'=>'Password should contain at least 6 characters.'],*/
			//[['business_service_taxno'], 'string', 'length' => [15, 15]],
			[['pancard','business_pancard'], 'match', 'pattern' => '/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]{10,10}$/','message'=>"Business Pancard should be alpha numeric and of 10 characters length."],
			[['business_service_taxno'], 'match', 'pattern' => '/^(?=.*\d)(?=.*[A-Za-z])[A-Za-z0-9]{15,15}$/','message'=>"Business Service Taxno should be alpha numeric and of 15 characters length."],
			[['business_name'], 'string', 'length' => [2, 50]],
			[['registered_address', 'office_address'], 'string', 'length' => [5, 120]],
			[['registered_city', 'registered_state', 'office_city' , 'office_state'], 'string', 'length' => [2, 40]],
            //[['mobile_number','landline'], 'match', 'pattern' => '/^([+]?[0-9 ]+)$/', 'message'=>"Number should be numeric and can contain + , - "],
            [['mobile_number', 'alternate_number'], 'string', 'length' => [10, 10]],
            [['landline', 'ifsc_code'], 'string', 'length' => [7, 15]],
			[['ac_number'], 'integer', 'message'=>'Account number must be a numeric value'],
            [['ac_number'], 'string', 'length' => [8,15]],
            [['ac_number'], 'match', 'pattern' => '/^[0-9]{8,15}$/'],
            [['mobile_number', 'alternate_number'], 'match', 'pattern' => '/^[0-9]{10,10}$/'],
			[['registered_pincode', 'office_pincode'], 'integer','message'=>'Pincode must be numeric value'],
            [['registered_pincode', 'office_pincode'], 'string', 'length' => [6,8]],
			[['registered_pincode', 'office_pincode'], 'match', 'pattern' => '/^[0-9]{6,8}$/'],
            // [['registered_address','office_address'], 'string', 'length' => [10, 500]],
			[['image'], 'required','when'=>function($model){
				return !empty($model->image);
			},'whenClient' => "function (attribute, value) {
                return ( $('#users-image').get(0).files.length ==0 && $('#users-image').prev('input').val() == '' );
            }"],
            [['business_logo'], 'required','when'=>function($model){
                return !empty($model->business_logo);
            },'whenClient' => "function (attribute, value) {
                return ($('#users-business_logo').get(0).files.length ==0 && $('#users-business_logo').prev('input').val()=='');
            }"],
			[['image', 'business_logo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg', 'maxSize' => 1024 * 1024 * 1, 'tooBig' => 'You can just upload 1MB image'],
            [['name', 'contact', 'otp', 'password', 'mobile_tokens', 'hash_token', 'image', 'account_type', 'business', 'pancard', 'status', 'role', 'social', 'created_on', 'modified_on', 'firstname', 'lastname', 'email', 'mobile_number', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'business_logo', 'states', 'business_service_taxno', 'has_cp', 'compliance', 'registered_city', 'registered_state', 'registered_pincode', 'office_city' , 'office_state', 'office_pincode', 'c_type', 'c_others', 'b_type', 'b_others',  'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code', 'membership', 'plocation', 'device'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['status', 'has_cp', 'role', 'account_type', 'ctype', 'btype']
        ];
    }
    
    public function checkpassword($attr, $params){
        echo $attr.'<br/>';
        echo $params.'<br/>';
        if(empty($this->password))
            $this->addError($attr, "$attr must be required");   
        die('pass: '.$this->password);
        $res = self::find()->select([$attr])->where([$attr=>$this->$attr])->one();
        if(!empty($res))
            $this->addError($this->password, "$this->password must be unique");        
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'firstname' => Yii::t('app', 'First Name'),
            'lastname' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'contact' => Yii::t('app', 'Contact'),
            'otp' => Yii::t('app', 'Otp'),
            'password' => Yii::t('app', 'Password'),
            'mobile_tokens' => Yii::t('app', 'Mobile Tokens'),
            'hash_token' => Yii::t('app', 'Hash Token'),
            'image' => Yii::t('app', 'Image'),
            'account_type' => Yii::t('app', 'Account Type'),
			// 'company_type' => Yii::t('app', 'Company Type'),
			// 'business_type' => Yii::t('app', 'Business Type'),
            'business' => Yii::t('app', 'Business'),
            'account' => Yii::t('app', 'Account Details'),
            'pancard' => Yii::t('app', 'Pancard'),
            'status' => Yii::t('app', 'Status'),
            'role' => Yii::t('app', 'Role'),
            'social' => Yii::t('app', 'Social'),
            'has_cp' => Yii::t('app', 'Credit Period'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			// echo '<pre>'; print_r($_FILES);
			// echo '<pre>'; print_r($this); die('beforeSave');
            if(Yii::$app->controller->action->id == "update"){
                if(!empty($this->password)){
                    $this->password = md5($this->password);
                    //if($this->oldAttributes['password'] != md5($this->password)){}
                }else{
                    $this->password = $this->oldAttributes['password'];
                }
            }else{
				if(!empty($this->password))
                // $this->password = md5($this->password);
                $this->password = $this->password;
            }
			if(!empty($this->pancard)) $this->pancard = strtoupper($this->pancard);
			if(!empty($this->firstname) || !empty($this->lastname)){
				$this->name = [
					 'firstname' => empty($this->firstname) ? NULL : $this->firstname,
					 'lastname' => empty($this->lastname) ? NULL : $this->lastname,
				];
			}
            $this->contact = [
                 'email' => empty($this->email) ? "" : $this->email,
                 'mobile_number' => empty($this->mobile_number) ? "" : $this->mobile_number,
                 'alternate_number' => empty($this->alternate_number) ? "" : $this->alternate_number,
            ];
			if(!empty($this->c_type) || !empty($this->c_others)){
				$this->company_type = [
					 'ctype' => empty($this->c_type) ? '' : $this->c_type,
					 'text' => empty($this->c_others) ? '' : $this->c_others,
				];
			}
			if(!empty( $this->b_type) || !empty($this->b_others)){
				$this->business_type = [
					 'btype' => empty($this->b_type) ? '' : $this->b_type,
					 'text' => empty($this->b_others) ? '' : $this->b_others,
				];
			}
			$business = empty($this->business) ? [] : $this->business;
			if(!empty($this->business_name)) $business['name'] = $this->business_name;
			if(!empty($this->business_pancard)) $business['pancard'] = $this->business_pancard;
			if(!empty($this->landline)) $business['landline'] = $this->landline;
			if(!empty($this->business_service_taxno)) $business['service_taxno'] = strtoupper($this->business_service_taxno);
			if(!empty($this->business_logo)){
				$business['logo'] = basename($this->business_logo);
			} 
			unset($business['business_logo']); // unsetting the business_logo 
			unset($business['business_service_taxno']); // unsetting the business_service_taxno 
			unset($business['business_name']); // unsetting the business_name 
			if(!empty($this->registered_address)){
				$business['registered_address' ] = empty($this->registered_address) ? '' : $this->registered_address;
				$business['office_address' ] = empty($this->office_address) ? '' : $this->office_address;
				$business['landline'] = empty($this->landline) ? '' : $this->landline;
				$business['name'] = empty($this->business_name) ? '' : $this->business_name;
				$business['service_taxno'] = empty($this->business_service_taxno) ? '' : $this->business_service_taxno;
				$business['logo'] = empty($this->business_logo) ? '' : basename($this->business_service_taxno);
				// $business['pancard'] = empty($this->business_pancard) ? NULL : strtoupper($this->business_pancard);
				// $business['states' ] = empty($this->states) ? NULL : $this->states;
				$business['registered_city'] = empty($this->registered_city) ? '' : $this->registered_city;
				$business['registered_state'] = empty($this->registered_state) ? '' : $this->registered_state;
				$business['registered_pincode'] = empty($this->registered_pincode) ? '' : $this->registered_pincode;
				$business['office_city'] = empty($this->office_city) ? '' : $this->office_city;
				$business['office_state'] = empty($this->office_state) ? '' : $this->office_state;
				$business['office_pincode'] = empty($this->office_pincode) ? '' : $this->office_pincode;				
			}
			$this->business = $business;
			if(!empty($this->ac_number)){
				$this->account = [
					'ac_number' => empty($this->ac_number) ? '' : $this->ac_number,
					'holder_name' => empty($this->holder_name) ? '' : $this->holder_name,
					'bank_name' => empty($this->bank_name) ? '' : $this->bank_name,
					'branch_address' => empty($this->branch_address) ? '' : $this->branch_address,
					'branch_code' => empty($this->branch_code) ? '' : $this->branch_code,
					'ifsc_code' => empty($this->ifsc_code) ? '' : $this->ifsc_code,
				];
			}
            if(!empty($this->image)) $this->image  = basename($this->image);
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
           /*  if($this->isNewRecord) $this->created_on = Common::currentTimeStamp();
            else $this->modified_on = Common::currentTimeStamp(); */
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        $this->firstname = empty($this->name['firstname']) ? NULL : $this->name['firstname'];
        $this->uname = empty($this->name['firstname']) ? NULL : $this->name['firstname'];
        $this->lastname = empty($this->name['lastname']) ? NULL : $this->name['lastname'];
        $this->email = empty($this->contact['email']) ? NULL : $this->contact['email'];
        $this->b_type = empty($this->business_type['btype']) ? NULL : $this->business_type['btype'];
        $this->c_type = empty($this->company_type['ctype']) ? NULL : $this->company_type['ctype'];
        $this->mobile_number = empty($this->contact['mobile_number']) ? NULL : $this->contact['mobile_number'];
        $this->alternate_number = empty($this->contact['alternate_number']) ? NULL : $this->contact['alternate_number'];
        $this->business_name = empty($this->business['name']) ? NULL : $this->business['name'];
        $this->registered_address = empty($this->business['registered_address']) ? NULL : $this->business['registered_address'];
        $this->office_address = empty($this->business['office_address']) ? NULL : $this->business['office_address'];
        $this->landline = empty($this->business['landline']) ? NULL : $this->business['landline'];
        $this->business_pancard = empty($this->business['pancard']) ? NULL : $this->business['pancard'];
        $this->registered_city = empty($this->business['registered_city']) ? NULL : $this->business['registered_city'];
        $this->registered_state = empty($this->business['registered_state']) ? NULL : $this->business['registered_state'];
        $this->registered_pincode = empty($this->business['registered_pincode']) ? NULL : $this->business['registered_pincode'];
        $this->office_city = empty($this->business['office_city']) ? NULL : $this->business['office_city'];
        $this->office_state = empty($this->business['office_state']) ? NULL : $this->business['office_state'];
        $this->office_pincode = empty($this->business['office_pincode']) ? NULL : $this->business['office_pincode'];
        $this->business_service_taxno = empty($this->business['service_taxno']) ? NULL : $this->business['service_taxno'];
		$this->business_logo = empty($this->business['logo']) ? '' : Common::getMediaPath($this, $this->business['logo']);
		/* Added @ the Time of Trucker Module starts here */
        $this->ac_number = empty($this->account['ac_number']) ? NULL : $this->account['ac_number'];
        $this->holder_name = empty($this->account['holder_name']) ? NULL : $this->account['holder_name'];
        $this->bank_name = empty($this->account['bank_name']) ? NULL : $this->account['bank_name'];
        $this->branch_address = empty($this->account['branch_address']) ? NULL : $this->account['branch_address'];
        $this->branch_code = empty($this->account['branch_code']) ? NULL : $this->account['branch_code'];
        $this->ifsc_code = empty($this->account['ifsc_code']) ? NULL : $this->account['ifsc_code'];
		/* Added @ the Time of Trucker Module ends here */
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
        // $this->membership = empty($this->membership['plan_type']) ? '' : $this->membership['plan_type'];
        // $this->status = $this->status ? 'Active' : 'Inactive';

        if(Yii::$app->controller->action->id != "update"){
			/* $this->status = 'Inactive';
			if($this->status == 1){
				$this->status = 'Active';
			}else if($this->status == 3){
				$this->status = 'Deleted';
			} */
            $this->status = $this->status ? 'Active' : 'Inactive';
            $this->has_cp = $this->has_cp ? 'Yes' : 'No';
            if(!empty($this->image)){
                $this->image = Common::getMediaPath($this,$this->image);
            }
        }
    }
    
	/* send verificattion email */
    public function sendVerificationMail($model = false, $url = null){ //return true;
        $model = empty($this) ? $model : $this;
        $validationLink = empty($url) ? 'site/validate-email/' : $url;
        $subject = "CargoWala email verification mail !";
        $link = Yii::$app->urlManager->createAbsoluteUrl([$validationLink, 'token' => $model->hash_token]);
        /*<p>If you need help or have any questions, please visit <a href="http://www.guidanceclub.com/frontend/" target="_blank">www.guidanceclub.com/contact-us/</a></p>*/
        $message = '<p>Hi ' . $model->name['firstname'] . '</p>
            <p>Thanks for Signup with cargowala.</p>
            <p>You need to click on the link below in order to  verify your email ID.</p>
            <p><a href="' . $link . '" target="_blank">' . $link . '</a></p>
            <p>For any queries write to us at support@cargowala.com</p>
            <p>Cheers!! <br>
            Team CargoWala</p>';
        return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($this->email)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)->send();
    }
	
	/* send Notification email */
    public function sendNotificationMail($model = false, $userType = null, $password){ //return true;
        $model = empty($this) ? $model : $this;
        $subject = "Welcome to Cargowala !";
		$siteUrl = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['site/login']);
		if($userType == 'Moderators')
			$siteUrl = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
        // $subject = "New ".$userType. " is added by CargoWala administrator !";
        $name = $model->name['firstname'].' '.$model->name['lastname'];
        $message = '<p>Hi ' . $name . '</p>
        <p>Your account is created as '.$userType.' by CargoWala administrator.</p>
        <p>You can login to your account by the following link.</p>
        <p><a href="' .$siteUrl. '" target="_blank"> click here to login </a></p>
        <p>Your credentials for the same are as below!</p><br/>
        <p>Email : '.$model->contact['email'].'</p>
        <p>Password : '.$password.'</p><br/><br/>
        <p>Cheers!! <br>
        Team CargoWala</p>';
        return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($this->email)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)->send();
    }
	
	/* Send Reset Password Email */
    public function resetPasswordEmail($model = false){
        $resetPasswordLink = Yii::$app->urlManager->createAbsoluteUrl(['site/password-reset', 'token' => $model->hash_token, 'id' => (string)$model->_id]);
        $subject = "Cargowala reset password link !";
        $name = $model->name['firstname'].' '.$model->name['lastname'];
        $message = '<p>Hi ' . ucwords($name) . '</p>
        <p>Please find below the link for reset password.</p>
        <p><a href="' .$resetPasswordLink. '" target="_blank"> click here</a> to reset your password.</p>
        <br/><br/>
        <p>Cheers!! <br>
        Team CargoWala</p>';
        return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($this->email)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)->send();
    }    
    
	/* Send Reset Password Email */
    public function resetPasswordTokenEmail($model = false){
        $link = Yii::$app->urlManager->createAbsoluteUrl(['site/validate-email', 'token' => $model->hash_token, 'id' => (string)$model->_id]);
        $subject = "Cargowala email verification link !";
        $name = $model->name['firstname'].' '.$model->name['lastname'];
        $message = '<p>Hi ' . $name . '</p>
            <p>Below is the email verification link.</p>
            <p>You need to click on the link below in order to  verify your email ID.</p>
            <p><a href="' . $link . '" target="_blank">' . $link . '</a></p>
            <p>For any queries write to us at support@cargowala.com</p>
            <p>Cheers!! <br>
            Team CargoWala</p>';
        return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($this->email)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)->send();
    }

	public function emailNotifications($sendTo){
		switch($sendTo){
			case "driver_added":
		         $subject = "CargoWala Notification | New driver added with your account.";
				 $msg = '<p>Hi ' . Yii::$app->user->getName() . '</p>';
				 $msg .= '<p>This is notifiation message.</p>';
				 $msg .= '<p>New Driver added in your account.</p>';
				 $msg .= '<p><a href="' . Yii::$app->urlManager->createAbsoluteUrl(['site/login']). '" target="_blank"> click here to login </a></p>';
				 $msg .= '<p>For any queries write to us at support@cargowala.com</p>';
				 $msg .= '<p>Cheers!! <br>Team CargoWala</p>';
				 Users::sendMail(Yii::$app->user->getEmail(),$subject,$msg);
			break;
			case "vahicle_added":
				 $subject = "CargoWala Notification | New Vehicle added with your account.";
				 $msg = '<p>Hi ' . Yii::$app->user->getName() . '</p>';
				 $msg .= '<p>This is notifiation message.</p>';
				 $msg .= '<p>New Vehicle added in your account.</p>';
				 $msg .= '<p><a href="' . Yii::$app->urlManager->createAbsoluteUrl(['site/login']). '" target="_blank"> click here to login </a></p>';
				 $msg .= '<p>For any queries write to us at support@cargowala.com</p>';
				 $msg .= '<p>Cheers!! <br>Team CargoWala</p>';
				 Users::sendMail(Yii::$app->user->getEmail(),$subject,$msg);
			break;	
			default:
			break;	
		}
	}
	// get user name by id...
	public function getName(){
		try{
		    $name= $this->name['firstname'];
			$name.=empty($this->name['lastname'])?null:' ' . $this->name['lastname'];
			return $name;
		}catch(Exception $e){}
	}
	//get user mobile no...
	public function getMobileNo(){
		try{
		return $this->contact['mobile_number'];
		}catch(Exception $e){}
	}
	// get shipper name and mobile no
	public function getNameandMobileNo(){
		$name = $this->getName();
		if(($no = $this->getMobileNo())) $name .= '<br> Ph No.';
		   return $name . $no;
	}
	// get shipper name and mobile no
	public function assignTrucker(){ 
		$name = $this->getName();
		if(empty($name)) return "Assign Trucker";
		return $this->getNameandMobileNo();
	}
	
	// load user by Id...
	public function loadUser($id){
		if(($user = Users::findOne($id))!=null)
			return $user;
		return new self();
	}
    
	public function sendMail($to,$subject,$message){
		if(empty($to))return;
		return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($to)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)
        ->send();
	}
    
}