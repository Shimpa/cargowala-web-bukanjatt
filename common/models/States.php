<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "states".
 *
 * @property \MongoId|string $_id
 * @property mixed $name
 * @property mixed $isState
 * @property mixed $country_id
 */
class States extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return [Yii::$app->mongodb->defaultDatabaseName, 'states'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'isState',
            'status',
            'country_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'isState','status', 'country_id'], 'safe']
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeType()
    {
        return [

        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'name' => 'Name',
            'isState' => 'Is State',
            'status' => 'Status',
            'country_id' => 'Country ID',
        ];
    }
}
