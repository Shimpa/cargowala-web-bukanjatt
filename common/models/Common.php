<?php
namespace common\models;
use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\AdminSettings;
defined('DS') or define('DS',"/");


class Common {
// const MEDIA = "../../common/media";
const MEDIA = "../../media";
const HTTP_SUCCESS = 200;	
const HTTP_BAD_REQUEST = 400;	

public function getAdmin_contact(){
	$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
	return $admin_contact = empty($adminsettings->admin_contact) ? '987XXXX210' : $adminsettings->admin_contact;
}

public function getAdmin_email(){
	$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
	return $admin_email = empty($adminsettings->admin_email) ? 'so*****************od' : $adminsettings->admin_email;
}

private static $model,$data,$component;
  
  /* arr2var convert json object to array and
   * will extract array to var
   * @params (array)$array or (Object),$flag =EXTR_PREFIX_SAME||EXTR_PREFIX_ALL||EXTR_PREFIX_INVALID||EXTR_OVERWRITE
   * @(string)$prefix
   */ 
  public function arr2Var($array,$return=false,$flag=EXTR_PREFIX_SAME,$prefix="pct_"){
    is_array($array) or $array = self::jsonDecode($array,true);
    if($return) return $array; 
    extract($array,$flag=EXTR_PREFIX_SAME,$prefix="pct_");
  }
  static public function Mediapath(){
    return realpath(self::MEDIA);
  }
 /* get base url domain url
  */
  static public function baseUrl(){
    return 'http://52.27.166.47'.DS.'common/media'.DS;  
    // return $_SERVER['SERVER_ADDR'].DS.'common/media'.DS;  
  }

  static public function getUrlMediaPath(){
    return dirname(Yii::$app->urlmanager->CreateAbsoluteUrl('')).DS.self::MEDIA.DS;
  }

    /* show avatar image 
    * @params $avatar_image
    * @returns string
    */
  static public function avatar($avatar_image=null){
    $avatar_path = "avatars".DS; 
    $avatar = self::baseUrl().$avatar_path.$avatar_image;     
    return $avatar;        
  }    
   static public function model($model){
    self::$model =$model;
    return new static();
  }
  /* volume comic temprary image path
   */
  public function mediaTempPath(){
    $id= $user_id = (string)Common::loggedInUser();
    return self::getUrlMediaPath()."volumns/tmp".$id.DS;    
  }
  public function mediaTempUriPath(){
    $id= $user_id = (string)Common::loggedInUser();
    return self::Mediapath()."/volumns/tmp".$id.DS;    
  }
  
  
  /* noImage return no-image.jpg with media path
   * @return (string) no-image media path
   */ 
   static public function noImage($size = 'XS'){
    $dirpath = self::Mediapath()."/noimage".DS."no-image.jpg";
    $image   = self::getUrlMediaPath() ."/noimage".DS."no-image.jpg"; 
    if(!file_exists($dirpath))return;
    $file = pathinfo($image); 
    $sizes = Yii::$app->params['image.sizes'];
    list($width,$height) = $sizes[$size];
    return self::getUrlMediaPath() ."/noimage".DS.$file['filename']."-".$width."x".$height.".".$file['extension'];
    //return self::getUrlMediaPath() ."noimage".DS."no-image.jpg";
  }
 /* get full image with media directort path
  * @return (mixed) image with path
  */ 
  static public function getMediaPath($model,$image,$context=false){
    return Common::onlyMediaPath($model,$context) . $image;
  }
  /* onlyMediaPath returns image path excluding image name
   * @params (object) $model,(context) $context if model is
   * array then @$context is used as model
   * @return (string) image path excluding image name;
   */ 
  public function onlyMediaPath($model,$context=false){
    is_object($model) && $id=$model->_id or is_array($model) && ($id = (string)$model['_id']) && $model = $context;
    return  self::getUrlMediaPath() . ltrim($model::PARENTDIR,DS) .$model::CHILDDIR ."-".$id .DS;
  } 
  /* imageThumb @returns  image thumbnail by image name of given size
   * if file not exists then method will return default no image
   * if (string)$size null and file exist then method returns
   * full size image path... Note. this method depends on model method
   * @params (string) $path image path, (string) $size L(Large),XS(eXtra small),S(Small)
   * @return (string) image
   */
  static public function imageThumb($image,$size){
    $model = self::$model;
    $path  = self::getUrlMediaPath() . ltrim($model::PARENTDIR,'/') .$model::CHILDDIR ."-".$model->_id .DS;
    $dirpath = self::Mediapath().$model::PARENTDIR.$model::CHILDDIR ."-".$model->_id .DS;
    if(empty($image) || !@file_exists($dirpath.$image)) return $image = self::noImage($size);
    $file = pathinfo($image);  
    $sizes = Yii::$app->params['image.sizes'];
    list($width,$height) = $sizes[$size];
    return $path .$file['filename']."-".$width."x".$height.".".$file['extension'];
  }
  /* thumbnail @returns  image thumbnail by 'index'($model->index) of given size
   * if file not exists then method will return default no image
   * if (string)$size null and file exist then method returns
   * full size image path... Note. this method depends on model method
   * @params (string) $path image path, (string) $size L(Large),XS(eXtra small),S(Small)
   * @return (string) image
   */ 
  static public function thumbnail($index,$size){
    $model = self::$model;
    return self::imageThumb($model->$index,$size);
  }
  /* path2thumb @returns thumbnail by given image $path(string)
   * if file not exists then method will return default no image
   * if (string)$size null and file exist then method returns
   * full size image path...
   * @params (string) $path image path, (string) $size L(Large),XS(eXtra small),S(Small)
   * @return (string) image
   */ 
  public function path2Thumb($path,$size=null){
    $url = parse_url($path);
    $file = pathinfo($path);
    $fileuri = $_SERVER['DOCUMENT_ROOT'].$url['path'];
    $dirpath = $file['dirname'];
    if(empty($path) || !@file_exists($fileuri)) return $image = self::noImage();      
    $sizes = Yii::$app->params['image.sizes'];
    list($width,$height) = isset($sizes[$size])?$sizes[$size]:array(null,null);
    if(!$size) return $file['dirname'] . DS .$file['basename'];
    return $file['dirname'] . DS .$file['filename']."-".$width."x".$height.".".$file['extension'];
  }
 
  /* Create directory from given path
   * @param (String) $path
   * @return created direcory path if success else throw
   *  exception
   */
  static public function mkdir($dir, $permissions=0755, $recursive=false){
    $dir = self::Mediapath().$dir;
    $base = pathinfo($dir);
    if(!is_dir($base['dirname'])) mkdir($base['dirname'],$permissions,$recursive);
    if(!is_dir($dir)) mkdir($dir,$permissions,$recursive);
      return $dir;
  }
    
  /* checks variable value is set else return value in var @$instead
   * @params $var,$instead
   * @return set value or $instead
   */
  static public function haveVal($var,$instead=NULL){
        return isset($var)?$var:$instead;     
  }
    
  public function loadData($data){ $class =__CLASS__;
    $self = new $class;
    self::$data  =$data;
    return $self;    
  }
    
  public function val($index,$model=false){
    $model or $model = self::$model;
    if(is_object($model)) return isset($model->$index)?$model->$index:NULL;
    else return isset($model[$index])?$model[$index]:NULL;
  }
    
  /* check a value exist 
   */
  public function valExist($model,$index){
    if(is_object($model)){ return isset($model->$index)?$model->$index:0;
    }else{ return isset($model[$index])?$model[$index]:0;
    }     
  }
    
    /* DelFile method deletes file and its thumbnail if file not exist then returns response back
    * @params (string) $files path of file
    * 
    */
    static public function DelFile($files){
        if(!file_exists($files) || is_dir($files))return;
        $file = pathinfo($files);
        /*$defaultSizes  = Yii::$app->params['image.sizes'];
        foreach($defaultSizes as $size){ 
            list($width,$height) = $size;
            $f = $file['dirname'].DS. $file['filename'] ."-".$width ."x".$height .".".$file['extension'];
            $f1 = $file['dirname'].DS. $file['filename'] ."-240x200".$file['extension'];
            @unlink($f1);
            @unlink($f);
        }*/
        @unlink($files);
       /* if (self::is_dir_empty($file['dirname'])) {
            rmdir($dir);
        }*/
    }
    
  /* Function is to check if the Directory is empty or not
   * 
   */    
    public function is_dir_empty($dir) {
       // die($dir);
      if (is_readable($dir)){  
          //die('if');
          return TRUE;
      } 
        die('aasda');
      return (count(scandir($dir)) == 2);
    }
    
  /* DelAllMedia method deletes all file,thumbnail and folder too  if directory not exist then returns response back
   * @params (string) $dir path of directory
   * 
   */
  static public function DelAllMedia($dir){
    //$dir  = self::Mediapath() . $dir;
    if(!is_dir($dir)) return;
      $directory = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS);
        foreach(new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::CHILD_FIRST) as $path ) {
             if(!$path->isDir()){
              $file = realpath($path->__toString());
              @unlink($file);        
            }else{
               $dirpath = realpath($path->__toString());
               rmdir($dirpath); 
            }     
        }
         rmdir($dir);
  }
 /* upload image from tmp file create a copy of image on destination
  * applying given options
  * @params $file (blob),option(array)
  */ 
 public function uploadImage($image,$options){
   if(empty($image['tmp_name']) || empty($image['name'])) return ['error'=>'invalid Image'];
     $options['image'] = $image['name'];
     return self::_createImage($image['tmp_name'],$options);   
 }
 private function _createImage($tmpImage,$options){
   $file =pathinfo($options['image']);
   $data =[];
   !empty($options['extensions']) or $options['extensions'] = ['jpg','png'];
   !empty($options['mime']) or $options['mime'] = [];
   !empty($options['size']) or $options['size'] = (5*1024*1024);
   !empty($options['path']) or $options['path'] = "/";
   !empty($options['rename']) or $options['rename'] = false;
   
     if((filesize($tmpImage)/1024/1024)>$options['size'])
        $data['error']['size'] = "`".$options['image']."` size exceded `" . $options['size'] ."` limit.";
   
     if(!in_array($file['extension'],$options['extensions']))
        $data['error']['type'] = "`".$options['image']."` invalid format. only ".implode(",",$options['extensions']) ." are allowed";
     if(!empty($options['mime'])){
         $mime= mime_content_type($tmpImage);
         if(!in_array($mime,$options['mime'])){
            $data['error']['type'] = "`".$options['image']."` invalid format. only ".implode(",",$options['extensions']) ." are allowed"; 
         }
     }
   
     if(!realpath($options['path']))     
        $data['error']['path'] = "`".$options['path']."' is invalid path to save image.";   
     
     
     if(in_array($file['extension'],['jpg','png','bmp','jpeg','gif'])){
   
         $source = null;$saveImage="";
   
         switch($file['extension']){
     
            case "jpg":
            case "jpeg" :
                ini_set('gd.jpeg_ignore_warning', 1); 
                $source =  @imagecreatefromjpeg($tmpImage);
                $saveImage = "imagejpeg";     
            break;    
            case "gif":        
                $source =  imagecreatefromgif($tmpImage);       
                $saveImage = "imagegif";
            break;         
            case "png":       
                $source =  imagecreatefrompng($tmpImage);
                $saveImage = "imagepng";
            break;    
            default:            
                $source =  imagecreatefromjpeg($tmpImage);
                $saveImage = "imagejpeg";
            break;
  
        }
        if(!$source) 
          $data['error']['image'] = "`".$options['path']."' is invalid(bad) image to save image.";     
        if(!empty($data['error']))
           return $data; 
        $filename = !empty($options['rename'])?$options['rename'].".".$file['extension']:$file['basename']; 
        if($file['extension']=="png")    
            $saveImage($source,$options['path'].$filename,9,PNG_FILTER_AVG);
        else
            $saveImage($source,$options['path'].$filename);
            return $filename; 
        imagedestroy($source);
  
    }else{
     $filename = !empty($options['rename'])?$options['rename'].".".$file['extension']:$file['basename'];     
    if(copy($tmpImage,$options['path'].DS.$filename)){
        return $filename;   
    }
   }   
 }
  /* resizeImage method resize image to given size and save it as thumbnail 
  * @params (string)$image ,(int)$width,(int)$heigth
  */
  static public function resizeImage($image,$width=50,$height=50){
    if(!@file_exists($image))return;
      $file =pathinfo($image);
      $path = $file['dirname']; 
      $thumbnail = $path .DS. $file['filename'] ."-".$width ."x".$height .".".$file['extension'];  
      if(!@file_exists($thumbnail)){
           $source = null;
         switch($file['extension']){
            case "jpg":
            case "jpeg":
                $source =  imagecreatefromjpeg($image);
            break;    
            case "gif":
                $source =  imagecreatefromgif($image);
            break;    
            case "png":
                $source =  imagecreatefrompng($image);
            break;    
             default:
                $source =  imagecreatefromjpeg($image);
            break;
           } 
            //$thumb = imagescale($source,$width,$height,IMG_BICUBIC_FIXED);
            list($oldwidth,$oldheight) = getimagesize($image);
            
            $Wration  = $width/$oldwidth;
            $Hration  = $height/$oldheight;
            if($oldwidth>$oldheight){
                $width  = $Hration*$oldwidth;                
            }else{
                $height = $Wration*$oldheight;
            }       
            $thumb   = imagecreatetruecolor($width,$height);
            imagecopyresampled($thumb, $source, 0, 0, 0, 0, $width, $height, $oldwidth, $oldheight);
            if($file['extension']=="jpg")
               imagejpeg($thumb,$thumbnail,95);
            elseif($file['extension']=="png")   
               imagepng($thumb,$thumbnail,8,PNG_ALL_FILTERS);
            elseif($file['extension']=="gif")
               imagegif($thumb,$thumbnail,95);
            imagedestroy($thumb);   
         
      }
      return $thumbnail;
  }
  public function getExtension($filname){
    return pathinfo($filname,PATHINFO_EXTENSION);
  }
  /**
   *  current method runs resizeImage method and creates thumbnail to sizes
   *  given in configrations...
   *  
   **/
  static public function SiteStardardImageSizesResize($image){
    $defaultSizes  = Yii::$app->params['image.sizes'];
    foreach($defaultSizes as $size){
          list($width,$height) = $size;
          Common::resizeImage($image,$width,$height);
    }
  }
  /*
   * return now timestamp...
   */
  static public function currentTimeStamp(){
    return new \MongoDate();//date('Y-m-d H:i:s');
  }
  
	/*
	* return now timestamp...
	*/
	static public function convertDate($date){
		return $today = new \MongoDate(strtotime($date));
	}
	
  /* return date from mongo db to php format..
   * @params mongoDate
   * @return date php
   */
  public static function showDateFrontend($mongoObj,$format='m/d/Y'){
	//echo '<pre>'; print_r($mongoObj); die;
    return is_object($mongoObj)?date($format, $mongoObj->sec):$mongoObj; //$mongoDate->toDateTime()->format(...)
  }
  
   /* return date from mongo db to php format..
   * @params mongoDate
   * @return date php
   */
  public static function showDate($mongoObj,$format='Y-m-d H:i:s'){
    return is_object($mongoObj)?date($format, $mongoObj->sec):$mongoObj; //$mongoDate->toDateTime()->format(...)
  }
  
  
  /* method converts array to json format
   * @params $array (array),
   * $return return data instead of die;
   * $privacy_for_script add a secret or encoded script for privacy
   * $size limit for json encoding
   * @return json|null|die json
   */ 
  static public function encodeJSON($array,$privacy_for_script=true,$return=false,$size=512){
    $privacy = "for(;;);";
    if(!is_array($array)) return NULL;
      try{
       header('Content-type: application/json');
       $json = json_encode($array,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE,$size);
       if($privacy_for_script) $json=$privacy.$json;
       if($return) return $json;
          die($json);
      }catch(Exception $e){
          throw new Exception('Invalid json array or parameters');
      }
  }
  /* jsonDecode converts encoded json object to json object or
   * associated array if @$asArray=true,
   * @params (Object) $json,(boolean) $asArray,(int) $size
   * @return (Object) or (array) if $asArray=true,
   */ 
  public function jsonDecode($json,$asArray=true,$size=512){
      return json_decode($json,$asArray,$size);
  }
    /* jsonDecode converts encoded json object to json object or
   * associated array if @$asArray=true,
   * @params (Object) $json,(boolean) $asArray,(int) $size
   * @return (Object) or (array) if $asArray=true,
   */ 
  public function Jsonify($json){
      return str_replace('for(;;);','',$json);
  }
  /* get id of logged in user
   * @return id mixed
   */ 
  public function loggedInUser(){
    return (string)Yii::$app->user->getId();
   
  }
 /* get Class name as string
  * @params class object
  * @returns string class name
  */
 public function getName($object){
    try{
        if(is_object($object)){ $classname = get_class($object);
          if(strpos($classname,'\\')!=false) $classname = str_replace('\\','/',$classname);    
          return basename($classname);
        }
        
    }catch(Exception $e){
        throw new \Exception($e->getMessage());
    } 
 }    
  /* filter request data
   */ 
	public function filterData($index,$senitize="",$method='POST'){
		$types= array(
			'GET'=>INPUT_GET,
			'POST'=>INPUT_POST,
			'ENV'=>INPUT_ENV,
			'REQUEST'=>INPUT_REQUEST,
			'SERVER'=>INPUT_SERVER,
			'SESSION'=>INPUT_SESSION,
			'COOKIES'=>INPUT_COOKIE,
		);
		$type =isset($types[$method])?$types[$method]:NULL;
		return filter_input($type,$index,self::sanitizeType($senitize));
	}
	
	private function sanitizeType($type){
		$types =array(
		   'INT'=>FILTER_SANITIZE_NUMBER_INT,
		   'FLOAT'=>FILTER_SANITIZE_NUMBER_FLOAT,
		   'STRING'=>FILTER_SANITIZE_STRING,
		);
		return isset($type[$type])?$type[$type]:FILTER_UNSAFE_RAW;
	}
	
	/* set cookies for persistent or non-persistent data
	* @params (string) $name,(mixed)$value,(timestamp)$expire,(string) $path,(string) $domain
	* @boolean $secure,(boolean) $httpOnly
	* @return (boolean) 
	*/
	public function setCookies($name,$value,$options=[]){
		$ar = self::arr2Var($options,true);
		extract($ar,$flag=EXTR_PREFIX_SAME,$prefix="pct_"); 
		isset($expire) or $expire=Yii::$app->params['cookiExpire'];
		isset($path) or $path="/";
		isset($domain) or $domain=Yii::$app->params['domaiName'];
		isset($secure) or $secure=false;
		isset($httponly) or $httponly=false;
		return setcookie($name,$value,$expire,$path,$domain,$secure,$httponly);
	}
	
	/* genrate random security token
	* @param boolean $genrate...
	*/
	public function sToken($genrate=false){ 
		if(!$genrate && Yii::$app->session['sToken']!=null) return Yii::$app->session['sToken'];
		$char = 'QAWERTYUIOPSDFGHJKLMNBVCXZqwertyuioplkjhgfdsazxcvbnm';
		$nums = '09876543210987654321';
		$rand = mt_rand(time(),mt_getrandmax());
		$str  = str_shuffle($char.$nums.$rand);
		$limit = 16;
		return Yii::$app->session['sToken'] =substr($str,($start=rand(0,10)),($limit=(($limit-$start)+$limit)));
	}
	
	public function sTokenEqual($compare){
		return Yii::$app->session['sToken']===$compare;
	}
	
	public function uniqueName($token = false){
		$time =time() . microtime();
		$char = 'QAWERTYUIOPSDFGHJKLMNBVCXZqwertyuioplkjhgfdsazxcvbnm09876543210987654321';
		$str  = str_shuffle($char);
		return uniqid(md5($str.$time.$token));  
	}

    /* set Limit method encrypt (int) limit
    * @params (int)$limit
    * @return (int) encrypted $limit
    */ 
    public function setLimit($limit){
        $rand= substr(mt_rand(time(),mt_getrandmax()),2,2);
        $limit =$rand.$limit.($end=$rand);
        return $limit = $rand . (($limit*$rand)*$end)+$rand. strrev($end);   
    }
    
    /* get Limit method decrypt (int) limit
    * @params (int)$limit
    * @return (int) decrypted $limit
    */ 
    public function getLimit($limit){
        $rand = substr($limit,0,2);
        $end   = strrev(substr($limit,-2));
        $limit = substr($limit,2,strlen($limit));
        $limit = substr($limit,0,strlen($limit)-2);
        if($limit<=0 ||$rand<=0 ||$end<=0) return;
        $limit = (($limit-$rand)/$end)/$rand;
        $limit = substr($limit,2,strlen($limit));
        return $limit = substr($limit,0,strlen($limit)-2);    
    }
    
    /* genrate secret verification token based on params
    * token contains secret data and valid timestamp
    */
    public function genrateToken($secret){
        $token  = base64_encode($secret);
        $token  = base64_encode($token.'-'.time());
        return $token;
    }    

    /* genrate Dropdown listData Dynamically */    
    public function getList($model, $cond, $columns){
        $res = $model::find()->select($columns)->where($cond)->all();
        list($id,$name) = $columns;
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);
    }

    /* genrate Dropdown listData Dynamically */    
    public function getCompliance($compliance){
        $data = '';
        $data .= "<ul>";
        if(!empty($compliance)){
            foreach($compliance as $val){
                $data .= "<li>".ucfirst($val)."</li>";
            }
        }
        $data .= "</ul>";
        return $data;
    }

    /* genrate Dropdown listData Dynamically corresponding to Truck lanes */    
    public function getCustomList($model, $cond, $columns){
        $res = $model::find()->select($columns)->where($cond)->all();
        list($column1,$column2,$column3) = $columns;
        $res = array_map(function($res) use($column1, $column2, $column3) {return [$column1=>(string)$res->$column1, $column2=>$res->$column2.' To '.$res->$column3];}, $res);
        return ArrayHelper::map($res, $column1, $column2);
    }

    /* genrate Dropdown listData Dynamically corresponding to Truck lanes */    
    public function getOwnerList($model, $cond, $columns){
        $res = $model::find()->select($columns)->where($cond)->all();
        list($id, $name, $fname, $lname) = $columns;
        $res = array_map(function($res) use($id, $name, $fname, $lname) {return [$id=>(string)$res->$id, $name=>$res->$fname.' '.$res->$lname];}, $res);
        return ArrayHelper::map($res, $id, $name);		
    }

    /* Function to get users role */
    public function getRoles($type = NULL){
        // {1:"Trucker",2:"Shipper",3:"Agent"};
        $roles = ['1' => 'Trucker', '2' => 'Shipper', '3' => 'Agent'];
        if($type) return isset($roles[$type]) ? $roles[$type] : 'N-A';
        return $roles;
    }
    
    /* Function to get Account Types */
    public function getShipperAccountTypes($type = NULL){
        $accType = ['21' => 'Individual', '22' => 'Company'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }
    
    /* Function to get Account Types */
    public function getTruckerAccountTypes($type = NULL){
        $accType = ['11' => 'Individual', '12' => 'Fleet Owner'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }
    
    /* Function to get Account Types */
    public function getAccountTypes($type = NULL){
        $accType = ['11' => 'Individual', '12' => 'Fleet Owner', '3' => 'Agent'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }
    
    /* Function to get Account Types */
    public function getUserType($type = NULL){
        $accType = ['11' => 'Individual', '12' => 'Fleet Owner', '3' => 'Agent'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }

    /* Function to get Account Types */
    public function getMembershipType($type = NULL){
        $data = '';
		$data .= "<p>".ucfirst($type['plan_type'])."</p>";
		if($type['transactions']) $data .= "<p>".$type['transactions']." Transaction(s)</p>";
        return $data;		
    }

    /* Function to get Companies List */
    public function getCompaniesList($type = NULL){
        $accType = ['1' => 'Proprietary', '2' => 'Partnership Firm', '3' => 'Private Limited', '4' => 'Others', '5' => 'Public Sector', '6' => 'Traders'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }

    /* Function to get Business type */
    public function getBusinessList($type = NULL){
        $accType = ['1' => 'Logistics / Transporter', '2' => 'Manufacturing / Factory', '4' => 'Others', '5' => 'Traders / Distributer'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }

    /* Function to get Truck Categories */
    public function getTruckCategory($type = NULL){
        $category = ['1' => 'Small Trucks', '2' => 'Medium Trucks', '3' => 'Heavy Trucks', '4' => 'Others'];
        if($type) return isset($category[$type]) ? $category[$type] : 'N-A';
        return $category;
    }
    
    /* Function to get Truck Categories */
    public function getTruckTypes($type = NULL){
        $category = [ 1 => 'Below 5 Ton', 2 => '5 Ton to 15 Ton', 3 => 'Above 15 Ton' ];
        if($type) return isset($category[$type]) ? $category[$type] : 'N-A';
        return $category;
    }
    
    /*Function to Add Compliance to the Vehicle If there documentation
    like (Registration, Permit & Insurance Documents are pending to submit). */
    public function setCommonCompliance($dtarr){
		$compliance = [];
        foreach($dtarr as $key => $val){
            $compliance[$val] = str_replace("_"," ",$val).' is not received yet.';
        }
        return $compliance;
    }    
    
	/*Function to Add Compliance to the Vehicle If there documentation
    like (Registration, Permit & Insurance Documents are pending to submit). */
    public function setCompliance($model){
        $compliance = [];
        foreach($model['permit'] as $key => $val){
            if(empty($val)) $compliance[$key] = 'Permit '.str_replace("_"," ",$key).' is not received yet.';
        }
        if(self::checkDiff($model->permit_expiry_date, 'permit')) $compliance['permit_expired'] = 'Permit is expired.'; 
        foreach($model['registration'] as $key => $val){
            if(empty($val)){
                if($key == 'rc_pic') $compliance['rc_pic'] = 'Registration image is not received yet.';
                if($key == 'rc_number') $compliance['rc_number'] = 'Registration number is not received yet.';
            }
        }
        if(self::checkDiff($model->rc_expiry_date, 'registration')) $compliance['registration_expired'] ='Registration certificate is expired.';     		
        foreach($model['insurance'] as $key => $val){
            if(empty($val)) $compliance[$key] = 'Insurance '.str_replace("_"," ",$key).' is not received yet.';
        }
        if(self::checkDiff($model->renew_date, 'insurance')) $compliance['insurance_expired'] ='Insurance is expired.';
		if(empty($compliance)) return $compliance = '';
        return $compliance;
    }
    
    /* Function to calculate date difference between two dates*/
    public function checkDiff($date,$type){
        $now = time();
        $your_date = strtotime($date);
        $datediff = $your_date - $now;
        $days = floor($datediff/(60*60*24));
        if($days < 0) return true;
        return false;
    }
    
    /* Function to get the Ownner Name*/
    public function getOwner($id){
        $result = Moderators::findOne($id);
        if($result['name']['firstname']){
            return $id = $result['name']['firstname'].' '.$result['name']['lastname'];
        }else{
            return $id = 'N-A';
        }
    }

	/* Function to get the Ownner Name*/
    public function getOwnerName($id){
        $result = Users::findOne($id);
        if($result['name']['firstname']){
            return $id = $result['name']['firstname'].' '.$result['name']['lastname'];
        }else{
            return $id = 'N-A';
        }
    }
	
	/* Function to get Shipment type */
    public function getShipmentType($type){
		return empty($type['text']) ? 'N-A' : $type['text'];
    }
	
	/* send Email Notification */
    public function sendEmailNotification($to = null, $subject = null, $message = null){ 
		$msg = $message;
		$msg .= '<p>Cheers!! <br>Team CargoWala</p>';
		return self::sendMail($to, $subject, $msg);
    }	
 	
	/* send SMS Notification */
	public function sendSMSNotification($to = null, $msg = null){ 
		$pth  = "http://121.242.224.32:8080/smsapi/httpapi.jsp?username=" . urlencode(Yii::$app->params['smsusername']);		
	    $pth .= "&password=" . urlencode(Yii::$app->params['smspassword']);
	    $pth .= "&from=" . urlencode(Yii::$app->params['smssenderid']);
	    $pth .= "&to=" . urlencode($to);
	    $pth .= "&text=" . urlencode($msg);		
		$res = file_get_contents($pth);
    }
    
	public function sendMail($to,$subject,$message){
		if(empty($to))return;
		return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($to)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)
        ->send();
	}

	/* save and send Notification Via Cron Job on the bases of notification type*/
    public function saveNotification($to = null, $subject = null, $message = null, $type = null, $id = null, $isAdmin = false){ 
		$queueModel = new Queue();
		$queueModel->member = $to;
		$queueModel->subject = $subject;
		$queueModel->message = $message;
		$queueModel->status = 0;
		$queueModel->priority = 0;
		$queueModel->action = $type; // Email / Text Message / Push notification
		if($type == 'email'){
			$string = str_replace("\n", "<br/>", $message);
			$queueModel->message = $string;
		}
		if($type == 'push' || $isAdmin){
			$canView = !empty($id) ? new \MongoId($id) : 'admin';
			$notificationModel = new Webnotifications();
			$notificationModel->message = $message;
			$notificationModel->can_view = [$canView];
			$notificationModel->read = [];
			$notificationModel->save(false);
		}
		$queueModel->save(false);
    }
    
/************************************* testing purpose  *************************************/

	public function timetaken($start=false){
	  if($start){ echo '--Start Time--:'.microtime(); return;}  
	  echo '--End Time--:'.microtime();die;    
	}
    
	public static $start_memory;

	public function memoryUses($end=false){
		self::$start_memory = memory_get_usage();
		echo "Start:Time[".microtime(true)."] Memory Start[" . self::$start_memory ."]<br/>";
		if($end){ $end = (memory_get_usage()-self::$start_memory);
		echo "End:Time[".microtime(true)."]  Memory End[".memory_get_usage()."] Usage[".($end)."]";
		}
	}
	// et messages from local messages file...
	public function getLocalMessage($key,$data=array(),$filename=false){
		try{
			$filename = !empty($filename)?$filename:Yii::$app->controller->id;
			$ext  = '.mp';
			$file = realpath('../../locals/').'/'.$filename.$ext;
			$json = file_get_contents($file);
			$array = '{'.$json.'}';
			$array = json_decode($array,true);
			$message = empty($array[$key])?null:$array[$key];
			if($message && empty($data))return $message;
			if($message) return str_replace(array_keys($data),$data,$message);
			   return null;
		}catch(Exception $e){ print_r($e);
			return null;
		}
	}
	// get admin support no
	public function getSupportNo(){
		return '9463944676';
	}

}