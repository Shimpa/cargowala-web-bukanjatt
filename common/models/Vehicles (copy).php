<?php
namespace common\models;
use Yii;
/**
* This is the model class for collection "vehicles".
*
* @property \MongoId|string $_id
* @property mixed $type
* @property mixed $permit_info
* @property mixed $registration_info
* @property mixed $insurance_info
* @property mixed $owner_id
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/

class Vehicles extends \yii\mongodb\ActiveRecord{
    
    const PARENTDIR = '/vehicles';
    const CHILDDIR = '/vehicle';    
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return ['cargowla', 'vehicles'];
    }
    
    public $permit_state, $permit_pic, $permit_number, $permit_issueDate, $permit_expiryDate, $reg_pic, $reg_number, $insrnce_pic, $insrnce_issueDate, $insrnce_renewDate;
    
    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'type',
            'permit_info',
            'registration_info',
            'insurance_info',
            'owner_id',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['type', 'permit_state', 'permit_number', 'permit_issueDate', 'permit_expiryDate',  'reg_number', 'insrnce_issueDate', 'insrnce_renewDate', 'status'], 'required'],
            //[['permit_pic'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['type', 'permit_state', 'permit_pic', 'permit_number', 'permit_issueDate', 'permit_expiryDate', 'reg_pic', 'reg_number', 'insrnce_pic', 'insrnce_issueDate', 'insrnce_renewDate', 'owner_id', 'status'], 'safe'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
             'integer' => 'status',
        ];
    }


    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'permit_info' => Yii::t('app', 'Permit Info'),
            'registration_info' => Yii::t('app', 'Registration Info'),
            'insurance_info' => Yii::t('app', 'Insurance Info'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }

/*[0] => type
[1] => permit_state
[2] => permit_pic
[3] => permit_number
[4] => permit_issueDate
[5] => permit_expiryDate
[6] => reg_pic
[7] => reg_number
[8] => insrnce_pic
[9] => insrnce_issueDate
[10] => insrnce_renewDate
[11] => owner_id
[12] => status*/    
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            //echo '<pre>'; print_r($this); die('beforeSave');
            //echo '<pre>'; print_r($this->oldAttributes); die('beforeSave');
            $this->created_on = Common::currentTimeStamp();
            $this->owner_id = Yii::$app->user->getId();
            $this->permit_info = [
                 'permit_state' => empty($this->permit_state) ? NULL : $this->permit_state,
                 'permit_pic' => empty($this->permit_pic) ? NULL : $this->permit_pic,
                 'permit_number' => empty($this->permit_number) ? NULL : $this->permit_number,
                 'permit_issueDate' => empty($this->permit_issueDate) ? NULL : $this->permit_issueDate,
                 'permit_expiryDate' => empty($this->permit_expiryDate) ? NULL : $this->permit_expiryDate,
            ];
             $this->registration_info = [
                 'reg_pic' => empty($this->reg_pic) ? NULL : $this->reg_pic,
                 'reg_number' => empty($this->reg_number) ? NULL : $this->reg_number,
            ];
            $this->insurance_info = [
                 'insrnce_pic' => empty($this->insrnce_pic) ? NULL : $this->insrnce_pic,
                 'insrnce_issueDate' => empty($this->insrnce_issueDate) ? NULL : $this->insrnce_issueDate,
                 'insrnce_renewDate' => empty($this->insrnce_renewDate) ? NULL : $this->insrnce_renewDate,
            ];
            if(!$this->isNewRecord)
                $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update")
            $this->status = $this->status ? 'Active' : 'Inactive';
        $result = Moderators::findOne($this->owner_id);
        if($result['name']['firstname']){
            $this->owner_id = $result['name']['firstname'].' '.$result['name']['lastname'];
        }else{
            $this->owner_id = 'N-A';
        }
        $this->permit_state = empty($this->permit_info['permit_state']) ? NULL : $this->permit_info['permit_state'];
        $this->permit_number = empty($this->permit_info['permit_number']) ? NULL : $this->permit_info['permit_number'];
        $this->type = self::getTruckList($this->type);
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
    }    
    
    public function getTruckList($type = NULL){
        $trucks = ['1' => 'Tata Ace', '2' => 'Pick up', '3' => 'Tata 407', '4' => '14 Feet Open', '5' => '14 Feet Close', '6' => '17 Feet Open', '7' => '17 Feet Close'];
        if($type) return isset($trucks[$type]) ? $trucks[$type] : 'N-A';
        return $trucks;
    }    
    
}
