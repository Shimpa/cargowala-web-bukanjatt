<?php
namespace common\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Trucks;

/**
 * TrucksSearch represents the model behind the search form about `common\models\Trucks`.
 */
class TrucksSearch extends Trucks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'truck_type', 'name', 'desc', 'loading_capacity', 'created_by', 'status', 'created_on', 'modified_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Trucks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'truck_type', $this->truck_type])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'loading_capacity', $this->loading_capacity])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on]);
        return $dataProvider;
    }
}
