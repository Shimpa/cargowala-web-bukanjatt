<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;

/**
 * UsersSearch represents the model behind the search form about `common\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			[[ 'name', 'contact', 'otp', 'password', 'mobile_tokens', 'hash_token', 'account_type', 'business', 'pancard', 'status', 'role', 'social', 'created_on', 'modified_on', 'firstname', 'lastname', 'email', 'mobile_number', 'alternate_number', 'business_name', 'registered_address', 'office_address', 'landline', 'business_pancard', 'business_logo', 'states', 'business_service_taxno', 'has_cp', 'compliance', 'registered_city', 'registered_state', 'registered_pincode', 'office_city' , 'office_state', 'office_pincode', 'c_type', 'c_others', 'b_type', 'b_others',  'ac_number', 'holder_name', 'bank_name', 'branch_address', 'branch_code', 'ifsc_code', 'plocation'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
			return $dataProvider;
		}
        $status  = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;

        $has_cp  = $this->has_cp;
        if(isset($this->has_cp) && ($this->has_cp != '')) $has_cp = (int)$this->has_cp;
		$membership  = $this->membership;
        if(isset($this->membership) && ($this->membership != '')) $membership = (int)$this->membership;
		$query -> andFilterWhere(['like', '_id', $this->_id])
			->andFilterWhere(['like', 'name.firstname', $this->firstname])
            ->andFilterWhere(['like', 'name.lastname', $this->lastname])
            ->andFilterWhere(['like', 'contact.email', $this->email])
            ->andFilterWhere(['like', 'contact.mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'contact.alternate_number', $this->alternate_number])
            ->andFilterWhere(['like', 'otp', $this->otp])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'mobile_tokens', $this->mobile_tokens])
            ->andFilterWhere(['like', 'hash_token', $this->hash_token])
            ->andFilterWhere(['like', 'image', $this->image]);
            // ->andFilterWhere(['account_type' => $account_type])
            // ->andFilterWhere(['role' => $role])
			if(!is_array($this->role))
               $query->andFilterWhere(['role' =>(int)$this->role]);
            else
			  $query->andFilterWhere(['role' => $this->role]);
            $query->andFilterWhere(['like', 'membership.type', $this->membership])
            ->andFilterWhere(['membership.plan_code' => $membership])
            ->andFilterWhere(['like', 'company_type', $this->company_type])
            ->andFilterWhere(['like', 'business_type', $this->business_type])
            ->andFilterWhere(['like', 'business', $this->business])
            ->andFilterWhere(['like', 'pancard', $this->pancard])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['has_cp' => $has_cp])
            // ->andFilterWhere(['compliance' => $compliance])
            // ->andFilterWhere(['not', ['compliance' => null]])
            ->andFilterWhere(['like', 'social', $this->social])
            ->andFilterWhere(['like', 'created_on', $this->created_on]);
            // ->andFilterWhere(['like', 'modified_on', $this->modified_on])->orderBy('created_on DESC');
			//if(Yii::$app->user->isAdmin) // to display only shipper user in admin under shipper tab
			//	$query->andFilterWhere(['role'=>22]);
			if(is_array($this->role))
			   $this->role = ''; 
		   return $dataProvider;
    }
}
