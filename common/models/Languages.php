<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "languages".
 *
 * @property \MongoId|string $_id
 * @property mixed $language_name
 * @property mixed $language_code
 * @property mixed $language_desc
 * @property mixed $status
 */
class Languages extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return [Yii::$app->mongodb->defaultDatabaseName, 'languages'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'language_name',
            'language_code',
            'language_desc',
            'status',
        ];
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_name', 'language_code', 'language_desc', 'status'], 'required'],
			[['language_name'], 'unique', 'targetAttribute' => 'language_name'],
            [['language_name'], 'string', 'max' => 30],
            [['language_code'], 'string', 'max' => 5],
            [['language_name', 'language_code', 'language_desc', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeType(){
        return [
            'integer'=>['status'],
        ];
    }
    
     /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'language_name' => Yii::t('app', 'Language Name'),
            'language_code' => Yii::t('app', 'Language Code'),
            'language_desc' => Yii::t('app', 'Language Desc'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
	
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id != "update")
           $this->status = $this->status ? 'Active' : 'Inactive'  ;
    }
}
