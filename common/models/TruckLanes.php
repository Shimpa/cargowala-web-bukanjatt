<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "truck_lanes".
 *
 * @property \MongoId|string $_id
 * @property mixed $source
 * @property mixed $source_state
 * @property mixed $destination
 * @property mixed $destination_state
 * @property mixed $distance
 * @property mixed $price_per_km
 * @property mixed $price_per_ton
 * @property mixed $created_by
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class TruckLanes extends \yii\mongodb\ActiveRecord
{
    const PARENTDIR = '/truck_lanes';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CSV_UPLOAD = 'csv_upload';
    const SCENARIO_CALCULATION = 'farecalulation';
    public $truck_lane, $truck_category, $type, $loading_capacity, $term, $charges, $today_price;
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'truck_lanes'];
    }
    
    public $csvfile;

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'source',
            'source_state',
            'destination',
            'destination_state',
            'distance',
            'price_per_km',
            'price_per_ton',
            'weight',
            'lane_rate',
            'price_per_ton',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['source', 'source_state', 'destination', 'destination_state', 'distance', 'price_per_ton', 'status', 'price_per_km'];
        $scenarios[self::SCENARIO_UPDATE] = ['source', 'source_state', 'destination', 'destination_state', 'distance', 'price_per_ton', 'status', 'price_per_km'];
        $scenarios[self::SCENARIO_CSV_UPLOAD] = ['source', 'source_state', 'destination', 'destination_state', 'source_state', 'destination_state', 'distance',  'price_per_ton', 'status', 'csvfile', 'price_per_km'];
        // $scenarios[self::SCENARIO_CALCULATION] = ['distance', 'price_per_km', 'price_per_ton', 'truck_category', 'type', 'term', 'charges'];
        $scenarios[self::SCENARIO_CALCULATION] = ['distance', 'weight', 'today_price'];
        return $scenarios;
    }
    
    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['source', 'source_state', 'destination', 'destination_state', 'distance', 'weight', 'price_per_ton', 'status', 'csvfile', 'price_per_km', 'truck_category', 'type'], 'required'],
            [['csvfile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xls, xlsx, csv'],
            [['price_per_ton', 'price_per_km', 'distance', 'weight'], 'number'],
            [['price_per_ton', 'price_per_km', 'distance', 'weight'], 'string', 'length' => [1, 5]],
			[['source', 'source_state', 'destination', 'destination_state'], 'string', 'length' => [2, 50]],
            [['source', 'source_state', 'destination', 'destination_state', 'distance', 'price_per_km', 'price_per_ton', 'created_by', 'status', 'created_on', 'modified_on', 'csvfile'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => 'status',
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'source' => Yii::t('app', 'Source'),
            'source_state' => Yii::t('app', 'Source State'),
            'destination' => Yii::t('app', 'Destination'),
            'destination_state' => Yii::t('app', 'Destination State'),
            'distance' => Yii::t('app', 'Distance ( in km )'),
            'weight' => Yii::t('app', 'Weight ( in ton )'),
            'lane_rate' => Yii::t('app', 'Lane Rate'),
            'price_per_km' => Yii::t('app', 'Price Per Km'),
            'price_per_ton' => Yii::t('app', 'Price Per Ton'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'csvfile' => Yii::t('app', 'CSV File'),
            'term' => Yii::t('app', 'Tax Terms'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			if(empty($this->created_by)) $this->created_by = new \MongoId(Yii::$app->user->getId());
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];			
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id != "update"){
			$this->status = $this->status ? 'Active' : 'Inactive';
			$this->created_by = Common::getOwner($this->created_by);        
			$this->created_on = Common::showDate($this->created_on);
			$this->modified_on = Common::showDate($this->modified_on);
		}
    }    
    
}