<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Moderators;

/**
 * ModeratorsSearch represents the model behind the search form about `common\models\Moderators`.
 */
class ModeratorsSearch extends Moderators
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'name', 'contact', 'firstname', 'lastname', 'email', 'mobile_number', 'password', 'hash_token', 'status', 'role', 'created_on', 'modified_on', 'isAdmin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params){
        $query = Moderators::find()->where(['isAdmin' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'name.firstname', $this->firstname])
            ->andFilterWhere(['like', 'name.lastname', $this->lastname])
            ->andFilterWhere(['like', 'contact.email', $this->email])
            ->andFilterWhere(['like', 'contact.mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'hash_token', $this->hash_token])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on])
            ->andFilterWhere(['like', 'isAdmin', $this->isAdmin])->orderBy('created_on DESC');
        //echo '<pre>'; print_r($query); echo '</pre>';

        return $dataProvider;
    }
}
