<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "tax_terms".
 *
 * @property \MongoId|string $_id
 * @property mixed $term
 * @property mixed $name
 * @property mixed $type
 * @property mixed $rate
 * @property mixed $priority
 * @property mixed $created_by
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class TaxTerms extends \yii\mongodb\ActiveRecord{

    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'tax_terms'];
    }

    /**
     * @inheritdoc
     */
    public function attributes(){
        return [
            '_id',
            'term',
            'name',
            'type',
            'rate',
            'priority',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['term', 'name', 'type', 'rate', 'priority', 'status'], 'required'],
            [['priority', 'rate'], 'number'],
            [['rate'], 'string', 'length' => [1, 5]],
            [['priority'], 'string', 'length' => [1, 2]],
            [['term', 'name', 'type', 'rate', 'priority', 'created_by', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['priority','status'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'term' => Yii::t('app', 'Tax Term'),
            'name' => Yii::t('app', 'Term Name'),
            'type' => Yii::t('app', 'Tax Type'),
            'rate' => Yii::t('app', 'Tax Rate'),
            'priority' => Yii::t('app', 'Term Priority'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			if(Yii::$app->controller->action->id == "create")
			$this->created_on = Common::currentTimeStamp();
            $this->created_by = Yii::$app->user->getId();
            if(!$this->isNewRecord)
            $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update"){
            $this->status = $this->status ? 'Active' : 'Inactive';
            $this->term = $this->term ? 'Tax' : 'Insurance';
            $this->type = $this->type ? 'Percentage' : 'Fixed';
        }
        $this->created_by = Common::getOwner($this->created_by);        
        $this->created_on = Common::showDate($this->created_on);
        $this->modified_on = Common::showDate($this->modified_on);
    }

}