<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "payments".
 *
 * @property \MongoId|string $_id
 * @property mixed $shipment_id
 * @property mixed $payer_id
 * @property mixed $ship_by
 * @property mixed $payment_type
 * @property mixed $total
 * @property mixed $outstanding
 * @property mixed $transactions
 * @property mixed $created_on
 * @property mixed $modified_on
 * @property mixed $status
 */
class Payments extends \yii\mongodb\ActiveRecord
{
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'payments'];
    }

    public $instalment, $mode;	
	
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'shipment_id',
            'payer_id',
            'ship_by',
            'payment_type',
            'total',
            'outstanding',
            'transactions',
            'created_on',
            'modified_on',
            'status',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment_id', 'payer_id', 'ship_by', 'payment_type', 'total', 'outstanding', 'transactions', 'created_on', 'modified_on', 'status'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['payment_type', 'status', 'instalment', 'mode', 'outstanding'],
        ];
    }	
	
    /**
     * @inheritdoc
     */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'shipment_id' => Yii::t('app', 'Shipment ID'),
            'payer_id' => Yii::t('app', 'Payer ID'),
            'ship_by' => Yii::t('app', 'Ship By'),
            'payment_type' => Yii::t('app', 'Payment Type'),
            'total' => Yii::t('app', 'Total'),
            'outstanding' => Yii::t('app', 'Outstanding'),
            'transactions' => Yii::t('app', 'Transactions'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($this->isNewRecord) $this->created_on = Common::currentTimeStamp();
			else $this->modified_on = Common::currentTimeStamp(); 
			return true;
        }else  return false;
    }
	
}