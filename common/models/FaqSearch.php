<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Faq;

/**
 * FaqSearch represents the model behind the search form about `common\models\Faq`.
 */
class FaqSearch extends Faq
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'topic', 'question', 'answer', 'created_on', 'modified_on', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $status  = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
         $topic  = $this->topic;
        if(isset($this->topic) && ($this->topic != '')) $topic = new \MongoId($this->topic);
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['topic' => $topic])
            ->andFilterWhere(['like', 'question.en', $this->question])
            ->andFilterWhere(['like', 'answer.en', $this->answer])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on])
            ->andFilterWhere(['status' => $status]);

        return $dataProvider;
    }
}
