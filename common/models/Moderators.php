<?php
namespace common\models;
use Yii;

/**
* This is the model class for collection "moderators".
*
* @property \MongoId|string $_id
* @property mixed $name
* @property mixed $contact
* @property mixed $password
* @property mixed $hash_token
* @property mixed $status
* @property mixed $role
* @property mixed $created_on
* @property mixed $modified_on
* @property mixed $isAdmin
*/
class Moderators extends \yii\mongodb\ActiveRecord{
    const PARENTDIR = '/moderators';
    const CHILDDIR = '/moderators';     
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    
    /**
    * @inheritdoc
    */
    public $firstname, $lastname, $email, $mobile_number;
    
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'moderators'];
    }

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'name',
            'contact',
            'password',
            'hash_token',
            'status',
            'role',
            'created_on',
            'modified_on',
            'isAdmin',
            'image',
            //'isAdmin',
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['firstname', 'lastname', 'email', 'mobile_number', 'password', 'image'];
        $scenarios[self::SCENARIO_UPDATE] = ['firstname', 'lastname', 'email', 'mobile_number'];
        return $scenarios;
    }
    
    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['firstname', 'lastname', 'email', 'mobile_number', 'password', 'status', 'role'], 'required'],
            ['email', 'email'],
            [['firstname', 'lastname'], 'match', 'pattern' => '/^[a-zA-Z]+$/','message'=>"Numbers not allowed in  name"],
            [['mobile_number'], 'integer','message'=>'Number must be numeric value like 9876543210, 01239876542'],
			// ['mobile_number', 'number'],
            [['firstname','lastname'], 'string', 'length' => [2, 25]],
            [['email'], 'unique','targetAttribute'=>'contact.email',],
            [['password'], 'string', 'length' => [6, 20]],
            [['mobile_number'], 'string', 'length' => [10, 10]],
            [['image'], 'required','when'=>function($model){
				return !empty($model->image);
			},'whenClient' => "function (attribute, value) {
                return ($('#moderators-image').get(0).files.length ==0 && $('#moderators-image').prev('input').val() == '');
            }"],
            [['name', 'contact', 'password', 'hash_token', 'status', 'role', 'created_on', 'modified_on', 'isAdmin', 'image'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['status', 'isAdmin', 'role']
        ];
    }    
    
	/* send Notification email */
    public function sendNotificationMail($model = false, $userType = null, $password){ //return true;
        $model = empty($this) ? $model : $this;
        $subject = "Welcome to Cargowala !";
        // $subject = "New ".$userType. " is added by CargoWala administrator !";
        $name = $model->name['firstname'].' '.$model->name['lastname'];
        $message = '<p>Hi ' . $name . '</p>
        <p>Your account is created as '.$userType.' by CargoWala administrator.</p>
        <p>You can login to your account by the following link.</p>
        <p><a href="' . Yii::$app->urlManager->createAbsoluteUrl(['site/login']). '" target="_blank"> click here to login </a></p>
        <p>Your credentials for the same are as below!</p><br/>
        <p>Email : '.$model->contact['email'].'</p>
        <p>Password : '.$password.'</p><br/><br/>
        <p>Cheers!! <br>
        Team CargoWala</p>';
        return Yii::$app->mailer->compose()
        ->setFrom(Yii::$app->params['supportEmail'])
        ->setTo($this->email)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($message)->send();
    }    
    
    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            /*'firstname' => Yii::t('app', 'First Name'),
            'lastname' => Yii::t('app', 'Last Name'),*/
            'contact' => Yii::t('app', 'Contact'),
            'password' => Yii::t('app', 'Password'),
            'hash_token' => Yii::t('app', 'Hash Token'),
            'status' => Yii::t('app', 'Status'),
            'role' => Yii::t('app', 'Role'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'isAdmin' => Yii::t('app', 'Is Admin'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            //echo '<pre>'; print_r($this->oldAttributes); die('beforeSave');
            if(Yii::$app->controller->action->id == "update"){
                if(!empty($this->password)){
                    $this->password = md5($this->password);
                    //if($this->oldAttributes['password'] != md5($this->password)){}
                }else{
                    $this->password = $this->oldAttributes['password'];
                }
            }else{
				$this->created_on = Common::currentTimeStamp();
                //$this->password = empty($this->password) ? NULL : md5($this->password);
            }
            $this->hash_token = empty($this->email) ? NULL : md5($this->email.time());
            $this->name = [
                 'firstname' => empty($this->firstname) ? NULL : $this->firstname,
                 'lastname' => empty($this->lastname) ? NULL : $this->lastname,
            ];
            $this->contact = [
                 'email' => empty($this->email) ? NULL : $this->email,
                 'mobile_number' => empty($this->mobile_number) ? NULL : $this->mobile_number,
            ];
            if(!$this->isNewRecord)
                $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update")
            $this->status = $this->status ? 'Active' : 'Inactive';
        $this->firstname = empty($this->name['firstname']) ? NULL : $this->name['firstname'];
        $this->lastname = empty($this->name['lastname']) ? NULL : $this->name['lastname'];
        $this->email = empty($this->contact['email']) ? NULL : $this->contact['email'];
        $this->mobile_number = empty($this->contact['mobile_number']) ? NULL : $this->contact['mobile_number'];
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
    }
    
}