<?php
namespace common\models;
use Yii;
/**
* This is the model class for collection "bids".
*
* @property \MongoId|string $_id
* @property mixed $shipment_id
* @property mixed $trucker_id
* @property mixed $price
* @property mixed $description
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/
class Bids extends \yii\mongodb\ActiveRecord{

	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'bids'];
    }
	const POSTED   =  0;	
    const ACCEPTED =  1;	
    const REJECTED =  2;

	/**
	* @inheritdoc
	*/
    public function attributes(){
        return [
            '_id',
            'shipment_id',
            'trucker_id',
            'price',
            'description',
            'status',
            'created_on',
            'modified_on',
        ];
    }

	/**
	* @inheritdoc
	*/
	public function rules(){
        return [
			[['price'],'required'],
            [['shipment_id', 'trucker_id', 'price', 'description', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

	/**
	* @inheritdoc
	*/
	public function attributeType(){
        return [
            'integer' => ['status','price'],
        ];
    }


	/**
	* @inheritdoc
	*/
	public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'shipment_id' => Yii::t('app', 'Shipment ID'),
            'trucker_id' => Yii::t('app', 'Trucker ID'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			$this->shipment_id = new \MongoId($this->shipment_id);
			$this->trucker_id = new \MongoId($this->trucker_id);
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }

	public function getUser(){
        return $this->hasOne(Users::className(), ['_id' => 'trucker_id']);
    }

	
	public function getShipment(){
        return $this->hasOne(Bookings::className(), ['_id' => 'shipment_id']);
    }
	// status as text...
	public function getStatusText($status){
		$statusText =[
		            0=>'Bid Posted',
			        1=>'Bid Accepted',
			        2=>'Bid Rejected',
		]; 
		return $statusText[$status];
	}
	
}