<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bookings;

/**
 * BookingsSearch represents the model behind the search form about `common\models\Bookings`.
 */
class BookingsSearch extends Bookings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shipment_id', 'loading', 'unloading', 'transit', 'truck', 'shipper_id', 'trucker_id', 'driver_id', 'vehicle_id', 'items', 'overall_weight', 'invoice_id', 'status', 'vehicle', 'shipment_detail', 'shipment_url', 'created_on', 'modified_on', 'lp_name', 'lp_address', 'lp_city', 'lp_state', 'lp_latitude', 'lp_longitude', 'up_name', 'up_address', 'up_city', 'up_state', 'up_latitude', 'up_longitude', 't_date', 't_time', 't_expdatetime', 'est_distance', 'est_time', 'start_date', 'end_date', 'category', 'ttype', 'quantity', 'load_type',  'priority_delivery', 'name', 'desc', 'parent_category', 'sub_category', 'weight', 'length', 'width', 'height', 'total_item', 'total_weight', 'is_insured', 'insurance_price', 'insurance', 'shipment_weight', 'shipment_type', 'priority_delivery_charges', 'repeat', 'invoicing_type'], 'safe']			
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bookings::find()->with(['username','bids']);
// echo '<pre>'; print_r($query); echo '</pre>';
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		// $shipment_type = $this->shipment_type ;
		// if(isset($this->shipment_type) && ($this->shipment_type != '')) $shipment_type = (int)$this->shipment_type ;
		$query->andFilterWhere(['like', '_id', $this->_id])
		->andFilterWhere(['like', 'shipment_id', $this->shipment_id])
		->andFilterWhere(['like', 'loading.lp_address', $this->loading])
		->andFilterWhere(['like', 'unloading.up_address', $this->loading])
		->andFilterWhere(['like', 'shipment_detail.transit_date', $this->transit])
		->andFilterWhere(['like', 'truck', $this->truck])
		->andFilterWhere(['like', 'overall_weight', $this->overall_weight])
		->andFilterWhere(['like', 'invoice_id', $this->invoice_id])
		
			
		->andFilterWhere(['like', 'shipment_detail', $this->shipment_detail])
		->andFilterWhere(['like', 'shipment_url', $this->shipment_url])
		->andFilterWhere(['like', 'created_on', $this->created_on])
		// ->andFilterWhere(['items_total'=>$items_total ])
		->andFilterWhere(['like', 'modified_on', $this->modified_on]);
		if(is_array($this->status))	
		   $query->andWhere(['status' =>['$in'=> $this->status]]);
		elseif(isset($this->status) && $this->status!="") $query->andWhere(['status' =>(int)$this->status]);
		if($this->shipper_id)
			$query->andFilterWhere(['shipper_id' => new \MongoId($this->shipper_id)]);
		else
			$query->andFilterWhere(['like', 'shipper_id', $this->shipper_id]);
		if($this->trucker_id)
			$query->andFilterWhere(['trucker_id' => new \MongoId($this->trucker_id)]);
		else
			$query->andFilterWhere(['like', 'trucker_id', $this->trucker_id]);
		if($this->shipment_type && !is_array($this->shipment_type))
			$query->andFilterWhere(['shipment_type.code' => (int)$this->shipment_type]);
		else
			$query->andFilterWhere(['shipment_type.code' => $this->shipment_type]);
		if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'pending'){
			$query->andFilterWhere(['>=', 'transit.t_expdatetime', time()]);
		}
		if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'bidboard')
			$query->andFilterWhere(['shipment_type.code' => 5]);
		if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'regular')
			$query->andFilterWhere(['shipment_type.code' => 1]);
		// echo '<pre>'; print_r($dataProvider); echo '</pre>';
		if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'new'){
			$query->andFilterWhere(['trucker_id' =>['$ne'=>true]]);
			$query->orderBy('created_on ASC');
		}else $query->orderBy('transit.t_expdatetime ASC');
		return $dataProvider;
    }
}
