<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "truckers".
 *
 * @property \MongoId|string $_id
 * @property mixed $name
 * @property mixed $contact
 * @property mixed $password
 * @property mixed $image
 * @property mixed $created_by
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class Truckers extends \yii\mongodb\ActiveRecord
{
     public $firstname, $lastname, $email, $mobile_number;
    
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return [Yii::$app->mongodb->defaultDatabaseName, 'truckers'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'contact',
            'password',
            'image',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'registered_address', 'office_address', 'landline_number','business_pancard', 'password', 'account_type', 'company_type', 'business_type', 'pancard', 'status', 'role'], 'required'],
//            [['mobile_number', 'landline_number'], 'integer'],
//            [['email'], 'email'],
//            [['mobile_number','landline_number'], 'string', 'length' => [8, 15]],            
            [['name', 'contact', 'password', 'image', 'created_by', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeType()
    {
        return [

        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'contact' => Yii::t('app', 'Contact'),
            'password' => Yii::t('app', 'Password'),
            'image' => Yii::t('app', 'Image'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
}