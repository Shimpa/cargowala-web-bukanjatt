<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Shipper;

/**
 * ShipperSearch represents the model behind the search form about `common\models\Shipper`.
 */
class ShipperSearch extends Shipper
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'firstname', 'lastname', 'email', 'mobile_number', 'password', 'mobile_tokens', 'hash_token', 'image', 'account_type', 'company_type', 'business_type', 'business', 'pancard', 'status', 'role', 'social', 'created_on', 'modified_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shipper::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'name.firstname', $this->firstname])
            ->andFilterWhere(['like', 'name.lastname', $this->lastname])
            ->andFilterWhere(['like', 'contact.mobile_number', $this->mobile_number])
            ->andFilterWhere(['like', 'otp', $this->otp])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'mobile_tokens', $this->mobile_tokens])
            ->andFilterWhere(['like', 'hash_token', $this->hash_token])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'company_type', $this->company_type])
            ->andFilterWhere(['like', 'business_type', $this->business_type])
            ->andFilterWhere(['like', 'business', $this->business])
            ->andFilterWhere(['like', 'pancard', $this->pancard])
            ->andFilterWhere(['status' => $status]);
//            ->andFilterWhere(['like', 'role', $this->role])
//            ->andFilterWhere(['like', 'social', $this->social])
//            ->andFilterWhere(['like', 'created_on', $this->created_on])
//            ->andFilterWhere(['like', 'modified_on', $this->modified_on]);
        return $dataProvider;
    }
}
