<?php

namespace common\models;

use Yii;
use common\models\Common;
/**
 * This is the model class for collection "membership".
 *
 * @property \MongoId|string $_id
 * @property mixed $name
 * @property mixed $description
 * @property mixed $price
 * @property mixed $duration
 * @property mixed $transactions
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class Membership extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return [Yii::$app->mongodb->defaultDatabaseName, 'membership'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'description',
            'price',
            'duration',
            'transactions',
            'status',
            'mtype',
            'created_on',
            'modified_on',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price', 'duration', 'transactions', 'status', 'mtype', 'created_on', 'modified_on'], 'safe']
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeType(){
        return [
            'integer' => ['status']	
		];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'duration' => Yii::t('app', 'Duration'),
            'transactions' => Yii::t('app', 'Transactions'),
            'status' => Yii::t('app', 'Status'),
            'mtype' => Yii::t('app', 'Membership Type'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
	
		public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($this->isNewRecord){
			   $this->created_on = Common::currentTimeStamp();
			   $this->status = 1;	
			}else $this->modified_on = Common::currentTimeStamp();
			if($this->mtype == 1){
				$this->mtype = [
					'code' => '1',
					'text' => 'Silver',
				];
			}else{
				$this->mtype = [
					'code' => '2',
					'text' => 'Gold',
				];				
			}
            return true;
        }else{
            return false;
        }
    }
	
    public function afterFind(){
        parent::afterFind(); 
        if(Yii::$app->controller->action->id != "update"){
            $this->status = $this->status ? 'Active' : 'Inactive';
        }
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
    }	
}
