<?php
namespace common\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TruckLanes;
/**
* TruckLanesSearch represents the model behind the search form about `common\models\TruckLanes`.
*/
class TruckLanesSearch extends TruckLanes{
    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['_id', 'source', 'source_state', 'destination', 'destination_state', 'distance', 'price_per_km', 'price_per_ton', 'created_by', 'status', 'created_on', 'modified_on'], 'safe'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function scenarios(){
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * Creates data provider instance with search query applied
    *
    * @param array $params
    *
    * @return ActiveDataProvider
    */
    public function search($params){
        $query = TruckLanes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    '_id' => SORT_DESC,
                ]
            ],            
        ]);
        //echo '<pre>'; print_r($params); echo '</pre>';
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            die('fails');
            return $dataProvider;
        }
        $status = $this->status;
        if(isset($this->status) && ($this->status != '')) $status = (int)$this->status;
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'source_state', $this->source_state])
            ->andFilterWhere(['like', 'destination', $this->destination])
            ->andFilterWhere(['like', 'destination_state', $this->destination_state])
            ->andFilterWhere(['like', 'distance', $this->distance])
            ->andFilterWhere(['like', 'price_per_km', $this->price_per_km])
            ->andFilterWhere(['like', 'price_per_ton', $this->price_per_ton])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['status' => $status])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on]);
            // ->andFilterWhere(['like', 'modified_on', $this->modified_on])->orderBy('created_on DESC');
        return $dataProvider;
    }
}