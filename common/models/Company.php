<?php
namespace common\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
* This is the model class for collection "Company".
*
* @property \MongoId|string $_id
* @property mixed $name
* @property mixed $desc
* @property mixed $created_by
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/

class Company extends \yii\mongodb\ActiveRecord{
    
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'Company'];
    }

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'name',
            'desc',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['name', 'desc', 'status'], 'required'],
            [['name', 'desc', 'created_by', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => 'status',
        ];
    }


    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', 'Desc'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
        
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->created_by = Yii::$app->user->getId();
            $this->created_on = Common::currentTimeStamp();
            if(!$this->isNewRecord)
            $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update")
            $this->status = $this->status ? 'Active' : 'Inactive';
        $this->created_by = Common::getOwner($this->created_by);
        $this->created_on = Common::showDate($this->created_on);
        $this->modified_on = Common::showDate($this->modified_on);
    }
    
    public function getCompaniesList(){
        $res = Company::find()->select(['_id, name'])->where(['status'=>1])->all();
        list($id, $name) = ['_id', 'name'];
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);
    }
    
}