<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "admin_settings".
 *
 * @property \MongoId|string $_id
 * @property mixed $admin_email
 * @property mixed $company_name
 * @property mixed $company_logo
 * @property mixed $lane_rate
 * @property mixed $insurance_rate
 * @property mixed $priority_delivery_rate
 * @property mixed $commision
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class AdminSettings extends \yii\mongodb\ActiveRecord{
    const PARENTDIR = '/companies';
    const CHILDDIR = '/company';
	
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'admin_settings'];
    }

    /**
	* @inheritdoc
	*/
	public function attributes(){
        return [
            '_id',
            'admin_email',
            'admin_contact',
            'company_name',
            'company_logo',
            'lane_rate',
            'insurance_rate',
            'priority_delivery_rate',
            'commision',
            'created_on',
            'modified_on',
        ];
    }

	/**
	* @inheritdoc
	*/
	public function rules(){
        return [
            [['lane_rate', 'insurance_rate', 'priority_delivery_rate'], 'required'],
            [['admin_email'], 'email'],
			[['lane_rate', 'insurance_rate', 'priority_delivery_rate'], 'integer', 'message'=>'Rate must be numeric value like 1.5, 2.0 or 2'],
			[['lane_rate', 'insurance_rate', 'priority_delivery_rate'], 'string', 'length' => [1, 5]],
            [['admin_email', 'admin_contact', 'company_name', 'company_logo', 'lane_rate', 'insurance_rate', 'priority_delivery_rate', 'commision', 'created_on', 'modified_on'], 'safe']
        ];
    }

	/**
	* @inheritdoc
	*/
	public function attributeType(){
        return [

        ];
    }

	/**
	* @inheritdoc
	*/
	public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'admin_email' => Yii::t('app', 'Admin Email'),
            'admin_contact' => Yii::t('app', 'Admin Contact'),
            'company_name' => Yii::t('app', 'Company Name'),
            'company_logo' => Yii::t('app', 'Company Logo'),
            'lane_rate' => Yii::t('app', 'Lane Rate (in Rupees)'),
            'insurance_rate' => Yii::t('app', 'Insurance Rate (Percentage)'),
            'priority_delivery_rate' => Yii::t('app', 'Priority Delivery Rate (Percentage)'),
            'commision' => Yii::t('app', 'Commision'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->company_logo  = basename($this->company_logo);
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];			
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
		$this->company_logo = empty($this->company_logo)? null : Common::onlyMediaPath($this) . $this->company_logo; 
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
    }
	
}