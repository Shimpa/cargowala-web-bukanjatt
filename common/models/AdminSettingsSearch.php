<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AdminSettings;

/**
 * AdminSettingsSearch represents the model behind the search form about `common\models\AdminSettings`.
 */
class AdminSettingsSearch extends AdminSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'admin_email',  'admin_contact', 'company_name', 'company_logo', 'lane_rate', 'insurance_rate', 'priority_delivery_rate', 'created_on', 'modified_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminSettings::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'admin_email', $this->admin_email])
            ->andFilterWhere(['like', 'admin_contact', $this->admin_contact])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'company_logo', $this->company_logo])
            ->andFilterWhere(['like', 'lane_rate', $this->lane_rate])
            ->andFilterWhere(['like', 'insurance_rate', $this->insurance_rate])
            ->andFilterWhere(['like', 'priority_delivery_rate', $this->priority_delivery_rate])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'modified_on', $this->modified_on]);

        return $dataProvider;
    }
}
