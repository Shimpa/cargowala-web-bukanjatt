<?php

namespace common\models;
use common\models\Company;
use common\models\Business;
use Yii;

/**
* This is the model class for collection "shipper".
*
* @property \MongoId|string $_id
* @property mixed $name
* @property mixed $contact
* @property mixed $otp
* @property mixed $password
* @property mixed $mobile_tokens
* @property mixed $hash_token
* @property mixed $image
* @property mixed $account_type
* @property mixed $company_type
* @property mixed $business_type
* @property mixed $business
* @property mixed $pancard
* @property mixed $status
* @property mixed $role
* @property mixed $social
* @property mixed $created_on
* @property mixed $modified_on
*/

class Shipper extends \yii\mongodb\ActiveRecord{

    const PARENTDIR = '/shippers';
    const CHILDDIR = '/shipper';    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';    
    public $confirm_password;
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'shipper'];
    }
    
    public $firstname, $lastname, $email, $mobile_number, $business_name, $registered_address, $office_address, $landline_number, $business_pancard;

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'name',
            'contact',
            'otp',
            'password',
            'mobile_tokens',
            'hash_token',
            'image',
            'account_type',
            'company_type',
            'business_type',
            'business',
            'pancard',
            'status',
            'role',
            'social',
            'created_on',
            'modified_on',
        ];
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'registered_address', 'office_address', 'landline_number', 'business_pancard', 'password', 'account_type', 'company_type', 'business_type', 'pancard', 'status', 'role'];
        $scenarios[self::SCENARIO_UPDATE] = ['firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'registered_address', 'office_address', 'landline_number','business_pancard', 'account_type', 'company_type', 'business_type', 'pancard', 'status', 'role'];
        return $scenarios;
    }    
    
    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'registered_address', 'office_address', 'landline_number','business_pancard', 'password', 'account_type', 'company_type', 'business_type', 'pancard', 'status', 'role'], 'required'],
            [['mobile_number', 'landline_number'], 'integer'],
            [['email'], 'email'],
            [['mobile_number','landline_number'], 'string', 'length' => [8, 15]],
            [['registered_address','office_address'], 'string', 'length' => [10, 500]],
            [['name', 'contact', 'otp', 'password', 'mobile_tokens', 'hash_token', 'image', 'account_type', 'company_type', 'business_type', 'business', 'pancard', 'status', 'role', 'social', 'created_on', 'modified_on', 'firstname', 'lastname', 'email', 'mobile_number', 'business_name', 'registered_address', 'office_address', 'landline_number','business_pancard'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => 'status',
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'contact' => Yii::t('app', 'Contact'),
            'otp' => Yii::t('app', 'Otp'),
            'password' => Yii::t('app', 'Password'),
            'mobile_tokens' => Yii::t('app', 'Mobile Tokens'),
            'hash_token' => Yii::t('app', 'Hash Token'),
            'image' => Yii::t('app', 'Image'),
            'account_type' => Yii::t('app', 'Account Type'),
            'company_type' => Yii::t('app', 'Company Type'),
            'business_type' => Yii::t('app', 'Business Type'),
            'business' => Yii::t('app', 'Business'),
            'pancard' => Yii::t('app', 'Shipper Pancard'),
            'status' => Yii::t('app', 'Status'),
            'role' => Yii::t('app', 'Role'),
            'social' => Yii::t('app', 'Social'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }

    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->created_on = Common::currentTimeStamp();
            if(Yii::$app->controller->action->id == "update"){
                if(!empty($this->password)){
                    $this->password = md5($this->password);
                    //if($this->oldAttributes['password'] != md5($this->password)){}
                }else{
                    $this->password = $this->oldAttributes['password'];
                }
            }else{
                $this->password = empty($this->password) ? NULL : md5($this->password);
            }
            $this->hash_token = empty($this->email) ? NULL : md5($this->email.time());
            $this->name = [
                 'firstname' => empty($this->firstname) ? NULL : $this->firstname,
                 'lastname' => empty($this->lastname) ? NULL : $this->lastname,
            ];
            $this->contact = [
                 'email' => empty($this->email) ? NULL : $this->email,
                 'mobile_number' => empty($this->mobile_number) ? NULL : $this->mobile_number,
            ];
            $this->business = [
                 'business_name' => empty($this->business_name) ? NULL : $this->business_name,
                 'registered_address' => empty($this->registered_address) ? NULL : $this->registered_address,
                 'office_address' => empty($this->office_address) ? NULL : $this->office_address,
                 'landline_number' => empty($this->landline_number) ? NULL : $this->landline_number,
                 'business_pancard' => empty($this->business_pancard) ? NULL : $this->business_pancard,
            ];
            if(!$this->isNewRecord)
                $this->modified_on = Common::currentTimeStamp();
            echo '<pre>'; print_r($this); die('beforeSave');
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        $this->firstname = empty($this->name['firstname']) ? NULL : $this->name['firstname'];
        $this->lastname = empty($this->name['lastname']) ? NULL : $this->name['lastname'];
        $this->email = empty($this->contact['email']) ? NULL : $this->contact['email'];
        $this->mobile_number = empty($this->contact['mobile_number']) ? NULL : $this->contact['mobile_number'];
        $this->business_name = empty($this->business['business_name']) ? NULL : $this->business['business_name'];
        $this->registered_address = empty($this->business['registered_address']) ? NULL : $this->business['registered_address'];
        $this->office_address = empty($this->business['office_address']) ? NULL : $this->business['office_address'];
        $this->landline_number = empty($this->business['landline_number']) ? NULL : $this->business['landline_number'];
        $this->business_pancard = empty($this->business['business_pancard']) ? NULL : $this->business['business_pancard'];
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
        if(Yii::$app->controller->action->id != "update"){
            $this->status = $this->status ? 'Active' : 'Inactive';
            $compDetail = Company::findOne($this->company_type);
            if($compDetail['name']){
                $this->company_type = $compDetail['name'];
            }
            $busiDetail = Business::findOne($this->business_type);
            if($busiDetail['name']){
                $this->business_type = $busiDetail['name'];
            }
            if(!empty($this->image)){
                $this->image = Common::getMediaPath($this,$this->image);
            }            
        }        
    }
    
    public function getAccountTypes($type = NULL){
        $accType = ['1' => 'Account Type 1', '2' => 'Account Type 2', '3' => 'Account Type 3', '4' => 'Account Type 4', '5' => 'Account Type 5', '6' => 'Account Type 6', '7' => 'Account Type 7'];
        if($type) return isset($accType[$type]) ? $accType[$type] : 'N-A';
        return $accType;
    }    
}