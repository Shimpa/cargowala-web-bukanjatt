<?php
namespace common\models;
use Yii;
use common\models\Bids;
use common\models\Categories;
use common\models\Drivers;
use common\models\Vehicles;

/**
* This is the model class for collection "bookings".
*
* @property \MongoId|string $_id
* @property mixed $loading
* @property mixed $unloading
* @property mixed $transit
* @property mixed $truck
* @property mixed $load
* @property mixed $shipper_id
* @property mixed $trucker_id
* @property mixed $driver_id
* @property mixed $vehicle_id
* @property mixed $items
* @property mixed $invoice_id
* @property mixed $status
* @property mixed $created_on
* @property mixed $modified_on
*/
class Bookings extends \yii\mongodb\ActiveRecord{
    const PARENTDIR = '/shipments';
    const CHILDDIR = '/shipment';   
    const REGULAR_BOARD = 1;
    const BID_BOARD =5;
    const ACCEPTED =6;
    const BID_ACCEPTED =6;
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'bookings'];
    }
    
    public $lp_name, $lp_address, $lp_city, $lp_state, $lp_latitude, $lp_longitude, $up_name, $up_address, $up_city, $up_state, $up_latitude, $up_longitude, $t_date, $t_time, $t_expdatetime, $est_distance, $est_time, $start_date, $end_date, $category, $ttype, $quantity, $load_type,  $priority_delivery, $name, $desc, $parent_category, $sub_category, $weight, $length, $width, $height, $total_item, $total_weight, $is_insured, $insurance_price, $repeat, $driver;

    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'shipment_id',
            'loading',
            'unloading',
            'transit',
            'truck',
            'shipper_id',
            'trucker_id',
            'driver_id',
            'vehicle_id',
            'items',
            'overall_weight',
            'overall_total',
            'invoice_id',
            'insurance',
            'status',
            'vehicle',
            'shipment_detail',
            'shipment_weight',
            'priority_delivery_charges',
            'shipment_type',
            'shipment_url',
            'invoicing_type',
            'created_on',
            'modified_on',
            'date',
            'items_total',
            // 'bids',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules(){
        return [
            [['shipment_id', 'loading', 'unloading', 'transit', 'truck', 'load', 'shipper_id', 'trucker_id', 'driver_id', 'vehicle_id', 'items', 'overall_weight', 'overall_total', 'invoice_id', 'status', 'vehicle', 'shipment_detail', 'shipment_url', 'created_on', 'modified_on', 'lp_name', 'lp_address', 'lp_city', 'lp_state', 'lp_latitude', 'lp_longitude', 'up_name', 'up_address', 'up_city', 'up_state', 'up_latitude', 'up_longitude', 't_date', 't_time', 't_expdatetime', 'est_distance', 'est_time', 'start_date', 'end_date', 'category', 'ttype', 'quantity', 'load_type',  'priority_delivery', 'name', 'desc', 'parent_category', 'sub_category', 'weight', 'length', 'width', 'height', 'total_item', 'total_weight', 'is_insured', 'insurance_price', 'insurance', 'shipment_weight', 'shipment_type', 'priority_delivery_charges', 'repeat', 'invoicing_type', 'date', 'items_total'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => ['status', 'category', 'load_type', 'quantity', 'load_type', 'priority_delivery', 'invoicing_type']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'shipment_id' => Yii::t('app', 'Shipment ID'),
            'loading' => Yii::t('app', 'Loading'),
            'unloading' => Yii::t('app', 'Unloading'),
            'transit' => Yii::t('app', 'Transit'),
            'truck' => Yii::t('app', 'Truck'),
            'shipper_id' => Yii::t('app', 'Shipper ID'),
            'trucker_id' => Yii::t('app', 'Trucker ID'),
            'driver_id' => Yii::t('app', 'Driver ID'),
            'vehicle_id' => Yii::t('app', 'Vehicle ID'),
            'items' => Yii::t('app', 'Items'),
            'overall_weight' => Yii::t('app', 'Overall  Weight'),
            'overall_total' => Yii::t('app', 'Overall  Total'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'status' => Yii::t('app', 'Status'),
            'shipment_detail' => Yii::t('app', 'Shipment Detail'),
            'shipment_url' => Yii::t('app', 'Shipment URL'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			$expDateTime = time();
			if(!empty($this->t_date) && !empty($this->t_time)){
				$datetime = $this->t_date.' '.$this->t_time;
				$expDateTime = strtotime($datetime)*1000;
				$isoDate = Common::convertDate($datetime);
			}
			if(empty($this->repeat)){
				$this->loading = [
					// 'lp_name' => empty($this->lp_name) ? NULL : $this->lp_name,
					'lp_address' => empty($this->lp_address) ? NULL : $this->lp_address,
					'lp_city' => empty($this->lp_city) ? NULL : $this->lp_city,
					'lp_state' => empty($this->lp_state) ? NULL : $this->lp_state,
					'lp_latitude' => empty($this->lp_latitude) ? NULL : $this->lp_latitude,
					'lp_longitude' => empty($this->lp_longitude) ? NULL : $this->lp_longitude,
				];
				$this->unloading = [
					// 'up_name' => empty($this->up_name) ? NULL : $this->up_name,
					'up_address' => empty($this->up_address) ? NULL : $this->up_address,
					'up_city' => empty($this->up_city) ? NULL : $this->up_city,
					'up_state' => empty($this->up_state) ? NULL : $this->up_state,
					'up_latitude' => empty($this->up_latitude) ? NULL : $this->up_latitude,
					'up_longitude' => empty($this->up_longitude) ? NULL : $this->up_longitude,
				];
				$this->truck = [
					'category' => empty($this->category) ? NULL : $this->category,
					'ttype' => empty($this->ttype) ? NULL : new \MongoId($this->ttype),
					'quantity' => empty($this->quantity) ? NULL : $this->quantity,
					'load_type' => empty($this->load_type) ? 0 : $this->load_type,
					'priority_delivery' => empty($this->priority_delivery) ? 0 : $this->priority_delivery,
				];
			}
            $this->transit = [
				't_date' => empty($this->t_date) ? NULL : $this->t_date,
				't_time' => empty($this->t_time) ? NULL : $this->t_time,
				't_expdatetime' => $expDateTime,
				'date' => empty($this->t_date) ? NULL : $isoDate,
				'est_distance' => empty($this->est_distance) ? NULL : $this->est_distance,
				'est_time' => empty($this->est_time) ? NULL : $this->est_time,
				'start_date' => empty($this->transit['start_date']) ? NULL : $this->transit['start_date'],
				'end_date' => empty($this->transit['end_date']) ? NULL : $this->transit['end_date'],
            ];
            /*$this->items = [
                 'name' => empty($this->name) ? NULL : $this->name,
                 'desc' => empty($this->desc) ? NULL : $this->desc,
                 'parent_category' => empty($this->parent_category) ? NULL : $this->parent_category,
                 'sub_category' => empty($this->sub_category) ? NULL : $this->sub_category,
                 'weight' => empty($this->weight) ? NULL : $this->weight,
                 'dimension' => empty($this->dimension) ? NULL : $this->dimension,
                 'total' => empty($this->total) ? NULL : $this->total,
                 'is_insured' => empty($this->is_insured) ? NULL : $this->is_insured,
                 'insurance_image' => empty($this->insurance_image) ? NULL : basename($this->insurance_image),
                 'insurance_price' => empty($this->insurance_price) ? NULL : $this->insurance_price,
            ];*/
            if($this->isNewRecord) $this->created_on = Common::currentTimeStamp();
			else $this->modified_on = Common::currentTimeStamp();			
            return true;
        }else{
            return false;
        }
    }

    public function afterFind(){
        parent::afterFind();
        $this->lp_name = empty($this->loading['lp_name']) ? NULL : $this->loading['lp_name'];
        $this->lp_address = empty($this->loading['lp_address']) ? NULL : $this->loading['lp_address'];
        $this->lp_city = empty($this->loading['lp_city']) ? NULL : $this->loading['lp_city'];
        $this->lp_state = empty($this->loading['lp_state']) ? NULL : $this->loading['lp_state'];
        $this->lp_latitude = empty($this->loading['lp_latitude']) ? NULL : $this->loading['lp_latitude'];
        $this->lp_longitude = empty($this->loading['lp_longitude']) ? NULL : $this->loading['lp_longitude'];
        $this->up_name = empty($this->unloading['up_name']) ? NULL : $this->unloading['up_name'];
        $this->up_address = empty($this->unloading['up_address']) ? NULL : $this->unloading['up_address'];
        $this->up_city= empty($this->unloading['up_city']) ? NULL : $this->unloading['up_city'];
        $this->up_state = empty($this->unloading['up_state']) ? NULL : $this->unloading['up_state'];
        $this->up_latitude = empty($this->unloading['up_latitude']) ? NULL : $this->unloading['up_latitude'];
        $this->up_longitude = empty($this->unloading['up_longitude']) ? NULL : $this->unloading['up_longitude'];
        $this->t_date = empty($this->transit['t_date']) ? NULL : $this->transit['t_date'];
        $this->t_time = empty($this->transit['t_time']) ? NULL : $this->transit['t_time'];
        $this->t_expdatetime = empty($this->transit['t_expdatetime']) ? NULL : $this->transit['t_expdatetime'];
        $this->est_distance = empty($this->transit['est_distance']) ? NULL : $this->transit['est_distance'];
        $this->est_time = empty($this->transit['est_time']) ? NULL : $this->transit['est_time'];
        $this->category = empty($this->truck['category']) ? NULL : $this->truck['category'];
        $this->ttype = empty($this->truck['ttype']) ? NULL : $this->truck['ttype'];
        $this->quantity = empty($this->truck['quantity']) ? NULL : $this->truck['quantity'];
        $this->load_type = empty($this->truck['load_type']) ? NULL : $this->truck['load_type'];
        $this->priority_delivery = empty($this->truck['priority_delivery']) ? NULL : $this->truck['priority_delivery'];
    }

	public function getBidprice(){
        return $this->hasOne(Bids::className(), ['shipment_id' => '_id']);
    }
	
	public function getTruckers(){
        return $this->hasOne(Users::className(), ['_id' => 'trucker_id']);
    }

	public function getShipper(){
        return $this->hasOne(Users::className(), ['_id' => 'shipper_id']);
    }

	 public function getUsername(){
		 return $this->hasOne(Users::className(), ['_id' => 'shipper_id']);
	 }
	 
	public function getBids(){
        return $this->hasMany(Bids::className(), ['shipment_id' => '_id'])->andWhere(['status'=>Bids::POSTED]);
    }
	
	public function getPayments(){
		 return $this->hasOne(Payments::className(), ['shipment_id' =>'_id']);
	 }
	
	public function getBidsCount($id, $bids){
		$counter = count($bids);
		$bids = $counter;
		$data = $bids;
		if($counter > 0){
			$data .= "<a class='viewBid $id' ><br/>View Bid</a>";
		}
		if($counter > 1){
			$data .= "<a rel='$id' class='viewBid'><br/>View Bids </a>";
		}
		return $data;
    }
	/* get truck category */
	public function getTruckCategory($truck){
      $types = Common::getTruckTypes();
	  try{	
	  $category  = $truck['category']; 	
	  return $types[$category]; 	
	  }catch(Exception $e){}
	}
	/* booking status text  and codes*/
	public function bookingStatus(){
		return [
		    0=>"Pending",
	        1=>"Trucker Canceled",
	        2=>"Shipper Canceled",
	        3=>"Expired",
	        4=>"In-Transit",
	        5=>"Completed",
	        6=>"Accepted",
	        8=>"Bid Accepted",
	        9=>"In-Complete",	
		];
	}
		
	/* get booking status text by id*/
	public function getStatusText($code){
		try{
		   $status = self::bookingStatus();
		   return $status[$code];			
		}catch(Exception $e){
			
		}
	}
	/* get css class according order status */
	public function getStatusClass($status){
		$classes =  [
		           0=>'status-pending', 	
		           1=>'status-cancelled', 	
		           2=>'status-cancelled', 	
		           3=>'status-expired', 	
		           4=>'status-intransit', 	
		           5=>'status-completed', 	
		           6=>'status-accepted', 	
		           8=>'status-accepted',
			       9=>'status-incomplete'
		];
		return empty($classes[$status])?null:$classes[$status];
	}
	/* validate is bid load board or regular board*/
	public function isBidBoard(){
		print_r($this->modelClass);die;
	}
	/* booking items list */
	public function Items($items){
		$html ='<ul>';
		foreach($items as $_items){
			$html.='<li> <ul>';
			  foreach($_items as $label=>$item){
				if(in_array($label,['id','_id'])) continue;  
			       $html.='<li><label>'.ucwords(str_replace("_"," ",$label)) .'</label> : ';
				if(in_array($label,['parent_category','sub_category']))  
				   $html.='<span>'. Categories::getName($item) . '</span></li>';
				else $html.='<span>'. $item . '</span></li>';  
			  }
			$html.='</ul></li>';
		}
		$html.'</ul>';
		return $html;
	}
	/* show invoice total of shipment */
	public function getInvoice($shipment){
		$detail  = $shipment->shipment_detail;
		$html='<ul>';
		$html.='<li><label>Shipment Weight: <label> '.$detail['shipment_weight'] .' Ton(s)</li>';
		if(!empty($shipment->items_total))
		   $html.='<li><label>Items Total :</label> '.$shipment->items_total .'</li>';
		if(!empty($shipment->invoice_total))
		   $html.='<li><label>Items Total :</label>'.$shipment->invoice_total .'</li>';
		$html.='<li><label>Load fare: Rs '.$detail['load_fare'] .'</li>';
		if(!empty($detail['insurance_charges']))
		   $html.='<li><label>Insurance Charges: Rs '.$detail['insurance_charges'] .'</li>';
		if(!empty($detail['priority_delivery_charges']))
		   $html.='<li><label>Priority Charges: Rs '.$detail['priority_delivery_charges'] .'</li>';
		if(!empty($shipment->shipment_type['code']) && 
		   $shipment->shipment_type['code']==self::REGULAR_BOARD){
			       $html.='<li>Taxes<br/><ul>';
		if(!empty($detail['taxes'])){
		   foreach($detail['taxes'] as  $label=>$tax){
		           $html.="<li><label>{$tax['name']} : {$tax['rate']}" .'</li>';
		   }
		}
		           $html.'</ul></li>';
		}
		if(!empty($shipment->shipment_type['code']) && 
		   $shipment->shipment_type['code']==self::REGULAR_BOARD){
		$html.='<li><hr/><label>Overall Total: Rs '.empty($detail['overall_total'])?0:$detail['overall_total'] .'</li>';
		}else{
		 if($shipment->status==self::ACCEPTED){	
		 $html.='<li><hr/><label>Final Bid Price: Rs '.empty($detail['bid_price'])?0:$detail['bid_price'] .'</li>';	
		 }else{
			$html.='<li><hr/><label>Overall Total: Rs '.empty($detail['overall_total'])?0:$detail['overall_total'] .'<br/><em>no bid accepted</em></li>';
			 
		 }
		}
		$html.='</ul>';
		return $html;
		
	}
	/* get assigned Vehicles and Drivers */
	public function getVehicles($data){ 
		$html='<ul>';
		$html.='<li>';
		$count =0;
		if(!empty($data->vehicle)){
		    $vehicles = '<ul>';
		    foreach($data->vehicle as $vehicle){
				if(!empty($vehicle['driver']) || !empty($vehicle['id'])) ++$count;
			    $vehicles.='<li>';
			    if(!empty($vehicle['driver']))
			       $vehicles.='<label> Driver :</label> <strong>' . Drivers::getNameandNo($vehicle['driver']).'</strong>';				
			    if(!empty($vehicle['id']))
			       $vehicles.= '<label> Vehicle No.</label> '.Vehicles::getRegNo($vehicle['id']);
			       $vehicles.'</li>';
		    }
		    $vehicles.='</ul>';
		}
		if(empty($data->vehicle)){
			$vehicles = '<ul>';
			$vehicles.='<li>No Vehicle/Driver Assigned</li>';
		    $vehicles.='</ul>';
		}
		$html.="{$count} Assigned  {$vehicles}</li></ul>";
		return $html;
	}
	/* get type of shipment and give option to change shipment type*/
	public function shipmentType($model){
		$text =  $model->shipment_type['text'];
		if($model->shipment_type['code']==self::REGULAR_BOARD && 
		   Yii::$app->controller->id=='loadboard'){
		   $text .=  "  ||  <a href='javascript:;' ng-click=\"changeShipmentType('".(string)$model->_id."')\" >Post on Bid Load Board</a>"; 
		}return $text;
	}
	 
}