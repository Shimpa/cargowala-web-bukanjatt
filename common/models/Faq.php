<?php
namespace common\models;
use Yii;
/**
* This is the model class for collection "faq".
*
* @property \MongoId|string $_id
* @property mixed $topic
* @property mixed $question
* @property mixed $answer
* @property mixed $created_on
* @property mixed $modified_on
* @property mixed $status
*/

class Faq extends \yii\mongodb\ActiveRecord{
    
	public $en_title;
	
	/**
	* @inheritdoc
	*/
	public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'faq'];
    }

	/**
	* @inheritdoc
	*/
	public function attributes(){
        return [
            '_id',
            'topic',
            'question',
            'answer',
            'created_on',
            'modified_on',
            'status',
        ];
    }
	
    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
             'integer' => ['status']
        ];
    }	

	/**
	* @inheritdoc
	*/
	public function rules(){
        return [
            [['topic', 'question', 'answer', 'status'], 'required'],
            [['topic', 'question', 'answer', 'created_on', 'modified_on', 'status'], 'safe']
        ];
    }

	/**
	* @inheritdoc
	*/
	public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'topic' => Yii::t('app', 'Topic'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
			if(!empty($this->topic)) $this->topic = new \MongoId($this->topic);
            return true;
        }else{
            return false;
        }
		// echo '<pre>'; print_r($insert); die('beforeSave');
    }	
	
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update")
            $this->status = $this->status ? 'Active' : 'Inactive';
        $this->en_title = empty($this->question['en']) ? '' : $this->question['en'];
        // $this->description = empty($this->description['en']) ? '' : $this->description['en'];
        $this->created_on = Common::showDate($this->created_on);
        $this->modified_on = Common::showDate($this->modified_on);
    }

}