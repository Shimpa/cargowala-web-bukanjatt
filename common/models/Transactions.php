<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "transactions".
 *
 * @property \MongoId|string $_id
 * @property mixed $amount
 * @property mixed $payment_id
 * @property mixed $debug
 * @property mixed $ttype
 * @property mixed $user_id
 * @property mixed $created_on
 * @property mixed $modified_on
 * @property mixed $status
 */
class Transactions extends \yii\mongodb\ActiveRecord
{
	/**
	* @inheritdoc
	*/
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'transactions'];
    }	

    /**
     * @inheritdoc
     */
    public function attributes(){
        return [
            '_id',
            'amount',
            'payment_id',
            'debug',
            'ttype',
            'user_id',
            'created_on',
            'modified_on',
            'status',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'payment_id', 'debug', 'ttype', 'user_id', 'created_on', 'modified_on', 'status'], 'safe']
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
            'integer' => []
        ];
    }
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'amount' => Yii::t('app', 'Amount'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'debug' => Yii::t('app', 'Debug'),
            'ttype' => Yii::t('app', 'Ttype'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
	
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            if($this->isNewRecord) $this->created_on = Common::currentTimeStamp();
			else $this->modified_on = Common::currentTimeStamp(); 
			return true;
        }else  return false;
    }

}