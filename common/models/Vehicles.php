<?php
namespace common\models;
use Yii;
use yii\web\UploadedFile;
use common\models\Common;
use common\models\Trucks;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for collection "vehicles".
 *
 * @property \MongoId|string $_id
 * @property mixed $truck_category
 * @property mixed $type
 * @property mixed $vehicle_number
 * @property mixed $permit
 * @property mixed $registration
 * @property mixed $insurance
 * @property mixed $owner_id
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class Vehicles extends \yii\mongodb\ActiveRecord
{
    const PARENTDIR = '/vehicles';
    const CHILDDIR = '/vehicle';    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_ADMIN_CREATE = 'admin_create';
    const SCENARIO_ADMIN_UPDATE= 'admin_update';
    
    /**
    * @inheritdoc
    */
    public static function collectionName(){
        return [Yii::$app->mongodb->defaultDatabaseName, 'vehicles'];
    }
    
    public $pic, $pnumber, $permit_issue_date, $permit_expiry_date, $rc_pic, $rc_number, $image, $issue_date, $renew_date, $rc_expiry_date;
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'truck_category',
            'truck_type',
            'loading_capacity',
            'vehicle_number',
            'permit',
            'registration',
            'insurance',
            'owner_id',
            'created_by',
            'status',
            'truck_status',
            'is_available',
            'compliance',
            'created_on',
            'modified_on',
        ];
    }
    
    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADMIN_CREATE] = ['truck_category', 'truck_type', 'loading_capacity', 'vehicle_number', 'pnumber', 'permit_issue_date', 'permit_expiry_date', 'issue_date', 'renew_date', 'status', 'compliance', 'rc_expiry_date', 'pic', 'rc_pic', 'image', 'owner_id'];
        $scenarios[self::SCENARIO_ADMIN_UPDATE] = ['truck_category', 'truck_type', 'loading_capacity', 'vehicle_number', 'pnumber', 'permit_issue_date', 'permit_expiry_date', 'issue_date', 'renew_date', 'status', 'compliance', 'rc_expiry_date', 'pic', 'rc_pic', 'image', 'owner_id'];
        $scenarios[self::SCENARIO_CREATE] = ['truck_category', 'truck_type', 'loading_capacity', 'vehicle_number', 'pnumber', 'permit_issue_date', 'permit_expiry_date', 'issue_date', 'renew_date', 'status',  'compliance','rc_expiry_date','pic','rc_pic','image'];
        $scenarios[self::SCENARIO_UPDATE] = ['truck_category', 'truck_type', 'loading_capacity','vehicle_number', 'pnumber', 'permit_issue_date', 'permit_expiry_date', 'issue_date', 'renew_date', 'status',  'compliance','rc_expiry_date','pic','rc_pic','image'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            // [['truck_category', 'truck_type', 'pnumber', 'permit_expiry_date', 'rc_expiry_date', 'renew_date', 'status', 'vehicle_number', 'owner_id'], 'required'],
            [['truck_category', 'truck_type', 'vehicle_number', 'pnumber',  'permit_expiry_date', 'renew_date', 'status', 'rc_expiry_date', 'owner_id'], 'required'],
            [['vehicle_number'], 'unique'],
            [['vehicle_number'], 'match','pattern'=>'/^[a-zA-Z0-9]+$/','message'=>'Vehicle RC is not valid no.'],
            [['loading_capacity'], 'number'],
            [['loading_capacity'], 'string', 'length' => [1, 4]],
            [['vehicle_number'], 'string', 'length' => [8, 20]],
            [['pnumber'], 'string', 'length' => [8, 20]],
            [['pnumber'], 'findunique', 'params'=>'permit.pnumber'],
            // [['rc_number'], 'findunique', 'params'=>'rc_number'],
			[['pic'], 'required','when'=>function($model){
				return !empty($model->pic);
			},'whenClient' => "function (attribute, value) {
				return ($('#vehicles-pic').get(0).files.length ==0 && $('#vehicles-pic').prev('input').val()=='');
			}"],
			[['rc_pic'], 'required','when'=>function($model){
				return !empty($model->rc_pic);
			},'whenClient' => "function (attribute, value) {
				return ($('#vehicles-rc_pic').get(0).files.length ==0 && $('#vehicles-rc_pic').prev('input').val()=='');
			}"],
			[['image'], 'required','when'=>function($model){
				return !empty($model->image);
			},'whenClient' => "function (attribute, value) {
				return ($('#vehicles-image').get(0).files.length ==0 && $('#vehicles-image').prev('input').val()=='');
			}"],
			[['pic', 'rc_pic', 'image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif, jpeg'],
			[['truck_category', 'truck_type', 'vehicle_number', 'loading_capacity', 'pic', 'pnumber', 'permit_issue_date', 'permit_expiry_date','rc_expiry_date', 'owner_id', 'created_by', 'status', 'truck_status', 'is_available', 'created_on', 'modified_on', 'issue_date', 'renew_date', 'rc_pic', 'rc_number', 'compliance', 'image'], 'safe'],
        ];
    }

    /**
    * @inheritdoc
    */
    public function attributeType(){
        return [
             'integer' => ['status', 'truck_status', 'is_available', 'truck_category']
        ];
    }
   
    public function findunique($attr, $params){
        $res = self::find()->select([$attr])->where([$attr=>$this->$attr])->one();
        if(!empty($res))
            $this->addError($attr, "$attr must be unique");        
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels(){
        return [
            '_id' => Yii::t('app', 'ID'),
            'truck_category' => Yii::t('app', 'Vehicle Category'),
            'truck_type' => Yii::t('app', 'Vehicle Type'),
            'loading_capacity' => Yii::t('app', 'Loading Capacity ( in Tons )'),
            'vehicle_number' => Yii::t('app', 'Vehicle Number'),
            'permit' => Yii::t('app', 'Permit'),
            'pnumber' => Yii::t('app', 'Permit Number'),
            'pic' => Yii::t('app', 'Permit Image'),
            'registration' => Yii::t('app', 'Registration'),
            'rc_pic' => Yii::t('app', 'RC Image'),
            'rc_expiry_date' => Yii::t('app', 'RC Expiry Date'),
            'rc_number' => Yii::t('app', 'Vehicle / RC number'),
            'insurance' => Yii::t('app', 'Insurance'),
            'image' => Yii::t('app', 'Insurance Image'),
            'issue_date' => Yii::t('app', 'Insurance Issue Date'),
            'renew_date' => Yii::t('app', 'Insurance Renew Date'),
            'owner_id' => Yii::t('app', 'Owner Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Vehicle Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
            $this->permit = [
                // 'pic' => empty($this->permit['pic']) ? NULL : $this->permit['pic'],
                'pic' => empty($this->pic) ? NULL : basename($this->pic),
                'pnumber' => empty($this->pnumber) ? NULL : $this->pnumber,
                'permit_expiry_date' => empty($this->permit_expiry_date) ? NULL : Common::convertDate($this->permit_expiry_date),
            ];
            $this->registration = [
				// 'rc_pic' => empty($this->registration['rc_pic']) ? NULL : $this->registration['rc_pic'],
				'rc_pic' => empty($this->rc_pic) ? NULL : basename($this->rc_pic),
				'vehicle_number' => empty($this->vehicle_number) ? NULL : $this->vehicle_number,
				'expiry_date' => empty($this->rc_expiry_date) ? NULL : Common::convertDate($this->rc_expiry_date),
            ];
            $this->insurance = [
                 // 'image' => empty($this->insurance['image']) ? NULL : $this->insurance['image'],
                 'image' => empty($this->image) ? NULL : basename($this->image),
                 'renew_date' => empty($this->renew_date) ? NULL : Common::convertDate($this->renew_date),
            ];
			//unset($this->vehicle_number);
            $this->compliance = Common::setCompliance($this);
			$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
            if($this->isNewRecord){
				$id = !empty(Yii::$app->request->post('_id')) ? Yii::$app->request->post('_id') :  Yii::$app->user->getId();
				if(empty($this->created_by)) $this->created_by = new \MongoId($id);
				if(!empty(Yii::$app->request->post('_id'))) $this->owner_id = new \MongoId($id);
				if(!empty($this->owner_id)) $this->owner_id = new \MongoId($this->owner_id);
				if(!empty($this->truck_type)) $this->truck_type = new \MongoId($this->truck_type);
			}else{
				$this->modified_on = Common::currentTimeStamp();
				$this->created_by =  $this->oldAttributes['created_by'];
				$this->owner_id =  $this->oldAttributes['owner_id'];
				$this->truck_type =  $this->oldAttributes['truck_type'];
			}
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind(); 
        if(Yii::$app->controller->action->id != "update"){
            $this->status = $this->status ? 'Active' : 'Inactive';
            $this->truck_type = Trucks::getName($this->truck_type);
            $this->truck_category = Common::getTruckTypes($this->truck_category);
            if($this->truck_status == 0) $this->truck_status =  'Not Assigned'; 
            if($this->truck_status == 1) $this->truck_status =  'Assigned'; 
            if($this->truck_status == 2) $this->truck_status =  'In-Transit'; 
            $this->is_available = $this->is_available ? 'Available' : 'Not Available'; 			
        }
		$this->owner_id = Common::getOwnerName($this->owner_id);
        $this->pnumber = empty($this->permit['pnumber']) ? NULL : $this->permit['pnumber'];
        $this->pic = empty($this->permit['pic']) ? NULL : Common::onlyMediaPath($this) . $this->permit['pic'];
        $this->rc_pic = empty($this->registration['rc_pic']) ? NULL : Common::onlyMediaPath($this) . $this->registration['rc_pic'];
        $this->image = empty($this->insurance['image']) ? NULL : Common::onlyMediaPath($this) . $this->insurance['image'];
        $this->permit_expiry_date = empty($this->permit['permit_expiry_date']) ? NULL : Common::showDateFrontend($this->permit['permit_expiry_date']);
        $this->renew_date = empty($this->insurance['renew_date']) ? NULL : Common::showDateFrontend($this->insurance['renew_date']);
        $this->vehicle_number = empty($this->registration['vehicle_number']) ? NULL : $this->registration['vehicle_number'];
        $this->rc_expiry_date = empty($this->registration['expiry_date']) ? NULL : Common::showDateFrontend($this->registration['expiry_date']);
        $this->created_on = empty($this->created_on) ? NULL : Common::showDate($this->created_on);
        $this->modified_on = empty($this->modified_on) ? NULL : Common::showDate($this->modified_on);
    }
    
    public function getTruckList($type = NULL){
        $trucks = ['1' => 'Tata Ace', '2' => 'Pick up', '3' => 'Tata 407', '4' => '14 Feet Open', '5' => '14 Feet Close', '6' => '17 Feet Open', '7' => '17 Feet Close'];
        if($type) return isset($trucks[$type]) ? $trucks[$type] : 'N-A';
        return $trucks;
    }
    
    public static  function getTruckTypes(){
        $res = Trucks::find()->select(['_id','name'])->where(['status'=>1])->all();
        list($id,$name) = ['_id','name'];
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);
    }
	
	public function getOwner(){
        return $this->hasOne(Users::className(), ['_id' => 'owner_id']);
    }	
	/* get vehcile registration no */
	public function getRegNo($vehicle_id){
		try{
		   $res = Vehicles::find()->select(['registration.vehicle_number'])->where(['_id'=>$vehicle_id])->one();
		if($res==null)return;
		   return $res->registration['vehicle_number'];
		}catch(Exception $e){}
	}

}