<?php

namespace common\models;
use common\models\Moderators;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for collection "categories".
 *
 * @property \MongoId|string $_id
 * @property mixed $parent_id
 * @property mixed $name
 * @property mixed $desc
 * @property mixed $image
 * @property mixed $created_by
 * @property mixed $status
 * @property mixed $created_on
 * @property mixed $modified_on
 */
class Categories extends \yii\mongodb\ActiveRecord{
    
    const PARENTDIR = '/categories';
    const CHILDDIR = '/category';
    
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return [Yii::$app->mongodb->defaultDatabaseName, 'categories'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'parent_id',
            'name',
            'desc',
            'image',
            'created_by',
            'status',
            'created_on',
            'modified_on',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'desc', 'status'], 'required'],
//            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['parent_id', 'name', 'desc', 'image', 'created_by', 'status', 'created_on', 'modified_on'], 'safe']
        ];
    }

     /**
     * @inheritdoc
     */
    public function attributeType()
    {
        return [
            'integer' => 'status',
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'name' => Yii::t('app', 'Name'),
            'desc' => Yii::t('app', 'Desc'),
            'image' => Yii::t('app', 'Image'),
            'created_by' => Yii::t('app', 'Created By'),
            'status' => Yii::t('app', 'Status'),
            'created_on' => Yii::t('app', 'Created On'),
            'modified_on' => Yii::t('app', 'Modified On'),
        ];
    }
    
    public function beforeSave($insert){
        if(parent::beforeSave($insert)){
			// if(empty($this->created_by)) $this->created_by = new \MongoId(Yii::$app->user->getId());
			// $this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];			
            // if(!$this->isNewRecord) $this->modified_on = Common::currentTimeStamp();
			if($this->isNewRecord){
				$this->created_on = empty($this->created_on) ? Common::currentTimeStamp() : $this->oldAttributes['created_on'];
				$this->created_by = new \MongoId(Yii::$app->user->getId());				
			}else{
				$this->modified_on = Common::currentTimeStamp();
			}
			if(!empty($this->parent_id)) $this->parent_id = new \MongoId($this->parent_id);
            return true;
        }else{
            return false;
        }
    }
    
    public function afterFind(){
        parent::afterFind();
        if(Yii::$app->controller->action->id!= "update"){
			$this->status = $this->status ? 'Active' : 'Inactive';
			$this->created_by = Common::getOwner($this->created_by);
			$this->created_on = Common::showDate($this->created_on);
			$this->modified_on = Common::showDate($this->modified_on);
		}
        if(Yii::$app->controller->action->id == "index" || Yii::$app->controller->action->id == "view"){
            if(!empty($this->parent_id)){
                $cat_name = self::findOne($this->parent_id);
                $this->parent_id = $cat_name['name'];
            }else{
                $this->parent_id = 'Self';
            }
        }
        if(Yii::$app->controller->action->id != "update" || Yii::$app->controller->action->id != "delete"){
            if(!empty($this->image)){
               $this->image = Common::getMediaPath($this,$this->image);
            }
        }
    }
    
    public static  function getParentCategories(){
        $res = Categories::find()->select(['_id','name'])->where(['parent_id' => ''])->all();
        list($id,$name) = ['_id','name'];
        $res = array_map(function($res) use($id,$name) {return [$id=>(string)$res->$id, $name=>$res->$name];}, $res);
        return ArrayHelper::map($res, $id, $name);        
    }
	/* get name by category id */
	public function getName($id){
		try{
		    $category =Categories::find()->select(['name'])->one();
		    return $category['name'];
		}catch(Exception $e){}
	}

}