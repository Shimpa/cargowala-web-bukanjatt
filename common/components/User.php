<?php
namespace common\components;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
// use yii\db\ActiveRecord;
use yii\mongodb\ActiveRecord;
use common\models\Common;
use common\models\Users;
use yii\web\User as WebUser;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends WebUser
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE =  1;
public $auth_key,$username,$isAdmin=false;
      public static function collectionName(){
        return ['cargowla', 'users'];
    }
	
    /**
    * @inheritdoc
    */
    public function attributes(){
        return [
            '_id',
            'name',
            'contact',
            'otp',
            'password',
            'mobile_tokens',
            'hash_token',
            'image',
            'account_type',
            'company_type',
            'business_type',
            'business',
            'pancard',
            'status',
            'role',
            'social',
            'created_on',
            'modified_on',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['_id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['contact.email' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /* get logged in user firstnamename*/
	public function getID()
    {
		//print_r($this->getIdentity());die;
        return (string)$this->getIdentity()->_id;
    }
	/* get logged in user firstnamename*/
	public function getName()
    {
		//print_r($this->getIdentity());die;
        return $this->getIdentity()->name['firstname'];
    }
	/* get logged in user email*/
	public function getEmail()
    {
		//print_r($this->getIdentity());die;
        return $this->getIdentity()->contact['email'];
    }
	/* get user company name*/
	public function getCompany(){
		return @$this->getIdentity()->business['name'];
	}
	/* get user profile pic*/
	public function getImage(){
		return Common::onlyMediaPath(['_id'=>Yii::$app->user->getId()],new Users()).$this->getIdentity()->image;
	}


}
