"use strict";
var day1 = 1000*60*60*24*1;
$('#vehicle-insurance-expire_date').datetimepicker({
	format: 'YYYY-MM-DD',
	//defaultDate: false,
	//minDate:Date.now(),
});
$('#vehicle-insurance-expire_date').on("dp.show", function (e) { 
  $('#vehicle-insurance-expire_date').data("DateTimePicker").minDate(new Date(Date.now()-day1));
});
$('#vehicle-registration-expiry_date').datetimepicker({
	format: 'YYYY-MM-DD',
	//defaultDate: Date.now()+1000*60*60*8,
	//minDate:Date.now(),
});
$('#vehicle-registration-expiry_date').on("dp.show", function (e) { 
  $('#vehicle-registration-expiry_date').data("DateTimePicker").minDate(new Date(Date.now()-day1));
});
$('#vehicle-permit-permit_expiry_date').datetimepicker({
	format: 'YYYY-MM-DD',
	//defaultDate: Date.now()+1000*60*60*8,
	//minDate:Date.now(),
});
$('#vehicle-permit-permit_expiry_date').on("dp.show", function (e) { 
  $('#vehicle-permit-permit_expiry_date').data("DateTimePicker").minDate(new Date(Date.now()-day1));
});

addEvent(selectAllElement("a.truck-type"),'click',changeRadioState);
function changeRadioState(e){ e.preventDefault();
	var checked =selectElement('input[type="radio"]',this)
	    checked.checked=true;
	selectAllElement("a.truck-type",false,function(e){
	var radio = selectElement('input[type="radio"]',e),	
	    id = e.getAttribute('href'),	
	    pid = id.replace('#trucktype','');
		if(radio.checked==false)
		   selectElement("div.truck"+pid).classList.remove('truck'+pid+"-active");
	});
							 
	getChild(checked);
	//DeliveryOption();						 
}
function getChild(radio){ 
	var pid = radio.getAttribute('id').replace('trucktype','');
	radio.previousElementSibling.classList.add('truck'+pid+'-active');
	$.post(truckerUrl('dropdownlist/tt-childernbyid'),{pid:pid},function(res){
		var html='',select1 = selectElement("#vehicle-truck_type"),selectVal = select1.getAttribute('data-val');
		for(var s in res){
			if(selectVal){ var sel = selectVal==res[s]._id?"selected=selected":'';
			   html+="<option value='"+res[s]._id+"' "+sel+"  >"+res[s].name+" ( "+res[s].loading_capacity+" TON )</option>";
		    }else{
			   html+="<option value='"+res[s]._id+"'>"+res[s].name+" ( "+res[s].loading_capacity+" TON )</option>";
			}
		}
		select1.innerHTML = html;
		html=null;
	});
	
}
selectAllElement('[name="Vehicle[truck_category]"]',null,function(v,k){ 
	if(v.checked==true)getChild(v);
})

paginate({
	offset:$("#offset").val(),
	boxHeight:78,
	params:{q:$("[name='q']").val(),avail:$("[name='avail']").val()},
	method:'POST'
},function(res){ $("tr#load-data td").html('Scroll Down &darr;'); 
	if(isEmpty(res)) return $("tr#load-data td").html('No More Records');	
	$("tbody").append(res)			
});
// assign vehicle
$(document).on('click',"[data-assign]",function(e){ e.preventDefault();
	var $this = $(this);									  
	$.post($this.attr('href'),{_ajx_:true,_id_:$this.attr('data-assign'),_csrf:_csrf},function(res){ 
		if(res && res.status==200){ 
		   $this.parent("#statustxt").html(res.txt)	
		}else if(res && res.status==400){ var err='';
			for(var i in res.error)
				err+=res.error[i] + '\n';
			alert(err)
		}
	})
});
//change availebility
$(document).on('change','.switch-input',function(e){
	$.post($('[name="availbility-url"]').val(),{_csrf:_csrf,v:$(this).prop('checked'),_id:$(this).next().val()},function(){})
});
