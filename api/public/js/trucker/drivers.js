"use strict";
var day1 = 1000*60*60*24*1;
var years50 = 1000*60*60*24*30*12*50;
var years18 = 1000*60*60*24*30*12*18;
$('#drivers-licence-expiry').datetimepicker({
	defaultDate: false,
	format: 'YYYY-MM-DD',
	//minDate:Date.now(),
});
$('#drivers-licence-expiry').on("dp.show", function (e) { 
  $('#drivers-licence-expiry').data("DateTimePicker").minDate(new Date(Date.now()-day1));
});
$('#drivers-date_of_birth').datetimepicker({
	format: 'YYYY-MM-DD',
	//defaultDate: Date.now(),
	//minDate:Date.now(),
});
$('#drivers-date_of_birth').on("dp.show", function (e) {
  $('#drivers-date_of_birth').data("DateTimePicker").minDate(new Date((Date.now()-years18)-years50)).maxDate(new Date(Date.now()-years18));
});


addEvent(selectAllElement("a.truck-type"),'click',changeRadioState);
function changeRadioState(e){ e.preventDefault();
	var checked =selectElement('input[type="radio"]',this)
	    checked.checked=true;
	selectAllElement("a.truck-type",false,function(e){
	var radio = selectElement('input[type="radio"]',e),	
	    id = e.getAttribute('href'),	
	    pid = id.replace('#trucktype','');
		if(radio.checked==false)
		   selectElement("div.truck"+pid).classList.remove('truck'+pid+"-active");
	});
							 
	getChild(checked);
	//DeliveryOption();						 
}
function getChild(radio){ 
	var pid = radio.getAttribute('id').replace('trucktype','');
	radio.previousElementSibling.classList.add('truck'+pid+'-active');

	
}

paginate({
	offset:$("#offset").val(),
	boxHeight:78,
	method:'POST',
	params:{q:$("[name='q']").val(),avail:$("[name='avail']").val()},
},function(res){ $("#load-data td").html(''); 
	if(isEmpty(res)) return $("#load-data td").html('No More Records');	
	$("tbody").append(res)			
});
// assign drivers
$(document).on('click',"[data-assign]",function(e){ e.preventDefault();
	var $this = $(this);									  
	$.post($this.attr('href'),{_ajx_:true,_id_:$this.attr('data-assign'),_csrf:_csrf},function(res){ 
		if(res && res.status==200){ 
		   $this.parent("#statustxt").html(res.txt)	
		}else if(res && res.status==400){ var err='';
			for(var i in res.error)
				err+=res.error[i] + '\n';
			alert(err)
		}
	})
});
//change availebility
$(document).on('change','.switch-input',function(e){
	$.post($('[name="availbility-url"]').val(),{_csrf:_csrf,v:$(this).prop('checked'),_id:$(this).next().val()},function(){})
});