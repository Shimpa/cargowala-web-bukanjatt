"use strict";
var upcomingTab=true,activeOrderTab = false,historyOrderTab  =false;
function dateInterval(date1,date2){
 var startTime = new Date(date1).getTime();//moment(date1, 'hh:mm:ss a');
 var endTime   = new Date(date2||Date.now()).getTime();//moment(date2 ||Date.now(), 'hh:mm:ss a');
 var duration  = startTime  - endTime;
 var seconds   = 1000,
	 minutes   = seconds*60,
	 hours     = minutes*60,
	 days      = hours*24, 
	 weeks     = days*7,
	 months    = weeks*30,
	 years     = months*12,
	 interval  = '';
 if(duration>years)
	interval +=Math.ceil(duration/years%12) +" Years(s) ";
 if(duration>months)
	interval +=Math.ceil(duration/months%30) +" Month(s) ";
 else if(duration>weeks)
	interval +=Math.ceil(duration/weeks%7) +" Week(s) ";
 else if(duration>days)
	interval +=Math.ceil(duration/days)  +" Day(s) ";
 if(duration>hours)
	interval +=Math.ceil(duration/hours%60) +" Hour(s) ";
 if(duration>minutes)
	interval +=Math.ceil(duration/minutes%60) +" Minute(s) ";
  if(duration>seconds)
	interval +=Math.ceil(duration/seconds%60) +" Second(s) "; 
	
  return interval || "Expired";	
}
function loadShipmentPendingYet(){
	$("span[data-expire]").each(function(k,val){
		var $this = this;
		setInterval(function(){
			var intr = dateInterval($($this).data('expire'));
			if(intr=='Expired'){
				$("#isExpired").html('<p><span>Bid Expired</span></p>');
			    $("."+$this.getAttribute('id')).addClass('disabled').removeAttr('data-target');
			}
			else $this.innerHTML = intr; 	
		},1000)
	});
}
loadShipmentPendingYet();
$("#bids").on('submit',function(e){ e.preventDefault();
	$("#submitbtn").next().click();
	location.href = $('[name="redirect"]').val();
});
function lastBid(id){ if(!id)return;
 //if($('[name="Bids[price]"]').length<=0)return;	
$.ajax({
	url:$('[name="last-bid-paced"]').val(),
	method:'post',
	data:{'_csrf':_csrf,_id:id},
	dataType:'json',
	success:function(res){
		if(res.status && res.status==200){
			var ele  =$("#"+res.bid.shipment_id);
			if(ele){
			   ele.addClass('hide');
			   ele.data('rebid',true);
			   ele.next().removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id).removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id+" strong").html(res.bid.price);	
			   if(res.bid.status==2){
				 $("#ab-"+res.bid.shipment_id+" small.rejected-bid").css('display','block');  
			   }	
			}
			for(var i in res.bid){
				$('[name="Bids['+i+']"]').val(res.bid[i]).focus();
				if(i=='price')
			       $("#last-posted-bid").css('display','inline').find("span").html(res.bid[i]);
			}
		}
	}
})
}
lastBid($('[name="data-rebid"]').val());
/* get last bid detail on shipment detail page for trucker
 if shipper rejected then shoem them last bid amount they have posted
*/
function lastBidOnDetailPage(id){ if(!id)return;
 //if($('[name="Bids[price]"]').length<=0)return;	
$.ajax({
	url:$('[name="last-bid-ondetailpage"]').val(),
	method:'post',
	data:{'_csrf':_csrf,_id:id},
	dataType:'json',
	success:function(res){
		if(res.status && res.status==200){
			var ele  =$("#"+res.bid.shipment_id);
			if(ele){
			   ele.addClass('hide');
			   ele.data('rebid',true);
			   ele.next().removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id).removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id+" strong").html(res.bid.price);	
			   	
			}
			for(var i in res.bid){
				$('[name="Bids['+i+']"]').val(res.bid[i]).focus();
				if(i=='price')
			       $("#last-posted-bid").css('display','inline').find("span").html(res.bid[i]);
			}
		}
	}
})
}
lastBidOnDetailPage($('[name="data-rebid"]').val());
function checkStatus(){
$(".bidnow").each(function(k,v){
	var id = this.getAttribute('id');
	if(!this.getAttribute('data-rebid'))
	    lastBid(id);	
});
}
checkStatus();
//change tab event...
$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
  var active =$(this).parent('li').hasClass('active');
  upcomingTab=false,activeOrderTab=false,historyOrderTab=false;	
  var activeTab =$(".tab-content .active .order")	//active tab order(rows) length...
  switch(true){
	  case ($(this).attr('href')=="#active" && active):
		activeOrderTab=true;
		if(activeTab.length<2){  
		   paginateTab();  
		   suddenCall()	
		}  
	  break;
	  case ($(this).attr('href')=="#history" && active):
		historyOrderTab=true;
		if(activeTab.length<2){  
		   paginateTab();  
		   suddenCall()	
		}
	  break;
	  case ($(this).attr('href')=="#upcoming" && active):
		upcomingTab=true;
		if(activeTab.length<2){  
		   paginateTab();  
		   suddenCall()	
		}  
	  break;
	  default:
		upcomingTab=true;
		if(activeTab.length<2){  
		   paginateTab();  
		   suddenCall()	
		}  
	  break;	  
  }	
})
//display remaining text limit for text control 
function getRemainText(){
  selectAllElement('[data-remain-trigger]',null,function(textarea){
	if(textarea){
	   var limit   = parseInt(textarea.getAttribute('data-remain-limit')),current = $(textarea).val().length,
		   control = $(textarea.getAttribute('data-remain-text'));
		   setRemainTextLimit(textarea,limit);
		   control.html(current+"/"+limit);
	   textarea.onkeyup = function(){
		   setRemainTextLimit(this,limit);		 
		   control.html($(this).val().length+"/"+limit);
	   }	   	
	}
  });
}getRemainText();
function setRemainTextLimit(ele,limit){
	var current   =$(ele).val().length; 
    if(current>limit){
	   $(ele).val( $(ele).val().substr(0,limit));
	   return false;
	}
}
// shipment confirmation...
function getBidPrice(bids){ 
	if(bids.length<=0)return;
	var ids=[];
	$.each(bids,function(){
	  var id = this.getAttribute('data-bid-price').replace('shipment','');  
	      ids.push(id);  
    });
	$.ajax({
	  url:$('[name="getbid-price"]').val(),
	  method:'POST',
	  data:{_id_:ids,_csrf:_csrf},
	  success:function(res){
		  if(res.status==200 && res.dta){
			 res.dta.forEach(function(obj){
				      showPrice(obj);
			 });		   
		  }
	  }
  })
  	
}getBidPrice($('[data-bid-price]'));
//show bid price...
function showPrice(dta){
	if(dta.shipment_id){
	  var ele = $('[data-bid-price="shipment'+dta.shipment_id+'"]');
		  ele.html( ele.attr('data-rs')+" "+dta.price)
	      ele.removeClass('cs-hidden').next().css('display','none');
		  var row =ele.parents('.row');
		  row.find('[data-reject]').attr('data-reject',dta._id);
		  row.find('[data-accept]').attr('value',dta._id);
	}
}
// shipment accepted by trucker confirmation
$(document).on('submit','[data-shipment="confirmed"]',function(e){ e.preventDefault();
	$.ajax({
		 url:$(this).attr('action'),
		 method:'PUT',
		 data:$(this).serialize(),
         success:function(res){
			if(res.status==200)
			  $(this).parent().html(res.success);	 
		 }
	});
});

if($('[name="bidloadboard-paginate"]').length){
paginate({
				offset:$("#offset").val(),
				url:$('[name="bidloadboard-paginate"]').val(),
				boxHeight:555,
				method:'POST',
	            params:$("#filters").serialize(),
			},function(res){ $("#load-page").html(''); 
				if(!res){ return $("#load-page").removeClass('cs-hidden').html('No More Records');}
				  $("#bidload-board-before").before(res);
				  loadShipmentPendingYet()			
				  //loadBidCount($(res).find('[data-bids^=shipment]'));		
});
}
if($('[name="regularloadboard-paginate"]').length){
paginate({
				offset:$("#offset").val(),
				url:$('[name="regularloadboard-paginate"]').val(),
				boxHeight:555,
				method:'POST'
			},function(res){ $("#load-page").html(''); 
				if(!res){ return $("#load-page").removeClass('cs-hidden').html('No More Records');}
				  $("#regularload-board-before").before(res);
				  loadShipmentPendingYet()			
				  //loadBidCount($(res).find('[data-bids^=shipment]'));		
});
}
function paginateTab(){
if($('[name="tab-paginate"]').length){
	console.log("sdasdas");
   paginate({ 	
	        offset:$("#offset").val(),
	        url:$('.tab-content .active [name="tab-paginate"]').val(),
			boxHeight:189,
			method:'POST'
			},function(res){ $("#load-page").html(''); 
				if(!res){ return $(".active #load-page").removeClass('cs-hidden').html('No More Records');}
				  $(".tab-content .active").append(res);
				 if(!historyOrderTab)			
				    getBidPrice($(res).find('[data-bid-price]'));
				if(upcomingTab)
				   UpcDriverVehicles($(res).find('[data-assigned]'));			
				if(activeOrderTab)
				   trackIntransit($(res).find('[data-in-sh]'));	
				if(historyOrderTab)
				   historyColl($(res).find('[data-sh]'));	
				  var aTab = document.querySelectorAll('.active');			
				  angular.element(aTab).injector().invoke(["$compile" ,function($compile) {
                         var scope = angular.element(aTab).scope()
                             $compile(aTab)(scope);

                        }
                  ]);			
				  //loadBidCount($(res).find('[data-bids^=shipment]'));		
   });
 }
}paginateTab();

// assign driver assign vehicles...
$(document).on('click','[data-manage-vehicle],[data-manage-driver]',function(e){ e.preventDefault();
	var $link = $(this),body =$link.parents('tbody');														
	$($('#manage-driver-vehicle').find('.modal-dialog')).css('width',$(document).width()/1.5+"px");				$('#frame-loader').removeClass('cs-hidden');															
	$('#manage-driver-vehicle').on('shown.bs.modal', function() { 
		var iframe =$(this).find('iframe');		   
		    iframe.attr('src',$link.attr('data-href')).css('width',$(document).width()/1.5+"px")
            iframe.load(function(){
				$('#frame-loader').addClass('cs-hidden');
				iframe.removeClass('cs-hidden');
			}) 
    })
	$('#manage-driver-vehicle').on('hide.bs.modal', function() {
		body.attr('data-assigned',"shipment"+$link.next('[name="ship_id"]').val());	
		$(this).find('iframe').removeAttr('src').addClass('cs-hidden').removeAttr('style');
        $('#frame-loader').removeClass('cs-hidden');
		UpcDriverVehicles(body);
    }); 
	
});
// get assigned vehicle driver list...
function UpcDriverVehicles(obj){ if(obj.length<=0)return;
	$.post($('[name="getassigned-vehicles"]').val(),{_s_:obj.attr('data-assigned').replace('shipment',''),_ajx_:1,_csrf:_csrf},function(res){
		if(res.status==200){ var html,row=0,htmldata='',sid;
			if(res.data && res.data.length>0){
				if(res.u)getShipperDetail(res.u);
			   res.data.forEach(function(d){ sid= d.sid;
			    html = $('.vehicle-driver_template tfoot').html();
				html = html.replace('{%sno%}',++row);
				html = html.replace('{%sid%}',sid);
				html = html.replace('{%sid%}',sid);
				html = html.replace('{%href-vehicle%}',d.vehicleurl).replace('{%href-driver%}',d.driverurl);
				html = html.replace('{%vehicle_no%}',d.vehicle_no);							
				html = html.replace('{%dassignorchange%}',d.dassignorchange);							
				html = html.replace('{%vassignorchange%}',d.vassignorchange);							
				if(d.driver){
				   html = html.replace('{%driver_image%}','').replace('{%driver_name%}',d.driver).replace(new RegExp('{%driver_no%}','g'),'');
				}else{
				   html = html.replace(new RegExp('{%driver_no%}','g'),d.driver_no);
				   html = html.replace('{%driver_name%}',d.driver_name).replace('{%driver_image%}','<img src ="'+d.driver_image+'" class="img-circle dimg pull-left" />');
			    }	
				htmldata+=html;   
			   });			   	
			}else if(res.data.length<=0 && res.sid){	sid= res.sid;						 
				htmldata = $('.vehicle-driver_template tbody').html();
			}
			var obj=$('[data-assigned="shipment'+sid+'"]');
				obj.html(htmldata);
			    obj.removeAttr('data-assigned');				
		}
	});
}
UpcDriverVehicles($('[data-assigned]'));
// get intransit vehicles detail...
function intransitVehicles(obj,vehicles){
	if(vehicles){
		var html = $(obj).html(),oldhtml = html,htmlrow='',sno=0;
		vehicles.forEach(function(v){
			html = oldhtml;
			html=html.replace('{%vehicle_no%}',v.vehicle_no).replace(new RegExp('{%driver_no%}','g'),v.mobile_no);
			html=html.replace('{%driver_image%}',v.driver_image).replace('{%driver_name%}',v.driver_name);
			html=html.replace('{%started_from%}',v.track.started_from).replace('{%start_date%}',v.track.start_date);
			html=html.replace('{%start_time%}',v.track.start_time).replace('{%reached%}',v.track.reached||null);
			html=html.replace('{%date%}',v.track.date||null).replace('{%time%}',v.track.time||null);
			html=html.replace(new RegExp('{%sno%}','g'),++sno).replace('{%loader%}','cs-hidden');
		  htmlrow+=html; 	
		});
		$('[data-in-sh="'+obj.attr('data-in-sh')+'"]').html(htmlrow);
	}else{
		
	}
}
// get shipper detail...
function getShipperDetail(d){
		var html = $("div.shipper-data").html();
		html = html.replace('{%shipper_name%}',d.name).replace('{%stars%}',d.stars);
		html = html.replace(new RegExp('{%contact_no%}','g'),d.contact_no);
		$("[data-shipper='shipper"+d.id+"']").html(html);
        $('span.stars').stars();
}
// cancel shipment...
$(document).on('click','.cancel-shipment',function(e){ e.preventDefault();
	var prop =   confirm("Are you sure want to cancel order");
	if(!prop)return;												  
													  
	var $this  =$(this);												  
	$.post($this.attr('href'),{_ajx_:true,_csrf:_csrf,_id_:$this.attr('href').split('/').pop()},function(res){
		if(res.status==200){
		   $(".s"+$this.attr('href').split('/').pop()+" .order-mask").css('display','block');
		}else if(res.status==400){
			alert(res.error);
		} 
	});
});
$(document).on('submit','#accept-regular-shipment',function(e){ e.preventDefault();
	var $this = $(this);												   
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res.statusCode==200){
			$("#myModal"+res.id).find(".modal-body").html('<p>'+res.txtMsg+'</p>');
			$("#ship"+res.id).css('display','none');
		}else{
			$("#myModal"+res.id).find(".modal-body").html('<p>'+res.txtMsg+'</p>');
		}
	})
});
// track intransit...
function trackIntransit(obj){
	$.post($("[name='intransit-url']").val(),{_ajx_:true,_csrf:_csrf,_id_:obj.attr('data-in-sh').replace('shipment','')},function(res){
		if(res.status==200 && res.d.length>0){
		   if(res.user)getShipperDetail(res.user);
			intransitVehicles(obj,res.d);
		}
	});
}
// histroty tab...
function historyColl(obj){
	$.post($("[name='history-url']").val(),{_ajx_:true,_csrf:_csrf,_id_:obj.attr('data-sh').replace('history','')},function(res){
		if(res.status==200 && res.t){
		   var body = $(obj).html(),html =$(obj).find("#templ-row").html();	
		   html=html.replace('{%started_from%}',res.t.started_from).replace('{%start_date%}',res.t.start_date);
		   html=html.replace('{%start_time%}',res.t.start_time).replace('{%reached%}',res.t.endonplace);
		   html=html.replace('{%reached_date%}',res.t.endondate).replace('{%reached_time%}',res.t.endontime);
		   $("[data-sh='"+obj.attr('data-sh')+"']").html(html);	
		}else{
			$(obj).first('tr').addClass('cs-hidden').next('tr').removeClass('cs-hidden');
		}
	});
}
// filters...
/*$("#filters").on('submit',function(e){ e.preventDefault();
	var $this = $(this);								  
	$.post($this.attr('action'),$this.serialize(),function(res){
		console.log(res);
	});
});*/
/* change time...*/
$('#filter-date_from').datetimepicker({
	format: 'DD/MM/YYYY',
	//defaultDate: Date.now(),
	//minDate:Date.now(),
});
$('#filter-date_to').datetimepicker({
	format: 'DD/MM/YYYY',
	//defaultDate: Date.now(),
	//minDate:Date.now(),
});
$('#filter-date_from').on("dp.show", function (e) {$('#filter-date_to').val('');});
$('#filter-date_to').on("dp.show", function (e) {
    var from = $('#filter-date_from').val().split('/');
	from =  new Date(from[2],from[1]-1,from[0]); 
	$('#filter-date_to').data("DateTimePicker").minDate(new Date(from.getTime()));
  
});
// get filter states...
function getStates(){ if(!$('[name="filter-states"]').length)return;
	//var filter =  JSON.parse(filters||{});
	console.log(filters);				 
	//if(Object.keys(filters).length>0)				 
	//filters = JSON.parse(filters);				 
	console.log(filters.area);				 
	$.post($('[name="filter-states"]').val(),{_csrf:_csrf},function(res){
		var options = '<option>--Select--</option>';
		res.forEach(function(s){ 
			console.log(s +"--")
			console.log(s==filters.area)
			if(filters.area && (filters.area==s || filters.area.indexOf(s)!=-1))
			   options+='<option value="'+s+'" selected="selected" >'+s+'</option>';
			else options+='<option value="'+s+'"  >'+s+'</option>';
		})	
		$("#filters-area").html(options);
		$(".select2").select2();
		//$(".select2").select2();
	})
}getStates();







