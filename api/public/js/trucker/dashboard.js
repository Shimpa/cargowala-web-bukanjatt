//otp
//otp
$(document).on('click',".sendotpon",function(e){ e.preventDefault();
	$.post($("#sendotp").val(),{_csrf:_csrf},function(res){
		if(res.status==200){
			$("#msg").addClass('alert alert-success').html(res.msg);
			$(".notsentotp").css('display','none');
			$("#verifyotp").css('display','block');
		}
	});
});
$(document).on('click',".cancel-before",function(e){ e.preventDefault();
			$(".notsentotp").css('display','block');
			$("#changeobilenumber").removeClass('active').css('display','none');
	
});
$(document).on('click',".change-mobileno",function(e){ e.preventDefault();
			$(".notsentotp").css('display','none');
			$("#changeobilenumber").addClass('active').css('display','block');
	
});
$(document).on('submit',"#changeobilenumber",function(e){ e.preventDefault();
		$(".active #msg").removeClass('alert').removeClass('alert-warning');
		$(".active #msg1").removeClass('alert').removeClass('alert-warning');
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res.status==200){
			$("#msg1").addClass('alert alert-success').html(res.msg);
			$("#verifyotp").css('display','block');
			$(".notsentotp").css('display','none');
			$("#changeobilenumber").css('display','none');
		}else if(res.status==400){
		    $(".active #msg").addClass('alert alert-warning').html(res.msg||null);	
		}
	});
});
$(document).on('click',".resendotpon",function(e){ e.preventDefault();
	$("#msg1").removeClass('alert').removeClass('alert-success');							  
	$("#msg").removeClass('alert').removeClass('alert-success');							  
	$.post($('#resendotp').val(),{_csrf:_csrf},function(res){
		if(res.status==200){
			$("#msg1").addClass('alert alert-success').html(res.msg);
			$(".notsentotp").css('display','none');
			$("#verifyotp").css('display','block');
		}
	});
});

$(document).on('submit','#verifyotp',function(e){ e.preventDefault();
	$("#msg1").removeClass('alert').removeClass('alert-warning');
	$("#msg1").removeClass('alert').removeClass('alert-success');								 
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res.status==200){
			$("#msg1").addClass('alert alert-success').html(res.msg);
			setTimeout(function(){
				location.reload();
			},2000);
			
		}else if(res.status==400){
			$("#msg1").addClass('alert alert-warning').html(res.msg||null);;
		}
	});
});
$('span.stars').stars();
(function dasboardStats(){ if(!$('#dastats').length)return;
	$.post($('#dastats').val(),{_csrf:_csrf},function(res){
		if(res.status==200){
			var rb =0, bb=0,tc=0,dc=0;
			var tout = setInterval(function(){
				if(rb!=res.data.regular)
				   $(".regular-badge").html(++rb)
				if(bb!=res.data.bidboard)
				   $(".bidload-badge").html(++bb)
				if(rb>=res.data.regular && bb>=res.data.bidboard)   
					clearInterval(tout);
			},10);
			var tout1 = setInterval(function(){
				if(dc!=res.dc)
				   $(".drivers-badge").html(++dc)
				if(tc!=res.tc)
				   $(".trucks-badge").html(++tc)
				if(dc>=res.dc && tc>=res.tc)   
					clearInterval(tout1);
			},10);
			
		}else if(res.status==400){
			$("#msg1").addClass('alert alert-warning').html(res.msg||null);;
		}
	});
}).call(this);
(function totalEarned(){ if(!$('#ttlstats').length)return;
	$.post($('#ttlstats').val(),{_csrf:_csrf},function(res){
		if(res.status==200){
			var te =0;
			var tout = setInterval(function(){
				if(te!=res.data.earned)
				   $(".total-earned").html(te+100)			
				if(te>=res.data.earned)   
					clearInterval(tout);
			},100);
		
			
		}else if(res.status==400){
			$("#msg1").addClass('alert alert-warning').html(res.msg||null);;
		}
	});
}).call(this);