"use strict";

var pending,completed;



//change tab event...
$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
  var active =$(this).parent('li').hasClass('active');
  var activeTab =$(".tab-content .active .row")	//active tab order(rows) length...
  switch(true){
	  case ($(this).attr('href')=="#home" && active):
		pending=true;
		if(activeTab.length<2)  
		   paginateTab();  
	  break;
	  case ($(this).attr('href')=="#profile" && active):
		completed=true;
		if(activeTab.length<2)  
		   paginateTab();  
	  break;
	  default:
		pending=true;
		if(activeTab.length<2)  
		   paginateTab();  
	  break;	  
  }	
})
function paginateTab(){
paginate({
				offset:$("#offset").val(),
				url:$('.active #payment-paginate').val(),
				boxHeight:500,
				method:'POST'
			},function(res){ $(".active #load-page").html(''); 
				if(!res){ return $(".active #load-page").html('No More Records');}
						
				  $(".active #payments-before").before(res);
});
}paginateTab();
// buy credits...
$("#buy-credits").on('submit',function(e){ e.preventDefault();
  var $this = $(this)										  
  $.post($this.attr('action'),$this.serialize(),function(res){
	  if(res.status==200){
		  $("#buy-credits-billing").removeClass('hidden').html(res.html);
		  $("#mask-temp").addClass('hidden');
	  }else if(res.status==400){
		$("#buy-credits-billing").html('');
		$("#mask-temp").removeClass('hidden');
		$("#msg").removeClass('hidden').html(res.html);
	  }
  })	
})
// change quantity...
$("#credit-quantity").on('mpchange change',function(){
		var cv = parseInt($("#creditval").html()),qt = $(this).val();
		$("#qty").html(qt)
		$("#subttl").html(cv*qt)
		$("#buy-credits-billing").html('');
		$("#mask-temp").removeClass('hidden');
	})
// payment tab change text 
$(document).on('shown.bs.tab','a[data-toggle="tab"]',function(e){
  $("#citrusNumber").val('');$("#citrusExpiry").val('');$("#citrusCardHolder").val('');	$("#citrusCvv").val('');	
  $("#home-txt").html($("#home-txt").data('credit'));
  $("select option[value='credit']").attr('selected',true);	
  if($(this).data('label')=='debit'){
	  $("#home-txt").html($("#home-txt").data('debit'));
      $("select option[value='debit']").attr('selected',true);
  }
  	
})
(function totalEarned(){ if(!$('#ttlstats').length)return;
	$.post($('#ttlstats').val(),{_csrf:_csrf},function(res){
		if(res.status==200){
			var te =0;
			var tout = setInterval(function(){
				if(te!=res.data.earned)
				   $(".total-earned").html(te+100)			
				if(te>=res.data.earned)   
					clearInterval(tout);
			},100);
		
			
		}else if(res.status==400){
			$("#msg1").addClass('alert alert-warning').html(res.msg||null);;
		}
	});
}).call(this);