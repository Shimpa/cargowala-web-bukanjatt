"use strict";
$('#bid-loading-date').datetimepicker({
	format: 'DD/MM/YYYY',
	minDate:Date.now()+1000*60*60*1,
});
$('#bid-loading-time').datetimepicker({
	format: 'HH:mm:ss',
	minDate: Date.now()+1000*60*120,
});
$('#bid-loading-date').focus().blur();
$('#bid-loading-time').focus().blur();


// select truck type and change  category options respectively truck type 
addEvent(selectAllElement("a.truck-type"),'click',changeRadioState);
function changeRadioState(e){ e.preventDefault();
	var checked =selectElement('input[type="radio"]',this)
	    checked.checked=true;
	selectAllElement("a.truck-type",false,function(e){
	var radio = selectElement('input[type="radio"]',e),	
	    id = e.getAttribute('href'),	
	    pid = id.replace('#trucktype','');
		if(radio.checked==false)
		   selectElement("div.truck"+pid).classList.remove('truck'+pid+"-active");
	});
							 
	getChild(checked);
	DeliveryOption();						 
}
// get truck type options data...
function getChild(radio){ 
	var pid = radio.getAttribute('id').replace('trucktype','');
	radio.previousElementSibling.classList.add('truck'+pid+'-active');
	if(!radio.getAttribute('data-post')) return;
	$.post(shipperUrl('dropdownlist/tt-childernbyid'),{pid:pid},function(res){
		var html='',select1 = selectElement("ul.dropdown-truck1"),selectVal = select1.getAttribute('data-val');
		setCaptionandValue(null);
		for(var s in res){
			html+="<li><a data-href='"+JSON.stringify(res[s])+"' href='javascript:;'>"+res[s].name+" ( "+res[s].loading_capacity+" TON )</a></li>";
		}
		if(selectVal){ var val = JSON.parse(selectVal); 
		   select1.previousElementSibling.children[0].innerHTML = val.name+" ( "+val.loading_capacity+" TON )";
		   select1.removeAttribute('data-val');
		   selectElement("#shipments-truck").value = selectVal;			  
		}
		select1.innerHTML = html;
		html=null;
	});
	
}
// on dropdown click select dropdown option
liveEvent("click",'ul.dropdown-truck1 li a',setCaptionandValue);
function setCaptionandValue(val){
	var cnrl = selectElement("#shipments-truck"); 
	    cnrl.value = val===null?val:this.getAttribute('data-href');
	    selectElement(cnrl.getAttribute('data-rel')+" b").innerHTML = val===null?'Select truck':this.innerHTML;
}
function DeliveryOption(){
selectAllElement('input[name="shipments[delivery-type]"]:checked',false,function(e){
selectElement('div.fullload-type').style.display = e.value==0?'none':'block';
});
}
/* load data */
//if(shipment['truck-type']!=-1)
 //  selectElement("a[href='#trucktype"+shipment["truck-type"]+"']").click();
//console.log(history.state);
window.addEventListener("popstate", function(e) {
 //console.log(e)
}, false);
var fname =  {'loading-city':0,'loading-state':1,'unloading-city':2,'unloading-state':3};

// load saved data of current page...
function loadPageData(h){ if(!h)return;
   var step2 =resolveCookiesObject(getCookie('step2'));
	console.log(step2);
	for( var f in step2){
		var fld;		   
		   if(step2[f]=='c1'){
			  fld = selectElement('a[href="#'+f+'"]');
			  if(fld)				  
			     fld.click(); 
		   }else if(step2[f]=='tr1'){
			    var fld = selectElement("[data-trigger='"+f+"']"),
			        trgr= fld.getAttribute('id'),
			        fld = selectElement('a[href="#'+trgr+'"]');
			        fld.click();
		   }else{
			  var fld =selectElement('#'+f); 
			   if(fld){
				  if(f =='dropdown-truck1'){ 
					  fld = selectElement("."+f);
					  fld.setAttribute('data-val', JSON.stringify(step2[f]));
				  }else fld.value = step2[f];
			   
			   }
		   }
		   
  }

}
// get trip info driver name contact no etc.
function tripInfo(){
	var drivers = $('[data-driver]')
	if(!drivers.length)return;	
	$.ajax({
		url:$("input[name='get-trip-info']").val(),
		method:'POST',
		data:$('form').serialize(),
		dataType:'json',
		success:function(res){
		  if(res && res.status){
			for(var i in res.data){
				$('[data-driver="'+res.data[i]._id+'"]').html(res.data[i]._id);
			}  
		  }	
		}
	})
}
tripInfo();

/* change time...*/
$('#shipment-change_time').datetimepicker({
	format: 'HH:mm:ss',
	//defaultDate: Date.now(),
	//minDate:Date.now(),
});
$('#shipment-change_time').on("dp.show", function (e) {
    var d = new Date(),expd = new Date(shipment_datetime),max,min;
  if(d.toDateString() == expd.toDateString()){
	 min= Date.now();max =expd.setHours(23,59,59)	 
	$('#shipment-change_time').data("DateTimePicker").minDate(new Date(min)).maxDate(new Date(max));
  }
});
$("#changetime-submit").on('click',function(e){ e.preventDefault();
	var $this = $(this),form =$this.parents('form');										   
	$.post(form.attr('action'),form.serialize(),function(res){
		if(res){
			if(res.status==200){
				$(".change-time-success").html('<p  class="text-success">'+res.success+'</p>');
			}else if(res.status==400){
				$(".shipment-change_time-error").html(res.error)
			}
		}
	});
});
// bid load board bids count...
function loadBidCount(shipments){
  var ids=[];
  if(shipments.length<=0)return;
  $.each(shipments,function(ele,i){
	var id = this.getAttribute('data-bids').replace('shipment','');  
	ids.push(id);  
  });
  $.ajax({
	  url:$('[name="getbids-count"]').val(),
	  method:'POST',
	  data:{_id_:ids,_csrf:_csrf},
	  success:function(res){
		  if(res.status==200 && res.d){
			 res.d.forEach(function(obj){
				      showBids(obj);
			 });		   
		  }
	  }
  })	
}loadBidCount($("[data-bids^=shipment]"));
function showBids(dta,manual){
	if(dta._id){
	  var ele = $("[data-bids='shipment"+dta._id+"']"),refer;
	      ele.prev().css('display','none'),blackstrip = ele.parents("#blackstrip"),
		  refer = blackstrip.next('div').find('a');
		  refer.attr('data-refer','vbids'+dta._id);
	  if(dta.count>0){
		 ele.next().html(dta.count +" "+ele.next().attr('title')).css('display','inline-block');		  
		 blackstrip.removeClass('col-md-12').addClass('col-md-9');
		if(refer.length){
		   refer.parent().removeClass('cs-hidden');	
		}
	  }else if(dta.count<=0){
		ele.css('display','inline');
		ele.next().css('display','none');
		var blackstrip = ele.parents("#blackstrip");  
		blackstrip.addClass('col-md-12').removeClass('col-md-9').removeClass('pR0');
		if(refer.length){ 
			if(manual) refer.click();
		   refer.parent().addClass('cs-hidden');	
		}  
	  }	
	}
}
// get posted bids...
var row=0;
$(document).on('click',"[data-refer]",function(){ row=0;
   var $this=$(this).get(0),sid =$this.getAttribute('data-refer').replace('vbids',''),
	   loader = $("#ViewBids .modal-body #bid-loader");
												 console.log(loader);
   loader.removeClass('cs-hidden');	
   $("#ViewBids .modal-body #no-data").addClass('cs-hidden');	
   $.ajax({
	    url:$('[name="getbids"]').val(),
	    method:'POST',
	    data:{sid:sid,_csrf:_csrf},
	    success:function(res){ loader.addClass('cs-hidden');
		  if(res.status==200 && res.d.length>0){
			 var html='';
			res.d.forEach(function(obj,i){
				var htmlrow=getBidRowHtml(); 
				htmlrow=htmlrow.replace('{%_id%}',obj._id||'').replace('{%_s_%}',obj.shipment_id)
				htmlrow=htmlrow.replace('{%company%}',obj.trucker_id.business?obj.trucker_id.business.name:'');
				htmlrow=htmlrow.replace('{%city%}',obj.trucker_id.business?obj.trucker_id.business.registered_city:'');
				htmlrow=htmlrow.replace('{%description%}',obj.description||'');
				htmlrow=htmlrow.replace('{%price%}',obj.price||0);
				htmlrow=htmlrow.replace('{%rating%}',obj.rating||0);
				html+=htmlrow;
				if((res.d.length>=2) && i%2==0) html+='<hr>';
			});
			$("#ViewBids .modal-body").html(html);
			$('span.stars').stars();
			$this.removeAttribute('data-refer');  
		  }else{ $("#ViewBids .modal-body #no-data").removeClass('cs-hidden');}
	 }
   })	
})

function getBidRowHtml(){ ++row;
	var html;		
		html = $("#bidrow").html()
	    html = html.replace('{%sno%}',row);
	return html;					 
}
$(document).on('click',"[data-parent]",function(){
  $(this).find('span').hasClass('up-arrow')?
  $(this).find('span').removeClass('up-arrow'):
  $(this).find('span').addClass('up-arrow');
});
$(document).on('click','button[type="submit"]',function(e){ e.preventDefault();
	var $this = $(this),form =$this.parents('form');													   
	    form.find('[name="_m_"]').val($this.data('submit'));	 					   
		$.ajax({
			url:form.attr('action'),
			method:'POST',
			data:form.serialize(),
			success:function(res){
				if(res && res.status==200){
					if(res.dta.next){
					   $("#bid-accepted").click();	
					   //location.href  = res.dta.next; 	
					}
					
					var row =form.parent('.row'),countEle =$("[data-bids='shipment"+res.dta.sid+"']").next();
					if(!res.dta.a){
					    row.html('<div class="col-md-8 text-center text-success">'+res.dta.msg+'</div>').fadeOut(3000);
					}
					var count = parseInt(countEle.text().replace(/[A-z]/g,''));
					if(!res.dta.a)
					    count=count-1;
					    showBids({_id:res.dta.sid,count:count},true);	
				}else if(res.status==401){
					$("[data-bids='shipment"+dta._id+"']")
					var row =form.parent('.row');
					    row.html('<div class="col-md-8 text-center text-success">'+res.html+'</div>').delay(8000).css('display','none');
				}
			}
		});												   
})
paginate({
				offset:$("#offset").val(),
				url:$('#bidloadboard-paginate').val(),
				boxHeight:231,
				method:'POST'
			},function(res){ $("#load-page").html(''); 
				if(!res){ return $("#load-page").html('No More Records');}
						
				  $("#bidload-board-before").before(res);
				  loadBidCount($(res).find('[data-bids^=shipment]'));		
});

//change shipment  date time
$(document).on('submit',"#biddatetime",function(e){ e.preventDefault();
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res.status==200){
			$('#myModal').modal('hide');
		}else{
			$("#datetimechanged").css('display','block').html(res.html)
		}
	});
});

