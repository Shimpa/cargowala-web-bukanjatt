"use strict";
$("#ex19").slider({
	formatter:function(cv){
		var parent =$("span#amounttopay").parent();
		//console.log($("span#amounttopay").get(0).offsetTop);
	    var i=37;
		$("#ndinstal").show();
		$("#instno").html(2);
		$(".ndinstall").show();
		var stint = 100*(cv/$("#overall_amount").val()),scnind= 100-stint;
		$("span#stinstallamt").html(stint+"%");
		$("span#ndinstallamt").html(scnind+"%");
		$("#users-rest_payment").val(scnind);
		if(cv==$("#overall_amount").val()){
			$("#ndinstal").hide();
			$("#instno").html(1);
			$(".ndinstall").hide();
		}
		$("span#amounttopay").html((cv).toFixed(2));	
		$("span#ndamounttopay").html((scnind*$("#overall_amount").val()/100).toFixed(2));	
	}
});
$("#payment").on('submit',function(e){e.preventDefault();
	   $(".rest-payment-err-id").addClass('cs-hidden');
							  
	if($("[name='Users[stpaymentmethod]']:checked").length<=0){
	   $(".strest-payment-error-id").removeClass('cs-hidden');	
		return; 
	}
	if($("[name='Users[ndpaymentmethod]']:checked").length<=0){
	   $(".ndrest-payment-error-id").removeClass('cs-hidden');	
		return; 
	}								  
	$("input[type='submit']").attr('disabled',true);								  
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res){ $("input[type='submit']").attr('disabled',false);
			if(res.status==400){
				mp.validate(res.errors,res.form);
			}else if(res.status==200){
				if(res.next)
				   location.href =res.next;	
					
			}
		} 
	});
});
paginate({
				offset:$("#offset").val(),
				url:$('#payment-paginate').val(),
				boxHeight:500,
				method:'POST'
			},function(res){ $("#load-page").html(''); 
				if(!res){ return $("#load-page").html('No More Records');}
						
				  $("#payments-before").before(res);
});
$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
  $("#citrusNumber").val('');$("#citrusExpiry").val('');$("#citrusCardHolder").val('');	$("#citrusCvv").val('');	
  $("#home-txt").html($("#home-txt").data('credit'));
  $("select option[value='credit']").attr('selected',true);	
  if($(this).data('label')=='debit'){
	  $("#home-txt").html($("#home-txt").data('debit'));
      $("select option[value='debit']").attr('selected',true);
  }
  	
})