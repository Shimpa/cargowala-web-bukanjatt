"use strict";
loadLiveData();
function isBidPosted(ele){ 
   if(!ele)return;	
   $.ajax({
	  url:$("#bid-url").val(),
	  method:'POST',
	  data:{sid:ele.getAttribute('data-bids').replace('trucker-',''),_csrf:_csrf},
	  success:function(res){
		  var html="";
		  if(res.status){
			 if(res.data && res.data.t){var data = res.data.t;
				html+='<div class="row">';
				html+='<div class="col-md-12 pR10 col-xs-6 col-sm-4">';
				html+='Trucker Name : - <strong class="mT0 fnt18"> '+data.trucker_id.name.firstname+" "+data.trucker_id.name.lastname+'</strong></div>';
				html+='<div class="col-md-12 col-xs-6 col-sm-4 mT20">';
				html+='Rating : - <span class="stars"><span>'+data.trucker_id.rating||0+'</span></span></div>';
				if(ele.getAttribute('data-history')!=1){					
				   html+='<p class="col-sm-6 col-xs-12 col-md-12 mT20"> ';
				   html+='Contact No : - <a href="tel: +'+data.trucker_id.contact.mobile_number+'" class="pR20">'+data.trucker_id.contact.mobile_number+'</a></p>';
				}
				ele.removeAttribute('data-bids');						
				ele.innerHTML = html;		  
				$('span.stars').stars();						
			 }else{
				html+='<div class="row">';
				html+='<div class="col-md-12 pR10 col-xs-6 col-sm-4">'; 
				html+='<strong class="mT0 fnt18"></strong></div></div>'; 
			  $('[data-bids="trucker-'+res.data.sid+'"]').html(html);  
		     }
			ele.removeAttribute('data-bids');  
		  }
			  
	  }, 
   });	
}
function loadLiveData(){
selectAllElement('.active div[data-bids^=trucker]',null,function(ele,i){	
	isBidPosted(ele);
});	
}
function PaginateTab(){
paginate({
				offset:$("#offset").val(),
				url:$('.tab-content .active #paginate-url').val(),
				boxHeight:510,
				method:'POST'
			},function(res){ $(".tab-content .active #load-page").html(''); 
				if(isEmpty(res)){ return $(".tab-content .active #load-page").html('No More Records'); activeData=false;}
			var aTab = document.querySelectorAll('.active');				
			$(".tab-content .active #active-shipments").before(res);
			loadLiveData();    angular.element(aTab)
					   .injector()
					   .invoke([
    "$compile" ,function($compile) {

        var scope = angular.element(aTab).scope()
        $compile(aTab)(scope);

    }
]);
							
});
}
PaginateTab();
$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){ 
	var active =$(this).parent('li').hasClass('active');
  	if($(this).attr('href')=='#shipmentshistory' && active){
	if($("#shipmentshistory .row").length<=0){
		PaginateTab();
	    suddenCall();
	}
		
	}
       
});
//remove incomplete shipments
$("#incomplete-remove").on('click',function(e){ e.preventDefault();			   
	$(this).parents('form').attr('action',$(this).attr('href')).submit();
});
/*
var historyTab = false,activeData=true,historyData=true;
function PaginateTab(){ 
	if(!historyTab && activeData && $('#active-paginate').length){
paginate({
				offset:$("#offset").val(),
				url:$('#active-paginate').val(),
				boxHeight:710,
				method:'POST'
			},function(res){ $(".active #load-page").html(''); 
				if(!res){ return $(".active #load-page").html('No More Records'); activeData=false;}
			var aTab = document.querySelectorAll('.active');				
			$("#active-shipments").before(res);
			loadLiveData();    angular.element(aTab)
					   .injector()
					   .invoke([
    "$compile" ,function($compile) {

        var scope = angular.element(aTab).scope()
        $compile(aTab)(scope);

    }
]);
							
});
}else if(historyTab && historyData && $('#history-paginate').length){
	paginate({
				offset:$("#offset").val(),
				url:$('#history-paginate').val(),
				boxHeight:710,
				method:'POST'
			},function(res){ $(".active #load-page").html(''); 
				if(!res){$(".active #load-page").html('No More Records');historyData=false;}
				$("#history-load").before(res);
				loadLiveData();
				//$('body').scrollTop(90);			
	    });
}
}


$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
	var active =$(this).parent('li').hasClass('active');
  	if($(this).attr('href')=='#shipmentshistory' && active){
	    historyTab =true;
	if($("#shipmentshistory .row").length<=0){	
	    PaginateTab();
		PaginateTab();
	}
		
	}else{ 
	   historyTab=false;
	}
       
});*/
$( window ).scroll(PaginateTab);
//otp
$(document).on('click',".sendotpon",function(e){ e.preventDefault();
	$.post(shipperUrl('users/sendotp'),{_csrf:_csrf},function(res){
		if(res.status==200){
			$(".notsentotp").css('display','none');
			$("#verifyotp").css('display','block');
		}
	});
});
$(document).on('click',".cancel-before",function(e){ e.preventDefault();
			$(".notsentotp").css('display','block');
			$("#changeobilenumber").css('display','none');
	
});
$(document).on('click',".change-mobileno",function(e){ e.preventDefault();
			$(".notsentotp").css('display','none');
			$("#changeobilenumber").css('display','block');
	
});
$(document).on('submit',"#changeobilenumber",function(e){ e.preventDefault();
	$.post($(this).serialize(),function(res){
		if(res.status==200){
			$(".notsentotp").css('display','block');
			$("#changeobilenumber").css('display','none');
		}
	});
});
$(document).on('click',".resendotpon",function(e){ e.preventDefault();
	$.post(shipperUrl('users/resendotp'),{_csrf:_csrf},function(res){
		if(res.status==200){
			$(".notsentotp").css('display','none');
			$("#verifyotp").css('display','block');
		}
	});
});

