$('#bid-loading-date').datetimepicker({
	format: 'DD/MM/YYYY',
	minDate:Date.now()+1000*60*60*1,
});
$('#bid-loading-time').datetimepicker({
	format: 'HH:mm:ss',
	minDate: Date.now()+1000*60*120,
});
$('#bid-loading-date').focus().blur();
$('#bid-loading-time').focus().blur();

var sid;
function getCategories(obj){
var obj =  this.window?obj:this;
	obj.previousElementSibling.value = obj.options[obj.selectedIndex].text;
var opt = '<option value="">--Select --</option>';	
$.post('/shipper/dropdownlist/truck-categories',{pid:obj.value||"",_csrf:_csrf},function(res){
    res.forEach(function(c,d){ 
	    opt+='<option ';	
		opt+=sid==c._id?"selected=selected ":"";
		opt+='value="'+c._id+'">'+c.name+'</option>';
	});
	sid=undefined;
	if(obj.getAttribute('data-update'))
	  selectElement(obj.getAttribute('data-update')).innerHTML  =opt;	
	else{
		obj.innerHTML = opt; 
	    obj.setAttribute('data-update','#item-sub_category');
	}
});	
}

var pdropdn = selectElement('#item-parent_category');
if(pdropdn)
   getCategories(pdropdn);
addEvent(pdropdn,'change',getCategories);
addEvent(selectElement("#item-sub_category"),'change',scname);

function scname(){
	this.previousElementSibling.value = this.options[this.selectedIndex].text;
	this.setAttribute('data-value',this.value)
}
function listItem(item){ 	
 var html ='<li class="list-group-item item'+item.id+'"   data-item=\''+JSON.stringify(item)+'\' >';
	 html+='<i class="fa fa-plus-circle"></i> <span class="text-primary">Item Name - </span>';
	 html+='<strong> '+item.name +'</strong>';
	 html+='<a href="javascript:;" onclick="editItem(this);" class="pull-right mR10"><i class="fa fa-pencil-square"></i></a>';
	 html+='<a href="javascript:;" onclick="deleteItem(this);" class="pull-right mR10"><i class="fa fa-trash-o"></i> </a>';
	 html+='<ul class="list-unstyled">';
	 html+='<li class="pL15"><span class="text-primary">Category - </span><strong>'+item.pcname+'</strong></li>';  
     html+='<li class="pL15"><span class="text-primary">Sub Category - </span><strong>'+item.scname+'</strong></li>';
    html+='</ul></li>';
	selectElement("#itmlst ul").insertBefore(html2Node(html),selectElement("#itmlst ul").childNodes[0]);
	selectElement('#item input[type="submit"]').nextElementSibling.click();
	selectElement("#item-browse").nextElementSibling.value="";
	selectElement("#item-browse").previousElementSibling.src=assetsp+"/images/invoice.png";
	selectElement('#item input[name="item[id]"]').value="";
	if(document.querySelectorAll('#itmlst ul li').length>3){
	       document.querySelector('li.alert-info').style.display = 'none';
		   document.querySelector('div.fade-mask').style.display = 'none';
	}
}
function editItem(obj){
    if(selectElement('[name="item[id]"]').value)return;
	//selectElement('#item input[type="submit"]').value =
	var item = obj.parentNode.getAttribute('data-item'),item= JSON.parse(item),id;
	for(var i in item){
		id = item.id;
		var ele = selectElement('[name="item['+i+']"]');
		if(ele){ var name = ele.getAttribute('id');
			if(name=='item-sub_category'){
			    sid =  item['sub_category'];				
			}
			if(i=='invoice_image'){
				selectElement("#item-browse").previousElementSibling.setAttribute('src',item.invoice_path+item.invoice_image);
			  //selectElement("#item-invoice_image").value = item.invoice_image;
			}
			if(i=='total_weight')
			   $("#totalitemweight").html(item[i] +"kg")	
		    ele.value=item[i];				
		}
	}
	getCategories(selectElement('#item-parent_category'))
	var edele = selectElement('li.item'+id);
	    edele.parentNode.removeChild(edele);
}

function deleteItem(obj){
	$("#loader-item").css('display','block');
	var item = obj.parentNode.getAttribute('data-item'),item= JSON.parse(item),id;
	    item['_csrf'] = _csrf;
	$.ajax({'url':selectElement('input[name="url"]').value,type:'DELETE',data:item,success:function(res){
		$("#loader-item").css('display','none');
		if(res.status==200){
			$("li.item"+res.i).remove();
			if(document.querySelectorAll('#itmlst ul li').length<3){
	           document.querySelector('li.alert-info').style.display = 'block';
		       document.querySelector('div.fade-mask').style.display = 'block';
	     }
		}
	}});
}
function loadPageData(h){ if(!h)return; 			
						 
   var step3 =resolveCookiesObject(getCookie('step3')),items = step3.items;
    delete step3.items;
	for(var i in step3){
		listItem(step3[i]);
	}
	for(var i in items){
		var ele  =selectElement("[name='items["+i+"]]")
		
		if(i=='insurance_image'){
		   var im  =selectElement('#items-insurance');
			   im.nextElementSibling.value=items.insurance_image;
			   im.previousElementSibling.previousElementSibling.src = item.image_path + items.insurance_image;
			   im.previousElementSibling.previousElementSibling.classList.remove('hidden');
		}
		if(i=='insured'){
			selectElement("#items-insured"+items.insured).checked = true;
		}else if(ele)
		         ele.value = item[i];
		   
	}					 

}
loadPageData(location.hash);
addEvent("input[name='items[insured]']",'change',isInsured);
function isInsured(){ console.log(this.value);
  					 
  if(this.value==2){ var uploader = selectElement('img.insurance-image');
	 uploader.classList.remove('hidden');
	 uploader.setAttribute('src',uploader.getAttribute('data-src')) 
  }
  else{ 
	  selectElement('img.insurance-image').classList.add('hidden');	
	  selectElement('img.insurance-image').src="";
	  selectElement('#items-insurance').value="";
	  }
}

//change shipment  date time
$(document).on('submit',"#biddatetime",function(e){ e.preventDefault();
	$.post($(this).attr('action'),$(this).serialize(),function(res){
		if(res.status==200){
			$('#myModal').modal('hide');
			location.reload();
		}else{
			$("#datetimechanged").css('display','block').html(res.html)
		}
	});
});
// change item weight...
$("#item-weight").on('blur',function(e){
	calculateWeight();
});

