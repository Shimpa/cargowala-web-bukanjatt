// my shipment angular controller

cwApp.controller('myShipment',['$scope',function($scope){
	$scope.hi = "Hello";
	$scope.notifications= [{message:"ssadsd"},{message:"sadsad"}];
}]);
cwApp.directive('bidTimer',['$interval','$compile',function($interval,$compile){
var timeout;
 return {
	restrict:'A',
	scope:{
		expiry:'=expiry',
		now:'=now',
	},
	multiElement: true,
	replace:true, 
	link: function (scope,element,attr){

	function updateElement(){
		var timer = dateInterval(scope.expiry,new Date());
		if(timer!='Expired')
		   element.text(attr.label+" "+timer);
		else element.text(timer);
		
	}
	scope.$watch(attr.bidTimer,function(value){
		updateElement()
	})	
	element.on('$destroy', function() {
      $interval.cancel(timeout);
    });
    // start the UI update process; save the timeoutId for canceling
    timeout = $interval(function() {
      updateElement(); // update DOM
    }, 1000);	
	}

}}])
function dateInterval(date1,date2){
 var startTime = new Date(date1).getTime();//moment(date1, 'hh:mm:ss a');
 var endTime   = new Date(date2||Date.now()).getTime();//moment(date2 ||Date.now(), 'hh:mm:ss a');
 var duration  = startTime  - endTime;
 var seconds   = 1000,
	 minutes   = seconds*60,
	 hours     = minutes*60,
	 days      = hours*24, 
	 weeks     = days*7,
	 months    = weeks*30,
	 years     = months*12,
	 interval  = '';
 if(duration>years)
	interval +=Math.ceil(duration/years%12) +" Years(s) ";
 if(duration>months)
	interval +=Math.ceil(duration/months%30) +" Month(s) ";
 else if(duration>weeks)
	interval +=Math.ceil(duration/weeks%7) +" Week(s) ";
 else if(duration>days)
	interval +=Math.ceil(duration/days)  +" Day(s) ";
 if(duration>hours)
	interval +=Math.ceil(duration/hours%60) +" Hour(s) ";
 if(duration>minutes)
	interval +=Math.ceil(duration/minutes%60) +" Minute(s) ";
  if(duration>seconds)
	interval +=Math.ceil(duration/seconds%60) +" Second(s) "; 
	
  return interval || "Expired";	
}
/* payment controller */
cwApp.controller('paymentCtrl',['$scope',function($scope){
	$scope.ship={};
	$scope.hi = "sdadas";
	$scope.setShipment=function(s){
		console.log(s);
	}
	$scope.excludeInsurance =  function(state){
	console.log($scope.ship);
	console.log($scope.insurance);
		
	}
	
}]);
cwApp.filter('striptagsandexcerpt',function(){
	return function(text,length){
		length = length||200;
		if(text.length>length)
		   return text.substr(0,length).replace(new RegExp('<.+?>','g'),'')+'... ';
		return text;
	}
})

cwApp.directive('notificationCounter',['$interval','$http',function($interval,$http){
	return {
		restrict:'E',		
		transclude:true,
		priority:1,
		scope:{
			lnurl:'@',
			url:'@',
			mrkead:'@',
			seeall:'@',
		},
		template:'<div class="dropdown info"><button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" ng-click="loadNotification();" aria-haspopup="true" aria-expanded="true">Notifications <span  class="badge" >{{count}}</span></button><ul scroll-bar class="dropdown-menu scrollbar-control" aria-labelledby="dropdownMenu1" post="true" ><li class="pT10 pL10 pR10 pB10" ng-repeat="n in notifications"  ><p>{{n.message|striptagsandexcerpt:100}}<small><a ng-click="markRead(\'{{n._id}}\')">{{markreadtxt}}</a></small></p></li><li class="text-center" style="color: #262626;text-decoration: none;background-color: #f5f5f5;"><a href="{{seeall}}" >See all</a></li></ul></div>',
		link:function(scope,element,attr){
		    var timeout,wait=false;
		    function nCounter(){
		        if(wait)return;
		        wait=true;	
		        $http.get(scope.url).then(function(res){
		            wait=false;
					if(res.data.count){
	                    //element.parent().next().attr('post',true);
						scope.count=res.data.count; 	
					}
	            });	
	        }
			element.on('$destroy',function(){
					$interval.clear(timeout);				   
			});						   
			timeout=$interval(function(){
				        nCounter();					   
			        },10000);
			nCounter();
		},
		controller:['$http','$scope','$element','$sce',function($http,$scope,$element){
			$scope.markreadtxt ='Mark Read';
			//$scope.notifications = [{message:'no notification found'}];
			$scope.loadNotification=function(e){
				$http.post($scope.lnurl).success(function(r){ 
					  if(r.status==200){
					    $scope.notifications = r.n;
					  }
				})
			}
			$scope.markRead=function(id){ 
				$http.post($scope.mrkead,{id:id}).success(function(r){ 
					  if(r.status==200){
					    $scope.markreadtxt ='';
					  }
				})
			}
			
			//$element.parent().bind('click',loadNotification);
		}]
	}
}]);
cwApp.directive('scrollBar',function(){
	return{
		restrict:'A',
		//transclude:true,
		priority:0,
		scope:{
			type:'@'
		},
		link:function(scope,element,attr){
			
			function createScroller(){
				var div = document.createElement('div');
				div.classList.add('scroller');
				div.style.backgroundColor = "red";
				div.style.width = '5px';
				div.style.position ='absolute';
				div.style.top=0;
				div.style.right=0;
				div.offsetHeight = element.prop('scrollHeight');
				console.log(element);
				element.css({'height':500+'px','overflow':'hidden'});
				var span =document.createElement('span');
				span.style.width = "5px";
				span.style.backgroundColor = "blue";
				span.style.height = element.prop('scrollHeight')
				element.append(div);
			}
			//createScroller();
		}
	};
});
