"use strict";
var assetsp = "/cw-assets/";
window.mp = {
	validate:function(err,form){ //console.log(err);
	removeErrors();
	 $(".mperror").html('');	
	 for(var field in err){	
		 var fld =field.replace(".",'-');
		 //fld =fld.length>1?fld[1]:fld[0];
		 //console.log(fld);
	     var ele   = document.querySelector("#"+form+"-"+fld),
		    crele  = document.querySelector("."+form+"-"+fld+"-error");		 
		 //console.log(crele);
		 if(crele)crele.innerHTML = "";
		 if(ele){
		    ele.mpinvalid = true;
			ele.addEventListener('focus',setInvalidate);			 
		 }
		 customBubble(ele,err[field].message,crele); 
			//ele.focus(); 
	 }
	},
};
liveEvent("input",'input',resetError);
liveEvent('change',"input",resetError);
liveEvent('input',"textarea",resetError);
liveEvent('change',"select",resetError);
function resetError(){
  selectAllElement('form',null,function(ele){
	  ele.removeAttribute('ferrors');
  })	
}
function removeErrors(){
	var errs =elementsArray(document.querySelectorAll(".mpmsg")),
		cerrs=elementsArray(document.querySelectorAll(".cmperrmsg span"));
		for(var er in errs){
			if(errs[er])
		       errs[er].parentElement.removeChild(errs[er]);
		}
	    for(var r in cerrs){
			if(cerrs[r])
		       cerrs[r].parentElement.removeChild(cerrs[r]);
		}
}
function setInvalidate(){ 
	this.mpinvalid = false;
	removeNode(this.errmsgele);
}
var ll=0,lt=0;
function customBubble(e,msg,crele){
	if(window.event)
	   window.event.preventDefault();
	var span =document.createElement('span');
	    span.classList.add('mperror');
	if(crele){
	   crele
		   .errmsgele = span;	
	   span.innerHTML = msg;	
	   crele.appendChild(span);	
	   setTimeout(function(){
		   removeNode(span);
	    },10000);
	   return;	  	
	}
	if(!e)return;
	var offset =  getExeOffets(e);
	var div  =document.createElement('span');
	div.classList.add('mpmsg');
	span.classList.add('mperror');
	span.innerHTML =msg;
	//span.innerHTML =e.target.validationMessage;
	div.appendChild(span);
	div.style.width =  (e.offsetWidth+40)
	div.style.left  =  offset.left;
	div.style.top   = (offset.top+46);
	document.body.appendChild(div);
	e.errmsgele    = div;	
	setTimeout(function(){
		removeNode(div);
	},10000);
}
function removeNode(ele){
	try{
	  ele.parentElement.removeChild(ele);
	}catch(e){}
}
function getExeOffets(ele){
	 var top = 0, left = 0;
    do {
        top += ele.offsetTop  || 0;
        left += ele.offsetLeft || 0;
        ele = ele.offsetParent;
    } while(ele);

    return {
        top: top,
        left: left
    };
}
var trgr = document.querySelectorAll("[data-trigger^='#']");
var files= document.querySelectorAll("input[type='file']");
var files=elementsArray(files);
var trgr =elementsArray(trgr);
trgr.forEach(function(e,k){
	e.ondragenter = e.ondragover = function (ee) {
                    ee.preventDefault();
                    ee.dataTransfer.dropEffect = 'copy';
                    return false;
    }
	 
	e.onclick = function(){ var ele = document.querySelector(e.getAttribute('data-trigger'));
						   ele.imagelink =e;
						   ele.click()
    }
	e.ondrop = showThumb
});
function showThumb(ee){ee.preventDefault(); 
 var files,image,fr;					   
 if(ee.dataTransfer){
	files = ee.dataTransfer.files;
	image = ee.target; 
 }else{
	files = ee.target.files;
	image = ee.target.imagelink; 
 }
 var dta_fld = image.getAttribute('data-field'),id;
	if(dta_fld)				   
       id = dta_fld.replace("#",'');
	else id = image.getAttribute('data-trigger').replace("#",'');				   
		document.querySelector("."+id+'-error').innerHTML ="";	
var file_control = document.querySelector(image.getAttribute('data-trigger'));					   
if(file_control && files.length>0){
   var maxsize    = file_control.getAttribute('data-maxsize'),
	   extensions = file_control.getAttribute('data-extensions').split(',');
	if((files[0].size/1024/1024)>maxsize){
		document.querySelector("."+id+'-error').innerHTML = "Image Size exceeded limit.";
		return;
	}
	var ext = files[0].type.split("/").pop(),extstatus=false;

	for(var i in extensions){
		if(ext==extensions[i])extstatus=true;
	}
	if(!extstatus){
		document.querySelector("."+id+'-error').innerHTML = "Image type not supported.";	
		return;
	}
	if(!window.FormData){
		document.querySelector("."+id+'-error').innerHTML = "Image upload not supported Pleae try with latest browser.";	
		return;
	}
}					   
//var file_control = image.getAttribute('data-trigger');
					   
if(files.length>1)return;
  fr  = new FileReader();
  fr.onload =  function(e1){
  image.src  = e1.target.result;
  if(dta_fld)	  
     document.querySelector(image.getAttribute('data-field')).value = e1.target.result;
  else{
	  postImage(file_control,files);
  }	  
  }	 
  fr.readAsDataURL(files[0]);	
}
files.forEach(function(e,k){
  	e.onchange  =showThumb;
});
function postImage(control,file){
	var data  = new FormData();
	    data.append('image',file[0]);
	    data.append('imagename',control.getAttribute('id'));
	$.ajax({
		url:control.getAttribute('upload-url')||$('#upload-url').val(),
		method:'POST',
		data:data,
		cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
		success:function(res){ console.log(res) },
		error:function(err){ console.log(err)},
		
	})
}
function elementsArray(ele,clbk){
 return [].slice.call(ele)	
}
function selectAllElement(qs,html,clbk){
	html = html || document;
	var els = html.querySelectorAll(qs);
	if(!clbk)
	   return els;
	els =  elementsArray(els);
	els.filter(clbk);
}
function selectElement(qs,html){ 
	html = html || document;
	//console.log(qs);
    return html.querySelector(qs);
}
function addEventInternal(el,evt,clbk,capture){  //console.log(el)
	if('addEventListener' in el)
	   el.addEventListener(evt,clbk,capture);
	else if('attachEvent' in el)   
    	el.attachEvent('on'+evt,clbk,capture);
}
function addEvent(el,evt,clbk,capture){
 if(el && el.constructor.name == 'String'  && el.constructor.name!='HTMLDocument')
	 el =  selectAllElement(el);
	if(!el || el.length<=0) return;
  if(el && el.length>1 &&  el.constructor==NodeList){
	var  eles =  elementsArray(el); 
	for(var e in eles){
		addEventInternal(eles[e],evt,clbk,capture);
	}  
  }else if(el && !Array.isArray(el)){
	 //console.log(el);
	  addEventInternal(el,evt,clbk,capture);  
  }
}
function liveEvent (eventType, elementQuerySelector, cb) {
    document.addEventListener(eventType, function (event) {

        var qs = document.querySelectorAll(elementQuerySelector);

        if (qs) {
            var el = event.target, index = -1;
            while (el && ((index = Array.prototype.indexOf.call(qs, el)) === -1)) {
                el = el.parentElement;
            }

            if (index > -1) {
                cb.call(el, event);
            }
        }
    });
}
function html2Node(html){
	 if(!html  || typeof html == 'NodeList')return;
 var div = document.createElement('div');	
	 div.innerHTML =html;
	 return div.firstChild;
}
function newNode(tag){
	return document.createElement(tag);
}
/* user type */
addEvent('[data-for^="#"]','click',selectOption);
addEvent("input.register-usertype",'change',activeateOption);

function selectOption(e){ e.preventDefault();
	var hash= this.getAttribute('data-for');
		location.hash = hash.replace("register-usertype-","");				 
	selectElement(hash).click();
}
function activeateOption(){ 
	selectAllElement("a.usertype",null,function(ele){
		if(ele)
		   ele.classList.remove('activate-tb')
	});
	selectElement("a[data-for=\"#"+this.getAttribute('id')+'"\]').classList.add('activate-tb')
}
!(function loadFromHash(){
  var hash = location.hash,ele;
  if(hash.length>2){ ele =  selectElement("#register-usertype-"+hash.replace('#',''));	
    if(ele)
	   ele.click();	
  }
}).call(this);
/*  business type */
addEvent("label.option-form-select",'click',selectaOption);
addEvent("label.option-form-select1",'click',selectaOption1);
function selectaOption(e){ //e.preventDefault(); console.log(this);
	selectAllElement("label.option-form-select",null,function(ele){
		if(ele)
		   ele.classList.remove('activate-tb1')
	});
	this.classList.add('activate-tb1');					  
	
}
function selectaOption1(e){ //e.preventDefault(); console.log(this);
	selectAllElement("label.option-form-select1",null,function(ele){
		if(ele)
		   ele.classList.remove('activate-tb1')
	});
	this.classList.add('activate-tb1');
}
/* company-address */

addEvent(selectElement("#register-business-officeaddresssameasbusiness"),'change',makeSameAddress)
function makeSameAddress(){ 
  var checked =  this.checked;	
  selectAllElement(".office-address-control input[type='text']",null,function(ele){console.log(ele);
	ele.focus();  
	ele.value = checked?selectElement('#'+ele.getAttribute('id').replace('office','registered')).value:"";
  });
}
/* resend verification email */
liveEvent('click',"#resend-verification-email",sendVerificationEmail);
function sendVerificationEmail(e){ e.preventDefault();
	if(selectElement("#login-msg"))							  
	   selectElement("#login-msg").html = "";
	var email =selectElement('#login input[type="text"]');							  
	$.post('/webapp/resend-email-token',{email:email?email.value:null},function(res){
		var iv = selectElement("#isverified-msg"),loginpage = $("#login-msg");	
		if(res.errors){
			if(iv)
		       iv.html =res.errors.username.message;
			else
				mp.validate(res.errors,'login');
		}else{
			if(loginpage.length>0)
			   loginpage.html(res.display.html);
			else iv.html =res.display.html;
		}
	})
}
function shipperUrl(url){
	return '/shipper/'+url;
}
function truckerUrl(url){
	return '/trucker/'+url;
}
function pushUrl(stateobj,title,url){
	if(window.history.pushState){
		window.history.pushState(stateobj,title,url);
		 return event.preventDefault();
	}
}
var cookies = {};
function loadCookies(){
	var c = document.cookie;	
        c = c.split(';');
	for(var i in c){
		var arr = c[i].split('='),key = arr[0].replace(/\s/g,'');
		if(!(key in {'PHPSESSID':0}))
		   cookies[key]  =arr[1];
	}
	
}
loadCookies();
function getCookie(name){ //console.log(cookies);
   return cookies[name] || null;
}
function setCookies(name,value){
	if(value.prototype.name == 'Object')
	   value =JSON.stringify(value);	
	document.cookies +="; "+name+"="+value;
}
function resolveCookiesObject(str){
	return JSON.parse(decodeURIComponent(str).replace(/^j:/,''));
}
Object.defineProperty(this,'_csrf',{
	get:function(){ return selectElement('input[name="_csrf"]').value || null },
	configrable:false,
	//writable:false,
});
var totalitemweight =$("#totalitemweight");
function getField(field){
   return document.getElementById(field);
 }
 function plus(field){
	var mpchange = new Event('mpchange');  
    var val = getField(field).value || 1;
        val = isNaN(val)?1:val;
    if(getField(field)){
       getField(field).value = parseInt(val)+1;
	   getField(field).dispatchEvent(mpchange);	
	   if(totalitemweight.length>0){
		calculateWeight()
	   }	
    }
	 
 }
 function minus(field){
   var mpchange = new Event('mpchange'); 	 
   var val =getField(field).value || 1;
       val = isNaN(val)?1:val;
   if(getField(field) && getField(field).value>1){
       getField(field).value = parseInt(val)-1;
	   getField(field).dispatchEvent(mpchange);	
	   if(totalitemweight.length>0){
		calculateWeight()
	   }
    }
	 
	 
 }
//calculate item weight...
function calculateWeight(){
	var weight = parseFloat($("#item-weight").val()),
		items  = parseInt($("#item-total_item").val()),
		totalWeight  = items*weight;
	    if(weight)
	       totalitemweight.html(totalWeight+' kg');	
	    else
	       totalitemweight.html('0 kg');	
}

// incomplete shipments....
addEvent(selectElement("#incomplete-remove"),'click',removeIncomplete);
function removeIncomplete(e){
	e.preventDefault();
	var form = selectElement('#complete-shipment');
	    form.setAttribute('action',this.href);
	    form.submit();	
}
addEvent(selectElement("[data-close='me']"),'click',hideMPMessage);
function hideMPMessage(){
	var msgc = selectAllElement('div.mp-message-container');
	if(!msgc || msgc.length<=0)return;
	selectAllElement('div.mp-message-container',null,function(k){
		k.parentNode.removeChild(k);
	});
}
setInterval(hideMPMessage,5000)
var boxHeight= 0,params={},waiting,empty=false;
function paginate(p,callback){
boxHeight = p.boxHeight||boxHeight;
params['url']  =  p['url'] || undefined;	
if(p['params'] && p['params'].isPrototypeOf(new Object()))
   params['data'] =  p['params'];
else{
   params['data'] ={};
   params['data']['p'] = p['params'] || undefined;
}	
params['success']  =  callback;	
params['method']   =  p.method||'GET';
}
function getPage(){
  $("#load-page").html('Loading...');
  waiting=1;
  if(!$("#paginate-url").length && !params['url']) return;
	params.data['_csrf']   = _csrf;  
	params.data['offset']  = $("#offset").val();	  
  $.ajax({
		   url:params['url'] || $("#paginate-url").val(),
		   method:params.method,
		   data:params.data,
		   dataType:params.dataType||'html',
		   success:function(res){ 
			if(res<=3) empty=true; 
			params.success.call(params.success,res);
			waiting=0;   
		   },
  });
}
function suddenCall(){	getPage();}
addEvent(document,'scroll',function(e){	if(!$("#paginate-url"))return;
	if(document.body.scrollTop>window.innerHeight && params.success){
	   pullUpButton();	
	}else if(document.body.scrollTop<(window.innerHeight/3))
	  hidePullUpButton();	
	var scrollHeight = (document.body.scrollHeight - window.innerHeight) -boxHeight;
	if((document.body.scrollTop > scrollHeight) && (params.success && !waiting && !empty))
	   getPage()
});
function pullUpButton(){
  if(document.querySelector('.pull-up-button')==null){
	var dv = document.createElement('span');
	dv.innerHTML  = "&#8593;";
	dv.classList.add('pull-up-button')
	//dv.classList.add('glyphicon') 
	//dv.classList.add('glyphicon-chevron-up');
	dv.onclick = moveTop;
	document.body.appendChild(dv);
   }
}
function hidePullUpButton(){
	var pub = document.querySelector('.pull-up-button');
	if(pub)
	   pub.parentElement.removeChild(pub);
}
function moveTop(){
 $(document.body).stop().animate({scrollTop:0}, '700', 'swing', function() { 
   //alert("Finished animating");
  });
}

//star rating...
$.fn.stars = function() {
    return $(this).each(function() {
        // Get the value
        var val = parseFloat($(this).html());
        // Make sure that the value is in 0 - 5 range, multiply to get width
		//val = Math.round(val * 4) / 4; /* To round to nearest quarter */
        //val = Math.round(val * 2) / 2; /* To round to nearest half */
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}
// check is empty data ot not
  function isEmpty( data ){
      return !$.trim(data)
  }
// business type...
$("[name='Register[business_type][btype]']").on('change',function(e){
		$("#register-business_type-text").addClass('hidden')
		$("#register-business_type-text").val('')
	if(this.value==4){
		$("#register-business_type-text").removeClass('hidden')
	}
})
$("[name='Register[company_type][ctype]']").on('change',function(e){
		$("#register-company_type-text").addClass('hidden')
		$("#register-company_type-text").val('')
	if(this.value==4){
		$("#register-company_type-text").removeClass('hidden')
	}
})

