"use strict";
function dateInterval(date1,date2){
 var startTime = new Date(date1).getTime();//moment(date1, 'hh:mm:ss a');
 var endTime   = new Date(date2||Date.now()).getTime();//moment(date2 ||Date.now(), 'hh:mm:ss a');
 var duration  = startTime  - endTime;
 var seconds   = 1000,
	 minutes   = seconds*60,
	 hours     = minutes*60,
	 days      = hours*24, 
	 weeks     = days*7,
	 months    = weeks*30,
	 years     = months*12,
	 interval  = '';
 if(duration>years)
	interval +=Math.ceil(duration/years%12) +" Years(s) ";
 if(duration>months)
	interval +=Math.ceil(duration/months%30) +" Month(s) ";
 else if(duration>weeks)
	interval +=Math.ceil(duration/weeks%7) +" Week(s) ";
 else if(duration>days)
	interval +=Math.ceil(duration/days)  +" Day(s) ";
 if(duration>hours)
	interval +=Math.ceil(duration/hours%60) +" Hour(s) ";
 if(duration>minutes)
	interval +=Math.ceil(duration/minutes%60) +" Minute(s) ";
  if(duration>seconds)
	interval +=Math.ceil(duration/seconds%60) +" Second(s) "; 
	
  return interval || "Expired";	
}
function loadShipmentPendingYet(){
	$("span[data-expire]").each(function(k,val){
		var $this = this;
		setInterval(function(){
			var intr = dateInterval($($this).data('expire'));
			if(intr=='Expired'){
				$this.parentElement.parentElement.innerHTML = '<p><span>Bid Expired</span></p>';
			    $("."+$this.getAttribute('id')).addClass('disabled').removeAttr('data-target');
			}
			else $this.innerHTML = intr; 	
		},1000)
	});
}
loadShipmentPendingYet();
$("#bids").on('submit',function(e){ e.preventDefault();
	$("#submitbtn").next().click();
	location.href = $('[name="redirect"]').val();
});
function lastBid(id){
$.ajax({
	url:$('[name="last-bid-paced"]').val(),
	method:'post',
	data:{_csrf:_csrf,_id:id},
	dataType:'json',
	success:function(res){
		if(res.status && res.status==200){
			var ele  =$("#"+res.bid.shipment_id);
			if(ele){
			   ele.addClass('hide');
			   ele.data('rebid',true);
			   ele.next().removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id).removeClass('hide');	
			   $("#ab-"+res.bid.shipment_id+" strong").html(res.bid.price);	
			   	
			}
			for(var i in res.bid){
				$('[name="Bids['+i+']"]').val(res.bid[i]).focus();
			}
		}
	}
})
}
lastBid();
function checkStatus(){
$(".bidnow").each(function(k,v){
	var id = this.getAttribute('id');
	if(!this.getAttribute('data-rebid'))
	    lastBid(id);	
});
}
checkStatus();
