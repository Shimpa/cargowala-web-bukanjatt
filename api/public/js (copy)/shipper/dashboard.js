"use strict";
$('#bid-loading-date').datetimepicker({
	format: 'DD/MM/YYYY',
	minDate:Date.now()+1000*60*60*1,
});
$('#bid-loading-time').datetimepicker({
	format: 'HH:mm:ss',
	minDate: Date.now()+1000*1,
});
$('#bid-loading-date').focus().blur();
$('#bid-loading-time').focus().blur();
var data  = {},reg_str = new RegExp('[A-z]','g');
$( "#bid-loading" ).autocomplete({
  minLength: 1,
  source: function( request, response ) {
	var term = request.term;
	//return data.lp;  
    var dta =  searchCity(data.lp,request.term);
		$(this).get(0).element[0].onkeyup =  function(e){
			if(e.keyCode==8){
				$("#loading-city").val('');
				$("#unloading-city").val('');
				$("#loading-state").val('');
				$("#unloading-state").val('');
			}
			validateBooking(true);			
			var dta =  searchCity(data.lp,this.value);
		    if(dta.length>0){
			  $("#bid-loading").val(this.value).css('display','block').focus();	
	          $("#bid-custom-loading").val('').css('display','none');
		    }
			if(this.value==""){
			 $("#bid-loading").css('display','block').focus();	
	         $("#bid-custom-loading").css('display','none');
			}			
		}  
	if(dta.length>0){
	   $("#bid-loading").css('display','block').focus();	
	   $("#bid-custom-loading").css('display','none');	
	   response(dta);
	}else{
		  $("#bid-loading").css('display','none');	
	      $("#bid-custom-loading").css('display','block').val($("#bid-loading").val()).focus();
		  $("#bid-loading").val('')
		var options = {
                      types: ['geocode'],
                      componentRestrictions: {country: "in"}
                    };
		var input   = document.getElementById("bid-custom-loading");
	    var ac = new google.maps.places.Autocomplete(input, options);
		google.maps.event.addListener(ac, 'place_changed', function() { 
			$("#bid-unloading").css('display','none');	
	        $("#bid-custom-unloading").css('display','block');
			var place = ac.getPlace(),city,state; 
			    place = place.formatted_address.split(",");
			console.log(place);
			if(place.length>2 && reg_str.test(place[1])==true){
				city  = place[0].replace(/^\s|\d|\s$/g,"");
				state = place[1].replace(/^\s|\d|\s$/g,"");			
			}else{
				city = place[0];
				state= place[0];
			}
				
			$.post("/shipper/dropdownlist/_up_",{c:city,s:state},function(res){
				   if(res.length>0){
		             $("#bid-unloading").css('display','block');	
	                 $("#bid-custom-unloading").css('display','none');
					 data.up = res;
				   }else{
					  data.up = []; 
					 $("#bid-unloading").css('display','block');	
	                 $("#bid-custom-unloading").css('display','none');
				   }	
	        });
			$("#loading-city").attr('name','Bid[loading-city]').val(city);  
	        $("#loading-state").attr('name','Bid[loading-state]').val(state);
		
			$("#bid-unloading").val('');
	        $("#bid-custom-unloading").val('');
			$("#unloading-city").val('');
	        $("#unloading-state").val('');
			validateBooking(false);
        });
		input.onkeyup =  function(e){
			validateBooking(true);
			if(e.keyCode==8){
				$("#loading-city").val('');
				$("#unloading-city").val('');
				$("#loading-state").val('');
				$("#unloading-state").val('');
			}
			var dta =  searchCity(data.lp,this.value);
		    if(dta.length>0){
			  $("#bid-loading").val(this.value).css('display','block').focus();	
	          $("#bid-custom-loading").val('').css('display','none');
		    }
			if(this.value==""){
			 $("#bid-loading").css('display','block').focus();	
	         $("#bid-custom-loading").css('display','none');
			}			
		}
		google.maps.event.addDomListener(input, 'keydown', function(e) { 
            if (e.keyCode == 13) { 
                 e.preventDefault(); 
            }
		});
	}  
  },
  select: function (event, ui) {  
    $.post("/shipper/dropdownlist/_up_",{c:ui.item.city,s:ui.item.state},function(res){
		 if(res.length>0){
		  $("#bid-unloading").css('display','block');	
	      $("#bid-custom-unloading").css('display','none');
	       data.up = res;
		 }else{
					  $("#bid-unloading").css('display','none');	
	                  $("#bid-custom-unloading").css('display','block').focus();
				   }
	});
	$("#loading-city").attr('name','Bid[loading-city]').val(ui.item.city);  
	$("#loading-state").attr('name','Bid[loading-state]').val(ui.item.state);  
	$("#bid-unloading").val('');
	$("#bid-custom-unloading").val('');
	$("#unloading-city").val('');
	$("#unloading-state").val('');
	validateBooking(false);  
  },   	
});
$( "#bid-unloading" ).autocomplete({
  minLength: 1,
  source: function( request, response ) {
	var term = request.term;
	//return data.lp;  
    var dta =  searchCity(data.up,request.term);
    		$(this).get(0).element[0].onkeyup =  function(e){
			if(e.keyCode==8){
				//$("#loading-city").val('');
				$("#unloading-city").val('');
				//$("#loading-state").val('');
				$("#unloading-state").val('');
			}
			validateBooking(true);			
			var dta =  searchCity(data.up,this.value);
		    if(dta.length>0){
			  $("#bid-unloading").val(this.value).css('display','block').focus();	
	          $("#bid-custom-unloading").val('').css('display','none');
		    }
			if(this.value==""){
			 $("#bid-unloading").css('display','block').focus();	
	         $("#bid-custom-unloading").css('display','none');
			}			
		}
	if(dta.length>0){
	   $("#bid-unloading").css('display','block');	
	   $("#bid-custom-unloading").css('display','none');	
	   response(dta);
	}else{
		  $("#bid-unloading").css('display','none');	
	      $("#bid-custom-unloading").css('display','block').val($("#bid-unloading").val()).focus();
		  $("#bid-unloading").val('')
		var options = {
                      types: ['geocode'],
                      componentRestrictions: {country: "in"}
                    };
		var input1   = document.getElementById("bid-custom-unloading");
	    var ac1 = new google.maps.places.Autocomplete(input1, options);
		google.maps.event.addListener(ac1, 'place_changed', function() { 
			$("#bid-unloading").css('display','none');	
	        $("#bid-custom-unloading").css('display','block');
			var place1 = ac1.getPlace(),city1,state1; 
			    place1 = place1.formatted_address.split(","); 
			if(place1.length>2 && reg_str.test(place1[1])==true){
				city1  = place1[0].replace(/^\s|\d|\s$/g,"");
				state1 = place1[1].replace(/^\s|\d|\s$/g,"");			
			}else{
				city1 = place1[0];
				state1= place1[0];
			}
	
			if($("#bid-loading").val()||$("#bid-custom-loading").val()){
		    getDirection($("#bid-loading").val()||$("#bid-custom-loading").val(),
						 $("#bid-unloading").val()||$("#bid-custom-unloading").val() 
						);
			}
			   $("#unloading-city").attr('name','Bid[unloading-city]').val(city1);  
	           $("#unloading-state").attr('name','Bid[unloading-state]').val(state1);

			validateBooking(false);
			
        });
		input1.onkeyup =  function(e){
		    if(e.keyCode==8){
				//$("#loading-city").val('');
				$("#unloading-city").val('');
				//$("#loading-state").val('');
				$("#unloading-state").val('');
			}
			validateBooking(true);
			var dta =  searchCity(data.up,this.value);
		    if(dta.length>0){
			  $("#bid-unloading").val(this.value).css('display','block').focus();	
	          $("#bid-custom-unloading").val('').css('display','none');
		    }
			if(this.value==""){
			 $("#bid-unloading").css('display','block').focus();	
	         $("#bid-custom-unloading").css('display','none');
			}
			
		}
	   google.maps.event.addDomListener(input1, 'keydown', function(e) { 
            if (e.keyCode == 13) { 
                 e.preventDefault(); 
            }
        });
	}  

  },
  select: function (event, ui) { 
    $.post("/shipper/dropdownlist/_up_",{c:ui.item.city,s:ui.item.state},function(res){
		data.up = res;
	});
	getDirection($("#bid-loading").val()||$("#bid-custom-loading").val(),
						 $("#bid-unloading").val()||$("#bid-custom-unloading").val() 
						);
	$("#unloading-city").attr('name','Bid[unloading-city]').val(ui.item.city);  
	$("#unloading-state").attr('name','Bid[unloading-state]').val(ui.item.state);  
    validateBooking(false);	
  }
});

function searchCity(arr,term){
	var a =[];
	for(var i in arr){
		var lbl = arr[i].city + "," + arr[i].state+",India";
		var re = new RegExp('^'+term,'ig');
		if(re.test(lbl))
			a.push({
				label:lbl,
				city:arr[i].city,
				state:arr[i].state
			});
	}
	return a;
}

$.post('/shipper/dropdownlist/__lp__',function(res){ data.lp=res || []});
$('#shipment-address').on('submit',function(e){ e.preventDefault();
	var form = $(this),
		submit = form.find('button'); 								  
		submit.html($('<img>').attr('src',submit.attr('data-src')));						  
	$.post($(form).attr('action'),$(form).serialize(),function(res){
		 selectElement(".datetime-error").innerHTML = "";
		 submit.html(submit.attr('data-value'));
		if(res.status==200){
		    window.location.hash = "step1";
			window.location.href =res.next+"#step2";
		}else if(res.status==400){
			if(res.html){
			  selectElement(".datetime-error").innerHTML = res.html;
			}else{ 
			//$('input[type="text"]').val('');
			$('input.hidden-vals').val('');
			$("#bid-loading").val(this.value).css('display','block').focus();	
	        $("#bid-custom-unloading").val('').css('display','none');
			$("#bid-unloading").css('display','block').focus();	
	        $("#bid-custom-unloading").css('display','none');
			validateBooking(false);
			}
		}
	});							  
});

var formbg = selectElement(".setformopacity");
formbg.onmouseover = function(){this.style.backgroundColor = "rgba(255,255,255,.9)";};
formbg.onmouseout  = function(){this.removeAttribute('style');};
function validateBooking(state){
	var valu = $("#loading-city").val() && $("#loading-state").val() &&
		       $("#unloading-state").val() && $("#unloading-state").val();
	if(valu)
	  $("#submit button").attr('disabled',false).attr('title'," ");
    else $("#submit button").attr('disabled',true).attr('title','Please select city from list');
}
var fname =  {'loading-city':0,'loading-state':1,'unloading-city':2,'unloading-state':3};
function loadPageData(h){ if(!h)return;
	var step1 =resolveCookiesObject(getCookie('dashboard')),btn;
   for( var f in step1){
	   var fld=selectElement("#"+f);
	   if(fld){
		  if(f in fname){
			 fld.setAttribute('name','Bid['+f+']');
			 fld.value = step1[f];
		  }else fld.value = step1[f]; 
	   }
   }
   validateBooking();			   

}	
loadPageData(location.hash);




