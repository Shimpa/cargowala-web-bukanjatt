"use strict";
loadLiveData();
function isBidPosted(ele){ 
   if(!ele)return;	
   $.ajax({
	  url:$("#bid-url").val(),
	  method:'POST',
	  data:{sid:ele.getAttribute('data-bids').replace('trucker-',''),_csrf:_csrf},
	  success:function(res){
		  var html="";
		  if(res.status){
			 if(res.data && res.data.t){var data = res.data.t;
				html+='<hr><div class="row">';
				html+='<div class="col-md-7 pR10 col-xs-6 col-sm-4">';
				html+='Trucker Name :<h3 class="mT0"> '+data.trucker_id.name.firstname+" "+data.trucker_id.name.lastname+'</h3></div>';
				html+='<div class="col-md-5 text-right pL0 pR0 col-xs-6 col-sm-4">';
				html+='<span class="stars">3.45</span></div>';
				if(ele.getAttribute('data-status')!=0 && ele.getAttribute('data-status')!=9){					
				   html+='<p class="col-sm-6 col-xs-12 col-md-12 pL0 pR0"> ';
				   html+='Contact No. <a href="tel: +'+data.trucker_id.contact.mobile_number+'" class="pull-right">'+data.trucker_id.contact.mobile_number+'</a></p>';
				}
				ele.removeAttribute('data-bids');						
				ele.innerHTML = html;		  
				$('span.stars').stars();						
			 }else{
			  $('[data-bids="trucker-'+res.data.sid+'"]').remove();  
		     }
			  
		  }
			  
	  }, 
   });	
}
function loadLiveData(){
selectAllElement('.active div[data-bids^=trucker]',null,function(ele,i){	
	isBidPosted(ele);
});	
}
var historyTab = false,activeData=true,historyData=true;
function PaginateTab(){ 
	if(!historyTab && activeData && $('#active-paginate').length){
paginate({
				offset:$("#offset").val(),
				url:$('#active-paginate').val(),
				boxHeight:710,
				method:'POST'
			},function(res){ $(".active #load-page").html(''); 
				if(!res){$(".active #load-page").html('No More Records'); activeData=false;}
				$("#active-shipments").before(res);
				loadLiveData();			
});
}else if(historyTab && historyData && $('#history-paginate').length){
	paginate({
				offset:$("#offset").val(),
				url:$('#history-paginate').val(),
				boxHeight:710,
				method:'POST'
			},function(res){ $(".active #load-page").html(''); 
				if(!res){$(".active #load-page").html('No More Records');historyData=false;}
				$("#history-load").before(res);
				loadLiveData();
				//$('body').scrollTop(90);			
	    });
}
}
PaginateTab();

$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){
	var active =$(this).parent('li').hasClass('active');
  	if($(this).attr('href')=='#shipmentshistory' && active){
	    historyTab =true;
	if($("#shipmentshistory .row").length<=0){	
	    PaginateTab();
		PaginateTab();
	}
		
	}else{ 
	   historyTab=false;
	}
       
});
$( window ).scroll(PaginateTab);
