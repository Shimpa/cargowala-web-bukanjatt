"use strict";
var cp= require('child_process');
var path= require('path');
var email_queue = [];
process.on('message',function(m){
	if(m.queue){
	   email_queue = m.queue;	
	}else email_queue  = [{member:m.email,message:m.message}];
	sendEmail();
  process.exit(1);
});
function sendEmail(){
	var email,message,obj={};
	if(email_queue.length<=0)return;
	   obj  =email_queue.pop();
	email   = obj.member;
	message = obj.message;
        //options.agent = keepAliveAgent;
	var p  = path.normalize('./Workers/Mailer.php ');
	cp.exec('php '+p+email+" onemail "+message,function(res){ 
		console.log(res);
		if(res) sendEmail();
	}); 
}