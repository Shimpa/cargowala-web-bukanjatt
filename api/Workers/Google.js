"use strict";
var map = require('google_directions');
process.on('message',function(m){
    var params = {// REQUIRED
            origin: m.lp_address,
            destination: m.up_address,
            key: 'AIzaSyCWpDytcN-LWndoxERaYu31eB400w4TVYo',
// OPTIONAL
            mode: "",
            avoid: "",
            language: "en",
            units: "",
            region: "in",
    };
	map.getDirections(params, function (err, data){
           if (err) process.send(null);
           if(data.routes[0]!=undefined){
			  process.send(JSON.stringify(data.routes[0] || {}));   
		   }else process.send(null);
	});
});
