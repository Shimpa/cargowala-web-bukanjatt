<?php
require dirname(__DIR__).'/vendor/PHPMailer/PHPMailerAutoload.php';
class Mailer extends PHPMailer{
    private $name,$email_to;
	private $method;
	private $hash_token;
    private $isSmtp = false;
    public function __construct($args){
		$this->initArgs($args);
		$this->trigger($args);
    }
	private function initArgs($args){ 
		$this->email_to   = empty($args[1])?'':$args[1];
		$this->name       = empty($args[4])?'':$args[4];
		$this->hash_token = empty($args[3])?'':$args[3];
	}
	private function hashToken(){	   
		return $this->hash_token;
	}
	private function emailTo(){
		return $this->email_to;
	}
	private function trigger($arr){
		$method = @$arr[2];
		if(is_callable(get_class($this),$method))
			$this->$method();
	}
	private function confirmemail(){ //die('here');
	  $html = '<p>Hi '.$this->name.',</p>
            <p>Welcome Aboard!!!</p>

            <p>Please <a href="http://cargowala.com/dev/frontend/web/index.php?r=site/validate-email&token=' . $this->hashToken() .'" target="_blank">Click Here</a> to verify your email id.</p>

            <p>For any queries write to us at support@cargowala.com</p>

            <p>Cheers!! <br>
            Team Cargowala</p>';
      $subject = 'Confirm Your Email | Cargowala';
	  $this->sendMail($subject,$html);
	}
  private function passwordupdated(){
	  $html = '<p>Hello '.$this->name.'</p>
            <p>Your password updated successfully.</p>
            
			
			
            <p>For any queries write to us at support@cargowala.com</p>

            <p>Cheers!! <br>
            Team Cargowala</p>';
      $subject = 'Password Updated | Cargowala';
	  $this->sendMail($subject,$html);
  }
   private function resetpassword(){
	  $html = '<p>Hello '.$this->name.'</p>
            <p>Your have requested for reset password.</p>
            
			<p>Click on below link to reset password.</p>
			
			<p><a href="http://cargowala.com/dev/frontend/web/index.php?r=site/reset-password&token=' . $this->hashToken() .'" target="_blank">Click Here</a></p>
              
            <p>For any queries write to us at support@cargowala.com</p>

            <p>Cheers!! <br>
            Team Cargowala</p>';
      $subject = 'Password Reset Mail | Cargowala';
	  $this->sendMail($subject,$html);
  }	
  private function sendMail($subject,$html){
	  $this->setFrom('no-reply@cargowala.com','no-reply');
	  $this->addAddress($this->emailTo());
	  $this->Subject = $subject;
      $this->MsgHTML($html);
	  $this->isHTML(true);	
      //$this->AltBody = 'This is the body in plain text for non-HTML mail clients';	
	  if(!$this->send())
         print_r($this->ErrorInfo);
	  else die("Mail sent successfully.");  	
	  exit(0);
  }	
}
new Mailer($argv);
print_r($argv);die;
