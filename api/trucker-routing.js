module.exports = function(trucker){
trucker.use('/', require('./routes/trucker/index'));
trucker.use('/users', require('./routes/trucker/users'));
trucker.use('/dropdownlist', require('./routes/dropdownlist/'));
trucker.use('/shipments', require('./routes/trucker/shipments'));
trucker.use('/vehicles', require('./routes/trucker/vehicles'));
trucker.use('/drivers', require('./routes/trucker/drivers'));
trucker.use('/bids', require('./routes/trucker/bids'));
trucker.use('/users/payments', require('./routes/trucker/users/payments'));
};