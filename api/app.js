var compression   = require('compression')
var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var mongo         = require('mongoose');
var app           = express();
var shipper       = express();
var trucker       = express();
var agent         = express();
var webapp        = express();
var session       = require('express-session');
var expressLayouts= require('express-ejs-layouts');
var passport      = require('passport');
var FacebookStrategy  = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true });

//const MongoStore  = require('connect-mongo')(session);

// india timezone
process.env.TZ = 'Asia/Kolkata';

//compression...
app.use(compression({filter: shouldCompress}))

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}
// page title common function...
global.Appage     ={};
global.pageTitle  = 'CargoWala';
global.Appage.title= function(t){
global.pageTitle  = 'CargoWala - ';
 pageTitle+=t; 	
} 

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'layouts/mylayout');
var loadedModule = false;
app.use(function(req,res,next){
var type = req.url.split('/');
	if(type.length>0)
	   loadedModule = type[1] in {'shipper':21,'trucker':12}?type[1]:'shipper';
app.set('layout', 'layouts/'+loadedModule+'/mylayout');
next();	
});
app.use(expressLayouts);
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'book-truck.png')));
//app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({limit:'7mb'}));
app.use(bodyParser.urlencoded({ extended: true ,limit:'7mb',keepExtensions: true,defer: true}));
//app.use(express.methodOverride());
app.use(cookieParser());
var maxAge  =1000*60*60*24*5;
maxAge  =0;
app.use(express.static(path.join(__dirname, 'public'),{ maxAge: maxAge }));
mongo.connect('mongodb://127.0.0.1:27017/cargowla-dev'); // database connection... 
app.use('/img',express.static(path.join("../media", '/')));
// session uses
app.use(session({
    /*store: new MongoStore({
        //url: 'mongodb://127.0.0.1:27017/cargowla',
        mongooseConnection: mongo.connection,
		autoRemove: 'interval',
        autoRemoveInterval: 60*60*24*2,
		touchAfter: 24 * 3600		
    }),*/
	/*cookie:{
	     maxAge:1000*60*60*5	
	},*/
	resave: false,
	saveUninitialized: true,
	secret: 'dsfhldfhdh0987070',
}));

app.use([setLanguage,setBaseUrl]);
global.MD5        = require('md5');
global.appModel   = require('./models/');
global.appHelper  = require('./helpers');
global.Const      = appHelper('Const');
global.Logger     = appHelper('Logger');
global.Common     = appHelper('Common');
global.Cache      = appHelper('Cache');
global.Validate   = appHelper('Validations');
//global.assetsp    = '/cw-assets/';
global.assetsp    = '/';
global.sbtnimg    = global.assetsp+'images/loader16x16w.gif';
global.sbtnimg_black    = global.assetsp+'images/bid-load-board.gif';
var User = appHelper('User');
global.appLang = function(code){
	var lang={'en':"English",'hi':"Hindi"};
	try{
	if(!(code in lang))
		code = 'en';
	return (function appLanguage(){
	    return {
			code:code,
			text:lang[code],
		}; 
    }).call(this);
	}catch(e){
		console.log(e.message);
	}
}
// logged in user preloaded information...

app.use(function(req,res,next){
   global.loggedUser = false;	
   if(req.session && req.session.user){
		  if(req.session.user.constructor &&  req.session.user.constructor.name != 'User'){	 
			 req.session.user  = new User(req.session);
		  }
		     var Settings = appModel('Settings');
		     var Memberships = appModel('Memberships');
             var Settings = new Settings();
		     Settings.Model.findOne({},'-created_on -modified_on -_id',function(err,s){
				 if(req.session && s)
			        req.session.settings  = s;
			 });
	         global.loggedUser =req.session.user;	
	    next();		
	}else next();
});




var traceon = false;
console.log  = function(msg){
	if(traceon)
	   this.trace(msg);
	else this.warn(msg);
}


// set application language...
function setLanguage(req,res,next){
	try{
	var route   = req.url.split("/"),
		action  = route[2] || 'default';
	req.appLang = global.appLang(req.session.applang).code;
	global.SITEURL  = req.protocol + "://" + req.get('host') + req.originalUrl || "http://localhost";
	global.BASEURL  = req.protocol + "://" + req.get('host') || "http://localhost";	
	var AL = req.appLang,action = action.replace('-','_',action);
	req.__    = require('./locales')(AL,action);
	global.tt = req.__;
	global.__ = req.__;
	next();
	}catch(e){
		console.log("Error:" + e.message);
	}
}
// set site base url...
function setBaseUrl(req,res,next){
	try{	
	req.session.setFlash  =  function(key,msg){
		//if(req.session.fmsg && req.session.fmsg['init']!=1)	
	       req.session.fmsg  ={init:1};
		req.session.fmsg[key] = msg;
	}
	req.session.Flash  = function(key){
		if(!key || !req.session || !req.session.fmsg)return;
		console.log(req.session.fmsg);
		console.log(key);
		var flash  = req.session.fmsg[key] || false;
		req.session.fmsg[key] =null;
		delete req.session.fmsg[key];
		return flash;
	}
	req.session.hasFlash  = function(key){
		if(!key || !req.session || !req.session.fmsg)return;
		return req.session.fmsg[key] || false;
	}
	global.flash = req.session.Flash;	
	global.hasflash = req.session.hasFlash;	
	}catch(e){
	 console.log(e.message);	
	}
	var host  = req.get('host');
	req.site_url  = req.protocol + "://" + req.get('host') + req.originalUrl || "http://localhost";	
	
	next();
}



/* facebook login */
  passport.use(new FacebookStrategy({
    clientID: '167771633620431',
    clientSecret: 'ded234b16ab754c4c04c3802f63436fa',
    callbackURL: "http://cargowala.com/webapp/facebook-callback",
    profileFields: ['id', 'email','photos', 'locale', 'name','verified']
  },
  function(accessToken, refreshToken, profile, cb) {
    return cb(profile._json || null); 
  }
));
/* google login */
passport.use(new GoogleStrategy({
    clientID: '931749727484-5v70n9flg7u2b9ar077ovgmjrfoev9s5.apps.googleusercontent.com',
    clientSecret: 'mCa-7g4hIBbQfOqAmebbiyyi',
    callbackURL: "http://cargowala.com/webapp/google-callback"
  },
  function(accessToken, refreshToken, profile, cb) {
      return cb(profile._json|| null);
  }
));


/* global local view accesible varibales */
//global.tt = require('./locales')(global.appLang,'default');
global.re = appHelper('RegularExp');
global._locals = require('./locales')();
global.DateTime = appHelper('DateTime');
global.shipperUrl = function(url,callback){
	if(callback) url =  callback(url);
	   return '/shipper/'+ url || null;
};
global.truckerUrl = function(url,callback){
	if(callback) url = callback(url);
	return '/trucker/'+ url || null;
};
global.agentUrl = function(url,callback){
	if(callback) url = callback(url);
	return '/agent/'+ url || null;
};
global.webappUrl = function(url){
	return '/webapp/'+ url || null;
};
global.absUrl = function(url){
	return global.BASEURL+url;
}


app.use('/webapp/',webapp);
app.use('/shipper/',shipper);
app.use('/trucker/',trucker);
app.use('/agent/',agent);
//app.use('/',require('./routes/index'));
require('./webapp-routing')(webapp);
require('./shipper-routing')(shipper);
require('./trucker-routing')(trucker);
require('./agent-routing')(agent);
global.UploadedFiles=[];
shipper.post('/upload',_AccessRules,uploadFile);
trucker.post('/upload',_AccessRules,uploadFile);
shipper = trucker = webapp  = null;
function uploadFile(req,res){ 
	var  formidable = require('formidable'),
	     form = new formidable.IncomingForm();
	form.parse(req, function(err, fields, file) { 
	var image = file.image;
	if(!Common.validImageSize(image.size))
	   res.json({status:Const.HTTP_BAD_REQUEST,error:req.__().e_imagesize_exceeded});
	else if(!Common.validImageType(image.type))			 
	   res.json({status:Const.HTTP_BAD_REQUEST,error:req.__().e_imagetype_unsupported});
	else{
		var img = {},ele={},stat=true;		
		global.UploadedFiles.forEach(function(element,i){ 
			if(element.hasOwnProperty(fields.imagename) && stat){
			   global.UploadedFiles[i][fields.imagename] = image.path;
			   return stat = false;  	
			}
		});
		if(global.UploadedFiles.length<=0){
		   img[fields.imagename] =true;
		   img['tmpfile']  =image.path;
		   img['filename']  = Date.now()+".jpg";
		   global.UploadedFiles.push(img);		
		}else if(stat){
		   img[fields.imagename] =true;
		   img['tmpfile']  =image.path;
		   img['filename']  = Date.now()+".jpg"; 	
		   global.UploadedFiles.push(img);			   
		} 
		res.json({status:Const.HTTP_SUCCESS,name:image.name});
	}
	});
}
function _AccessRules(req,res,next){
	if(req.session.user.isShipper() || req.session.user.isTrucker() || req.session.user.isAgent())
	   return next();	
	throw new Error();
}



//res.removeHeader("X-Powered-By");
/* authenticate every request
   validate is it valid request
*/
function authenticate(req,res,next){
	//console.log(req.session.sToken); 
	next();
}
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
	//new Logger(req.url+"("+err.status|| 500+":"+err.message+")"); 
	//console.log(err)  
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
	//next(err);  
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
