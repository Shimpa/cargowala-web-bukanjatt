"use strict";
var router = require('express').Router(),
    Users   = appModel('Users'),
	Users   = new Users(),
	worker  = appHelper('Worker');
router.post('/login', function(req, res,next) {
  var username = req.body.username || false,
	  social   = req.body.social   || false;
	  Users.oldPassword = req.body.password || false;
	  Users.userType    = 3;
	Users.load({'contact.email':username},function(err,user){
		if(err) return new Logger(err);
		switch(true){			
			case (!social && (!Users.validPassword || !Users.userType)):
			 res.out(Const.HTTP_BAD_REQUEST); 
			break;
			case (!social && user && !Users.isActive && user.hash_token):
			 res.out(Const.CUSTOM_MAILNOTVERIFIED); 
			break;
			case ((!social && !Users.isActive) || (social && user && !Users.isActive)):
			 res.out(Const.CUSTOM_INACTIVE); 
			break;
			case (user && Users.validPassword && Users.isActive):
			setAuthentication(req,res,user);	
			res.out(Const.HTTP_SUCCESS,getProfileData(user));		   	
			break;
			case (user && (social!==false)):
			setAuthentication(req,res,user);
			//console.log(user);	
		    res.out(Const.HTTP_SUCCESS,getProfileData(user));
			break;	
			default:
			 register(req,res); 
			break;			
		}
	});	
	username = social = null;	
});
/* POST register user. */
router.post('/register', function(req, res) {
 req.body.contact ={
	email:req.body.username 
 };
 req.body.status  = 0;
 addUser(req,res);
});
function addUser(req,res){
     req.body.role    = 3;
	 var User  = Users.User(req.body);
     User.save(function(err){
	 if(err){ 
		 new Logger(err);
		 return res.out(Const.HTTP_CONFLICT); 
	 } 
	 res.out(Const.HTTP_SUCCESS,getProfileData(User));
	});	
}

router.post('/resend-email-token',function(req,res,next){
	var email = req.body.email || false;
	var token = MD5(email+Date.now())+":"+Date.now(); //{$set:{hash_token:token}},
	Users.Model.findOne({'contact.email':email},function(err,user){
	 if(user){
		worker.execPHP('Mailer.php '+email+" confirmemail "+token,function(err,r){
		         console.log(err);	 
		         console.log(r);
		    if(!err){
			   user.hash_token=  token;
	           user.save();
			   res.out(Const.HTTP_SUCCESS);
			}else{
			   res.out(Const.CUSTOM_SERVER_ERROR);
			}
			//next();
          }); 
	 }else{
		res.out(Const.HTTP_BAD_REQUEST); 
	 }	
	});
});

// change user password...
router.post('/update-password',function(req,res,next){
	var user_id = req.body._id || false;
	var oldpassword = req.body.oldpassword || false;
	var newpassword = req.body.newpassword || false;
	if(!newpassword)
	  res.out(Const.HTTP_BAD_REQUEST);	
	Users.Model.findOne({_id:user_id,password:MD5(oldpassword)},function(err,user){
	 if(user){
		user.isvalidated = 1; 
	    user.password=  MD5(newpassword);
	    user.save();
		var name  =  user.name.firstname || '  '; 
		worker.execPHP('Mailer.php '+user.contact.email+" passwordupdated  fgfdgdfgdfg42425 "+name,function(err,r){
		         console.log(err);	 
		         console.log(r);
			//next();
          }); 
	    res.out(Const.HTTP_SUCCESS);	
	 }else{
		res.out(Const.HTTP_BAD_REQUEST); 
	 }	
	});
});
// change user password...
router.post('/forgot-password',function(req,res,next){
	var email = req.body.email || false;
	var token = MD5(email+Date.now())+":"+Date.now();
	Users.Model.findOne({"contact.email":email},function(err,user){
	 if(user){
		if(user.hash_token!==null){
		 var otoken =   user.hash_token.split(":");
		 var oneday = 1000*60*60*24;	
		 if(otoken[1] && Date.now()<=otoken[1]+oneday)
			res.out(Const.HTTP_TOO_MANY_REQUEST);
			return;
		}
		  	
		user.isvalidated = 1; 
	    user.hash_token=  token;
	    user.save();
		var name  =  user.name.firstname || '  '; 
		worker.execPHP('Mailer.php '+user.contact.email+" resetpassword "+token+" "+name,function(err,r){
		         console.log(err);	 
		         console.log(r);
			//next();
          }); 
	    res.out(Const.HTTP_SUCCESS);	
	 }else{
		res.out(Const.HTTP_NO_CONTENT); 
	 }	
	});
});

//create profile ...
router.post('/create-profile',function(req,res){ 
    var re      = appHelper('RegularExp');
	(re.isString(24,32).test(req.body._id) && re.isString(2,50).test(req.body.firstname) 
 && re.isString(2,50).test(req.body.lastname)) || res.out(Const.HTTP_BAD_REQUEST);
	 var dta={};	
	Users.Model.findOne({'_id':req.body._id},function(err,user){
		err  && new Logger(err);
		user || res.out(Const.HTTP_NO_CONTENT);
		//remove unwanted indexes
		delete req.body.password;
		delete req.body.status;
		delete req.body.hash_token;
		delete req.body.mobile_tokens;
		delete req.body.created_on;
		delete req.body.modified_on;
		delete req.body.mobile_tokens;
		for(let i in req.body){
			user[i]=req.body[i] || '';
		}
		
		var em  = user.contact.email;
		user.contact ={
			email     : em,
			mobile_no : req.body.mobile_no
		}
		user.name = {
		   firstname: req.body.firstname,	
		   lastname : req.body.lastname,	
		};
		uploadImage(req,res,user);
		if(!(re.isUrl(req.body.image_url) || (req.body.image_base64 && req.body.image_base64.length>10)))             saveUser(user,res);
				
	});	
});
function saveUser(user,res){
	user.isvalidated =1;
	user.save(function(er){
		(er && new Logger(er)) || res.out(Const.HTTP_SUCCESS,getProfileData(user));		
	});
}

router.post('/get-otp',function(req,res){
	var re      = appHelper('RegularExp');
	if(req.body.mobile_no===undefined || !re.mobile_no.test(req.body.mobile_no)
     ||(req.body._id && req.body._id.length<10)){ 
	 res.out(Const.HTTP_BAD_REQUEST);
	 return;	
    }

	Users.Model.findOne({'contact.mobile_no':req.body.mobile_no},function(err,user){
	 err && new Logger(err); 
	 if(user){ res.out(Const.HTTP_CONFLICT);return;	}
	   Users.Model.findOne({_id:req.body._id},function(err,usr){
		 err && new Logger(err);
		 if(!usr || usr==null){ res.out(Const.HTTP_BAD_REQUEST);return;}
		     usr.isvalidated =1;
		     var otp_no = ''+(Math.floor(Math.random(1+Date.now())*Date.now())),
		         otp_no = otp_no.slice(0,6),
			     mno    = ''+req.body.mobile_no.slice(-4,10); 
		     usr.otp = {
			     otp_type :"create_profile",
			     otp_no   :parseInt(otp_no),
			     validity :Date.now()  //  valid for 4 hours
		    };
		    usr.save();
	
		    var msg  = "One Time Password for Cargowala Registration";
		        msg += " on XXX XXX "+mno;
		        msg += " is "+otp_no+" This can be used only once and is valid for next 4 hours from"+ new Date();
	        var pth  = "http://136.243.19.2/http-api.php?username="+Const.smsusername;
	            pth += "&password="+Const.smspassword;
	            pth += "&senderid="+Const.smssenderid;
	            pth += "&route=1&number="+req.body.mobile_no;
	            pth += "&message="+msg;
            var url  = require('url');
	            url  = url.parse(pth);
	        require('http').get({
                  host:url.host,
		          path:url.path		    
	                           },(body)=>{
		//console.log(body)
		                         res.out(Const.HTTP_SUCCESS);
	                }).on('error', (e) => {
                           console.log(`Got error: ${e.message}`);
                });//
        });
    }); 		
});
router.post('/verify-otp',function(req,res){
	if(!(/^[0-9]{6,6}$/.test(req.body.otp))){ res.out(Const.HTTP_BAD_REQUEST); return;}
	Users.Model.findOne({_id:req.body._id,'otp.otp_no':req.body.otp},'_id',function(err,usr){
		err && new Logger(err);
		if(!usr || usr===null){ res.out(Const.HTTP_BAD_REQUEST); return;}
		else{
		  usr.invalidated = 1;
		  usr.otp = {type:'',
					 otp_no:'',
					 validated:''};
		  usr.save(function(err){
			(err && new Logger(err)) || res.out(Const.HTTP_SUCCESS);	  
		  });	
		  
		}
	});
})

/* set authentication token*/
function setAuthentication(req,res,usr){
	req.session.sToken = MD5(Date.now()+usr.contact.email+usr.password+Date.now()); 
}

function getProfileData(user){
  return {
				      _id       : user._id,
				      firstname : user.name.firstname    || "",
				      lastname  : user.name.lastname     || "",
				      email     : user.contact.email,
				      mobile_no : user.contact.mobile_no || "",
				      image     : Common.ImagePath(user,'image',Users)
				      
	};	
}



function register(req,res){
	req.body.password = MD5(Date.now());	
	req.body.status   = 1;
	req.body.contact = {email:req.body.username};
	switch(req.body.social){
		case "facebook":
		addUser(req,res);	
		break;
		case "google":
		addUser(req,res);	
		break;
		default:
		res.out(Const.HTTP_BAD_REQUST);	
		break	
	}
}
function uploadImage(req,res,usr){
	var fs = require('fs');
	    //gm = require('gm').subClass({imageMagick: true});
	if(req.body.image_url && req.body.image_url.length>0){
    var arr    = req.body.image_url.split("."),
	ext        = arr.pop(),
	fn = usr.image || Date.now()+"."+ext;//.split('?').pop();   
	var url = require('url');
	url     = url.parse(req.body.image_url);
	require('http').get({
	   host:url.host,
	   path:url.path,	   
	   },function(_res){ 
		var stream = fs.createWriteStream(Common.media_dir_path(req.body._id,Users)+fn); 
		_res.pipe(stream);
		usr.image = fn;	   
		saveUser(usr,res);	
	   });
	}else if(req.body.image_base64){
	   fn = usr.image || Date.now()+".jpg"; 
	   var base64Data = req.body.image_base64.replace(/^data:image\/jpeg;base64,/, "");
       //var bfr = new Buffer(base64Data,'base64').toString('binary');
		fs.writeFile(Common.media_dir_path(req.body._id,Users)+fn, base64Data,'base64',function(err) {
        (err && new Logger(err))	
		usr.image = fn;
		saveUser(usr,res);	
       });
	}
}
module.exports = router;
worker = router = null; 
