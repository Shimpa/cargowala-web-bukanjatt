var router = require('express').Router();
var Bids   = appModel('Bids'),
	Bids   = new Bids,
	worker = appHelper('Worker'),
	re     = appHelper('RegularExp');


router.post('/place', function(req, res) {
     var bid = Bids.Bid(req.body);
	 uploadImage(bid,'insurance.image',req,res);
	 if(!req.body.insurance)
	    saveBid(res,bid);
	
});
function saveBid(res,bid){
	bid.save(function(err){
	   (err && new Logger(err)) ||
		res.out(Const.HTTP_SUCCESS);   
	});
}

function uploadImage(bid,index,req,res){
	var fs = require('fs');
	if(req.body.insurance){
	   fn = bid.insurance.image || Date.now()+".jpg"; 
	   var base64Data = req.body.insurance.replace(/^data:image\/jpeg;base64,/, "");
       //var bfr = new Buffer(base64Data,'base64').toString('binary');
		fs.writeFile(Common.media_dir_path(req.body._id,Bids)+fn, base64Data,'base64',function(err) {
        (err && new Logger(err))	
		bid[index] = fn;
		saveBid(res,bid);	
       });
	
   }
}


module.exports = router;
