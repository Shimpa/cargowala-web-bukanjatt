var express  = require('express');
var passport = require('passport');
var router   = express.Router();
var User     = appModel('Users');
var Settings = appModel('Settings');
var Settings = new Settings();
var Users    = new User();
var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
	};
/* GET home or login and redirects. */
router.get('/',function(req, res, next) { 
    res.redirect(webappUrl(''));
}).get('/login',function(req,res){
	 res.redirect(webappUrl(''));
});

router.get('/reset-password',function(req,res){
	res.render('shipper/reset-password',{ layout: 'layouts/shipper/loginregister-layout',isverified:null,token:req.params.token||null});
}).post('/reset-password',function(req,res){
	var tk = req.body.Reset.token.split(":"),token=tk[0],htime=tk[1],
		np = req.body.Reset.new_password    ||null,
		cp =req.body.Reset.confirm_password || null;
	if(!np)
		return res.json({errors:{new_password:{message:_locals().new_password}},form:'reset'});
	if(!cp || np!=cp)
		return res.json({errors:{confirm_password:{message:_locals().confirm_password}},form:'reset'});		
	if(np && cp && !appHelper('RegularExp').isString(32,32,token||''))
	  return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
	else{
		Users.Model.findOne({hash_token:tk},'contact.email name',function(err,user){
			if(!user || err)
	          return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
			else{
			worker.execPHP('Mailer.php '+email+" passwordupdated "+user.name,function(err,r){
		        if(!err){
			      user.password = np;
				  user.isvalidated=1;
				  user.save();			      
			    }
              });				
			return res.json({success:Const.HTTP_SUCCESS,display:{html:_locals().success_reset},form:'reset'});
		  }
		})
	}
		
		
}).get('/register',function(req,res){ //get register and redirects
  res.redirect(webappUrl('register'))
});
router.get('/user-type',validateRegister,function(req,res){ // get info of user type agent|Individual|company
	res.render('shipper/user-type',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});
}).post('/user-type',validateRegister,function(req,res){ 
	
	try{	
      if(Validate.isEmpty(req.body.Register))
	     return res.json({'errors':{'usertype':{'message':req.__('register').e_user_type}}});
		if(req.body.Register.usertype && !(req.body.Register.usertype in Users.Roles()))
	     return res.json({'errors':{'usertype':{'message':req.__('register').e_user_type}}});
		 Users.Model.findOne({'_id':req.session.register._id},function(err,user){
			user.isvalidated  =1; 
			user.account_type =req.body.Register.usertype;
			user.role =req.body.Register.usertype;
			if(req.body.Register.usertype==Users.INDIVIDUALSHIPPER) 
			   user.plocation    ='dashboard'; 
			else 
			   user.plocation    ='company'; 
			   user.save({ validateBeforeSave: false },function(err){ 
				if(err) return res.json({'errors':{'usertype':{'message':err.message}}});
				setpLocation(req,user.plocation);
				if(user.role==Users.INDIVIDUALSHIPPER){ 										 if(user.hash_token!="" && user.status==Users.INACTIVE){
				   req.session.setFlash('verify-email',req.__('users').t_verification_email_sent)	
				   return res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
				}else{
				   setpLocation(req,user.plocation);
				   setAuthentication(req,res,user);		
				   return res.json({status:Const.HTTP_SUCCESS,location:shipperUrl(user.plocation,function(url){
                                   if(url=='dashboard') return 'users/dashboard';
                                      return url;
                                   })});
                                 }
				}else{
				   return res.json({status:Const.HTTP_SUCCESS,location:shipperUrl(user.plocation,function(url){
                                   if(url=='dashboard') return 'users/dashboard';
                                      return url;
                                   })});
				}
			}); 
		 });	   
	}catch(e){ console.log(e.message)
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
	
}).get('/company',validateRegister,function(req,res){ // company information form	
	res.render('shipper/company',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});
}).post('/company',validateRegister,function(req,res){
	try{
	Users.Model.findOne({'_id':req.session.register._id},function(err,user){
		user['business_type'] = req.body.Register['business_type'];
		user['company_type']  = req.body.Register['company_type'];
		for(var k in req.body.Register.business)
		    user.business[k]  = req.body.Register.business[k] || " ";
		
		user.plocation        = 'company-address';
		user.isvalidated      = 1;
		user.save(function(err){  console.log(err);
			if(err)
			  return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors});
			else{ 
				setpLocation(req,user.plocation);
				return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
			}
		})
	});
	}catch(e){
		return res.json({status:Const.HTTP_SUCCESS,location:'login'});
	}
}).get('/company-address',validateRegister,function(req,res){
	req.session.user =  {};
	res.render('shipper/company-address',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});	
}).post('/company-address',validateRegister,function(req,res){
	try{
	Users.Model.findOne({_id:req.session.register._id},function(err,user){
		if(!user || err) return res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});
		for(var k in req.body.Register.business){ 
			user.business[k] = req.body.Register.business[k];
		}
		user.plocation        = 'dashboard';
		user.isvalidated      = 1;		
		user.save(function(err){ 
			if(err)
			  return res.json({errors:err.errors});
			else{ 
				delete req.session.register;
				if(user.status==Users.ACTIVE && user.hash_token==""){
				   setpLocation(req,user.plocation);
				   setAuthentication(req,res,user);	
				   return res.json({status:Const.HTTP_SUCCESS,location:shipperUrl(user.plocation,function(url){
                                   if(url=='dashboard') return 'users/dashboard';
                                      return url;
                                   })});	
				}else{
					return res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});
				}
			}
		});
	});
	}catch(e){ console.log(e.message);
		res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});
	}
});

router.post('/logout',function(req,res){
	req.session.destroy();
	return res.redirect(shipperUrl(''));
})
function isUserCompletedProfile(req,res,user,url){
	req.session.user = {};
	if(user.status==Users.INACTIVE && user.hash_token!="" && !ValidateIsShipper(user.role))	
	   res.redirect(shipperUrl(''));
	else if(ValidateIsShipper(user.role) && user.plocation!="dashboard")
	   res.redirect(shipperUrl(user.plocation));
	else if(user.status==Users.ACTIVE && ValidateIsShipper(user.role) && user.plocation=="dashboard")
	   res.redirect(shipperUrl(user.plocation));
	else res.redirect(shipperUrl(''));
	
}
function setAuthentication(req,res,user,s){
	req.session['_user_'] = user;
	//req.session.settings = s;
	setpLocation(req,user.plocation,res);
}
// registration authentication...

function registredUserProfile(req,res,user){
   var url =shipperUrl('');	
   if(user.status==	Users.INACTIVE && user.hash_token!="" && Users.ValidateIsShipper(user.role)){
	  req.session.register = {
	   _id:user._id,
	   token:MD5(user._id+Date.now())
      };
      req.session.register.email = user.contact.email || false;
	  url = user.plocation; 
   }
   setpLocation(req,url,res);   	
}
function setpLocation(req,url,res){
  try{ 
  if(url){
	   if(req.session.register)
	      req.session.register['redirectTo']  =  url;
	   if(req.session.user._id)
	      req.session.user.redirectTo = url;
  }
  }catch(e){
	//res.redirect('/shipper/');  
  }
}
function validateRegister(req,res,next){
	try{
	if(!Users.ValidateIsShipper(req.session.register.role))	
	   throw Error('');	
	req.session.register._id;
	validateAction(req,res,next);
	}catch(e){ //console.log(e.message);
	 res.redirect(webappUrl(''));	
	}
}
function validateAction(req,res,next){
 var url = req.session.register && 'redirectTo' in req.session.register?req.session.register.redirectTo:webappUrl('');
 
 if(url==req.path.replace('/','')) next();
 else if(url!=req.path.replace('/',''))    	 
   return res.redirect(url);
 else if(url=='dashboard') return res.redirect(shipperUrl('users/dashboad'));	
	
}
function verifyEmail(req){
  var email =  req.session.register.email || false;
  if(email){
	  var url = "//www."+email.substring(email.lastIndexOf('@'));	
	return '<p class="text-danger"> You have not verified email id yet.</p><a href="javascript:;" id="resend-verification-email">Resend Verification mail</a> | <a href="'+url+'" target="_blank">Verify Now</a>';  
  }
}

router.post('/resend-email-token',function(req,res,next){ // resend verification email...
	var email = req.body.email || req.session.register.email,worker  = appHelper('Worker');
	if(!email) return res.json({'errors':{'username':{'message':_locals().email_error}}});
	var token = MD5(email+Date.now())+":"+Date.now(); //{$set:{hash_token:token}},
	Users.Model.findOne({'contact.email':email},function(err,user){
	 if(user){
		worker.execPHP('Mailer.php '+email+" confirmemail "+token,function(err,r){
		         console.log(err);	 
		         console.log(r);
		    if(!err){
			   user.isvalidated= 1;	
			   user.hash_token=  token;
	           user.save();
			   var url = "//www."+email.substring(email.lastIndexOf('@'));	
			   var html = '<p class="text-danger"> You have not verified email id yet.</p><a href="'+url+'">Verify Now</a>';	
			   return res.json({success:Const.HTTP_SUCCESS,'display':{'html':html}});
			}else{
			   return res.json({'errors':{'username':{'message':err.message}}});
			}
			//next();
          }); 
	 }else{
		return res.json({'errors':{'username':{'message':_locals().email_error}}});
	 }	
	});
});
function AccessRules(req,res,next){
	if((req.session.user && req.session.user._id))
	   return res.redirect(shipperUrl(req.session.user.plocation||null,function(url){
		   if(url=="dashboard") return 'users/dashboard';
		      return url; 
	   }));
	next();
}
module.exports = router;
