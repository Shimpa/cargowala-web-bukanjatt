"use strict";
var router = require('express').Router(),
    User    = appHelper('User'),
    Notify  = appHelper('Notify'),
    Users   = appModel('Users'),
    WebNotifications   = appModel('WebNotifications'),
	//Notify   = new Notify(),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
Trucks  = appModel('Trucks'),
Trucks  = new Trucks(),
Drivers  = appModel('Drivers'),
Drivers  = new Drivers(),
Shipments   = appModel('Shipments'),
Shipments   = new Shipments(),
WebNotifications   = new WebNotifications();
var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
	};
router.get('/dashboard',AccessRules,csrfProtection,function(req,res){ Appage.title('Dashboard');
									 
  delete req.session.register; // remove registration session. to set user completed profile	
  Shipments.Model.aggregate(
		  {
        $group: {
            _id:null,
		totalshipments:{$sum:{$cond:[{$eq:['$shipper_id',req.session.user.ID]},1,0]}},	
		pendingshipments:{$sum:{$cond:[{$and:[{$eq:['$shipper_id',req.session.user.ID]},{$eq:['$status',Shipments.PENDING]}]},1,0]}},
		intransitshipments:{$sum:{$cond:[{$and:[{$eq:['$shipper_id',req.session.user.ID]},{$eq:['$status',Shipments.INTRANSIT]}]},1,0]}},
		scast:{$sum:{$cond:[{$and:[{$eq:['$trucker_id',req.session.user.ID]},{$eq:['$status',Shipments.COMPLETED]}]},1,0]}}	// total shipments completed as trucker...
		},
		/*$group: {
            _id: '$shipper_id',$cond:[{$eq:['shipper_id',req.session.user.ID]},'$loading.lp_city',null],
			fromcity:{$last:'$loading.lp_city'},tocity:{$last:'$unloading.up_city'},
		// total shipments completed as trucker...
		} */ },function(err,res1){ 
		Shipments.Model.findOne({$query:{shipper_id:req.session.user.ID,status:Shipments.COMPLETED},$orderby: {$natural : -1},$limit:1},'loading.lp_city unloading.up_city',function(err,ship){
		
		
		res.render('agent/users/dashboard',{title:'Dashboard',layout:'layouts/agent/dashboard',extractScripts: true,csrfToken: req.csrfToken(),otpverified:req.session.user.isMobileVerified,otpsent:req.session.user.isOTPSent,stats:res1[0]||{},lastship:ship||{},otpverified:req.session.user.isMobileVerified,otpsent:req.session.user.isOTPSent});
		});
	})
  
}).post('/agents-stats',csrfProtection,AccessRules,function(req,res){
	Trucks.Model.count({created_by:req.session.user._id},function(err,truck_count){
	    Drivers.Model.count({created_by:req.session.user._id},function(err,driver_count){
		res.json({status:Const.HTTP_SUCCESS,tc:truck_count||0,dc:driver_count||0});
	  })														 
    });	
}).post('/asshipper',csrfProtection,AccessRules,function(req,res){
var userdata = {};
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
	    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
		if(user.role == Users.AGENT){
		   userdata = user.toJSON();
		   userdata['parent_role'] =  Users.AGENT;
		   userdata['role'] =  Users.SHIPPERCOMPANY;
		   req.session['_user_']  = userdata;	
		   return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('users/dashboard')})	
		}
	});
}).post('/astrucker',csrfProtection,AccessRules,function(req,res){
	var userdata = {};
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
	    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
		if(user.role == Users.AGENT){
		   userdata = user.toJSON();
		   userdata['parent_role'] =  Users.AGENT;
		   userdata['role'] =  Users.TRUCKERCOMPANY;
		   req.session['_user_']  = userdata;	
		   res.json({status:Const.HTTP_SUCCESS,next:truckerUrl('users/dashboard')})	
		}
	});
}).get('/notification-counter',AccessRules,function(req,res){
	WebNotifications.Model.count({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},function(err,c){
		res.json({count:c||0});
	});
}).post('/load-notifications',AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},'message read',function(err,rec){
		res.json({status:Const.HTTP_SUCCESS,n:rec});
	});
}).post('/webnoti-read',AccessRules,function(req,res){
	WebNotifications.Model.update({_id:req.body.id},{$push:{read:req.session.user.ID}},function(err,affected){
		if(affected)
		   res.json({status:Const.HTTP_SUCCESS});
	});
}).get('/change-password',csrfProtection,AccessRules,function(req,res){
	res.render('shipper/users/change_password',{csrfToken: req.csrfToken()});
}).post('/update-mobile',csrfProtection,AccessRules,function(req,res){
	if(Validate.isNaN(req.body.Users.mobile,10,10))
	   return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__().e_enteralidmobileno});	
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},'otp contact',function(err,user){
	  if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	  if(user){
		 user.contact['mobile_number']  = req.body.Users.mobile;
		 user.save(function(err){
			if(err) 
			return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
			if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_'] = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_successfullyupdated});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		} 
			 
			
			 
		 }) 
	  }	
	});
}).post('/change-password',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID||false},function(err,user){
		if(err || !user) return res.json({status:Const.HTTP_BAD_REQUEST,display:{error:req.__('users').e_invalid_request_recordnot_found}});
		for(var i in req.body.Users)
		    user[i] = req.body.Users[i] || " ";
		user.validate(function(err){
			if(err)return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});		
		    user.password = MD5(req.body.Users.confirm_password);	        	
			user.save({validateBeforeSave:false},function(err){ console.log(err);
								
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_pasword_changed_successfully},form:'users'});
			})		
		}) 		
	});
}).post('/sendotp',csrfProtection,AccessRules,function(req,res){
	if(req.session.user.isOTPVerified)
		return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_otpalready_verified});
    Users.Model.findOne({_id:req.session.user.ID},'contact.mobile_number otp',function(err,user){
		if(err)
		   return res.json({status:Const.HTTP_SUCCESS,msg:err.message});
		if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		}
	});	
}).post('/resendotp',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID},'contact.mobile_number otp',function(err,user){
	  if(user.otp['verified']!=1){	
	   var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
		})
	  }else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
	  }
	});
}).post('/validate-otp',csrfProtection,AccessRules,function(req,res){
	try{	
	Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
		if(user && user.otp.verified!=1){
	        if(user.otp.validity<Date.now()){
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_otp_expired});
			}

		    if(user.otp.otp_no!=req.body.Users.otp)
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_invalidotp});
	        if(user.otp.otp_no==req.body.Users.otp){
				user.otp = {verified:1};
				user.save(function(err){ console.log(err);
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					req.session['_user_']= user; 
				     return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otp_verified});
					
		        })
			}	
				
	}
	});
	}catch(e){
		return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	}
	
	}).post('/remindemelater',csrfProtection,AccessRules,function(req,res){
	//remind you after 5 hours
	res.cookie('_vmltr_',1,{maxAge:1000*60*60*5, path: '/', httpOnly: true })
	   .json({status:Const.HTTP_SUCCESS});
}).get('/notifications',csrfProtection,AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:{$in:[req.session.user.ID]},read:{$nin:[req.session.user.ID]}},'message',function(err,rec){
		console.log(err)
		console.log(rec)
		
		res.render('shipper/users/notifications',{extractScripts:true,rec:rec});
	});
})



//access rules...

function AccessRules(req,res,next){
	try{
	if(req.session.user.isAgent() && req.session.user._id)
	   next();
	else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

module.exports = router;
