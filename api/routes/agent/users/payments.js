"use strict";
var router = require('express').Router(),
    Users   = appModel('Users'),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
Payments  = appModel('Payments'),
Payments  = new Payments(),
Shipments   = appModel('Shipments'),
Shipments   = new Shipments();
var poffset;
router.get('/',AccessRules,csrfProtection,function(req,res){
	try{ poffset=0;
   Shipments.Model;	Payments.Model.find({payer_id:req.session.user.ID||false}).populate('shipment_id','loading.lp_city unloading.up_city truck transit.t_time transit.t_date shipment_detail.truck_name').skip(poffset).limit(Payments.LIMIT).exec(function(err,rec){
	if(!err) poffset+=Payments.LIMIT;   
	res.render('shipper/payments/list',{extractScripts:true,payments:rec,csrfToken:req.csrfToken()});
	})
	}catch(e){
		res.redirect(shipperUrl('users/dashboard'));
	}
}).post('/',csrfProtection,AccessRules,function(req,res){
	try{ 
   Shipments.Model;	Payments.Model.find({payer_id:req.session.user.ID||false}).populate('shipment_id','loading.lp_city unloading.up_city truck transit.t_time transit.t_date shipment_detail.truck_name').skip(poffset).limit(Payments.LIMIT).exec(function(err,rec){
	if(!err) poffset+=Payments.LIMIT;   
	res.render('shipper/payments/paginate',{layout:false,extractScripts:true,payments:rec});
	})
	}catch(e){
		res.redirect(shipperUrl('users/dashboard'));
	}
}).get('/final-pay/:id',csrfProtection,AccessRules,function(req,res){
	Shipments.Model;	
	Payments.Model.findOne({payer_id:req.session.user.ID||false,_id:req.params.id||false},function(err,payment){ console.log(payment);
		if(err || !payment) return res.render('shipper/payments/final_payment',{extractScripts:true,err:err.message||req.__('shipments').t_invalid_request});
		if(payment.status==Payments.FULLPAYMENTDONE){ 
			return res.render('shipper/payments/final_payment',{extractScripts:true,err:req.__('shipments').t_already_paid});
		}else if(payment.transactions[0].status==Payments.PENDING){
			req.session.payment = {
				_id:payment._id,
				instalment:1
			};
			return res.render('shipper/payments/final_payment',{extractScripts:true,err:req.__('shipments').t_not_paid1st_instalment,transactions:payment.transactions[0]});
		}else if(payment.transactions[1].status==Payments.PENDING){
			req.session.payment = {
				_id:payment._id,
				instalment:2
			};
		    return res.render('shipper/payments/final_payment',{extractScripts:true,transactions:payment.transactions[1]});	
		}
	});
});

//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isShipper() && req.session.user._id)
	   next();
	else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

module.exports = router;
