"use strict";
var router = require('express').Router(),
    csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
TruckLanes  = appModel('TruckLanes'),
TruckTypes  = appModel('TruckTypes'),
TruckTypes  = new TruckTypes(),
TruckLanes  = new TruckLanes(),
Shipments   = appModel('Shipments'),
Trucks      = appModel('Trucks'),
Bids        = appModel('Bids'),
Users       = appModel('Users'),
Users       = new Users(),
Bids        = new Bids(),
Shipments   = new Shipments(),
Trucks  = new Trucks();
var UploadImage    = appHelper('Image'),
	images   = {
		insurance:{
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
		},
		rc:{
		  'extensions':['jpg','jpeg','png','bmp'],
		  'maxsize':1.5	
		},
		permit:{
		  'extensions':['jpg','jpeg','png','bmp'],
		  'maxsize':1.5	
		}
	};
var offset,params={},assign2shipment,vid;
router.get('/',csrfProtection,AccessRules,function(req,res){ Appage.title('Vehicles List');
	try{														
	offset=0;var cond = {owner_id:req.session.user.ID||false},qp=null;						
	var prm = {};		
	params = req.query;
	if(params.q) prm = params;	// on page call														
	if(params.p && params.p['q']!='') prm = params.p; //on popup call
	if(prm.q && prm.q!=''){														
	   cond= {owner_id:req.session.user.ID||false,$or:[{'registration.vehicle_number':new RegExp('^'+prm.q,'i')},{'truck_type.name':new RegExp('^'+prm.q,'i')}]}; 
	   qp = prm.q;	
	}
	console.log(cond);														
	TruckTypes.Model;Trucks.Model.find(cond,
	'truck_type truck_category is_available truck_status registration.vehicle_number')
	.populate('truck_type').sort({'modified_on':1}).skip(offset).limit(Trucks.LIMIT)
	.exec(function(err,doc){ 
	           if(err || doc.length<=0) doc = [];
	              offset+=Trucks.LIMIT;													
	              res.render('trucker/vehicles/list',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc,offset:offset,qp:qp,avail:false,Trucks:Trucks});		
	});
	}catch(e){
		var doc = [],
			prm = {};
		params = req.query;
		if (params.q) prm = params, qp = null; // on page call														
		if (params.p && params.p['q'] != '') prm = params.p; //on popup call
		if (prm.q && prm.q != '') {
			qp = prm.q;
		}
		offset += Trucks.LIMIT;
		res.render('trucker/vehicles/list', {
			extractScripts: true,
			csrfToken: req.csrfToken(),
			doc: doc,
			qp: qp,
			offset: offset,
			avail: false,
			Trucks: Trucks
		});
	}
}).get('/assign-vehicle/:id?/:did?',csrfProtection,AccessRules,function(req,res){ Appage.title('Assign Vehicle');
	offset=0;var cond = {owner_id:req.session.user.ID||false},qp=null;										
	params = req.query,assign2shipment=null,vid=null;
	if(params.q && params.q!=''){														
	   cond= {owner_id:req.session.user.ID||false,$or:[{'registration.vehicle_number':new RegExp('^'+params.q,'i')},{'truck_type.name':new RegExp('^'+params.q,'i')}]}; 
	   qp = params.q;	
	}
	Bids.Model.findOne({shipment_id:req.params.id,trucker_id:req.session.user.ID,status:Bids.ACCEPTED},'shipment_id',function(err,data){
	   if(!err && data){ 
			assign2shipment =data.shipment_id;
			vid = req.params.did; 
		}else if(!data){
			assign2shipment =req.params.id;
			vid = req.params.did; 
		}
	})																	   
	TruckTypes.Model;Trucks.Model.find(cond,
	'truck_type truck_category status is_available truck_status registration.vehicle_number')
	.populate('truck_type').sort({'modified_on':1}).skip(offset).limit(Trucks.LIMIT)
	.exec(function(err,doc){ 
	           if(err || doc.length<=0) doc = undefined;
	              offset+=Trucks.LIMIT;													
	              res.render('trucker/vehicles/popuplist',{layout:'layouts/trucker/popuplayout',extractScripts: true,csrfToken: req.csrfToken(),doc:doc,offset:offset,qp:qp,Trucks:Trucks});		
	});													
}).post('/assign',csrfProtection,AccessRules,function(req,res){ 
     try{
		if(!req.body._ajx_) throw new Error(req.__().unknown_request);
		if(!req.body._id_) throw new Error(req.__().mismatch_params);
		 console.log(assign2shipment)
		 console.log(vid)
		Shipments.Model.findOne({_id:assign2shipment,'vehicle._id':vid},'vehicle',function(err,doc){
			var oldvehicle; console.log(doc)
			if(!err && doc.vehicle && doc.vehicle.length>0){  
			   var vexists = false; // vehicle exsits or not set default false 	
			   doc.vehicle.forEach(function(v){
				   if(v._id==vid){
					  if(v.id)oldvehicle = v.id;
					     v.id = req.body._id_;
				   }
			   });
			    doc.save(function(err){  console.log(err) });	
			   }else return res.json({status:Const.HTTP_BAD_REQUEST,error:[req.__().unknown_request]});
			   if(oldvehicle){
				 Trucks.Model.update({_id:oldvehicle,owner_id:req.session.user.ID},{$set:{truck_status:Trucks.NOTASSIGNED}},function(err,affected){})
				 Trucks.notifyShipperTrucker(req,Shipments,assign2shipment,req.body._id_,oldvehicle); 
			   }else Trucks.notifyShipperTrucker(req,Shipments,assign2shipment,req.body._id_); 
			   Trucks.Model.update({_id:req.body._id_,owner_id:req.session.user.ID},{$set:{truck_status:Trucks.ASSIGNED}},function(err,affected){ console.log(err)  
			      if(err) return res.json({status:Const.HTTP_BAD_REQUEST,error:err.errors});
		             return res.json({status:Const.HTTP_SUCCESS,txt:req.__().t_assigned})  
		    })	     
	      }) 
	 }catch(e){
	   return res.json({status:Const.HTTP_BAD_REQUEST,error:[e.message]})	 
	 }
}).post('/list',csrfProtection,AccessRules,function(req,res){
	var cond = {owner_id:req.session.user.ID};
	if(req.body.p && req.body.p['q']!=''){														
	   cond= {owner_id:req.session.user.ID||false,$and:[{'name.firstname':new RegExp('^'+req.body.p['q'],'i')},{'name.lastname':new RegExp('^'+req.body.p['q'],'i')}]}; 
	}
	TruckTypes.Model;Trucks.Model.find(cond,
	'truck_type truck_category is_available status truck_status registration.vehicle_number')
	.populate('truck_type').sort({'modified_on':1}).skip(offset).limit(Trucks.LIMIT)
	.exec(function(err,doc){
	      if(err) doc = {};
		    offset +=Trucks.LIMIT;
		   
	        res.render('trucker/vehicles/paginate',{layout:false,doc:doc,offset:offset,avail:req.body.p['avail']||false,Trucks:Trucks});		
    });													
}).get('/add-vehicle',csrfProtection,AccessRules,function(req,res){ Appage.title('Add Vehicle');
	Trucks.Model.count({owner_id:req.session.user.ID},function(err,count){					
	res.render('trucker/vehicles/add-vehicle',{extractScripts: true,csrfToken: req.csrfToken(),image:images,doc:{},truck_count:count||0,individualTrucker:req.session.user.isIndividualTrucker}); 
	});
}).get('/update-vehicle/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Update Vehicle');		      Trucks.Model.findOne({owner_id:req.session.user.ID||false,_id:req.params.id},function(err,doc){
	if(err) return res.redirect(truckerUrl('vehicles'));	 
	  res.render('trucker/vehicles/add-vehicle',{extractScripts: true,csrfToken: req.csrfToken(),image:images,doc:doc,Trucks:Trucks,truck_count:0});
   });
}).post('/add-vehicle',csrfProtection,AccessRules,function(req,res){
	Trucks.Model.count({owner_id:req.session.user.ID},function(err,count){ var count =  count ||0;
	 if(count>1 && req.session.user.isIndividualTrucker && !req.body.Vehicle.id) return res.json({status:Const.HTTP_BAD_REQUEST,errors:true,display:{error:req.__().t_individual_trucker},form:'vehicle'});
																	  
	var insurance_image = req.body.Vehicle?req.body.Vehicle.insurance['image']:null,
		rc_image        = req.body.Vehicle?req.body.Vehicle.registration['rc_pic']:null,
		permit_image    = req.body.Vehicle?req.body.Vehicle.permit['pic']:null;
	delete req.body.Vehicle.insurance['image'];
	delete req.body.Vehicle.registration['rc_pic'];
	delete req.body.Vehicle.permit['pic'];
	var body = req.body.Vehicle,insuranceimage,rcimage,permitimage;
	
    Trucks.Model.findOne({'_id':req.body.Vehicle.id||false,owner_id:req.session.user.ID},function(err,Truck){
	   if(!req.body.Vehicle.id)
	      var Truck = Trucks.Insert(body);
		if(!err && req.body.Vehicle.id){
			insuranceimage = Truck.insurance['image']; 
			rcimage        = Truck.registration['rc_pic']; 
			permitimage    = Truck.permit['pic']; 
		}
	    body.insurance['image'] = insuranceimage || Date.now();
	    body.permit['pic'] = rcimage || Date.now();
	    body.registration['rc_pic'] = permitimage || Date.now();
		body.permit['permit_expiry_date'] = Common.makeISODate(body.permit['permit_expiry_date']);
	    body.registration['expiry_date']  = Common.makeISODate(body.registration['expiry_date']);
	    body.insurance['renew_date']      = Common.makeISODate(body.insurance['renew_date']);
		body['status']       = Truck.status || Trucks.ACTIVE;
	    body['truck_status'] = Truck.truck_status || Trucks.NOTASSIGNED;
		body['is_available'] = Trucks.AVAILABLE;
		if(req.body.Vehicle.id)
	       body['is_available'] = body['is_available']=='on'?Trucks.AVAILABLE:Trucks.NOTAVAILABLE;
	    body['owner_id']     = req.session.user.ID;
	    body['created_by']   = body['created_by'] || req.session.user.ID;
		//if(req.body.Vehicle.id){
		   for(var i in body)	
			  Truck[i]  = body[i]; 
		//}
		Truck.save(function(err){
		if(err) return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'vehicle'});
		  if(insurance_image){
		      UploadImage.allowedExtensions = images.insurance.extensions;
			  UploadImage.allowedSize       = images.insurance.maxsize;
			  UploadImage.setFilename(body.insurance['image']);
		      var fn =UploadImage.base64(insurance_image).upload(Common.media_dir_path(Truck._id,Trucks));	
		    Truck.insurance['image'] = fn;
		  }
		  if(permit_image){	
		    // permit image...
		      UploadImage.allowedExtensions = images.permit.extensions;
			  UploadImage.allowedSize       = images.permit.maxsize;
			  UploadImage.setFilename(body.permit['pic']);
		      var fn =UploadImage.base64(permit_image).upload(Common.media_dir_path(Truck._id,Trucks));	
			  Truck.permit['pic'] = fn;
		  }
		  if(rc_image){	
		    //RC image...
		      UploadImage.allowedExtensions = images.permit.extensions;
			  UploadImage.allowedSize       = images.permit.maxsize;
			  UploadImage.setFilename(body.registration['rc_pic']);
		      var fn =UploadImage.base64(rc_image).upload(Common.media_dir_path(Truck._id,Trucks));	
			  Truck.registration['rc_pic'] = fn;
		  }
		  Truck.save();
		  insurance_image = rc_image = permit_image  =null;
		  return res.json({status:Const.HTTP_SUCCESS,next:truckerUrl('vehicles')});
	    });
	});
   })
}).get('/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Vehicles List');
    if(!req.params)return													   
	TruckTypes.Model;	Trucks.Model.findOne({owner_id:req.session.user.ID||false,_id:req.params.id}).populate('truck_type').exec(function(err,doc){
	 if(err) doc = {};
	    res.render('trucker/vehicles/detail',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc,Trucks:Trucks});		
	});													
}).post('/change-availbility',csrfProtection,AccessRules,function(req,res){ 
	
   Trucks.Model.update({_id:req.body._id,owner_id:req.session.user.ID},{$set:{is_available:req.body.v=='true'?1:0,modified_on:Date.now()}},function(err,afftected){
	 res.end(Const.SUCCESS);  
   })


});





//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isTrucker() && req.session.user._id){
	 if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}else res.redirect(webappUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

module.exports = router;
