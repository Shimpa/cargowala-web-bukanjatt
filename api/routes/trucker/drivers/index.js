"use strict";
var router = require('express').Router(),
    csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
TruckLanes  = appModel('TruckLanes'),
TruckTypes  = appModel('TruckTypes'),
TruckTypes  = new TruckTypes(),
TruckLanes  = new TruckLanes(),
Trucks      = appModel('Trucks'),
Worker      = appHelper('Worker'),
Shipments   = appModel('Shipments'),
Drivers     = appModel('Drivers'),	
Bids        = appModel('Bids'),	
Shipments   = new Shipments(),
Drivers     = new Drivers();
Bids        = new Bids();
Trucks      = new Trucks();
var files   = [];
var UploadImage    = appHelper('Image'),
	images   = {
		profile:{
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
		},
		adhaar:{
		  'extensions':['jpg','jpeg','png','bmp'],
		  'maxsize':1.5	 
		},
		licence:{
		  'extensions':['jpg','jpeg','png','bmp'],
		  'maxsize':1.5	
		}
	};
var limit = 3,offset,params={},assign2shipment,vid;
router.get('/',csrfProtection,AccessRules,function(req,res){ Appage.title('Drivers List');
	try{														
    offset=0;var cond = {owner_id:req.session.user.ID||false},qp=null;						
	var prm = {};		
	params = req.query;
	if(params.q) prm = params;	// on page call														
	if(params.p && params.p['q']!='') prm = params.p; //on popup call
	if(prm.q && prm.q!=''){															
	   cond= {owner_id:req.session.user.ID||false,$or:[{'name.firstname':new RegExp('^'+prm.q,'i')},{'name.lastname':new RegExp('^'+prm.q,'i')}]}; 
	   qp = prm.q;	
	}														
	Drivers.Model.find(cond,
	'image name truck_type status driver_status is_available')
	.sort({'modified_on':1}).skip(offset).limit(Drivers.LIMIT)
	.exec(function(err,doc){
	       if(err) doc = {};
		   offset +=Drivers.LIMIT;
	          res.render('trucker/drivers/list',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc,Drivers:Drivers,offset:offset,qp:qp,avail:0});	
	});	
	}catch(e){
	  var prm = {};		
	params = req.query;
	if(params.q) prm = params;	// on page call														
	if(params.p && params.p['q']!='') prm = params.p; //on popup call
	if(prm.q && prm.q!=''){	
		  qp = prm.q;	
	}
		var doc = [];
		   offset +=Drivers.LIMIT;
	          res.render('trucker/drivers/list',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc,Drivers:Drivers,offset:offset,qp:qp,avail:0});	
	}
}).get('/assign-driver/:id?/:vid?',csrfProtection,AccessRules,function(req,res){ Appage.title('Manage Drivers'); ///manage drivers assign drivers popup content
    offset=0;var cond = {owner_id:req.session.user.ID||false},qp=null;assign2shipment=null,vid=null;										
	params = req.query;
	if(params.q && params.q!=''){														
	   cond= {owner_id:req.session.user.ID||false,$or:[{'name.firstname':new RegExp('^'+params.q,'i')},{'name.lastname':new RegExp('^'+params.q,'i')}]}; 
	   qp = params.q;	
	}
   Bids.Model.findOne({shipment_id:req.params.id,trucker_id:req.session.user.ID,status:Bids.ACCEPTED},'shipment_id',function(err,data){
	   if(!err && data){ 
			assign2shipment =data.shipment_id;
			vid = req.params.vid; 
		}else{
			assign2shipment =req.params.id;
			vid = req.params.vid; 
		}
	})																	 
	Drivers.Model.find(cond,
	'image name truck_type driver_status is_available status')
	.sort({'modified_on':1}).skip(offset).limit(Drivers.LIMIT)
	.exec(function(err,doc){
	       if(err) doc = {};
		   offset +=Drivers.LIMIT;
	          res.render('trucker/drivers/popuplist',{layout:'layouts/trucker/popuplayout',extractScripts: true,csrfToken: req.csrfToken(),doc:doc,Drivers:Drivers,offset:offset,qp:qp,avail:1});	
	});													
}).post('/assign',csrfProtection,AccessRules,function(req,res){ 
     try{
		if(!req.body._ajx_) throw new Error(req.__().unknown_request);
		if(!req.body._id_) throw new Error(req.__().mismatch_params);
		Shipments.Model.findOne({_id:assign2shipment,'vehicle._id':vid},'vehicle',function(err,doc){
			var olddriver;
			if(!err && doc.vehicle && doc.vehicle.length){  
			   var vexists = false; // vehicle exsits or not set default false 	
			   doc.vehicle.forEach(function(vehicle){
				   if(vehicle._id==vid){
					  if(vehicle.driver)olddriver =vehicle.driver; 
					  vehicle.driver = req.body._id_;
					
				   }
			   });
			   doc.save();
			   }else return res.json({status:Const.HTTP_BAD_REQUEST,error:[req.__().unknown_request]});
			   if(olddriver){
				  Drivers.Model.update({_id:olddriver,owner_id:req.session.user.ID},{$set:{driver_status:Drivers.NOTASSIGNED}},function(err,affected){});
				  Drivers.notifyDriverShipperTrucker(req,Shipments,assign2shipment,
													 req.body._id_,olddriver); 
			   }else Drivers.notifyDriverShipperTrucker(req,Shipments,assign2shipment,req.body._id_);
			   Drivers.Model.update({_id:req.body._id_,owner_id:req.session.user.ID},{$set:{driver_status:Drivers.ASSIGNED}},function(err,affected){  
			      if(err) return res.json({status:Const.HTTP_BAD_REQUEST,error:err.errors});
		             return res.json({status:Const.HTTP_SUCCESS,txt:req.__().t_assigned})  
		       })	     
	      }) 
	 }catch(e){
	   return res.json({status:Const.HTTP_BAD_REQUEST,error:[e.message]})	 
	 }
}).post('/list',csrfProtection,AccessRules,function(req,res){
	var cond = {owner_id:req.session.user.ID},qp=null;
	if(req.body.p && req.body.p['q']!=''){														
	   cond= {owner_id:req.session.user.ID||false,$or:[{'name.firstname':new RegExp('^'+req.body.p['q'],'i')},{'name.lastname':new RegExp('^'+req.body.p['q'],'i')}]}; 
	   qp = req.body.q;	
	}
		Drivers.Model.find(cond,
	'image name truck_type driver_status status is_available')
	.sort({'modified_on':1}).skip(offset).limit(Drivers.LIMIT)
	.exec(function(err,doc){ console.log("addsasdsadasdasd"+offset);
	      if(err) doc = {};
		    offset +=Drivers.LIMIT;
		  
	        return res.render('trucker/drivers/paginate',{layout:false,doc:doc,offset:offset,Drivers,Drivers,avail:req.body.p['avail']||false});		
    });													
}).get('/add-driver',csrfProtection,AccessRules,function(req,res){ Appage.title('Add Driver'); global.UploadedFiles =[];
	res.render('trucker/drivers/add-driver',{extractScripts: true,csrfToken: req.csrfToken(),image:images,doc:{}});  
}).get('/update-driver/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Update Driver');		      Drivers.Model.findOne({owner_id:req.session.user.ID||false,_id:req.params.id},function(err,doc){
	if(err) return res.redirect(truckerUrl('drivers'));	 
	  res.render('trucker/drivers/add-driver',{extractScripts: true,csrfToken: req.csrfToken(),image:images,doc:doc,Drivers:Drivers});
   });
}).post('/au-drivers',csrfProtection,AccessRules,function(req,res){ 
	var body = req.body.Drivers;
	   
	Drivers.Model.findOne({_id:req.body._id,owner_id:req.session.user.ID},function(err,Driver){
		
		 global.UploadedFiles.forEach(function(ele){
			if(ele.hasOwnProperty('drivers-image'))
	           body['image'] = ele.filename;
			if(ele.hasOwnProperty('drivers-licence-image'))
	           body['licence']['image'] = ele.filename;
			if(ele.hasOwnProperty('drivers-address-adhaar_image'))
	           body['address']['adhaar_image'] = ele.filename;
			
		});
            if(!Driver)   		
	           var Driver = Drivers.Insert(body);
	    Driver.created_by   = Driver.created_by || req.session.user.ID;
	    Driver.owner_id     = req.session.user.ID;
		if(Driver.isNew){
			Driver.created_on   = Date.now();
		}else{
			var oldpassword = Driver.password,
				old_mobileno =Driver.mobile_no;
			for(var i in body)				
				Driver[i] = body[i];
			if(Validate.isEmpty(body.password)) // if password not updated
				Driver.password = oldpassword;
			else Driver.updatePassword =1;  // in case password updated
			if(old_mobileno!=body.mobile_no)
			   Driver.mobilenoChanged =1; // if mobile number updated	
			if(body['is_available'])
			   Driver.is_available = body['is_available']=='on'?Drivers.AVAILABLE:Drivers.NOTAVAILABLE;
		    	
		}
	    Driver.save(function(err){
			if(err){
			if(err.errors){		
			  return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'drivers'});
			}else
		      return res.json({status:Const.HTTP_BAD_REQUEST,'errors':{'mobile_no':{'message':err.message}},form:'drivers'});
			}
			   var inst = {};
	               inst['_id']  =Driver._id;
	               inst['parent_dir'] = Drivers.parent_dir; 
	               inst['child_dir']  = Drivers.child_dir; 
	               global.UploadedFiles.push(inst);
			   var image = Worker.fork('MoveImage');
			       image.send(global.UploadedFiles);
	               image.on('message',function(dta){ 
			       if(dta==null)
			          image.kill();	
		           });
			   return res.json({status:Const.HTTP_SUCCESS,next:truckerUrl('drivers')});
		});
	});
	
}).get('/view/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Driver Detail');
    if(!req.params)return;
	  Drivers.Model.findOne({_id:req.params.id,owner_id:req.session.user.ID},function(err,doc){
	    if(err) doc = null;
	       res.render('trucker/drivers/detail',{extractScripts: true,csrfToken: req.csrfToken(),driver:doc,Drivers:Drivers});	
	  
	});													
}).post('/intransit',csrfProtection,AccessRules,function(req,res){ Appage.title('Driver Detail')
    Trucks.Model;Drivers.Model;
    Shipments.Model.findOne({_id:req.body._id_,trucker_id:req.session.user.ID},'vehicle loading.lp_address transit.t_date transit.t_time shipper_id').populate('vehicle.driver','name mobile_no location  updated_on image').populate('vehicle.id','registration.vehicle_number').populate('shipper_id','name contact rating').exec(function(err,doc){
		if(doc && !err){
			var array = [];
			if(doc.vehicle.length>0){
			   doc.vehicle.forEach(function(vehicle){ 
				   array.push({
					   vehicle_no :vehicle.id.registration.vehicle_number,
					   driver_name:vehicle.driver.name.firstname+" "+vehicle.driver.name.lastname,
					   mobile_no  :vehicle.driver.mobile_no,
					   driver_image:Common.MediaPath(vehicle.driver,'image',Drivers),
					   track      :     {started_from:doc.loading.lp_address,start_date:doc.transit.t_date,start_time:doc.transit.t_time,reached:vehicle.driver.location.place||req.__('shipments').t_driver_no_started_trip,
					   date:vehicle.driver.location.place?(new Date(vehicle.driver.updated_on||0)).toDateString():req.__('shipments').t_driver_no_started_trip,time:vehicle.driver.location.place?(new Date(vehicle.driver.updated_on||0)).toTimeString():req.__('shipments').t_driver_no_started_trip
				       },					  
				   });
			   })
			   return res.json({status:Const.HTTP_SUCCESS,d:array, user:{
						   name:doc.shipper_id.name.firstname+" "+doc.shipper_id.name.lastname,
						   contact_no:doc.shipper_id.contact.mobile_number,
						   starts:doc.shipper_id.rating||0,
				           id:doc.shipper_id._id,
					   }});
			}else return res.json({status:Const.HTTP_NO_CONTENT});
		}else return res.json({status:Const.HTTP_NO_CONTENT});
	})  
}).post('/change-availbility',csrfProtection,AccessRules,function(req,res){ 
	
   Drivers.Model.update({_id:req.body._id,owner_id:req.session.user.ID},{$set:{is_available:req.body.v=='true'?1:0,modified_on:Date.now()}},function(err,afftected){
	  console.log(err); 
	 res.end();  
   })


});






//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isTrucker() && req.session.user._id){
	  if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}else res.redirect(webappUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
function makeISODate(date){ if(!date)return null; 
  var dt = date.split("-");
      dt = new DateTime(dt[1]+"/"+dt[2]+"/"+dt[0]+" 24:00:00").toISOString();
	 console.log(dt); 
	return dt;					   
}
module.exports = router;
