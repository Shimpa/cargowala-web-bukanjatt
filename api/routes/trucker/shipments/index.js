	var express = require('express');
	var router = express.Router();
	var User = appModel('Users');
	var Shipments = appModel('Shipments');
	var Categories = appModel('Categories');
	var Bids = appModel('Bids');
	var Drivers = appModel('Drivers');
	var Trucks = appModel('Trucks');
	var Drivers = new Drivers();
	var Trucks = new Trucks();
	var Shipments = new Shipments();
	var Categories = new Categories();
	var Users = new User();
	var Bids = new Bids();
	var csrf = require('csurf'),
		https= require('https');
	var csrfProtection = csrf({
		cookie: true
	});
	var UploadImage = appHelper('Image'),
		images = {
			'extensions': ['jpg', 'jpeg', 'png', 'bmp'],
			'maxsize': 1.5 // size in MB
		};
	/* GET home page. */
	var blboffset;
	router.get('/bid-load-board', csrfProtection, AccessRules, function (req, res) {
		Appage.title('Bid Board');
		try {
			blboffset = 0;
			var cond = {};
			    cond['status'] =Shipments.PENDING;
			    cond['shipment_type.code'] =Shipments.BID_BOARD;
			    cond['transit.t_expdatetime'] ={$gte: DateTime.now()};
			
			if(req.query && req.query.Filters){
				var filters  = req.query.Filters,
					fromdate = filters.date_from.split('/'),
					todate   = filters.date_to.split('/');
			        fromdate = new Date(fromdate[2],fromdate[1]-1,fromdate[0]);
				    todate   = new Date(todate[2],todate[1]-1,todate[0]);
				    if(filters.area)
				     cond['loading.lp_state']   = new RegExp(filters.area,'i') || undefined;	
				    if(Array.isArray(filters.area) && filters.area.length>0)
				       cond['loading.lp_state'] ={$in:filters.area};	
					   console.log(fromdate);	
					   console.log(todate);	
				     if(!isNaN(todate.getTime()))			    
				       cond['transit.date'] = {$lte:todate.getTime()};
				    if(!isNaN(fromdate.getTime()))
					   cond['transit.date'] = {$gte:fromdate.getTime()};
			}
			Shipments.Model.find(cond, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id').populate('truck.ttype', 'name loading_capacity').skip(blboffset).limit(Shipments.LIMIT_BID_BOARD)
				.sort({
					'transit.t_expdatetime': 1
				}).exec(function (err, doc) {
			
					blboffset += Shipments.LIMIT_BID_BOARD;
					res.render('trucker/shipments/bid-board', {
						extractScripts: true,
						csrfToken: req.csrfToken(),
						doc: doc,
						filters:req.query.Filters||{},
						
					});
				});
		} catch (e) { //console.log(e.message);
			res.redirect(webappUrl(''));
		}

	}).post('/bid-load-board', csrfProtection, AccessRules, function (req, res) {
		Appage.title('Bid Board');
		try {
			var cond = {};
			    cond['status'] =Shipments.PENDING;
			    cond['shipment_type.code'] =Shipments.BID_BOARD;
			    cond['transit.t_expdatetime'] ={$gte: DateTime.now()};
			if(req.body && req.body.Filters){
				var filters  = req.body.Filters,
					fromdate = filters.date_from.split('/'),
					todate   = filters.date_to.split('/');
			        fromdate = new Date(fromdate[2],fromdate[1]-1,fromdate[0]);
				    todate   = new Date(todate[2],todate[1]-1,todate[0]);
				     if(filters.area)
				     cond['loading.lp_state']   = new RegExp(filters.area,'i') || undefined;	
				    if(Array.isArray(filters.area) && filters.area.length>0)
				       cond['loading.lp_state'] ={$in:filters.area};	
				     if(!isNaN(todate.getTime()))			    
				       cond['transit.date'] = {$lte:todate.getTime()};
				    if(!isNaN(fromdate.getTime()))
					   cond['transit.date'] = {$gte:fromdate.getTime()};
			}
			Shipments.Model.find(cond, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id').populate('truck.ttype', 'name loading_capacity').skip(blboffset).limit(Shipments.LIMIT_BID_BOARD)
				.sort({
					'transit.t_expdatetime': 1
				}).exec(function (err, doc) {
					blboffset += Shipments.LIMIT_BID_BOARD;
					res.render('trucker/shipments/paginate_loadboard_bids', {
						layout: false,
						csrfToken: req.csrfToken(),
						doc: doc
					});
				});
		} catch (e) {
			res.redirect(webappUrl(''));
		}

	}).get('/detail/:id', csrfProtection, AccessRules, function (req, res) {
		Appage.title('Shipment detail');
		Categories.Model;
		Shipments.Model.findOne({
			_id: req.params.id,
			status: Shipments.PENDING,
			'shipment_type.code': Shipments.BID_BOARD
		}).populate('items.parent_category').populate('items.sub_category').populate('shipper_id').exec(function (err, doc) {
			if (err || !doc)
				res.render('trucker/shipments/detail', {
					extractScripts: true,
					csrfToken: req.csrfToken(),
					doc: doc
				});
			req.session.loadedShipment = {
				_id: doc._id,
				fcity: doc.loading.lp_city,
				tcity: doc.unloading.up_city,
				dtime: doc.transit.t_date + " " + doc.transit.t_time,
				dtime: doc.transit.t_date + " " + doc.transit.t_time,
				reference_no: doc.shipment_id,
			};
			if(doc && doc.shipper_id){
			req.session.loadedShipper = {
				_id:doc.shipper_id._id,
				name: doc.shipper_id.name.firstname+" "+doc.shipper_id.name.lastname,
				device_token: Common.deviceToken(doc.shipper_id.device ||null),
				mobile_no: doc.shipper_id.contact.mobile_number,
				email: doc.shipper_id.contact.email,
			}
			}

			res.render('trucker/shipments/detail', {
				extractScripts: true,
				csrfToken: req.csrfToken(),
				doc: doc,
				signature:Common.generateSignature(1,3),
				transactions:req.session.user.pendingTransaction,
			});
		});
	}).post('/place-bid', csrfProtection, AccessRules, function (req, res) {
		try {
		Users.Model.findOne({_id:req.session.user},function(err,user){
			if (err) return res.json({
								status: Const.HTTP_BAD_REQUEST,
								errors: err.message,
								form: 'bids'
							});
			if(req.session.user.pendingTransaction<=0){
				return res.json({
								status: Const.HTTP_BAD_REQUEST,
								errors: req.__('users').e_no_more_transactions,
								form: 'bids'
							});
			}
			var template = {};
			template[Shipments.namespace.shippername] = req.session.loadedShipper.name;
			template[Shipments.namespace.loadingcity] = req.session.loadedShipment.fcity;
			template[Shipments.namespace.unloadingcity] = req.session.loadedShipment.tcity;
			template[Shipments.namespace.datetime] = req.session.loadedShipment.dtime;
			template[Shipments.namespace.bidder] = req.session.user.getName();
			if (!Validate.isEmpty(req.body.Bids._id)) {
				Bids.Model.findOne({
					trucker_id: Common.getLoggedUserInfo(req, '_id'),
					_id: req.body.Bids._id
				}, function (err, doc) {
					if (!err && doc) {
						delete req.body.Bids._id;
						var old_amount = doc.price;
						var old_bidstatus = doc.status;
						for (var i in req.body.Bids)
							doc[i] = req.body.Bids[i];
						    doc.status = Bids.POSTED;
						doc.save(function (err) {
							if (err) return res.json({
								status: Const.HTTP_BAD_REQUEST,
								errors: err.errors,
								form: 'bids'
							});
						if(user.membership.transactions && old_bidstatus==Bids.REJECTED)
						   user.membership.transactions-=1;
						user.save();	
						req.session['_user_']= user.toJSON();	
							var reflink = '<a href="'+truckerUrl('shipments/detail/'+doc.shipment_id)+'">'+req.session.loadedShipment.reference_no+'</a>';
							var msg = Common.replaceValue(req.__('shipments').t_updatedbidonshipment,['<<shipper_name>>','<<trucker_name>>','<<old_amount>>','<<new_amount>>'],[req.session.loadedShipper.name,req.session.user.getName(),old_amount,doc.price]);
							Bids.bidUpdated(req.session.loadedShipper,msg,req.session.loadedShipper._id,reflink,req.session.loadedShipment.reference_no,true);
							delete req.session.loadedShipment;
							delete req.session.loadedShiper;
							return res.json({
								status: Const.HTTP_SUCCESS,
								display: {
									success: req.__().t_bid_updated_successfully,
									form: 'bids'
								}
							})
						});
					}

				});
			} else {
				delete req.body.Bids._id;
				var Bid = Bids.Insert(req.body.Bids);
				Bid.trucker_id = Common.getLoggedUserInfo(req, '_id');
				Bid.status = Bids.POSTED;
				Bid.created_on = Date.now();
				Bid.save(function (err) {
					if (err) return res.json({
						status: Const.HTTP_BAD_REQUEST,
						errors: err.errors,
						form: 'bids'
					});
					var msg = Common.replaceValue(req.__('shipments').t_newbidonshipment,['<<shipper_name>>','<<trucker_name>>','<<amount>>'],[req.session.loadedShipper.name,req.session.user.getName(),Bid.price]);
				    var reflink = '<a href="'+truckerUrl('shipments/detail/'+Bid.shipment_id)+'">'+req.session.loadedShipment.reference_no+'</a>';	Bids.bidPlaced(req.session.loadedShipper,msg,req.session.loadedShipper.id,reflink,req.session.loadedShipment,false);
					delete req.session.loadedShipment;
					delete req.session.loadedShiper;
					if(user.membership.transactions)
						   user.membership.transactions -=1;
					user.save();
					req.session['_user_']= user.toJSON();						
					return res.json({
						status: Const.HTTP_SUCCESS,
						display: {
							success: req.__().t_bid_posted_successfully,
							form: 'bids'
						}
					})
				});
			}
		  });
		} catch (e) {
			console.log(e.message);
		}
	}).post('/last-bid-placed', csrfProtection, AccessRules, function (req, res) {
		try {
			var id = req.body._id || req.session.loadedShipment._id;
			Bids.Model.findOne({
				shipment_id: id,
				//status: Bids.POSTED,
				trucker_id: Common.getLoggedUserInfo(req, '_id')
			}, 'price description shipment_id status', function (err, bid) {
				if (!err && bid)
					res.json({
						status: Const.HTTP_SUCCESS,
						bid: bid
					});
				else res.end();
			})
		} catch (e) {

		}
	}).post('/last-bid-placedondetail', csrfProtection, AccessRules, function (req, res) {
		try {
			var id = req.body._id || req.session.loadedShipment._id;
			Bids.Model.findOne({
				shipment_id: id,
				trucker_id: Common.getLoggedUserInfo(req, '_id')
			}, 'price description shipment_id', function (err, bid) {
				if (!err && bid)
					res.json({
						status: Const.HTTP_SUCCESS,
						bid: bid
					});
				else res.end();
			})
		} catch (e) {

		}
	}).post('/getassigned-vehicles', csrfProtection, AccessRules, function (req, res) {
		if (!req.body._ajx_) return res.json({
			status: Const.HTTP_BAD_REQUEST,
			error: req.__().t_unknown_request
		});
		try {
			var db = require('mongoose');
			Bids.Model.findOne({
				'shipment_id': req.body._s_,
				trucker_id: req.session.user.ID,
				status: Bids.ACCEPTED
			}, 'shipment_id', function (err, data) {
				Drivers.Model;
				Trucks.Model;
			
				Shipments.Model.findOne({
					_id: data ? data.shipment_id : req.body._s_
				}, 'vehicle truck.quantity shipper_id').populate('shipper_id', '_id name contact.mobile_number rating').populate('vehicle.id', 'registration.vehicle_number').populate('vehicle.driver', 'name image mobile_no').exec(function (err, shipment) { 
					var vehicles = [],
						quantity = shipment.truck.quantity || 0,
						info = {},
						_vehicle = {},
						_vehicles = [];
					if (shipment) {
						
						var user = {
							id: shipment.shipper_id._id,
							name: shipment.shipper_id.name.firstname + " " + shipment.shipper_id.name.lastname,
							contact_no: shipment.shipper_id.contact.mobile_number,
							stars: shipment.shipper_id.rating || 0,
						};
					}
					if (shipment && shipment.vehicle.length > 0) {
						shipment.vehicle.forEach(function (v, i) {
							--quantity, info = {}, _vehicle = {};
							_vehicle = v.toJSON();
							//if(!v._id)
							_vehicle['_id'] = v._id || db.Types.ObjectId(Date.now());
							_vehicles.push(_vehicle);
							info['vehicle_no'] = info['driver'] = req.__('shipments').t_not_assignedyet;
							info['driverurl'] = truckerUrl('drivers/assign-driver/' + shipment._id + "/" + _vehicle['_id']);
							info['vehicleurl'] = truckerUrl('vehicles/assign-vehicle/' + shipment._id + "/" + _vehicle['_id']);
							info['vassignorchange'] = req.__().t_assign_vehicle;
							info['dassignorchange'] = req.__().t_assign_driver;

							if (v.driver && v.driver._id) {
								info['driver_name'] = v.driver.name.firstname + " " + v.driver.name.lastname;
								info['driver_no'] = v.driver.mobile_no;
								info['driver_image'] = Common.MediaPath(v.driver, 'image', Drivers);
								info['dassignorchange'] = req.__().t_change_driver;
								info['vehicleurl'] = truckerUrl('vehicles/assign-vehicle/' + shipment._id + "/" + _vehicle['_id']);
								info['driver'] = null;
							}
							if (v.id && v.id._id) {
								info['vehicle_no'] = v.id.registration.vehicle_number;
								info['vassignorchange'] = req.__().t_change_vehicle;
								info['driverurl'] = truckerUrl('drivers/assign-driver/' + shipment._id + "/" + _vehicle['_id']);
							}

							info['sid'] = shipment._id;
							vehicles.push(info);
						});
						for (var i = 0; i < quantity; i++) {
							info = {}, _vehicle = {};
							//_vehicle['_id'] = db.Types.ObjectId(DateTime.now());
							_vehicles.push(_vehicle);
							info['vehicle_no'] = info['driver'] = req.__('shipments').t_not_assignedyet;
							info['driverurl'] = truckerUrl('drivers/assign-driver/' + shipment._id + "/" + _vehicle['_id']);
							info['vehicleurl'] = truckerUrl('vehicles/assign-vehicle/' + shipment._id + "/" + _vehicle['_id']);
							info['sid'] = shipment._id;
							info['vassignorchange'] = req.__().t_assign_vehicle;
							info['dassignorchange'] = req.__().t_assign_driver;
							vehicles.push(info);
						}
						shipment.vehicle = _vehicles;
						shipment.save();
						return res.json({
							status: Const.HTTP_SUCCESS,
							data: vehicles,
							u: user
						});
					} else if (shipment && shipment.vehicle.length <= 0) {
						for (var i = 0; i < quantity; i++) {
							info = {};
							//_vehicle['_id'] = db.Types.ObjectId(Math.random()*DateTime.now()+1);
							_vehicles.push(_vehicle);
							info['vehicle_no'] = info['driver'] = req.__('shipments').t_not_assignedyet;
							info['driverurl'] = truckerUrl('drivers/assign-driver/' + shipment._id + "/" + _vehicle['_id']);
							info['vehicleurl'] = truckerUrl('vehicles/assign-vehicle/' + shipment._id + "/" + _vehicle['_id']);
							info['vassignorchange'] = req.__().t_assign_vehicle;
							info['dassignorchange'] = req.__().t_assign_driver;
							info['sid'] = req.body._s_;
							vehicles.push(info);
						}
						shipment.vehicle = _vehicles;
						shipment.save();
						return res.json({
							status: Const.HTTP_SUCCESS,
							data: vehicles,
							u: user
						});
					} else return res.json({
						status: Const.HTTP_SUCCESS,
						data: [],
						sid: req.body._s_,
						u: []
					});
				})
			})
		} catch (e) {
			res.json({
				status: Const.HTTP_BAD_REQUEST,
				error: req.__().t_unknown_request
			});
		}
	}).post('/cancel/:id?', csrfProtection, AccessRules, function (req, res) {
		Bids.Model.findOne({
			'shipment_id': req.body._id_,
			trucker_id: req.session.user.ID,
			status: Bids.ACCEPTED
		}, 'shipment_id', function (err, data) {
			var shipment_id = '';
			if(err || !data)
				shipment_id =req.body._id_;
			else shipment_id =data.shipment_id;
			Shipments.Model.findOne({
				_id: shipment_id
			}, 'status shipper_id vehicle shipment_id').populate('shipper_id', 'contact name device').populate('vehicle.driver', 'device mobile_no name').exec(function (err, shipment) {
				if (shipment) {
					shipment.status = Shipments.TRUCKER_CANCELED;
					shipment.save(function (err) {
						var name = shipment.shipper_id.name.firstname + " " + shipment.shipper_id.name.lastname;
						Shipments.Canceled(shipment, Common.replaceValue(req.__().t_cancledby_trucker, ['<<shipper_name>>', '<<reference_no>>', '<<support_no>>'], [name, shipment.shipment_id, Const.SUPPORT_NO]));
						if (shipment.vehicle.length > 0) {
								var driver_ids = [],vehicle_ids = [];							
							shipment.vehicle.forEach(function (vd) {
								if(vd.driver){
								var d = vd.driver;
								var driver_name = d.name.firstname + " " + d.name.lastname;
									//tokens = [],
									//mobile_no = [];									
									driver_ids.push(d._id);
								//tokens.push(d.device_token || null);
								//mobile_no.push(d.mobile_no);
								
							 
							  vehicle_ids.push(vd.id);
							var device_token =null;	
							if(d && (device_token = Common.deviceToken(d.device||null))!==false)	
							Shipments.NotifyDrivers(device_token, d.mobile_no, Common.replaceValue(req.__().t_notifydrivers, ['<<driver_name>>', '<<reference_no>>', '<<support_no>>'], [driver_name, shipment.shipment_id, Const.SUPPORT_NO])); 	
							 }		
							});							
							Shipments.CanceledByTruckerFreeDriverVehicle(driver_ids,vehicle_ids);
						}
						return res.json({
							status: Const.HTTP_SUCCESS
						});
					});
				} else if (err) return res.json({
					status: Const.HTP_BAD_REQUEST,
					error: req.__().e_cancel_shipment
				});
			})
		})
	}).post('/tripendon', csrfProtection, AccessRules, function (req, res) {
		try {
			Shipments.Model.findOne({
				_id: req.body._id_
			}, 'vehicle loading.lp_address transit.t_date transit.t_time', function (err, doc) {
				if (doc && !err) {
					res.json({
						status: Const.HTTP_SUCCESS,
						t: {
							started_from: doc.loading.lp_address,
							start_date: doc.transit.t_date,
							start_time: doc.transit.t_time,
							endonplace: doc.vehicle.end_point,
							endondate: (new Date(doc.vehicle.end_at || 0)).toDateString(),
							endontime: (new Date(doc.vehicle.end_at || 0)).toTimeString(),
						}
					});
				}
			});
		} catch (e) {
			console.log(e.message);
		}
	});
	var rboffset;
	router.get('/regular-load-board', csrfProtection, AccessRules, function (req, res) {
		try {
		  if(req.session.user.isMembershipExpired){
			 return res.render('trucker/shipments/regular-board', {
						extractScripts: true,
						csrfToken: req.csrfToken(),
				        expired:1
			  });   
		  }
		  var cond = {};
			    cond['status'] =Shipments.PENDING;
			    cond['shipment_type.code'] =Shipments.REGULAR_BOARD;
			    cond['transit.t_expdatetime'] ={$gte: DateTime.now()};
			
			if(req.query && req.query.Filters){
				var filters  = req.query.Filters,
					fromdate = filters.date_from.split('/'),
					todate   = filters.date_to.split('/');
			        fromdate = new Date(fromdate[2],fromdate[1]-1,fromdate[0]);
				    todate   = new Date(todate[2],todate[1]-1,todate[0]);
				    if(filters.area)
				     cond['loading.lp_state']   = new RegExp(filters.area,'i') || undefined;	
				    if(Array.isArray(filters.area) && filters.area.length>0)
				       cond['loading.lp_state'] ={$in:filters.area};	
					   console.log(fromdate);	
					   console.log(todate);	
				     if(!isNaN(todate.getTime()))			    
				       cond['transit.date'] = {$lte:todate.getTime()};
				    if(!isNaN(fromdate.getTime()))
					   cond['transit.date'] = {$gte:fromdate.getTime()};
			}
		  Users.Model.findOne({_id:req.session.user.ID},'membership',function(err,user){
			rboffset = 0;
			Shipments.Model.find(cond, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id shipment_detail.overall_total').populate('truck.ttype', 'name loading_capacity').skip(rboffset).limit(Shipments.LIMIT_BID_BOARD)
				.sort({
					'transit.t_expdatetime': 1
				}).exec(function (err, doc) {
					rboffset += Shipments.LIMIT_BID_BOARD;
					res.render('trucker/shipments/regular-board', {
						extractScripts: true,
						csrfToken: req.csrfToken(),
						doc: doc,
						expired:0,
						transactions:user.membership.transactions,
						filters:req.query.Filters||{}
					});
				});
		});		  
		} catch (e) { console.log(e.message);
			res.redirect(webappUrl(''));
		}
	}).post('/regular-load-board', csrfProtection, AccessRules, function (req, res) {
		try {
			if(req.session.user.isMembershipExpired){
			   return res.json({status:Const.HTTP_NON_AUTHORATIVE,msg:req.__('users').t_membership_expired})	
			}
			var cond = {};
			    cond['status'] =Shipments.PENDING;
			    cond['shipment_type.code'] =Shipments.REGULAR_BOARD;
			    if(fromdate.length>2 && fromdate.getTime())			    
				       cond['transit.date'] = {$gte:todate.getTime()};
				    if(todate.length>2 && todate.getTime())
					   cond['transit.date'] = {$lte:fromdate.getTime()};
			if(req.body && req.body.Filters){
				var filters  = req.body.Filters,
					fromdate = filters.date_from.split('/'),
					todate   = filters.date_to.split('/');
			        fromdate = new Date(fromdate[2],fromdate[1]-1,fromdate[0]);
				    todate   = new Date(todate[2],todate[1]-1,todate[0]);
				    if(filters.area)
				     cond['loading.lp_state']   = new RegExp(filters.area,'i') || undefined;	
				    if(Array.isArray(filters.area) && filters.area.length>0)
				       cond['loading.lp_state'] ={$in:filters.area};	
					   console.log(fromdate);	
					   console.log(todate);	
				     if(!isNaN(todate.getTime()))			    
				       cond['transit.date'] = {$lte:todate.getTime()};
				    if(!isNaN(fromdate.getTime()))
					   cond['transit.date'] = {$gte:fromdate.getTime()};
			}
			Shipments.Model.find(cond, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id shipment_detail.overall_total').populate('truck.ttype', 'name loading_capacity').skip(rboffset).limit(Shipments.LIMIT_BID_BOARD)
				.sort({
					'transit.t_expdatetime': 1
				}).exec(function (err, doc) {
					rboffset += Shipments.LIMIT_BID_BOARD;
					res.render('trucker/shipments/paginate_regularloadboard_bids', {
						layout: false,
						extractScripts: true,
						csrfToken: req.csrfToken(),
						doc: doc,
						
					});
				});
		} catch (e) { //console.log(e.message);
			res.redirect(webappUrl(''));
		}

	}).post('/accept-regular-shipment', csrfProtection, AccessRules, function (req, res) {
		try {
			Users.Model;
			Shipments.Model.findOne({
				_id: req.body._id_,
				status: Shipments.PENDING,
				'shipment_type.code': Shipments.REGULAR_BOARD
			}).populate('shipper_id', 'name contact device').exec(function (err, shipment) {
				if (err || !shipment) return res.json({
					statusCode: Const.HTTP_NO_CONTENT,
					txtMsg: req.__().t_shipmentnot_found
				});
				shipment.status = Shipments.ACCEPTED;
				shipment.trucker_id = req.session.user.ID;
				shipment.save(function (err) {
					var trucker_name = req.session.user.getName(),
						shipper_name = shipment.shipper_id.name.firstname + " " + shipment.shipper_id.name.lastname,
						trucker = req.session.user;
					//notify shipper...				   
					Shipments.regularBoardAccepted({
							_id: shipment._id,
							device_token: Common.deviceToken(shipment.shipper_id.device||null),
							mobile_no: shipment.shipper_id.contact['mobile_number'],
							email: shipment.shipper_id.contact['email']
						},
						Common.replaceValue(req.__('shipments').t_regulatshipment_accepted, ['<<trucker_name>>', '<<reference_no>>', '<<date>>', '<<time>>', '<<shipper_name>>','<<trucker_contactno>>', '<<amount>>', '<<support_no>>'], [trucker_name, shipment.refrence_no, shipment.transit.t_date, shipment.transit.t_time, shipper_name, trucker.getMobileNo(),shipment.shipment_detail.overall_total, Const.SUPPORT_NO]));
					//notify trucker...
					Shipments.regularBoardAccepted({
							_id: shipment._id,
							device_token: trucker.deviceToken(),
							mobile_no: trucker.getMobileNo(),
							email: trucker.getEmail()
						},
						Common.replaceValue(req.__('shipments').t_notify_trucker_regular_accepted, ['<<trucker_name>>', '<<reference_no>>', '<<date>>', '<<time>>', '<<shipper_name>>','<<shipper_contactno>>' ,'<<amount>>', '<<support_no>>'], [trucker_name, shipment.refrence_no, shipment.transit.t_date, shipment.transit.t_time, shipper_name,shipment.shipper_id.contact.mobile_number,shipment.shipment_detail.overall_total, Const.SUPPORT_NO]));

					if (err) return res.json({
						statusCode: Const.HTTP_UNPROCESSABLE,
						txtMsg: req.__().t_err_while_updating
					});
					return res.json({
						statusCode: Const.HTTP_SUCCESS,
						id:shipment._id,
						txtMsg: req.__('shipments').t_regular_bid_accepted
					});
				});
			});
		} catch (e) {
			console.log(e.message);
		}

	}).post('/area-states', csrfProtection, AccessRules,function(req,res){
		Shipments.Model.distinct('loading.lp_state',{status:0},function(err,states){
			res.json(states);
		});
	}).get('/view/:id', csrfProtection, AccessRules, function (req, res) {
		Categories.Model;
		Shipments.Model.findOne({_id:req.params.id}).populate('truck.ttype', 'name loading_capacity').populate('items.parent_category').populate('items.sub_category').exec(function(err,shipment){
			res.render('trucker/shipments/regularboarddetail',{extractScripts: true,csrfToken: req.csrfToken(),shipment: shipment,Shipments:Shipments});
		});	
	});
	/*var coffset;
	router.get('/confirmation',csrfProtection,AccessRules,function(req,res){
		coffset=0;
		Shipments.Model.find({status:Shipments.BID_ACCEPTED},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id').populate('truck.ttype','name loading_capacity').sort({'transit.texpdatetime':-1}).skip(coffset).limit(Shipments.LIMIT_BID_BOARD).exec(function(err,doc){ console.log(doc);
			if(doc)
			  coffset +=Shipments.LIMIT_BID_BOARD	
			res.render('trucker/shipments/confirmation',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc});
		}); 
	}).post('/confirmation',csrfProtection,AccessRules,function(req,res){
		Shipments.Model.find({status:Shipments.Bid_ACCEPTED},{$sort:{'transit.texpdatetime':-1}}).skip(coffset).limit(Shipments.LIMIT_BID_BOARD).exec(function(err,doc){
			if(doc)
			  coffset +=Shipments.LIMIT_BID_BOARD	
			res.render('trucker/shipments/paginate-confirmation',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc});
		}); 
	}).put('/confirmed',csrfProtection,AccessRules,function(req,res){ // trucker accepted shipment double confirmation
		Bids.Model.findOne({_id:req.body.id,trucker_id:req.session.user.ID,status:Bids.ACCEPTED},'price shipper_id').populate('trucker_id','name').exec(function(err,bid){
			Shipments.Model.findOne({_id:bid.shipment_id||false},'transit.t_date transit.t_time ').populate('shipper_id','name contact device_token').exec(function(err,shipment){
			  if(doc && !err){
				 var trucker_name = bid.trucker_id.name.firstname+" "+bid.trucker_id.name.lastname,
					 shipper_name = shipment.shipper_id.name.firstname+" "+bid.shipper_id.name.lastname; 
				 Shipments.truckerConfirmedShipment(Common.replaceValue(req.__().t_trucker_confirmedshipment,['<<trucker_name>>','<<shipper_name>>','<<date>>','<<time>>','<<support_no>>'],
				 [trucker_name,shipper_name,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO],
				 shipment));
			}		
		  });
		});	
	});*/
	//access rules...
	function AccessRules(req, res, next) {
		try {
			if(req.session.user.isTrucker() && req.session.user._id){
	  if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));
	}else res.redirect(webappUrl(''));
		} catch (e) {
			res.redirect(webappUrl(''));
		}
	}

	module.exports = router;