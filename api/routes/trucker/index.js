var express  = require('express');
var passport = require('passport');
var router   = express.Router();
var User     = appModel('Users');
var Settings = appModel('Settings');
var Settings = new Settings();
var Users    = new User();

var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
	};
/* GET home page. */
router.get('/',function(req, res, next) {
	res.redirect(webappUrl(''));
	req.session.trucker={}; 												 
	console.log('index');												 
	console.log(req.session.trucker);												 
	res.render('trucker/login',{ layout: 'layouts/trucker/loginregister-layout','image':JSON.stringify(images),isverified:null});
});
router.get('/login',function(req,res){
	res.redirect(webappUrl(''));
}).post('/login',function(req,res){ 	
	var sess = (req.session.trucker && req.session.trucker._id)?req.session.trucker._id:false;
	if(sess)
	  return res.redirect(truckerUrl("users/dashboard"));
    var username = req.body.Login.username || false,
		password = req.body.Login.password || false; 
	    
	    Users.oldPassword = password;
	    Users.load({'contact.email':username},function(err,user){
		   if(err) return new Logger(err);
		   switch(true){			
			case ((!Users.validPassword || !Users.isTrucker)): 
			 res.json({msg:_locals().invalid_credential}); 
			break; 
			case ((user && !Users.isActive && user.hash_token!==null)):
				var ht = user.hash_token.split(":"),time = 1000*60*60*24*1,htime  = ht[1]+time;
				   if(htime<Date.now())     
			          res.json({msg:_locals().verify_email_with_link}); 
				   else
			          res.json({msg:_locals().verify_email}); 
			break;
			case ((!Users.isActive && !user.hash_token)):
			 res.json({msg:_locals().inactive_user}); 
			break;
			case (user && Users.validPassword && Users.isActive):			
			 Settings.Model.findOne({},'-created_on -modified_on -_id',function(err,s){				
				registredUserProfile(req,res,user);
			    setAuthentication(req,res,user,s);				
		        return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl('login')});  
			 });	   
			break;	
			default:
			 res.json({msg:_locals().invalid_credential}); 
			break;			
		}
	});	
	
	
});
router.get('/forgot-password',function(req,res){
  res.render('shipper/forgot-password',{ layout: 'layouts/shipper/loginregister-layout',isverified:null});	
}).post('/forgot-password',function(req,res){
	var worker = appHelper('Worker'),email=req.body.Forgot.email;
	if(!appHelper('RegularExp').isEmail(email||''))
	   return res.json({errors:{email:{message:_locals().email}},form:'forgot'});
	var query =Users.Model.findOne({"contact.email":email,status:1});
	    query =query.select('-_id name contact.email hash_token');
	    query.exec(function(err,user){
			if(err || !user)
			   return res.json({errors:{email:{message:_locals(appLang,'login').registred_email}},form:'forgot'});             var ht= user.hash_token.split(":"),time = 1000*60*60*2,htime=ht[1]+time,
				    token = MD5(user.contact.email+Date.now())+":"+Date.now();
			if(htime<Date.now())
			   return res.json({errors:{email:{message:_locals().recently_sent_email}},form:'forgot'});
			else{
			   worker.execPHP('Mailer.php '+email+" confirmemail "+token,function(err,r){
		        if(!err){
			      user.hash_token = token;
			      user.modified_on= Date.now();
				  user.isvalidated= 1;	
			      user.save();
			      
			}
           });
		    return res.json({success:Const.HTTP_SUCCESS,display:{html:_locals().recently_sent_email},form:'forgot'});	
		 }
		});
	
}).get('/reset-password',function(req,res){
	res.render('shipper/reset-password',{ layout: 'layouts/shipper/loginregister-layout',isverified:null,token:req.params.token||null});
}).post('/reset-password',function(req,res){
	var tk = req.body.Reset.token.split(":"),token=tk[0],htime=tk[1],
		np = req.body.Reset.new_password    ||null,
		cp =req.body.Reset.confirm_password || null;
	if(!np)
		return res.json({errors:{new_password:{message:_locals().new_password}},form:'reset'});
	if(!cp || np!=cp)
		return res.json({errors:{confirm_password:{message:_locals().confirm_password}},form:'reset'});		
	if(np && cp && !appHelper('RegularExp').isString(32,32,token||''))
	  return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
	else{
		Users.Model.findOne({hash_token:tk},'contact.email name',function(err,user){
			if(!user || err)
	          return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
			else{
			worker.execPHP('Mailer.php '+email+" passwordupdated "+user.name,function(err,r){
		        if(!err){
			      user.password = np;
				  user.isvalidated=1;
				  user.save();			      
			    }
              });				
			return res.json({success:Const.HTTP_SUCCESS,display:{html:_locals().success_reset},form:'reset'});
		  }
		})
	}
		
		
}).get('/register',function(req,res){ Appage.title('Register'); 
   res.redirect(webappUrl('register'));
})/*.post('/register',function(req,res){
	var user = {
		name:{
			firstname:req.body.Register.name['firstname'],
			lastname:req.body.Register.name['lastname'],
		},
		contact:{
		      email:req.body.Register.email,	
		      mobile_number:req.body.Register.mobile_number,	
		},
		status:0,
		password:req.body.Register.password,
		image   :req.body.Register.image?'default':null,
		confirm_password : req.body.Register.confirm_password,
		has_cp  :0,
		role    :Users.INDIVIDUALTRUCKER,
	};
	var user = Users.Add(user);
	user.save(function(err){ //console.log(err.errors); 
		//console.log(err)
		if(err){
			if(err.errors){		
			  return res.json({'errors':err.errors});
			}else
		      return res.json({'errors':{'contact.email':{'message':err.message}}});
		}
		else{
			user.image = user.image=='default'?null:user.image;
		    UploadImage.allowedExtensions = images.extensions;
			UploadImage.allowedSize       = images.maxsize;
			UploadImage.setFilename(user.image);
			var fn =UploadImage.base64(req.body.Register.image).upload(Common.media_dir_path(user._id,Users));	
			user.isvalidated=1;
			user.image = fn;
			user.plocation  = 'user-type';
			user.save(function(er){
			  registredUserProfile(req,res,user);			  	
		      return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});  
			}); 
		}
		
	})
 
})*/
router.get('/user-type',validateAction,validateRegister,function(req,res){ // get info of user type agent|Individual|company
	res.render('trucker/user-type',{ layout: 'layouts/trucker/loginregister-layout',isverified:verifyEmail(req)});
}).post('/user-type',validateAction,validateRegister,function(req,res){  
	
	try{								    
      if(Validate.isEmpty(req.body.Register))
	     return res.json({'errors':{'usertype':{'message':req.__('register').e_user_type}}});
		if(req.body.Register.usertype && !(req.body.Register.usertype in Users.Roles()))
	     return res.json({'errors':{'usertype':{'message':req.__('register').e_user_type}}});
		 Users.Model.findOne({'_id':req.session.register._id},function(err,user){
			user.isvalidated  =1; 
			user.account_type =req.body.Register.usertype;
			user.role =req.body.Register.usertype;
			user.membership  = {
			     plan_type:'Free',
				 plan_code:0,
				 transactions:10,
				 start_from:DateTime.now()
			};  
		    user.plocation    ='company'; 
			   user.save(function(err){ 
				if(err) return res.json({'errors':{'usertype':{'message':err.message}}});
				setpLocation(req,user.plocation);
				//if(user.role==Users.INDIVIDUALTRUCKER){ 												  
				/*if(user.hash_token!="" && user.status==Users.INACTIVE){
				   req.session.setFlash('verify-email',_locals().verify_email)	
				   return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl('')});
				}else{
				   if(user.role==Users.INDIVIDUALTRUCKER)
					  return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl("users/"+user.plocation)});*/
				   return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
			//	}
				//}else{
				  // return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
				//}
			}); 
		 });	   
	}catch(e){ //console.log(e.message)
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
	
}).get('/company',validateAction,validateRegister,function(req,res){ // company information form	
	res.render('trucker/company',{ layout: 'layouts/trucker/loginregister-layout',action:false,user:{},isverified:verifyEmail(req)});
}).post('/company',validateAction,validateRegister,function(req,res){
	try{ console.log(req.session.register._id);
	Users.Model.findOne({'_id':req.session.register._id},function(err,user){
		//user['business_type'] = req.body.Register['business_type'];
		//user['company_type']  = req.body.Register['company_type'];
              console.log(req.body.Register.business);
		for(var k in req.body.Register.business)
		user.business[k]      = req.body.Register.business[k] || null;
		user['pancard']       = req.body.Register.pancard || null;
		user.plocation        = 'company-address';
		user.isvalidated      = 1;
                console.log(user);		 
		user.save(function(err,usr){ console.log(err);  
			if(err)
			  return res.json({errors:err.errors});
			else{ 
				setpLocation(req,user.plocation);
				res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
			}
		})
	});
	}catch(e){
		res.json({status:Const.HTTP_SUCCESS,location:'login'});
	}
}).get('/company-address',validateRegister,function(req,res){	
	res.render('trucker/company-address',{ layout: 'layouts/trucker/loginregister-layout',action:false,user:{},csrfToken:false,isverified:verifyEmail(req)});	
}).post('/company-address',validateRegister,function(req,res){
	try{
	Users.Model.findOne({_id:req.session.register._id},function(err,user){
		if(!user || err) return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl('')});
		for(var k in req.body.Register.business){ 
			user.business[k] = req.body.Register.business[k];
		}
		user.plocation        = 'bank-info';
		user.isvalidated      = 1;		
		user.save(function(err){ 
			if(err)
			  return res.json({errors:err.errors});
			else{ 
				//delete req.session.register;
				setpLocation(req,user.plocation);
				return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});	
			}
		});
	});
	}catch(e){ //console.log(e.message);
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
}).get('/bank-info',validateRegister,function(req,res){	
	res.render('trucker/bank-info',{ layout: 'layouts/trucker/loginregister-layout',isverified:verifyEmail(req),action:false,user:{},csrfToken:false});
}).post('/bank-info',validateRegister,function(req,res){ 
	try{
	Users.Model.findOne({_id:req.session.register._id},function(err,user){
		if(!user || err) return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl('')});
		for(var k in req.body.Register.account){ 
			user.account[k] = req.body.Register.account[k];
		}
		user.plocation        = 'dashboard';
		user.isvalidated      = 1;		
		user.save(function(err){ 
			if(err)
			  return res.json({errors:err.errors});
			else{ 
			   delete req.session.register;
			   if(!req.session.user.isActive() && !Validate.isEmpty(user.hash_token)){
				req.session.setFlash('verify-email',req.__('users').t_verification_email_sent)
				return res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});	   
					
			   }else{
				setAuthentication(req,res,user)   
				setpLocation(req,user.plocation);
				return res.json({status:Const.HTTP_SUCCESS,location:truckerUrl('users/'+user.plocation)});
			   }
			    	
			}
		});
	});
	}catch(e){ //console.log(e.message);
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
}).get('/skip-bank-detail',function(req,res){ // get info of
	try{
	Users.Model.findOne({'_id':req.session.register._id},function(err,user){
		if(user.plocation=='bank-info'){
			user.plocation = 'dashboard';
			user.save(function(err){ console.log(err);
				if(!err){ 
				  delete req.session.register;
			   if(!req.session.user.isActive() && !Validate.isEmpty(user.hash_token)){
				req.session.setFlash('verify-email',req.__('users').t_verification_email_sent)
				return res.redirect(webappUrl(''));	   
					
			   }else{
				setAuthentication(req,res,user)   
				setpLocation(req,user.plocation);
				return res.redirect(truckerUrl('users/'+user.plocation));
			   }
				   
				}
			})
		}
	});
	}catch(e){ console.log(e.message);
		res.redirect(webappUrl(''));
	}
}).get('/help-and-support',function(req,res){
	
	res.render('trucker/contact_us',{})
});

router.get('/logout',function(req,res){
	req.session.destroy();
	return res.redirect(webappUrl(''));
})



function isUserCompletedProfile(req,res,user,url){
	req.session.user = {};
	if(user.status==Users.INACTIVE && user.hash_token!="" && !ValidateIsShipper(user.role))	
	   res.redirect(shipperUrl(''));
	else if(ValidateIsShipper(user.role) && user.plocation!="dashboard")
	   res.redirect(shipperUrl(user.plocation));
	else if(user.status==Users.ACTIVE && ValidateIsShipper(user.role) && user.plocation=="dashboard")
	   res.redirect(shipperUrl(user.plocation));
	else res.redirect(shipperUrl(''));
	
}
function setAuthentication(req,res,user,s){	
	req.session['_user_'] = user;
	req.session.settings = s;
	setpLocation(req,user.plocation,res);
}
// registration authentication...

function registredUserProfile(req,res,user){ //console.log(user);
   var url =webappUrl('');	
   if(Users.ValidateIsTrucker(user.role)){
	  req.session.register = {
	   _id:user._id,
	   role:user.role,	  
	   token:MD5(user._id+Date.now()),
	   redirectTo:user.plocation,	  
      };
      req.session.register.email = Validate.isEmpty(user.hash_token)?false:user.contact.email;
	  url = user.plocation; 
   }
   setpLocation(req,url,res);   	
}
function setpLocation(req,url,res){
  try{
  if(url){
	 if(req.session.register)
		req.session.register['redirectTo'] = url;  
	 if(req.session.user.isLoaded) 
	    req.session.user.redirectTo = url;
	 else req.session.user = {uncprofile:url};  
  }
  }catch(e){ console.log(e.message);
	//res.redirect('/shipper/');  
  }
}
function validateRegister(req,res,next){	
	try{
	//if(!Users.ValidateIsTrucker(req.session.register.role) || !Users.ValidateIsAgent(req.session.register.role))	
	  // throw Error('');	
	req.session.register._id;
	validateAction(req,res,next);
	}catch(e){
	 res.redirect(webappUrl(''));	
	}  
} 
function validateAction(req,res,next){
 var url = req.session.register && req.session.register.redirectTo?req.session.register.redirectTo:webappUrl('');
 if(url==req.path.replace('/','')) next();
 else if(url=='dashboard') return res.redirect(truckerUrl('users/dashboard'));	
 else if(url!=req.path.replace('/','')) return res.redirect(url);
	
}
function verifyEmail(req){
  var email =  req.session.register.email || false;
  if(email){
	  var url = "//www."+email.substring(email.lastIndexOf('@'));	
	return '<p class="text-danger"> You have not verified email id yet.</p><a href="javascript:;" id="resend-verification-email">Resend Verification mail</a> | <a href="'+url+'" target="_blank">Verify Now</a>';  
  }
}

router.post('/resend-email-token',function(req,res,next){ // resend verification email...
	var email = req.body.email || req.session.register.email,worker  = appHelper('Worker');
	if(!email) return res.json({'errors':{'username':{'message':_locals().email_error}}});
	var token = MD5(email+Date.now())+":"+Date.now(); //{$set:{hash_token:token}},
	Users.Model.findOne({'contact.email':email},function(err,user){
	 if(user){
		worker.execPHP('Mailer.php '+email+" confirmemail "+token,function(err,r){
		         console.log(err);	 
		        //s console.log(r);
		    if(!err){
			   user.isvalidated= 1;	
			   user.hash_token=  token;
	           user.save();
			   var url = "//www."+email.substring(email.lastIndexOf('@'));	
			   var html = '<p class="text-danger"> You have not verified email id yet.</p><a href="'+url+'">Verify Now</a>';	
			   return res.json({success:Const.HTTP_SUCCESS,'display':{'html':html}});
			}else{
			   return res.json({'errors':{'username':{'message':err.message}}});
			}
			//next();
          }); 
	 }else{
		return res.json({'errors':{'username':{'message':_locals().email_error}}});
	 }	
	});
});
function AccessRules(req,res,next){ 
	//req.session.trucker = User;	
	try{
	if(req.session.user.isTrucker() && req.session.user._id)
	   next();
	else res.redirect(webappUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
module.exports = router;
