var router = require('express').Router(),
    csrf = require('csurf'),
    csrfProtection = csrf({ cookie: true });
var db      = require('mongoose');
var Bids   = appModel('Bids'),
	Bids   = new Bids;
/* get bid price for trucker user
 * @params _id_ (array) bid_id
 * @returns json response (json)
 */ 
router.post('/get-price',csrfProtection,AccessRules,function(req,res){
	var ids = req.body._id_.map(function(val){
		return db.Types.ObjectId(val);
	}); 
	Bids.Model.find({shipment_id:{$in:ids},trucker_id:req.session.user.ID,
					 status:Bids.ACCEPTED},'price shipment_id',function(err,bid){
		if(bid && !err)
		  return res.json({status:Const.HTTP_SUCCESS,dta:bid});
		else return res.json({status:Const.BAD_REQUEST,dta:[]});
	})
});

//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isTrucker() && req.session.user._id){
	  if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));
	}else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
module.exports= router;