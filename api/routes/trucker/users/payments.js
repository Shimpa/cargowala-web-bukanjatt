"use strict";
var router = require('express').Router(),
    Users   = appModel('Users'),
	Membership=appModel('Memberships'),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),

Payments= appModel('Payments'),
Shipments   = appModel('Shipments'),
Payments= new Payments(),
Shipments   = new Shipments();
var Membership = new Membership(),offset,coffset=0;

router.get('/',csrfProtection,AccessRules,function(req,res){
	offset=0,coffset=0;
    Shipments.Model; /*status:Payments.PENDING*/	Payments.Model.find({'ship_by':req.session.user.ID}).populate('shipment_id','loading.lp_city unloading.up_city truck transit.t_time transit.t_date shipment_detail.truck_name shipment_id').skip(offset).limit(Payments.LIMIT).exec(function(err,rec){
	if(!err && rec.length>0) offset+=Payments.LIMIT;	
	res.render('trucker/payments/list',{extractScripts:true,csrfToken:req.csrfToken(),payments:rec});
	})
}).post('/pending',csrfProtection,AccessRules,function(req,res){
	Shipments.Model; /*status:Payments.PENDING*/	Payments.Model.find({'ship_by':req.session.user.ID}).populate('shipment_id','loading.lp_city unloading.up_city truck transit.t_time transit.t_date shipment_detail.truck_name shipment_id').skip(offset).limit(Payments.LIMIT).exec(function(err,rec){
	if(!err && rec.length>0) offset+=Payments.LIMIT;	
	res.render('trucker/payments/pending_paginates',{layout:false,extractScripts:true,csrfToken:req.csrfToken(),payments:rec});
	})
}).post('/completed',csrfProtection,AccessRules,function(req,res){
	Shipments.Model; /*status:Payments.PENDING*/	Payments.Model.find({'ship_by':req.session.user.ID}).populate('shipment_id','loading.lp_city unloading.up_city truck transit.t_time transit.t_date shipment_detail.truck_name shipment_id').skip(coffset).limit(Payments.LIMIT).exec(function(err,rec){
	if(!err && rec.length>0) coffset+=Payments.LIMIT;	
	res.render('trucker/payments/completed_paginates',{layout:false,extractScripts:true,csrfToken:req.csrfToken(),payments:rec});
	})
})





//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isTrucker() && req.session.user._id){
	  if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));
	}else res.redirect(webappUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

module.exports = router;
