"use strict";
var router = require('express').Router(),
    Users   = appModel('Users'),
	Membership=appModel('Memberships'),
	WebNotifications   = appModel('WebNotifications'),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
TruckLanes  = appModel('TruckLanes'),
Payments    = appModel('Payments'),
TruckLanes  = new TruckLanes(),
Shipments   = appModel('Shipments'),
Trucks      = appModel('Trucks'),
Bids      = appModel('Bids'),
Shipments   = new Shipments(),
Bids   = new Bids(),
Notify  = appHelper('Notify'),
Drivers  = appModel('Drivers'),
Drivers  = new Drivers(),	
Transactions    =appModel('Transactions'),
WebNotifications=new WebNotifications,
Transactions=new Transactions;
var Trucks = new Trucks();
var Payments = new Payments();
var Membership = new Membership();
var crypto = require('crypto'); 
var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
	};
router.get('/dashboard',AccessRules,csrfProtection,function(req,res){ Appage.title('Dashboard');
									 
    res.render('trucker/users/dashboard',{title:'Dashboard',extractScripts: true,csrfToken: req.csrfToken(),layout:'layouts/trucker/dashboard',otpverified:req.session.user.isMobileVerified,otpsent:req.session.user.isOTPSent});
}).get('/edit-profile',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},function(err,user){
		var error = false;
		if(err || !user) error = 1;	   
	    res.render('trucker/users/edit_profile',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user,image:images,Users:Users});
	});
}).post('/update-mobile',csrfProtection,AccessRules,function(req,res){
	if(Validate.isNaN(req.body.Users.mobile,10,10))
	   return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__().e_enteralidmobileno});	
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},'otp contact',function(err,user){
	  if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	  if(user){
		 user.contact['mobile_number']  = req.body.Users.mobile;
		 user.save(function(err){
			if(err) 
			return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
			if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_'] = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_successfullyupdated});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		} 
			 
			
			 
		 }) 
	  }	
	});
}).post('/edit-profile',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},'otp contact',function(err,user){
		user.update = 67; // to identify it is update case...
		var old_mobileno = user.contact['mobile_number'];
		var data = {
		name:{
			firstname:req.body.Users.firstname,
			lastname:req.body.Users.lastname,
		},
		contact:{
		      mobile_number:req.body.Users.mobile_number,
			  email:user.contact.email,
		},
	    },imagename = user.image;
		
		delete req.body.Users.status;
		delete req.body.Users.password;
		delete req.body.Users._id;
		delete req.body.Users.role;
		delete req.body.Users.hash_token;
		delete req.body.Users.plocation;
		
		for(var i in req.body.Users)
			user[i] = req.body.Users[i];
			
		for(var i in data)
		    user[i]  = data[i];	
		user['image']= req.body.Users.oimage;
        var business  = {};; 
		for(var i in user.business)
			business[i] = user.business[i];
		business['service_taxno'] =  req.body.Users.service_taxno;		
		user.business = business;
	   user.save(function(err){ console.log(err);
		   if(err) return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});
		   if(!Validate.isEmpty(req.body.Users.image)){
		    var imagename = imagename || Date.now();
		    UploadImage.allowedExtensions = images.extensions;
			UploadImage.allowedSize       = images.maxsize;
			UploadImage.setFilename(imagename);
			var fn =UploadImage.base64(req.body.Users.image).upload(Common.media_dir_path(user._id,Users));	
			user.image = fn;   
		   }
			user.isvalidated=1;
		    if(old_mobileno!=user.contact.mobile_number){	
	   var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['verified'] = 0;
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;					
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_profile_updatedotp_sent},form:'users',next:shipperUrl('users/dashboard')}) 			
		        })
	  }else{
			 return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_profile_updated},form:'users'});    		
	  }
	   })
	});
}).get('/business-detail',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID||false,status:Users.ACTIVE},function(err,user){
		var error = false;
		if(err || !user) error = 1;	  
		console.log(user);
	    res.render('trucker/company',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user,action:truckerUrl('users/business-detail')});
	});

}).post('/business-detail',csrfProtection,function(req,res){
	
 	Users.Model.findOne({_id:req.session.user.ID||false,status:Users.ACTIVE},function(err,user){
	   if(err || !user) return res.json({status:Const.HTTP_BAD_REQUEST,html:err.massege,form:'users'});
	   delete req.body.Register.oimage;
	   delete req.body.Register.status;
	   delete req.body.Register.password;
	   delete req.body.Register._id;
	   delete req.body.Register.role;
	   delete req.body.Register.hash_token;
	   delete req.body.Register.plocation;

	  for(var i in req.body.Register.business)
		  user.business[i] = req.body.Register.business[i] || null;
	  if(req.body.registerascompany && user.role==Users.INDIVIDUALTRUCKER){
		  user.role = Users.TRUCKERCOMPANY;
	  }
		
		user.save(function(err){
			if(err){  
			for(var i in err.errors)
		    var errr  = err.errors[i].message;
			return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'register'});
		    }else
			 return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__().t_business_updated},form:'register'});
			 	
		});
	});

}).get('/company-address',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},function(err,user){
		var error = false;
		if(err || !user) error = 1;	  
		console.log(user);
	    res.render('trucker/company-address',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user,action:'users/company-address'});
	});
}).post('/company-address',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},function(err,user){
	   if(err || !user) return res.json({status:Const.HTTP_BAD_REQUEST,html:err.massege,form:'users'});
	   delete req.body.Register.oimage;
	   delete req.body.Register.status;
	   delete req.body.Register.password;
	   delete req.body.Register._id;
	   delete req.body.Register.role;
	   delete req.body.Register.hash_token;
	   delete req.body.Register.plocation;
	  for(var i in req.body.Register.business){
		  user.business[i] = req.body.Register.business[i] || " ";
	  }
	  user.business['officeaddresssameasbusiness'] =  req.body.Register.business.officeaddresssameasbusiness;
		
		user.save(function(err){
			if(err){  
			//for(var i in err.errors)
		    //var errr  = err.errors[i].message;
			return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'register'});
		    }else
			 return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__().t_business_updated},form:'register'});
			 	
		});
	});
}).get('/bank-detail',AccessRules,csrfProtection,function(req,res){
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},function(err,user){
		var error = false;
		if(err || !user) error = 1;	  
	    res.render('trucker/bank-info',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user,action:'users/bank-info'});
	});
}).post('/bank-info',AccessRules,csrfProtection,function(req,res){ 
	try{
	Users.Model.findOne({_id:getLoggedUserInfo(req,'_id')||false,status:Users.ACTIVE},function(err,user){
		
		if(!user || err) return res.json({status:Const.HTTP_SUCCESS,display:{error:req.__().e_bankinfo},form:'register'});
		user.update = 67;
		for(var k in req.body.Register.account){ 
			user.account[k] = req.body.Register.account[k];
		}
		user.save(function(err){ 
			if(err)
			  return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'register'});
			else{ 
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__().t_bankinfo_updated},form:'register'});	
			    	
			}
		});
	});
	}catch(e){ console.log(e.message);
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
});
router.get('/incomplete-shipment',csrfProtection,AccessRules,function(req,res){
	Shipments.Model.findOne({status:Shipments.PENDING},{},{'sort':{_id:-1}},function(err,doc){
	  var ship_dtl = {}
	      for(var i in doc.shipment_detail)
			  ship_dtl[i] = doc.shipment_detail[i];
		ship_dtl['_id'] = doc._id;
	  res.render('shipper/users/incomplete-shipment',{extractScripts: true,csrfToken: req.csrfToken(),doc:ship_dtl});	
	});
}).post('/complete-shipment',csrfProtection,AccessRules,function(req,res){
	Shipments.Model.findOne({_id:req.body._id,status:Shipments.PENDING},'redirect_to loading unloading',function(err,doc){
		if(err || !doc) return res.redirect(shipperUrl('users/dashboard'));
	    req.session.shipment = {
	    	_id:doc._id,
			loading:doc.loading.lp_address,
			unloading:doc.unloading.up_addesss,
			incomplete:1,			
	    };
		res.redirect(shipperUrl('shipments/'+doc.redirect_to));
		
	});
	
}).post('/remove-incomplete',csrfProtection,AccessRules,function(req,res){
	Shipments.Model.findOne({_id:req.body._id,status:Shipments.PENDING},'redirect_to loading unloading',function(err,doc){
		if(err || !doc){
			req.session.setFlash('error',req.__('shipments').t_error_removing);
			return res.redirect(shipperUrl('users/dashboard'));
		}
	    doc.remove();
		req.session.setFlash('info',req.__('shipments').t_shipment_removed);
		return res.redirect(shipperUrl('users/dashboard'));
		
	});
}).get('/vehicles',csrfProtection,AccessRules,function(req,res){ Appage.title('Vehicles List');
    
																
	Trucks.Model.find({created_by:req.session.user.ID||false},function(err,doc){
	 
	 res.render('trucker/users/vehicles',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc});		
	});															
																
});
var myoffset,aoffset=0,hoffset=0;
router.get('/my-shipments',csrfProtection,AccessRules,function(req,res){ Appage.title('My Shipments');
	try{ myoffset = 0;aoffset=0,hoffset=0;																	
   Shipments.Model.find({trucker_id:req.session.user.ID,status:Shipments.ACCEPTED},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status vehicles shipper_id shipment_type.code shipment_detail.overall_total').populate('truck.ttype','name loading_capacity')
	.sort({'transit.t_expdatetime':1}).skip(myoffset).limit(Shipments.MYSHIPMENT_LIMIT).exec(function(err,doc){
	myoffset+=Shipments.MYSHIPMENT_LIMIT;	
	res.render('trucker/users/my_shipments',{extractScripts: true,csrfToken: req.csrfToken(),shipments:doc,Shipments:Shipments});   
   });
  }catch(e){ console.log(e.message);
    
  }
}).post('/upcoming',csrfProtection,AccessRules,function(req,res){ Appage.title('My Shipments');
	try{ 																	
   Shipments.Model.find({trucker_id:req.session.user.ID,status:Shipments.ACCEPTED,'transit.t_expdatetime':{$gt:DateTime.now()}},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status vehicles shipper_id shipment_type.code shipment_detail.overall_total').populate('truck.ttype','name loading_capacity')
	.sort({'transit.t_expdatetime':1}).skip(myoffset).limit(Shipments.MYSHIPMENT_LIMIT).exec(function(err,doc){
	myoffset+=Shipments.MYSHIPMENT_LIMIT;	
	res.render('trucker/users/upcoming_paginate',{layout: false,csrfToken: req.csrfToken(),shipments:doc,Shipments:Shipments});   
   });
  }catch(e){ console.log(e.message);
    
  }															 
}).post('/active',csrfProtection,AccessRules,function(req,res){ Appage.title('Active Shipments');
   	try{ 																	
   Shipments.Model.find({trucker_id:req.session.user.ID,status:Shipments.INTRANSIT},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status vehicles shipper_id shipment_type.code shipment_detail.overall_total').populate('truck.ttype','name loading_capacity')
	.sort({'transit.t_expdatetime':1}).skip(aoffset).limit(Shipments.MYSHIPMENT_LIMIT).exec(function(err,doc){
	if(doc)
	   aoffset+=Shipments.MYSHIPMENT_LIMIT;	
	res.render('trucker/users/active_paginate',{layout: false,csrfToken: req.csrfToken(),shipments:doc,Shipments:Shipments});   
   });
  }catch(e){ console.log(e.message);
    
  }	
}).post('/history',csrfProtection,AccessRules,function(req,res){ Appage.title('Shipments History');
   try{ var s = Shipments; 																	
    s.Model.find({trucker_id:req.session.user.ID,status:{$in:[s.SHIPPER_CANCELED,s.EXPIRED,s.COMPLETED,s.TRUCKER_CANCELED]}},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status vehicles shipper_id shipment_type.code shipment_detail.overall_total').populate('truck.ttype','name loading_capacity')
	.sort({'transit.t_expdatetime':1}).skip(hoffset).limit(Shipments.MYSHIPMENT_LIMIT).exec(function(err,doc){
	if(doc)
	   hoffset+=Shipments.MYSHIPMENT_LIMIT;	
	res.render('trucker/users/history_paginate',{layout: false,csrfToken: req.csrfToken(),shipments:doc,Shipments:Shipments});   
   });
  }catch(e){ console.log(e.message);
    
  } 

}).get('/membership',csrfProtection,AccessRules,function(req,res){ Appage.title('Membership Plans');   
   Membership.Model.find({status:Membership.ACTIVE},function(err,plans){
	  res.render('trucker/users/membership',{plans:plans}); 
   }); 																  
}).get('/upgrade-membership/:id',csrfProtection,AccessRules,function(req,res){
    Membership.Model.findOne({_id:req.params.id||false},function(err,plan){
	  if(err) return res.render('trucker/users/upgrade_plan',{extractScripts: true,err:err.message}); 
	 Users.Model.findOne({_id:req.session.user.ID},'contact business',function(err,user){
		if(err) return res.render('trucker/users/upgrade_plan',{extractScripts: true,err:err.message});
	  req.session.plan = plan;
	  var txtID = Date.now();	 
	  res.render('trucker/users/upgrade_plan',{extractScripts: true,plan:plan,
								user: user,
							  	csrfToken: req.csrfToken(),
							  	amount: plan.price,
							  	citrus: {
							  		txid: txtID,
							  		sign: Common.generateSignature(txtID,3),
							  		access: Const.citrus.ACCESS_KEY,
							  		vanity: Const.citrus.VANITY_URL,
							  		return_url: BASEURL+truckerUrl('users/activate-plan'),
							  		notify_url: '',//truckerUrl('users/activate-plan'),
							  		amount: 3,
							  	},
											  }); 
	 });
	});		
}).post('/activate-plan',AccessRules,function(req,res){
	if (!req.body && !req.body.signature)
	return res.render('trucker/users/success', {
		msg: req.__().e_invalid_request
	});

try {
	var post = req.body,msg;
	var data_string = post['TxId'] + post['TxStatus'] + post['amount'] + post['pgTxnNo'] + post['issuerRefNo'] + post['authIdCode'] + post['firstName'] + post['lastName'] + post['pgRespCode'] + post['addressZip'];
	var signature = crypto.createHmac('sha1', Const.citrus.SECRET_TOKEN).update(data_string).digest('hex');
	var plan = req.session.plan;
	if (signature == post['signature']) {
		delete req.session.plan;
	 var Transaction = Transactions.Load({
			amount: plan.price,
			payment_id: plan._id,
			status: {
				code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
				text: post['TxStatus'],
			},
			debug: post,
			ttype: {
				code: Transactions.SPENT,
				text: plan.name + " plan bought by trucker",
			},
			user_id: req.session.user.ID
		});			
		Transaction.save(function (err){
			if(err){
				return res.render('trucker/users/success', {extractScripts:true,
						msg: req.__('shipments').t_payment_successfully_completed_but_internal_error.replace('<transaction_id>',post['TxId'])
		});
			}else{
			   Users.Model.findOne({_id:req.session.user.ID},function(err,user){
				   var today = new Date();
				   user.membership['plan_code']   = plan.mtype.code;
				   user.membership['plan_type']   = plan.mtype.text;
				   user.membership['starts_from'] = today;
				   user.membership['transacions'] = parseInt(plan.transactions);
				   user.membership['expiry_date'] = new Date(today.setMonth(today.getMonth()+parseInt(plan.duration)));
				   user.save(function(err){
					   console.log(err);
					req.session['_user_'] = user;   
                   return res.render('trucker/users/success', {extractScripts:true,
		                        msg: req.__('shipments').t_payment_successfully_completed.replace('<transaction_id>',post['TxId'])
				   });
			    });
			 });		   
		   }
		});
	}else{
	    var Transaction = Transactions.Load({
				amount: req.session.plan.price,
				payment_id: req.session.plan._id,
				status: {
					code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
					text: post['TxStatus'],
				},
				debug: post,
				ttype: {
					code: Transactions.SPENT,
					text: "Trucker tried to buy "+req.session.plan.name,
				},
				user_id: req.session.user.ID
			});
			Transaction.save(function (err) {
				if(err){
					return res.render('trucker/users/success', {extractScripts:true,
		                        msg: req.__().t_payment_failed,
						        failed:1
	                });
				}else{
				    return res.render('trucker/users/success', {extractScripts:true,
		                        msg: req.__().t_payment_failed,
						        failed:1
	                });
				}
			});
	}
} catch (err2) {
	return res.render('trucker/users/success', {extractScripts:true,
		msg: err2.message,
		failed:1
	});
}
}).get('/notification-counter',AccessRules,function(req,res){
	WebNotifications.Model.count({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},function(err,c){
		res.json({count:c||0});
	});
}).post('/load-notifications',AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},'message read',function(err,rec){
		res.json({status:Const.HTTP_SUCCESS,n:rec});
	});
}).post('/webnoti-read',AccessRules,function(req,res){
	WebNotifications.Model.update({_id:req.body.id},{$push:{read:req.session.user.ID}},function(err,affected){
		if(affected)
		   res.json({status:Const.HTTP_SUCCESS});
	});
}).get('/change-password',csrfProtection,AccessRules,function(req,res){
	res.render('trucker/users/change_password',{csrfToken: req.csrfToken()});
}).post('/change-password',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID||false},function(err,user){
		if(err || !user) return res.json({status:Const.CUSTOM_SERVER_ERROR,display:{error:req.__('users').e_invalid_request_recordnot_found}});
		for(var i in req.body.Users)
		    user[i] = req.body.Users[i] || " ";
		user.validate(function(err){
			if(err)return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});		
		    user.password = MD5(req.body.Users.confirm_password);	        	
			user.save({validateBeforeSave:false},function(err){ console.log(err);
								
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_pasword_changed_successfully},form:'users'});
			})		
		}) 		
	});
}).get('/registerascompany/:id',csrfProtection,AccessRules,function(req,res){
	if(req.params.id!=req.session.user.ID)
	   return res.redirect(truckerUrl('users/edit-profile'));
	Users.Model.findOne({_id:req.session.user.ID},function(err,user){
		if(err){ return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});
	    }
		if(user.role == Users.INDIVIDUALTRUCKER){
			user.role=Users.TRUCKERCOMPANY;
			user.account_type=Users.TRUCKERCOMPANY;
		}
		user.save(function(err){ console.log(err)
		    req.session['_user_'] = user;
			return res.redirect(truckerUrl('users/edit-profile'));
		})
	});
}).post('/sendotp',csrfProtection,AccessRules,function(req,res){
	if(req.session.user.isOTPVerified)
		return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_otpalready_verified});
    Users.Model.findOne({_id:req.session.user.ID},function(err,user){
		if(err)
		   return res.json({status:Const.HTTP_SUCCESS,msg:err.message});
		if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		}
	});	
}).post('/resendotp',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
	  if(user.otp['verified']!=1){	
	   var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
		})
	  }else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
	  }
	});
}).post('/validate-otp',csrfProtection,AccessRules,function(req,res){
	try{	
	Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
		if(user && user.otp.verified!=1){
	        if(user.otp.validity<Date.now()){
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_otp_expired});
			}

		    if(user.otp.otp_no!=req.body.Users.otp)
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_invalidotp});
	        if(user.otp.otp_no==req.body.Users.otp){
				user.otp = {verified:1};
				user.save(function(err){
					req.session['_user_']= user; 
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
				     return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otp_verified});
					
		        })
			}	
				
	}
	});
	}catch(e){
		return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	}
	
}).post('/dashboard-stats',csrfProtection,AccessRules,function(req,res){

		res.json({status:Const.HTTP_SUCCESS,tc:truck_count||0,dc:driver_count||0,data:res1[0]||{}});
	
 
}).post('/trucker-eraned',csrfProtection,AccessRules,function(req,res){
	
   	Shipments.Model.find({truker_id:req.session.user.ID,status:Shipments.COMPLETED},'_id',function(err,shipment_ids){		
	Payments.Model.aggregate(
		{$match:{'shipment_id':{$in:shipment_ids}}},
		  {
        $group: {
            _id: null,
		earned:{$sum:'$total'},
		}},function(err,res1){
			res.json({status:Const.HTTP_SUCCESS,data:res1[0]||{earned:0}});
	});
  });
}).get('/notifications',csrfProtection,AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:new RegExp(req.session.user.ID,'i'),read:{$nin:[new RegExp(req.session.user.ID,'i')]}},'message',{$orderBy:{'created_date':-1}},function(err,rec){
	
		
		res.render('trucker/users/notifications',{extractScripts:true,rec:rec});
	});
}).get('/buy-transactions',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID},function(err,user){
	if(req.session.loadedShipment && req.session.loadedShipment._id)	
	req.session.membership = {redirect:truckerUrl('shipments/detail/'+req.session.loadedShipment._id)};
	else req.session.membership = {redirect:truckerUrl('shipments/regular-load-board')};	
	res.render('trucker/users/credits',{extractScripts:true,csrfToken:req.csrfToken(),creditprice:req.session.settings.creditprice||237});
	})
}).post('/buy-credits',csrfProtection,AccessRules,function(req,res){	
	Users.Model.findOne({_id:req.session.user.ID},function(err,user){
	var txtID = Date.now(),creditprice = req.session.settings.creditprice||237,sign=0,amount=0; // this will be change while migrating on live enviorment
	if(req.body.Credits && req.body.Credits['quantity']>0){
	  if((req.body.Credits['quantity']*creditprice)>=creditprice){
		 amount = req.body.Credits['quantity']*creditprice;  
		 sign   = Common.generateSignature(txtID,3) // amount will be changed while migration on live 
		 req.session.membership['quantity'] =req.body.Credits['quantity']; 
		 req.session.membership['amount']   =amount; 
	  }else{
		return res.json({status:Const.HTTP_BAD_REQUEST,html:req.__().t_credit_error})
	  }
	}else{
		return res.json({status:Const.HTTP_BAD_REQUEST,html:req.__().t_invalidcreditquantity})
	}	
	res.render('trucker/users/credit_billing',{extractScripts:true,csrfToken:req.csrfToken(),layout:false,user:user,citrus: {
							  		txid: txtID,
							  		sign: sign,
		                            access: Const.citrus.ACCESS_KEY,
		                            vanity: Const.citrus.VANITY_URL,   
							  		return_url: BASEURL+truckerUrl('users/buy-credits-payment'),
							  		notify_url: '',//truckerUrl('users/activate-plan'),
							  		amount: 3,
							  	},amount:3},function(err,html){
		return res.json({status:Const.HTTP_SUCCESS,html:html})
	});
	})
	
}).post('/buy-credits-payment',AccessRules,function(req,res){
	if (!req.body && !req.body.signature)
	return res.render('shipper/success', {
		msg: req.__().e_invalid_request
	});

try {
	var post = req.body,msg;
	var data_string = post['TxId'] + post['TxStatus'] + post['amount'] + post['pgTxnNo'] + post['issuerRefNo'] + post['authIdCode'] + post['firstName'] + post['lastName'] + post['pgRespCode'] + post['addressZip'];
	var signature = crypto.createHmac('sha1', Const.citrus.SECRET_TOKEN).update(data_string).digest('hex');
		var membership = req.session.membership;
	if (signature == post['signature'] && post['TxStatus'] == 'SUCCESS') {
	Users.Model.findOne({_id:req.session.user.ID},function(err,user){	
			if (post['TxStatus'] == 'SUCCESS') {			
            user.membership['transactions']+=membership['quantity'];
			user.save(function(err){
				if(err){ 
					req.session.setFlash('error',err.message);
					return res.redirect(membership['redirect']);
				}
				var Transaction = Transactions.Load({
				amount: membership['amount'],
				payment_id: user._id,
				status: {
					code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
					text: post['TxStatus'],
				},
				debug: post,
				ttype: {
					code: Transactions.SPENT,
					text: "Trucker bought "+ membership['quantity'] +" credits in RS "+membership['amount']
				},
				user_id: req.session.user.ID
			});
			delete req.session.membership;	
			Transaction.save(function (err) {
				if(err) req.session.setFlash('error',err.message);
				req.session['_user_'] = user;
				req.session.setFlash('success',req.__().t_credit_added_successfully);
				return res.redirect(membership['redirect']);
			});	
			});	
		}else return res.redirect(membership['redirect']);;	
			//});
	     });
		//});
	} else {
		
			var Transaction = Transactions.Load({
				amount: membership['amount'],
				payment_id: user._id,
				status: {
					code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
					text: post['TxStatus'],
				},
				debug: post,
				ttype: {
					code: Transactions.SPENT,
					text: "Trucker bought "+ membership['quantity'] +" credits "+membership['amount']
				},
				user_id: req.session.user.ID
			});
			Transaction.save(function (err) {
				if(err){ 
					req.session.setFlash('error',err.message);
					return res.redirect(membership['redirect']);
				}else{
					
					req.session.setFlash('error',post['TxMsg']);
					return res.redirect(membership['redirect']);
				
				}
			});
	}
} catch (err2) {
	 return res.redirect(truckerUrl('shipments/detail'+membership['shipment_id']));
}
});




//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isTrucker() && req.session.user._id){
	  if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(truckerUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}
	else res.redirect(webappUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

function genrateShipmentID(){
	var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var sid = shuffle(str.substring(0,12)+Date.now());
	return shuffle(str.substring(6,18));
	
}
function shuffle(string) {
    var parts = string.split('');
    for (var i = parts.length; i > 0;) {
        var random = parseInt(Math.random() * i);
        var temp = parts[--i];
        parts[i] = parts[random];
        parts[random] = temp;
    }
    return parts.join('');
}
// get logged in user info ...
function getLoggedUserInfo(req,key){
	return Common.getLoggedUserInfo(req,key);
}
module.exports = router;
