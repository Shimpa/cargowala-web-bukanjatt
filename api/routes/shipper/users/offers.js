"use strict";
var router = require('express').Router(),
    Users   = appModel('Users'),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true });
var Offers = appModel('Offers'),
    Users  = appModel('Users'),
	Users  = new Users(),
	Offers = new Offers();
var poffset;
router.get('/',AccessRules,csrfProtection,function(req,res){
	try{ poffset=0;
  	Offers.Model.find({role:Users.SHIPPER}).skip(poffset).limit(4).exec(function(err,rec){
	if(!err) poffset+=4;   
	res.render('shipper/users/offers',{extractScripts:true,offers:rec,csrfToken:req.csrfToken()});
	})
	}catch(e){
		res.redirect(shipperUrl('users/dashboard'));
	}
});

//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isShipper() && req.session.user._id){
	   if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(shipperUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}

module.exports = router;
