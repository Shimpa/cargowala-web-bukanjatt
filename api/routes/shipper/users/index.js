"use strict";
var router = require('express').Router(),
    User    = appHelper('User'),
    Notify  = appHelper('Notify'),
    Users   = appModel('Users'),
    WebNotifications   = appModel('WebNotifications'),
	//Notify   = new Notify(),
	Users   = new Users();
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true }),
TruckLanes  = appModel('TruckLanes'),
TruckLanes  = new TruckLanes(),
Shipments   = appModel('Shipments'),
Shipments   = new Shipments(),
WebNotifications   = new WebNotifications();
var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB
	};
router.get('/dashboard',AccessRules,csrfProtection,function(req,res){ Appage.title('Dashboard');
									 
  delete req.session.register; // remove registration session. to set user completed profile	
  res.clearCookie('dashboard');	
  res.clearCookie('step2');	
  res.clearCookie('step3');
  res.render('shipper/users/dashboard',{title:'Dashboard',extractScripts: true,csrfToken: req.csrfToken(),otpverified:req.session.user.isMobileVerified,otpsent:req.session.user.isOTPSent});
}).get('/edit-profile',csrfProtection,AccessRules,function(req,res){
	//req.session.user._id
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
		var error = false,otpsent;
		if(err || !user) error = 1;	   	
		if(user.otp['validity'])
			otpsent = user.otp['validity'].getTime()>Date.now();
	    res.render('shipper/users/edit_profile',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user,image:images,Model:Users,otpverified:req.session.user.isMobileVerified,otpsent:otpsent});
	});
}).post('/update-mobile',csrfProtection,AccessRules,function(req,res){
	if(Validate.isNaN(req.body.Users.mobile,10,10))
	   return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__().e_enteralidmobileno});	
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
	  if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	  if(user){
		 user.contact['mobile_number']  = req.body.Users.mobile;
		 user.save(function(err){
			if(err) 
			return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
			if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_'] = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_successfullyupdated});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		} 
			 
			
			 
		 }) 
	  }	
	});
}).post('/edit-profile',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
		user.update = 67; // to identify it is update case...	
		var old_mobileno = user.contact['mobile_number'];
		var data = {
		name:{
			firstname:req.body.Users.firstname,
			lastname:req.body.Users.lastname,
		},
		contact:{
		      mobile_number:req.body.Users.mobile_number,
			  email:user.contact.email,
		},
	    },imagename = user.image;
		//delete req.body.Users.oimage;
		delete req.body.Users.status;
		delete req.body.Users.password;
		delete req.body.Users._id;
		delete req.body.Users.role;
		delete req.body.Users.hash_token;
		delete req.body.Users.plocation;
		
		for(var i in req.body.Users)
			user[i] = req.body.Users[i];
			
		for(var i in data)
		    user[i]  = data[i];		
        var business  = {};; 
		for(var i in user.business)
			business[i] = user.business[i];
		business['service_taxno'] =  req.body.Users.service_taxno||null;		
		user.business = business;
	   user.save(function(err){
		   if(err) return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});
		    var imagename = imagename || Date.now();
		    UploadImage.allowedExtensions = images.extensions;
			UploadImage.allowedSize       = images.maxsize;
			UploadImage.setFilename(imagename);
			if(req.body.Users.image){
		   var fn =UploadImage.base64(req.body.Users.image).upload(Common.media_dir_path(user._id,Users));	
			user.isvalidated=1;
		   //console.log(fn);
			user.image = fn;   
			}else user.image = req.body.Users.oimage;
		    if(old_mobileno!=user.contact.mobile_number){	
	   var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['verified'] = 0;
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;					
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_profile_updatedotp_sent},form:'register',next:shipperUrl('users/dashboard')}) 			
		        })
	  }else{
			 return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_profile_updated},form:'register'});    		
	  }
	
	   })
	});
}).get('/business-detail',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
		var error = false;
		if(err || !user) error = 1;	   	
	    res.render('shipper/users/edit_business_detail',{extractScripts: true,csrfToken: req.csrfToken(),error:1,user:user});
	});

}).post('/business-detail',csrfProtection,AccessRules,function(req,res){
	
 	Users.Model.findOne({_id:req.session.user._id||false,status:Users.ACTIVE},function(err,user){
	   if(err || !user) return res.json({status:Const.HTTP_BAD_REQUEST,html:err.massege,form:'users'});
	   delete req.body.Users.oimage;
	   delete req.body.Users.statu;
	   delete req.body.Users.password;
	   delete req.body.Users._id;
	   delete req.body.Users.role;
	   delete req.body.Users.hash_token;
	   delete req.body.Users.plocation;
	   var btype =  req.body.Users.business_type,
		   ctype = 	req.body.Users.company_type,
		   b_type=  Users.businessType(),c_type = Users.companyType();
	   delete req.body.Users.business_type;
	   delete req.body.Users.company_type;
	  for(var i in req.body.Users.business)
		  user.business[i] = req.body.Users.business[i]||" ";
	  user.business_type['btype'] =  btype;
	  user.business_type['text']  =  b_type[btype];
	  user.company_type['ctype']  =  ctype;
	  user.company_type['text']   =  c_type[ctype];
	  user.business['officeaddresssameasbusiness'] =  req.body.Users.business.officeaddresssameasbusiness;
	  if(req.body.registerascompany && user.role==Users.INDIVIDUALSHIPPER){
		  user.role = Users.SHIPPERCOMPANY;
	  } 	
		
		user.save(function(err){
			if(err){
			var errr=[],i;	
			for(i in err.errors)
		        errr.push(err.errors[i].message);
			return res.json({status:Const.HTTP_BAD_REQUEST,display:{error:errr.shift()},errors:!!err.errors,form:'users'});
		    }else
			 return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__().t_business_updated},form:'users'});
			 	
		});
	});

});
router.post('/booking-location',AccessRules,csrfProtection,function(req,res){
  var pickup_date= req.body.Bid['loading-date']  || null,
	  pickup_time= req.body.Bid['loading-time']  || null;
	  if(pickup_date && pickup_time){
		 var dt  = pickup_date.split("/"),tm = pickup_time.split(":");
	     var bidtime    = new DateTime(dt[2]+" "+dt[1]+" "+dt[0]+" "+tm[0]+":"+tm[1]+":"+tm[2]).getTime(),
			 next2Hours = 1000*60*60*2;
	         if(bidtime<DateTime.now()+next2Hours){
				return res.json({status:Const.HTTP_BAD_REQUEST,html:req.__().e_invalid_datetime}); 
			 }
	  }
	
	var t_date = req.body.Bid['loading-date'].split('/');
	var transit_date =  new Date(t_date[2],t_date[1]-1,t_date[0]);
	    transit_date.setHours(5,29,10);
var sipid =genrateShipmentID(),Shipment=Shipments.Load({
		    loading:{
				lp_address:req.body.Bid['loading']!=""?req.body.Bid['loading']:req.body.Bid['custom-loading'],
				lp_city:req.body.Bid['loading-city']   || null,
				lp_state:req.body.Bid['loading-state'] || null,
		    },
		    unloading:{
				up_address:req.body.Bid['unloading']!=""?req.body.Bid['unloading']:req.body.Bid['custom-unloading'],	  
				up_city:req.body.Bid['unloading-city']   || null,	  
				up_state:req.body.Bid['unloading-state'] || null,	  
		    },
		    transit:{
				t_date:req.body.Bid['loading-date'] || null,
				t_time:req.body.Bid['loading-time'] || null,
				t_expdatetime:bidtime,
				date:transit_date,
		    },
	        status:Shipments.INCOMPLETE,
            web:1,
	        shipper_id :req.session.user.ID,
	        shipment_id:sipid,	    
		    redirect_to:'truck-load-type#step2', 
	});
var error =Shipment.validateSync();
if(error && error.errors!=undefined){ console.log(error);
	  return res.json({status:Const.HTTP_BAD_REQUEST,html:error.errors['loading.address'].message});
}
TruckLanes.Model.findOne({'source':Shipment.loading.lp_city,'source_state':Shipment.loading.lp_state,'destination':Shipment.unloading.up_city,'destination_state':Shipment.unloading.up_state},'distance price_per_km',function(err,dta){
    if(dta && dta.price_per_km && dta.distance){
		Shipment.transit['est_distance']  = dta.distance;
	   if('shipment' in req.session && req.session.shipment._id){		
	       Shipments.Model.findOne({_id:req.session.shipment._id || false},function(err,OpenShipment){
				OpenShipment.unloading.up_address = Shipment.unloading.up_address;
				OpenShipment.loading.lp_address   = Shipment.loading.lp_address;
				OpenShipment.unloading.up_city = Shipment.unloading.up_city;
				OpenShipment.loading.lp_city   = Shipment.loading.lp_city;
				OpenShipment.unloading.up_state= Shipment.unloading.up_state;
				OpenShipment.loading.lp_state  = Shipment.loading.lp_state;
				OpenShipment.transit.t_date    = Shipment.transit.t_date;
				OpenShipment.transit.t_time    = Shipment.transit.t_time;
				OpenShipment.transit['est_distance']  =  dta.distance;
				OpenShipment = saveShipmentDetail(OpenShipment,Shipment,dta.price_per_km,1); 
                OpenShipment.save(function(err,shipp){
	              if(err) return res.json({status:Const.HTTP_BAD_REQUEST,html:err.errors['loading.address'].message});
	              else{
		          req.session.shipment ={_id:OpenShipment._id,
									  loading  :OpenShipment.loading.lp_address, 
									  unloading:OpenShipment.unloading.up_address 
									 };	
		          setCookies(res,OpenShipment);
		          return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/truck-load-type')})	
	              }
                });	
            });	
	   }else{
		Shipment.transit['est_distance']  =  dta.distance;
		Shipment =saveShipmentDetail(Shipment,Shipment,dta.price_per_km,1);  
		Shipment.save(function(err){
			 if(err){ //console.log(err);
			  return res.json({status:Const.HTTP_BAD_REQUEST,html:error.errors.message});	
			}else{
			   req.session.shipment ={_id:Shipment._id,
									  loading  :Shipment.loading.lp_address, 
									  unloading:Shipment.unloading.up_address 
									 };	
				setCookies(res,Shipment); // browser back button does not load page data so saved in cookies and loaded thriugh frontend javascript...
		       return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/truck-load-type')})	
			}
		}); 
      }
	}else{
		var map = require('google_directions');
        var params = {
    // REQUIRED
            origin: Shipment.loading.lp_address,
            destination: Shipment.unloading.up_address,
            key: 'AIzaSyCWpDytcN-LWndoxERaYu31eB400w4TVYo',

    // OPTIONAL
            mode: "",
            avoid: "",
            language: "en",
            units: "",
            region: "in",
        };		
        map.getDirections(params, function (err, data){
           if (err) {
                 //console.log(err);
                 return 1;
           }
           if(data.routes[0]!=undefined){
			Shipment =saveShipmentDetail(Shipment,Shipment,req.session.settings.lane_rate||0);   
			Shipment.save(function(err){
			    if(err){ //console.log(err);
			        return res.json({status:Const.HTTP_BAD_REQUEST,html:error.errors.message});	
			    }else{
			        req.session.shipment ={_id:Shipment._id,
									  loading  :Shipment.loading.lp_address, 
									  unloading:Shipment.unloading.up_address 
									 };
					setCookies(res,Shipment);
		            return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/truck-load-type')})	
			    }
		    });
		   }else res.json({status:Const.HTTP_BAD_REQUEST,error:_locals().invalid_route});	
        });
	}
  });
}).get('/incomplete-shipment',csrfProtection,AccessRules,function(req,res){ Appage.title('Incomplete Shipment');
	Shipments.Model.findOne({status:Shipments.INCOMPLETE,web:1,shipper_id:req.session.user.ID},{},{'sort':{_id:-1}},function(err,doc){
          if(err) return res.redirect(shipperUrl('users/dashboard'));
		var ship_dtl={};
	        if(doc){  
	         for(var i in doc.shipment_detail)
			     ship_dtl[i] = doc.shipment_detail[i];
		         ship_dtl['_id'] = doc._id;
			}else ship_dtl =null;
	             res.render('shipper/users/incomplete-shipment',{extractScripts: true,csrfToken: req.csrfToken(),doc:ship_dtl});	
	});
}).post('/complete-shipment',csrfProtection,AccessRules,function(req,res){
	try{
	Shipments.Model.findOne({_id:req.body._id,status:Shipments.INCOMPLETE},'redirect_to shipment_detail.redirect_to loading unloading',function(err,doc){
		if(err || !doc) return res.redirect(shipperUrl('users/dashboard')); 
	    req.session.shipment = {
	    	_id:doc._id,
			loading:doc.loading.lp_address,
			unloading:doc.unloading.up_address,
			incomplete:1,			
	    };
		res.redirect(shipperUrl(doc.redirect_to,function(url){
			if(url=="dashboard") return "users/dashboard";
			else return "shipments/"+url;
		}));
		
	});
	}catch(e){ 
		
		res.redirect(shipperUrl('users/dashboard'));
	}
	
}).post('/remove-incomplete',csrfProtection,AccessRules,function(req,res){
	Shipments.Model.findOne({_id:req.body._id,status:Shipments.INCOMPLETE,shipper_id:req.session.user.ID},function(err,doc){
		if(err || !doc){
			req.session.setFlash('error',req.__('shipments').t_error_removing);
			return res.redirect(shipperUrl('users/dashboard'));
		}
	    doc.remove();
		req.session.setFlash('info',req.__('shipments').t_shipment_removed);
		return res.redirect(shipperUrl('users/dashboard'));
		
	});
})
var aoffset,hoffset,limit=2;
router.get('/my-shipments',csrfProtection,AccessRules,function(req,res){
	var s= Shipments;hoffset=0;aoffset=0;
	s.Model.find({$or:[{status:{$in:[s.INTRANSIT,s.BID_ACCEPTED,s.ACCEPTED,s.TRUCKER_CANCELED]}},{'shipment_type.code':s.REGULAR_BOARD,status:{$in:[s.INTRANSIT,s.BID_ACCEPTED,s.ACCEPTED,s.PENDING,s.TRUCKER_CANCELED]}}],shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status',{'sort':{'transit.t_expdatetimes':1,modified_on:-1}})
	.populate('truck.ttype','name loading_capacity').skip(aoffset).limit(limit||s.LIMIT)
	.exec(function(err,doc){ 
		aoffset +=s.LIMIT;
		limit=undefined;
	      res.render('shipper/users/shipments',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc,Shipments:Shipments,class4:false});
	});
}).post('/active-shipments',csrfProtection,AccessRules,function(req,res){
	var s= Shipments;
	s.Model.find({$or:[{status:{$in:[s.INTRANSIT,s.BID_ACCEPTED,s.ACCEPTED,s.TRUCKER_CANCELED]}},{'shipment_type.code':s.REGULAR_BOARD,status:{$in:[s.INTRANSIT,s.BID_ACCEPTED,s.ACCEPTED,s.PENDING,s.TRUCKER_CANCELED]}}],shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status',{'sort':{'transit.t_expdatetimes':1,modified_on:-1}})
	.populate('truck.ttype','name loading_capacity').skip(aoffset).limit(limit||s.LIMIT)
	.exec(function(err,doc){ 
		aoffset +=s.LIMIT;
	      res.render('shipper/users/paginate_active',{layout:false,csrfToken: req.csrfToken(),doc:doc,Shipments:Shipments,class4:false});
	});
}).post('/history-shipments',csrfProtection,AccessRules,function(req,res){
	var s= Shipments;
	s.Model.find({status:{$in:[s.EXPIRED,s.COMPLETED,s.SHIPPER_CANCELED]},shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time shipment_id status',{'sort':{modified_on:-1}})
	.populate('truck.ttype','name loading_capacity').skip(hoffset).limit(s.LIMIT)
	.exec(function(err,doc){ 
		hoffset +=s.LIMIT;
	      res.render('shipper/users/paginate-active',{layout:false,csrfToken: req.csrfToken(),doc:doc,Shipments:Shipments,class4:true});
	});
}).get('/notification-counter',AccessRules,function(req,res){
	WebNotifications.Model.count({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},function(err,c){
		res.json({count:c||0});
	});
}).post('/load-notifications',AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:{$in:[req.session.user.ID],read:{$nin:[req.session.user.ID]}}},'message read',function(err,rec){
		res.json({status:Const.HTTP_SUCCESS,n:rec});
	});
}).post('/webnoti-read',AccessRules,function(req,res){
	WebNotifications.Model.update({_id:req.body.id},{$push:{read:req.session.user.ID}},function(err,affected){
		if(affected)
		   res.json({status:Const.HTTP_SUCCESS});
	});
}).get('/change-password',csrfProtection,AccessRules,function(req,res){
	res.render('shipper/users/change_password',{csrfToken: req.csrfToken()});
}).post('/change-password',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID||false},function(err,user){
		if(err || !user) return res.json({status:Const.HTTP_BAD_REQUEST,display:{error:req.__('users').e_invalid_request_recordnot_found}});
		for(var i in req.body.Users)
		    user[i] = req.body.Users[i] || " ";
		user.validate(function(err){
			if(err)return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'users'});		
		    user.password = MD5(req.body.Users.confirm_password);	        	
			user.save({validateBeforeSave:false},function(err){ console.log(err);
								
				return res.json({status:Const.HTTP_SUCCESS,display:{success:req.__('users').t_pasword_changed_successfully},form:'users'});
			})		
		}) 		
	});
}).post('/sendotp',csrfProtection,AccessRules,function(req,res){
	if(req.session.user.isOTPVerified)
		return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_otpalready_verified});
    Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
		if(err)
		   return res.json({status:Const.HTTP_SUCCESS,msg:err.message});
		if(user.otp['verified']!=1){
			if(!user.otp['validity'] || user.otp['validity']<DateTime.now()){
				var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
				})
			}else{
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_sent});
			}		   	
		}else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
		}
	});	
}).post('/resendotp',csrfProtection,AccessRules,function(req,res){
	Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
	  if(user.otp['verified']!=1){	
	   var mno = String(user.contact['mobile_number']).slice(-4),
					otp = Math.floor(Math.random()*99999+100000),now;
					now = new Date();
				var message = "One Time Password for Cargowala Registration";
				    message+= " on XXX XXX " + mno;
				    message+= " is " + otp + " This can be used only once and is valid for next 4 hours from " + now;
				user.otp['otp_for'] = user.contact['mobile_number'];
				user.otp['otp_no'] = otp
				user.otp['otp_type'] = 'registration';
				user.otp['validity'] = new Date(now.setHours(now.getHours()+4));
				user.save(function(err){
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
					Notify.sendSMS(user.contact['mobile_number'],message,1);
					req.session['_user_']  = user;
				return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_verification_otp_sent});
					
		})
	  }else{
			return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otpalready_verified});		
	  }
	});
}).post('/validate-otp',csrfProtection,AccessRules,function(req,res){
	try{	
	Users.Model.findOne({_id:req.session.user.ID},'otp contact',function(err,user){
		if(user && user.otp.verified!=1){
	        if(user.otp.validity<Date.now()){
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_otp_expired});
			}

		    if(user.otp.otp_no!=req.body.Users.otp)
				return res.json({status:Const.HTTP_BAD_REQUEST,msg:req.__('users').t_invalidotp});
	        if(user.otp.otp_no==req.body.Users.otp){
				user.otp = {verified:1};
				user.save(function(err){
					req.session['_user_']= user; 
				    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
				     return res.json({status:Const.HTTP_SUCCESS,msg:req.__('users').t_otp_verified});
					
		        })
			}	
				
	}
	});
	}catch(e){
		return res.json({status:Const.HTTP_BAD_REQUEST,msg:err.message});
	}
	
	}).post('/remindemelater',csrfProtection,AccessRules,function(req,res){
	//remind you after 5 hours
	res.cookie('_vmltr_',1,{maxAge:1000*60*60*5, path: '/', httpOnly: true })
	   .json({status:Const.HTTP_SUCCESS});
}).get('/notifications',csrfProtection,AccessRules,function(req,res){
	WebNotifications.Model.find({can_view:new RegExp(req.session.user.ID,'i'),read:{$nin:[new RegExp(req.session.user.ID,'i')]}},'message',{$orderBy:{'created_date':-1}},function(err,rec){	
		
		res.render('shipper/users/notifications',{extractScripts:true,rec:rec});
	});
});


function saveShipmentDetail(Obj,Shipment,ppk,lexst){
	Obj.shipment_detail   = {  
		shipment_id:Shipment.shipment_id,
		from_city:Shipment.loading.lp_city, 
		from_state:Shipment.loading.lp_state, 
		from_address:Shipment.loading.lp_address, 
		to_city:Shipment.unloading.up_city, 
		to_state:Shipment.unloading.up_state, 
		to_address:Shipment.unloading.up_address,
		transit_date:Shipment.transit.t_date,
		transit_time:Shipment.transit.t_time, 
		est_distance:Shipment.transit.est_distance?Shipment.transit.est_distance:null, 
		per_ton_price:ppk, 
		lane_exist:lexst||0, 
	 };
	return Obj;
}
//access rules...
function AccessRules(req,res,next){ 
	try{
	if(req.session.user.isShipper() && req.session.user._id){console.log(req.session.user.redirectTo);
	   if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(shipperUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));
	}else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
function setCookies(res,data){
	//res.clearCookie('step2');
	//res.clearCookie('step3');
	res.cookie('dashboard',{'loading-city':data.loading.lp_city,
							'loading-state':data.loading.lp_state,
							'unloading-city':data.unloading.up_city,
							'unloading-state':data.unloading.up_state,
							'bid-loading':data.loading.lp_address,
							'bid-unloading':data.unloading.up_address,
							'bid-loading-date':data.transit.t_date,
							'bid-loading-time':data.transit.t_time,
		
	},10*60*1000);
}
function genrateShipmentID(){
	var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var sid = shuffle(str.substring(0,12)+Date.now());
	return shuffle(str.substring(6,18));
	
}
function shuffle(string) {
    var parts = string.split('');
    for (var i = parts.length; i > 0;) {
        var random = parseInt(Math.random() * i);
        var temp = parts[--i];
        parts[i] = parts[random];
        parts[random] = temp;
    }
    return parts.join('');
}
module.exports = router;
