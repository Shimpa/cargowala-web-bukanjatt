var router = require('express').Router(),
    csrf = require('csurf'),
    csrfProtection = csrf({ cookie: true });
var db      = require('mongoose');
var Bids   = appModel('Bids'),
	Users   = appModel('Users'),
	Users   = new Users(),
	Bids   = new Bids;
router.post('/isbid-accedpted',csrfProtection,AccessRules,function(req,res){
	try{
	var sid = req.body?req.body.sid:null;
	if(!sid) return res.json({status:Const.HTTP_SUCCESS,data:{sid:sid,t:null}});
	Bids.Model.findOne({shipment_id:sid,status:Bids.ACCEPTED},'trucker_id').populate('trucker_id','name contact.mobile_number rating').exec(function(err,doc){
		if(doc) return res.json({status:Const.HTTP_SUCCESS,data:{sid:req.body.sid,t:doc}});
		   return res.json({status:Const.HTTP_SUCCESS,data:{sid:req.body.sid,t:null}}); 
	});
	}catch(e){}
}).post('/bids-count',csrfProtection,AccessRules,function(req,res){
	try{
	if(req.body._id_.length<=0)return;
	var ids = req.body._id_.map(function(val){
		return db.Types.ObjectId(val);
	})
	Bids.Model.aggregate([
		{$match:{
			$and:[
			  {shipment_id:{$in:ids}},
			  {status:Bids.POSTED}
			]
		   }		   
		},
		{$group:{
			_id:"$shipment_id",
			count:{$sum:1}
		   }
		}],function(err,doc){
		   var dta = req.body._id_.map(function(val){
				var ship  = {};			   
				for(var i in doc){
				  if(val==doc[i]._id)
				    return doc[i]; 
				}			
			    ship._id  = val;
			    ship.count=0;
		        return ship;				   
	        });	
		return res.json({status:Const.HTTP_SUCCESS,d:dta})
	  });
		
	}catch(e){}
}).post('/get-bids',csrfProtection,AccessRules,function(req,res){
	try{
	Bids.Model.find({shipment_id:req.body.sid,status:Bids.POSTED},'price description trucker_id shipment_id')
		.populate('trucker_id','business.name business.registered_city rating')
	    .exec(function(err,doc){
		res.json({status:Const.HTTP_SUCCESS,d:doc});
	})
	}catch(e){}
}).post('/bid-status',csrfProtection,AccessRules,function(req,res){
    var Shipments  = appModel('Shipments');
    var Shipments  = new Shipments();			
	Shipments.Model; 	Bids.Model.findOne({_id:req.body._id_,shipment_id:req.body._s_,status:Bids.POSTED})
		.populate('shipment_id','shipper_id shipment_id transit.t_date transit.t_time')
		.populate('trucker_id','contact device_token name').exec(function(err,doc){ 
		   if(!doc || req.session.user.ID!=doc.shipment_id.shipper_id || err)
		     return res.json({status:Const.HTTP_UNAUTHORIZED,html:req.__('users').t_unautorizedaccess})
		console.log(parseInt(req.body._m_)==1);	 
		if(parseInt(req.body._m_)==1){
			req.session.shipment = {
				_id:doc.shipment_id._id,
				bid_id:req.body._id_
			};
			return res.json({status:Const.HTTP_SUCCESS,dta:{id:req.body._id_,sid:req.body._s_,next:1,msg:req.__('shipments').t_complete_payment_process,a:1}});
	    }else{	
			 
		   doc.status = Bids.REJECTED;
		   doc.save(function(err){
			   if(err) 
				 return res.json({status:Const.HTTP_UNAUTHORIZED,html:req.__('users').t_tryaftersometime});	
			   var link = '<a href='+truckerUrl('trucker/shipments/detail/'+doc.shipment_id._id)+'>';
			         link+= doc.shipment_id.shipper_id+'</a>';
			   var name = doc.trucker_id.name.firstname +" "+doc.trucker_id.name.lastname,
				   shipper_name = req.session.user.getName();
			   if(doc.status==Bids.ACCEPTED){
				  var shipper_dta = {
					   email:req.session.user.getEmail(), 
					   mobile_no:req.session.user.getMobileNo() 
				  };  
				  Bids.notifyShipper(shipper_dta,Common.replaceValue(req.__('bids').t_bid_accepted_notify_shipper,
					['<<trucker_name>>','<<shipper_name>>','<<amount>>','<<order_ref_number>>','<<date>>','<<time>>','<<support_no>>'],
				  [name,shipper_name,doc.price,link,doc.shipment_id.transit.t_date,doc.shipment_id.transit.t_time,
								   Const.SUPPORT_NO]										 
				  )); 
			      Bids.bidAccepted(doc,Common.replaceValue(req.__('bids').t_bid_accepted,
				  ['<<trucker_name>>','<<shipper_name>>','<<amount>>','<<order_ref_number>>','<<date>>','<<time>>','<<support_no>>'],
				  [name,shipper_name,doc.price,link,doc.shipment_id.transit.t_date,doc.shipment_id.transit.t_time,
								   Const.SUPPORT_NO]));
				  Shipments.bidAccepted(doc.shipment_id._id,doc.trucker_id._id);
				return res.json({status:Const.HTTP_SUCCESS,dta:{id:req.body._id_,sid:req.body._s_,msg:req.__('shipments').t_successfullyperofrmed,next:shipperUrl('users/my-shipments')}})   
		    }else if(doc.status==Bids.REJECTED){
				
				Users.Model.findOne({_id:doc.trucker_id},function(err,user){
					if(user.membership.transactions)
						   user.membership.transactions +=1;
					user.save(function(err){
							req.session['_user_']= user;
					});
				})
				  Bids.bidRejected(doc,Common.replaceValue(req.__('bids').t_bid_rejected,
				  ['<<trucker_name>>','<<order_ref_number>>'],[name,link]));
				return res.json({status:Const.HTTP_SUCCESS,dta:{id:req.body._id_,sid:req.body._s_,msg:req.__('shipments').t_successfullyperofrmed}})
			}
			
		})
	   }
	})
});


//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isShipper() && req.session.user._id){
	   if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(shipperUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
module.exports= router;