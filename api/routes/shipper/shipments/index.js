var router = require('express').Router();
var Categories  = appModel('Categories'),
	Categories  = new Categories();
var Shipments   = appModel('Shipments'),
	Shipments   = new Shipments,
	worker = appHelper('Worker'),
	re     = appHelper('RegularExp'),
	TruckLanes  = appModel('TruckLanes'),
    TruckLanes  = new TruckLanes(),
	Taxes       =appModel('Taxes'),
	Users       =appModel('Users'),
	Bids        =appModel('Bids'),
	Payments    =appModel('Payments'),
	Transactions    =appModel('Transactions'),
	Taxes       =new Taxes,
	Bids        =new Bids,
	Users       =new Users,
	Transactions=new Transactions,
	Payments    =new Payments;
var crypto = require('crypto'); 
var randomString = require('random-string');
var csrf = require('csurf'),
    csrfProtection = csrf({ cookie: true });
	var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':2.5 // size in MB
	},shipment_items={},shipper_id;
//shipper upcoming shipments...
/*router.get('/:u?/:l?',csrfProtection,AccessRules,function(req,res){
	try{
	shipper_id = req.session.user.ID;	
    getMyShipments([6],function(err,docs){ 
	  res.render('shipper/shipments/my_shipments',{extractScripts:true,csrfToken:req.csrfToken(),'shipments':docs});  
    },req.params.u,req.params.l);	
	}catch(e){console.log(e.message);
		req.session.setFlash('error',e.message);
		res.redirect(shipperUrl('users/dashboard'))
	}
})*/
router.get('/truck-load-type',csrfProtection,AccessRules,function(req, res) {
	try{
	Shipments.Model.findOne({_id:req.session.shipment._id || false},function(err,shipment){
		var google =worker.fork('Google');
		   google.on('message',function(data){ 
              var data = JSON.parse(data);
			  if(data.legs!=undefined){ 
				 var legs  = data.legs[0];
				  //var goog =  data.routes[0].  
			    shipment.loading.lp_latitude = shipment.loading.lp_latitude || legs.start_location.lat;   
			    shipment.loading.lp_longitude = shipment.loading.lp_longitude || legs.start_location.lng;   
			    shipment.unloading.up_latitude = shipment.unloading.up_latitude || legs.end_location.lat;   
			    shipment.unloading.up_longitude = shipment.unloading.up_longitude || legs.end_location.lng;   
			    shipment.transit.est_distance = shipment.transit.est_distance || Math.ceil(legs.distance['value']/1000);   
			    shipment.transit.est_time = shipment.transit.est_time || legs.duration['text'];				  
			    shipment.save();
			  }
		   })
		   google.send({lp_address:req.session.shipment.loading,up_address:req.session.shipment.unloading});
	Common.isShipmentExpired(shipment.transit);	
    res.render('shipper/shipments/truckloadtype',{extractScripts: true,csrfToken:req.csrfToken(),'shipment':shipment,isShipExpired:Common.isShipmentExpired(shipment.transit)});	
	});
	}catch(e){
		res.redirect(shipperUrl('users/dashboard'));
	}
}).post('/changebid-date-time',csrfProtection,AccessRules,function(req,res){
	var pickup_date= req.body.Bid['loading-date']  || null,
	    pickup_time= req.body.Bid['loading-time']  || null;
	    if(pickup_date && pickup_time){
		   var dt  = pickup_date.split("/"),tm = pickup_time.split(":");
	       var bidtime    = new DateTime(dt[2]+" "+dt[1]+" "+dt[0]+" "+tm[0]+":"+tm[1]+":"+tm[2]).getTime(),
			   next2Hours = 1000*60*60*2;
	           if(bidtime<DateTime.now()+next2Hours){
				return res.json({status:Const.HTTP_BAD_REQUEST,html:req.__('users').e_invalid_datetime}); 
			 }
		Shipments.Model.findOne({_id:req.session.shipment._id || false},function(err,shipment){
		    if(err)return res.json({status:Const.HTTP_BAD_REQUEST,html:err.message});
			shipment.transit['t_date'] = pickup_date;
			shipment.transit['t_time'] = pickup_time;
			shipment.transit['t_expdatetime'] = bidtime;
			shipment.save(function(err){ console.log(err);
				if(err)return res.json({status:Const.HTTP_BAD_REQUEST,html:err.message});
				return
			    //Shipments.notifyDateTimeChanged(req.session.shipment._id);	res.json({status:Const.HTTP_SUCCESS,html:req.__().t_datetime_changed_successfully});
			});			
		});
	  }	
}).post('/load-type',csrfProtection,AccessRules,function(req,res){
	try{
	    var body = req.body.shipments,errors = {},load_type = body['truck']?JSON.parse(body['truck']):{};
	    if(Validate.isEmpty(body['truck-type']))
		  errors['truck.category'] = {message:req.__().e_truck_category};	
		if(Validate.isEmpty(load_type._id))
		  errors['truck.ttype'] = {message:req.__().e_truck_type};	
		if(Validate.isEmpty(body['delivery-type']))
		  errors['truck.load_type'] = {message:req.__().e_truck_loadtype};
	    if(isNaN(parseInt(body['weight'])) || parseInt(body['weight']||0)<=0)
		  errors['shipments_weight'] = {message:req.__().e_total_weight};
		if(body['delivery-type']==1){
		  if(parseInt(body['quantity'])<=0)
		     errors['truck.quantity'] = {message:req.__().e_truck_quantity};	
		   if(!body['priority_delivery'] in {0:"priority off",1:"priority on"})
		      errors['truck.priority_delivery'] = {message:req.__().e_truck_priority_delivery};	
		}
	if(Object.keys(errors).length>0)return res.json({status:Const.HTTP_BAD_REQUEST,errors:errors,form:'truck'});
	Shipments.Model.findOne({_id:req.session.shipment._id || false},function(err,shipmentt){
	  if(err)return res.json({status:Const.HTTP_BAD_REQUEST,errors:{}});
		var body = req.body.shipments,
			load_type = body['truck']?JSON.parse(body['truck']):{},
			vali = {category:body['truck-type']||null,
				    load_type:body['delivery-type']||null,
				    ttype:load_type._id||null,				    
				    redirect_to:'items-detail#step3',
				   };
	  shipmentt.truck ={};
	  shipmentt.save(function(err){
		  console.log(err);
	  });	

	  for(var k in vali)
	        shipmentt.truck[k]=vali[k];
      vali =null;
	  shipmentt.shipment_weight = body['weight'];			    
	
	  if(shipmentt.truck.quantity!=undefined)	
	     delete shipmentt.truck.quantity;	
	  if(shipmentt.truck.priority_delivery!=undefined)	
	     delete shipmentt.truck.priority_delivery;	
		
		shipmentt.truck.quantity = body.quantity || 1;
	  if(body['delivery-type']==1){
		shipmentt.truck['priority_delivery'] = body.priority_delivery=='on'?1:0;
	  }
	//req.session.shipment.load_type  = shipmentt.truck.load_type;
	var shipment_dtl = {},tcn = '( '+ load_type.loading_capacity+ ' TON )';
	for(var i in shipmentt.shipment_detail)
		shipment_dtl[i]  = shipmentt.shipment_detail[i];
	shipment_dtl['truck_category'] = body['truck-type'];	
	shipment_dtl['truck_name']     = load_type.name + tcn;	
	shipment_dtl['truck_quantity'] = shipmentt.truck.quantity || 1;	
	shipment_dtl['load_type'] = shipmentt.truck.load_type==1?"Full Load":"Partial Load";	
	shipment_dtl['priority_delivery'] = body.priority_delivery=='on'?"Priority":"";
	shipmentt.shipment_detail = shipment_dtl;	
	shipmentt.redirect_to = "items-detail#step3";
	shipment_dtl =null;	
	shipmentt.save(function(err){
	
	  if(err)
	     return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'truck'});	
	  else{
		   var data  = {};
		       data['trucktype'+shipmentt.truck.category] = 'c1';   
		       data['delivery-type'+shipmentt.truck.load_type] = 'tr1';
		       data['dropdown-truck1'] = load_type;   
		       data['no_of_trucks'] = shipmentt.truck.quantity || 1;   
		       data['priority_delivery'] = shipmentt.truck.priority_delivery;   
		   setCookies(res,data,'step2')
		  return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/items-detail#step3')});	
	  }   
	});	
  });
 }catch(e){ res.redirect(shipperUrl('users/dashboard'));  }
}).get('/items-detail',csrfProtection,AccessRules,function(req,res){
 try{	
res.clearCookie('step3');		
Categories.Model;Shipments.Model.findOne({_id:req.session.shipment._id},'items truck transit').populate('items.parent_category').populate('items.sub_category').exec(function(err,items){  
	  if(!err && items){
		var it = [],sess={};
		for(var i in req.session.shipment)
			sess[i] = req.session.shipment[i];
		sess['load_type'] = items.truck?items.truck.load_type:null;  
		req.session.shipment = sess;sess=null; 
		items.items.forEach(function(item){ var itm = {'pcname':'','scname':''};
		 //itm = item;	
		 var pcat = item.parent_category,scat = item.sub_category;
	     itm['parent_category'] = pcat._id;		
		 itm.pcname =  pcat.name;
	     itm['scname'] = scat.name;		
	     itm['sub_category'] = scat._id;		
	     itm['name'] = item.name;
	     itm['id'] = item.id;
	     itm['desc'] = item.desc;
	     itm['weight'] = item.weight||null;
	     itm['length'] = item.length||null;
	     itm['width']  = item.width||null;
	     itm['height'] = item.height||null;
	     itm['total_items'] = item.total_items;
	     itm['invoice_image'] = item.invoice_image;
	     itm['total_weight'] = item.total_weight;
	     itm['invoice_path'] = '/img/shipments/shipment-'+req.session.shipment._id+'/';
			it.push(itm)	
		}); 
		setCookies(res,it,'step3');  
	  } 
     res.render('shipper/shipments/itemsdetail',{extractScripts: true,csrfToken:req.csrfToken(),load_type:items.truck?items.truck.load_type:null,'image':images,isShipExpired:Common.isShipmentExpired(items.transit),shipment:items});	
   });
 }catch(e){ 
	 res.redirect(shipperUrl('users/dashboard'));}
   	
}).post('/items-detail',csrfProtection,AccessRules,function(req,res){
	try{
	var item  =  req.body.item,errors={};
	if(Validate.isEmpty(item.name))
	   errors['name']  = {'message':req.__().e_itemname};
	if(Validate.isEmpty(item.parent_category))
	   errors['parent_category'] = {'message':req.__().e_parent_category};	
	if(Validate.isEmpty(item.sub_category))
	   errors['sub_category'] = {'message':req.__().e_sub_category};	
	if(Validate.isEmpty(item.desc))
	   errors['desc'] = {'message':req.__().e_itemdesc};	
	if(Validate.isEmpty(item.total_item))
	   errors['total_item'] = {'message':req.__().e_total_items};
	if(!item.id  && Validate.isEmpty(item.invoice))	
	   errors['invoice'] = {'message':req.__().e_item_invoice};
	if(req.session.shipment['load_type']==0 && parseInt(item.weight)<=0)
	   errors['total_weight'] =  {'message':req.__().e_total_weight};
	if(Object.keys(errors).length>0) return res.json({status:Const.HTTP_BAD_REQUEST,'errors':errors,form:'item'});	  var id = Date.now(),cond;
		
	if(item.weight)
	   item['total_weight'] = parseInt(item.weight)*item.total_item; 
	   	
	item['id']  = parseInt(item.id) || id;
	cond = {_id:req.session.shipment._id}; 
	if(item.invoice){	
	   item.invoice_image =  item.invoice_image || null;
	   UploadImage.allowedExtensions = images.extensions;
	   UploadImage.allowedSize       = images.maxsize;
       UploadImage.setFilenamePrefix('item-'+item.id);
       UploadImage.setFilename(item.invoice_image);
	   var fn =UploadImage.base64(item.invoice).upload(Common.media_dir_path(cond._id,Shipments));	
	   item['invoice_image'] = fn;	
	}
  //{_id:req.session.shipment._id || false,'items.id':item.id}
	Shipments.Model.update(cond,{'$pull':{'items':{'id':parseInt(item.id)}}},function(err,doc){
       Shipments.Model.update(cond,{'$push':{items:item}},{upsert:true,raw:true,new:true},function(err,doc,o){
		  if(!err){
			delete item['invoice'];  
			item['invoice_path'] = '/img/shipments/shipment-'+cond._id+"/";  
			item.invoice_image= item.invoice_image;
	         shipment_items[item.id]=item;
		     setCookies(res,shipment_items,'step3')  
		    return res.json({status:Const.HTTP_PARTIAL,data:item,cb:"listItem"});
		  }else return new Logger(err); 
	   });	
	});	
	}catch(e){ //console.log(e.message);
		res.redirect(shipperUrl('users/dashboard'));
	}
}).delete('/remove-item',csrfProtection,AccessRules,function(req,res){
	try{
	 Shipments.Model.update({_id:req.session.shipment._id},{'$pull':{'items':{'id':parseInt(req.body.id)}}},function(err,doc){	
		err && new Logger(err);
		Common.delImage({_id:req.session.shipment._id,invoice_image:req.body.invoice_image},
						'invoice_image',Shipments);
		delete shipment_items[req.body.id];
		setCookies(res,shipment_items,'step3');  
		res.json({status:Const.HTTP_SUCCESS,i:req.body.id});
	 });
	}catch(e){ //console.log(e.message);
	    res.redirect(shipperUrl('users/dashboard'));	
	}
}).post('/finalize-items',csrfProtection,AccessRules,function(req,res){
	try{
	var cond = {_id:req.session.shipment._id};
	Shipments.Model.findOne({_id:cond._id},function(err,data){
		if(err || !data) new Logger(err); 
		if(data.items.length<=0){
		  return res.json({status:Const.HTTP_BAD_REQUEST,errors:{items_list:{message:req.__().e_empty_shipment_items}},form:'items'});	
		}
		data.insurance   = {};
	
		var itm  = { items_total: req.body.items.items_total || 0};
		for(var i in itm)
		    data[i] = itm[i] || null;
		data.redirect_to = 'invoice#step4',
		data.insurance['text']   = req.body.items.insured==1?"Yes i want to buy insurance":
		                                 req.body.items.insured==2?"I already have insurance":
		                                 req.body.items.insured==3?"No i don`t want insurance":'No i don`t want insurance';
		data.insurance['insured_option'] = req.body.items.insured;
		
		data.insurance['amount']         =0; 
		data.invoicing_type = req.body.items.invoicing_type; 
		if(req.body.items.insurance){	
	       data.insurance['insurance_image']  =  data.insurance['insurance_image'] || null;
	       UploadImage.allowedExtensions = images.extensions;
	       UploadImage.allowedSize       = images.maxsize;
           //UploadImage.setFilenamePrefix('item-'+item.id);
           UploadImage.setFilename(data.insurance['insurance_image']);
	       var fn =UploadImage.base64(req.body.items.insurance).upload(Common.media_dir_path(cond._id,Shipments));
	       data.insurance['insurance_image'] = fn;	
	    }
		
		data.save(function(err){ 
			if(err)
			   return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'items'});
			shipment_items['items']= {items_total:data.items_total,insured:data.insurance.insurance_opton,
										  insurance_image:data.insurance['insurance_image'],
									      image_path:data.insurance['insurance_image']?'/img/shipments/shipment-'+cond._id+"/":null,
									 };		 
			setCookies(res,shipment_items,'step3');
			return res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/invoice#step4')})
			
		});
	});
	}catch(e){ res.redirect(shipperUrl('users/dashboard'));}
}).get('/invoice',csrfProtection,AccessRules,function(req,res){  
	try{
        var shipment_dtl={},insurance,settings;	Shipments.Model.findOne({_id:req.session.shipment._id}).populate('truck.ttype','loading_capacity').exec(function(err,shipment){
	    for(var i in shipment.shipment_detail)
			shipment_dtl[i]  =shipment.shipment_detail[i];
	    settings     = JSON.parse(JSON.stringify(req.session.settings));
		insurance    = shipment.insurance;
		if(insurance.insured_option==1)	
		   insurance['amount'] = shipment.items_total*parseFloat(settings.insurance_rate||0)/100;
        TruckLanes.Model.findOne({'source':shipment.loading.lp_city,'source_state':shipment.loading.lp_state,'destination':shipment.unloading.up_city,'destination_state':shipment.unloading.up_state},'distance price_per_km',
		function(err,dta){
			var shipment_weight = shipment.shipment_weight/1000;
			var weight=shipment.truck.ttype.loading_capacity>shipment_weight?                                        shipment.truck.ttype.loading_capacity:shipment_weight;			   
		   
	 		if(dta) shipment_dtl['per_ton_price'] = parseFloat(dta.price_per_km);
	        else shipment_dtl['per_ton_price']    = parseFloat(settings.lane_rate); 
		    
		    if(shipment.truck.load_type==0){ // partial load...
			  if(dta && shipment_dtl.lane_exist) 
			    shipment_dtl['load_fare'] = parseFloat(weight)*shipment_dtl['per_ton_price']*shipment.transit.est_distance;
			  else shipment_dtl['load_fare'] = parseFloat(weight)*shipment_dtl['per_ton_price']*shipment.transit.est_distance;	
		    }else{// in case of full load...		 
			  if(dta && shipment_dtl.lane_exist) 
			     shipment_dtl['load_fare']   = (weight)*shipment_dtl['per_ton_price']*shipment.transit.est_distance;
			  else shipment_dtl['load_fare'] = (weight)*shipment_dtl['per_ton_price']*shipment.transit.est_distance;				
		    }
			shipment_dtl['final_weight'] = weight;
	    shipment.priority_delivery_charges = 0; 
	    if(shipment.truck.priority_delivery==1){ // in case of prority delivery...
	       shipment.priority_delivery_charges =   (parseFloat(shipment_dtl['load_fare'])*parseFloat(settings.priority_delivery_rate||0))/100;
		 }
	     shipment_dtl['priority_delivery_charges'] = parseFloat(shipment.priority_delivery_charges);	         
	     shipment_dtl['shipment_weight']           = shipment.shipment_weight;	         
	     shipment_dtl['insurance_charges']         = insurance.amount||0;
	     shipment_dtl['sub_total']                 = parseFloat(shipment_dtl['load_fare']);
		if(insurance.insured_option==1)
		   shipment_dtl['sub_total'] += parseFloat(insurance.amount);
		if(shipment.truck.priority_delivery==1)
		   shipment_dtl['sub_total'] +=parseFloat(shipment_dtl['priority_delivery_charges']);
		   shipment_dtl['overall_total'] = shipment_dtl['sub_total']; 
	    Taxes.Model.find({status:1},'name rate -_id',{sort: {'priority': 1}},function(err,tax){
		   shipment_dtl['taxes'] = tax;	
		   /*for(var i in tax){
			 shipment_dtl['overall_total'] +=parseFloat(tax[i].rate||0); 
		   }*/
		   //for(var i  in shipment_dtl)
			//   shipment.shipment_detail[i] = shipment_dtl[i];
		   shipment['shipment_detail']      = shipment_dtl;		 
		   shipment['insurance']            = insurance;
		   shipment_dtl['truck_quantity']   = shipment.truck.quantity;		   	
		   shipment_dtl['transit']   = shipment.transit;		   	
	       shipment.save(function(err){
		   shipment_dtl = insurance  = null;	
			console.log(err);
		   });
		   res.render('shipper/shipments/invoice',{extractScripts: true,csrfToken:req.csrfToken(),'doc':shipment_dtl,isShipExpired:Common.isShipmentExpired(shipment.transit)});
	    });		  
	  });	
    });	
 }catch(e){
	//console.log(e.message);
	 res.redirect(shipperUrl('users/dashboard'));
 }
	
 
}).post('/postonbb',csrfProtection,AccessRules,function(req,res){
   try{	
   Shipments.Model.update({_id:req.session.shipment._id,status:Shipments.INCOMPLETE},{shipment_type:{code:5,text:req.__().t_bid_board},status:Shipments.PENDING},function(err,update){   
	   Shipments.PostedOnBidBoard(req);   
	   res.json({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/bid-load-board')})
	});
	   
   }catch(e){
	   console.log(e.message);
   }
}).get('/payment',csrfProtection,function(req,res){
	try{
    var $this = this; Shipments.Model.findOne({_id:req.session.shipment._id}).populate('truck.ttype').populate('shipper_id','pancard business.service_taxno has_cp').exec(function(err,doc){
	if(err || !doc)return res.redirect(shipperUrl('users/dashboard'));
	
	if(doc.shipment_type && doc.shipment_type.code==Shipments.BID_BOARD){           
		Bids.Model.findOne({shipment_id:req.session.shipment._id,_id:req.session.shipment.bid_id,status:Bids.POSTED},'price',function(err,bid){
		if(err || !bid)return res.redirect(shipperUrl('users/dashboard'));	
        var shipment_detail =  doc.shipment_detail;
			shipment_detail['bid_price'] = bid.price;
			shipment_detail['sub_total'] = bid.price;
			if(doc.insurance.insured_option==1)
		       shipment_detail['sub_total'] += parseFloat(doc.insurance.amount);
			shipment_detail['overall_total'] = shipment_detail['sub_total'];
			doc.bid_price  = bid.price;
		
			res.render('shipper/shipments/payment',{extractScripts: true,csrfToken:req.csrfToken(),shipment:doc,Shipments:Shipments});	
					
		
	    });
	}else{
	  doc.shipment_type = {
		code:Shipments.REGULAR_BOARD,
		text:'Regular load Board',  
	  };
		var shipment_dtl ={}
		   for(var i in doc.shipment_detail)
			shipment_dtl[i]  =doc.shipment_detail[i];
	     Taxes.Model.find({status:1},'name rate -_id',{sort: {'priority': 1}},function(err,tax){
		   shipment_dtl['taxes'] = tax;
		   shipment_dtl['overall_total'] =  parseFloat(shipment_dtl['overall_total']);	 
		   for(var i in tax){ 
			 shipment_dtl['overall_total'] +=parseFloat(tax[i].rate||0); 
		   }
		   //for(var i  in shipment_dtl)
			//   shipment.shipment_detail[i] = shipment_dtl[i];
		   doc['shipment_detail']      = shipment_dtl;		 
	       doc.save(function(err){ console.log(err);
		         shipment_dtl =  null;	
			     res.render('shipper/shipments/payment',{extractScripts: true,csrfToken:req.csrfToken(),shipment:doc,Shipments:Shipments});	
	    });		 
	    });		 
	  
	}
  });
 }catch(e){
	//console.log(e.message);
	 res.redirect(shipperUrl('users/dashboard'));
 }
}).post('/payment',csrfProtection,function(req,res){
   try{
 Shipments.Model.findOne({_id:req.session.shipment._id}).populate('truck.ttype').populate('trucker_id','contact device').exec(function(err,doc){ 
	 var ptotal = 100;  
	if(err || !doc)return res.redirect(shipperUrl('users/dashboard'));
	Users.Model.findOne({_id:req.session.user.ID},function(err,user){
		user.update =  67;
		if(user){
			var business =  {};business  =  user.business;
			business['service_taxno'] = req.body.Users.service_tax_no||null;
			business['pancard']       = req.body.Users.pancard_no||null;
			user.pancard = business['pancard'];
			for(var i in business)
		    user.business[i] = business[i];
		}
		user.save(function(err){ 
			if(err) return res.json({status:Const.HTTP_BAD_REQUEST,errors:err.errors,form:'payment'});
	if(doc.shipment_type.code==Shipments.BID_BOARD){                                        
		Bids.Model.findOne({shipment_id:req.session.shipment._id,_id:req.session.shipment.bid_id,status:Bids.POSTED},function(err,bid){
		if(err || !bid)return res.redirect(shipperUrl('users/dashboard'));
		var shipment_detail = {}
		for(var i in doc.shipment_detail)
		    shipment_detail[i] = doc.shipment_detail[i];	
		if(req.body.Users.insurance_option){
			doc.insurance.insured_option = 0;
			shipment_detail['sub_total'] = parseFloat(shipment_detail['overall_total'])-parseFloat(doc.insurance.amount||0);
			shipment_detail['overall_total'] = parseFloat(shipment_detail['overall_total'])-parseFloat(doc.insurance.amount||0);
			shipment_detail['insurance_charges'] = 0;
			doc.shipment_detail = shipment_detail;
			
		}	
		 //total percentage...	
		if(req.body.Users.rest_payment<100)
		   ptotal = ptotal-req.body.Users.rest_payment;	
         var Payment =Payments.Add({
			shipment_id:doc._id,
			payer_id   :req.session.user.ID,
			ship_by    :doc.trucker_id,
			payment_type:doc.truck.load_type,
			total:shipment_detail.overall_total,
			outstanding:shipment_detail.overall_total,
		});
		Payment.transactions.push({
			    instalment:ptotal,
				amount:(parseFloat(shipment_detail.overall_total)*ptotal)/100,
				mode:(doc.insurance && doc.insurance.insured_option==1)?2:req.body.Users.stpaymentmethod,
				status:Payments.PENDING,
				
		    });
		Payment.transactions.push({
			    instalment:req.body.Users.rest_payment,
				amount:(parseFloat(shipment_detail.overall_total)*req.body.Users.rest_payment)/100,
				mode:req.body.Users.ndpaymentmethod,
				status:Payments.PENDING,
				
		    });
		
		
		Payment.ship_by = bid.trucker_id;	
		Payment.save(function(err){
			
			if(err)res.json({status:Const.HTTP_BAD_REQUEST,err:err.errors});
			else if(req.body.Users.stpaymentmethod==Payments.CASH){
				bid.status = Bids.ACCEPTED;
			    bid.save(function(){				
				if(doc.insurance && doc.insurance.insured_option==1){
					req.session.payment = {
							_id:Payment._id,
							instalment:1,
				    };
					return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_payment_successfully,next:shipperUrl('shipments/pay')});
				}else{
				  doc.status = Shipments.ACCEPTED;	
				  doc.trucker_id = bid.trucker_id;	
				  doc.save(function(err){
					Payments.PayCashOnLoading(req,Payment,doc);  
				 return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_payment_successfully,next:shipperUrl('shipments/success')});
			     });
				}
			   });
				
			}else if(req.body.Users.stpaymentmethod==Payments.ONLINE){
				req.session.payment = {
							_id:Payment._id,
							instalment:1,
				    };
				return res.send({status:Const.HTTP_SUCCESS,msg:req.__().t_payment_successfully,next:shipperUrl('shipments/pay')});		
			}
		});	
	   });
	}else{ 
	  var shipment_detail = {}
		for(var i in doc.shipment_detail)
		    shipment_detail[i] = doc.shipment_detail[i];	
		if(req.body.Users.insurance_option){
			doc.insurance.insured_option = 0;
			shipment_detail['sub_total'] = parseFloat(shipment_detail['overall_total'])-parseFloat(doc.insurance.amount||0);
			shipment_detail['overall_total'] = parseFloat(shipment_detail['overall_total'])-parseFloat(doc.insurance.amount||0);
			shipment_detail['insurance_charges'] = 0;
			doc.shipment_detail = shipment_detail;
			doc.save(function(err){
				console.log(err);
			});
		}	
		
	  if(req.body.Users.rest_payment<100)
		   ptotal = ptotal-req.body.Users.rest_payment;	
        var Payment =Payments.Add({
			shipment_id:doc._id,
			payer_id   :req.session.user.ID,
			ship_by    :doc.trucker_id,
			payment_type:doc.truck.load_type,
			total:shipment_detail.overall_total,
			outstanding:shipment_detail.overall_total,
		});
		Payment.transactions.push({
			    instalment:ptotal,
				amount:(parseFloat(shipment_detail.overall_total)*ptotal)/100,
				mode:req.body.Users.stpaymentmethod,
				status:Payments.PENDING,
				
		    });
		Payment.transactions.push({
			    instalment:req.body.Users.rest_payment,
				amount:(parseFloat(shipment_detail.overall_total)*req.body.Users.rest_payment)/100,
				mode:req.body.Users.ndpaymentmethod,
				status:Payments.PENDING,
				
		    });
		Payment.save(function(err){
			if(err)res.json({status:Const.HTTP_BAD_REQUEST,err:err.errors});
			else if(req.body.Users.stpaymentmethod==Payments.CASH){
				if(doc.insurance && doc.insurance.insured_option==1){
					req.session.payment = {
							_id:Payment._id,
							instalment:1
				    };
					return res.json({status:Const.HTTP_SUCCESS,msg:req.__().t_payment_successfully,next:shipperUrl('shipments/pay')});
				}else{
				  doc.status = Shipments.PENDING;	
				  //doc.trucker_id = bid.trucker_id;	
				  doc.save(function(){
					Payments.PayCashOnLoading(req,Payment,doc);  
				 return res.json({status:Const.HTTP_SUCCESS,msg:req.__('shipments').t_payment_successfully,next:shipperUrl('shipments/success')});
			     });
				}
			}else if(req.body.Users.stpaymentmethod==Payments.ONLINE){
				req.session.payment = {
					_id:Payment._id,
					instalment:1
				};
				return res.send({status:Const.HTTP_SUCCESS,next:shipperUrl('shipments/pay')});		
			}
		})
	}
    });
   });
  });
 }catch(e){
	//console.log(e.message);
	 res.redirect(shipperUrl('users/dashboard'));
 }
	
}).get('/pay',csrfProtection,AccessRules,function(req,res){
	Payments.Model.findOne({_id:req.session.payment._id||false}).populate('shipment_id').exec(function(err,payment){
		if(err)return res.render('shipper/shipments/pay',{extractScripts: true,err:err.message});
		Users.Model.findOne({_id:req.session.user.ID},'contact business name',function(err,user){
			if(err) return res.render('shipper/shipments/pay',{extractScripts: true,err:err.message});
			var txid = randomString({ length: 20 }),amount=3,sign = Common.generateSignature(txid,amount);	
			res.render('shipper/shipments/pay',{extractScripts: true,
				user:user,
				csrfToken:req.csrfToken(),
				amount:payment.transactions[req.session.payment.instalment-1].amount,
				citrus:{
					    txid:txid,
					    sign:sign,
						access:Const.citrus.ACCESS_KEY,
						vanity:Const.citrus.VANITY_URL,
						return_url:Const.citrus.RETURN_URL,
						notify_url:Const.citrus.NOTIFY_URL,
						amount:amount,
		        },
				payment:payment,								
				shipment:payment.shipment_id,
				Shipments:Shipments,
				paymentof:req.session.payment								
			});
		});
	});
}).get('/detail/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Shipment Detail');
																  
   var Trucks = appModel('Trucks'),
   	Drivers = appModel('Drivers'),
   	Trucks = new Trucks(),
   	Drivers = new Drivers,
   	aoffset = hoffset = 0;
   Trucks.Model;
   Drivers.Model;
   req.session._shipment = {
   	id: req.params.id
   };
   Shipments.Model.findOne({
   _id: req.params.id
   }, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status vehicle').populate('truck.ttype', 'name loading_capacity').populate('vehicle.id', 'registration.vehicle_number').populate('vehicle.driver', 'name mobile_no rating').exec(function (err, doc) {
   if (err) doc = null;
   res.render('shipper/shipments/detail', {
   	extractScripts: true,
   	csrfToken: req.csrfToken(),
   	'shipment': doc,
   	Shipments:Shipments
   });
   });
   }).post('/getrip-info',csrfProtection,AccessRules,function(req,res){
	var Drivers =appModel('Drivers'),Drivers = new Drivers;
	Drivers.Model.find({_id:{$in:req.body.Driver}},'location',function(err,doc){
		if(!err && doc){		  	
		  res.json({status:Const.HTTP_SUCCESS,data:doc});	
		}
	})

}).get('/history/:id',csrfProtection,AccessRules,function(req,res){ Appage.title('Shipment History Detail');
   var Trucks = appModel('Trucks'),
   	Drivers = appModel('Drivers'),
   	Trucks = new Trucks(),
   	Drivers = new Drivers,
   	aoffset = hoffset = 0;
   Trucks.Model;
   Drivers.Model;
   req.session._shipment = {
   	id: req.params.id
   };
   Shipments.Model.findOne({
   	_id: req.params.id
   }, 'loading.lp_city unloading.up_city truck transit.t_date transit.t_time shipment_id status vehicle').populate('truck.ttype', 'name loading_capacity').populate('vehicle.id', 'registration.vehicle_number').populate('vehicle.driver', 'name mobile_no rating').exec(function (err, doc) {
   	if (err) doc = null;
   	res.render('shipper/shipments/history-detail', {
   		extractScripts: true,
   		csrfToken: req.csrfToken(),
   		'shipment': doc,
   		Shipments,
   		Shipments
   	});
   });
}).post('/reschedule',csrfProtection,AccessRules,function(req,res){
	var body = req.body.Shipment;
if (Validate.isEmpty(body.change_time))
	return res.json({
		status: Const.HTTP_BAD_REQUEST,
		error: req.__().e_change_time
	});
var time = body.change_time.split(":"),
	d = new Date(),
	dt = new Date(d.getFullYear(), d.getMonth(), d.getDay(), time[0] || 24, time[1] || 59, time[2] || 59),
	cdt = new Date(d.getFullYear(), d.getMonth(), d.getDay(), 24, 00, 00); //compare with...
var Bids = appModel('Bids'),
	Bids = new Bids();
if (dt.getTime() > cdt.getTime())
	return res.json({
		status: Const.HTTP_BAD_REQUEST,
		error: req.__().e_change_time
	});
Shipments.Model.findOne({
		_id: req.session._shipment.id || false
	}, 'transit vehicle')
	.populate('vehicle.driver', 'device_token mobile_no').exec(function (err, doc) {
		var transit = doc.transit,oldtime =transit.t_time;
		transit.t_time = dt.toLocaleTimeString();
		doc.transit = transit;
		doc.save(function (err) {
			console.log(err);
			if (err) return res.json({
				status: Const.HTTP_BAD_REQUEST,
				error: err.message
			});
			// shipment rescheduled notify truker if bids on shipment...
			var msg = req.__().template(req.session.user.getName(), req.__().t_shipmenttime_updated),
				emailMsg = req.__().template(req.session.user.getName(),
					truckerUrl('shipments/detail/' + req.session._shipment.id),
					req.__().t_notifyshippertimechanged);
			Shipments.notifyDateTimeChanged(req.session._shipment.id,oldtime);
			Shipments.timeUpdated(doc.vehicle);
			return res.json({
				status: Const.HTTP_SUCCESS,
				success: req.__().t_timechanged_succesfully
			});
		});
		//{$set:{'transit.t_time'dt.getLocalTimeString()}},
		//if(affected){

		// Bid	

		//}
	})
})
var blboffset;
router.get('/bid-load-board',csrfProtection,AccessRules,function(req,res){ Appage.title('Bia Load Board');
	var s= Shipments;blboffset=0
	s.Model.find({status:{$in:[s.PENDING]},'shipment_type.code':s.BID_BOARD,'transit.t_expdatetime':{$gt:DateTime.now()},shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime',{'sort':{modified_on:-1}})
	.populate('truck.ttype','name loading_capacity').skip(blboffset).limit(s.LIMIT_BID_BOARD)
	.exec(function(err,doc){
		blboffset+=s.LIMIT_BID_BOARD;
		res.render('shipper/shipments/bid-load-board',{extractScripts: true,csrfToken: req.csrfToken(),doc:doc});
	})
}).post('/bid-load-board',csrfProtection,AccessRules,function(req,res){
	var s= Shipments;
	s.Model.find({status:{$in:[s.PENDING]},'shipment_type.code':s.BID_BOARD,'transit.t_expdatetime':{$gt:DateTime.now()},shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime',{'sort':{modified_on:-1}})
	.populate('truck.ttype','name loading_capacity').skip(blboffset).limit(s.LIMIT_BID_BOARD)
	.exec(function(err,doc){
		blboffset+=s.LIMIT_BID_BOARD;
		res.render('shipper/shipments/paginate_bidloadboard',{layout:false,csrfToken: req.csrfToken(),doc:doc});
	})
}).get('/bid-detail/:id',csrfProtection,AccessRules,function(req,res){
    try{	Shipments.Model.findOne({_id:req.params.id||false,status:Shipments.PENDING,shipper_id:req.session.user.ID},'loading.lp_city unloading.up_city truck transit.t_date transit.t_time transit.t_expdatetime shipment_id status items insurance').populate('truck.ttype','name loading_capacity').exec(function(err,doc){
	  res.render('shipper/shipments/bid_detail',{extractScripts:true,csrfToken: req.csrfToken(),shipment:doc,Shipments:Shipments});
	});
	}catch(e){ console.log(e.message);
		res.redirect(shipperUrl('users/dashboard'));
	}
}).get('/success',AccessRules,function(req,res){
	var msg = req.session.payment_msg;
	delete req.session.payment_msg;
	res.render('shipper/shipments/success',{extractScripts:true,msg:msg});
}).post('/success',AccessRules,function(req,res){
	if (!req.body && !req.body.signature)
	return res.render('shipper/success', {
		msg: req.__().e_invalid_request
	});

try {
	var post = req.body,msg;
	var data_string = post['TxId'] + post['TxStatus'] + post['amount'] + post['pgTxnNo'] + post['issuerRefNo'] + post['authIdCode'] + post['firstName'] + post['lastName'] + post['pgRespCode'] + post['addressZip'];
	var signature = crypto.createHmac('sha1', Const.citrus.SECRET_TOKEN).update(data_string).digest('hex');
	if (signature == post['signature'] && post['TxStatus'] == 'SUCCESS') {
		Payments.Model.findOne({
			_id: req.session.payment._id
		}, function (err, payment) {
			var instalment    = payment.transactions[req.session.payment.instalment - 1];
			var instalment2nd = payment.transactions[1];
			//console.log(instalment);
	//console.log(post);
			if (post['TxStatus'] == 'SUCCESS') {
				instalment.status = 1;
				payment.outstanding = payment.outstanding - instalment.amount;
				if(instalment.status==1 && instalment2nd.status==1){
					payment.status = 1;
				}
			}
            
			payment.save(function (err) {
				if(err){
				  return res.render('shipper/shipments/success', {extractScripts:true,
		                        msg: req.__().t_payment_successfully_completed_but_internal_error.replace('<transaction_id>',post['TxId'])
	              });
				}
			    
				
				Shipments.Model.findOne({_id:payment.shipment_id},function(err,shipment){
				
					
				if(shipment && req.session.shipment){
				   req.session.shipment._id = shipment._id;
				}else{
				  req.session.shipment  ={_id:shipment._id};	
				}               
				var ship_id  =req.session.shipment && req.session.shipment._id?req.session.shipment._id:false,
				bid_id  =req.session.shipment && req.session.shipment.bid_id?req.session.shipment.bid_id:false; 	
					
				var Transaction = Transactions.Load({
					amount: instalment.amount,
					payment_id: req.session.payment._id,
					status: {
						code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
						text: post['TxStatus'],
					},
					debug: post,
					ttype: {
						code: Transactions.SPENT,
						text: "Spent By Shipper"
					},
					user_id: req.session.user.ID
				  })	
				
				
				Transaction.save(function (err) {
					if(err){
						 return res.render('shipper/shipments/success', {extractScripts:true,
		                        msg: req.__().t_payment_successfully_completed_but_internal_error.replace('<transaction_id>',post['TxId'])
	              });
					}else{
					Bids.Model.findOne({shipment_id:ship_id,_id:bid_id,status:Bids.POSTED},function(err,bid){
					if(bid){
				       bid.status = Bids.ACCEPTED;
					   shipment.trucker_id = bid.trucker_id;	
				 	   bid.save(function(err){
					   });
					req.session.trucker = {_id:bid.trucker_id};	
					}
					if(shipment.shipment_type.code== Shipments.BID_BOARD && req.session.payment.instalment==1){	
					   shipment.status =Shipments.ACCEPTED;
					}else if(shipment.shipment_type.code== Shipments.REGULAR_BOARD && req.session.payment.instalment==1){
					   shipment.status =Shipments.PENDING;	
					   Shipments.PostedOnRegularBoard(req);
					}
					Payments.PaymentCompletedOnline(req);	
					
					shipment.save(function(err){
					    
						//Shipments.mailInvoice();
  					    return res.render('shipper/shipments/success', {extractScripts:true,
		                        msg: req.__().t_payment_successfully_completed.replace('<transaction_id>',post['TxId'])
	                    });
                      });	
					});
				   }
				  });
			    });
			   });
			  });			
			//});
		//});
	} else {
		Payments.Model.findOne({
			_id: req.session.payment._id
		}, function (err, payment) {
			var instalment = payment.transactions[req.session.payment.instalment - 1].amount
			var Transaction = Transactions.Load({
				amount: instalment.amount,
				payment_id: req.session.payment._id,
				status: {
					code: post['TxStatus'] == 'SUCCESS' ? 1 : 0,
					text: post['TxStatus'],
				},
				debug: post,
				ttype: {
					code: Transactions.SPENT,
					text: "Spent By Shipper"
				},
				user_id: req.session.user.ID
			});
			Transaction.save(function (err) {
				if(err){
					return res.render('shipper/shipments/success', {extractScripts:true,
		                        msg: req.__().t_payment_failed,
						        failed:1
	              });
				}else{
				    return res.render('shipper/shipments/success', {extractScripts:true,
		                        msg: post['TxMsg'],
						        failed:1
	              });
				}
			});
		});
	}
} catch (err2) {
	return res.render('shipper/shipments/success', {extractScripts:true,
		msg: err2.message,
		failed:1
	});
}
      //  });
    //}	
}).post('/payment-response',function(req,res){
	console.log(req.body);
	res.end()
});


function uploadImage(bid,index,req,res){
	var fs = require('fs');
	if(req.body.insurance){
	   fn = bid.insurance.image || Date.now()+".jpg"; 
	   var base64Data = req.body.insurance.replace(/^data:image\/jpeg;base64,/, "");
       //var bfr = new Buffer(base64Data,'base64').toString('binary');
		fs.writeFile(Common.media_dir_path(req.body._id,Bids)+fn, base64Data,'base64',function(err) {
        (err && new Logger(err))	
		bid[index] = fn;
		saveBid(res,bid);	
       });
	
   }
}
/* get shipper myshipment by status condition
 *@params status (array) shipment status callback (callback),
 *offset (integer) page start from,upto (integer) records up to
 */ 
function getMyShipments(status,callback,offset,upto){ 
  Shipments.Model.find({shipper_id:shipper_id,status:{$in:status}},'shipment_id shipment_detail.truck_name loading.lp_city unloading.up_city truck transit.t_date transit.t_time vehicles').sort({'transit.t_expdatetime':1}).skip(offset||0).limit(upto||Const.SHIPMENT_LIST).exec(callback);
}
//access rules...
function AccessRules(req,res,next){
	try{
	if(req.session.user.isShipper() && req.session.user._id){
	   if(req.session.user.redirectTo=='dashboard')	
	     next();
	  else if(req.session.user.redirectTo) return res.redirect(shipperUrl(req.session.user.redirectTo))	
	  else return res.redirect(webappUrl(''));	
	}else res.redirect(shipperUrl(''));
	}catch(e){
		res.redirect(webappUrl(''));
	}
}
// set cookies for shipment next prevous steps...
function setCookies(res,data,step){	
	res.cookie(step,data,30*60*1000);
}






module.exports = router;
