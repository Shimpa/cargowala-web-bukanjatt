var express  = require('express');
var passport = require('passport');
var router   = express.Router();
var User     = appModel('Users');

var Users    = new User();
var UploadImage    = appHelper('Image'),
	images   = {
		'extensions':['jpg','jpeg','png','bmp'],
		'maxsize':1.5 // size in MB 
	};
/* GET home page. */
router.get('/',AccessRules,function(req, res, next) { Appage.title(req.__('users').t_login);
   req.session.user = {};	
   res.render('login',{ layout: 'layouts/loginregister-layout','image':JSON.stringify(images),isverified:null});
}).get('/login',AccessRules,function(req,res){
	  if(req.session.user.ID)
		 return res.json({status:Const.HTTP_SUCCESS,location:webappUrl('login')});  
	
}).post('/login',AccessRules,function(req,res){ 
	 if(req.session.user.ID)
		return res.redirect(webappUrl('login'));  
    var username = req.body.Login.username || false,
		password = req.body.Login.password || false;	    
	    Users.oldPassword = password;
	    Users.load({'contact.email':username},function(err,user){ 
		if(err) return new Logger(err);
		  switch(true){			
			case (!Users.validPassword): 
			    res.json({msg:_locals().invalid_credential}); 
			break;
			case ((user && !Users.isActive && user.hash_token!==null)):
				var ht = user.hash_token.split(":"),time = 1000*60*60*24*1,htime  = ht[1]+time;
				if(htime<Date.now())   
			       res.json({msg:_locals().verify_email_with_link}); 
		        else
			       res.json({msg:_locals().verify_email}); 
			break;
			case ((!Users.isActive && !user.hash_token)):
			    res.json({msg:_locals().inactive_user}); 
			break;
			case (user && Users.validPassword && Users.isActive):
				  registredUserProfile(req,res,user);	
			      setAuthentication(req,res,user);
		          res.json({status:Const.HTTP_SUCCESS,location:webappUrl('login')}); 
			break;	
		    default:
			    res.json({msg:_locals().invalid_credential}); 
			break;			
		  }
        });	
});
router.get('/forgot-password',function(req,res){
  res.render('forgot-password',{ layout: 'layouts/loginregister-layout',isverified:null});	
}).post('/forgot-password',function(req,res){
	var worker = appHelper('Worker'),email=req.body.Forgot.email;
	if(!appHelper('RegularExp').isEmail(email||''))
	   return res.json({errors:{email:{message:req.__('users').t_registred_email}},form:'forgot'});
	var query =Users.Model.findOne({"contact.email":email,status:Users.ACTIVE});
	    query =query.select('-_id name contact.email hash_token');
	    query.exec(function(err,user){
			if(err || !user)
			   return res.json({errors:{email:{message:req.__('users').t_registred_email}},form:'forgot'});             var ht= user.hash_token.split(":"),time = 1000*60*60*2,htime=ht[1]+time,
				    token = MD5(user.contact.email+Date.now())+":"+Date.now();
			if(htime<Date.now())
			   return res.json({errors:{email:{message:req.__('users').t_registred_email}},form:'forgot'});
			else{
			   worker.execPHP('Mailer.php '+email+" resetpassword "+token +" "+ user.name.firstname,function(err,r){
		        if(!err){
			      user.hash_token = token;
			      user.modified_on= Date.now();
				  user.isvalidated= 1;	
			      user.save();
			      
			}
           });
		    return res.json({status:Const.HTTP_SUCCESS,display:{html:req.__('users').t_recently_sent_email},form:'forgot'});	
		 }
		});
	
}).get('/reset-password',function(req,res){
	res.render('shipper/reset-password',{ layout: 'layouts/shipper/loginregister-layout',isverified:null,token:req.params.token||null});
}).post('/reset-password',function(req,res){
	var tk = req.body.Reset.token.split(":"),token=tk[0],htime=tk[1],
		np = req.body.Reset.new_password    ||null,
		cp =req.body.Reset.confirm_password || null;
	if(!np)
		return res.json({errors:{new_password:{message:_locals().new_password}},form:'reset'});
	if(!cp || np!=cp)
		return res.json({errors:{confirm_password:{message:_locals().confirm_password}},form:'reset'});		
	if(np && cp && !appHelper('RegularExp').isString(32,32,token||''))
	  return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
	else{
		Users.Model.findOne({hash_token:tk},'contact.email name',function(err,user){
			if(!user || err)
	          return res.json({errors:{new_password:{message:_locals().invalid_token}},form:'reset'});
			else{
			worker.execPHP('Mailer.php '+email+" passwordupdated "+user.name,function(err,r){
		        if(!err){
			      user.password = np;
				  user.isvalidated=1;
				  user.save();			      
			    }
              });				
			return res.json({success:Const.HTTP_SUCCESS,display:{html:_locals().success_reset},form:'reset'});
		  }
		})
	}
		
	 	
}).get('/register',function(req,res){ Appage.title(req.__('users').t_registration);
	req.session.user = {};var social = false
	if(req.session && req.session.social){
	   social = req.session.social;
	   delete req.session.social;	
	}
    res.render('register',{ layout: 'layouts/loginregister-layout','image':images,isverified:null,social:social});
}).post('/register',function(req,res){
	
	   var socialprovider = {provider:'webapp',uid:null},socialpassword=null;
	if(req.body.Register.social)
	   socialprovider = JSON.parse(req.body.Register.social)
	if(socialprovider.provider && socialprovider.uid!=null){
	   socialpassword = Math.floor(Date.now()+Math.random());
	}
	var user = {
		name:{
			firstname:req.body.Register.name['firstname'],
			lastname:req.body.Register.name['lastname'],
		},
		contact:{
		      email:req.body.Register.email,	
		      mobile_number:req.body.Register.mobile_number,	
		},
		status:Users.INACTIVE,
		password:socialpassword?socialpassword:req.body.Register.password,
		image   :req.body.Register.image?'default':null,
		confirm_password :socialpassword?socialpassword:req.body.Register.confirm_password,
		has_cp  :0,
		otp     :{otp_for:req.body.Register.mobile_number},
		role    :req.body.Register.user==0?21:12,
		social  :req.body.Register.social?JSON.parse(req.body.Register.social):socialprovider,
	};
        if(req.body.Register.social_image){
           user.status = 1;
           user.image  ='default';
        }
	var user = Users.Add(user);
	user.save(function(err){ //console.log(err.errors); 
		//console.log(err)
		if(err){
			if(err.errors){		
			  return res.json({'errors':err.errors});
			}else
		      return res.json({'errors':{'contact.email':{'message':err.message}}});
		}
		else{
			user.image = user.image=='default'?null:user.image;
		    UploadImage.allowedExtensions = images.extensions;
			UploadImage.allowedSize       = images.maxsize;
			user.plocation  = 'user-type';
			if(req.body.Register.social_image){
		      UploadImage.setFilename(null);
              UploadImage.setImageSavePath(Common.media_dir_path(user._id,Users));
              UploadImage.getSocialImage(req.body.Register.social_image,function(fn){
              user.image = fn;
              user.isvalidated=1;          
              user.save({ validateBeforeSave: false },function(er){
			      registredUserProfile(req,res,user);  
                  return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});  
	          }); 
			 })
			}else{
			  var fn = null;
			  if(req.body.Register.image){	
			     UploadImage.setFilename(user.image);
			     fn =UploadImage.base64(req.body.Register.image).upload(Common.media_dir_path(user._id,Users));
			  }
			  user.isvalidated=1;
			  user.image = fn;			  
			  user.save(function(er){
				delete req.session.social;  
			    registredUserProfile(req,res,user);	
		        return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});  
			  });
		    }
		}		
	});
})/*.get('/user-type',validateRegister,function(req,res){ // get info of user type agent|Individual|company
	res.render('shipper/user-type',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});
}).post('/user-type',validateRegister,function(req,res){ 
	
	try{								   
      if(!(req.body.Register.usertype in Users.Roles()))
	     return res.json({'errors':{'usertype':{'message':_locals(appLang,'register').user_type}}});
		 Users.Model.findOne({'_id':req.session.register._id},function(err,user){
			user.isvalidated  =1; 
			user.account_type =req.body.Register.usertype;
			user.role =req.body.Register.usertype;
			if(req.body.Register.usertype==Users.INDIVIDUALSHIPPER) 
			   user.plocation    ='dashboard'; 
			else 
			   user.plocation    ='company'; 
			   user.save({ validateBeforeSave: false },function(err){ 
				if(err) return res.json({'errors':{'usertype':{'message':err.message}}});
				setpLocation(req,user.plocation);
				if(user.role==Users.INDIVIDUALSHIPPER){ 												  
				if(user.hash_token!="" && user.status==Users.INACTIVE){
				   req.session.setFlash('verify-email',_locals().verify_email)	
				   return res.json({status:Const.HTTP_SUCCESS,location:'/shipper/'});
				}else
				   return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
				}else{
				   return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
				}
			}); 
		 });	   
	}catch(e){ //console.log(e.message)
		res.json({status:Const.HTTP_SUCCESS,location:shipperUrl('')});
	}
	
});.get('/company',validateRegister,function(req,res){ // company information form	
	res.render('shipper/company',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});
});.post('/company',validateRegister,function(req,res){
	try{
	Users.Model.findOne({'_id':req.session.register._id},function(err,user){
		user['business_type'] = req.body.Register['business_type'];
		user['company_type']  = req.body.Register['company_type'];
		for(var k in req.body.Register.business)
		user.business[k]      = req.body.Register.business[k];
		
		user.plocation        = 'company-address';
		user.isvalidated      = 1;		
		user.save(function(err,usr){ 
			if(err)
			  return res.json({errors:err.errors});
			else{ 
				setpLocation(req,user.plocation);
				res.json({status:Const.HTTP_SUCCESS,location:user.plocation});
			}
		})
	});
	}catch(e){
		res.json({status:Const.HTTP_SUCCESS,location:'login'});
	}
}).get('/company-address',validateRegister,function(req,res){	
	res.render('shipper/company-address',{ layout: 'layouts/shipper/loginregister-layout',isverified:verifyEmail(req)});	
}).post('/company-address',validateRegister,function(req,res){
	try{
	Users.Model.findOne({_id:req.session.register._id},function(err,user){
		if(!user || err) return res.json({status:Const.HTTP_SUCCESS,location:'login'});
		for(var k in req.body.Register.business){ 
			user.business[k] = req.body.Register.business[k];
		}
		user.plocation        = 'dashboard';
		user.isvalidated      = 1;		
		user.save(function(err){ 
			if(err)
			  return res.json({errors:err.errors});
			else{ 
				delete req.session.register;
				if(user.status==Users.ACTIVE && user.hash_token==""){
				   setpLocation(req,user.plocation);
				   return res.json({status:Const.HTTP_SUCCESS,location:user.plocation});	
				}else{
					return res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});
				}
			}
		});
	});
	}catch(e){ console.log(e.message);
		res.json({status:Const.HTTP_SUCCESS,location:webappUrl('')});
	}
});*/

router.get('/logout',function(req,res){
	var applang = req.session.applang || 'en';   
	delete req.session['_user_'];
	delete req.session.settings;
	delete req.session.user;
	delete req.session.shipment;
	//req.session.destroy();
	//req.session.applang = applang;
	return res.redirect(webappUrl(''));
})
router.get('/facebook-login',
  passport.authenticate('facebook', { failureRedirect: '/login',scope: ['email','public_profile'] }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.end('success');
  });
router.get('/facebook-callback',function(req, res,next) {
    passport.authenticate('facebook', function (usr, info) {
            if (!usr) return res.redirect(shipperUrl(''));
            var user = {
		name:{
			firstname:usr.first_name,
			lastname:usr.last_name,
		},
		contact:{
		      email:usr.email,	
		      mobile_number:null,	
		},
		status:1,
        social:{'provider':'facebook','uid':usr.id}, 		
		image   :'https://graph.facebook.com/'+usr.id+'/picture?width=800&height=800'  
	};
    Users.Model.findOne({'contact.email':user.contact.email},function(err,dta){
          if(err) return res.redirect(webappUrl(''));
          if(dta){
             return isUserCompletedProfile(req,res,dta,next); 
          }else{
             registerSocial(req,res,user);
          }
    });

    })(req, res, next);
}).get('/help-and-support',function(req,res){
	   var layout = {};
	if(req.session.user && req.session.user.isShipper())		
		layout = {layout:'layouts/shipper/mylayout'};
	if(req.session.user && req.session.user.isTrucker())
		layout = {layout:'layouts/trucker/mylayout'};
	
	   res.render('contact_us',layout);
	
});

router.get('/google-login',
  passport.authenticate('google', { scope: ['profile','email'] , failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.end('success');
  });
router.get('/google-callback',function(req, res,next) {
    passport.authenticate('google', function (usr, info) {
            if (!usr) return res.redirect(shipperUrl(''));
 console.log(usr);
		    if(!usr.emails || (usr.emails && usr.emails.length<=0)) res.redirect(shipperUrl(''));
            var user = {
		name:{
			firstname:usr.givenName||usr.name.givenName,
			lastname:usr.familyName||usr.name.familyName,
		},
		contact:{
		      email:usr.emails[0].value,	
		      mobile_number:null,	
		},
		status:1,				
        social:{'provider':'google',
				'uid':usr.id}, 
		password:Date.now(),
		image   :usr.image.url+"&sz=800",
                
	};
        Users.Model.findOne({'contact.email':user.contact.email},function(err,dta){
          if(err) return res.redirect(shipperUrl(''));
          if(dta){
            return isUserCompletedProfile(req,res,dta,next); 
          }else{
            registerSocial(req,res,user);
          }
        });         
        })(req, res, next);
});
function registerSocial(req,res,data){
  req.session.social= data;
  res.redirect(webappUrl('register'));	
  /*var user = Users.Add(data);
      user.hash_token = "fsfsdfsfsd ";
      user.save({validateBeforeSave:false},function(err){
        var image   = user.image;
        user.image = null;
        user.plocation  = 'user-type';
        UploadImage.setFilename(null);
        UploadImage.setImageSavePath(Common.media_dir_path(user._id,Users));
        UploadImage.getSocialImage(image,function(fn){
          user.image = fn;
          user.isvalidated=1;          
          user.save({ validateBeforeSave: false },function(er){
			registerActions(req,res,user,'user-type');  
            res.redirect(url);  
	 }); 
   });
 });*/  
}



function isUserCompletedProfile(req,res,user,next){
	req.session.user = {};
	if(user.status==Users.INACTIVE || user.hash_token!="")	
	   res.redirect(webappUrl(''));
	else if(user.plocation!="dashboard"){
	    registredUserProfile(req,res,user);
		res.redirect(shipperUrl(user.plocation));
	}else if(user.status==Users.ACTIVE && user.plocation=="dashboard"){ 
	   setAuthentication(req,res,user);
	   //AccessRules(req,res,next);
           return res.redirect(webappUrl(''))
	}else res.redirect(webappUrl(''));
	
}
function setAuthentication(req,res,user,s){ 
	req.session['_user_']  = user;	
	setpLocation(req,user.plocation,res);
}

// registration authentication...
function registredUserProfile(req,res,user){
    var url = webappUrl('');	
    req.session.register = {
	   _id:user._id,
	   token:MD5(user._id+Date.now()),
	   role:user.role,	  
	   redirectTo:user.plocation,	  
    };
    req.session.register.email = Validate.isEmpty(user.hash_token)?false:user.contact.email;
	url = user.plocation; 
    setpLocation(req,url,res);   	
}
function setpLocation(req,url,res){
  try{
     if(url){
         if(req.session.register)
		req.session.register['redirectTo'] = url;  
	 if(req.session.user.isLoaded) 
	    req.session.user.redirectTo = url;
	 else req.session.user = {uncprofile:url}; 
     }
  }catch(e){ //console.log(e.message);
	//res.redirect('/shipper/');  
  }
}
function validateRegister(req,res,next){
	try{
	    req.session.register._id;
	    validateAction(req,res,next);
	}catch(e){
	    res.redirect(webappUrl(''));	
	}
}
function validateAction(req,res,next){
 if(req.session.register && req.session.register.redirectTo){
    delete req.session.social;
	if(Users.ValidateIsShipper(req.session.register.role)){
	  return res.redirect(shipperUrl(req.session.register.redirectTo,function(url){
			       if(url=="dashboard") return 'users/dashboard';
					  return req.session.register.redirectTo;
		    }));
	}else if(Users.ValidateIsTrucker(req.session.register.role)){
		return res.redirect(truckerUrl(req.session.register.redirectTo,function(url){
			       if(url=="dashboard") return 'users/dashboard';
					  return req.session.register.redirectTo;
		    }));
	}else return res.redirect(webappUrl(''));
 }else return res.redirect(webappUrl(''));
}
function verifyEmail(req){
  var email =  req.session.register.email || false;
  if(email){
	  var url = "//www."+email.substring(email.lastIndexOf('@'));	
	return '<p class="text-danger">'+req.__('users').t_verify_email+'</p><a href="javascript:;" id="resend-verification-email">'+req.__('users').t_resend_email+'</a> | <a href="'+url+'" target="_blank">'+req.__('users').t_verify_now+'</a>';  
  }
}
  
router.post('/resend-email-token',function(req,res,next){ // resend verification email...
	var email,worker  = appHelper('Worker');
	if(req.session && req.session.register && req.session.register.email){
		email = req.session.register.email; 	
	}else if(req.body && req.body.email)  email = req.body.email;
	 else email =null;
	
	if(!email) return res.json({'errors':{'username':{'message':_locals().email_error}}});
	var token = MD5(email+Date.now())+":"+Date.now(); //{$set:{hash_token:token}},
	Users.Model.findOne({'contact.email':email},function(err,user){
	 if(user){
		worker.execPHP('Mailer.php '+email+" confirmemail "+token,function(err,r){
		         console.log(err);	 
		         console.log(r);
		    if(!err){
			   user.isvalidated= 1;	
			   user.hash_token=  token;
	           user.save(function(err){
				   console.log(err);
			   });
			   var url = "//www."+email.substring(email.lastIndexOf('@'));	
			   var html = '<p class="text-danger">'+req.__('users').t_verification_email_sent+'</p><a href="'+url+'">'+req.__('users').t_verify_now+'</a>';	
			   return res.json({success:Const.HTTP_SUCCESS,'display':{'html':html}});
			}else{
			   return res.json({'errors':{'username':{'message':err.message}}});
			}
			//next();
          }); 
	 }else{
		return res.json({'errors':{'username':{'message':_locals().email_error}}});
	 }	
	}); 
});
var csrf = require('csurf'),
    csrfProtection = csrf({ cookie: true });
router.post('/app-lang',function(req,res,next){ // resend verification email...
	if(!req.body.code)
	   req.session.applang = 'en';
	  req.session.applang = req.body.code;
	//Common.setSiteLanguage(req.body.lang);
	res.end();
});
function AccessRules(req,res,next){ 
	if((req.session.user && req.session.user._id)){ 
		switch(true){ 
			case (req.session.user.isShipper()):
			    return res.redirect(shipperUrl(req.session.user.redirectTo,function(url){
			       if(url=="dashboard") return 'users/dashboard';
					  return req.session.user.redirectTo;
		        }));
			break;
			case (req.session.user.isTrucker()):
				return res.redirect(truckerUrl(req.session.user.redirectTo,function(url){
			       if(url=="dashboard") return 'users/dashboard';
					  return req.session.user.redirectTo;
		        }));
			break;
			case (req.session.user.isAgent()):
				return res.redirect(agentUrl(req.session.user.redirectTo,function(url){
			       if(url=="dashboard") return 'users/dashboard';
					  return req.session.user.redirectTo;
		        }));
			break;	
			default:console.log(req.session['_user_']);
				return res.redirect(webappUrl(''));
				next();
			break;				
		}											   
	}else next();
}
module.exports = router;
