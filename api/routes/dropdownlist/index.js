var mongo = require('mongoose'),
express = require('express'),
states  = mongo.model('States',{},'states'),	
TruckLanes  = appModel('TruckLanes'),	
TruckLanes  = new TruckLanes(),	
router  = express.Router();
var csrf = require('csurf'),
    csrfProtection = csrf({ cookie: true });
router.post('/states',function(req,res){
	var st = states.find({status:1},'-_id name').lean();
	    st.exec(function(err,state){
		res.json(state);
	});
});
router.post('/__lp__',function(req,res){
	var pnts={};
	TruckLanes.Model.aggregate([
		{"$match":{status:1}},	
		{"$group":{'_id':{'source':"$source","state":"$source_state"}}},
	    {"$project" : {_id:0, city:"$_id.source", state:"$_id.state"}}
	],	
	function(err,tsource){
	   res.json(tsource);
  });
});

// get unloading points with fare and distance...
router.post('/_up_',function(req,res){
  var city = req.body.c,state=req.body.s,upd=[]; 
  TruckLanes.Model.aggregate([
		{"$match":{source:city}},	
		{"$group":{'_id':{'dest':"$destination","state":"$destination_state"}}},
        {"$project":{
			_id:0,
			"city":"$_id.dest",
			"ppk":"$price_per_km",
			"dst":"$distance",
			"state":"$_id.state",
		}},
  ],	
	function(err,up){
	  res.json(up);
  });
});
router.post('/tt-childernbyid',function(req,res){
	var TruckTypes  = appModel('TruckTypes'),
		TruckTypes  = new TruckTypes(); 
    TruckTypes.getChildrenByID(req.body.pid || false,function(tt){
		res.json(tt)
	});	
});
router.post('/truck-categories',csrfProtection,function(req,res){
	var Categories = appModel('Categories'),
	    Categories = new Categories;
	var parent_id = req.body.pid || "";
	Categories.Model.find({'parent_id':parent_id,status:1},'name',function(err,cats){
		res.json(cats);
	});	
});
module.exports = router;