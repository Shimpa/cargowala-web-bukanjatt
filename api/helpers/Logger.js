"use strict";
var worker  = appHelper('Worker');
class ExtendableError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.message = message; 
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else { 
      this.stack = (new Error(message)).stack; 
    }
  }
} 
Object.defineProperty(module.exports,'Logger',{
	configrable:false,
	enumerable :true,
	get:function(){ return Logger;}
});
class Logger extends ExtendableError{
	constructor(message){
		super(message);
		this.message = message || 'Custom message';
		this.error();
	}
 error(){ //console.log(this.stack)
	var w = worker.fork('Trace');	  
	if(typeof message !=String)
	var message =  this.message.toString() || this.message	
	w.send({'message':message}); 
 }
 trace(){
	 
 }	
	
}
