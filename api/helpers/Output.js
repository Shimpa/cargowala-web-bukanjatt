"use strict";
var exports = module.exports; 
var Const   = appHelper('Const');
Object.defineProperty(exports,'Output',{
	configrable:false,
	enumerable :true,
	get        :function(){ return new Output(); }
});
var res = Symbol('res');
class Output{
	constructor(){
		this[res] = '';
	}
 init(res){
	this[res] = res;
	//return this.sendMsg; 
 }	
 sendMsg(errCode,response,msg,_req,_res,next){ 
  var msg = !msg?null:msg;
      msg = Const.getMessage(errCode,msg);
  var ress= {
           status_code: errCode,
           ack: errCode==200?"success":"error",
           //message: msg,
	       sToken  : _req.session.sToken || false,
           response:response || []
         };	
		
	return ress;
  }
}