"use strict";

Object.defineProperty(module.exports,'RegularExp',{
	configrable:false,
	enumerable :true,
	get:function(){ return (new RegularExp());}
});
class RegularExp{
	constructor(){
		
	}
validate(re,v){ //console.log("Exp: "+re);
	//console.log("Val: " + v);
	return re.test(v);
}	
isEmail(v){ 
	 return this.validate(new RegExp("[^@]+@([^@\.]+\.)+[^@\.]","igm"),v);
 }
 isMobileno(v){
	 return this.validate(new RegExp("^[0-9]{10,10}$"),v);
 }
 get mobileno(){ 
	 return "^[0-9]{10,10}$";
 }	
 isString(min,max,v){
	return this.isText(min,max,v); 
	
 }
 isText(min,max,v){
	var rg  = '^[a-zA-Z0-9]',min =  min || '',max=max ||'';
	    rg +=(min || max)?'{'+min+','+max+'}':'';
	    rg += '$';
	return  this.validate(new RegExp(rg),v); 
	
 }
 isTextWithSpace(min,max,v){
	var rg  = '^[a-zA-Z0-9\\s]',min =  min || '',max=max ||'';
	    rg +=(min || max)?'{'+min+','+max+'}':'';
	    rg += '$';
	return  this.validate(new RegExp(rg),v); 
	
 }	
 isNumber(min,max,v){ //console.log(v);
	var rg  = '^[\\d]',min =  min || '',max=max ||'';
	    rg +=(min || max)?'{'+min  +',' + max+ '}':'';
	    rg += '$';
	 return  this.validate(new RegExp(rg),v);   
 }
 isUrl(url){
	var url =require('url').parse(url);
	if(url.host && url.path) return true;
	  return false;
 }
/* text validation must be alphabatic */ 	
 text(min,max,pattern){
	var min = min || 5,
		max = max || 15,
		p   = pattern || '^[^0-9]{'+min+','+max+'}$';  
	return p; 
 } 
/* alphanum validation must be alpha numeric chracters */ 	
 alphnum(min,max,pattern){
	var min = min || 6,
		max = max || 15,
		p   = pattern || '[a-Z0-9]{'+min+','+max+'}$';  
	return p; 
 } 
 password(v){ 
	return this.validate(/^([^\s\\]){6,15}$/,v);
}
// indian vehicle no...
indVehicleNo(v){
  return this.validate(new RegExp('^[A-Z]{2}\s[0-9]{2}\s[A-Z]{2}\s[0-9]{4}$'),v);	
}	
	
	
}