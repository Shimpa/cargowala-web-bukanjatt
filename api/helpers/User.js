"use strict";
var usr = Symbol('usr1');
var Common= appHelper('Common');
var Users= appModel('Users');
var Users= new Users();
class User{
constructor(session){
	if(session.trucker){
		session['_trucker_'] =  session['_trucker_'] || session.trucker;
		this[usr] = session['_trucker_'];
	}else if(session.shipper){
		session['_shipper_'] =  session['_shipper_'] || session.shipper;
		this[usr] = session['_shipper_'];
	}else if(session.user){
		this[usr] = session['_user_'];
		
	}
} 
set uncprofile(url){ 
	this.redirectTo =  url;
}
get uncprofile(){ 
	return this.redirectTo; 
}
set redirectTo(url){
	this._url =  url;
}
get redirectTo(){
	return this[usr].plocation || this._url;
}	
get _id(){
	return this.ID;
}	
get ID(){
	return this[usr]?this[usr]._id:false; 
}
getName(){
	try{
	return this[usr].name.firstname+ " " + this[usr].name.lastname;
	}catch(e){}
}
getImage(){
	try{
 	return Common.MediaPath(this[usr],'image',Users);
	}catch(e){}
}
deviceToken(){
  try{	
  return this[usr].device['token'] || false;	
  }catch(e){
	  return false;
  }
}
getEmail(){
  return this[usr].contact.email;	
}
getMobileNo(){
  return this[usr].contact.mobile_number;		
}
getBusinessName(){ 
  return this[usr].business?this[usr].business.name:false;
}
getRating(){
 return this[usr].rating || 0; 	
}	
isShipper(){
  return Users.ValidateIsShipper(this[usr].role) || false;	
}
isTrucker(){
  return Users.ValidateIsTrucker(this[usr].role);	  	
}
isAgent(){
  return Users.ValidateIsAgent(this[usr].role);	  	
}
asAgent(){
  return this[usr].parent_role && Users.ValidateIsAgent(this[usr].parent_role);	  	
}	
isActive(){
  return this[usr] && this[usr].status==1;	
}
get isIndividualTrucker(){
	return this[usr].role==Users.INDIVIDUALTRUCKER;
}
get isBusinessTrucker(){
	return this[usr].role==Users.TRUCKERCOMPANY;
}	
loadUser(user){
	if(user._id){
	   this[usr] = user; 
	}
}
get isFreeMember(){
	if(this[usr] && this[usr].membership){
	   return this[usr].membership.plan_code==Users.FREE_MEMBER; 	
	}
}
get isGoldMember(){
	if(this[usr] && this[usr].membership){
	   return this[usr].membership.plan_code==Users.GOLD_MEMBER; 	
	}
}
get isSilverMember(){
	if(this[usr] && this[usr].membership){
	   return this[usr].membership.plan_code==Users.SILVER_MEMBER; 	
	}
}
get isPaidMember(){
	if(this[usr] && this[usr].membership){
	   return this[usr].membership.plan_type==Users.SILVER_MEMBER || this[usr].membership.plan_type==Users.GOLD_MEMBER; 	
	}
}	
get pendingTransaction(){
	if(this[usr] && this[usr].membership){
	  return parseInt(this[usr].membership.transactions);
	}
}
get isMembershipExpired(){
	try{
	if(this[usr] && this[usr].membership){
		return !this.isFreeMember && this[usr].membership.expiry_date<DateTime.now()?'Expired':false;
	}
	}catch(e){
		
	}
}
get planText(){ 
	if(this.isFreeMember)
	   return 'Free Plan Member';
	if(this.isSilverMember)
	   return 'Silver Plan Member';
	if(this.isGoldMember)
	   return 'Gold Plan Member';
	
 }
 get isMobileVerified(){
	try{ 
	   return (this[usr] && ('otp' in this[usr]) &&  parseInt(this[usr].otp['verified'])==1); 
	}catch(e){
		
	}
 } 
  get isOTPSent(){
	try{ 
	   return (this[usr] && ('otp' in this[usr]) &&  parseInt(this[usr].otp['otp_no'])>0); 
	}catch(e){
		
	}
 } 	
}
var exports = module.exports; 
Object.defineProperty(exports,'User',{
	configrable:false,
	enumerable :true,
	get        :function(){ return User; }
});
