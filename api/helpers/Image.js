"use strict";
var fs = require('fs'),
	gm = require('gm').subClass({imageMagick: false});
var exports = module.exports;  
Object.defineProperty(exports,'Image',{
	configrable:false,
	enumerable :true,
	get        :function(){ return new Image(); }
});
let img  = Symbol('img');
let _class = Symbol('instance');
let imgtype = Symbol('type');
let mdl  = Symbol('model');
let idx  = Symbol('index');
let ext = Symbol('extensions');
let size = Symbol('size');
let allowedsize = Symbol('allowedsize');
let path = Symbol('path');
let tmpf = Symbol('tmpfile');
let prefix = Symbol('prefix');
class Image{
	constructor(indx,ext,size){
		this.allowedExtensions = ['jpg','png','gif','bmp'];
		this.allowedSize = 2;
		this.currentFilename = "";
		this[imgtype]  = null;
		this[mdl]     = null;
		this[idx]     = indx;
		this[path]    = null;
		this[tmpf]    = null;
		this[prefix]  = null;
		return this[img];
}
base64(im){
	this[img] = im;
	this.setType();
	this[img] = this[img].replace(/^data:image\/[a-z]{3,5};base64,/,"");
	return this;
}
webUrl(u,p,cb){ this[path] =p;
  var u         = u.split("?");
  var arr       = u[0].split(".");
  var fname= this[idx] || Date.now(),$this = this;
      this[ext] = arr.pop();
  var url = require('url');
  url     = url.parse(u[0]);
  var module = (url.protocol=="https:")?require('https'):require('http');
  module.get({
	   host:url.host,
	   path:url.path,	   
   },function(_res){ 
    var stream = fs.createWriteStream($this[path]+fname); 
	_res.pipe(stream);
	$this[img]  =fname;   
	$this.Quality(50,$this[path]+fname,$this[path]+fname);	
	$this.Quality(0.1,$this[path]+fname,$this[path]+'load-'+fname);
        cb.call(cb,fname+"jpg");
  });
}
getSocialImage(u,cb){
  var fname= this[idx] || Date.now(),$this = this;
  var request = require('request');
  request(u).pipe(fs.createWriteStream($this[path]+fname+".jpg"))
    .on('close', function(){
        $this[img]  =fname;   
	$this.Quality(50,$this[path]+fname+".jpg",$this[path]+fname);	
	$this.Quality(0.1,$this[path]+fname+".jpg",$this[path]+'load-'+fname);
        cb.call(cb,fname+".jpg");
    });
}
Quality(fltr,imgs,saveto){
var imgs  = imgs || this[path]+this[img],
	saveto = saveto || imgs; 	
var image = gm(imgs).flatten().interlace('Line');
	image.quality(fltr);
	image.write(saveto,function(err){
	//console.log("dsadas");	
	//console.log(err);
	});
}	
validateType(){
  var type  = {
	  "jpg":"image/jpg",
	  "jpg":"image/jpeg",
	  "png":"image/png",
	  "bmp":"image/bmp",
  };
  for(var e in this.allowedExtensions){
	  if(this[imgtype]== type[this.allowedExtensions[e]])
		 this[ext] = this.allowedExtensions[e];  
  }
 this.allowedExtensions = null;
 //console.log(this[imgtype]);	
 //console.log(this[ext]);	
 return this[ext];	  
}
validateSize(){
	var s =  this[size]<=this.allowedSize;
	this[size] = this.allowedSize  = null;
	//console.log(s);
	return s;
}
validateImage(){
	 var file          = fs.statSync(this[tmpf]);
	 this[size]    = file['size']/1024/1024;
}
setFilename(n){
 this[idx] =n
}
setFilenamePrefix(pf){
 this[prefix] =pf
}	
setImageSavePath(p){
 this[path] = p;
 return this;
}
upload(p){ this[path] = p; 
    var fn   = +Date.now()*Math.random(1)*100,
		fname= this[prefix] || Date.now(),
	    $this = this;	  
		this[tmpf] ='/tmp/'+fn;
	var err = fs.writeFileSync(this[tmpf],this[img],'base64');
	$this.validateImage();
	if($this.validateSize() && $this.validateType()){
	   delete $this[img];
	   delete $this[mdl];
	   delete $this[idx];
	   $this[img]  =this[idx] || fname+".jpg";
	   $this.Quality(50,$this[tmpf],$this[path]+$this[img]);	
	   $this.Quality(0.1,$this[tmpf],$this[path]+'load-'+$this[img]);
	return $this[img];
	}else{ console.log("error while uploading image."); return " "; }
	
}
setType(){
  var mtch = this[img].match(/image\/[a-z]{3,5}/gi);
	this[imgtype] = mtch[0] || false;
}	
internalupload(){
 this.validateSize() && this.validateType() &&  this.saveImage();
}
}