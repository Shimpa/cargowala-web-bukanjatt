"use strict";
var fs   = require('fs');
var path = require('path');
Object.defineProperty(module.exports,'Common',{
	configrable:false,
	enumerable :true,
	get:function(){ return (new Common());}
});
const media = Symbol('media');
const media_path  = "../media";
const DS  ="/";
class Common{
	constructor(){
		this[media] =media_path;
	}
deviceToken(device){
	try{
		return device['token'] || false;
	}catch(e){
		return false;
	}
}	
  mediapath(){
	return path.resolve(this[media])+DS;  
  }
  mediaurl(){
	return site_url+DS+"media/";  
  }	
  media_dir_path(id,model){
    var p =this.mediapath()+model.parent_dir;
	this.mkdir(p,true);
	p=p+DS+model.child_dir+"-"+id; 
	this.mkdir(p,true);  
	return p+DS;  
 }	
  mkdir(pt,abs){
	var p = abs?pt:this[media];
		p = path.resolve(p)+DS;
	try{
	var r = fs.statSync(p);
	    r.isDirectory() || fs.mkdirSync(p);	    		
	}catch(e){ 
		if(e.errno===-2)
		   fs.mkdirSync(p);
		else new Logger(e.message);
		
	}  
  }
  noImage(){
	return '/img/no-image.png';  
  }	
  OnlyPath(model,id){
	if(!id)
	   id =  model._id	
	try{   
	return '/img/'+model.parent_dir+DS+model.child_dir+"-"+id+DS;	
	}catch(e){ return "";}
  }
  MediaPath(data,index,model){
	try{  
	var path = this.OnlyPath(model,data['_id']||false),
	    image= data[index] || false;
		if(!image)
		   return this.noImage();	
	return path+image;  
	}catch(e){}
  } 	
  ImagePath(data,index,model){
	if(!index || !data[index])return '';
	var path = model.parent_dir+DS+model.child_dir+"-"+data._id+DS;
	try{  
    var r    = fs.statSync(this.mediapath()+path+data[index]);
	    if(r.isFile())return this.mediaurl()+path+data[index];
	       return '';
	}catch(e){
		return '';
	}
  }
  beforeImageLoad(data,index,model){
	var path = this.OnlyPath(model,data['_id']||false),
	    image= "load-"+data[index] || "";
	return path+image;   
  } 	
  delImage(data,index,model){
	if(!index || !data[index])return;
	var p = model.parent_dir+DS+model.child_dir+"-"+data._id+DS,$this=this;
	fs.unlink($this.mediapath()+p+data[index],function(err){
		if(err) new Logger(err);
		fs.unlink($this.mediapath()+p+"load-"+data[index]);
	});  
  }
  dateToString(dt){ if(!dt)return;
	dt =  (new Date(dt)).toISOString().split('T')[0];
	return dt;
				  
  }
makeISODate(date){ if(!date)return null; 
  var dt = date.split("-");
      dt = new DateTime(dt[1]+"/"+dt[2]+"/"+dt[0]+" 23:00:00").toISOString();
	return dt;					   
}
// get logged in user info ...
getLoggedUserInfo(req,key){
	try{ //console.log(req.session);
		if(req.session){ 
			if(key) return req.session._user_[key];
	      return req.session._user_;
		}
	}catch(e){
		console.log(e.message)
	}
}
validImageType(imgType){
	var type  = {
	  "jpg":"image/jpg",
	  "jpg":"image/jpeg",
	  "png":"image/png",
	  "bmp":"image/bmp",
  };
  for(var e in type){
	  if(imgType == type[e])
		 return true;  
  }
}	
validImageSize(imgSize){
  var allowedSize =  1.5;	
  return (imgSize/1024/1024)<allowedSize;
	
}
replaceValue(str,arg1,arg2){
  var str1 =str;	
  if(Array.isArray(arg1) && Array.isArray(arg2))
	for(var i in arg1){
		str1=str1.replace(arg1[i],arg2[i]);
	}
	return str1;
}
// percentize amout into parts
percentizeAmount(amount){ 
	var txt="",amount = parseFloat(amount);
	txt+=(amount*25)/100+",";
	txt+=(amount*50)/100+",";
	txt+=(amount*75)/100+",";
	txt+=amount;
	return txt;	
}
amountStep(amount){
	var amount = parseFloat(amount);
	return (amount*25)/100;
}
amountRemain(amount){
	var amount = parseFloat(amount);
	return (amount*75)/100;
}
/* check that is shipment expired or not
@params transit (object) contains shipment expire timestamp
@return bool true|false
*/
	
isShipmentExpired(transit){
	if(transit && transit.t_expdatetime){
		return Date.now()>transit.t_expdatetime;
	}
}
/* genrate security signature for citrus payment gateway
 * @params merchantTxnId (number) a unique number enclsode with transaction
   amount (curreny) used as signature param
 * @return hmac.digest('hex') (string) a signature string
 */
generateSignature(merchantTxnId, amount) {
         var crypto = require('crypto');  	
	
        //Need to change with your Secret Key
        var secret_key = Const.citrus.SECRET_TOKEN; 
            
        //Need to change with your Access Key
        var access_Key = Const.citrus.ACCESS_KEY; 
        //Should be unique for every transaction
        var txn_id = merchantTxnId;//randomString({ length: 20 });
        //Need to change with your Order Amount
     
        //Need to change with your Return URL
        var returnURL = Const.citrus.RETUREN_URL;
        //Need to change with your Notify URL
        var notifyUrl = Const.citrus.NOTIFY_URL;

        var data = 'merchantAccessKey=' + access_Key + '&transactionId=' + txn_id + '&amount=' + amount;
              
        // generate hmac
        var hmac = crypto.createHmac('sha1', secret_key);
        hmac.update(data);
        return hmac.digest('hex');
    }	
}
