"use strict";
var exports = module.exports;  
Object.defineProperty(exports,'Cache',{
	configrable:false,
	enumerable :true,
	get        :function(){ return (new Cache()); }
});
var memcache = new WeakMap();
class Cache {
	constructor(){
	this.mcExpire = 3600*24;	
	let Memcached = require('memcached'),
	mc = new Memcached('127.0.0.1:11211', {retries:10,retry:10000,remove:true,failOverServers:['192.168.0.103:11211']});
		memcache.set(this,mc);
	}

	
  setMC(k,v,expire){
	var expire = expire || this.mcExpire;    
	(memcache.get(this)).set(k,v,this.mcExpire,function(err){
		err && new Logger(err);
		
	});  
  }
  getMC(k,cb){
	(memcache.get(this)).get(k,cb);  
  }	
  get MCInstance(){ //console.log(memcache.get(this));
	return memcache.get(this);  
  }	
}
//init = memcache =null;