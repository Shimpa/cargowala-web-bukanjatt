"use strict";
var worker  = appHelper('Worker');
var re      = appHelper('RegularExp');
var Users   = appModel('Users'),
    Queue   = appModel('Queue'),
	Queue   = new Queue,
	Users   = new Users;
var mobile_worker_busy,mobile_worker=1; //false=free,true=busy;
var email_worker_busy,email_worker=1; //false=free,true=busy;
class Notify{

sendSMS(mobile_no, msg,priority) {
	try {
		if (!re.isMobileno(mobile_no)) return;
        //if(mobile_worker_busy){ // if busy
		   Queue.Add2Queue(mobile_no,msg,'mobile',priority);
		   return;	
		//}
		var sms = worker.fork('SmsNotification');
		sms.on('message', function (res) {
			if (res.finishedTask){ mobile_worker_busy = false;
			    mobile_worker-=1;
			}
			if (!mobile_worker_busy && mobile_worker < 2) {
				Queue.getNext10Rows('mobile',function(err,rows){
					var smsworkerchild = worker.fork('SmsNotification');
				        mobile_worker += 1;
				        smsworkerchild.send({
							   queue:rows 
				        });
				});
			}
		});
		sms.send({
				queue: false,
			    mobile_no:mobile_no,
			    message:msg
		});
	mobile_worker_busy = true;
} catch (e) {
	console.log(e.message);
}
}
sendPush(device_token,msg,object){
 try{
	if(!re.isString(device_token,32))return; 
	//if(email_worker_busy){ // if busy	
		Queue.Add2QueuePushNotification(device_token,msg,object);	
	    return;	
	//}  
	 
	if(!re.isString(device_token,32))return; 
	var push = worker.fork('PushNotification');  
	    push.on('message',function(res){
			
	});
	    push.send({device_token:device_token,message:message});	 
 }catch(e){
	    console.log(e.message);
 }
}
sendMail(email,msg,subject){
 try{ 
	if(!re.isEmail(email))return; 
	//if(email_worker_busy){ // if busy	
		Queue.Add2Queue(email,msg,'email',subject);	
	    return;	
	//} 
	var emailw = worker.fork('EmailNotification');  
	    emailw.on('message',function(res){
		 if (res.finishedTask){ email_worker_busy = false;
			    email_worker-=1;
			}
		if (!email_worker_busy && email_worker < 2) {
				Queue.getNext10Rows('email',function(err,rows){
					var emailw = worker.fork('EmailNotification');
				        email_worker += 1;
				        emailw.send({
							   queue:rows 
				        });
				});
			}	
	});
	if(Array.isArray(email)){
			for(var e in email){
		         emailw.send({
				queue: false,
			    email:email[e],
			    message:msg
		});
		}
				
		}else{ emailw.send({
				queue: false,
			    email:email,
			    message:msg
		}); 
			 }
	    	 
 }catch(e){
	   console.log(e.message);
 }
}	
webNotification(message,user_id,can_view){
	var WebNotify = appModel('WebNotifications'),
	    WebNoify  = new WebNotify();
	    WebNoify.notify(message,user_id,can_view);	
}	
	
bidUpdated(shipper_id,shipment){
  this.Shipper(shipper_id,shipment);	
}	
	
}
var exports = module.exports; 
Object.defineProperty(exports,'Notify',{
	configrable:false,
	enumerable :true,
	get        :function(){ return new Notify(); }
});