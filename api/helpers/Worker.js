"use strict";
var cp= require('child_process');
var path= require('path');
Object.defineProperty(module.exports,'Worker',{
	configrable:false,
	enumerable :true,
	get:function(){ return (new Worker());}
});
class Worker{
	constructor(){
		
	}
 fork(filename){
	return cp.fork('./Workers/'+filename); 
 }
 spawn(cmd,filename){
	return cp.spawn(cmd,'./Workers/'+filename); 
 }
 exec(filename,callback){
	return cp.exec('./Workers/'+filename,callback); 
 }
 execPHP(filename,callback){
	var p  = path.normalize('./Workers/'+filename);
	return cp.exec('php '+p,callback); 
 }
}