<?php 
class MailerCron{
    private $name,$email_to;
	private $records =[];
	protected $collection;
    private $isSmtp = false;
	private static $count=0;
	protected static $smscount=0;
	public function __construct(){
		
		$this->checkDB();
		$this->sendMail();
    }
	private function checkDB(){
	    $m = new MongoClient(); // connect
        $db = $m->selectDB("cargowla-dev");	
	    $this->collection = new MongoCollection($db, 'queue');
	}
	private function getNext10(){
		return $this->collection->find(['action'=>'email','status'=>0])->limit(10);
	}
	private function mailHeaders(){
    $from    = "no-reply@cargowala.com";
    $headers  = "From: " . strip_tags($from) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
    $headers .= "Organization: Sender Cargowala.com\r\n"; 
    $headers .= "Return-Path: {$from} \r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
    $headers .= "Content-Transfer-Encoding: binary";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    return $headers;
}	
	
	private function sendMail(){		
		set_time_limit(0);
        ignore_user_abort(true);
		$results = $this->getNext10();
		while($results->hasNext()){ $mail = $results->getNext();
			 if(isset($mail['member'])){
				 $body = '<p>'.$mail['message'].'</p>';
				 $subject = empty($mail['subject'])?'Cargowala Email':$mail['subject'];
				 mail($mail['member'],$subject,$body,$this->mailHeaders());
                 ++self::$count;       
                 if(self::$count==450){ self::$count=0; sleep(20);}
				 $this->changeStatus($mail['member']); 
			  }
		}
	}
	protected function changeStatus($mail){
		$this->collection->remove(['member'=>$mail]);
		echo time();
	}
}

class SMSCron extends MailerCron{
	public function __construct(){
		parent::__construct();
		$this->sendSMS();
		
	}
	private function getNext10SMS(){
		return $this->collection->find(['action'=>'mobile','status'=>1])->limit(10);
	}
	private function sendSMS(){
		set_time_limit(0);
        ignore_user_abort(true);
		$results = $this->getNext10SMS();
		while($results->hasNext()){ $sms = $results->getNext();		   
            if($sms['member']){
				$message = $sms['message'];
				$url ="http://121.242.224.32:8080/smsapi/httpapi.jsp";
				$url.="?username=trigmaht&password=trigma&";
			    $message = str_replace(array("<br />","<br/>","<br>"), "\n", $sms['message']); 
				$message = wordwrap( $message, 70 );
				$url.="from=CARGOW&to=".$sms['member']."&text=".urlencode(utf8_encode($message))."&coding=0";
 			    
				//$this->sendSMS2(urlencode($url));
				//echo $url;
				file_get_contents($url);
				++self::$smscount;       
                if(self::$smscount==450){ self::$smscount=0; sleep(20);}
				//$this->changeStatus($sms['member']);
			}								    
		}
	}
	private function sendSMS2($url){
		$curl_handle=curl_init();
curl_setopt($curl_handle, CURLOPT_URL,$url);
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Cargowala.com');
$query = curl_exec($curl_handle);
curl_close($curl_handle);
	}
}
 new SMSCron();
?>