
/** membership indexes **/
db.getCollection("membership").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** membership records **/
db.getCollection("membership").insert({
  "_id": ObjectId("579b3f1ae1ec61b67b8b4606"),
  "name": "Gold",
  "price": "2500",
  "duration": "9",
  "transactions": "6",
  "description": [
    "Gold Membership",
    "The classic pay more, get more structure.",
    "When the content is all the same, but you offer different payment options.",
    "When the content is unique to each level.",
    "When the level selection is based on the member, be it individual or group.",
    "When the content itself is offered in different methods based on level selection.",
    "Mainly for non-profits with creative membership level tiers."
  ],
  "mtype": {
    "code": "2",
    "text": "Gold"
  },
  "created_on": "2016-07-29 17:03:46",
  "status": NumberLong(1)
});
db.getCollection("membership").insert({
  "_id": ObjectId("579b3fc8e1ec61b67b8b4607"),
  "name": "Silver",
  "price": "2000",
  "duration": "9",
  "transactions": "4",
  "description": [
    "Silver Membership",
    "The classic pay more, get more structure.",
    "When the content is all the same, but you offer different payment options.",
    "When the content is unique to each level.",
    "When the level selection is based on the member, be it individual or group.",
    "When the content itself is offered in different methods based on level selection.",
    "Mainly for non-profits with creative membership level tiers."
  ],
  "mtype": {
    "code": "1",
    "text": "Silver"
  },
  "created_on": "2016-07-29 17:06:40",
  "status": NumberLong(1)
});
db.getCollection("membership").insert({
  "_id": ObjectId("57a4424be1ec61785b8b4e50"),
  "name": "test",
  "price": "12",
  "duration": "test",
  "transactions": "test",
  "description": [
    "test1",
    "test2"
  ],
  "mtype": {
    "code": "1",
    "text": "Silver"
  },
  "created_on": ISODate("2016-08-05T07:37:47.482Z"),
  "status": NumberLong(1)
});
db.getCollection("membership").insert({
  "_id": ObjectId("57a45d37e1ec61b77b8b605e"),
  "name": "trigma",
  "price": "22",
  "duration": "2",
  "transactions": "222",
  "description": [
    "ankur",
    "aman",
    "mandeep",
    "ajay"
  ],
  "mtype": {
    "code": "1",
    "text": "Silver"
  },
  "created_on": ISODate("2016-08-05T09:32:39.873Z"),
  "status": NumberLong(1)
});
db.getCollection("membership").insert({
  "_id": ObjectId("57a46362e1ec61a2058b5f54"),
  "name": "test",
  "price": "12",
  "duration": "2",
  "transactions": "222",
  "description": [
    "  member",
    "hello bhai ",
    "hello bhai 1",
    "hello bhai 2"
  ],
  "mtype": {
    "code": "1",
    "text": "Silver"
  },
  "created_on": "2016-08-05 15:28:58",
  "status": NumberLong(1),
  "modified_on": ISODate("2016-08-09T06:03:30.683Z")
});
db.getCollection("membership").insert({
  "_id": ObjectId("57a9d5cbe1ec610d1f8b4623"),
  "name": "",
  "price": "",
  "duration": "",
  "transactions": "",
  "description": [
    ""
  ],
  "mtype": {
    "code": "2",
    "text": "Gold"
  },
  "created_on": ISODate("2016-08-09T13:08:27.397Z"),
  "status": NumberLong(1)
});
