
/** tax_terms indexes **/
db.getCollection("tax_terms").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** tax_terms records **/
db.getCollection("tax_terms").insert({
  "_id": ObjectId("570e0ae6e1ec61893a8b45fb"),
  "term": "Tax",
  "name": "Priority Delivery",
  "rate": "25",
  "priority": "1",
  "status": NumberLong(1),
  "created_by": "56fa5ff715e704450e09aff7",
  "created_on": "2016-04-17 09:05:54",
  "modified_on": ISODate("2016-04-18T10:38:59.845Z"),
  "ttype": "Percentage"
});
db.getCollection("tax_terms").insert({
  "_id": ObjectId("57135221e1ec6100228b4568"),
  "term": "Tax",
  "name": "Service Tax",
  "rate": "14.5",
  "priority": "12",
  "status": NumberLong(1),
  "created_by": "56fa5ff715e704450e09aff7",
  "created_on": "2016-04-17 09:12:29",
  "modified_on": ISODate("2016-04-18T10:38:42.725Z"),
  "ttype": "Percentage"
});
db.getCollection("tax_terms").insert({
  "_id": ObjectId("5713536ce1ec6101228b456a"),
  "term": "0",
  "name": "Term Insurance",
  "rate": "200",
  "priority": NumberLong(2),
  "status": NumberLong(1),
  "created_by": "56fa5ff715e704450e09aff7",
  "created_on": ISODate("2016-04-17T09:12:12.410Z"),
  "ttype": "0"
});
