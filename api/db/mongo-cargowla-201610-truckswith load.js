
/** trucks indexes **/
db.getCollection("trucks").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** trucks records **/
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea75b9eedd01de0d8b4567"),
  "created_by": ObjectId("56d6e687eedd01c5268b4568"),
  "created_on": ISODate("2016-03-17T09:20:11.979Z"),
  "desc": "Tata Ace is a product from TATA Group, having a loading capacity of 2 tons.",
  "loading_capacity": "02",
  "modified_on": ISODate("2016-03-17T09:20:11.979Z"),
  "name": "Tata Ace",
  "status": NumberInt(1),
  "truck_type": "1"
});
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea78aceedd01ba198b4567"),
  "truck_type": "1",
  "name": "Tata 407",
  "loading_capacity": "4",
  "desc": "",
  "status": NumberInt(0),
  "created_by": ObjectId("56d6e687eedd01c5268b4568"),
  "created_on": ISODate("2016-03-17T09:28:12.240Z")
});
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea7e63eedd01bb0c8b4567"),
  "truck_type": "2",
  "name": "14 Feet Open",
  "loading_capacity": "9",
  "desc": "This truck is under Medium truck category.",
  "status": NumberInt(1),
  "created_by": ObjectId("56d6e687eedd01c5268b4568"),
  "created_on": ISODate("2016-03-17T09:52:36.100Z")
});
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea7ebceedd011b198b4567"),
  "truck_type": "2",
  "name": "14 Feet Close",
  "loading_capacity": "8",
  "desc": "This truck is beneficial for transporting sensitive things",
  "status": NumberInt(1),
  "created_by": ObjectId("56d6e687eedd01c5268b4568"),
  "created_on": ISODate("2016-03-17T09:54:04.969Z")
});
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea7effeedd01841a8b4567"),
  "truck_type": "3",
  "name": "24 Feet Close",
  "loading_capacity": "12",
  "desc": "This truck is under heavy truck category. ",
  "status": NumberInt(1),
  "created_by": ObjectId("56d6e687eedd01c5268b4568"),
  "created_on": ISODate("2016-03-17T09:55:11.890Z")
});
db.getCollection("trucks").insert({
  "_id": ObjectId("56ea7fb2eedd01bb0c8b4568"),
  "created_by": ObjectId("56d80bc7eedd01771a8b4567"),
  "created_on": ISODate("2016-03-18T08:03:30.178Z"),
  "desc": "This truck is from The \"Ashok Lay-land Brand\" and usually beneficial to transport  the heavy material like building material, raw material.",
  "loading_capacity": "16",
  "modified_on": ISODate("2016-03-18T08:03:30.178Z"),
  "name": "Ashok Layland",
  "status": NumberInt(1),
  "truck_type": "4"
});
