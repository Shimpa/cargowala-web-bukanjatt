
/** admin_settings indexes **/
db.getCollection("admin_settings").ensureIndex({
  "_id": NumberInt(1)
},[
  
]);

/** admin_settings records **/
db.getCollection("admin_settings").insert({
  "_id": ObjectId("57653a05e1ec61c5048b463e"),
  "admin_email": "asd@sf.sdf",
  "company_name": "sdf",
  "company_logo": "company_logo_1466254997.png",
  "lane_rate": "23",
  "insurance_rate": "2",
  "priority_delivery_rate": "2",
  "created_on": "2016-06-18 12:09:41",
  "modified_on": ISODate("2016-06-18T13:03:17.880Z")
});
