var compression   = require('compression')
var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var mongo         = require('mongoose');
var app           = express();
var shipper       = express();
var trucker       = express();
var session       = require('express-session');
var expressLayouts= require('express-ejs-layouts');
var compressor    = require('node-minify');
//const MongoStore  = require('connect-mongo')(session);



//compression...
app.use(compression({filter: shouldCompress}))

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false
  }

  // fallback to standard filter function
  return compression.filter(req, res)
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'mylayout');
app.use(expressLayouts);
// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'book-truck.png')));
//app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({limit:'5mb'}));
app.use(bodyParser.urlencoded({ extended: true ,limit:'5mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'),{ maxAge: 3600*60*1 }));

mongo.connect('mongodb://127.0.0.1:27017/cargowla'); // database connection... 

// session uses

app.use(session({
    /*store: new MongoStore({
        //url: 'mongodb://127.0.0.1:27017/cargowla',
        mongooseConnection: mongo.connection,
		autoRemove: 'interval',
        autoRemoveInterval: 60*60*24*2,
		touchAfter: 24 * 3600		
    }),*/
	resave: false,
	saveUninitialized: true,
	secret: 'dsfhldfhdh0987070',
}));

// memcached ..




global.appLang    = 'en';
global.MD5        = require('md5');
global.appModel   = require('./models/');
global.appHelper  = require('./helpers');
global.Const      = appHelper('Const');
global.Logger     = appHelper('Logger');
global.Common     = appHelper('Common');
global.Cache      = appHelper('Cache');
//appHelper('Objectlib');

global.appLanguage= function(){
   var language   = 'en';	
   return require('./messages')(language);	
}
/*
app.use(function(req, res, next){
            res.setHeader("Access-Control-Allow-Origin", "http://abcd:3001")
            res.setHeader("Access-Control-Allow-Methods", "POST, GET")
            res.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, Accept")
            res.setHeader("Access-Control-Max-Age", "1728000")
            next();
});
*/

/* minify js and css */
// Using Google Closure 
var f = new compressor.minify({
  type: 'yui-js',
  fileIn: './public/js/bootstrap.js',
  fileOut: './public/js-dist/base-min-gcc.js',
  callback: function(err, min){
    console.log(err);
    //console.log(min); 
  }
});




app.use([setLanguage,setBaseUrl]);
//app.use('api/*',authenticate)
//app.use('/v1/trucker/',trucker);
app.use('/shipper/',shipper);
app.use('/trucker/',trucker);
shipper.use('/',require('./routes/index'));
require('./shipper-routing')(shipper);
require('./trucker-routing')(trucker);
shipper = trucker = null;
// set application language...
function setLanguage(req,res,next){
	var route = req.url.split("/"),
		file  = route[2];
	global.appLang = route[3];	
	next();
}
// set site base url...
function setBaseUrl(req,res,next){
	var host  = req.get('host');
	global.site_url  = host || "http://localhost";	
	next();
}
//res.removeHeader("X-Powered-By");
/* authenticate every request
   validate is it valid request
*/
function authenticate(req,res,next){
	//console.log(req.session.sToken); 
	next();
}
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
	//new Logger(req.url+"("+err.status|| 500+":"+err.message+")"); 
	//console.log(err)  
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
	//next(err);  
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
