var site_action='default',site_lang='en';	

module.exports  =  function(lang,action){
	if(action!=undefined)
	   site_action = action;
	if(lang!=undefined)
	   site_lang = lang;
	var data =''; 
	return function(a,l){ //console.log() 
		site_action = a || site_action;
		site_lang   = l || site_lang;
		//console.log(site_lang +  " -  -" + site_action);
	   try{
          data = require('./'+site_lang+"/mppp-"+site_action);	
	   }catch(e){
		  data = require('./'+site_lang+"/mppp-default");
	   } 
	 //console.log(data);	
	   var dt =Object.create(data);
		   dt._e = function(k){ k = 'e_'+k;var er; 
			   if(dt[k]==undefined){  er=k.replace('e_',"");er= er.replace(/_/g," ");
				  return er.charAt(0).toUpperCase() +er.slice(1);
			   }else return dt[k];
		   };
		    dt._t = function(k){ k = 't_'+k;var er; 
			   if(dt[k]==undefined){  er=k.replace('t_',"");er= er.replace(/_/g," ");
				  return er.charAt(0).toUpperCase() +er.slice(1);
			   }else return dt[k];
		   };
		  dt.template = function(k,data){ k = 'tmpl_'+k;
			  if(dt[k]!=undefined){
				 var txt= dt[k]; 
			     for(var n in data)
					txt.replace(n,data[n]);
				 return txt;  
			  }
		  };
		return dt;
	
	}
};