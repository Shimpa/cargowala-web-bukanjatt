module.exports = {
e_mobile_no_alredy_exist:"Mobile no already registered",	
// add update drivers...
 t_driver_dob:"Date of birth",
 t_driver_image:"Upload image",
 t_password:"Password",
 t_firstname:"Firstname",
 t_lastname:"Lastname",
 t_add_driver:"Add New Driver",	
 t_update_driver:"Update Driver Detail",	
 t_driver_login_txt:"Driver login details",	
 t_driver_mobile_no:"Driver's Mobile number",	
 t_username_tagline:"This Mobile Number will be used by Driver to login.",	
 t_password:"Password",
 t_confirm_password:"Confirm Password",
 t_driver_licence_tagline:"Driver license details",
 t_licence_no:"License number",
 t_expiry_date:"Expiry Date (YYYY/MM/DD)",
 t_adhaar_card_detail:"Aadhar card details",
 t_adhaar_no:"Aadhar card number",
 t_driver_info:"Enter driver info",
 t_driver_name:"Name",
 t_driver_dob:"Date of birth (YYYY/MM/DD)",
 t_permanent_address:"Permanent Address",
 t_address:"Address",
 t_city:"City",
 t_state:"State",
 t_pincode:"Pin Code",
 t_infoprovidedis_correct:" All the information provided is true and correct to the best of my knowledge.",
//errors...
 e_driver_image:"Please upload driver Image", 	
 e_firstname:"Please enter firstname", 	
 e_lastname:"Please enter lastname", 	
 e_confirm_password:"Confirm password must be same as password.", 	
 e_driver_dob:"Please enter driver date of birth", 	
 e_driver_password:"Password must be greater then 6 chracters.", 		
 e_mobile_number:"Please enter valid mobile no.", 		
 e_driver_licence:"Please enter valid licence no.", 		
 e_licence_expiry:"Please select licence expiry date.", 		
 e_licence_image:"Please upload licence image.", 		
 e_address:"Please enter street address.", 		
 e_city:"Please enter address city.", 		
 e_state:"Please enter address state.", 		
 e_pincode:"Please enter Pin Code.", 		
 e_adhaar_no:"Please enter valid adhaar no.", 		
 e_adhaar_image:"Please upload adhaar image.", 		
 e_imagesize_exceeded:"Image size must be less then 1.6 mb.", 		
 e_imagetype_unsupported:"Only jpg,png,bmp image type supported.", 		
	
	
 //list...
  t_my_drivers:"My Drivers",	
  t_search_drivers:"Search driver here",	
  t_add_driver:"Add New Driver",
  t_driver_image:"Driver Image",
  t_driver_name:"Driver Name",
  t_driver_truck_category:"Truck Category",
  t_driver_isavail:"Available",
  t_yes:"Yes",
  t_no:"No",
  t_empty_record:"No Record Found.",	
  t_driver_activity:"Driver Activity",
  t_view_edit_detail:"View / Edit Details",
  t_assign_driver:"Assign Driver",
  t_intransit:"In-Transit",
  t_notassigned:"Not assigned",
  t_view_detail:"View detail",
  t_assign_now:"Assign Now",	
  t_avaialble:"Available",	
  t_assigned:"Assigned",
  t_notavailable:"Not Available",
  t_truck_inactiveadmin:"Inactive by Site Admin",
  //detail...
  t_view_driver:"View Driver",
  t_error_fetching:"Error occured while fetching record try another.",
  t_truck_typeandcat:"Truck type and Category",  	
  t_login_detail:"Driver login details",
  t_edit_driver:"Edit Driver Detail",
  unknown_request:"Unknown request type",
  mismatch_params:"Parameter missing",	
  t_scroll:"Scroll",	
  t_intable_below:"in the table below to view all the information",
	
// email push sms template text ...
t_manage_driver_vehicle:"Manage Driver Vehicle",	
	
	
  t_subject_driver_assigned:"Cargowala.com New driver assigned", 	
  // notify shipper when new driver assigned to their shipment	
  t_notify_shipper_newdriver_assigned:"Dear Shipper, Driver with Ph Num <<driverphone_number>> has been assigned to your order with order Ref. Num <<reference_no>> Your order will be shipped on <<date>> at <<time>>. You can contact the Trucker at <<trucker_contactno>> To manage your Vehicles and Drivers visit my shipments tab on your mobile app For queries/support contact us at  Helpline Number- <<support_no>>", // (E-mail, Sms, Push)
 // driver changed	
  t_notify_shipper_driver_changed:"Dear Shipper, a new Driver with Ph Num <<driverphone_number>> has been assigned to your order with order Ref. Num <<reference_no>> Your order will be shipped on <<date>> at <<time>>. You can contact the Trucker at <<trucker_contactno>> To manage your Vehicles and Drivers visit my shipments tab on your mobile app For queries/support contact us at  Helpline Number- <<support_no>>", // (E-mail, Sms, Push)	
  t_notify_trucker_newdriver_assigned:"Dear Trucker, Driver with Ph Num <<driverphone_number>> has been assigned to the order with order Ref. Num <<reference_no>> The order will be shipped on <<date>> at <<time>>. You can contact the owner of the shipment <<shipper_name>> at <<shipper_contactno>> To manage your Vhicles and Drivers click the link <<manage_driverslink>> or visit my shipments tab on your mobile app For queries/support contact us at  Helpline Number- <<support_no>>", // (E-mail, Sms)
  t_notify_driver_assigned2shipmentpaymentonline:"Dear Driver-You have been assigned to your order with order Ref. Num <<reference_no>> The order will be shipped on <<date>> at <<time>>. You can contact the owner of the shipment <<shipper_name>> at <<shipper_contactno>>. Here are the order details: Pickup city-<<pickup_city>>, Delivery City- <<delivery_city>>, Load Type-<<load_type>>,Weight in tones-<<weight>>,  Number of trucks required-<<no_of_truckes>>, Total Amount- <<total_amount>>, Amount paid <<amount_paid_type>>, <<paid_amount>>, Mode <<mode_of_payment>> Balance Amount Left <<balance_amount>>, Mode <<mode_for_balance>>",// (Sms, Push)
  t_notify_driver_assigned2shipmentpaymentcashonpickup:"Dear Driver-You have been assigned to your order with order Ref. Num <<reference_no>> The order will be shipped on <<date>> at <<time>>. You can contact the owner of the shipment <<shipper_name>> at <<shipper_contactno>>. Here are the order details: Pickup city-<<pickup_city>>, Delivery City- <<delivery_city>>, Load Type-<<load_type>>,Weight in tones-<<weight>>,  Number of trucks required-<<no_of_truckes>>, Total Amount- <<total_amount>>, Cash on Pick-Up <<amount_paid_type>>, <<paid_amount>>, Mode <<mode_of_payment>>  Balance Amount Left <<balance_amount>>,Mode <<balance_amount>>", // (Sms, Push)
	
};
 
