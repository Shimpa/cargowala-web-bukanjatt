module.exports = {
t_bid_accepted:"Hello <<trucker_name>>,<br/> Your bid for Rs. <<amount>> has been selected by the Shipper <<shipper_name>> for Rs. <<amount>>, Your need to Ship the order on <<date>> at <<time>>. Please assign the Driver and the vehicle on the shipment before <<date>>, <<time>> otherwise the order will get cancelled for more info please contact our support at <<support_no>>. Please collect the documents below during the Pickup of the Shipment from the shipper 1. Builty 2. Invoices of the Items 3. Photo Copy Of Shipper's Pan Card 4. Photo Copy of the Inc (If Any) ",
	
t_bid_rejected:"Hello <<trucker_name>>,<br/> Shipper has rejected your bid for the order. You can rebid on the order by clicking the <<order_ref_number>> or by visiting Bid load board tab from your <a href='//www.cargowala.com/webapp'>cargowala.com</a>",
t_bid_accepted_notify_shipper:"Hello <<shipper_name>>,<br/> You have selected the Bid of <<trucker_name>> for Rs. <<amount>>, Your Order will be Shipped on <<date>> at <<time>>. Please provide below mentioned Documents to the Driver at the Time of Pickup 1. Builty 2. Invoices of the Items 3. Photo Copy Of your Pan Card 4. Photo Copy of the Inc (If Any) Please contact our support for further queries <<support_no>> ",	
t_trucker_bid_rejected:"Cargowala.com: Your Bid rejected",	
t_trucker_bid_accepted:"Cargowala.com: Your Bid accepted",	
}