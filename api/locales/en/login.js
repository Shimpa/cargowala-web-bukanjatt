module.exports = {
  invalid_credential:"Entered wrong username or password.",
  verify_email_with_link :"Please check your inbox and verify registerd email <br/> or <a href='javascript:;' id='resend-verification-email'>Resend Verification mail</a>.",
  verify_email :"Please check your inbox and verify registerd email.",
  registred_email :"Please enter valid registerd email or contact with site administrator.",
  recently_sent_email :"Please check your inbox we have recenlty sent and email.",	
  inactive_user:"User is inactive please contact server administrator.",
  //regsitration...
  firstname :"Firstname must be 5-15 characters.",	
  lastname  :"Lastname must be 5-15 characters.",	
  email     :"Please enter a valid email (eg. username@domain.xyz).",	
  mobile_number :"Please enter a valid mobile no. (eg. 9463944676).",	
  password  :"Password can not be left blank (Must be alphanumeric and 6-15 characters).",	
  confirm_password  :"Confirm Password must be same as Password.",	
  image     :"Image can not left blank.",	
};