module.exports = {
  invalid_credential:"Entered wrong username or password.",
  verify_email :"Please check your inbox and verify registerd email or <a href='/shipper/resend-email-token' id='resend-verification-email'>Resend Verification mail</a>.",
  inactive_user:"User is inactive please contact server administrator.",
  inactive_user:"User is inactive please contact server administrator.",
  recently_sent_email:"Please check your inbox we have recently sent an email.",
  invalid_token:"Unexpected error occured try with valid link.",
  new_password:"Please enter your new password.",
  confirm_password:"Please repeat password again.",
  //regsitration...
  firstname :"Firstname must be 5-15 characters.",	
  lastname  :"Lastname must be 5-15 characters.",	
  email     :"Please enter a valid email (eg. username@domain.xyz).",	
  mobile_number :"Please enter a valid mobile no. (eg. 9463944676).",	
  password  :"Password can not be left blank (Must be alphanumeric and 6-15 characters).",	
  confirm_password  :"Confirm Password must be same as Password.",	
  image     :"Image can not left blank.",	
  email_error     :"Please enter valid registered email.",	

	
//layout text and errors....
	
t_need_help:"Need Help ?",
t_copyright:"Copyright Cargowala. All Rights Reserved",
t_terms_of_service:"Terms of Service",
t_privacy_policy:"Privacy Policy",
//footer content...
t_terms_and_condition:"Terms and Conditions", 
t_welcome:"Welcome",
	
//menu text and links
t_home:'Home',
t_regular_load_board:"Regular Load Board",
t_bid_load_board:"Bid Load Board",	
t_confirm_orders:"Confirmed orders",	
t_payments:"Payments",	
t_incompleteshipments:"Incomplete Shipment",	
t_myshipments:"My Shipments",	
t_myvehicles:"My Vehicles",	
t_mydrivers:"My Drivers",	
t_membership_plans:"Membership Plans",	
t_offers:"Offers",	
t_notification:"Notifications",	
t_helpandsupport:"Help &amp; Support",
	
t_edit_profile:"Edit Profile",
t_logout:"Logout",	
	
//shipper menu text and links
t_myshipments:'My Shipments',
t_bookatruck:"Book a Truck",

// contact us (help and support)
t_contact_us:"Contact Us",
t_payment_faq:"Payments FAQ's",
t_login_faq:"Login FAQ's",	
t_booking_truck:"Booking a truck",
t_agent_dashboard:"Agent Dashboard",

//language change links...
t_english:"English",	
t_hindi:"हिंदी",	
	
	
	
	
	
};