module.exports = {

//new 
t_payment_failed1:"Payment Failed due to below reason",	
t_invouce_detail:"Invoice Details",	
t_bidloadboard:"Bid Load Board",	
t_regularboard:"Regular Load Board",	
t_payments_status:"Payments Status",	
t_try_again:"Try Again",
t_completed:"Completed",	
t_progress:"Progress",	
t_pending:"Pending",	
t_left:"Left",
t_transactions_finished:"No Credits to bid Please buy credits to bid on shipment.",	
t_buy_transaction:"Buy Credits",	
t_success_txt:"you can view more payment detail in payment section. We are redirecting you on (MyShipments)  within",
t_click_here:"Click here",
t_seconds_or:"seconds(s) or ",	
t_per_ton_price:"Price per ton",
t_price_per_km:"Price per kilometer",
t_total_amount:"Total Amount",
t_first_instalment:"First Instalment",	
t_second_instalment:"Second Instalment",	
t_of_total:"of total",	
//payment page
t_selectpayment_txt:"Selct Your Payment Method",
t_credit_card:"Credit Card",
t_debit_card:"Debit Card",
t_netbanking:"Net Banking",
t_credit:"Credit",
t_debit:"Debit",
t_visa:"VISA",
t_master:"MASTER",
t_credit_cardno:"Credit Card no",
t_debit_cardno:"Debit Card no",
t_expiry_date:"Expiry Date",
t_cardholder_name:"Card Holder Name",
t_cvvno:"CVV",
t_secure_payment:"This payment is secured with Citrus Payment Gateway",
t_makepayment:"Pay",	
t_selectbank:"Select Your Bank",
t_to:"to",
t_billedto:"Billed To",	
t_orderno:"order No.",	
	
//text
t_transactions:"Credits",
t_notifications:"Notifications",
t_shipper_rejectedyour_bid:"Shipper rejected your bid",	
t_youcancledastrucker:"You canceled order",	
t_used:"Use",	
t_left:"Left",	
t_unknown_request:"Unknown Request",	
t_not_assignedyet:"not assigned",	
t_no_pending_shipment:"No Incomplete Shipment Found",	
t_continue:'CONTINUE',
t_available_for_expiryin:"Available for:",	
t_loading:"Loading...",	
t_saveitem_inlist:"SAVE ITEM IN LIST",	
t_change_date_time:"Change Date Time",	
t_shipment_expired:"Shipment Expired",	
t_datetime_changed_successfully:"Shipment date time updated successfully",	
t_shipment_expired_changedatetime:"Shipment expired before go ahead update shipment date time.",
t_make_payment:"Pay Payment",	
t_click_heretochangedatetime:"Click Here to change date time.",	
//errors...
 e_truck_category :"Select Truck type and Category.",	
 e_truck_type:"Please select truck.",	
 e_truck_loadtype:"Please select load type.",	
 e_truck_quantity:"Truck quantity must be 1.",	
 e_truck_priority_delivery:"Please select priority delivery.",	
 e_shipment_weight:"Please enter shipment overall weight.",	
//item-detail
 e_itemname:"Please enter item name",
 e_parent_category:"Please select item category.",	
 e_sub_category:"Please select item sub category.",	
 e_itemdesc:"Please enter item remark.",	
 e_total_items:"Please enter no of items.",	
 e_item_invoice:"Please upload invoice image.",	
 e_empty_shipment_items:"Please add atleast single item for shipment",	
 e_items_total:"Please enter overall item total.",	
 e_total_weight:"Please enter item weight.",	
 	
 	
	
	
// text ...
t_truck_loadtype:"Select truck and load type",	
t_below5ton:"Below 5 Ton",
t_shipment_weight:"Shipment Weight (in kg)",
t_5tonto15ton:"5 Ton to 15 Ton",
t_above15ton:"Above 15 Ton",	
t_select_truck:"select truck",	
t_select_loadtype:"Select your load type",	
t_priority_delivery_tagline:"Priority delivery is not available if you select partial load",	
t_partial_load:"Partial Load",	
t_full_load:"Full Load",
t_noof_trucks:"Number of trucks",	
t_priority_delivery:"Priority delivery",
t_trucker_name:"Trucker Name",	
t_contact_no:"Contact No.",	
t_contact_tagline:"For more info on priority delivery you can call us on",	
// items detail...
t_items_list:"Items List",
t_label_item:"Item",
t_enter_item_detail:"Enter item details",
t_item_name:"Item name",	
t_item_weight_opt:"Item weight (in kg optional)",	
t_item_weight_mndt:"Item weight (in kg mandatory)",	
t_item_size:"Item size (in inches)",	
t_item_length:"Length",	
t_item_width:"Width",	
t_item_height:"Height",	
t_select_category:"---- Select Category ----",	
t_no_of_items:"Number of items",	
t_noofitemsxtotalitemweight:"Number of items x Item Weight = Total Items weight",	
t_total_weight:"Total weight",	
t_upload_invoice:"Upload Invoice Copy",	
t_your_item_list:"Your item list",	
t_enter_invoice_label:"Enter invoice(s) total amount for all items you have added",	
t_total_invoice_amount:"Enter total invoice amount (mandatory)",	
t_buy_insurance_label:"Do you want to buy insurance ?",	
t_insurance_yesiwant:"Yes i want to buy insurance",	
t_insurance_ihave:"I already have an insurance",	
t_insurance_donotneed:"No, I don't need and insurance",	
t_insurance:"Insurance",	
t_next:"NEXT",
// invoice...
t_getbids_tagline:'Get More Bids for your load',	
t_bids_info_desc:'click to post you load on bid load board and get bids from other truckers also',	
t_load_detail:'Load Details',	
t_nof_trucks:'Number of trucks',
t_sub_total:"Sub total",
t_discount:"Discount",
t_total:"Total",
t_taxes:"Taxes",
t_tax:"Tax",
t_tons:"Tons",
t_ton:"TON",
t_rupee_sign:"Rs",
t_load_fare:"Load fare",
t_load_rate_today:"Load rate for today",
t_km:"km",
t_kg:"kg",
t_bid_board:"Bid Load Board",
t_rate_may_vary:"Transit rates may vary",
t_freight_charges:"Frieght Charges",
t_trucks:"Truck(s)",	
t_for:"for",
t_postonbid_board:"Post on Bid Board",
t_postonbidboard_areusure:"Are you sure you want to post your order on bid board.",	
//t_priority_delivery:"Priority Delivery Charges",
t_insurance_charges:"Insurance Charges",	
t_howantosee_invoice:"How you want to see invoice",		
t_per_ton:"Per Ton(s)",		
t_per_km:"Per Kilometer(s)",		
t_proceed_to_pay:"PROCEED TO PAY",	
//payment ...
t_enter_pan_label:"Enter PAN card and Service tax details",
t_enter_pancard:"Enter your PAN card number",
t_enter_service_taxno:"Enter Service tax number ",
t_select_payment:"Select payment amount",
t_paycash:"PAY IN CASH ON LOADING",
t_payonline:"PAY ONLINE NOW",
t_enter_coupon_tagline:"Enter if any coupon available",
t_enter_couponcode:"Enter Coupon code",
t_already_paid:"You had already paid full payment.",
t_not_paid1st_instalment:"You have not paid your first instalment yet.please pay this instalment.",
t_apply:"APPLY",
//pending shipment...
t_pending_shipment:"Incomplete Shipment",	
t_complete_shipment:"Complete Shipment Now",	
t_remove_shipment:"Remove Shipment",	
t_shipment_removed:"Your incomlete shipment removed successfully.",	
t_error_removing:"Error while removing incomplete shipment.please try again.",	
t_thisur_conven_txt:"This is for your convenience.  <br>Your details will not be shared with anybody.",	
t_payment_successfully_completed:"Payment process completed successfully.Refer your transaction id #<transaction_id> for any query related to payment",	
t_payment_failed:"Payment process failed.",	
t_payment_successfully_completed_but_internal_error:"Payment process completed successfully.But internal server while updating you record. please contact to site admin with transaction id #<transaction_id> for more info.",	

	
	
//trucker text and errors...	
t_filter:"Filter",
t_area_of_opration:"Areas of operation",
t_select_from_date:"Select From and To dates",
t_date_from:"Date from",
t_date_to:"Date to",
t_clear_filter:"Clear all filters",
t_apply_filter:"Apply Filter",
t_view_detail:"View Detail",	
t_no_record_found:"No result Found",	
t_accept_now:"ACCEPT NOW",	
t_bid_now:"BID NOW",	
t_bid:"Bid",	
t_accept_order:"Accept Order",	
t_want_to_order:"Are sure you want to accept the order ?",	
t_yes:"Yes",	
t_no:"No",
t_bid_price:"Bid Price",
t_bid_again:"BID AGAIN",
	
//shipment detail...	
t_place_your_bid:"Place Your Bid Here",
t_enter_bid_overal_amount:"Enter total amount for all trucks",
t_submit_your_bid:"Submit Your Bid",
t_bid_expires_after:"Bid available for ",
t_item:"Item",
t_category:"Category",	
t_sub_category:"Sub Category",	
t_item_weight:"Item Weight",	
t_inkg:"in Kilograms",	
t_nof_units:"No of Units",	
t_item_size:"Item Size",	
t_length:"Length",	
t_width:"Width",	
t_height:"Height",	
t_in_inches:"in Inches",	
t_item_desc:"Description",	
//errors...
e_bid_amount:"Please enter valid bid amount",
e_invalid_request:"Invalid request",
t_bid_posted_successfully:"Bid posted successfully",
t_bid_updated_successfully:"Bid Updated successfully",
t_enter_remark:"Enter Remark",
	
//myshipments...
t_myshipments:'My Shipments',
t_active:'Active',
t_upcompingorders:'Upcomming Orders',
t_activeorders:"Active Orders",	
t_history:"History <small> (Past shipments) </small>",
t_no:'No.',
t_assigned_vehicles:'Assigned Vehicles',
t_assigned_drivers:'Assigned Drivers',
t_vehicle_no:'Vehicle Number',
t_driver_detail:'Driver Detail',
t_reference_no:'Reference no',
t_bid_price:'Bid Price',	
t_status:'Status',	
t_cancel_order:'Cancel Order',	
t_shipment_track:'Shipment Track',
t_track_now:'Track Now',	
t_order_tracking_detail:'Order Tracking details',	
t_started_from:'Started from',	
t_at:'at',	
t_on:'on',
t_from:'from',	
t_reached:'Reached',
t_view_invoice_no:'View Invoice Number',	
t_upcoming_not_found:'No Upcoming Shipment',
t_no_vehicle_driver_assigned:"No Driver and Vehicle assigned yet",
t_view_detail:"View Detail",	
t_contact_no:"Contact No.",	
t_nomore_records:"No more records",
t_assign_vehicle:"Assign Vehicle",
t_change_vehicle:"Change Vehicle",
t_assign_driver:"Assign Driver",	
t_change_driver:"Change Driver",	
t_ordercanceled_successfully:"Shipment canceled successfully.you can view it now in 'History (Past Shipment)' tab.",
t_invoice_no:"Invoice No.",
t_shipment_status:"Shipment Status",	
// errors
e_cancel_shipment:"Error while canceling shipment try again later.",	
	
	
	
//shipment detail...	
	
t_shipment_detail:"Shipment Detail",
t_driver_vehile_assigned_tagline:"Assigned vehicle(s) and driver(s)",
t_no_vehicle_assigned:"No vehicle assigned yet",
t_no_driver_assigned:"No driver assigned yet",
t_vehicle_no:"Vehicle number",
t_txt_is_assigned_for_shipment:"is assigned for your shipment",
t_driver:"Driver",
t_assigned:"Assigned",
t_shipment_tracking_detail:"Shipment Tracking Details",	
t_shipment_started_on:"Shipment started on",	
t_driver_no_started_trip:"Driver not started trip yet.",	
t_trip_not_started_yet:"Trip not started yet",
t_loading_trucker_info:"Loading Trucker Information...",
t_change_timelinktxt:"Change Time",	
t_change_datetime:"Change Time",
t_change_time:"Change Time (hh:mm:ss)",
t_change_timeinfo:"You can change time of your current shipment",
t_timechanged_succesfully:"Shipment time changed successfully and notified related Truckers and Drivers",	
t_your_lastbidamount:"Last Bid Amount:",
t_change_driver:"Change Driver",	
t_assign_vehicle:"Assign Vehicle",	
t_assign_driver:"Assign Driver",	
//error...
e_change_time:"Time must be less then today 24:00:00(hh:mm:ss).",	

	
// bid load board...
t_source:"Source",	
t_destination:"Destination",	
t_date:"Date",	
t_time:"Time",	
t_load_type:"Load Type",	
t_priority_delivery:"Priority Delivery",	
t_order_detail:"View Detail",	
t_bids_recieved:"Bids Recieved",	
t_no_bid_found:"No Bid Found",	
t_bidposted_byou:"Order(s) posted by you",
t_no_bids_avail:"No bids available yet",	
t_view_order:"View order details",	
t_bids_available:"Bids Available",	
t_accept:"Accept",	
t_reject:"Reject",	
t_view_bids:"View Bids",
t_view_allbids:"View All Bids",
t_successfullyperofrmed:"Operation successfully performed.",
t_bid_accepted:"Bid Accepted",
t_accepted_txt:'You accepted a bid of {{trucker}}.now you can view this shipment in <br> <strong class="text-primary"> My Shipmenets</strong> listed in the side menu. click NO to stay here or YES to view <strong class="text-primary"> My Shipmenets</strong>;',
t_accept_bid_txt:"Accept Bid",
t_complete_payment_process:"Please complete the payment in order to confirm the Shipment",

// regular load board...
t_regular_board:"Regular Load Board",
t_order_avail_for:"Order availeble for",
t_err_while_updating:"Error: internal ser error please try again.",
t_regular_bid_accepted:"Order accepted successfully.",
t_shipmentnot_found:"Invalid request no content found.",	
	
	
// payments...
t_noneed_insurance:"No, I don't need insurance",	
t_select_payment_amount:"Select Payment Amount",	
t_have_coupon:"Have a coupon code",
t_enter_here:"Enter here !!!",	
t_coupon_code_lbl:"Enter coupon code",			
t_payby_cash_onloading:"PAY IN CASH ON LOADING",			
t_pay_online:"PAY ONLINE NOW",			
t_cashon_loading_txt:"Cash on Loading",			
t_pay_online_txt:"Pay Online",			
t_payment_detail:"Payment Detail",	
t_insurance_txt:"Insurance",
t_youare_paying:"You are paying your shipment payments by ",	
t_instalment:"instalment(s)",
t_1st_instament:"1st Instalment",	
t_2nd_instament:"2nd Instalment",	
t_howliketopay:"How would you like to pay your shipment payment",	
t_paymentin_future:"payment in future ?",
t_cashon_unloading_txt:"Cash on Unloading",	
t_submit:"Submit",	
t_payment_successfully:"You have completed payment process",	
t_pleaseselect_how_you_stpay:"Please select how you want to pay 1st Installment",	
t_pleaseselect_how_you_ndpay:"Please select how you want to pay 2nd Installment",	
	

// email subject and body
t_subjectbidupdeted:'Cargowala.com bid price updated by trucker',	
t_subjectnewbidposted:'Cargowala.com New bid posted on your shipment',	
t_subjectpostedonbidloadboard:"Cargowala.com Your shipment posted on Bid Load Board",
t_subjectpostedonregularloadboard:"Cargowala.com Your shipment posted on Regular Load Board",	
t_subjectpaymentdone:"Cargowala.com Your Payment Status",	
t_subjectregularbidaccedped:"Cargowala.com Regular Load Board Order Accepted",	
t_subjectcanceledbytrucker:"Cargowala.com Order canceled by trucker",
t_subject_datetimechanged:"Cargowala.com Shipment time changed",
//templetes...
tmpl_push_bid_updeted:"Hello {{shipper_name}}",
t_shipmenttime_updated:" {{shipper}} updated shipment time.",
	
// new bid posted on shipment
t_newbidonshipment:'Hello <<shipper_name>>,<br/>A New bid has been posted on your shipment <<reference_no>> by <<trucker_name>> of Rs.<<amount>>  You can checkout all the bids placed by the truckers on your mobile app/Web panel-Bid load Board section.',
	
t_updatedbidonshipment:'Hello <<shipper_name>>,<br/><<trucker_name>> updated your bid price <<old_amount>> to <<new_amount>> on your shipment <<reference_no>>  You can checkout the bids placed by the truckers on your mobile app/Web panel Bid load Board section.',	
	
	
	
	
t_trucker_confirmedshipment:'Hello <<shipper_name>>,<br/> Your Order has been confirmed by the Trucker <<trucker_name>> for Rs. <<amount>>, Your Order will be Shipped on <<date>> at <<time>>. Please provide us the documents below during the Pickup of the Shipment 1. Builty 2. Invoices of the Items 3. Photo Copy Of your Pan Card 4. Photo Copy of the Inc (If Any) Please contact our support for further queries <<support_no>>',	

//order canceled...
t_cancledby_trucker:'Dear <<shipper_name>>,<br/> Your order with reference number <<reference_no>> has been cancelled. We are sorry for the inconvinience caused. In the mean time, we are finding a new Trucker for your order. For any further queries contact us at <<support_no>>.',	
	
t_notifydrivers:"Dear <<driver_name>>,<br/> Your order with reference number <<reference_no>> has been cancelled. For any further queries contact us at <<support_no>>.",	

// regular shipment accepted....
t_regulatshipment_accepted: "Hello <<shipper_name>>,<br/> Your Order has been confirmed by the Trucker <<trucker_name>> for Rs. <<amount>>. You can contact the Trucker at <<trucker_contactno>>, Your Order will be Shipped on <<date>> at <<time>>. Please provide the documents below during the Pickup of the Shipment to the Driver 1. Builty 2. Invoices of the Items 3. Photo Copy Of your Pan Card 4. Photo Copy of the Inc (If Any) Please contact our support for further queries <<support_no>>",
t_notify_trucker_regular_accepted:"Hello <<trucker_name>>, You have confirmed the order for Rs. <<amount>> placed by  <<shipper_name>>, Your need to Ship the order on <<date>> at <<time>>. Please Collect the documents below during the Pickup of the Shipment from the shipper. You can contact the shipper at <<shipper_contactno>> 1. Builty 2. Invoices of the Items 3. Photo Copy Of your Pan Card 4. Photo Copy of the Inc (If Any) Please contact our support for further queries <<support_no>>",	
// trucker shipment confirmation...	
t_confirmed_order:"Confirmed Orders",	
t_accept_order:"Accept Order",	
t_accept_order_body:'Click yes to accept this order, you can assign Vehicles and Drivers for this order from <br> <strong class="text-primary"> My Shipmenets</strong> listed in the side menu.',
	
// shipment posted on bidload board notify shipper
t_postedon_bidloadboard:"Hello <<shipper_name>>,<br/> You order with the reference number <<reference_no>> has been placed successfully on our bid board on <<date>> at <<time>>. You can checkout the bids placed by the truckers on your mobile app/Web panel Bid load Board section", // email sms
// shipment posted on regular board notify shipper
t_postedon_regularboard:"Hello <<shipper_name>>,<br/> You order with the reference number <<reference_no>> has been placed successfully on <<date>> at <<time>>. We will notify once we find an appropriate Trucking partner for your load delivery", // (Email, SMS)
	
t_firstinstalment_paymentdone:"Thank you for choosing CargoWala as your Delivery Partner, an online payment of Rs. <<amount>> has been received by us for your order with Ref Num <<reference_no>>. You have to pay a sum Rs. <<balance_payment>> at the time of delivery.  To manage your payments please check payments section on mobile/web application, for any queries/support call us or write us at Helpline Number-<<support_no>>  Email Support-<<support_email>>",// (Email,SMS)
t_finalpaymentdone:"Thank you for choosing CargoWala as your Delivery Partner, A Balance payment of Rs. <<amount>> has been received by us online for your order with Ref Num <<reference_no>>. To manage your payments please check payments section on mobile/web application, for any queries/support call us or write us at Helpline Number-<<support_no>>", // (E-mail, Sms, Push)

	
// notify trucker if shipper paid payment...
t_paymentpaidbyshpper:"Dear <<trucker_name>>,<br/> the order with reference number <<reference_no>> placed by <<shipper_name>> has the payment status of, Total Amount- <<amount>>, Amount paid <<paymenttype>>, <<amount_paid>>, Mode <<payment_mode>> Balance Amount Left <<balance_amount>>,Mode <<balanace_amount_mode>> . For any queiries contact our customer care @<<support_no>>",// (E-mail, Sms, Push)
	
t_notifytrucker4secondpayment:"Dear <<trucker_name>>,<br/> the order with reference number <<reference_no>> placed by <<shipper_name>> has paid the balance Amount of rs <<amount>> online, the shipper has done his full and final payment. For any queiries contact our customer care @<<support_no>>",// ((E-mail, Sms, Push)
	
// cash onloading notify shipper	
t_1stpaymentbycash:"Dear <<shipper_name>>,<br/> You have to pay a sum of Rs <<amount>> to the driver on <<date>>, <<time>> @ the pickup point. To manage your payments please check payments section on mobile/web application, for any queries/support call us or write us at Helpline Number-<<support_no>>  Email Support-<<support_email>>", // (E-mail, Sms, Push)
// cash on loading notify trucker
t_1stpaymentnotifytrucker:"Dear <<trucker_name>>, Shipper has to pay a sum of Rs <<amount>> to the driver on <<date>>, <<time>> @ the pickup point. To manage your payments please check payments section on mobile/web application, for any queries/support call us or write us at Helpline Number-<<support_no>>  Email Support-<<support_email>>", // (E-mail, Sms, Push)

	
	
// mobile notification notify shipper if cash payment paid on unloading..	
t_cashonunloading:"Thank you for choosing CargoWala as your Delivery Partner, A payment of Rs. <<amount>> has been received by <<driver_name>> for your order with Ref Num <<reference_no>>. To manage your payments please click on the link below, for any queries/support call us or write us at Helpline Number-<<support_no>>", // (E-mail, Sms, Push)

	
// notify shipper shipment time changes	
t_notifyshippertimechanged:"Dear <<shipper_name>>, You have changed the Time of your order with reference number <<reference_no>> from <<old_time>> to <<new_time>>. We will notify the Trucker and the Driver about the same. In case, you can also get in touch with the Trucker at <<contactno_trucker>> and Driver at <<contactno_driver>> You can also contact us at <<support_no>> for any further queries.", // (Email, SMS)
//notify trucker time changed...
t_notifytruckertimechanged:"Dear <<trucker_name>>, the Shipper has changed the Time of the order with reference number <<reference_no>> from <<old_time>> to <<new_time>>. Please update the Driver about the same.In case, you can also get in touch with the Shipper, at <<contactno_shipper>>. You can  contact us at <<support_no>> for any further queries.", // (E-mail, Sms, Push)
//notify driver time changed
t_notifydrivertimechanged:"Dear <<driver_name>>, the Shipper has changed the Time of the order with reference number <<reference_no>> from <<old_time>> to <<new_time>>. Please get in touch with the Shipper at <<contactno_shipper>> You can also contact us at <<support_no>> for any further queries.", //(SMS, Push)
	
	
	
	
	
	
	
	
	
	
};
 