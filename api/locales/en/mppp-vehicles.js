module.exports = {
// vehicles...
t_myvehicles:"My Vehicles",
t_search_trucks:"Search Truck Here...",
t_add_newvehicles:"Add New Vehicles",
t_scroll:"Scroll",
t_tables_below_tagline:"in the table below to view all the information",
t_truck_type:"Truck Type",
t_truck_number:"Truck Number",
t_truck_name:"Truck Name",
t_truck_status:"Available",
t_truck_inactive:"Inactive",
t_truck_activity:"Truck Activity",
t_truck_detail:"Detail",
t_assign_truck:"Assign Truck",	
t_view_detail:" View Detail",
t_intransit:" In Transit",
t_assign_now:"Assign Now",
t_change_driver:"Change Driver",
t_notavailable:"Truck Not Available",
t_truck_inactiveadmin:"Inactive by Site Admin",
t_change_vehicles:"Change Vehicles",
//add vehicle...
t_add_truck:"Add New Truck",	
t_select_truck_type:"Select Truck type and Category",
t_enter_vehicle_insurance:"Enter Vehicle Insurance Details",	
t_vehicle_no:"Vehicle Number",
t_enterrc_detal:"Enter RC Detail",
t_rc_validup2:"RC Valid Upto (YYYY/MM/DD)",
t_insurance_validup2:"Insurance Valid Upto (YYYY/MM/DD)",
t_rcvalidup2:"RC Valid Upto",
t_insurancevalidup2:"Insurance Valid Upto",	
t_enter_permit_detail:"Enter Permit Details",
t_permit_no:"Permit Number",
t_permit_validup2:"Permit Valid Upto (YYYY/MM/DD)",
t_permitvalidup2:"Permit Valid Upto",	
t_tagline_infoistrue:"All the information provided is true and correct to the best of my knowledge.",
t_submit:"Submit",
t_available:"Available",
t_assigned:"Assigned",
t_notassigned:"Not Assigned",	
t_yes:"Yes",
t_no:"No",	
t_tons:"TON",	
// errors...
e_permit_image:"Please upload Permit Image",	
e_permit_number:"Please enter valid permit number.",
e_permit_expiry_date:"Please enter permit expiry date.",
e_rc_expiry_date:"Please enter RC expiry date.",
e_rc_image:"Please upload RC image.",
e_minmaxvehicle_no:"Vehicle number must be between 8 to 20 characters.",
e_insurance_image:"Please upload insurance image",
e_truck_type:"Please select truck type.",
e_truck_category:"Please select truck category.",
t_norecord_found:"No Record Found.",
t_individual_trucker:"Only a single truck registration is allowed in this account.",
	
//detail...
t_view_truck_detail:"View Truck Detail",
t_truck_type_category:"Truck Type and Category",	
t_rc_detail:"RC Detail",
t_expired:"Expired",
t_permit_detail:"Permit Detail",
t_vehicle_insurance_detail:"Vehicle Insurance Details",
t_edit_vehicle:"Edit vehicle detail",	
	
	
// email push sms template text ...
t_manage_driver_vehicle:"Manage Driver Vehicle",	
	
	
  t_subject_vehicle_assigned:"Cargowala.com New vehicle assigned", 	
  // notify shipper when new driver assigned to their shipment	
  t_notify_shipper_newvehicle_assigned:"Dear Shipper, Truck with Reg No <<regis_no>> has been assigned to your order with order Ref. Num <<reference_no>> Your order will be shipped on <<date>> at <<time>>. You can contact the Trucker at <<trucker_contactno>> To manage your order  visit my Shipments tab in your app For queries/support contact us at Helpline Number-<<support_no>>", // (E-mail, Sms, Push)
 // vehicle changed	
  t_notify_shipper_vehicle_changed:"Dear Shipper, a new Truck with Reg No <<regis_no>> has been assigned to your order with order Ref. Num <<reference_no>> Your order will be shipped on <<date>> at <<time>>. You can contact the Trucker at <<trucker_contactno>> To manage your order  visit my Shipments tab in your app For queries/support contact us at Helpline Number-<<support_no>>", // (E-mail, Sms, Push)		
	
  // notify trucker
  t_notifytruker_vehile_assigned:"Dear Trucker, You have assigned <<vehicle_name>> and <<regis_no>> to the order with order Ref. Num <<reference_no>> Your Truck will ship this order on <<date>> at <<time>>. You can contact the owner of the shipment <<shipper_name>> at <<shipper_contactno>> To manage your orders click the link <<manage_driverslink>> or visit my shipments tab on your mobile app For queries/support contact us at  Helpline Number-<<support_no>>", //(E-mail, Sms)

	
	
	
	
	
	
};
 
