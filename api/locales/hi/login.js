module.exports = {
 
 invalid_credential:"आपके द्वारा डाला गया ईमेल / पासवर्ड गलत है. पासवर्ड भूल गए?.",

  verify_email_with_link :"अपना ईमेल इनबॉक्स की जाँच करें और वेरीफाई करें <br/> or <a href='javascript:;' id='resend-verification-email'> वेरिफिकेशन मेल दोबारा भेजे </a>.",
  verify_email :"अपना ईमेल इनबॉक्स की जाँच करें और वेरीफाई करें .",
  registred_email :" कृपया सही ईमेल या फ़ोन नंबर दर्ज करें .",
  recently_sent_email :"अपना ईमेल इनबॉक्स चेक करें.", 
  inactive_user:"यूजर इनएक्टिव है ",
  //regsitration...
  firstname :"पहला नाम 5-15 करैक्टर का होना चाहिए .", 
  lastname  :"अंतिम नाम 5-15 करैक्टर का होना चाहिए.", 
  email     :"कृपया सही ईमेल दर्ज करें  (eg. username@domain.xyz).",  
  mobile_number :"कृपया सही फ़ोन नंबर दर्ज करें. (eg. 9463944676).",  
  password  :" पासवर्ड भरना होगा  (6-15 करैक्टर का होना चाहिए).", 
  confirm_password  :"पासवर्ड की पुष्टि कीजिये.", 
  image     :"इमेज  को नहीं छोड़ा जा सकता .", 
};