module.exports = {

t_go_back:"पिछले पेज पर जाइये ",	
	
	
	
//login -registration...
t_login:'लॉगिन',
t_registration:"लॉगिन",	
t_verification_email_sent:"वेरिफिकेशन मेल भेज दी गयी है, अपना मेल बॉक्स चेक करे ",
t_verify_now:"अभी वेरीफाई करे ",	
t_no_record_found:"कोई भइओ रिकॉर्ड नही मिला है ",
t_upload_profile_pic:"अपनी प्रोफाइल पिक्चर अपलोड करे ",	
t_verify_email:'अपने अभी तक अपना ईमेल रजिस्टर नही किया है .',
t_resend_email:"वेरिफिकेशन मेल दोबारा चाहेंगे ",	
//errors...
 e_loading_address :"शिपमेंट का लोडिंग एड्रेस गलत है .",	
 e_unloading_address:"शिपमेंट का अनलोडिंग एड्रेस सही नही है .",	
 e_invalid_datetime:"शिपमेंट का समय शुरू होने से कम से कम 2 घंटे पहले का होना चाहिए .",	
 //e_invalid_datetime:"लदान की तारीख और समय अब से कम से कम 8 घंटे होना चाहिए",
	

// label... 
t_find_your_truck:"अपना ट्रक ढूंढे ",	
t_enter_loading_point:"लोडिंग पॉइंट दर्ज करे ",	
t_enter_unloading_point:"अनलोडिंग पॉइंट दर्ज करे ",	
t_loading_date:"लोडिंग की तारिक ",	
t_loading_time:"लोडिंग का समय ",	
t_dashboard_tagline:"कार्गोवाला भारत का सबसे तेज़, आसान और भरोसेमंद जरिया है आपके सामान को एक जगह से दूसरी जगह तक पुहंचने के लिए ",	
t_select_city:"शहर का नाम भरे ",	
// button...
t_book_now:"अभी बुक करे ",	
t_continue:"आगे बढ़ना है ",
t_mobile_no_verification:"अपना फ़ोन नंबर वेरीफाई करे .",	
t_enter_otp_code:"फ़ोन पर आया गया कोड भरे ",	
t_remind_me_later:"मुझे अगले 5 घंटे में दोबारा याद करवाइये ",	
t_dontgetotp_msg:"अगर आपने अभी तक मोबाइल नही रजिस्टर किया है तो इस लिंक पर क्लिक कर के अभी रजिस्टर करे .",	
t_sendotpon:"कोड भेजे ",	
t_resend_otp:"कोड दोबारा भेजे ",	
t_otpalready_verified:"मोबाइल नंबर पहले से वेरीफाई हो चूका है .",
t_verification_otp_sent:"कोड के लिया अपना मोबाइल चेक करे .",	
t_otpalready_sent:"कोड के लिया अपना मोबाइल चेक करे .",	

	// edit profile...
t_edit_profile:"अपनी प्रोफाइल एडिट करे ",	
t_profile_updated:"प्रोफाइल अपडेट हुई .",	
t_business_updated:"कंपनी डिटेल अपडेट हुई  .",	
t_firstname:"पहला नाम",	
t_lastname:"अंतिम नाम ",	
t_enter_pano:"पैन कार्ड नंबर भरे ",	
t_enter_mobile:"मोबाइल नंबर भरे ",	
t_enter_serviceno:"अपना सर्विस टैक्स नंबर भरे ",	
t_change_password:"पासवर्ड बढ़ना चाहेंगे  ?",	
t_save_detail:"डिटेल्स को सेव करे ",	
t_register_as_company:"कंपनी रजिस्टर करे ",	
t_edit_company:"कंपनी की डिटेल्स को एडिट करे ",	
t_verify_mobile:"मोबाइल नंबर वेरीफाई करे ",	

//business detail
t_edit_company:"कंपनी की डिटेल्स को एडिट करे",
t_business_detail:"बिज़नस डिटेल भरे ",
t_company_address:"कंपनी का एड्रेस भरे ",
t_business_name:"बिज़नस का नाम भरे ",
t_landline_no:"लैंडलाइन नंबर भरे ",
t_select_business:"अपना बिज़नस टाइप चुने ",
t_manufacturing_company:"मैन्युफैक्चरिंग फैक्ट्री ",
t_logistics:"ट्रांसपोर्टर ",
t_warehouse:"कारखाने के  मालिक ",
t_distributer:"डिस्ट्रीब्यूटर का नाम ",
t_others:"बाकी ",
t_traders:"ट्रेडर ",
t_select_company:"कमपनी का टाइप सेलेक्ट करे ",
t_public_sector:"पब्लिक सेक्टर ",
t_proprietary:"प्रोपेरटिएर का नाम ",
t_partnerfirm:"परतमरशिप फर्म ",
t_private_limited:"प्राइवेट लिमिटेड ",
t_registered_company:"रजिस्टर्ड कंपनी का एड्रेस ",
t_address:"एड्रेस ",
t_city:"शहर ",
t_state:"राज्य ",
t_pincode:"पिनकोड ",
t_office_address:"ऑफिस का एड्रेस ",
t_same_as_address:"रजिस्टर्ड एड्रेस वाला ही ",
	

	
// trucker module...
	
t_signin_tagline:"अपने अकाउंट में अभी सिग्न इन करे ",	
t_facebook:"फेसबुक",	
t_google:"गूगल ",	
t_loginwith_email:"या फिर अपनी ईमेल से लॉगिन करे ",	
t_enter_email:"अपनी ईमेल भरे ",	
t_enter_password:"अपना पासवर्ड भरे ",	
t_remember_me:"रेमेम्बेर रखे ",	
t_forgot_password:"पासवर्ड भूल गए ?",	
t_byclicking_agree:"लॉगिन पर क्लिक करने से में कार्गोवाला के ",	
t_login:"लॉगिन ",	
t_register:"यहा रजिस्टर करे ",	
t_donot_have_account:"की आपका अकॉउंट नही बना हुआ ?",

//register ...
t_transforming_tagline:"डिलीवर करने का तारीक बदलेगा .",	
t_truckshell_tagline:"ट्रकिंग बनेगा प्लेसर ! ",	
t_ourpromise_tagline:"ये हमारा वादा है ",	
t_ourpromise_tagline:"ये हमारा वादा है ",	
t_best_shipping_company:"कागोवाल भारत की सबसे बड़ी शिपिंग कंपनी है ",	
t_best_shipping_company1:"कागोवाल भारत की सबसे बड़ी शिपिंग कंपनी है ",	
t_getstarted:"अभी अपना फ्री अकॉउंट बनाइये .",	
t_firstname:"पहला नाम ",	
t_lastname:"अंतिम नाम ",	
t_and:"और ",	
t_enter_confirm_password:"कन्फर्म पासवर्ड ",	
t_register_shipper:"शिप्पर बनकर रजिस्टर करे ",	
t_register_trucker:"तरुकर बनकर रजिस्टर करे ",	
t_already_have_account:"पहले से ही अकाउंट बना है ?",	
t_login_here:"यहा लॉगिन करे ",	
//user type...
t_proceed_as:"आगे बड़े ",
t_individual:"इंडिविजुअल बनकर ",
t_agent:"एजेंट बनकर ",
t_company:"कंपनी बनकर ",
t_fleet_owner:"फ्लीट ओनर बनकर ",
t_proceedas_individual_tagline:"यदि आप इंडिविजुअल बनकर आगे बढ़ते है <br> आप बाद में फ्लीट ओनर भी बन सकते है.",

// company detail...
t_enter_company_detail:"अपनी कंपनी की डिटेल भरे ",	
t_business_name:"बिज़नस का नाम ",
t_lanline_no:"लैंडलाइन नंबर ",
t_pancard_no:"पैन कार्ड नंबर भरे ",	
t_servicetax_no:"सर्विस टैक्स  नंबर भरे ",
//company address...
t_company_address:"कंपनी का एड्रेस भरे ",	
t_registered_company_address:"कंपनी का एड्रेस भरे s",	
t_enter_address:"एड्रेस भरे ",	
t_enter_city:"शहर ",	
t_enter_state:"राज्य ",	
t_enter_pincode:"पिनकोड ",	
t_enter_office_address:"ऑफिस का एड्रेस ",
t_regaddresssameas:"रजिस्टर्ड एड्रेस सेम है ",
// user bank info...
t_bankinfo_updated:"बैंक की डिटेल अपलोड हो गयी है .",
t_enter_bank_detail:"बैंक की अकाउंट डिटेल भरे ",	
t_account_number:"अकाउंट नंबर ",	
t_account_holdername:"अकाउंट होल्डर का नाम ",	
t_branch_code:"ब्रांच का कोड ",	
t_ifcs_code:"आई  फ स सी  कोड भरे ",	
t_bank_name:"बैंक का नाम ",
//errors
e_bankinfo:"जानकारी अपलोड कने में दिकत आ रही है, दोबारा प्रयास करे .",
e_account_number:"कृपया वैलिड अकाउंट नंबर भरे ",
e_bank_name:"बैंक का नाम भरे ",
e_holder_name:"अकाउंट होल्डर का नाम भरे ",
e_branch_code:"बैंक की ब्रांच कोड भरे ",
e_branch_address:"बैंक की ब्रांच एड्रेस भरे ",	
e_ifcs_code:"अकाउंट का आई फ स सी कोड भरे ",	
	
// trucker dashboard...

t_regular_load:"रेगुलर लोड बोर्ड ",	
t_bid_load_board:"बिड लोड बोर्ड ",	
t_my_vehicles:"मेरे ट्रक ",	
t_drivers:"ड्राइवर ",	
t_your_rating:"आपकी रेटिंग ",	
t_total_earning:"कुल एअर्निंग ",
	
//edit profile...
t_edit_company:"कंपनी डिटेल एडिट करे ",	
t_edit_company_address:"कंपनी एड्रेस एडिट करे ",	
t_edit_bank_info:"बैंक की डिटेल  एडिट करे ",	
	

// vehicles...
t_myvehicles:"मेरे ट्रक ",
t_search_trucks:"ट्रक यहा सर्च करे ...",
t_add_newvehicles:"यहा नए ट्रक ऐड करे ",
t_scroll:"स्क्रॉल ",
t_tables_below_tagline:"निचे टेबल में, साड़ी जानकारी जानने के लिए ",
t_truck_type:"ट्रक का टाइप ",
t_truck_number:"ट्रक का नंबर ",
t_truck_name:"ट्रक का नाम ",
t_truck_status:"ट्रक का स्टेटस ",
t_truck_activity:"ट्रक की एक्टिविटी ",
t_truck_detail:"डिटेल ",
t_assign_truck:"ट्रक असाइन करे ",	
t_view_detail:" डिटेल देखे ",
t_intransit:" प्रतिक्रिया में t",
t_assign_now:"अभी असाइन करे ",
t_change_driver:"ड्राइवर बदले ",
t_change_vehicles:"ट्रक बदले ",

t_unautorizedaccess:"आपके पास आगे बढ़ने की सहूलत नही .",
t_tryaftersometime:"तकनीकी खराबी आगयी है, कृपया दोबारा प्रयास करे .",	
	
// trucker dashboard...
t_upgrade_membership:"अभी मेंबरशिप को अपग्रेड करे ",
t_select_membership:"अपना मेम्बरशिप प्लान चुने ",
t_buy_now:"अभी खरीदे ",
t_month_txt:"महीना ",
	
// payments...
t_payments_txt:"पेमेंट ",
t_complete_payment_done:" पूरी पेमेंट हो चुकी है ",
t_payment_pending:"अभी पेमेंट बाकी है ",	
t_view_order_detail:"आर्डर की डिटेल को देखे ",	
t_view_status:"स्टेटस देखे ",
t_you_have_done_complete_payment:"आपने पूरी पेमेंट कर दी है ",
t_final_payment:"अंतिम पेमेंट ",
t_complete_payment:"पूरी पेमेंट ",
t_of_final_total:"कुल टोटल ",
t_online:"ऑनलाइन ",
t_on_unloading:"अनलोडिंग पर ",
t_on_loading:"लोडिंग पर ",
t_you_selected:"पेमेंट सेलेक्ट करे ",
t_op_earliar:"पुराण ऑप्शन .",
t_rest_amout:"आप बाकी की पेमेंट कर सकते है ",
t_now_txt:"अभी ",	
t_pay_onlinenow:"अभी ऑनलाइन पाय करे ",	
t_pay_onunloading:"अनलोडिंग पर पाय करे ",	
t_pay_complete_amount:"अभी पूरी अमाउंट भरे ",
t_first_payment:"पहली पेमेंट ",	
t_second_payment:"दूसरी पेमेंट ",	
t_payment_done:"आपने ये पेमेंट कम्पलीट कर दी है ",
//trucker payments...
t_total_earning:"कुल एअर्निंग ",	
t_tab_pending_payments:"पेंडिंग पेमेंट ",	
t_tab_completed_payments:"पूरी हो राखी पेमेंट ",
t_pendingfirst_payment:"पहली पेंडिंग पेमेंट ",	
t_completedfirst_payment:"पहली पेमेंट हो चुकी है ",	
t_pendingsecond_payment:"दूसरी पेंडिंग पेमेंट ",	
t_completedsecond_payment:"दूसरी पेमेंट हो चुकी है ",	
 
// change password...	
t_old_password:"पुराण पासवर्ड भरे ",
t_new_password:"न्य पासवर्ड भरे ",
t_repeat_password:"न्य पासवर्ड दोबारा भरे ",
t_submit:"सबमिट ",
t_pasword_changed_successfully:"पासवर्ड चेंज हो गया है .",
// errors
e_old_password:"वैलिड ओल्ड पासवर्ड भरे ",	
e_new_password:"न्य पासवर्ड भरे ",	
e_confirm_password:"दोबारा भर हुआ पासवर्ड पहले पासवर्ड सी मेल कहना चाहिए ",	
e_invalid_request_recordnot_found:"इनवैलिड रिक्वेस्ट का रिकॉर्ड नही मिला .",	

// forgot password...
t_forgot_ur_password:"क्या अपना पासवर्ड भूल गए है ",	
t_letus_know_email:"हमे अपनी रजिस्टर्ड ईमेल बताइये .",
t_or:"या ",	
t_back2_login:"लॉगिन पर वापिस जाइये ",	
	
//bid board...
t_complete_payment_process:"बिड एक्सेप्ट करने सी पहले पेमेंट कम्पलीट करे .",	
	
	
	
	
	
	
};
 