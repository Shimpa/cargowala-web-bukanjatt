module.exports = {
  invalid_credential:"आपके द्वारा डाला गया ईमेल / पासवर्ड गलत है .",
  verify_email :"अपना ईमेल इनबॉक्स चेक करें और वेरीफाई करें या <a href='/shipper/resend-email-token' id='resend-verification-email'> वेरिफिकेशन मेल दोबारा भेजो पर क्लिक करे </a>.",
  inactive_user:"यूजर इनएक्टिव है| सर्वर अधिकारी से संपर्क करे .",
  recently_sent_email:"अपना ईमेल इनबॉक्स चेक करें, हमने आपको अभी एक मेल भेजी है .",
  invalid_token:"एरर आया है, वैलिड लिंक पर क्लिक करके दोबारा प्रयास करे  .",
  new_password:"अपना नया पासवर्ड भरे .",
  confirm_password:"पासवर्ड दोबारा भरे .",
  //regsitration...
  firstname :"पहला नाम 5-15 करैक्टर का होना चाहिए.",  
  lastname  :"अंतिम नाम 5-15 करैक्टर का होना चाहिए .",  
  email     :"कृपया सही ईमेल दर्ज करें  (eg. username@domain.xyz).",  
  mobile_number :"कृपया सही फ़ोन नंबर दर्ज करें. (eg. 9463944676).",  
  password  :"पासवर्ड भरना होगा  (6-15 करैक्टर का होना चाहिए) .", 
  confirm_password  :"पासवर्ड की पुष्टि कीजिये ", 
  image     :"इमेज  को नहीं छोड़ा जा सकता.",  
  email_error     :"आपके द्वारा डाला गया ईमेल गलत है .",  

  
//layout text and errors....
  
t_need_help:"सहायता चाहिए? ",
t_copyright:"कॉपीराइट  कार्गोवाला . आल  राइट्स  रिजर्व्ड ",
t_terms_of_service:"टर्म्स ऑफ़ सर्विस ",
t_privacy_policy:"प्राइवेसी पालिसी ",
//footer content...
t_terms_and_condition:"टर्म्स एंड कंडीशन्स ", 
t_welcome:"आपका स्वागत है ",
  
//menu text and links
t_home:'होम ',
t_regular_load_board:"रेगुलर बिड बोर्ड ",
t_bid_load_board:"बिड लोड बोर्ड ",  
t_confirm_orders:"कनफर्म्ड आर्डर ", 
t_payments:"पेमेंट्स ", 
t_incompleteshipments:"इन्कम्प्लीट शिपमेंट्स ", 
t_myshipments:"मेरी शिपमेंट्स ",  
t_myvehicles:"मेरे ट्रक ",  
t_mydrivers:"मेरे ड्राइवर ",  
t_membership_plans:"मेम्बरशिप प्लान  ", 
t_offers:"ऑफर ",  
t_notification:"नोटिफिकेशन ", 
t_helpandsupport:"हेल्प और सपोर्ट ",
  
t_edit_profile:"एडिट प्रोफाइल ",
t_logout:"लॉगआउट ", 
  
//shipper menu text and links
t_myshipments:'मेरी शिपमेंट्स ',
t_bookatruck:"ट्रक बुक करे ",

// contact us (help and support)
t_contact_us:"संपर्क करे ",
t_payment_faq:"पेमेंट सम्भदित जानकारी ",
t_login_faq:"लॉगिन सम्भदित जानकारी ", 
t_booking_truck:"ट्रक बुक करे ",


//language change links...
t_english:"English",  
t_hindi:"हिंदी",  
  
  
  
  
  
};