module.exports = function(shipper){
shipper.use('/', require('./routes/shipper/index'));
shipper.use('/users', require('./routes/shipper/users/'));
shipper.use('/dropdownlist', require('./routes/dropdownlist/'));
shipper.use('/shipments', require('./routes/shipper/shipments/'));
shipper.use('/users/my-shipments', require('./routes/shipper/users/'));
shipper.use('/bids', require('./routes/shipper/bids/'));
shipper.use('/users/offers', require('./routes/shipper/users/offers'));
shipper.use('/users/payments', require('./routes/shipper/users/payments'));
/*shipper.use('/my-shipments',function(req,res,next){
	var url = req.baseUrl.split('/').slice(-1);
	console.log(url);
	switch(url[0]){
		case 'my-shipments':
		req.url = '/users/my-shipments';
		next();	
		break;
		default:
		 req.url = '/users/dashboard';	
		break;	
	}
	next();
	/*if(url && url[0]=='my-shipments'){
	   req.url = '/shipments/';
	   next();
	}
	else next();*/
//});*/

//shipper.use('/bid', require('./routes/bid/'));	
//shipper.use('/page', require('./routes/page/'));
	
};