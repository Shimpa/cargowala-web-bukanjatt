"use strict";
var db   = require('mongoose');	
class Transactions{
	constructor(){
	}
 get SPENT(){ return 1}
 get RECIEVED(){ return 2}
 get RETURN(){ return 3}
	
 get Model(){
	return db.models.Transactions || db.model('Transactions',Schema(this),'transactions');  
 }
Load(data){ 
 return new this.Model(data);	
}	
 tType(index){
	var tt = {1:"Spent",2:"Recieved",3:"Return"};
	if(index) return Object.keys(tt); 
	return tt; 
 }	
}
function Schema(self){
  var schema = new db.Schema({
    amount:Number,
	payment_id:String,  
	status:{
		code:Number,
		text:String
	},
	debug:db.Schema.Types.Mixed,
	ttype:{ //transaction type
		code:{
			type:Number,
			enum:self.tType(1)
		},
		text:String
	},
	user_id:{
		type:db.Schema.Types.ObjectId,
	    ref :'Users'
	}	  
  },{'collection':'transactions'});	
  return schema	
}
module.exports =  Transactions;