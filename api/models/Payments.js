"use strict";
var db      = require('mongoose'),
    re      = appHelper('RegularExp'),
    worker  = appHelper('Worker'),
    TruckTypes  = appModel('TruckTypes'),
    Users   = appModel('Users'),
    Users   = new Users(),
	Notify  = appHelper('Notify'),	
	cache   = appHelper('Cache');	

/*Object.defineProperty(exports,'Users',{
	configrable:false,
	enumerable :true,
	get:function(){ return (new Users());}
});*/

var schema = Symbol("schema");
var tt     = Symbol("truck_types");
var arrtt  = Symbol('array_tt');

class Payments{

get FULLPAYMENTDONE(){ return 1}
get CASH(){ return 1}
get ONLINE(){ return 2}
get PENDING(){ return 0}
get SUCCESS(){ return 1}
get FAILED(){ return 3}
get LIMIT(){ return 2}
Add(u){
	return new this.Model(u);
}	
truckType(){
	this[tt].getAll({},function(err,tt){
	 return tt;	
	});
}
getLoadType(index){
	var lt = {
		1:"Partial Load",
		2:"Full Load"
	};
	if(index) return Object.keys(lt);
	return lt;
}
 get Model(){
	return db.models.Payments || db.model('Payments',Schema(this),'payments');  
 }		
orderStatus(index){
  var st = {
	        1:"Placed",
	        2:"Intransit",
	        5:"Completed",
  };
  if(index) return Object.keys(st);	
	 return st; 
}
/* notify shipper that your payment completed successfully
 if payment done on bidload board shipment then notify trucker and driver too
 @params req (object) contains shipment id,payment
 */	
PaymentCompletedOnline(req){
 try{
 this.Model.findOne({_id:req.session.payment._id}).populate('shipment_id').exec(function(err,payment){
	 var msg1 =req.__('shipments').t_firstinstalment_paymentdone;
	 if(req.session.payment.instalment==2)
	    msg1 =req.__('shipments').t_finalpaymentdone;
	 var amount =  payment.transactions[req.session.payment.instalment-1].amount;
	 var balance_payment =  payment.transactions[1].amount;
	 var msg = Common.replaceValue(msg1,			['<<amount>>','<<reference_no>>','<<balance_payment>>','<<support_no>>','<<support_email>>'],				  [amount,payment.shipment_id.shipment_id,balance_payment,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
		Notify.sendSMS(req.session.user.getMobileNo(),msg);
		Notify.sendPush(req.session.user.deviceToken(),msg,{shipmenr_id:payment.shipment_id._id});
       var msg = Common.replaceValue(msg1,			['<<amount>>','<<reference_no>>','<<balance_payment>>','<<support_no>>','<<support_email>>'],[amount,'<a href="'+absUrl(shipperUrl('shipments/detail/'+payment.shipment_id._id))+'">'+payment.shipment_id.shipment_id+'</a>',balance_payment,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]); Notify.sendMail(req.session.user.getEmail(),msg,__('shipments').t_subjectpaymentdone);
	   var trucker_id  = req.session.trucker?req.session.trucker._id:false;
	   Users.Model.findOne({_id:payment.shipment_id.trucker_id||trucker_id},'contact device name',function(err,user){
		    if(!user)return;
		    if(req.session.trucker)
		    delete req.session.trucker._id;
	       if(payment.shipment_id.shipment_type.code==5){ //bid load board 
		      if(req.session.payment.instalment==1){
				var firstins  = payment.transactions[0];  
				var secondins = payment.transactions[1];  
				var secondmode = payment.transactions[1].mode==1?"Cash":'Online';  
				var name = user.name.firstname+" "+user.name.lastname;
				var msg  = Common.replaceValue(req.__('shipments').t_paymentpaidbyshpper,			['<<trucker_name>>','<<amount>>','<<shipper_name>>','<<reference_no>>','<<balance_amount>>','<<paymenttype>>','<<amount_paid>>','<<payment_mode>>','<<balanace_amount_mode>>','<<support_no>>','<<support_email>>'],				  [name,payment.total,req.session.user.getName(),payment.shipment_id.shipment_id,balance_payment,'Partial',amount,'Online',secondins.amount,secondmode,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
				Notify.sendSMS(user.contact.mobile_number,msg); 
				var msg  = Common.replaceValue(req.__('shipments').t_paymentpaidbyshpper,			['<<trucker_name>>','<<amount>>','<<shipper_name>>','<<reference_no>>','<<balance_amount>>','<<paymenttype>>','<<amount_paid>>','<<payment_mode>>','<<balanace_amount_mode>>','<<support_no>>','<<support_email>>'],				  [name,payment.total,req.session.user.getName(),'<a href="'+absUrl(shipperUrl('shipments/detail/'+payment.shipment_id.shipment_id))+'">'+payment.shipment_id.shipment_id+'</a>',balance_payment,'Partial',amount,'Online',secondins.amount,secondmode,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
				Notify.sendMail(user.contact.email,msg,__('shipments').t_subjectpaymentdone);  
			  }else if(req.session.payment.instalment==2){
			    
				var firstins  = payment.transactions[0];  
				var secondins = payment.transactions[1];  
				var name = user.name.firstname+" "+user.name.lastname;
				var msg  = Common.replaceValue(req.__('shipments').t_notifytrucker4secondpayment,			['<<trucker_name>>','<<amount>>','<<shipper_name>>','<<reference_no>>','<<support_no>>'],	  [name,payment.amount,req.session.user.getName(),payment.shipment_id.shipment_id,Const.SUPPORT_NO]);
				Notify.sendSMS(user.contact.mobile_number,msg); 
				if(user.device && user.device.token){  
				   Notify.sendPush(user.device.token,msg,{shipment_id:payment.shipment_id.shipment_id}); 
				}
				var msg  = Common.replaceValue(req.__('shipments').t_notifytrucker4secondpayment,			['<<trucker_name>>','<<amount>>','<<shipper_name>>','<<reference_no>>','<<support_no>>'],[name,amount,req.session.user.getName(),'<a href="'+absUrl(shipperUrl('shipments/detail/'+payment.shipment_id.shipment_id))+'">'+payment.shipment_id.shipment_id+'</a>',Const.SUPPORT_NO]);
				Notify.sendMail(user.contact.email,msg,__('shipments').t_subjectpaymentdone); 
			  }
			  delete req.session.payment;
			  delete req.session.shipment; 
	       }
	   })    
   })
 }catch(e){
	 console.log(e.message);
 }
 }
/* notify shipper trucker if payment method by cash on loading*/
/* @params req(object) request instance,payment (object)	payment instance,shipment (object) */
PayCashOnLoading(req,payment,shipment){
	try{	

	 var msg1 =req.__('shipments').t_1stpaymentbycash;

	 var amount =  payment.transactions[0].amount;
	 var msg = Common.replaceValue(msg1,			['<<amount>>','<<shipper_name>>','<<referance_no>>','<<date>>','<<time>>','<<support_no>>','<<support_email>>'],				  [amount,req.session.user.getName(),shipment.shipment_id,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
		Notify.sendSMS(req.session.user.getMobileNo(),msg);
		if(req.session.user.deviceToken()){  
		  Notify.sendPush(req.session.user.deviceToken(),msg,{shipment_id:payment.shipment_id.shipment_id}); 
	    }
       var msg = Common.replaceValue(msg1,			['<<amount>>','<<shipper_name>>','<<referance_no>>','<<date>>','<<time>>','<<support_no>>','<<support_email>>'],[amount,req.session.user.getName(),'<a href="'+absUrl(shipperUrl('shipments/detail/'+shipment.shipment_id))+'">'+shipment.shipment_id+'</a>',shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]); Notify.sendMail(req.session.user.getEmail(),msg,__('shipments').t_subjectpaymentdone);

	       //if(payment.shipment_id.shipment_type.code==5){ //bid load board 
		     
				var firstins  = payment.transactions[0];
                var user = shipment.trucker_id;		
  				var name = doc.trucker_id.name.firstname+" "+doc.trucker_id.name.lastname;
				var msg  = Common.replaceValue(req.__('shipments').t_1stpaymentnotifytrucker,			['<<trucker_name>>','<<amount>>','<<date>>','<<time>>','<<support_no>>','<<support_email>>'],				  [name,amount,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
				Notify.sendSMS(user.contact.mobile_number,msg);
		        if(user.device && user.device.token){  
				   Notify.sendPush(user.device.token,msg,{shipment_id:payment.shipment_id.shipment_id}); 
				}
				
				var msg  = Common.replaceValue(req.__('shipments').t_paymentpaidbyshpper,			['<<trucker_name>>','<<amount>>','<<date>>','<<time>>','<<support_no>>','<<support_email>>'],[name,amount,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO,Const.SUPPORT_EMAIL]);
				Notify.sendMail(user.contact.email,msg,__('shipments').t_subjectpaymentdone);  
			
	   //})    
   //})
 }catch(e){
	 console.log(e.message);
 } 
}	

}
// schema structure
function Schema(self){
  if(!self)return;
  return new db.Schema({
		shipment_id    :{
			type:db.Schema.Types.ObjectId,
			ref :'Shipments'
		},
	    payer_id:{
			type:db.Schema.Types.ObjectId,
			ref :'Users'
		},
	    ship_by:{
			type:db.Schema.Types.ObjectId,
			ref :'Users'
		},
	    payment_type:Number,
	    total:Number,  
	    outstanding:Number,
	    transactions:[{
			_id:{
				type:db.Schema.Types.ObjectId,
			    ref :'Transactions'
			},
			instalment:Number,
			amount:Number,
			mode:Number,				
			remarks:String,
			status:Number,
		}],
	  status:{
	   type:Number,
       enum:[0,1],
	   default:0,
       } //1= complete payment done 0= payment pending

	},{collection:"payments"}); 

 };

module.exports = Payments;
