"use strict";
var db   = require('mongoose');	
var Notify    = appHelper('Notify');
class Trucks{
	constructor(){
	}
get ACTIVE(){return 1;}
get INACTIVE(){return 0;}
get AVAILABLE(){return 1;}
get NOTAVAILABLE(){return 0;}
get NOTASSIGNED(){return 0;}
get FREE(){return 0;}
get ASSIGNED(){return 1;}
get LIMIT(){return 8;}
get parent_dir(){return "vehicles";}
get child_dir(){return "vehicle";}	
 get Model(){
	return db.models.Trucks || db.model('Trucks',Schema(this),'vehicles');  
 }
 Insert(dta){
	return new this.Model(dta)
 }	
 Categories(index){
	var c = {1:"Below 5 Ton",2:"5 To 15 Ton",3:"Above 15 Ton"};  
	if(index)
	  return Object.keys(c);
	return c; 
 }
/* notify shipper trucker who are connected with current shipment
   if vehicle changed for current shipment then again notify users. 
 *@params req (object) request object,Shipments (instance),shipment_id (mongoose object id),vehicle_id(mongoose object), old_vehicle_id  (mongoose object) if old_vehicle_id then driver replaced for current shipment, old_vehicle_id=undefined then new vehicle assigned
 */
notifyShipperTrucker(req,Shipments,ship_id,vehicle_id,old_vehicle_id){
var vehicleins =  this;
Shipments.Model.findOne({_id:ship_id||false}).populate('trucker_id').populate('shipper_id').exec(function(err,shipment){
	   if(err) return;
		var trucker =  shipment.trucker_id,shipper = shipment.shipper_id;
  vehicleins.Model.findOne({_id:vehicle_id}).populate('truck_type').exec(function(err,vehicle){
		 if(err) return;		   
		// notify shipper
	    var msg = req.__('vehicles').t_notify_shipper_newvehicle_assigned;
	    if(old_vehicle_id)
		   msg =req.__('vehicles').t_notify_shipper_vehicle_changed;	
		msg= Common.replaceValue(msg,['<<regis_no>>','<<date>>','<<time>>','<<trucker_contactno>>','<<support_no>>'],[vehicle.registration.vehicle_number,shipment.transit.t_date,shipment.transit.t_time,trucker.contact.mobile_number,Const.SUPPORT_NO]);
		var smsmsg = msg.replace('<<reference_no>>',shipment.shipment_id);   
		Notify.sendSMS(shipper.contact.mobile_number,smsmsg);
		var emailmsg = msg.replace('<<reference_no>>','<a href="'+shipperUrl('shipments/detail/'+shipment._id)+'">'+shipment.shipment_id+'</a>');   
		Notify.sendMail(shipper.contact.email,emailmsg,req.__('vehicles').t_subject_vehicle_assigned);
		//notify trucker
		var msg1= req.__('vehicles').t_notifytruker_vehile_assigned,
		    shipper_name = shipper.name.firstname+" "+shipper.name.lastname,
			mangedriverlink ='<a href="'+truckerUrl('users/my-shipments')+'">'+req.__('drivers').t_manage_driver_vehicle+'</a>';
		msg1 = Common.replaceValue(msg1,['<<vehicle_name>>','<<regis_no>>','<<date>>','<<time>>','<<shipper_name>>','<<shipper_contactno>>','<<manage_driverslink>>','<<support_no>>'],[vehicle.truck_type.name,vehicle.registration.vehicle_number,shipment.transit.t_date,shipment.transit.t_time,shipper_name,shipper.contact.mobile_number,mangedriverlink,Const.SUPPORT_NO]);
		
		smsmsg =  msg1.replace('<<reference_no>>',shipment.shipment_id);   
		Notify.sendSMS(trucker.contact.mobile_number,smsmsg);
		
		smsmsg =  msg1.replace('<<reference_no>>',shipment.shipment_id);   
		   
		emailmsg = msg.replace('<<reference_no>>','<a href="'+shipperUrl('shipments/detail/'+shipment._id)+'">'+shipment.shipment_id+'</a>');
		Notify.sendMail(trucker.contact.email,emailmsg,req.__('vehicles').t_subject_vehicle_assigned);
  });
  });
}
	
}
function Schema(self){
  var re   = appHelper('RegularExp'),truck;
    truck  = new db.Schema({
	truck_type:{
		type:db.Schema.Types.ObjectId,
		ref:'TruckTypes',
		required:[true,__().e_truck_type]
	},
	truck_category:{type:Number,enum:self.Categories(1),
				   required:[true,__().e_truck_category]
		},
	//vehicle_number:{type:String,min:10,max:10},
	permit:{
		pic:{type:String,
			 required:[true,__().e_permit_image]
			},
		pnumber:{type:String,
			     validate:[function(v){ 
				    return re.isString(8,20,v);
				 },__().e_permit_number],
				 required:[true,__().e_permit_number]
			},
		permit_expiry_date:{type:Date,required:[true,__().e_permit_expiry_date]},
	},
	registration:{
	   rc_pic:{type:String,
			 required:[true,__().e_rc_image]
			},
	   vehicle_number:{type:String,
					   min:[10,__().e_minmaxvehicle_no],max:[10,__().e_minmaxvehicle_no],
					  required:[true,__().e_minmaxvehicle_no]
			},
	   expiry_date:{type:Date,required:[true,__().e_rc_expiry_date]}, 	
	},
	insurance:{
	  image:{type:String,
			 required:[true,__().e_insurance_image]
			},
	  renew_date:{type:Date,required:[true,__().e_insurance_expiry_date]}, 	
	},
	created_by:{
		type:db.Schema.Types.ObjectId,
		ref:'Moderators'
	},  
	status:{
		type:Number,
		enum:[0,1],
		default:1,
	},
	compliance:db.Schema.Types.Mixed,
	created_on:Date,
	modified_on:{type:Date,default:Date.now()},
	is_available:{type:Number,enum:[0,1],default:1},
	truck_status:{type:Number,enum:[0,1],default:0},
	owner_id:{
		type:db.Schema.Types.ObjectId,
		ref:'Users'
	}  
  },{'collection':'vehicles'});	
  return truck;	
}
module.exports =  Trucks;