"use strict";
var db    = require('mongoose');
class WebNotifications{
	constructor(){

	}
 get Model(){
	return db.models.WebNotifications || db.model('WebNotifications',Schema(this),'webnotifications');  
 }
 notify(message,user_id,can_view){
	 var data = {
		         message:message,
	            };
	 data['can_view'] = [];
	 data['can_view'].push(user_id);
	 if(can_view)
		data['can_view'].push(can_view);
	var n = new this.Model(data);
	n.save(function(err){
		console.log(err);
	}); 
 }
 	 

}
function Schema(self){
  var re   = appHelper('RegularExp'),driver;
    return new db.Schema({
	message:{
		type:String,
		max:900,
	},
	can_view:[], // who`s id in array that can view this notification... 1=admin	
	read:[], // user id added in array if they have red notification... blank mean unread	
	created_on:{
		type:Date,
		default:Date.now()
	},
  },{'collection':'webnotifications'});
    	
}
module.exports =  WebNotifications;