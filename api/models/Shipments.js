"use strict";
var db      = require('mongoose'),
    re      = appHelper('RegularExp'),
    worker  = appHelper('Worker'),
    TruckTypes  = appModel('TruckTypes'),
    Drivers  = appModel('Drivers'),
    Trucks  = appModel('Trucks'),
    Drivers  = new Drivers(),
    Trucks  = new Trucks(),
    TruckTypes  = appModel('TruckTypes'),
    TruckTypes  = new TruckTypes,
	cache   = appHelper('Cache'),	
	Notify  = appHelper('Notify');	

var tt     = Symbol("truck_types");
var arrtt  = Symbol('array_tt');
var _model = Symbol('_model');

class Shipments{
	constructor(){
	  //Schema.bind(this);	
      if(this[arrtt]===undefined)
	 	 this[arrtt]  = this.truckTypes();
	}
get PENDING(){ return 0;}
get TRUCKER_CANCELED(){ return 1;}
get SHIPPER_CANCELED(){ return 2;}
get EXPIRED(){ return 3;}
get INTRANSIT(){ return 4;}
get COMPLETED(){ return 5;}
get ACCEPTED(){ return 6;}
get BID_ACCEPTED(){ return 8;}
get INCOMPLETE(){ return 9;}
get REGULAR_BOARD(){ return 1;}
get BID_BOARD(){ return 5;}
get MYSHIPMENT_LIMIT(){ return 1;} //upcoming history active trucker LIMIT...
get LIMIT(){ return 6;} //ACTIVE HISTORY SHIPMENT LIMIT...
get LIMIT_BID_BOARD(){ return 6;} //ACTIVE HISTORY confirmation SHIPMENT LIMIT...
get PARTIAL_LOAD(){return 1;}	
get FULL_LOAD(){return 2;}	
get namespace(){
	return {
	         shipper:"{{shippername}}",
		     location:"{{loadingunloadingcity}}",
		     loadingcity:"{{loadingcity}}",
		     unloadingcity:"{{unloadingcity}}",
		     datetime:"{{shipmentdatetime}}",
		     date:"{{shipmentdate}}",
		     time:"{{shipmenttime}}",
		     trucker:"{{truckername}}",
		     bidder:"{{bidder}}"
	};
}	

	
get parent_dir(){
	return "shipments";
}
get child_dir(){
	return "shipment";
}
get Model(){
	   return db.models.Shipments || db.model('Shipments',Schema(this),'bookings');	
}
// get current status text....
currentStatus(code){
  var status = this.orderStatus();
	return status[code] || null
}	
Load(data){ 
 return new this.Model(data);	
}	
	
truckTypes(index){
	TruckTypes.getAll({},function(err,tty){
	var tt = {0:'Tata Ace',1:'Pick up',2:'Tata 407',3:'14 Feet Open',4:'14 Feet Close',5:'17 Feet Open',6:'17 Feet Close'};
	    if(index) return Object.keys(tt);
		  return tt;	  
	});	
}
getLoadType(index){
	var lt = {
		1:"Partial Load",
		2:"Full Load"
	};
	if(index) return Object.keys(lt);
	return lt;
}
LoadType(index){
	var lt = {
		1:"Partial Load",
		2:"Full Load"
	};
	return lt[index]||null;
}	
orderStatus(index){
  var st = {
	        0:"Pending",
	        1:"Trucker Canceled",
	        2:"Shipper Canceled",
	        3:"Expired",
	        4:"Intransit",
	        5:"Completed",
	        6:"Accepted",
	        8:"Bid Accepted",
	        9:"In Complete",
  };
  if(index) return Object.keys(st);	
	 return st; 
}
bidBoard(index){
	var bb = {
		1:"Regular Board",
		2:"Bid Board",
	};
     if(index) return Object.keys(bb);	
	 return bb;	
}
tripStatus(index){
	var ts = {
		0:"Assigned",
		1:"Intransit",
		2:"Completed"
	}
	if(index) return Object.keys(ts);	
	 return ts;	
}
// if shipment time updated then notify users...
//@params drivers assigned vehicles notify drivers if assiged to current shipment
//@params shipment_id check if bids posted	on it notify them
timeUpdated(drivers,shipment_id,pushMsg,emailMsg){
  if(drivers.length>0){
	var tokens = [],mobile_nos = [];	
    drivers.forEach(function(driver){
	   if(driver.device['token'])
	      tokens.push(driver.driver.device['token']);
	      mobile_nos = driver.mobile_no;  
       });
	  this.NotifyDrivers(tokens,mobile_nos,pushMsg,{shipment_id:shipment_id});
    }
  }
NotifyDrivers(device_tokens,mobile_nos,msg){
	if(device_tokens)
	   Notify.sendPush(device_tokens,msg);
	if(mobile_nos)
       Notify.sendSMS(mobile_nos,msg);
}	
/* shipper accepted trucker posted bid change shipment status to bid accepted
 * params id (shipment_id),trucker_id whose  bid accepted
 */
bidAccepted(id,trucker_id){
  this.Model.update({_id:id},{$set:{status:this.ACCEPTED,trucker_id:trucker_id}},function(err,affected){
	//console.log(err);  
  })
 }
/* method invokes when trucker canceled shipment
 * data (Object) contains truker data
 * msg (String) text message 
 */
Canceled(data,msg){
	if(data.shipper_id.device_token)
	  Notify.sendPush(data.shipper_id.device_token,msg,{shipment_id:data._id});	
	if(data.shipper_id.contact && data.shipper_id.contact.mobile_number)
	   Notify.sendSMS(data.shipper_id.contact.mobile_number,msg);	
	if(data.shipper_id.contact && data.shipper_id.contact.email)
	   Notify.sendMail(data.shipper_id.contact.email,msg,__('shipments').t_subjectcanceledbytrucker);	
	Notify.webNotification(msg,data.shipper_id._id);
}
regularBoardAccepted(data,msg){
   Notify.webNotification(msg,data._id);	
   Notify.sendPush(data.device_token,msg);
   Notify.sendSMS(data.mobile_no,msg);
   Notify.sendMail(data.email,msg,__('shipment').t_subjectregularbidaccedped);
}
/*notify shipper shipment posted on bid load board */
/* @params request (object)	*/
PostedOnBidBoard(req){
	try{
	this.Model.findOne({_id:req.session.shipment._id}).populate('shipper_id').exec(function(err,shipment){
		var msg = Common.replaceValue(req.__('shipments').t_postedon_bidloadboard,			['<<shipper_name>>','<<amount>>','<<reference_no>>','<<date>>','<<time>>','<<support_no>>'],				  [req.session.user.getName(),shipment.shipment_detail.overall_total,shipment.shipment_id,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO]);
	    Notify.webNotification(msg,req.session.shipment._id);	
		Notify.sendSMS(req.session.user.getMobileNo(),msg);
		Notify.sendPush(req.session.user.deviceToken(),msg,{shipment_id:shipment._id});
       var msg = Common.replaceValue(req.__('shipments').t_postedon_bidloadboard,			['<<shipper_name>>','<<amount>>','<<reference_no>>','<<date>>','<<time>>','<<support_no>>'],				  [req.session.user.getName(),shipment.shipment_detail.overall_total,'<a href="'+absUrl(shipperUrl('shipments/detail/'+shipment._id))+'">'+shipment.shipment_id+'</a>',shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO]); Notify.sendMail(req.session.user.getEmail(),msg,__('shipments').t_subjectpostedonbidloadboard);
		
	   	
	   	
	});
	}catch(e){
		console.log(e.message);
	}
}
/* notify shiiper shipment posted on regulr board */
PostedOnRegularBoard(req){
	try{
	this.Model.findOne({_id:req.session.shipment._id},function(err,shipment){
		var msg = Common.replaceValue(req.__('shipments').t_postedon_regularboard,			['<<shipper_name>>','<<amount>>','<<reference_no>>','<<date>>','<<time>>','<<support_no>>'],				  [req.session.user.getName(),shipment.shipment_detail.overall_total,shipment.shipment_id,shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO]);
		Notify.sendSMS(req.session.user.getMobileNo(),msg);
		Notify.sendPush(req.session.user.deviceToken(),msg,{shipment_id:req.session.shipment._id});	
       var msg = Common.replaceValue(req.__('shipments').t_postedon_regularboard,			['<<shipper_name>>','<<amount>>','<<reference_no>>','<<date>>','<<time>>','<<support_no>>'],				  [req.session.user.getName(),shipment.shipment_detail.overall_total,'<a href="'+absUrl(shipperUrl('shipments/detail/'+shipment._id))+'">'+shipment.shipment_id+'</a>',shipment.transit.t_date,shipment.transit.t_time,Const.SUPPORT_NO]); Notify.sendMail(req.session.user.getEmail(),msg,__('shipments').t_subjectpostedonregularloadboard);
	  
	  	
	});
	}catch(e){
		console.log(e.message);
	}
}	
/* order canceled by trucker do avaialeble vehicle and driver
 * @param driverids (array) driver ids, vehicleids (array) vehicle ids
*/
CanceledByTruckerFreeDriverVehicle(driverids,vehicleids){
	if(driverids.length>0){
	   Drivers.Model.update({_id:{$in:driverids}},{$set:{driver_status:Drivers.NOTASSIGNED}},{multi:true},function(err,affected){});

	}
	if(vehicleids.length>0){
	   Trucks.Model.update({_id:{$in:vehicleids}},{$set:{truck_status:Trucks.NOTASSIGNED}},{multi:true},function(err,affected){}) 	
	
	}
}	
/* notify shipper trucker and driver(s) if assigned to this shipment
 @params shipment_id(shipment_id)
 */
notifyDateTimeChanged(shipment_id,oldtime){
	this.Model.findOne({_id:shipment_id})
		.populate('shipper_id','contact name device')
		.populate('trucker_id','contact name device')
		.populate('vehicle.driver','mobile_no name')
		.exec(function(err,shipment){
		       var trucker = shipment.trucker_id,
				   shipper = shipment.shipper_id,
				   drivers = shipment.vehicle,truckername=null,trucker_mobileno=null,
				   shippername = shipper.name.firstname+" "+shipper.name.lastname;
		           if(trucker){
				      var truckername = trucker.name.firstname+" "+trucker.name.lastname,
						  trucker_mobileno=trucker.contact.mobile_number;
					   
				   }
		Notify.sendMail(shipper.contact.email,Common.replaceValue(
			  __('shipments').t_notifyshippertimechanged,['<<shipper_name>>','<<reference_no>>',
              '<<old_time>>','<<new_time>>','<<contactno_trucker>>','<<contactno_driver>>',
			  '<<support_no>>'],[shippername,shipment.shipment_id,oldtime,shipment.transit.t_time,
              trucker_mobileno,Const.SUPPORT_NO]),__('shipments').t_subject_datetimechanged);
		if(truckername){
		var msg = Common.replaceValue(__('shipments').t_notifytruckertimechanged,                                               ['<<trucker_name>>','<<reference_no>>','<old_time>>',
								       '<<new_time>>','<<contactno_shipper>>',                '<<support_no>>'],[truckername,shipment.shipment_id,oldtime,
										shipment.transit.t_time,shipper.contact.mobile_number,
				   Const.SUPPORT_NO]);
		Notify.sendMail(trucker.contact.email,msg,__('shipments').t_subject_datetimechanged)
		Notify.sendSMS(trucker_mobileno,msg);
		}
		drivers.forEach(function(driver){
		  var drivername = driver.name.firstname+" "+driver.name.lastname;	
		  var msg = Common.replaceValue(__('shipments').t_notifytruckertimechanged,                                               ['<<driver_name >>','<<reference_no>>','<old_time>>',
								       '<<new_time>>','<<contactno_shipper>>',                '<<support_no>>'],[drivername,shipment.shipment_id,oldtime,
										shipment.transit.t_time,shipper.contact.mobile_number,
				   Const.SUPPORT_NO]);	
		  Notify.sendSMS(driver.mobile_no,msg);
		})
	    });
}	
/* trucker confirmed shippment accepeted by shipper send notification to shipper tabout this..
 * @params msg (string) (text,email,push message),dta (object) contain shipment shipper info
 */
/*truckerConfirmedShipment(msg,dta){
 try{
	if(dta.shipper_id.contact.mobile_number) //send sms
	   Notify.sendSMS(dta.shipper_id.contact.mobile_number,msg);
	 
	if(dta.shipper_id.contact.email) // send email
	   Notify.sendMail(dta.shipper_id.contact.mobile_number,msg);
	
	if() 
	  	
 }catch(e){
	new Logger(e.message); 
 }
}*/	
}
// schema structure
function Schema(self){
  if(!self)return;
  var ship= new db.Schema({
		loading:{
	            lp_address:String,
			    lp_city:String, 
			    lp_state:String, 
			    lp_latitude:Number, 
			    lp_longitude:Number, 
		},
	    unloading:{
	            up_address:String,
			    up_city:String, 
			    up_state:String, 
			    up_latitude:Number, 
			    up_longitude:Number, 
		},
	    transit:{
			    t_date:String, //it is for display text date 
			    t_time:String,
			    t_expdatetime:Number,
			    est_distance:Number,
			    est_time:String,
			    date:Date,// shipment transit date date object for seaching orting by transit date
		},
	    truck:{
			  category:Number,
              ttype:{type:db.Schema.Types.ObjectId,ref:'TruckTypes'},
              quantity:Number,
              load_type:Number,
              priority_delivery:Number,
		},
	    redirect_to:String,
	    items:[{
			   id:Number,
			   name:String,
			   desc:String,
			   parent_category:{type:db.Schema.Types.ObjectId,ref:'Categories'},
			   sub_category:{type:db.Schema.Types.ObjectId,ref:'Categories'},
			   item_weight:Number,
			   total_item:Number,
			   total_weight:Number,			   
			   invoice_image:String,			   
			   weight:Number,			   
			   length:Number,			   
			   width:Number,			   
			   height:Number,			   
			   			   
		}], 
	    taxes:db.Schema.Types.Mixed, 
	    items_total:Number, 
	    shipment_weight:Number,
	    invoicing_type:Number,
	    priority_delivery_charges:Number, 
	    order_type:db.Schema.Types.Mixed, 
	    insurance :{
			        amount:Number,
			        text:String,
			        insured_option:Number, 
			        insurance_image:String,
		},
	    payments  :db.Schema.Types.Mixed,
	    status    :{
		          type:Number,
			      enum:self.orderStatus(1)
		},
            web:{
			type:Number,
			enum:[0,1]
		},
	    who_accepted:{
                      type:db.Schema.Types.ObjectId,
			          ref:'Users', 
		},
	    shipper_id:{
                      type:db.Schema.Types.ObjectId,
			          ref:'Users', 
		},
	    trucker_id:{
                      type:db.Schema.Types.ObjectId,
			          ref:'Users', 
		},
	    vehicle:[{
			id:{
				type:db.Schema.Types.ObjectId,
				ref:'Trucks',
			},
			driver:{ type:db.Schema.Types.ObjectId,
				     ref:'Drivers', 
				   },
			status:{type:Number,enum:self.tripStatus(1)},
			start_on:Date,
			time_at:Date, 
			end_at:Date,
			location:{
			       lat:Number,
			       lng:Number,
				   text:String, 
		   }
		//},
		}],
	    shipment_id:{type:String,max:12,min:10},
	    shipment_detail:db.Schema.Types.Mixed,
	    shipment_type:db.Schema.Types.Mixed,
	    created_on  :{type:Date,
		},
	    modified_on :{type:Date,default:DateTime.now()},
		isvalidated:{
			type:Number,
			enum:[0,1]
		}
	},{collection:"bookings"}); 
    ship.path('loading.lp_address').validate(function(v){ 
	    if(this.loading.lp_city==null || this.loading.lp_state==null)
		   this.invalidate('loading.address',__().e_loading_address);
		if(this.unloading.up_city==null || this.unloading.up_state==null)
		   this.invalidate('loading.address',__().e_unloading_address);
	});
	ship.path('truck.category').validate(function(v){ 
	    if(this.truck.category==null)
		   this.invalidate('truck.category',__().e_truck_category);
		if(this.truck.ttype==null)
		   this.invalidate('truck.ttype',__().e_truck_type);
		if(this.truck.load_type==null)
		   this.invalidate('truck.load_type',__().e_truck_loadtype);

		if(this.truck.load_type==1){
		   if(parseInt(this.truck.quantity)<=0)
		       this.invalidate('truck.quantity',__().e_truck_quantity);
		   if(!this.truck.priority_delivery in {0:"priority off",1:"priority on"})
		       this.invalidate('truck.quantity',__().e_truck_priority_delivery);	
		}
	});
	ship.path('items_total').validate(function(v){
		
	    if(Validate.isEmpty(this.items_total) || isNaN(parseFloat(this.items_total)) || parseFloat(this.items_total)<=0)
		   this.invalidate('items_total',__().e_items_total);
	});
	return ship;
}

function registerIndex(schema){ 
  schema.index({'contact.email':1,'contact.mobile_no':1,hash_token:1,password:1});
}
module.exports = Shipments;
