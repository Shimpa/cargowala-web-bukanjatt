"use strict";
var db     = require('mongoose');
class Settings{

get parent_dir(){
	return "settings";
}
get child_dir(){
	return "setting";
}	
constructor(){
	
}
Add(u){ 
	return new this.Model(u);
}
get Model(){ //console.log(Schema(this))
        return db.models.Settings || db.model('Settings',Schema(this),'admin_settings'); 
 
};

}
// schema structure
function Schema(self){
  if(!self)return;
  return new db.Schema({		
		created_on:{type:Date},
		modified_on:{type:Date},	
	},{collection:"admin_settings"});		
 };

module.exports = Settings;
