"use strict";
var db   = require('mongoose'); 
var Notify    = appHelper('Notify');
var Payments = appModel('Payments');
var Payments = new Payments();
class Drivers{
	constructor(){
	}
get ACTIVE(){return 1;}
get INACTIVE(){return 0;}
get AVAILABLE(){return 1;}
get NOTAVAILABLE(){return 0;}
get ASSIGNED(){return 1;}
get NOTASSIGNED(){return 0;}
get LIMIT(){return 8;}
get parent_dir(){return "drivers";}
get child_dir(){return "driver";}	
 get Model(){
	return db.models.Drivers || db.model('Drivers',Schema(this),'drivers');  
 }
 Insert(dta){
	return new this.Model(dta)
 }	
 Categories(index){
	var c = {1:"Below 5 Ton",2:"5 To 15 Ton",3:"Above 15 Ton"};  
	if(index)
	  return Object.keys(c);
	return c; 
 }
/* notify shipper trucker and drivers who has linked with current shipment
   if driver changed for current shipment then again notify users. 
 *@params req (object) request object,Shipments (instance),shipment_id (mongoose object id),driver_id(mongoose object), old_driver_id  (mongoose object) if old_driver_id then driver replaced for current shipmen, old_driver_id=undefined then new driver assigned
 */
notifyDriverShipperTrucker(req,Shipments,ship_id,driver_id,old_driver_id){
var driverins = this;
Shipments.Model.findOne({_id:ship_id||false}).populate('trucker_id').populate('shipper_id').exec(function(err,shipment){
	   if(err) return;
		var trucker =  shipment.trucker_id,shipper = shipment.shipper_id;
  driverins.Model.findOne({_id:driver_id},function(err,driver){
		 if(err || !driver) return;		   
		// notify shipper
	    var msg = req.__('drivers').t_notify_shipper_newdriver_assigned;
	    if(old_driver_id)
		   msg =req.__('drivers').t_notify_shipper_driver_changed;	
		msg= Common.replaceValue(msg,['<<driverphone_number>>','<<date>>','<<time>>','<<trucker_contactno>>','<<support_no>>'],[driver.mobile_no,shipment.transit.t_date,shipment.transit.t_time,trucker.contact.mobile_number,Const.SUPPORT_NO]);
		var smsmsg = msg.replace('<<reference_no>>',shipment.shipment_id);   
		Notify.sendSMS(shipper.contact.mobile_number,smsmsg);
	    if(shipper.device && shipper.device['token'])
		   Notify.sendPush(shipper.device['token']||null,smsmsg,{shipment_id:ship_id});
		var emailmsg = msg.replace('<<reference_no>>','<a href="'+shipperUrl('shipments/detail/'+shipment._id)+'">'+shipment.shipment_id+'</a>');   
		Notify.sendMail(shipper.contact.email,emailmsg,req.__('drivers').t_subject_driver_assigned);
		//notify trucker
		var msg1= req.__('drivers').t_notify_trucker_newdriver_assigned,
		    shipper_name = shipper.name.firstname+" "+shipper.name.lastname,
			mangedriverlink ='<a href="'+truckerUrl('users/my-shipments')+'">'+req.__('drivers').t_manage_driver_vehicle+'</a>';
		msg1 = Common.replaceValue(msg1,['<<driverphone_number>>','<<date>>','<<time>>','<<shipper_name>>','<<shipper_contactno>>','<<manage_driverslink>>','<<support_no>>'],[driver.mobile_no,shipment.transit.t_date,shipment.transit.t_time,shipper_name,shipper.contact.mobile_number,mangedriverlink,Const.SUPPORT_NO]);
		
		smsmsg =  msg1.replace('<<reference_no>>',shipment.shipment_id);   
		Notify.sendSMS(trucker.contact.mobile_number,smsmsg);
	   if(trucker.device && trucker.device['token'])
		  Notify.sendPush(trucker.device['token'],smsmsg,{shipment_id:ship_id});
		
		smsmsg =  msg1.replace('<<reference_no>>',shipment.shipment_id);   
		   
		emailmsg = msg.replace('<<reference_no>>','<a href="'+shipperUrl('shipments/detail/'+shipment._id)+'">'+shipment.shipment_id+'</a>');
		Notify.sendMail(trucker.contact.email,emailmsg,req.__('drivers').t_subject_driver_assigned);
  Payments.Model.findOne({shipment_id:shipment._id},function(err,payment){
			var transst = payment.transactions[0],
				transnd= payment.transactions[1];
		// notify drivers
		var msg2 = '',load_type = parseInt(shipment.truck.load_type)==1?req.__('shipments').t_partial_load:req.__('shipments').t_full_load;
		 var balance_mode = transnd.mode==1?"Cash":"Online"; 	
		if(transst.mode==2 && transst.status==1){ // paid online
		  msg2 = Common.replaceValue(req.__('drivers').t_notify_driver_assigned2shipmentpaymentonline,['<<date>>','<<time>>','<<shipper_name>>','<<shipper_contactno>>','<<pickup_city>>','<<delivery_city>>','<<load_type>>','<<weight>>','<<no_of_truckes>>','<<total_amount>>','<<amount_paid_type>>','<<paid_amount>>','<<mode_of_payment>>','<<balance_amount>>','<<mode_for_balance>>','<<support_no>>'],[shipment.transit.t_date,shipment.transit.t_time,shipper_name,shipper.contact.mobile_number,shipment.loading.lp_city,shipment.unloading.up_city,shipment.unloading.up_city,load_type,shipment.shipment_weight/1000,shipment.truck.quantity,payment.total,'Partial',transst.amount,'Online',transnd.amount,balance_mode,Const.SUPPORT_NO]);   
		}else if(transst.mode==1 && transst.status==0){
		   msg2 = Common.replaceValue(req.__('drivers').t_notify_driver_assigned2shipmentpaymentcashonpickup,['<<date>>','<<time>>','<<shipper_name>>','<<shipper_contactno>>','<<pickup_city>>','<<delivery_city>>','<<load_type>>','<<weight>>','<<no_of_truckes>>','<<total_amount>>','<<amount_paid_type>>','<<paid_amount>>','<<mode_of_payment>>','<<balance_amount>>','<<mode_for_balance>>','<<support_no>>'],[shipment.transit.t_date,shipment.transit.t_time,shipper_name,shipper.contact.mobile_number,shipment.loading.lp_city,shipment.unloading.up_city,shipment.unloading.up_city,load_type,shipment.shipment_weight/1000,shipment.truck.quantity,payment.total,'Partial',transst.amount,'Cash',transnd.amount,balance_mode,Const.SUPPORT_NO]); 	
		}
	    smsmsg = msg2.replace('<<reference_no>>',shipment.shipment_id); 
		Notify.sendSMS(driver.mobile_no,smsmsg);
	    if(driver.device && driver.device['token']) 
		   Notify.sendPush(driver.device['token']||null,smsmsg,{shipment_id:ship_id});
	    
   });
  });	
 });
}	
}
function Schema(self){
  var re   = appHelper('RegularExp'),driver;
    driver = new db.Schema({
	truck_type:{
		type:Number,
		required:[true,__('shipments').e_truck_loadtype]
	    },
	image:{type:String,
		   required:[true,__('drivers').e_driver_image]
		},
	date_of_birth:{type:Date,required:[true,__('drivers').e_driver_dob]
		},
	name:{
		firstname:{
			type:String,
			required:[true,__('drivers').e_firstname]
		},
		lastname:{
			type:String,
			required:[true,__('drivers').e_lastname]
		},
	},	
	mobile_no:{
		type:Number,
				validate:[function(v){
						return re.isMobileno(v); 
					},
				__('drivers').e_mobile_number],
			    required:[true,__('drivers').e_mobile_number],
	},	
	device_token:String,
	device:{
			  token:String,
			  status:{
				  code:Number,
				  text:String,
			  },
		},	
	password:{type:String},
	licence:{
		number:{
			type:String,
			min:[4,__('drivers').e_driver_licence],
			max:[25,__('drivers').e_driver_licence],
			required:[true,__('drivers').e_driver_licence],
		},
		expiry:{
		     type:Date,
			 required:[true,__('drivers').e_licence_expiry], 
		},
		image:{
		     type:String,
			 required:[true,__('drivers').e_licence_image], 
		},
	},
	address:{
	   address:{
		  type:String,
		  required:[true,__('drivers').e_address],   
	   },
	   city:{
		  type:String,
		  required:[true,__('drivers').e_city],   
	   },
	   state:{
		  type:String,
		  required:[true,__('drivers').e_state],   
	   },
	   pin_code:{
		  type:Number,
		  required:[true,__('drivers').e_pincode],   
	   },
	   adhaar_no:{
		  type:Number,
		  validate:[function(v){
			return re.isNumber(12,12,v);  
		  },__('drivers').e_adhaar_no],
		  required:[true,__('drivers').e_adhaar_no],   
	   },
	   adhaar_image:{
		  type:String,
		  required:[true,__('drivers').e_adhaar_image],   
	   },   	
	},	
	status:{
		type:Number,
		enum:[0,1],
		default:1
	},
	driver_status:{
		type:Number,
		enum:[0,1],
		default:0
	},
	is_available:{
		type:Number,
		enum:[0,1],
		default:1
	},	
	location:{
			       lat:Number,
			       lng:Number,
				   text:String, 
    },	
	compliance:db.Schema.Types.Mixed,	
	created_on:Date,
	modified_on:{type:Date,default:Date.now()},
	created_by:{
		type:db.Schema.Types.ObjectId,
		ref:'Moderators'
	},	
	owner_id:{
		type:db.Schema.Types.ObjectId,
		ref:'Users'
	}  
  },{'collection':'drivers'});
    driver.virtual('confirm_password')
        .get(function() {
             return this._confirm_password;
        })
        .set(function(value) {
		    this._confirm_password =value;
    });
	  driver.path('password').validate(function(v){
		if(this.isNew){
		 if(Validate.isEmpty(this.password))	
			this.invalidate('password',__('drivers').e_driver_password);
		 	
		} 
		if(!Validate.isEmpty(this.password)){ 
			if(this.password!=this.confirm_password && this.updatePassword)
			   this.invalidate('confirm_password',__('drivers').e_confirm_password);
		   
		}
	},null);
	driver.pre('save',function(next){
	   beforeSave.call(this,self,next);
  });
  return driver;	
}

/* before save function 
 * invalidate key is used to skip already exist
   validation check when we will use find save
   function
 */

function beforeSave(Driver,next){
 var self = this,
	 worker  = appHelper('Worker'),
	 errMsg = __('drivers').e_mobile_no_alredy_exist;
	//console.log(self);

  if(self.updatePassword){
	 self.password   = MD5(self.password); 
  }
 if(self.isNew){
	self.created_on = Date.now()  
	self.password   = MD5(self.password);
 }else self.modified_on  = Date.now();
console.log('------------------ mobile no changed ------------')	
console.log(self.mobilenoChanged)	
if(self.mobilenoChanged || self.isNew){
 Driver.Model.findOne({'mobile_no':self.mobile_no},'mobile_no',function(err,u){
	if(err)return new Logger(err);
	else if(u){
		self.invalidate('mobile_no',errMsg);
		next(new Error(errMsg));
	}else{ 
	   /*if(self.isNew){			
	      worker.execPHP('Mailer.php '+self.contact.email+" confirmemail "+self.hash_token +" "+ self.name.firstname,function(err,res){
		         console.log(err);	 
		         //console.log(res);	 
		         
          });*/
		  next(); 
       }
    })
}else next();

}
module.exports =  Drivers;