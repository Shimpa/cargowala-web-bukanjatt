"use strict";
class ancModel{
	constructor(){
		this._db = require('mongoose'); 
		this._lang = appLanguage; 
		this._re   = appHelper('RegularExp'); 
		this._worker  = appHelper('Worker'); 
		this._cache   = appHelper('Cache'); 
	}
 get db(){
	return this._db; 
 }	
	
}

module.exports = ancModel;