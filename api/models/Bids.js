"use strict";
var db      = require('mongoose');
var Notify  = appHelper('Notify'); 
/*Object.defineProperty(exports,'Users',{
	configrable:false,
	enumerable :true,
	get:function(){ return (new Users());}
});*/

class Bids{
constructor(){}
get POSTED(){ return 0;}	
get ACCEPTED(){ return 1;}	
get REJECTED(){ return 2;}	
get parent_dir(){
	return "bids";
}
get child_dir(){
	return "bid";
}
orderStatus(index){
  var st = {
	        0:"Pending",
	        1:"Accepted",
	        2:"Rejected",
};
  if(index) return Object.keys(st);	
     return st; 
}
Insert(r){
 return this.Model(r);	
}
get Model(){
	return db.models.Bids || db.model('Bids',Schema(this),'bids');	
}	
Bid(dta){
 return new this.Model(dta);	
}

	
/*notify shipper if trucker posted a bid or update bid on shipper posted shipments
 *@params data (object) shipper info,_template  (object) a text with namspace,bid_updated (boolean) internal use *only */	
bidPlaced(data,_template,shipper_id,reflink,shipment,updated){
 Notify.webNotification(_template,shipper_id);	
 Notify.sendPush(data.device_token,_template,{shipment_id:shipment._id});
 var msg =  _template.replace('<<reference_no>>',shipment.reference_no);		
 Notify.sendSMS(data.mobile_no,msg);
 _template = _template.replace('<<reference_no>>',reflink);
 if(updated)	
   Notify.sendMail(data.email,_template,__('shipments').t_subjectbidupdeted);
 else Notify.sendMail(data.email,_template,__('shipments').t_subjectnewbidposted);	
}
/* if trucker makes changes in their already posted bid notify Shipper */	
bidUpdated(data,_template,shipper_id,reflink,refno){ this.bidPlaced(data,_template,shipper_id,reflink,refno,true); }	
/* if shipper updated shipment time and truker posted bids on it notify them
 * @params shipment_id bids podted on which shipment
 */
shipmentTimeUpdated(shipment_id,msg,emailMsg){
	this.Model.find({shipment_id:shipment_id}).populate('trucker_id','device_token contact').exec(function(err,doc){
		var accepted = [],bidder=[];
		doc.forEach(function(bid){
			if(doc.status==this.POSTED && doc.trucker_id.device['token'])
			   bidder.push(doc.trucker_id.device['token']);
			else if(doc.status==this.ACCEPTED && doc.trucker_id.device['token'])
			   accepted.push(doc.trucker_id.device['token']);	
		    Notify.sendMail(bid.trucker_id.contact.email,emailMsg);
		});
		if(accepted.length>0){
			bidder=null;
			Notify.sendPush(accepted,msg,{'shipment_id':shipment_id});
			accepted = null;
		}else{ 
			accepted=null;
			Notify.sendPush(bidder,msg,{'shipment_id':shipment_id});			
			bidder=null;
		}
	});
}
/* if shipper accepted bid posted by trucker notify them
 *@params doc (Object) contains shiper and trucker and shipment data,
 *msg (String) text message
 */
bidAccepted(doc,msg){
  if(doc.trucker_id.contact && doc.trucker_id.contact.email)
	 Notify.sendMail(doc.trucker_id.contact.email,msg,__('bids').t_trucker_bid_accepted);
  if(doc.trucker_id.contact && doc.trucker_id.contact.mobile_number)	
	 Notify.sendSMS(doc.trucker_id.contact.mobile_number,msg);
  if(doc.trucker_id.device['token'])	
	Notify.sendPush(doc.trucker_id.device['token'],msg,{shipment_id:doc.shipment_id._id});
  Notify.webNotification(msg,doc.trucker_id._id);	
}
bidRejected(doc,msg){
  if(doc.trucker_id.contact && doc.trucker_id.contact.email)
	 Notify.sendMail(doc.trucker_id.contact.email,msg,__('bids').t_trucker_bid_rejected);
  if(doc.trucker_id.contact && doc.trucker_id.contact.mobile_number)	
	 Notify.sendSMS(doc.trucker_id.contact.mobile_number,msg);
  if(doc.trucker_id.device['token'])	
	Notify.sendPush(doc.trucker_id.device['token'],msg,{shipment_id:doc.shipment_id._id});
  Notify.webNotification(msg,doc.trucker_id._id);	
}
/* notify shipper for trucker bid accepted...
*/
notifyShipper(dta,msg){
  if(dta.email)
	 Notify.sendMail(dta.email,msg);
  if(dta.mobile_no)	
	 Notify.sendSMS(dta.mobile_no,msg);
 }	
}
// schema structure
function Schema(self){
  if(!self)return;
  var sch= new db.Schema({
		trucker_id:{
			    type:db.Schema.Types.ObjectId,
			    ref:'Users',
			    required:__().e_invalid_request,
		},
	    shipment_id:{
			    type:db.Schema.Types.ObjectId,
			    ref:'Shipments',
			    required:__().e_invalid_request,
		},
	    price:{type:Number,validate:[function(v){
			return parseFloat(this.price)>=0;
		},__().e_bid_amount],required:__().e_bid_amount
	    },
	    description:String,
	    status:{type:Number,enum:self.orderStatus(1)},
	    created_on:{type:Date},
	    modified_on:{type:Date,default:Date.now()},
		
	},{collection:"bids"});

return sch;	
 };

function registerIndex(schema){ 
  schema.index({'trucker_id':1,status:1});
  schema.index({'shipper_id':1,status:1});
}
module.exports = Bids;
