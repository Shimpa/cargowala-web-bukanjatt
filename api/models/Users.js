"use strict";
var db     = require('mongoose');
let schema = Symbol("schema");
let model  = Symbol("model");
let _usr   = Symbol('_usr');
class Users{

get ACTIVE(){ return 1;}
get INACTIVE(){ return 0;}
get BLOCKED(){ return 2;}
get INDIVIDUALSHIPPER(){ return 21;}
get INDIVIDUALTRUCKER(){ return 11;}
get SHIPPERCOMPANY(){ return 22;}
get TRUCKERCOMPANY(){ return 12;}
get AGENT(){ return 3;}
get parent_dir(){return "users";}
get child_dir(){return "user";}
get FREE_MEMBER(){ return 0; } //FREE MEMBER	
get GOLD_MEMBER(){ return 2; } //GOLD MEMBER	
get SILVER_MEMBER(){ return 1; } //SILVER MEMBER	

constructor(){ //console.log(process);
	let _self          = this;
	this.isActive      = 0;
    this.validPassword = false;
    this._oldPassword  = null;
	this.userType      = 0;
	this.isTrucker     = 0;
	this.isShipper     = 0;
	_self =null;	
}
Add(u){ 
	return new this.Model(u);
}
get Model(){ //console.log(Schema(this))
        return db.models.Users || db.model('Users',Schema(this),'users'); 
 
 };
 set oldPassword(old_password){
	this._oldPassword  = old_password; 
 }	
 load(condition,callback,fields){ 
	 var fields = fields || null,self=this;
	 this.isActive      = 0;
	 this.validPassword = false;
	 var query  = this.Model.findOne(condition);
	 fields && query.select(fields);
	 query.exec(function(err,res){  
	     self[_usr] = res;           
		 try{ 
			self.validPassword = MD5(self._oldPassword)===res.password; 
			self.isActive      = res.status===1; 
			self.isTrucker     = res.role in {11:"i",12:"fo"}; 
			self.isShipper     = res.role in {21:"i",22:"c"}; ; 
		    callback.call(null,err,res);  
		 }catch(e){
			callback.call(null,err,res); 
		 }
	 });
 }	
 accountType(index){
	var at = {1:"Individual",
			  5:"Company",
			  3:"Agent"
			 };
	 if(index)
		return Object.keys(at);
	 return at;
 }
 companyType(index){
	var at = {6:"Trade",
			  1:"Proprietary",
			  2:"Partnership Firms",
			  3:"Private Limited",
			  4:"Others",
			  5:"Public Sector",
			 };
	 if(index)
		return Object.keys(at);
	 return at;
 }
 businessType(index){
	var at = {2:"Manufacturing/Factory",
			  1:"Logistics/Transporter",
			  4:"Others",
			  5:"Traders/Distributer",
			 };
	 if(index)
		return Object.keys(at);
	 return at;
 }
 ValidateIsTrucker(role){
	var trucker =  {11:"Trucker Individual",12:"Fleet Owner"};
	if(role) return role in trucker; 
	return trucker; 
 }	
 ValidateIsShipper(role){
	var shipper = {21:"Shipper Individual",22:"Shipper Company"}; 
	if(role) return role in shipper;
	return shipper; 
 }
 ValidateIsAgent(role){
	var agent = {3:"Agent"};
	if(role in agent) return role in agent;
	return agent; 
 }	
 Roles(index){
	var at = {},sp = this.ValidateIsShipper(),tr =this.ValidateIsTrucker(),ag = this.ValidateIsAgent();
	for(var i in sp) at[i] =sp[i]; 
	for(var i in tr) at[i] =tr[i]; 
	for(var i in ag) at[i] =ag[i]; 
	 
	 if(index)
		return Object.keys(at);
	 return at;
 } 
loadUser(_id){
  this.Model.findOne({_id:_id},'contact mobile_tokens name')	
}	
}
// schema structure
function Schema(self){
  if(!self)return;
var re   = appHelper('RegularExp'),user;
  user   = new db.Schema({
		name:{
			firstname:{
				type :String,
				validate:[function(v){
						  return re.isTextWithSpace(2,15,v); 
					},
				 __('register').e_firstname],
				required:[true, __('register').e_firstname],
			},
			lastname:{
				type :String,
				validate:[function(v){
						return re.isText(2,15,v); 
					},
				__('register').e_lastname],
				required:[true,__('register').e_lastname]
			},
		},
		contact:{
			email:{
				type:String,
				validate:[function(v){
						return re.isEmail(v); 
					},
				__('register').e_email],
			    required:[true,__('register').e_email]
			},
			mobile_number:{
				type:Number,
				validate:[function(v){
						return re.isMobileno(v); 
					},
				__('register').e_mobile_number],
			    required:[true,__('register').e_mobile_number]
			},
		    alternate_number:Number,
		},
	    device_token:String,
	    device:{
			  token:String,
			  status:{
				  code:Number,
				  text:String,
			  },
		},
	    otp:{
			
	        otp_type:String,
	        otp_no:Number,
			otp_for:Number,
			validity:Date,
			verified:Number,
        },
		password:{
			type:String,
			required:[true,__('register').e_password]
		},
		hash_token:{
			type:String,
			min :32,
			max :32,
		},
		image:{
			type:String,
			//required:[true,__('register').e_image],
			imagetype:{
				type:String,
				enum:['jpg','png','bmp'],
				required:[true,__('register').e_imagetype],
			},
			imagesize:{
				type:Number,
				max :1.5,
				required:[true,__('register').e_imagesize],
			}
		},
		account_type:{
			type:Number,
			enum:self.accountType(1),
			//required:true,
		},
		company_type:{
			ctype:{
			      type:Number,
			      enum:self.companyType(1),
			      //required:[true,tt.company_type],	
			},
			text:String,
		},
		business_type:{
			btype:{
				type:Number,
			    enum:self.businessType(1),
				//required:[true,tt.business_type],
			},
			text:String,
			
		},
		business:{ 
			name:String,
			landline:String,
			registered_address:String,
			registered_city:String,
			registered_state:String,
			registered_pincode:String,
			office_address:String,
			office_city:String,
			office_state:String,
			office_pincode:String,			
			service_taxno:String,			
		},
	    account:{
			"ac_number":String,
			 "holder_name":String,
			 "bank_name": String,
			 "branch_address": String,
			 "branch_code": String,
			 "ifsc_code": String,
			
		},
		pancard:{type:String,max:12},
		status:{type:Number,enum:[0,1],required:true},
		plocation:String,
		role  :{type:Number,enum:self.Roles(1),//required:true
			   },
	    rating:Number,
		social:db.Schema.Types.Mixed,
		created_on:{type:Date},
		modified_on:{type:Date,default:Date.now()},
		isvalidated:{
			type:Number,
			enum:[0,1]
		},
	    membership: {
                   plan_type:{type:String,
							  enum:['Free','Silver','Gold'],
				   },
			       plan_code:{
					          type:Number,
					          enum:[0,1,2], //0==free,1==Silver,2=Gold
				   },
                   transactions:Number,
    			   starts_from :Date,
			       expiry_date :Date,
        },

	},{collection:"users"});
	user.virtual('confirm_password')
        .get(function() {
             return this._confirm_password;
        })
        .set(function(value) {
		    this._confirm_password =value;
    });
	user.virtual('old_password')
        .get(function() {
             return this._old_password;
        })
        .set(function(value) {
		    this._old_password =value;
    });
	user.virtual('new_password')
        .get(function() {
             return this._new_password;
        })
        .set(function(value) {
		    this._new_password =value;
    });
    
    user.path('password').validate(function(v){ //console.log(this);
		if(this.isNew){ console.log(!re.password(this.password));
			if(!re.password(this.password)) this.invalidate('password',__('register').password);
			if(this.password!=this.confirm_password)
			   this.invalidate('confirm_password',__('register').e_confirm_password);	
		    
		}
		if(this.old_password){
			if(!re.password(this.new_password))        
		   this.invalidate('new_password',__('users').e_new_password);
		if(!re.password(this.old_password)) 
		   this.invalidate('old_password',__('users').e_old_password);
		if(!re.password(this.confirm_password))
		   this.invalidate('confirm_password',__('users').e_confirm_password);
	    if(MD5(this.old_password)!=this.password)
		   this.invalidate('old_password',__('users').e_old_password);	
        if(this.new_password!=this.confirm_password)
		   this.invalidate('confirm_password',__('users').e_confirm_password);
		}
		if(self.ValidateIsShipper(this.role)){ 
		if(this.update==67 && (this.pancard=="" || !re.isString(10,10,this.pancard))){
			 this.invalidate('pancard',__('register').e_pancard_no);
			 }
			 if(this.role==self.SHIPPERCOMPANY && this.update==67 && (Validate.isEmpty(this.business.service_taxno) 
			 || !re.isText(15,15,this.business.service_taxno)))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);
		}
	},null);
	user.path('business_type.btype').validate(function(v){
		if(!this.isNew){
		   if(isNaN(this.business_type.btype))
			   this.invalidate('business_type.btype',__('register').e_business_type);
		}
	},null);

	user.path('business.name').validate(function(v){
		if(!this.isNew){ 
		   if(Validate.isEmpty(this.business.name))
			   this.invalidate('business.name',__('register').e_business_name);
		   if(!re.isNumber(7,11,this.business.landline))
			   this.invalidate('business.landline',__('register').e_landline);
		   if(!self.ValidateIsShipper(this.role)){ 
			   if((this.pancard=="" || !re.isString(10,10,this.pancard)))
			      this.invalidate('pancard',__('register').e_pancard_no);
			   /*if(Validate.isEmpty(this.business.service_taxno) 
			 || re.isText(15,15,this.business.service_taxno))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);*/
		   }else{ 
			 if(this.update==67 && (this.pancard=="" || !re.isString(10,10,this.pancard))){
			 this.invalidate('pancard',__('register').e_pancard_no);
			 }
			 /*if(this.role==self.SHIPPERCOMPANY && this.update==67 && (Validate.isEmpty(this.business.service_taxno) 
			 || !re.isText(15,15,this.business.service_taxno)))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);*/  		   
           } 
		if(self.ValidateIsShipper(this.role)){ 	
		   if(Validate.isNaN(this.company_type.ctype))
			   this.invalidate('company_type.ctype',__('register').e_company_type);
		   else if(this.company_type.ctype==4 && this.company_type.others=="")
			   this.invalidate('company_type.ctype',__('register').e_company_type_others);	
		   if(Validate.isNaN(this.business_type.btype))
			   this.invalidate('business_type.btype',__('register').e_business_type);	
		   else if(this.business_type.btype==4 && this.business_type.others=="")
			   this.invalidate('business_type.btype',__('register').e_business_type_others);
		  if(this.role==self.SHIPPERCOMPANY && this.update==67 && (Validate.isEmpty(this.business.service_taxno)
			 || !re.isText(15,15,this.business.service_taxno)))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);
		}
	   }
	},tt.business_name);
	user.path('business.registered_address').validate(function(v){ 
		if(!this.isNew){ 
		   if(Validate.isEmpty(this.business.registered_address))
			   this.invalidate('business.registered_address',__('register').e_registered_address);
		   if(Validate.isEmpty(this.business.registered_city))
			   this.invalidate('business.registered_city',__('register').e_registered_city);
		   if(Validate.isEmpty(this.business.registered_state))
			   this.invalidate('business.registered_state',__('register').e_registered_state);
		   if(Validate.isEmpty(this.business.registered_pincode))
			   this.invalidate('business.registered_pincode',__('register').e_registered_pincode);
		if(this.business.officeaddresssameasbusiness){ 	
		   if(Validate.isEmpty(this.business.office_address))
			   this.invalidate('business.office_address',__('register').e_office_address);
		   if(Validate.isEmpty(this.business.office_city))
			   this.invalidate('business.office_city',__('register').e_office_city); 
		   if(Validate.isEmpty(this.business.office_state))
			   this.invalidate('business.office_state',__('register').e_office_state); 
		   if(Validate.isEmpty(this.business.office_pincode))
			   this.invalidate('business.office_pincode',__('register').e_office_pincode); 	
		}
	  }
	},null);
	user.path('account.ac_number').validate(function(v){ 
		if(!this.isNew && this.update==67){
		   if(Validate.isEmpty(this.account.ac_number) || !re.isNumber(8,'',this.account.ac_number))
			   this.invalidate('account.ac_number',__('users').e_account_number);
		   if(Validate.isEmpty(this.account.bank_name))
			   this.invalidate('account.bank_name',__('users').e_bank_name);
		   if(Validate.isEmpty(this.account.holder_name))
			   this.invalidate('account.holder_name',__('users').e_holder_name);
		   if(Validate.isEmpty(this.account.bank_name))
			   this.invalidate('account.bank_name',__('users').bank_name);
		   if(Validate.isEmpty(this.account.branch_code) || !re.isString(6,'',this.account.branch_code))
			   this.invalidate('account.branch_code',__('users').e_branch_code);
		   if(Validate.isEmpty(this.account.branch_address))
			   this.invalidate('account.branch_address',__('users').e_branch_address); 				
		   if(Validate.isEmpty(this.account.ifsc_code) || !re.isString(11,11,this.account.ifsc_code))
			   this.invalidate('account.ifsc_code',__('users').e_ifsc_code);				
		
	  }
	},null);
  user.pre('save',function(next){
	   beforeSave.call(this,self,next);
  });
  registerIndex.call(null,user);	
  return user;
 };

/* before save function 
 * invalidate key is used to skip already exist
   validation check when we will use find save
   function
 */

function beforeSave(User,next){
 var self = this,
	 worker  = appHelper('Worker'),
	 errMsg = __().e_email_already_exists;
	//console.log(self);
 if(self.isvalidated){ 
	delete self.isvalidated;
	return next();
 }
 if(self.isNew){
	self.created_on = Date.now()  
	self.password   = MD5(self.password);
	self.hash_token = "";  
	if(self.social && self.social.provider=='webapp')
	   self.hash_token = MD5(self.contact.email+Date.now())+":"+Date.now();
 }else self.modified_on  = Date.now();

if(self.social && self.social.provider!='webapp') return next();
 User.Model.findOne({'contact.email':self.contact.email},'contact.email',function(err,u){
	if(err)return new Logger(err);
	else if(u){
		self.invalidate('contact.email',errMsg);
		next(new Error(errMsg));
	}else{ 
	   if(self.isNew){			
	      worker.execPHP('Mailer.php '+self.contact.email+" confirmemail "+self.hash_token +" "+ self.name.firstname,function(err,res){
		         console.log(err);	 
		         //console.log(res);	 
		         
          });
		  next(); 
       }else next();
    }	
})

}
function registerIndex(schema){ 
  schema.index({hash_token:1});
  schema.index({'contact.mobile_no':1,password:1});
  schema.index({'contact.email':1,password:1});
  schema.index({'contact.email':1});
}
module.exports = Users;
