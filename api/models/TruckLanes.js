"use strict";
var db   = require('mongoose');
let schema = Symbol('_schema'); 
class TruckLanes{
	constructor(){
		Schema.bind(this);		
		if(this[schema] === undefined)
		   this[schema] = Schema(this);
	}
get Model(){
   return  db.models.TruckLanes || db.model('TruckLanes',this[schema],'truck_lanes');		 
}		
 getOne(cond,callback){
	return this.Model.findOne(cond,callback); 
 }
 getAll(cond,callback){
	var cond = cond || {}; 
	return this.Model.find(cond,callback); 	 
 }	
}
function Schema(){ 
  return new db.Schema({	 
  },{'collection':'truck_lanes'});
  					   
}

module.exports =  TruckLanes;