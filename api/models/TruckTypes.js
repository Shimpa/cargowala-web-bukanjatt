"use strict";
var db   = require('mongoose');	
class TruckTypes{
	constructor(){
	}
 get Model(){
	return db.models.TruckTypes || db.model('TruckTypes',Schema(this),'trucks');  
 }	
 getOne(cond,callback){
	return this.Model.findOne(cond,callback); 
 }
 getAll(cond,callback){
	var cond = cond || {}; 
	return this.Model.find(cond,callback); 	 
 }
 getChildrenByID(parent_id,callback){ console.log("ddfasas---" +  parent_id);
	if(!parent_id) callback.call(null,null); 
	this.Model.find({truck_type:parent_id,'status':1},'name loading_capacity',function(err,tt){ console.log(tt);
		if(!err && tt)
		   callback.call(null,tt);
		else callback.call(null,null);
	});
 } 	
}
function Schema(){
  var schema = new db.Schema({
	name:{
		type:String,
		required:true
	},
	loading_capacity:Number,  
	status:{
		type:Number,
		enum:[0,1]
	}  
  },{'collection':'trucks'});	
  return schema	
}
module.exports =  TruckTypes;