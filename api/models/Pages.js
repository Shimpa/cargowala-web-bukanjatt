"use strict";
var db    = require('mongoose');
let _model= Symbol('model');
class Pages{
	constructor(){

	}
	
 get Model(){
	if(this[_model]===undefined)
	   this[_model] = db.model('CmsPages',{},'cms_pages');  
	return this[_model]; 
 }	

	
 getOne(cond,callback,fields){
	var model = this.Model.findOne(cond);
	fields && model.select(fields);
	model.lean().exec(callback); 
 }
 getAll(cond,callback,fields){
	var model = this.Model.find(cond);
	fields && model.select(fields);
	model.lean().exec(callback); 
 }	
}


module.exports =  Pages;