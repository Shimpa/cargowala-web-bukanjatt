"use strict";
var db   = require('mongoose');	
class Memberships{
	constructor(){
	}
 get ACTIVE(){ return 1;}	
 get Model(){
	return db.models.Memberships || db.model('Memberships',Schema(this),'membership');  
 }	
 	
}
function Schema(){
  var schema = new db.Schema({
	name:{
		type:String,
		required:true
	},
	price:String,
	duration:Number,
	transactions:Number,
	description:[],
	mtype:{
		code:Number,
		text:String,
	},
	created_on:Date,
	modified_on :{type:Date,default:DateTime.now()},  
	status:{
		type:Number,
		enum:[0,1]
	}  
  },{'collection':'membership'});	
  return schema	
}
module.exports =  Memberships;