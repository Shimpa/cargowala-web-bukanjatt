"use strict";
var db   = require('mongoose');	
class Queue{
	constructor(){
	}
get ACTIVE(){return 1;}
get INACTIVE(){return 0;}
	
 get Model(){
	return db.models.Queue || db.model('Queue',Schema(this),'queue');  
 }
 Insert(dta){
	return new this.Model(dta)
 }
// add row to queue
//@params:member is mobile_no or email,message (text) message what you want to send on mobile or email	
 Add2Queue(member,message,action,subject){
	if(Array.isArray(member)){
	   for(var i in member){
		 if(action=='mobile'){
	   this.Insert({member:member[i],message:message,status:0,action:action,priority:subject||0}).save(); 
		   }else{
		this.Insert({member:member[i],message:message,status:0,action:action,subject:subject||null}).save();  
		  }
		   
	   }	
	}else 
	  if(action=='mobile'){			   this.Insert({member:member,message:message,status:0,action:action,priority:subject||0}).save(); 
	}else{
	this.Insert({member:member,message:message,status:0,action:action,subject:subject||null}).save();  
   } 
 }
 Add2QueuePushNotification(member,message,object){
	if(Array.isArray(member)){
	   for(var i in member){	   this.Insert({member:member[i],message:message,status:0,action:'push',related_id:object}).save();  
		   
	   }	
	}else{ 
	this.Insert({member:member,message:message,status:0,action:'push',related_id:object}).save();  
	}
}	
 getNext10Rows(action,callback){
	this.Model.find({status:0,action:action},'member message').skip(0).limit(10).exec(callback); 
 }	

	
}
function Schema(self){
  var re   = appHelper('RegularExp'),queue;
    queue  = new db.Schema({
	  member:String,
	  message:String,
	  subject:String, // subject could be heading in push notifications
	  image:String,
	  related_id:db.Schema.Types.Mixed, // shipment id trucvker id shiper id or any other id...	
	  action:{
		  type:String,
		  enum:['mobile','email','push']
	  },	
	  status:{
		   type:Number,
		   enum:[0,1] //0 = initiated,1=completed 
	  },
	  priority:{
		   type:Number,
		   enum:[0,1], //0 = normal,1=high 
		   default:0,
	  }	
  },{'collection':'queue'});	
  return queue;	
}
module.exports =  Queue;