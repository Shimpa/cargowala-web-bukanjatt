"use strict";
var db     = require('mongoose');
let schema = Symbol("schema");
let model  = Symbol("model");
let _usr   = Symbol('_usr');
class Moderators{

get ACTIVE(){ return 1;}
get INACTIVE(){ return 0;}
get BLOCKED(){ return 2;}
get INDIVIDUALSHIPPER(){ return 21;}
get INDIVIDUALTRUCKER(){ return 11;}
get SHIPPERCOMPANY(){ return 22;}
get TRUCKERCOMPANY(){ return 12;}
get parent_dir(){return "users";}
get child_dir(){return "users";}	

constructor(){ //console.log(process);
	let _self          = this;
	this.isActive      = 0;
    this.validPassword = false;
    this._oldPassword  = null;
	this.userType      = 0;
	this.isTrucker     = 0;
	this.isShipper     = 0;
	_self =null;	
}
Add(u){ 
	return new this.Model(u);
}
get Model(){ //console.log(Schema(this))
        return db.models.Users || db.model('Moderators',Schema(this),'moderators'); 
 
 };
 	
}
// schema structure
function Schema(self){
  if(!self)return;
var re   = appHelper('RegularExp'),user;
  user   = new db.Schema({
		name:{
			firstname:{
				type :String,
				validate:[function(v){
						  return re.isText(2,15,v); 
					},
				 __('register').e_firstname],
				required:[true, __('register').e_firstname],
			},
			lastname:{
				type :String,
				validate:[function(v){
						return re.isText(2,15,v); 
					},
				__('register').e_lastname],
				required:[true,__('register').e_lastname]
			},
		},
		contact:{
			email:{
				type:String,
				validate:[function(v){
						return re.isEmail(v); 
					},
				__('register').e_email],
			    required:[true,__('register').e_email]
			},
			mobile_number:{
				type:Number,
				validate:[function(v){
						return re.isMobileno(v); 
					},
				__('register').e_mobile_number],
			    required:[true,__('register').e_mobile_number]
			}
		},
	    otp:{
	        otp_type:String,
	        otp_no:Number,
			validity:Date,
        },
		password:{
			type:String,
			required:[true,__('register').e_password]
		},
		hash_token:{
			type:String,
			min :32,
			max :32,
		},
		image:{
			type:String,
			required:[true,__('register').e_image],
			imagetype:{
				type:String,
				enum:['jpg','png','bmp'],
				required:[true,__('register').e_imagetype],
			},
			imagesize:{
				type:Number,
				max :1.5,
				required:[true,__('register').e_imagesize],
			}
		},		
		status:{type:Number,enum:[0,1],required:true},
		
		role  :{type:Number,enum:self.Roles(1),//required:true
			   },
	    rating:Number,
		created_on:{type:Date},
		modified_on:{type:Date,default:Date.now()},
		isvalidated:{
			type:Number,
			enum:[0,1]
		},
	    update:{
			type:Number,
			enum:[67]
		},
	},{collection:"users"});
	user.virtual('confirm_password')
        .get(function() {
             return this._confirm_password;
        })
        .set(function(value) {
		    this._confirm_password =value;
    });
    user.path('password').validate(function(v){ 
		if(this.isNew){
			if(!re.password(this.password)) this.invalidate('password',tt.password);
			if(this.password!=this.confirm_password)
			   this.invalidate('confirm_password',__('register').e_confirm_password);	
		    
		}
	},null);
	user.path('business_type.btype').validate(function(v){
		if(!this.isNew){
		   if(isNaN(this.business_type.btype))
			   this.invalidate('business_type.btype',__('register').e_business_type);
		}
	},null);
	user.path('business.name').validate(function(v){
		if(!this.isNew){ 		    
		   if(Validate.isEmpty(this.business.name))
			   this.invalidate('business.name',__('register').e_business_name);
		   if(!re.isNumber(7,11,this.business.landline))
			   this.invalidate('business.landline',__('register').e_landline);
		   if(!self.ValidateIsShipper(this.role)){ 
			   if((this.pancard=="" || !re.isString(10,10,this.pancard)))
			      this.invalidate('pancard',__('register').e_pancard_no);
			   /*if(Validate.isEmpty(this.business.service_taxno) 
			 || re.isText(15,15,this.business.service_taxno))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);*/ 
		   }else if(this.update==67 && (this.pancard=="" || !re.isString(12,12,this.pancard))){
			 this.invalidate('pancard',__('register').e_pancard_no);
                   } 
		if(self.ValidateIsShipper(this.role)){ 	
		   if(Validate.isNaN(this.company_type.ctype))
			   this.invalidate('company_type.ctype',__('register').e_company_type);
		   else if(this.company_type.ctype==4 && this.company_type.others=="")
			   this.invalidate('company_type.ctype',__('register').e_company_type_others);	
		   if(Validate.isNaN(this.business_type.btype))
			   this.invalidate('business_type.btype',__('register').e_business_type);	
		   else if(this.business_type.btype==4 && this.business_type.others=="")
			   this.invalidate('business_type.btype',__('register').e_business_type_others);
		  if(this.update==67 && Validate.isEmpty(this.business.service_taxno) 
			 && re.isText(15,15,this.business.service_taxno))
			   this.invalidate('business.service_taxno',__('register').e_service_taxno);
		}
	   }
	},tt.business_name);
	user.path('business.registered_address').validate(function(v){ 
		if(!this.isNew){ 
		   if(Validate.isEmpty(this.business.registered_address))
			   this.invalidate('business.registered_address',__('register').e_registered_address);
		   if(Validate.isEmpty(this.business.registered_city))
			   this.invalidate('business.registered_city',__('register').e_registered_city);
		   if(Validate.isEmpty(this.business.registered_state))
			   this.invalidate('business.registered_state',__('register').e_registered_state);
		   if(Validate.isEmpty(this.business.registered_pincode))
			   this.invalidate('business.registered_pincode',__('register').e_registered_pincode);
		if(this.business.officeaddresssameasbusiness){ 	
		   if(Validate.isEmpty(this.business.office_address))
			   this.invalidate('business.office_address',__('register').e_office_address);
		   if(Validate.isEmpty(this.business.office_city))
			   this.invalidate('business.office_city',__('register').e_office_city); 
		   if(Validate.isEmpty(this.business.office_state))
			   this.invalidate('business.office_state',__('register').e_office_state); 
		   if(Validate.isEmpty(this.business.office_pincode))
			   this.invalidate('business.office_pincode',__('register').e_office_pincode); 	
		}
	  }
	},null);
	user.path('account.ac_number').validate(function(v){ 
		if(!this.isNew){
		   if(Validate.isEmpty(this.account.ac_number) || !re.isNumber(8,'',this.account.ac_number))
			   this.invalidate('account.ac_number',__('users').e_account_number);
		   if(Validate.isEmpty(this.account.bank_name))
			   this.invalidate('account.bank_name',__('users').e_bank_name);
		   if(Validate.isEmpty(this.account.holder_name))
			   this.invalidate('account.holder_name',__('users').e_holder_name);
		   if(Validate.isEmpty(this.account.bank_name))
			   this.invalidate('account.bank_name',__('users').bank_name);
		   if(Validate.isEmpty(this.account.branch_code) || !re.isString(6,'',this.account.branch_code))
			   this.invalidate('account.branch_code',__('users').e_branch_code);
		   if(Validate.isEmpty(this.account.branch_address))
			   this.invalidate('account.branch_address',__('users').e_branch_address); 				
		   if(Validate.isEmpty(this.account.ifcs_code) || !re.isString(11,11,this.account.ifcs_code))
			   this.invalidate('account.ifcs_code',__('users').e_ifcs_code);				
		
	  }
	},null);
  user.pre('save',function(next){
	   beforeSave.call(this,self,next);
  });
  registerIndex.call(null,user);	
  return user;
 };

/* before save function 
 * invalidate key is used to skip already exist
   validation check when we will use find save
   function
 */

function beforeSave(User,next){
 var self = this,
	 worker  = appHelper('Worker'),
	 errMsg = __().e_email_already_exists;
	//console.log(self);
 if(self.isvalidated){ 
	delete self.isvalidated;
	return next();
 }
 if(self.isNew){
	self.created_on = Date.now()  
	self.password   = MD5(self.password);
	self.hash_token = "";  
	if(self.social && self.social.provider=='webapp')
	   self.hash_token = MD5(self.contact.email+Date.now())+":"+Date.now();
 }else self.modified_on  = Date.now();

if(self.social && self.social.provider!='webapp') return next();
 User.Model.findOne({'contact.email':self.contact.email},'contact.email',function(err,u){
	if(err)return new Logger(err);
	else if(u){
		self.invalidate('contact.email',errMsg);
		next(new Error(errMsg));
	}else{ 
	   if(self.isNew){			
	      worker.execPHP('Mailer.php '+self.contact.email+" confirmemail "+self.hash_token +" "+ self.name.firstname,function(err,res){
		         console.log(err);	 
		         //console.log(res);	 
		         
          });
		  next(); 
       }else next();
    }	
})

}
function registerIndex(schema){ 
  schema.index({hash_token:1});
  schema.index({'contact.mobile_no':1,password:1});
  schema.index({'contact.email':1,password:1});
  schema.index({'contact.email':1});
}
module.exports = Users;
