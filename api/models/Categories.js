"use strict";
var db   = require('mongoose');
class Categories{
	constructor(){
	}
get Model(){
   return  db.models.Categories || db.model('Categories',Schema(),'categories');		 
}		
 getOne(cond,callback){
	return this.Model.findOne(cond,callback); 
 }
 getAll(cond,callback){
	var cond = cond || {}; 
	return this.Model.find(cond,callback); 	 
 }	
}
function Schema(){ 
  return new db.Schema({
	name:String,  
  },{'collection':'categories'});  					   
}

module.exports =  Categories;