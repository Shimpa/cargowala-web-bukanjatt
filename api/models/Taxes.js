"use strict";
var db   = require('mongoose');	
class Taxes{
	constructor(){
	}
 get Model(){
	return db.models.Taxes || db.model('Taxes',Schema(this),'tax_terms');  
 }	
 getOne(cond,callback){
	return this.Model.findOne(cond,callback); 
 }
 getAll(cond,callback){
	var cond = cond || {}; 
	return this.Model.find(cond,callback); 	 
 }
 getChildrenByID(parent_id,callback){
	if(!parent_id) callback.call(null,null); 
	this.Model.find({truck_type:parent_id,'status':1},'name loading_capacity',function(err,tt){ console.log(tt);
		if(!err && tt)
		   callback.call(null,tt);
		else callback.call(null,null);
	});
 } 	
}
function Schema(){
  var schema = new db.Schema({
	name:{
		type:String,
		required:true
	},
	rate:Number,  
	status:{
		type:Number,
		enum:[0,1]
	}  
  },{'collection':'trucks'});	
  return schema	
}
module.exports =  Taxes;