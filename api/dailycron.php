<?php
class DBConnection{
	protected $collection;
	protected static $cronActive=0;
	public function __construct(){
		$this->checkDB();	
    }
	private function checkDB(){ 
	    $m = new MongoClient('mongodb://127.0.0.1:27017/cargowla-dev'); // connect
        $db = $m->selectDB("cargowla-dev");	
	    $this->collection = new MongoCollection($db, 'bookings');
	}
	
	protected function getExpiredShipments(){  //print_r(time());die;
		//0 = pending 6= accepted 8= bid accepted by trucker 
		return $this->collection->find(['status'=>['$in'=>[0,6,8]],'transit.t_expdatetime'=>['$lt'=>time()*1000]])->sort(['transit.t_expdatetime'=>1])->limit(100);
	}
}

class DailyCron extends DBConnection{
	public function __construct(){
		parent::__construct();
		$this->isShipmentExpired();
	}
	private function expireShipment($shipment_id){ //3 = expired
		$this->collection->update(['_id'=>$shipment_id],['$set'=>['status'=>3]]);
	}
	protected function isShipmentExpired(){
		set_time_limit(0);
        ignore_user_abort(true);
		$results = $this->getExpiredShipments();
		if(empty($results->hasNext()))exit;
		while($results->hasNext()){ $shipment = $results->getNext();
			$vehicle_asigned = false;					   
			if(!empty($shipment['vehicle'])){					   
			   foreach($shipment['vehicle'] as $vehicle){
				   if(!empty($vehicle['driver']) || !empty($vehicle['id'])){
					   $vehicle_assigned = true;
				   }
			   }				
			}
			if(!$vehicle_asigned){ //print_r($shipment['_id']);die; 
				$this->expireShipment($shipment['_id']);
			}					   
			
		}$this->isShipmentExpired();
	}
		
}
 new DailyCron();

?>