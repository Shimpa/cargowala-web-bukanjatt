<?php  
class MailerCron{
	protected $collection;
    private $isSmtp = false;
	private static $count=0;
	protected static $smscount=0;
	protected static $hsmspcount=0; // high prioprity sms count //
	public function __construct(){
		$this->checkDB();	
    }
	private function checkDB(){
	    $m = new MongoClient('mongodb://127.0.0.1:27017/cargowla-dev'); // connect
        $db = $m->selectDB("cargowla-dev");	
	    $this->collection = new MongoCollection($db, 'queue');
	}
	
	protected function changeStatus($mail){
		$this->collection->remove(['member'=>$mail]);
		echo time();
	}
}

class SMSCron extends MailerCron{
	public function __construct(){
		parent::__construct();
		$this->highPriority();
		$this->sendSMS();
		
	}
	private function getNextSMS(){
		return $this->collection->find(['action'=>'mobile','status'=>0,'priority'=>0])->limit(100);
	}
	private function sendSMS(){
		set_time_limit(0);
        ignore_user_abort(true);
		$results = $this->getNextSMS();
		if(empty($results->hasNext())){
		   $hpsms =$this->collection->find(['action'=>'mobile','status'=>0,'priority'=>1])->limit(2);	
			if(empty($hpsms->hasNext()))exit; 
			   return;
		}
		if(self::$smscount>=600){ self::$smscount=0; sleep(2);}
		while($results->hasNext()){ $sms = $results->getNext();		   
            if($sms['member']){
				$message = $sms['message'];
				$url ="http://121.242.224.32:8080/smsapi/httpapi.jsp";
				$url.="?username=trigmaht&password=trigma&";
			    $message = str_replace(array("<br />","<br/>","<br>"), "\n", $sms['message']); 
				$message = wordwrap( $message, 70 );
				$url.="from=CARGOW&to=".$sms['member']."&text=".urlencode(utf8_encode($message))."&coding=0";
 			    
				//$this->sendSMS2(urlencode($url));
				//echo $url;
				file_get_contents($url);
				++self::$smscount;       
                
				$this->changeStatus($sms['member']);
			}								    
		}
		$this->sendSMS();
	}

	private function highPriority(){
		set_time_limit(0);
        ignore_user_abort(true);
		$records =$this->collection->find(['action'=>'mobile','status'=>0,'priority'=>1])->limit(100);
		if(empty($records->hasNext())){
		   $normalsms =$this->collection->find(['action'=>'mobile','status'=>0,'priority'=>0])->limit(2);	
			if(empty($normalsms->hasNext()))exit; 
			   return;
		}
		if(self::$hsmspcount>=600){ self::$hsmspcount=0; sleep(2);}
		while($records->hasNext()){ $sms = $records->getNext();		   
            if($sms['member']){
				$message = $sms['message'];
				$url ="http://121.242.224.32:8080/smsapi/httpapi.jsp";
				$url.="?username=trigmaht&password=trigma&";
			    $message = str_replace(array("<br />","<br/>","<br>"), "\n", $sms['message']); 
				$message = wordwrap( $message, 70 );
				$url.="from=CARGOW&to=".$sms['member']."&text=".urlencode(utf8_encode($message))."&coding=0";
 			    
				//$this->sendSMS2(urlencode($url));
				//echo $url;
				file_get_contents($url);
				++self::$hsmspcount;               
				$this->changeStatus($sms['member']);
			}								    
		}
		$this->highPriority();
		
	}	
}
 new SMSCron();

?>