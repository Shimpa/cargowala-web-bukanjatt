<?php  
class MailerCron{
	private $records =[];
	protected $collection;
    private $isSmtp = false;
	private static $count=0;
	private static $hpcount=0;  //high priority mail count
	public function __construct(){
		$this->checkDB();
		//$this->highPriority();
		$this->sendMail();
    }
	private function checkDB(){
	    $m = new MongoClient('mongodb://127.0.0.1:27017/cargowla-dev'); // connect
        $db = $m->selectDB("cargowla-dev");	
	    $this->collection = new MongoCollection($db, 'queue');
	}
	private function getNext10(){
		return $this->collection->find(['action'=>'email','status'=>0,'priority'=>0])->limit(100);
	}
	private function mailHeaders(){
    $from    = "no-reply@cargowala.com";
    $headers  = "From: " . strip_tags($from) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
    $headers .= "Organization: Sender Cargowala.com\r\n"; 
    $headers .= "Return-Path: {$from} \r\n";
    $headers .= "X-Priority: 3\r\n";
    $headers .= "X-Mailer: PHP". phpversion() ."\r\n";
    $headers .= "Content-Transfer-Encoding: binary";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    return $headers;
}	
	
	private function sendMail(){		
		set_time_limit(0);
        ignore_user_abort(true);
		$results = $this->getNext10();		
		if(empty($results->hasNext())){
			$hpmail = $this->collection->find(['action'=>'email','status'=>0,'priority'=>1])->limit(2);
		   if(empty($hpmail->hasNext()))exit;
		      return; 	
		}
		if(self::$count>=450){ self::$count=0; sleep(2);}
		while($results->hasNext()){ $mail = $results->getNext();
			 if(isset($mail['member'])){
				 $body = '<p>'.$mail['message'].'</p>';
				 $subject = empty($mail['subject'])?'Cargowala Email':$mail['subject'];
				 mail($mail['member'],$subject,$body,$this->mailHeaders());
                 ++self::$count;       
                 
				 $this->changeStatus($mail['member']); 
			  }
		}
		$this->sendMail();
	}
	private function highPriority(){
		$records =$this->collection->find(['action'=>'email','status'=>0,'priority'=>1])->limit(100);
		if(empty($records->hasNext())){
			$normalrec = $this->collection->find(['action'=>'email','status'=>0,'priority'=>0])->limit(2);
		   if(empty($normalrec->hasNext()))exit;
		      return; 	
		}
		if(self::$hpcount>=450){ self::$hpcount=0; sleep(2);}
		while($records->hasNext()){ $mail = $records->getNext();
			 if(isset($mail['member'])){
				 $body = '<p>'.$mail['message'].'</p>';
				 $subject = empty($mail['subject'])?'Cargowala Email':$mail['subject'];
				 mail($mail['member'],$subject,$body,$this->mailHeaders());
                 ++self::$hpcount;                 
				 $this->changeStatus($mail['member']); 
			  }
		}
		$this->highPriority();	
	}
	private function endCron(){
		
	}
	protected function changeStatus($mail){
		$this->collection->remove(['member'=>$mail]);
		echo time();
	}
}
new MailerCron();

?>