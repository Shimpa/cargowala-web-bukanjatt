<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [  

          [
           'label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words("Manage " . StringHelper::basename($generator->modelClass)))) ?>,
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],         
];
?>
 <section class="content <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-create">

        
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
