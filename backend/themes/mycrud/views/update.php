<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Update {modelClass}: ', ['modelClass' => Inflector::camel2words(StringHelper::basename($generator->modelClass))]) ?> . ' ' . $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model-><?= $generator->getNameAttribute() ?>, 'url' => ['view', <?= $urlParams ?>]];
$this->params['breadcrumbs'][] = <?= $generator->generateString('Update') ?>;
$this->menu = [

          [
           'label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words("Create " . StringHelper::basename($generator->modelClass)))) ?>,
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words("Manage " . StringHelper::basename($generator->modelClass)))) ?>,
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words("View " . StringHelper::basename($generator->modelClass)))) ?>,
           'url'   => ['view',<?= $urlParams ?>],
           'wrap'=>true,
           'icon'=>'fa-eye',
            
          ],
          [
           'label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words("Delete " . StringHelper::basename($generator->modelClass)))) ?>,
           'url'   => ['delete',<?= $urlParams ?>],
           'itemOptions'=>['data-confirm'=>"<?= $generator->generateString('Are you sure you want to delete this item?') ?>",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-update">

          
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= "<?= " ?>Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= "<?= " ?>$this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
