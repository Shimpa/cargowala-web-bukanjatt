<?php

namespace backend\themes\admin;

use yii\web\AssetBundle;

class GCAdminAsset extends AssetBundle
{
    public $sourcePath = '@backend/themes/admin/assets';
    public $baseUrl    = '@backend';
        public $publishOptions = [
    'forceCopy' => true,
];                     
    public $css        = [        
        'bootstrap/css/bootstrap.min.css',
        'dist/css/AdminLTE.min.css',
        'plugins/iCheck/square/blue.css',
        'dist/css/skins/_all-skins.min.css',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/daterangepicker/daterangepicker-bs3.css',
        'plugins/iCheck/all.css',
        'plugins/colorpicker/bootstrap-colorpicker.min.css',
        'plugins/timepicker/bootstrap-timepicker.min.css',
        'plugins/select2/select2.min.css',
        'dist/css/lightbox.min.css',
        'dist/css/autocomplete.css',
        'developer.css',
    ];
    public $js         = [
        //'plugins/jQuery/jQuery-2.1.4.min.js',
        //'bootstrap/js/bootstrap.min.js',
        'plugins/jQueryUI/jquery-ui.js',
        'dist/js/lightbox.min.js',
        'plugins/iCheck/icheck.min.js',
        'plugins/fastclick/fastclick.min.js',
        'dist/js/app.min.js',
        'plugins/sparkline/jquery.sparkline.min.js',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
        'plugins/chartjs/Chart.min.js',
        'plugins/datatables/jquery.dataTables.min.js',
        'plugins/datatables/dataTables.bootstrap.min.js',
        'plugins/select2/select2.full.min.js',
        'plugins/input-mask/jquery.inputmask.js',
        'plugins/input-mask/jquery.inputmask.date.extensions.js',
        'plugins/input-mask/jquery.inputmask.extensions.js',
        'plugins/daterangepicker/daterangepicker.js',
        'plugins/colorpicker/bootstrap-colorpicker.min.js',
        'javascripts/MiniProg.js',
        'javascripts/developers.js',
        //'dist/js/pages/dashboard2.js',
        'dist/js/demo.js',
    ];
    public $depends    = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init()
    {
        parent::init();
    }
}