<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Common;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Shipper */

$this->title = 'CargoWala | View Shipper | '.$model->firstname.' '.$model->lastname;
$title = $model->firstname.' '.$model->lastname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Shippers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Shippers'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
          ],
          [
           'label' => Yii::t('app', 'Update  Shippers'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
          ],
          [
           'label' => Yii::t('app', 'Manage  Shippers'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
          ],
          /*[
           'label' => Yii::t('app', 'Delete  Shippers'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
          ],*/
];
?>
<section class="content shipper-view">
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($title) ?></h3>
                <div class="box-tools pull-right">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                    <?php /*echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                    ],
                    ])*/ ?>
                </p>
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'firstname',
            'lastname',
            'email',
            'mobile_number',
            ['attribute' => 'image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            ['attribute' => 'account_type', 'value' => Common::getShipperAccountTypes($model->account_type)],
            ['attribute' => 'company_type', 'value' => Common::getCompaniesList($model->c_type)],
            ['attribute' => 'business_type', 'value' => Common::getBusinessList($model->b_type)],
            'business_name',
            'landline',
            'registered_address',
            'registered_city',
            'registered_state',
            'registered_pincode',
            'office_address',
            'office_city',
            'office_state',
            'office_pincode',
            'pancard',
            'has_cp',
            'status',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>