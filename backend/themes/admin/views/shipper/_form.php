<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
use common\models\Company;
use common\models\Business;
/* @var $this yii\web\View */
/* @var $model common\models\Shipper */
/* @var $form yii\widgets\ActiveForm */
/*echo '<pre>';
print_r($model);
echo '</pre>';*/
?>

<div class="box-body shipper-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'firstname') ?>
        </div>         
        
        <div class="col-md-6">
            <?= $form->field($model, 'lastname') ?>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['readonly'=> 'true']) ?>
        </div>

       <!-- <div class="col-md-6">
            <?php //echo $form->field($model, 'password')->passwordInput(); ?>
        </div>-->
    </div>
	
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'mobile_number') ?>
        </div>
        
        <div class="col-md-6">
		<?php if($model->role == 22){
			echo $form->field($model, 'role')->dropDownList(Common::getShipperAccountTypes(),['class'=>'form-control', 'prompt'=>'Select Account Type', 'disabled' => 'true']); 
		}else{ 
			echo $form->field($model, 'role')->dropDownList(Common::getShipperAccountTypes(),['class'=>'form-control', 'prompt'=>'Select Account Type']); 
		}?>
        </div>
    </div>
	
    <div class="row">        
        <div class="col-md-6">
            <?php echo $form->field($model, 'company_type')->dropDownList(Common::getCompaniesList(),['class'=>'form-control', 'prompt'=>'Select Company Type']); ?>
			<!--<input id='other_company' value='sample value'/>-->
        </div>
        
        <div class="col-md-6">
            <?php echo $form->field($model, 'business_type')->dropDownList(Common::getBusinessList(),['class'=>'form-control', 'prompt'=>'Select Business Type']); ?>          
        </div>        
    </div>
    
    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
		$img = Common::getMediaPath($model, $model["image"]);
		$file_info = pathinfo($img);
		if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?></div>

		<div class="col-md-6">
            <?= $form->field($model, 'pancard'); ?>
        </div>
    </div>    
    
    <br/>
    <label>Business Information</label> <hr/>
    <div class='row'>
        <div class="col-md-6">
            <?php echo $form->field($model, 'business_name'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'landline'); ?>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6">
            <?php echo $form->field($model, 'registered_address'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'office_address'); ?>
        </div>
	</div>

    <div class="row">    
        <div class="col-md-6">
            <?php echo $form->field($model, 'registered_city'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'office_city'); ?>
        </div>
    </div>
	
    <div class="row">    
        <div class="col-md-6">
            <?php echo $form->field($model, 'registered_state'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'office_state'); ?>
        </div>
    </div>
	
    <div class="row">    
        <div class="col-md-6">
            <?php echo $form->field($model, 'registered_pincode'); ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'office_pincode'); ?>
        </div>
    </div>
	<input type="checkbox" id='sameaddress' > Same as Registered Address<br/>
	<hr/>
    
    <div class='row'>
        <div class="col-md-6">
            <?php echo $form->field($model, 'has_cp')->dropDownList(['1' => 'Yes', '0' => 'No'],['prompt'=>'Choose Option']); ?>
        </div>
        
        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>
    </div>

    <div class="col-md-6">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
	
    $('#sameaddress').on('click', function(e, state) {
		if($(this).is(':checked')){
			$('#users-office_address').val($('#users-registered_address').val());
			$('#users-office_city').val($('#users-registered_city').val());
			$('#users-office_state').val($('#users-registered_state').val());
			$('#users-office_pincode').val($('#users-registered_pincode').val());
		}else{
			$('#users-office_address, #users-office_city, #users-office_state, #users-office_pincode').val('');
		}
    })
    
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>