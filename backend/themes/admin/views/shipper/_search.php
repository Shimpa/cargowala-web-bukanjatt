<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShipperSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipper-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'contact') ?>

    <?= $form->field($model, 'otp') ?>

    <?= $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'mobile_tokens') ?>

    <?php // echo $form->field($model, 'hash_token') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'account_type') ?>

    <?php // echo $form->field($model, 'company_type') ?>

    <?php // echo $form->field($model, 'business_type') ?>

    <?php // echo $form->field($model, 'business') ?>

    <?php // echo $form->field($model, 'pancard') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'social') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'modified_on') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
