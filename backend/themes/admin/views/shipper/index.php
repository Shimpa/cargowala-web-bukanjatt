<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ShipperSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CargoWala | Manage Shippers');
$title = Yii::t('app', 'Manage Shippers');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
	[
		'label' => Yii::t('app', 'Create  Shippers'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box shipper-index">
	<div class="box-header mtb10">
		<h3 class="box-title"><?= Html::encode($title) ?> List</h3>
		<div class="box-tools pull-right">
			<p><?php //echo Html::a(Yii::t('app', 'Create Shipper'), ['create'], ['class' => 'btn btn-success']) ?></p>
		</div>    
	</div><!-- /.box-header -->
<?php //echo '<pre>'; print_r($searchModel); echo '</pre>'; die; ?>
   
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array(
                'attribute' => 'image',
                'format' => ['image', ['width'=>'80', 'height'=>'60']],
            ),
            'firstname',
            'lastname',
            'email',
            'mobile_number',
            /* ['attribute'=>'compliance', 'format'=>'html', 'value'=>function($data){
                return '<div> Compliance <title><i class="fa fa-files-o"></i>'.Common::getCompliance($data->compliance).'</title></div>';
            }], */
            ['attribute'=>'role', 'filter' => [21 => 'Individual', 22 => 'Company'], 'format'=>'html', 'value'=>function($data){
                return Common::getShipperAccountTypes($data->role);
            }],
            ['attribute'=> 'has_cp', 'filter' => ['0'=> 'No', '1'=> 'Yes'], 'format' => 'raw', 'value' => function($data){
              return '<input class="changeCP" type="checkbox" data-size="mini" name="cp-checkbox" value='.$data->_id->__toString().' '.$a =($data->has_cp == 'Yes') ? "checked='checked'" : "" .' >';
            }],
            ['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
               return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
            }],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({ 
            type: "POST",
            url: '?r=shipper/changestatus',
            data: {id:id},
            success: function(msg){}
        });
    })
    
    $(".changeCP").bootstrapSwitch({
        'onText' : 'Yes',
        'offText' : 'No',
    });
    
    $('input[name="cp-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({ 
            type: "POST",
            url: '?r=shipper/changecp',
            data: {id:id},
            success: function(msg){}
        });
    })
	$(".glyphicon-trash").hide(); // hides the delete button from Action listing
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>