<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CargoWala | Manage Categories');
$title = Yii::t('app', 'Manage Categories');
$this->params['breadcrumbs'][] = $title;
$this->menu = [

          [
           'label' => Yii::t('app', 'Create  Categories'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],       


];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box categories-index">
        <div class="box-header mtb10">
         
                  <h3 class="box-title"><?= Html::encode($title) ?> List</h3>
                 <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>

         </p>
    
                </div>    
                </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute'=>'parent_id', 'filter'=>Categories::getParentCategories()],            
            array(
                'attribute' => 'image',
                'format' => ['image',['width'=>'80','height'=>'60']],
            ),
            'name',
            'desc',
			['attribute'=> 'status', 'filter' => [0 => 'Inactive', 1 => 'Active'], 'format' => 'raw', 'value' => function($data){
			   return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
			}],
            // 'created_by',
            // 'created_on',
            // 'modified_on',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
      /* $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      }); */
</script>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=categories/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
	
	$(".glyphicon-trash").hide(); // hides the delete button from Action listing
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>