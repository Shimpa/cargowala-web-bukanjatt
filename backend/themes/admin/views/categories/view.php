<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */

$this->title = 'CargoWala | Manage Categories | View Category : '.$model->name;
$title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Categories'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
          ],
          [
           'label' => Yii::t('app', 'Update  Categories'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
          ],
          [
           'label' => Yii::t('app', 'Manage  Categories'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
          ],
          /*[
           'label' => Yii::t('app', 'Delete  Categories'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
          ],*/
];
?>
 <section class="content categories-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?php /* echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'parent_id',
            'name',
            'desc',
            ['attribute' => 'image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'created_by',
            'status',
            // 'created_on',
            // 'modified_on',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>   
