<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body categories-form">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
  <div class="row">
      <div class="col-md-6">
        <?php // echo Html::activeDropDownList($model, 'parent_id', Common::getList($model,['status'=>1],['_id','name']),['class'=>'form-control', 'prompt'=>'Select Parent Category']); ?>
        <?php echo $form->field($model, 'parent_id')->dropDownList(Common::getList($model,['parent_id'=>'', 'status'=>1],['_id','name']),['class'=>'form-control', 'prompt'=>'Select Parent Category']); ?>
      </div>
  </div>

  <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'name'); ?>
      </div>
  </div>

    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
		$file_info = pathinfo($model["image"]);
		if(!empty($file_info['extension'])) echo '<image src='.$model["image"].' height="150px" width="150px" data-toggle="lightbox" />';}?></div>
    </div><br/>	  

  <div class="row">
      <div class="col-md-6">
		<?php echo $form->field($model, 'desc')->textarea(array('maxlength' => 555, 'rows' => 6, 'cols' => 50)); ?>
      </div>
  </div>

  <div class="row">
      <div class="col-md-6">
        <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
      </div>
  </div>
      
  <div class="row">
      <div class="col-md-6">
          <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
          </div>
      </div> 
  </div> 
    <?php ActiveForm::end(); ?>
  </div>
</div>