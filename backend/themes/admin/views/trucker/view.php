<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = 'CargoWala | View Trucker | '.$model->firstname;
$title = $model->firstname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Truckers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Users'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Update  Users'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => Yii::t('app', 'Manage  Users'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
		/*[
			'label' => Yii::t('app', 'Delete  Users'),
			'url'   => ['delete','id' => (string)$model->_id],
			'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
			'wrap'=>true,
			'icon'=>'fa-times',
		],*/
];
?>
 <section class="content users-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // '_id',
            'firstname',
            'lastname',
            'email',
			['attribute' => 'account_type', 'value' => Common::getAccountTypes($model->account_type)],
            ['attribute' => 'image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'mobile_number',
            'alternate_number',
            'landline',
            'pancard',
            'business_service_taxno',
            'business_name',
			 ['attribute' => 'business_logo', 'format' => ['image',['width'=>'120','height'=>'120']]],
			 ['attribute' => 'Membership', 'format' => 'html', 'value' => Common::getMembershipType($model->membership)],	
            'registered_address',
            'registered_city',
            'registered_state',
            'registered_pincode',
            'office_address',
            'office_city',
            'office_state',
            'office_pincode',
            'ac_number',
            'holder_name',
            'bank_name',
            'branch_address',
            'branch_code',
            'ifsc_code',
            'has_cp',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>   
