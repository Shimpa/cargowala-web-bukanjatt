<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Common;


/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CargoWala | Manage Truckers');
$title = Yii::t('app', 'Manage Truckers');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
	[
		'label' => Yii::t('app', 'Create Truckers'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box users-index">
        <div class="box-header mtb10">
         
                  <h3 class="box-title"><?php  echo Html::encode($title) ?> List</h3>
                 <div class="box-tools pull-right">
                  <p>
        <?php //echo Html::a(Yii::t('app', 'Create Truckers'), ['create'], ['class' => 'btn btn-success']) ?>

         </p>
    
                </div>    
                </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php //echo '<pre>'; print_r($dataProvider); die;?>
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            array(
                'attribute' => 'image',
                'format' => ['image', ['width'=>'80', 'height'=>'60']],
            ),			
            'firstname',
            'lastname',
            'email',
            'mobile_number',
            ['attribute'=>'role', 'filter' => ['11' => 'Individual', '12' => 'Fleet Ownner'], 'format'=>'html', 'value'=>function($data){
                return Common::getAccountTypes($data->role);
            }],
			['attribute'=>'membership', 'format'=>'html', 'value'=>function($data){
                return Common::getMembershipType($data->membership);
            }],			
			/* ['attribute'=>'compliance', 'format'=>'html', 'value'=>function($data){
                return '<div> Compliance <title><i class="fa fa-files-o"></i>'.Common::getCompliance($data->compliance).'</title></div>';
            }], */
            //'status',
            ['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
               return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
            }],
            // 'image',
            // 'account_type',
            // 'company_type',
            // 'business_type',
            // 'business',
            // 'pancard',
            // 'status',
            // 'states',
            // 'role',
            // 'has_cp',
            // 'social',
            // 'created_on',
            // 'modified_on',
            // 'compliance',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
	/* $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false
		});
	}); */
</script>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({ 
            type: "POST",
            url: '?r=trucker/changestatus',
            data: {id:id},
            success: function(msg){}
        });
    })
    
    $(".changeCP").bootstrapSwitch({
        'onText' : 'Yes',
        'offText' : 'No',
    });
    
    $('input[name="cp-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({ 
            type: "POST",
            url: '?r=trucker/changecp',
            data: {id:id},
            success: function(msg){}
        });
    })
	
	$(".glyphicon-trash").hide(); // hides the delete button from Action listing
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>	