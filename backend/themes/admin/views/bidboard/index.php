<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Bookings;
use common\models\Common;
use common\models\Users;
$userModel = new Users();
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name.' | Manage Bid Load Boards';
$title = Yii::t('app', 'Bid Load Board');
$this->params['breadcrumbs'][] = $this->title;
/* $this->menu = [
	[
		'label' => Yii::t('app', 'Create  Bookings'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
]; */
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box bookings-index">
<div class="box-header mtb10">

<h3 class="box-title"><?= Html::encode($title) ?> List</h3>
<div class="box-tools pull-right">
<p>
<?= Html::a(Yii::t('app', 'Create Bookings'), ['create'], ['class' => 'btn btn-success']) ?>

</p>

</div>    
</div><!-- /.box-header -->
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="box-body">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				// 'shipment_id',
				['attribute'=>'shipment_id','value'=>function($data){ return $data->shipment_id;}, 'label'=>'Order Id'],
				['attribute'=>'shipper_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[21,22,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'html', 'label'=>'Shipper Name', 'value'=>function($data){
					return Common::getOwnerName($data->shipper_id);
				}],
				['attribute'=>'shipment_type', 'filter'=>[1 => 'Regular Load Board', 5 => 'Bid Load Board'], 'format'=>'html', 'label'=>'Shipment Type', 'value'=>function($data){
					return Common::getShipmentType($data->shipment_type);
				}],
				// ['attribute'=>'created_on','value'=>function($data){ return $data->sec;}, 'label'=>'Order Date'],
				['attribute'=>'transit','value'=>function($data){ return $data->t_date.' '.$data->t_time;}, 'label'=>'Pickup Date'],
				['attribute'=>'trucker_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'html', 'label'=>'Trucker Name', 'value'=>function($data){
					return Common::getOwnerName($data->trucker_id);
				}],						
				['attribute'=>'loading','value'=>function($data){ return $data->lp_address;}, 'label'=>'Pickup Address'],
				// ['attribute'=>'Bpartner','format'=>'raw', 'value'=>function($data){ return '<div ng-controller="ModalDemoCtrl"><button class="btn btn-default" ng-click="open(sm)">Mycargo</button></div>';}, 'label'=>'Business Partner'],
				['attribute'=>'unloading','value'=>function($data){ return $data->up_address;}, 'label'=>'Drop Address'],
				// ['attribute'=>'items_total','value'=>function($data){ return $data->items_total;}, 'label'=>'Price'],
				// 'unloading',
				// 'transit',
				// 'truck',
				// 'user_id',
				// 'items',
				// 'overall_weight',
				// 'invoice_id',
				// 'status',
				// 'shipment_detail',
				// 'shipment_url',
				// 'created_on',
				// 'modified_on',
				['class' => 'yii\grid\ActionColumn'],
			],
		]); ?>
	 </div>	
</div>
 </div>
</div>
</section>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>