<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
echo '<pre>'; print_r($model); die('update');
/* @var $this yii\web\View */
/* @var $model common\models\Bookings */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body bookings-form">
  <div class="row">
    <?php $form = ActiveForm::begin(); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipment_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'loading') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'unloading') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'transit') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'truck') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipper_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'trucker_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'driver_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'vehicle_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'items') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'overall_weight') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'overall_total') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'invoice_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'insurance') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'status') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'vehicle') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipment_detail') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipment_weight') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'priority_delivery_charges') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipment_type') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'shipment_url') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'invoicing_type') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'created_on') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'modified_on') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'date') ?>

    </div>         
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>    
