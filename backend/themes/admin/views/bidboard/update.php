<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bookings */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bookings',
]) . ' ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->menu = [

          [
           'label' => Yii::t('app', 'Create  Bookings'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Manage  Bookings'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => Yii::t('app', 'View  Bookings'),
           'url'   => ['view','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-eye',
            
          ],
          [
           'label' => Yii::t('app', 'Delete  Bookings'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content bookings-update">

          
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
