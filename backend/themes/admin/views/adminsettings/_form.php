<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\common;
/* @var $this yii\web\View */
/* @var $model common\models\AdminSettings */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body admin-settings-form">
	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'admin_email') ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'company_name') ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?php if(Yii::$app->controller->action->id == "update"){
				echo '<image src='.$model["company_logo"].' height="150px" width="150px" data-toggle="lightbox" />';
			}?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'company_logo')->fileInput(); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'lane_rate') ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'insurance_rate') ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'priority_delivery_rate') ?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
	</div> 
	<?php ActiveForm::end(); ?>
</div>