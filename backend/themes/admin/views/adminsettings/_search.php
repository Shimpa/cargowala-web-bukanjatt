<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AdminSettingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'admin_email') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'company_logo') ?>

    <?= $form->field($model, 'lane_rate') ?>

    <?php // echo $form->field($model, 'insurance_rate') ?>

    <?php // echo $form->field($model, 'priority_delivery_rate') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'modified_on') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
