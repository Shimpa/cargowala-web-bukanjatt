<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AdminSettings */

$this->title = $model->_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admin Settings'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Admin Settings'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Update  Admin Settings'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => Yii::t('app', 'Manage  Admin Settings'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => Yii::t('app', 'Delete  Admin Settings'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content admin-settings-view">
	<div class="box box-default">
	<div class="box-header mtb10">
	  <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
		<div class="box-tools pull-right">
			<p>
				<?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
				<?php /*echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
					'class' => 'btn btn-danger',
					'data' => [
						'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
						'method' => 'post',
					],
				]) */?>
			</p>
		</div>
	</div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'admin_email',
            'company_name',
			['attribute' => 'company_logo', 'format' => ['image',['width'=>'180','height'=>'150']]],
            'lane_rate',
            'insurance_rate',
            'priority_delivery_rate',
            'created_on',
            'modified_on',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section> 