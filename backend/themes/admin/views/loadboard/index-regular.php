<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Bookings;
use common\models\Common;
use common\models\Users;
$userModel = new Users();
$this->title = Yii::$app->name.' | Manage Load Boards';
$title = 'Manage Load Boards';
$this->params['breadcrumbs'][] = $title;
$this->params['breadcrumbs'][] = $page_title;
/* $this->menu = [
	[
		'label' => Yii::t('app', 'Create  Bookings'),
		'url' => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
]; */
?>
<section class="content">

<div class="row">
<div class="col-xs-12">
<div class="box bookings-index">
<div class="box-header mtb10">

<h3 class="box-title"><?= Html::encode($page_title) ?> List</h3>
<div class="box-tools pull-right">
<p>
<?php /*echo Html::a(Yii::t('app', 'Create Bookings'), ['create'], ['class' => 'btn btn-success']) */?>

</p>

</div>    
</div><!-- /.box-header -->
	<div class="box-body" ng-controller="Booking">
		
	 <div id="myModaltruker" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Assign Trucker</h4>
			</div>
			<div class="modal-body">
			<input type="hidden" name="shipment_id" >
			<div class="row">
		
			<div class="col-md-12">
			 <div class="col-md-6 pull-left">
			<input type="text" ng-model="searchTrucker"  placeholder="Search">
			<span><a href="javascript:;" ng-click="refineSearch(searchTrucker)"><i class="fa fa-search"></i></a></span>	 
			 </div>
			</div>	
			<hr/>	
			<div class="col-md-6">
			
			 </div>	
			</div>	
				<table width="80%">
				<thead>
				<tr>
					<td>Name</td>
					<td>Phone No.</td>
					<td>Assign</td>
					</tr>
				</thead>
				<tbody>
				<tr ng-repeat="bp in truckers">
					<td>{{bp.name.firstname}} {{bp.name.lastname}}</td>
					<td>{{bp.contact.mobile_number}}</td>
					<td><input type="radio" name="truker-group" 
					ng-click="assignShipment(bp._id['$id'],1)" value="{{bp._id['$id']}}"></td>
				</tr>
				</tbody>		
				</table>
			</div>
			<div class="modal-footer">
				<p class="success1 text-center hidden alert alert-success"></p>
				<p class="error1 hidden alert alert-warning"></p>
			</div>
		</div>

	</div>
</div>
<!-- get truckers for post new bid -->
<div class="modal fade postnewbidtruker" tabindex="-1" id="postnewbidtruker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Active Truckers</h4>
      </div>
      <div class="modal-body">
			<table width="80%">
				<thead>
				<tr>
					<td>Name</td>
					<td>Phone No.</td>
					<td>Assign</td>
					</tr>
				</thead>
				<tbody>
				<tr ng-repeat="bp in truckers">
					<td>{{bp.name.firstname}} {{bp.name.lastname}}</td>
					<td>{{bp.contact.mobile_number}}</td>
					<td><a data-trucker="{{bp._id['$id']}}" data-trucker-name="{{bp.name.firstname}} {{bp.name.lastname}}"  ng-click="selectTrucker($event);">Select</a></td>
				</tr>
				</tbody>		
		    </table>
		</div>	  
      <div class="modal-footer">
     
      </div>
    </div>
  </div>
</div>
<button type="button" id="loadtrukers" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#postnewbidtruker"></button>		
	<input type="hidden" id="readTruckersUrl" value="<?=Url::to(['bookings/getbp_'])?>">
	<input type="hidden" id="asp2" value="<?=Url::to(['bookings/asp2__'])?>">
	<input type="hidden" id="sbids" value="<?=Url::to(['loadboard/b_ds__'])?>">
	<input type="hidden" id="cnbid" value="<?=Url::to(['loadboard/b_rjc_'])?>">
	<input type="hidden" id="isbidpostedb4" value="<?=Url::to(['loadboard/isbidostedb4'])?>">
	<input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">	
	<button type="button" id="changetrucker" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#myModaltruker"></button>
		
<!--- view bids -->
<!-- Small modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Posted Bids</h4>
      </div>
      <div class="modal-body">
			 	<table width="80%">
				<thead>
				<tr>
					<td>Trucker Name</td>
					<td>Phone No.</td>
					<td>Bid Price</td>
					<td>Bid Status</td>
					<td>Action</td>
					</tr>
				</thead>
				<tbody>
				<tr id="bids-loader">
				   <td>Please wait Loading...</td>
				</tr>
				<tr ng-repeat="bid in bids">
					<td>{{bid.trucker}}</td>
					<td>{{bid.contact_no}}</td>
					<td>{{bid.price}}</td>
					<td id="b{{bid._id}}" >{{bid.status}}</td>
					<td><a href="" data-bid="{{bid._id}}" ng-click="cancelBid($event)" >X</a></td>
					
				</tr>
				<tr>
				  <td colspan="5" ng-show="!bids.length" >No Records Found</td>
				</tr>	
				</tbody>		
				</table>		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>	
<button type="button" id="loadbids-model" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#myModal"></button>	

<!-- post new bid */
<!--- view bids -->
<!-- Small modal -->
<div class="modal fade " tabindex="-1" id="myModalpostnewbid" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Post New Bids</h4>
      </div>
      <div class="modal-body">
		<form id="postnewbid" ng-submit="submitNewBid($event);" action="<?=Url::to(['loadboard/postnbid'])?>">
		<div class="col-md-10 col-offset-md-2">
		  <input type="hidden" name="Bids[shipment_id]" id="shipment_id">
		  <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>">
			
		  <div class="form-group">		    
			<span>{{loading_please_wait}}</span>
					  
		  </div>
			<div class="form-group">		    
			<input type="hidden" ng-model="truckerId" name="Bids[trucker_id]" value="{{truckerId}}">
			<input type="hidden" ng-model="bidId" name="Bids[_id]" value="{{lastBid._id['$id']}}">
			<span id="trucker_name"><a ng-click=chooseTrucker('loadtrukers') data-target="model">{{select_trucker}}</a></span> 
					  
		  </div>	
		  <div class="form-group">
			<label>Bid Price</label>
			<input type="number" name="Bids[price]" class="form-control" value="{{lastBid.price}}" >
			<span ng-show="lastBid.price">Last price Rs {{lastBid.price}}</span>  
		  </div>
		  <div class="from-group">	
			<label>Remark</label>  
			<textarea class="form-control" name="Bids[description]">{{lastBid.description}}</textarea> 
		  </div>
		  <div class="form-group mT10">
		  <input type="submit" class="btn btn-default" name="submit" value="Submit">
		  <input type="reset" class="btn btn-default hidden" name="reset"  value="Reset">
			<p class="alert alert-success mT10" ng-show="msg.success" >{{msg.success}}</p>  
			<p class="alert alert-error mT10" ng-show="errors.length" ng-repeat="error in errors" >{{error}}</p>  
		  </div>	
		  </div>	 	
		  </form>
		</div>	  
      <div class="modal-footer">
     
      </div>
    </div>
  </div>
</div>	
<button type="button" id="postnewbid-model" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#myModalpostnewbid"></button>	
		
		<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],
						// 'shipment_id',
						['attribute'=>'shipment_id','value'=>function($data){ return $data->shipment_id;}, 'label'=>'Order No'],
						['attribute'=>'shipper_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[21,22,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'html', 'label'=>'Shipper Name', 'value'=>function($data){
							return Users::loadUser($data->shipper_id)->getNameandMobileNo();
						}],
				
						['attribute'=>'transit','value'=>function($data){ return $data->t_date.' '.$data->t_time;}, 'label'=>'Pickup Date'],
						
						
						['attribute'=>'loading','format'=>'html','value'=>function($data){ return $data->lp_address.'<br> / '.$data->unloading['up_address'];}, 'label'=>'Pickup Address/Drop Address'],
						// ['attribute'=>'created_on','value'=>function($data){ return $data->sec;}, 'label'=>'Order Date'],
						
						['attribute'=>'trucker_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'raw', 'label'=>'Status/Business Partners', 'value'=>function($data){
							if(in_array($data->status,[0,1,6,8])){
							   $html ='';
							if(empty($data->trucker_id)){
							   $html.='<br/><a href="javascript:;" id="i'.$data->_id.'"';
							   $html.='ng-click="loadModel(\''.$data->_id.'\')" >';
							   $html.='Assign Trucker';
							   $html.='<span class="fa fa-caret-down" ></span></a>';
							}elseif(!empty($data->trucker_id)){
							    $html.="<br/> Assigned ";
							    $html.= Users::loadUser($data->trucker_id)->assignTrucker();
								//$html.='<br/><a href="javascript:;" id="i'.$data->_id.'"';
							    //$html.='ng-click="loadModel(\''.$data->_id.'\')" >';
							    //$html.='Change Trucker';
							    //$html.='<span class="fa fa-caret-down" ></span></a>';
							}
						
							return $html;	
							}else{
							    $html= Users::loadUser($data->trucker_id)->assignTrucker();
							 return $html;	
						    }
						}],						
		
				['attribute'=>'status', 'format'=>'raw','filter'=>Bookings::bookingStatus(),'value'=>function($data){ return '<span class="status-badge '.Bookings::getStatusClass($data->status).'">' .   Bookings::getStatusText($data->status).'</span>';  }],
						['attribute'=>'shipment_details.overall_total','format'=>'raw', 'value'=>function($data){ return empty($data->shipment_detail['overall_total'])?0:$data->shipment_detail['overall_total'];}, 'label'=>'Shipper Price'],
						
						['attribute'=>'vehicles','format'=>'raw', 'value'=>function($data){
							return Bookings::getVehicles($data);}, 'label'=>'Assigned Vehicle/Drivers'],
				
						// ['attribute'=>'items_total','value'=>function($data){ return $data->items_total;}, 'label'=>'Price'],
						// 'unloading',
						// 'transit',
						// 'truck',
						// 'user_id',
						// 'items',
						// 'overall_weight',
						// 'invoice_id',
						// 'status',
						// 'shipment_detail',
						// 'shipment_url',
						// 'created_on',
						// 'modified_on',
						['class' => 'yii\grid\ActionColumn',
						 'template'=>'{view}{update}',],
					],
				]); ?>
	 </div>	
</div>
 </div>
</div>
</section>

<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
	
	 $('input[name="BookingsSearch[transit]"]').daterangepicker({
        singleDatePicker: true,
		// "minDate": new Date(Date.now()),
		// "maxDate": new Date(Date.now()+(1000*60*60*60*12*12*5)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true,
		format: 'DD/MM/YYYY',
    });
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>