<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'shipment_id') ?>

    <?= $form->field($model, 'loading') ?>

    <?= $form->field($model, 'unloading') ?>

    <?= $form->field($model, 'transit') ?>

    <?php // echo $form->field($model, 'truck') ?>

    <?php // echo $form->field($model, 'shipper_id') ?>

    <?php // echo $form->field($model, 'trucker_id') ?>

    <?php // echo $form->field($model, 'driver_id') ?>

    <?php // echo $form->field($model, 'vehicle_id') ?>

    <?php // echo $form->field($model, 'items') ?>

    <?php // echo $form->field($model, 'overall_weight') ?>

    <?php // echo $form->field($model, 'overall_total') ?>

    <?php // echo $form->field($model, 'invoice_id') ?>

    <?php // echo $form->field($model, 'insurance') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'vehicle') ?>

    <?php // echo $form->field($model, 'shipment_detail') ?>

    <?php // echo $form->field($model, 'shipment_weight') ?>

    <?php // echo $form->field($model, 'priority_delivery_charges') ?>

    <?php // echo $form->field($model, 'shipment_type') ?>

    <?php // echo $form->field($model, 'shipment_url') ?>

    <?php // echo $form->field($model, 'invoicing_type') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'modified_on') ?>

    <?php // echo $form->field($model, 'date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
