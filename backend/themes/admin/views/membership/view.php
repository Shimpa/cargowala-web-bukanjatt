<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Membership */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Memberships'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Memberships'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Update  Memberships'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => Yii::t('app', 'Manage  Memberships'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => Yii::t('app', 'Delete  Memberships'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
// echo "<pre>"; print_r($model); exit;
?>
 <section class="content membership-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // '_id',
            'name',
			  ['attribute'=>'description', 'format'=>'html', 'value'=>call_user_func(function($data){
				return '<div> Benefits : <title><i class="fa fa-files-o"></i>'.Common::getCompliance($data->description).'</title></div>';
			}, $model)], 	 
            // 'description[]',
            'price',
            'duration',
            'transactions',
            'status',
            // 'created_on',
            // 'modified_on',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>   
