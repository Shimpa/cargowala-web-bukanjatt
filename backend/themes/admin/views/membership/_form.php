<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Membership */
/* @var $form yii\widgets\ActiveForm */
// echo "<pre>"; print_r($page); exit;
// echo "<pre>"; print_r($model); exit;
?>
<div class="box-body membership-form">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'name') ?>
		</div>         
		<div class="col-md-6">
			<?= $form->field($model, 'price') ?>
		</div>      
	</div>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'duration') ?>
		</div>         
		<div class="col-md-6">
			<?= $form->field($model, 'transactions') ?>
		</div>
    </div>
	<div class="row">
		<div class="col-md-6">	
		<!--div class="col-md-8 input_fields_wrap">
		<button class="add_field_button">Add More Fields</button>
			<?php //echo $form->field($model, 'description') ?>
			<?php //echo $form->field($model, 'description')->textarea(array('maxlength' => 300, 'rows' => 3, 'cols' => 35)); ?>			
		</div--> 
		</div> 
    </div>
	<div class="row">	
		<div class="col-md-6 ">
			<div class="form-group field-membership-description input_fields_wrap">
			<?php if($page == 'update'){?>
			<textarea name="Membership[description][]" class="form-control" id="membership-description" cols="50" rows="6" maxlength="300"> <?php echo htmlspecialchars($model['description'][0]); ?></textarea>
			<div class="help-block"></div>
			 <?php for($i=1; $i<count($model['description']); $i++): ?>
			 <div class="col-md-8"><div class="form-group field-membership-description input_fields_wrap"><textarea class="form-control" name="Membership[description][]" cols="50" rows="6" maxlength="300"/><?php echo htmlspecialchars($model['description'][$i]); ?> </textarea><a href="#" class="remove_field">Remove</a></div></div>
			  <?php endfor;
			}else{
				echo $form->field($model, 'description[]')->textarea(array('maxlength' => 300, 'rows' => 6, 'cols' => 50)); 
			?><div class="help-block"></div>
			<?php }?>
			</div>
		</div>
		<div class="col-md-6">
			<button class="add_field_button">Add More</button>
		</div> 
	</div> 
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'mtype')->dropDownList(['1' => 'Silver', '2' => 'Gold'],['prompt'=>'Choose Plan Type']); ?>
        </div>
    </div>	
	<div class="col-md-6">      
		<div class="form-group">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
	</div> 
    <?php ActiveForm::end(); ?>
</div>