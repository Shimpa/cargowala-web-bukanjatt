<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MembershipSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CargoWala | Manage Memberships');
$title = Yii::t('app', 'Manage Memberships');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create Membership Plan'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
          ],       
];
// echo "<pre>"; print_r($dataProvider); exit;
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box membership-index">
        <div class="box-header mtb10">
                  <h3 class="box-title">Membership Plans List</h3>
                 <div class="box-tools pull-right">
                  <p>
					<?= Html::a(Yii::t('app', 'Create Membership'), ['create'], ['class' => 'btn btn-success']) ?>
				 </p>
                </div>    
                </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'price',
			 ['attribute'=>'description',  'format'=>'html', 'value'=>function($data){
				return '<div> Benefits : <title><i class="fa fa-files-o"></i>'.Common::getCompliance($data->description).'</title></div>';
			}],	
			['attribute'=> 'mtype', 'filter' => ['1'=> 'Silver', '2'=> 'Gold'], 'format' => 'html', 'value' => function($data){
			   return  $data->mtype['text'];
			}],
            // 'mtype.text',
            'duration',
            'transactions',
            ['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
			   return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
			}],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
  /* $(function () {
	$("#example1").DataTable();
	$('#example2').DataTable({
	  "paging": true,
	  "lengthChange": false,
	  "searching": false,
	  "ordering": true,
	  "info": true,
	  "autoWidth": false
	});
  }); */
</script>
	<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=membership/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>