<?php

/**
 * Topbar menu layout.
 *
 * @var \yii\web\View $this View 
 */

use backend\themes\admin\widgets\Menu;
use yii\helpers\Url;
echo Menu::widget(
    [
        'options' => [
            'class' => 'nav navbar-nav'
        ],
        'items' => [
            [
                'label' => Yii::t('app', 'Dashboard'),
                'url' => ['site/dashboard'],
                /*'icon' => 'fa-dashboard',*/
                'active' => Yii::$app->request->url === Url::to(['site/dashboard'])
            ],
            [
                'label' => Yii::t('app', 'Users'),
                'url' => 'javascript:;',
                'options'=>['class'=>'dropdown'],
                'submenuClass'=>'dropdown-menu',
                'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                'items' => [
                    [
                        'label' => Yii::t('app', 'Manage Moderators'),
                        'url' => ['moderators/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Shippers'),
                        'url' => ['shipper/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Truckers'),
                        'url' => ['truckers/index'],
                    ],
                ]
            ],            
            [
                'label' => Yii::t('app', 'Truck Master'),
                'url' => 'javascript:;',
                'options'=>['class'=>'dropdown'],
                'submenuClass'=>'dropdown-menu',
                'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                'items' => [
                    [
                        'label' => Yii::t('app', 'Manage Truck Lanes'),
                        'url' => ['trucklanes/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Tax Terms'),
                        'url' => ['taxterms/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Fare Calculation'),
                        'url' => ['trucklanes/farecalculation'],
                    ],
                ]
            ],
            [
                'label' => Yii::t('app', 'Masters'),
                'url' => 'javascript:;',
                'options'=>['class'=>'dropdown'],
                'submenuClass'=>'dropdown-menu',
                'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels') || Yii::$app->user->can('BViewComments'),
                'items' => [
                    [
                        'label' => Yii::t('app', 'Manage Languages'),
                        'url' => ['languages/index'],
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Content Pages'),
                        'url' => ['cmspages/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Categories'),
                        'url' => ['categories/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Businesses'),
                        'url' => ['business/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Companies'),
                        'url' => ['company/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Vehicles'),
                        'url' => ['vehicles/index'],
                    ],
                    [
                        'label' => Yii::t('app', 'Manage Truck Types'),
                        'url' => ['trucks/index'],
                    ],
                ]
            ],
        ]
    ]
);