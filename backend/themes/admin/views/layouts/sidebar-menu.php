<?php
/**
* Sidebar menu layout.
*
* @var \yii\web\View $this View
*/
use backend\themes\admin\widgets\Menu;
use yii\helpers\Url;
echo Menu::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => [
            [
                'label' => Yii::t('app', 'Dashboard'),
                'url' => ['site/dashboard'],
                'icon' => 'fa fa-dashboard',
                'active' => Yii::$app->controller->id === 'site',
            ],
            /*[
                'label' => Yii::t('app', 'Manage Moderators'),
                'url' => ['moderators/index'],
                'icon' => 'fa fa-users',
                'active' => Yii::$app->controller->id === 'moderators',
            ],*/
            [
                'label' => Yii::t('app', 'Manage Shippers'),
                'url' => ['shipper/index'],
                'icon' => 'fa fa-users',
                'active' => Yii::$app->controller->id === 'shipper',
            ],
            [
                'label' => Yii::t('app', 'Manage Truckers'),
                'url' => ['trucker/index'],
                'icon' => 'fa fa-users',
                'active' => Yii::$app->controller->id === 'trucker',
            ],
            [
                'label' => Yii::t('app', 'Manage Agents'),
                'url' => ['agent/index'],
                'icon' => 'fa fa-users',
                'active' => Yii::$app->controller->id === 'agent',
            ],
			[
				'label' => Yii::t('app', 'Manage Vehicles'),
				'url' => ['vehicles/index'],
				'icon' => 'fa fa-th',
				'active' => Yii::$app->controller->id === 'vehicles',
			],
            [
                'label' => Yii::t('app', 'Manage Drivers'),
                'url' => ['driver/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'driver',
           ],
           
			[
                'label' => Yii::t('app', 'Manage Masters'),
                'url' => '#',
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'bookings',
				'items' => [
					[
						'label' => Yii::t('app', 'Manage Categories'),
						'url' =>['categories/index'],
						// 'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'categories',
					],
					[
						'label' => Yii::t('app', 'Manage Languages'),
						'url' => ['languages/index'],
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'languages',
					],
					[
						'label' => Yii::t('app', 'Manage Tax Terms'),
						'url' =>  ['taxterms/index'],
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'taxterms',
					],
					[
						'label' => Yii::t('app', 'Fare Calculation'),
						'url' => ['trucklanes/farecalculation'],
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === Url::to(['trucklanes/farecalculation']),
					],
					[
						'label' => Yii::t('app', 'Manage Truck Types'),
						'url' => ['trucks/index'],
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'trucks',
					],
					 [
						'label' => Yii::t('app', 'Manage Truck Lanes'),
						'url' => ['trucklanes/index'],
						//'icon' => 'fa fa-circle-o',
						'active' => Yii::$app->controller->id === 'trucklanes' && Yii::$app->request->url !== Url::to(['trucklanes/farecalculation']),
					],
				
				],
            ],
            		
            /*[
                'label' => Yii::t('app', 'Manage Businesses'),
                'url' => ['business/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'business',
            ],
            [
                'label' => Yii::t('app', 'Manage Companies'),
                'url' => ['company/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'company',
            ],*/
		
		
			/*[
                'label' => Yii::t('app', 'Manage Membership'),
                'url' => ['membership/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'membership',
            ],*/
			[
                'label' => Yii::t('app', 'Manage Orders'),
                'url' => ['bookings/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'bookings',
				'items' => [
					[
						'label' => Yii::t('app', 'New Orders'),
						'url' => Url::to(['bookings/index', 'type' => 'new']),
						// 'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'bookings',
					],
					[
						'label' => Yii::t('app', 'Active Orders'),
						'url' => Url::to(['bookings/index', 'type' => 'active']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'bookings',
					],
					[
						'label' => Yii::t('app', 'Intransit Orders'),
						'url' => Url::to(['bookings/index', 'type' => 'intransit']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'bookings',
					],
					[
						'label' => Yii::t('app', 'Cancelled Orders'),
						'url' => Url::to(['bookings/index', 'type' => 'cancelled']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'bookings',
					],
					[
						'label' => Yii::t('app', 'Past Orders'),
						'url' => Url::to(['bookings/index', 'type' => 'past']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'bookings',
					],
					[
						'label' => Yii::t('app', 'Bid Load Board'),
						'url' => Url::to(['loadboard/index', 'type' => 'bidboard']),
						// 'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'loadboard',
					],
					[
						'label' => Yii::t('app', 'Regular Load Board'),
						'url' => Url::to(['loadboard/index', 'type' => 'regular']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'loadboard',
					],
				],
            ],
			/*[
                'label' => Yii::t('app', 'Manage Load Boards'),
                'url' => ['loadboard/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'loadboard',
				'items' => [
					[
						'label' => Yii::t('app', 'Bid Load Board'),
						'url' => Url::to(['loadboard/index', 'type' => 'bidboard']),
						// 'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'loadboard',
					],
					[
						'label' => Yii::t('app', 'Regular Load Board'),
						'url' => Url::to(['loadboard/index', 'type' => 'regular']),
						// 'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'loadboard',
					],
				],
            ],*/
			[
                'label' => Yii::t('app', 'Manage Content Pages'),
                'url' => ['cmspages/index'],
                'icon' => 'fa fa-th',
                'active' => Yii::$app->controller->id === 'faq',
				'items' => [
					[
						'label' => Yii::t('app', 'Manage CMS Pages'),
						'url' => ['cmspages/index'],
						//'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'cmspages',
					],
					[
						'label' => Yii::t('app', 'Manage Topics'),
						'url' => ['topics/index'],
						//'icon' => 'fa fa-dashboard',
						'active' => Yii::$app->controller->id === 'faq',
					],
					[
						'label' => Yii::t('app', 'Manage Faqs'),
						'url' => ['faq/index'],
						//'icon' => 'fa fa-users',
						'active' => Yii::$app->controller->id === 'faq',
					]
				],
            ],
        ],
    ]);
 ?>