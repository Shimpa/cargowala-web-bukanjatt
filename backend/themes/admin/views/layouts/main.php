 <?php
/**
* Theme main layout.
*
* @var \yii\web\View $this View
* @var string $content Content
*/
use web\themes\Theme;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\models\Common;
use common\models\Admin;
use common\models\Moderators;
use common\models\AdminSettings;
use yii\helpers\Url;
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 9]><!--> <html ng-app="cargoWala" class="gt-ie8 gt-ie9 not-ie" lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
        <?= Html::csrfMetaTags() ?> 
        <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>	
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>	
        <!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->

	 	
    </head>
   <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody(); ?>
    <div class="wrapper">

      <header class="main-header">
<?php 
    $result = Moderators::findOne(Yii::$app->user->getId());
    if(!empty($result)){
        // echo '<pre>'; print_r($result); echo '</pre>';
        $fname = !empty($result['name']['firstname']) ? $result['name']['firstname'] : '';
        $lname = !empty($result['name']['lastname']) ? $result['name']['lastname'] : '';
        $user_name = $fname.' '.$lname;
        $user_type = ($result->isAdmin) ? 'Administrator' : 'Moderator';
        $yrdata= strtotime($result->created_on);
        $created_on = date('F - Y', $yrdata);
    }
	$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
	$url = Url::to(['adminsettings/create']);
	if(isset($adminsettings->_id)) $url = Url::to(['adminsettings/view', 'id' => $adminsettings->_id->__toString()]);
?>
        <!-- Logo -->
        <a href="<?php echo Url::to(['site/dashboard']);?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>C</b>Wala</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Cargo</b>Wala</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button -->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
           <div class="">
            <?php //echo $this->render('//layouts/topbar-menu') ?>   
           </div>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php if(!empty($result["image"])){
                    $img = Common::getMediaPath($result, $result["image"]);
                    echo '<image src='.$img.' height="30px" width="30px" style="border-radius:10px" />';
                }?>                    
                 <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                  <span class="hidden-xs"><?= $user_name;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?php if(!empty($result["image"])){
                        $img = Common::getMediaPath($result, $result["image"]);
                        echo '<image src='.$img.' height="60px" width="60px" style="border-radius:20px" />';
                    }?>
                   <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
                    <p>
                      <?= $user_name;?> - <?= $user_type;?>
                      <small>Member since <?= $created_on;?></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                   <!-- <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>-->
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo Url::to(['moderators/update', 'id' => Yii::$app->user->getId()]);?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-left">
                      <a href="<?php echo $url;?>" class="btn btn-default btn-flat">Settings</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo Url::to(['site/logout']);?>" data-method="post" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--<li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>-->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
              <br/><br/>
            <?php /*<div class="pull-left image">
              <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
              <br/><br/>
            </div>
            <div class="pull-left info">
              <p><?= $user_name;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>*/ ?>
          </div>
          <!-- search form -->
          <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <!--<ul class="sidebar-menu">
            <li class="header">SUB NAVIGATION</li>
            </ul>-->      
          <?php echo $this->render('//layouts/sidebar-menu') ?>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><p>&nbsp;</p></h1>
          <ol class="breadcrumb" style="left:12px; font-size:14px">
            <li><a href='<?= Url::to(['site/dashboard']);?>'><i class='fa fa-home'></i>Home</a></li>
            <?php if(!empty($this->params['breadcrumbs'])){
                foreach($this->params['breadcrumbs'] as $bread): 
                    if(is_array($bread)):
                        $label  = $bread['label'];
                        $url    = Url::to($bread['url']);
                    else:
                        $label  = $bread;
                        $url    = "javascript:void(0);";
                    endif;
             ?>
            <li <?=Yii::$app->request->url==$url ? "active" : NULL; ?> ><a href="<?=$url?>"><?=$label?></a></li>
            <?php endforeach;
            }?>  
          </ol>
        </section>

        <!-- Main content -->
        <?= $content ?>   
        <!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.0.1
        </div>
        <strong>Copyright &copy; <?=date('Y')?> .</strong> All rights reserved.
      </footer>
      <!-- Control Sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
	 
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script>
 var cWala =  angular.module('cargoWala',[]);
$(document).ready(function() {
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
   
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="form-group field-membership-description input_fields_wrap"><textarea class="form-control" name="Membership[description][]" cols="50" rows="6" maxlength="300"/> </textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
        }
    });
   
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
   

<?php $this->endBody(); ?>
 

</html>
 <?php $this->endPage(); ?>