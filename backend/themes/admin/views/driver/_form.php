<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
use common\models\Users;
/* @var $this yii\web\View */
/* @var $model common\models\Drivers */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body drivers-form">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'firstname') ?>
			</div>
			
			<div class="col-md-6">
				<?= $form->field($model, 'lastname') ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6"><?= $form->field($model, 'image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
			// $img = Common::getMediaPath($model, $model["image"]);
			$img = $model["image"];
			$file_info = pathinfo($img);
			if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?></div>

			<div class="col-md-6">
				<?= $form->field($model, 'date_of_birth')->textInput(['readonly'=>'true']); ?>
			</div>
		</div><br/>
		
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'driver_address') ?>
			</div>
			 
			<div class="col-md-6">
				<?= $form->field($model, 'driver_city') ?>
			</div>
		</div>
			 
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'driver_state') ?>
			</div>
			 
			<div class="col-md-6">
				<?= $form->field($model, 'driver_pin_code') ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'adhaar_image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
				$img = $model["adhaar_image"];
				$file_info = pathinfo($img);
				if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?>
			</div>

			<div class="col-md-6">
				<?= $form->field($model, 'adhaar_no'); ?>
			</div>
		</div><br/>
		
		<div class="row">
			<div class="col-md-6">
				<?php if(Yii::$app->controller->action->id == "update"){
					echo $form->field($model, 'owner_id')->textInput(['readonly' => 'true']);
				}else{ ?>			
					<?php $userModel = new Users() ?>
					<?php echo $form->field($model, 'owner_id')->dropDownList(Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12]],['_id', 'name', 'firstname', 'lastname']),['class' => 'form-control', 'prompt' => 'Select Owner']); 
				}?>
			</div>
			
			<div class="col-md-6">
				<?= $form->field($model, 'truck_type')->dropDownList(Common::getTruckTypes(),['class'=>'form-control', 'prompt'=>'Select Truck Category']); ?>
			</div>
		</div>
		 
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'mobile_no') ?>
			</div>
		  <?php if(Yii::$app->controller->action->id != "update"){ ?>
				<div class="col-md-6">
					<?= $form->field($model, 'password')->passwordInput() ?>
				</div>
		  <?php }?>
		</div>     
			  
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'licence_number') ?>
			</div>
			 
			<div class="col-md-6">
				<?= $form->field($model, 'licence_expiry')->textInput(['readonly'=>'true']); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'licence_image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
				$img = $model["licence_image"];
				$file_info = pathinfo($img);
				if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?>
			</div>			 
			<div class="col-md-6">
				<?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
			</div>
		</div><br/>
			
		<?php /*<div class="row">
			<div class="col-md-6">
				<?php //echo $form->field($model, 'is_available')->dropDownList(['1' => 'Available', '0' => 'Not available'],['prompt'=>'Choose Option']); ?>
			</div>		
		</div> */?>
    
		<div class="row">
			<div class="col-md-6">      
				<div class="form-group">
					<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				</div>
			</div>
		</div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$image = $model->image;
$ad_image = $model->adhaar_image;
$lc_image = $model->licence_image;
$JS = <<<SCRIPT
$(document).ready(function(){

    $('#sameaddress').on('click', function(e, state) {
		if($(this).is(':checked')){
			$('#users-office_address').val($('#users-registered_address').val());
			$('#users-office_city').val($('#users-registered_city').val());
			$('#users-office_state').val($('#users-registered_state').val());
			$('#users-office_pincode').val($('#users-registered_pincode').val());
		}else{
			$('#users-office_address, #users-office_city, #users-office_state, #users-office_pincode').val('');
		}
    })
    
	var image ='$image';
	if(image){
		$("#drivers-image").prev('input').val(image);
	}
	
	var ad_image ='$ad_image';
	if(ad_image){
		$("#drivers-adhaar_image").prev('input').val(ad_image);
	}
	
	var lc_image ='$lc_image';
	if(lc_image){
		$("#drivers-licence_image").prev('input').val(lc_image);
	}
	
    $('#drivers-date_of_birth').daterangepicker({
        singleDatePicker: true,
        autoclose: true,
	   // "minDate": new Date(Date.now()-(1000*60*60*60*12*12*58)),
	   "maxDate": new Date(Date.now()-(1000*60*60*60*12*12*21)),
	   "startDate": new Date(Date.now()-(1000*60*60*60*12*12*21)),
		showDropdowns: true
    });
	
	$('#drivers-licence_expiry').daterangepicker({
        singleDatePicker: true,
		"minDate": new Date(Date.now()),
		"maxDate": new Date(Date.now()+(1000*60*60*60*12*12*30)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });	

});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>