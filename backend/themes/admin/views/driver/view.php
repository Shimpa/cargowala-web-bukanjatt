<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Common;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Drivers */
$this->title = 'CargoWala | Manage Drivers | View Driver | '.$model->firstname.' '.$model->lastname;
$title = $model->firstname.' '.$model->lastname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Drivers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Drivers'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Update  Drivers'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => Yii::t('app', 'Manage  Drivers'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          /*[
           'label' => Yii::t('app', 'Delete  Drivers'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
          ],*/
];
?>
 <section class="content drivers-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstname',
            'lastname',
            'mobile_no',
			['attribute' => 'image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'truck_type',
            'driver_address',
            'driver_city',
            'driver_state',
            'driver_pin_code',
            ['attribute' => 'licence_image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'licence_number',
            'licence_expiry',
            'adhaar_no',
			['attribute' => 'adhaar_image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'status',
            'driver_status',
            'is_available',
            // 'location',
            'owner_id',
            // 'compliance',
            'date_of_birth',
            // 'created_on',
            // 'modified_on',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>   
