<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\LanguagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CargoWala | Manage Languages');
$title = Yii::t('app', 'Manage Language');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
            [
                'label' => Yii::t('app', 'Create  Languages'),
                'url'   => ['create'],
                'wrap'=>true,
                'icon'=>'fa-plus',
            ],       
        ];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box languages-index">
        <div class="box-header mtb10">
            <h3 class="box-title"><?= Html::encode($title) ?> List</h3>
             <div class="box-tools pull-right">
                <p>
                    <?= Html::a(Yii::t('app', 'Create Languages'), ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>    
        </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
           // '_id',
            'language_name',
            'language_code',
            'language_desc',
            ['attribute'=> 'status', 'filter' => ['' =>'All', '0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
               return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
            }],			
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
     /*  $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      }); */
</script>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=languages/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>