<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Languages */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body languages-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'language_name') ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'language_code') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
    		<?php echo $form->field($model, 'language_desc')->textarea(array('maxlength' => 300, 'rows' => 6, 'cols' => 50)); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>
    </div>
    
        
    <div class="row">      
        <div class="col-md-6">      
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div> 
    </div>
    
    <?php ActiveForm::end(); ?>
</div>