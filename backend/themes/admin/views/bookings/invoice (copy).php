<?php
/**
 * @copyright Copyright &copy;2014 Giandomenico Olini
 * @company Gogodigital - Wide ICT Solutions 
 * @website http://www.gogodigital.it
 * @package yii2-tcpdf
 * @github https://github.com/cinghie/yii2-tcpdf
 * @license GNU GENERAL PUBLIC LICENSE VERSION 3
 * @tcpdf library 6.0.075
 * @tcpdf documentation http://www.tcpdf.org/docs.php
 * @tcpdf examples http://www.tcpdf.org/examples.php
 */
// Load Component Yii2 TCPDF 
\Yii::$app->get('tcpdf');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Gogogital.it');
$pdf->SetTitle('Yii2 TCPDF Example');
$pdf->SetSubject('Yii2 TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Yii2 TCPDF Example', 'Gogodigital - Wide ICT Solutions | gogodigital.it', array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// set default font subsetting mode
$pdf->setFontSubsetting(true);
// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// Set some content to print
$html = <<<HTML


<style type="text/css">

	a{text-decoration:none;}
	.AnnouncementTD{color:#7f8c9d;font-family: sans-serif;font-size:16px;text-align:right;line-height:150%;}
	.AnnouncementTD a{color:#7f8c9d;}

	.viewOnlineTD{color:#ffffff;font-family: sans-serif;font-size:12px;text-align:left;line-height:22px;}
	.viewOnlineTD a{color:#ffffff;}

	.menuTD{color:#ffffff;font-family: sans-serif;font-size:12px;text-align:right;line-height:22px;}
	.menuTD a{color:#ffffff;}	
	
	.buttonTD, .iconTextTD,.td528Button	{color:#ffffff;font-family: sans-serif;font-size:15px;font-weight:lighter;text-align:center;line-height:23px; font-weight: bold;}
	.iconTextTD	{text-align:left; font-size:13px;color:#c0c7d4;}
	.buttonTD a,.td528Button a{color:#ffffff;display:block;}	
	.iconTextTD a{color:#febf4e; font-weight:bold;}		
	
	.headerTD{color:#7f8c9d;font-family: sans-serif;font-size:18px;text-align:center;line-height:25px;}
	.headerTD a{color:#febf4e;}
	.header2TD,.iconHDTD{color:#cfd6e2;font-family: sans-serif;font-size:17px;text-align:center;line-height:25px;}
	.header2TD a,.iconHDTD a{color:#febf4e; font-weight:bold;}
	.header3TD{color:#7f8c9d;font-family: sans-serif;font-size:17px;text-align:center;line-height:27px;}
	.header3TD a{color:#febf4e; font-weight:bold;}
	.header4TD{color:#7f8c9d;font-family: sans-serif;font-size:18px;text-align:left;line-height:25px;}
	.header4TD a{color:#febf4e;}
	.headerPrcTD{color:#7f8c9d;font-family: sans-serif;font-size:40px;text-align:center;}
	.headerPrcTD a{color:#7f8c9d;}
	.iconHDTD{color:#ffffff;}
	
	.RegularTextTD,	.RegularText2TD, .RegularText3TD, .confLinkTD{color:#7f8c9d;font-family: sans-serif; font-size:13px;text-align:left;line-height:23px;}
	.RegularText3TD	{text-align:center; font-size:15px;}
	.RegularTextTD a, .RegularText2TD a, .RegularText3TD a{color:#febf4e; font-weight:bold;}
	.confLinkTD a{color:#67bffd; font-weight:bold;word-break:break-all;}
	
	.invoiceTD{color:#7f8c9d;font-family: sans-serif; font-size:19px;text-align:center;line-height:23px;}
	.invoiceTD a{color:#febf4e;}
	.invCap{color:#7f8c9d;font-family: sans-serif;text-align:center;font-size:15px;}
	.invCap a{color:#7f8c9d;}
	.invReg{color:#7f8c9d;font-family: sans-serif;font-size:13px;text-align:left;}
	.invReg a{color:#7f8c9d;}
	.invInfoA{color:#7f8c9d;font-family: sans-serif; font-size:12px;text-align:right;line-height:20px;}
	.invInfoA a{color:#7f8c9d;pointer-events:none;}
	.invInfoB{color:#7f8c9d;font-family: sans-serif; font-size:12px;text-align:left;line-height:20px;}
	.invInfoB a{color:#7f8c9d;pointer-events:none;}	
	
	td a img{text-decoration:none;border:none;}
	
	.companyAddressTD{color:#7f8c9d;font-family: sans-serif;font-size:13px;text-align:center;line-height:190%;}
	.companyAddressTD a{color:#7f8c9d;}
	.companyAddress2TD{color:#7f8c9d;font-family: sans-serif;font-size:13px;text-align:center;line-height:190%;}
	.companyAddress2TD a{color:#7f8c9d;pointer-events:none;}
	
	.mailingOptionsTD,.termsConTD,.termsCon2TD{color:#888888;font-family: sans-serif;font-size:12px;text-align:center;line-height:170%;}
	.mailingOptionsTD a,.termsConTD a,.termsCon2TD a{color:#888888;font-weight:bold;}
	
	.termsConTD {text-align:left;}
	.termsCon2TD {text-align:right;}
	.termsConTD a,.termsCon2TD a{font-weight:normal;}

	.ReadMsgBody{width:100%;}
	.ExternalClass{width:100%;}
	body{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-font-smoothing:antialiased;margin:0 !important;padding:0 !important;min-width:100% !important;}
		


@media only screen and (max-width: 599px) 
		   {
		body{min-width:100% !important;}   

		td[class=viewOnlineTD]{text-align:center !important;}
		table[class=table600Logo] {width:440px !important;border-bottom-style:solid !important;border-bottom-color:#e1e1e1 !important;border-bottom-width:1px !important;}
		td[class=tdLogo]{width:440px !important;}
		table[class=table600Menu]{width:440px !important;}
		td[class=AnnouncementTD]{width:440px !important;text-align:center !important;font-size:17px !important;}
		table[class=table600Menu] td{height:20px !important;}
		table[class=tbl6AnctText] .menuTD{text-align:center !important;font-size:13px !important;line-height:150% !important;}
		table[class=tbl6AnctText]{width:440px !important;}
		td[class=viewOnlineTD]{width:440px !important;}
		td[class=menuTD]{width:440px !important;font-weight:bold !important;}
		table[class=table600]{width:440px !important;}
		table[class=image600] img{width:440px !important; height:auto !important;}		
		table[class=AnncTable]{width:100% !important;border:none !important;}
		table[class=table280d]{width:440px !important; border-radius:0 0 0 0 !important;}
		td[class=LMrg]{height:8px !important;}
		td[class=LMrg2]{height:6px !important;}
		td[class=LMrg3]{height:10px !important;}
		table[class=tblRgBrdr]{border-right:none !important;}
		td[class=td147]{width:215px !important;}
		table[class=table147]{width:215px !important;}
		table[class=table147tblp]{width:175px !important;}
		td[class=mrgnHrzntlMdl]{width:10px !important;}
		td[class=mvd]{height:30px !important;width:440px !important;}
		table[class=centerize]{margin:0 auto 0 auto !important;}
		table[class=tblBtnCntr2]{width:398px !important; margin:0 auto 0 auto !important;}
		td[class=table28Sqr] img{width:440px !important;height:auto !important;margin:0 auto 0 auto !important; border-radius:4px 4px 0 0 !important;}
		td[class=tbl28Rctngl] img{width:215px !important;height:auto !important;margin:0 auto 0 auto !important;}
		td[class=headerTD]{text-align:center !important;}
		td[class=header4TD]{text-align:center !important;}
		td[class=headerPrcTD]{font-size:25px !important;}
		td[class=RegularTextTD]{font-size:13px !important;}
		td[class=RegularText2TD]{height:0 !important; font-size:13px !important;}
		td[class=RegularText3TD]{font-size:13px !important;}
		td[class=mailingOptionsTD]{text-align:center !important;}
		td[class=companyAddressTD]{text-align:center !important;}
		td[class=esFrMb]{width:0 !important;height:0 !important;display:none !important;}
		table[class=table280brdr]{width:440px !important;}
		table[class=table608]{width:438px !important;}
		table[class=table518b]{width:398px !important;}
		table[class=table518]{width:398px !important;}
		table[class=table518c]{width:195px !important;}
		table[class=table518c2]{width:195px !important;}
		td[class=imgAltTxticTD] img{width:398px !important;height:auto !important; margin:0 auto 17px auto !important;}
		td[class=iconPdngErase]{width:0 !important; display:none !important;}
		td[class=table608]{width:438px !important;}
		td[class=invReg]{width:133px !important; font-size:14px !important;text-align:center !important;font-weight:lighter !important;}
		td[class=invInfoA]{text-align:right !important;width:195px !important;}
		td[class=invInfoB]{text-align:left !important;width:195px !important;}
		td[class=invoiceTD]{width:390px !important; font-weight:bold;}
		td[class=td528Button]{width:358px !important;}
		table[class=table528Button]{width:358px !important;}
		table[class=table528Social]{width:398px !important;}
		table[class=table250]{width:177px !important;}
		}
		


@media only screen and (max-width: 479px) 
		   {
		body{min-width:100% !important;} 
		
		td[class=viewOnlineTD]{text-align:center !important;}
		table[class=table600Logo]{width:300px !important;border-bottom-style:solid !important;border-bottom-color:#e1e1e1 !important;border-bottom-width:1px !important;}
		td[class=tdLogo]{width:300px !important;}
		table[class=table600Menu]{width:300px !important;}
		td[class=AnnouncementTD]{width:300px !important;text-align:center !important;font-size:17px !important;}
		table[class=table600Menu] td{height:20px !important;}
		table[class=tbl6AnctText] .menuTD{text-align:center !important;font-size:12px !important;line-height:150% !important;}
		table[class=tbl6AnctText]{width:300px !important;}
		td[class=viewOnlineTD]{width:300px !important;}
		td[class=menuTD]{width:300px !important;font-weight:bold !important;}
		table[class=table600]{width:300px !important;}
		table[class=image600] img{width:300px !important;height:auto !important;}
		table[class=table608]{width:298px !important;}
		td[class=table608]{width:298px !important;}
		table[class=table518]{width:260px !important;}
		table[class=table518b]{width:268px !important;}
		table[class=table518c]{width:268px !important;}
		table[class=table518c2]{width:268px !important; margin:20px 0 0 0 !important;}
		td[class=imgAltTxticTD] img{width:268px !important;height:auto !important;margin:-4px auto 15px auto !important;}
		table[class=table280Button]{width:264px !important;}
		table[class=centerize]{margin:0 auto 0 auto !important;}
		table[class=tblBtnCntr2]{width:264px !important; margin:0 auto 0 auto !important;}
		table[class=AnncTable]{width:100% !important;border:none !important;}
		table[class=table280d]{width:300px !important; border-radius:0 0 0 0 !important;} 
		td[class=LMrg]{height:8px !important;}
		td[class=LMrg2]{height:6px !important;}
		td[class=LMrg3]{height:10px !important;}
		td[class=wz]{width:15px !important;}
		table[class=tblRgBrdr]{border-right:none !important;}
		td[class=td147]{width:147px !important;}
		table[class=table147]{width:147px !important;}
		table[class=table147tblp]{width:117px !important;}
		td[class=mrgnHrzntlMdl]{width:6px !important;}
		td[class=mvd]{height:30px !important;width:300px !important;}
		td[class=iconPdngErase]{width:0 !important; display:none !important;}
		td[class=table28Sqr] img{width:300px !important;height:auto !important;margin:0 auto 0 auto !important; border-radius:4px 4px 0 0 !important;}
		td[class=tbl28Rctngl] img{width:147px !important;height:auto !important;margin:0 auto 0 auto !important;}
		td[class=headerTD]{font-size:16px !important; font-weight:bold !important;text-align:center !important;}
		td[class=header4TD]{font-size:16px !important; font-weight:bold !important;text-align:center !important;}
		td[class=iconHDTD]{font-size:16px !important;text-align:center !important;}
		td[class=headerPrcTD]{font-size:18px !important;}
		td[class=RegularTextTD]{font-size:13px !important;text-align:left !important;}
		td[class=RegularText2TD]{height:0 !important;font-size:13px !important;}
		td[class=RegularText3TD]{font-size:13px !important;}
		td[class=mailingOptionsTD]{text-align:center !important;}
		td[class=companyAddressTD]{text-align:center !important;}
		td[class=esFrMb]{width:0 !important;height:0 !important;display:none !important;}
		table[class=table280brdr]{width:300px !important;}
		td[class=invReg]{width:89px !important; font-size:13px !important;text-align:center !important;}
		td[class=invInfoA]{text-align:center !important;width:268px !important;}
		td[class=invInfoB]{text-align:center !important;width:268px !important;}
		td[class=invoiceTD]{width:250px !important;}
		td[class="buttonVrt"]{height:16px !important;}
		td[class="buttonVrt2"]{height:12px !important;}
		td[class="buttonVrt3"]{height:10px !important;}
		td[class=td528Button]{width:238px !important;}
		table[class=table528Button]{width:238px !important;}
		table[class=table528Social]{width:266px !important;}
		table[class=table250]{width:117px !important;}
		td[class=termsCon2TD]{text-align:center !important;}
		td[class=termsConTD]{text-align:center !important;}
		}



</style>
</head>
<body style="background-color:#ededed; margin:0 auto; padding:0;">
<center> 


<!--LOGO SECTION-->
<table style="table-layout:fixed;margin:0 auto;mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td align="center">  
<table class="table600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="618">
<tbody><tr>
<td class="tdLogo" align="left" bgcolor="#ededed" width="618"> 
<table class="table600Logo" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="230">
<tbody><tr>
<td>
		<table class="centerize" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0">
		<tbody><tr>
		<td style="line-height:1px;" align="center" valign="middle" height="100"><a href="#" target="_blank"><img src="images/logo.png" style="display:block;" alt="Logo" align="top" border="0" hspace="0" vspace="0" width="281" height="43"></a></td>
		<td class="esFrMb" width="30"></td>
        </tr>
		</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr> 
</tbody></table>         
</td>
</tr>
</tbody></table>  
<!--LOGO SECTION-->
          
      
<!-- INVOICE SECTION -->
<table style="table-layout:fixed;margin:0 auto;mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td align="center"> 
<table class="table600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" border="0" cellpadding="0" cellspacing="0" width="618">
<tbody><tr>
<td>
<table class="table600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" border="0" cellpadding="0" cellspacing="0" width="610">
<tbody><tr>
<td>  
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" border="0" cellpadding="0" cellspacing="0" width="608"> 
<tbody><tr>   
<td>
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border:1px solid #ffffff; border-radius:4px 4px 0 0;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody><tr>
<td style="font-size:0;line-height:0;" height="15">&nbsp;</td>
</tr>
<tr>	
<td>
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody><tr>
<td class="wz" width="20"></td>							
<td>
<!--(BILLED TO) SECTION-->
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<!--BILLED TO-->	
<td class="invInfoA" width="270"><strong>Billed To:</strong></td>
</tr>
<tr>
<td class="invInfoA">Ajay Sharma</td>
</tr>
<tr>
<td class="invInfoA"><a href="#">3rd floor, DLF Info City</a></td>
</tr>
<tr>
<td class="invInfoA"><a href="#">IT Park, Chandigarh</a></td>
</tr>
</tbody></table>
<table class="table518c2" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<td class="invInfoB" width="250"><strong>CargoWala</strong><br>P.O. Box 18728, <br>DLF Info City <br>IT Park, Chandigarh</td>
<td class="iconPdngErase" style="font-size:0;line-height:0;" width="20">&nbsp;</td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
<tr>
<td colspan="3" style="font-size:0;line-height:0;" height="15">&nbsp;</td>
</tr>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<td class="invInfoA" width="270"><a href="#"><strong>Invoice Date</strong>: Sep 5, 2016</a></td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<td class="invInfoB" width="250"><strong>Invoice No:</strong> A - 21637</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<td class="invInfoA" width="270"></td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="270">
<tbody><tr>
<td class="invInfoB" width="250"><strong>Order No:</strong> 637</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>
</td>
</tr> 
<tr>
<td style="font-size:0;line-height:0;" height="30">&nbsp;</td>
</tr>
</tbody></table>



<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" border="0" cellpadding="0" cellspacing="0" width="608"> 
<tbody><tr>   
<td>

<!--heading section -->                                                                  		
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody><tr>																																		<td align="center">
<table style="border-radius:4px 4px 4px 4px;mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#fe5502" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="wz" width="20"></td>
<td>
<table class="table528Button" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
<tbody><tr>
<td style="font-size:0;line-height:0;" height="8">&nbsp;</td>
</tr>
<tr>	
                                                                        
                                                                  	
                                                                        
<td class="td528Button" align="center" width="528">
<strong>Fare / Invoice Details </strong>
</td>
</tr>
<tr>	
<td style="font-size:0;line-height:0;" height="8">&nbsp;</td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>

<!-- Heading section ends -->                                                                                                

<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border:1px solid #ffffff; border-radius:4px 4px 0 0;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody><tr>
<td style="font-size:0;line-height:0;" height="15">&nbsp;</td>
</tr>
<tr>	
<td>
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="200">
<tbody><tr>
<td class="invInfoA" width="200">Rs 2,000</td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<td class="invInfoB" width="350"><strong>Freight Charges</strong><br>(Price per ton = Rs100) or (Price per km = Rs100)
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>

<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="200">
<tbody><tr>
<td class="invInfoA" width="200">Rs 1,000</td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<td class="invInfoB" width="350"><strong>Prioroty delivery rate </strong>
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>

<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="200">
<tbody><tr>
<td class="invInfoA" width="200">Rs 3,000</td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<td class="invInfoB" width="350"><strong>Insurance</strong>
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>

<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt; margin-top:20px;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt; border-top:2px solid #7f8c9d; border-bottom:2px solid #7f8c9d" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="218">
<tbody><tr>
<!--INVOICE DATE-->	
<td class="invInfoA" width="218" ><strong>Rs 5,000</strong></td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt; border-top:2px solid #7f8c9d; border-bottom:2px solid #7f8c9d" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<!--INVOICE NO-->	
<td class="invInfoB" width="350"><strong>Total</strong>
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>

<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt; margin-top:20px" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="200">
<tbody><tr>
<!--INVOICE DATE-->	
<td class="invInfoA" width="200">Rs 2,000</td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<!--INVOICE NO-->	
<td class="invInfoB" width="350"><strong>First Payment - Online</strong><br>
(25% of final total)
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>


<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt; margin-top:20px" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody>
<tr>
<td class="wz" width="20"></td>		
<td>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="right" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="200">
<tbody><tr>
<!--INVOICE DATE-->	
<td class="invInfoA" width="200">Rs 3,000</td>
</tr>
</tbody></table>
<table class="table518c" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="350">
<tbody><tr>
<!--INVOICE NO-->	
<td class="invInfoB" width="350"><strong>Second Payment - Cash on Unloading </strong><br>
(75% of final total)
</td>
<td class="iconPdngErase" width="20"></td>
</tr>
</tbody></table>
</td>
<td class="wz" width="20"></td>
</tr>
</tbody></table>
</td>
</tr> 
<tr>
<td style="font-size:0;line-height:0;" height="30">&nbsp;</td>
</tr>
</tbody></table>
</td>
</tr>                                                  
</tbody></table>
</td>
</tr>                                                  
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>
<table style="table-layout:fixed;mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td class="mvd" style="font-size:0;line-height:0;" align="center" bgcolor="#ededed" width="610" height="30">&nbsp;</td>
</tr>
</tbody></table>
<!--END OF THE MODULE-->

<!--FOOTER SECTION-->
<table style="table-layout:fixed;margin:0 auto;mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td align="center"> 
<table class="table600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="618">
<tbody><tr>
<td align="center">
<table class="table600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="left" border="0" cellpadding="0" cellspacing="0" width="610">
<tbody><tr>
<td align="center">  
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border:1px solid #ffffff; border-radius:4px 4px 4px 4px;" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608"> 
<tbody><tr>   
<td align="center">
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" border="0" cellpadding="0" cellspacing="0"> 
<tbody>
<tr>
<td style="font-size:0;line-height:0;" height="18">&nbsp;</td>
</tr> 
<tr>	
<td align="center">
<table class="table608" style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="608">
<tbody><tr>																																																			
<td align="center">
<table class="table528Social" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border:1px solid #febf4e; border-radius:4px 4px 4px 4px;" align="center" bgcolor="#fe5502" border="0" cellpadding="0" cellspacing="0" width="568">
<tbody><tr>
<td align="center">

<!-- ICONS MUST BE 80 px X 80 px-->
<table style="mso-table-lspace:0pt;mso-table-rspace:0pt;" align="center" bgcolor="#fe5502" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<!--FACEBOOK SECTION-->
<td><a href="#" target="_blank"><img src="images/socialFacebook.jpg" alt="Facebook" border="0" width="40" height="40"></a></td>
<!--TWITTER SECTION-->
<td><a href="#" target="_blank"><img src="images/socialTwitter.jpg" alt="Twitter" border="0" width="40" height="40"></a></td>
<!--LINKEDIN SECTION-->
<td><a href="#" target="_blank"><img src="images/socialLinkedin.jpg" alt="LinkedIn" border="0" width="40" height="40"></a></td>
<!--INSTAGRAM SECTION-->
<td><a href="#" target="_blank"><img src="images/socialInstagram.jpg" alt="Instagram" border="0" width="40" height="40"></a></td>
<!--GOOGLE SECTION-->
<td><a href="#" target="_blank"><img src="images/socialGoogle.jpg" alt="Google" border="0" width="40" height="40"></a></td>
</tr>

</tbody></table>
</td>
</tr>
</tbody></table> 
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="buttonVrt" style="font-size:0;line-height:0;" height="21">&nbsp;</td>
</tr> 
</tbody></table>
</td>
</tr>                                                  
</tbody></table>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr>
<td class="mvd" style="font-size:0;line-height:0;" height="20">&nbsp;</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>     
<table style="table-layout:fixed;margin:0 auto;" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
<td align="center">
<table class="table600" align="center" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="618">
<tbody><tr>
<td> 
<table class="table600" align="left" bgcolor="#ededed" border="0" cellpadding="0" cellspacing="0" width="610">         	
<tbody><tr>
<td style="font-size:0;line-height:0;" height="0">&nbsp;</td>
</tr>
<tr>

<td class="mailingOptionsTD" height="5">You are receiving this because you are a current customer of <a href="#" target="_blank">CargoWala.</a><br><a href="#" target="_blank">Subscribe</a> | <a href="" target="_blank">Unsubscribe</a> form <a href="" target="_blank">CargoWala</a> | <a href="#" target="_blank">Forward</a> invoice to a friend<br>\A9 CargoWala 2016 - All rights reserved</td>
<!--= End of the section -->	
</tr>
<tr>
<td style="font-size:0;line-height:0;" height="25">&nbsp;</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody></table>                                                      
</td>
</tr>
</tbody></table>
<!--FOOTER ENDS HERE-->
</center>
</body>
HTML;
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('yii2_tcpdf_example.pdf', 'D');
//============================================================+
// END OF FILE
//============================================================+
// Close Yii2
\Yii::$app->end();
?>