<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\Users;
use common\models\Trucks;

/* @var $this yii\web\View */
/* @var $model common\models\Bookings */

$this->title = $model->loading['lp_address'];
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<input type="hidden" id="postonbidbrd" value="<?=Url::to(['loadboard/changeshipmenttype'])?>">
<input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
<div class="bookings-view" ng-controller="Booking">


    <p>
        <?php //Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Email invoive', ['invoice', 'id' => (string)$model->_id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to send invoice?',
                'method' => 'post',
            ],
        ]) ?>
		<?= Html::a('Genrate Buility', ['genratebuility', 'id' => (string)$model->_id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to send invoice?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // '_id',
            'shipment_id',
			['attribute'=>'shipment_type','format'=>'raw','value'=>$model->shipmentType($model)],
            ['attribute'=>'loading','label'=>'Loading Address','value'=>$model->loading['lp_address']], 
			 ['attribute'=>'unloading','label'=>'Unloading Address','value'=>$model->unloading['up_address']], 
			 ['attribute'=>'transit','label'=>'Date/Time','value'=>$model->transit['t_date']."/".$model->transit['t_time']], 
			 ['attribute'=>'truck.category','value'=>$model::getTruckCategory($model->truck)], 
			 ['attribute'=>'truck.ttype','value'=>Trucks::getName($model->truck['ttype'])], 
             'truck.quantity',
			 ['attribute'=>'truck.load_type','label'=>'Load Type','value'=>$model->truck['load_type']==1?'Full Load':"Partial Load"],
			 ['attribute'=>'truck.priority_delivery','label'=>'Priority Delivery','value'=>$model->truck['priority_delivery']==1?'Yes':"No"], 
             'truck.load_type',
             //'truck.quantity',
             'truck.quantity',
             //'insurance.text',
             ['attribute'=>'insurance','label'=>'Insurance','value'=>empty($model->insurance['text'])?null:$model->insurance['text']], 
			['attribute'=>'insurance','label'=>'Insurance Amount','value'=>empty($model->insurance['insured_option'])==1?0:$model->insurance['amount']],
			['attribute'=>'shipper_id','format'=>'raw','label'=>'Shipper' ,'value'=>Users::loadUser($model->shipper_id)->getNameandMobileNo()],
			['attribute'=>'items','format'=>'raw','label'=>'Items' ,'value'=>$model::Items($model->items)],
			['attribute'=>'trucker_id','format'=>'raw','label'=>'Trucker','value'=>Users::loadUser($model->shipper_id)->getNameandMobileNo()],
			
			['attribute'=>'status','value'=>$model::getStatusText($model->status)],
			['attribute'=>'shipment_details','format'=>'raw','label'=>'Invoice','value'=>$model::getInvoice($model)],
             //'user_id',
            //'overall_weight',
            //'invoice_id',
            //'shipment_detail',
            //'shipment_url',
            //'created_on',
            //'modified_on',
        ],
    ]) ?>

</div>
