<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Bookings;
use common\models\Common;
use common\models\Users;
$userModel = new Users();
/* @var $this yii\web\View */
/* @var $searchModel common\models\BookingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::$app->name.' | Manage Orders';
$title = 'Manage Orders';
$this->params['breadcrumbs'][] = $title;
$this->params['breadcrumbs'][] = $page_title;
?>
<!-- Trigger the modal with a button -->
<button type="button" id="changetrucker" class="btn btn-info btn-lg hidden" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!--div ng-controller="ModalDemoCtrl">
    <button type="button" class="btn btn-default" ng-click="open('sm')">Open me!</button> 
</div-->
<div class="row" ng-controller="Booking">
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Change Trucker</h4>
			</div>
			<div class="modal-body">
			<input type="hidden" name="shipment_id" >
			<div class="row">
			<div class="col-md-12">
			 <div class="col-md-6 pull-left">
			<input type="text" ng-model="searchTrucker"  placeholder="Search">
			<span><a href="javascript:;" ng-click="refineSearch(searchTrucker)"><i class="fa fa-search"></i></a></span>	 
			 </div>
			</div>	
			<hr/>	
			<div class="col-md-6">
			
			 </div>	
			</div>	
				<table width="80%">
				<thead>
				<tr>
					<td>Name</td>
					<td>Phone No.</td>
					<td>Assign</td>
					</tr>
				</thead>
				<tbody>
				<tr ng-repeat="bp in truckers">
					<td>{{bp.name.firstname}} {{bp.name.lastname}}</td>
					<td>{{bp.contact.mobile_number}}</td>
					<td><input type="radio" name="truker-group" 
					ng-click="assignShipment(bp._id['$id'])" value="{{bp._id['$id']}}"></td>
				</tr>
				</tbody>		
				</table>
			</div>
			<div class="modal-footer">
				<p class="success1 text-center hidden alert alert-success"></p>
				<p class="error1 hidden alert alert-warning"></p>
			</div>
		</div>

	</div>
</div>
	<input type="hidden" id="readTruckersUrl" value="<?=Url::to(['bookings/getbp_'])?>">
	<input type="hidden" id="asp2" value="<?=Url::to(['bookings/asp2__'])?>">
	<input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
    <div class="col-xs-12">
        <div class="box moderators-index">
            <div class="box-header mtb10">
                <h3 class="box-title"><?php echo Html::encode($page_title) ?> List </h3>
                <div class="box-tools pull-right">
                    <!--p><?= Html::a('Create Bookings', ['create'], ['class' => 'btn btn-success']) ?></p-->
                </div>    
            </div><!-- /.box-header -->
            <div class="box-body">
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'columns' => [
						['class' => 'yii\grid\SerialColumn'],
						// 'shipment_id',
						['attribute'=>'shipment_id','value'=>function($data){ return $data->shipment_id;}, 'label'=>'Order No'],
						['attribute'=>'shipper_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[21,22,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'html', 'label'=>'Shipper Name', 'value'=>function($data){
							return Users::loadUser($data->shipper_id)->getNameandMobileNo();
						}],
						['attribute'=>'shipment_type', 'filter'=>[1 => 'Regular Load Board', 5 => 'Bid Load Board'], 'format'=>'html', 'label'=>'Shipment Type', 'value'=>function($data){
							return Common::getShipmentType($data->shipment_type);
						}],
						['attribute'=>'transit','value'=>function($data){ return $data->t_date.' '.$data->t_time;}, 'label'=>'Pickup Date'],
						
						
						['attribute'=>'loading','format'=>'html','value'=>function($data){ return $data->lp_address.'<br> / '.$data->unloading['up_address'];}, 'label'=>'Pickup Address/Drop Address'],
						// ['attribute'=>'created_on','value'=>function($data){ return $data->sec;}, 'label'=>'Order Date'],
						
						['attribute'=>'trucker_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12,3]],['_id', 'name', 'firstname', 'lastname']), 'format'=>'raw', 'label'=>'Status/Business Partners', 'value'=>function($data){
							if(in_array($data->status,[0,1,6,8])){
							   $html ='<span class="status-badge '.Bookings::getStatusClass($data->status).'">' . Bookings::getStatusText($data->status).'</span>';
							/*if(empty($data->trucker_id)){
							   $html.='<br/><a href="javascript:;" id="i'.$data->_id.'"';
							   $html.='ng-click="loadModel(\''.$data->_id.'\')" >';
							   $html.='Assign Trucker';
							   $html.='<span class="fa fa-caret-down" ></span></a>';
							}else{*/	
							    $html.="<br/> Assigned ";
							    $html.= Users::loadUser($data->trucker_id)->assignTrucker();
								//$html.='<br/><a href="javascript:;" id="i'.$data->_id.'"';
							    //$html.='ng-click="loadModel(\''.$data->_id.'\')" >';
							    //$html.='Change Trucker';
							    //$html.='<span class="fa fa-caret-down" ></span></a>';
							// }
						
							return $html;	
							}else{
							  $html= '<span class="status-badge '.Bookings::getStatusClass($data->status).'">' .   Bookings::getStatusText($data->status).'</span>';
							    $html.="<br/>";
							    $html.= Users::loadUser($data->trucker_id)->assignTrucker();
							    return $html;
						    }
						}],						
				
				
						['attribute'=>'shipment_details.overall_total','format'=>'raw', 'value'=>function($data){ return empty($data->shipment_detail['overall_total'])?0:$data->shipment_detail['overall_total'];}, 'label'=>'Shipper Price'],
						
						['attribute'=>'vehicles','format'=>'raw', 'value'=>function($data){
							return Bookings::getVehicles($data);}, 'label'=>'Assigned Vehicle/Drivers'],
				
						// ['attribute'=>'items_total','value'=>function($data){ return $data->items_total;}, 'label'=>'Price'],
						// 'unloading',
						// 'transit',
						// 'truck',
						// 'user_id',
						// 'items',
						// 'overall_weight',
						// 'invoice_id',
						// 'status',
						// 'shipment_detail',
						// 'shipment_url',
						// 'created_on',
						// 'modified_on',
						['class' => 'yii\grid\ActionColumn',
						 'template'=>'{view}',],
					],
				]); ?>
             </div>
        </div>
    </div>
</div>
</section>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
	
	 $('input[name="BookingsSearch[transit]"]').daterangepicker({
        singleDatePicker: true,
		// "minDate": new Date(Date.now()),
		// "maxDate": new Date(Date.now()+(1000*60*60*60*12*12*5)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true,
		format: 'DD/MM/YYYY',
    });	
	
	$(".glyphicon-trash").hide(); // hides the delete button from Action listing
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>