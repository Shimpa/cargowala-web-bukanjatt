<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Bookings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'shipment_id') ?>

    <?= $form->field($model, 'loading') ?>

    <?= $form->field($model, 'unloading') ?>

    <?= $form->field($model, 'transit') ?>

    <?= $form->field($model, 'truck') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'items') ?>

    <?= $form->field($model, 'overall_weight') ?>

    <?= $form->field($model, 'invoice_id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'shipment_detail') ?>

    <?= $form->field($model, 'shipment_url') ?>

    <?= $form->field($model, 'created_on') ?>

    <?= $form->field($model, 'modified_on') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
