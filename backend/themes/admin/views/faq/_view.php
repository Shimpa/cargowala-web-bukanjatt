<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
?>
<?php //echo '<pre>'; print_r($model);?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        '_id',
        ['attribute'=>"question",'value'=>$model->question[$lang]],
       // 'alias',
        // "description.$lang",
        ['attribute'=>"answer",'value'=>$model->answer[$lang]],
        // 'image',
        'status',
        'created_on',
        'modified_on',
    ],
]) ?>