<?php
use yii\helpers\Html; ?>
<div class="row">
	<div class="col-md-6">
		<br/>
		<label for="faq-question" class="control-label">Question</label>
		<?php //echo HTML::input ( 'text', $name = 'Faq[question]['.$lang.']', empty($model->question[$lang]) ? '' : $model->question[$lang], $options = ['class'=>'form-control'] ) ?>
		<?php echo HTML::textarea ($name = 'Faq[question]['.$lang.']', empty($model->question[$lang])?'':$model->question[$lang], $options = ['class'=>'form-control', 'rows' => 3, 'cols' => 50] ) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<br/>
		<label for="faq-answer" class="control-label">Answer</label>
		<?php echo HTML::textarea ($name = 'Faq[answer]['.$lang.']', empty($model->answer[$lang])?'':$model->answer[$lang], $options = ['class'=>'form-control', 'rows' => 8, 'cols' => 50] ) ?>
	</div>
</div>