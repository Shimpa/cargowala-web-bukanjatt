<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body faq-form">
  <div class="row">
    <?php $form = ActiveForm::begin(); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'topic_id') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'question') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'answer') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'created_on') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'modified_on') ?>

    </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'status') ?>

    </div>         
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>    
