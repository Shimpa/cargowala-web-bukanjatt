<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Topics;
/* @var $this yii\web\View */
/* @var $searchModel common\models\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CargoWala | Manage FAQS');
$title = Yii::t('app', 'Manage FAQS');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
	[
		'label' => Yii::t('app', 'Create  Faqs'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],       
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box faq-index">
	<div class="box-header mtb10">
		<h3 class="box-title"><?= Html::encode($title) ?> List</h3>
		<div class="box-tools pull-right">
		<p>
		<?= Html::a(Yii::t('app', 'Create Faq'), ['create'], ['class' => 'btn btn-success']) ?>
		</p>
		</div>    
	</div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
 
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			// '_id',
			['attribute'=>'topic', 'filter'=>Topics::getTopics(), 'format'=>'html', 'value'=>function($data){ return Topics::getTopicName($data->topic);}],
			['attribute'=>'question', 'value'=>function($m){ return empty($m->question['en']) ? '' : $m->question['en']; }],
			['attribute'=>'answer', 'value'=>function($m){ return empty($m->answer['en']) ? '' : $m->answer['en']; }],
			// 'created_on',
			['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
				return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
			}],	
			['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=faq/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
	// $(".filters").hide(); // hides the delete button from Action listing
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>