<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Moderators;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $model common\models\Moderators */
$firstname = !empty($model->firstname) ? $model->firstname : '';
$lastname = !empty($model->lastname) ? $model->lastname : '';
$this->title = $firstname.' '.$lastname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Moderators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
      [
       'label' => Yii::t('app', 'Create  Moderators'),
       'url'   => ['create'],
       'wrap'=>true,
       'icon'=>'fa-plus',
      ],
      [
       'label' => Yii::t('app', 'Update  Moderators'),
       'url'   => ['update','id' => (string)$model->_id],
       'wrap'=>true,
       'icon'=>'fa-edit',
      ],
      [
       'label' => Yii::t('app', 'Manage  Moderators'),
       'url'   =>['index'],
       'wrap'=>true,
       'icon'=>'fa-list',
      ],
      [
       'label' => Yii::t('app', 'Delete  Moderators'),
       'url'   => ['delete','id' => (string)$model->_id],
       'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
       'wrap'=>true,
       'icon'=>'fa-times',
      ],
];
?>
 <section class="content moderators-view">
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <div class="box-tools pull-right">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
              </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'firstname',
            'lastname',
            'email',
            //'password',
            //'hash_token',
            'status',
            // ['attribute' => 'role', 'value' => Common::getRoles($model->role)],
            'created_on',
            'modified_on',
           // 'isAdmin',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>