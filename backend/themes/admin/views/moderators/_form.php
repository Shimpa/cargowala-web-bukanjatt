<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;

/* @var $this yii\web\View */
/* @var $model common\models\Moderators */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body moderators-form">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'firstname') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'lastname') ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['readonly' => !$model->isNewRecord]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput(); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'mobile_number');?>
            </div>
        </div>
    
        <div class="row">
          <div class="col-md-6">  
            <?php if(Yii::$app->controller->action->id == "update"){
                $img = Common::getMediaPath($model, $model["image"]);
                echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';
            }?>
          </div>
        </div>    

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'image')->fileInput(); ?>
            </div>
        </div>
    
        <?php /*<div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?php echo $form->field($model, 'role')->dropDownList(Common::getRoles(),['class'=>'form-control', 'prompt'=>'Select Role']); ?>
            </div>
        </div> */?>

        <div class="row">
            <div class="col-md-6">      
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div> 
        </div> 

    <?php ActiveForm::end(); ?>
</div>