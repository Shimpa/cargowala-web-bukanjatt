<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Moderators */
$firstname = !empty($model->firstname) ? $model->firstname : '';
$lastname = !empty($model->lastname) ? $model->lastname : '';
$this->title = $firstname.' '.$lastname;
$this->title = Yii::t('app', 'Update {modelClass} : ', [
    'modelClass' => 'Moderator',
]) . ' ' . $firstname.' '.$lastname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Moderators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->params['breadcrumbs'][] = $firstname.' '.$lastname;
$this->menu = [
      [
       'label' => Yii::t('app', 'Create  Moderators'),
       'url'   => ['create'],
       'wrap'=>true,
       'icon'=>'fa-plus',
      ],
      [
       'label' => Yii::t('app', 'Manage  Moderators'),
       'url'   =>['index'],
       'wrap'=>true,
       'icon'=>'fa-list',
      ],
      [
       'label' => Yii::t('app', 'View  Moderators'),
       'url'   => ['view','id' => (string)$model->_id],
       'wrap'=>true,
       'icon'=>'fa-eye',
      ],
      [
       'label' => Yii::t('app', 'Delete  Moderators'),
       'url'   => ['delete','id' => (string)$model->_id],
       'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
       'wrap'=>true,
       'icon'=>'fa-times',
      ],
];
?>
 <section class="content moderators-update">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <!--<div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>-->
        </div><!-- /.box-header -->
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</section>