<?php
/*
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    <?= $form->field($model,'file')->fileInput() ?>
   
    <div class="form-group">
        <?= Html::submitButton('Save',['class'=>'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); */?>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TruckLanes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body truck-lanes-form">
<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
    
    <div class="row">
        <div class="col-md-6">&nbsp;</div>
    </div>    
            
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model,'csvfile')->fileInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6"><p style='color:red;'>Note : Only accepted csv file in predefined format to avoid errors.</p></div>
    </div>    
    
    <div class="row">
        <div class="col-md-6">&nbsp;</div>
    </div>    
    
    <div class="col-md-6">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div> 

    <?php ActiveForm::end(); ?>
</div>