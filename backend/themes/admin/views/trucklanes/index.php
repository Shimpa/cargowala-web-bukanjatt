<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TruckLanesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manage Truck Lanes');
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
    [
        'label' => Yii::t('app', 'Upload CSV File'),
        'url'   => ['uploadcsv'],
        'wrap'=>true,
        'icon'=>'fa-plus',
    ],       
    [
        'label' => Yii::t('app', 'Create  Truck Lanes'),
        'url'   => ['create'],
        'wrap'=>true,
        'icon'=>'fa-plus',
    ],       
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box truck-lanes-index">
    <div class="box-header mtb10">
        <h3 class="box-title"><?= Html::encode($this->title) ?> List</h3>
        <div class="box-tools pull-right">
            <p><?= Html::a(Yii::t('app', 'Upload CSV File'), ['uploadcsv'], ['class' => 'btn btn-success']) ?>&nbsp;<?= Html::a(Yii::t('app', 'Create Truck Lanes'), ['create'], ['class' => 'btn btn-success']) ?></p>
        </div>
    </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'source',
            'source_state',
            'destination',
            'destination_state',
            'distance',
            'price_per_km',
            'price_per_ton',
            //['attribute' => 'status', 'filter'=>['0'=> 'Inactive', '1'=> 'Active']],
            ['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
               return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
            }],			
            // 'created_by',
            // 'status',
            // 'created_on',
            // 'modified_on',
            ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
 </div>
</div>
</section>
<script>
	/*$(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      }); */
    </script>
<?php 
//$model = new Moderators();
//echo '<pre>'; print_r($model); die;
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=trucklanes/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>