<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\TruckLanes */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'CargoWala | Fare Calculation');
$title = Yii::t('app', 'Fare Calculation');
$this->params['breadcrumbs'][] = $title;
?>
<section class="content truck-lanes-create" style="min-height:auto">
  <div class="box box-default pull-left">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title;?></h3>
      <!--<div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
      </div>-->
    </div><!-- /.box-header -->
<div class="row">	
<div class="col-md-7">
	<div class="box-body truck-lanes-form">
	  <?php $form = ActiveForm::begin(['id' => 'calculationForm']); ?>
		<div class="row">
			<div class="col-md-10">
				<?php echo $form->field($model, 'truck_lane')->dropDownList(Common::getCustomList($model,['status'=>1],['_id','source','destination']),['class'=>'form-control', 'prompt'=>'Select Truck Lane']); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10">
				<?= $form->field($model, 'distance') ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10">
				<?= $form->field($model, 'weight') ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10">	
				<?= $form->field($model, 'loading_capacity')->hiddenInput(['readonly' => 'true' ])->label(false); ?>
				<?= $form->field($model, 'charges')->hiddenInput(['readonly' => 'true' ])->label(false); ?>
				<?= $form->field($model, 'today_price')->hiddenInput(['readonly' => 'true' ])->label(false); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10">
				<input type="checkbox" id='taxes'  name='taxes'> Including Taxes<br/>
			</div>
		</div>
		<br/><br/>
		<div class="col-md-10">      
			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Calculate') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
<div class="col-md-4" id='fareDiv' style='border:2px solid #fe5502; background:#ECF0F5; margin : 35px 0px; border-radius:5px'>
	<h3 class="box-title">Calculated Fare</h3> 
		<div class="row">
			<div class="col-md-6">
				<div class="box-body truck-lanes-form">Sub Total</div>
			</div>
			<div class="col-md-6 text-right" id="total">
				<div class="box-body truck-lanes-form" id="sub_total">00.00</div>
			</div>
		</div>
         <div id="taxes-cont">
			 <div class="row">
				<div class="col-md-6">
					<div class="box-body truck-lanes-form">Tax</div>
				</div>
				<div class="col-md-6 text-right" id="total">
					<div class="box-body truck-lanes-form">00.00</div>
				</div>
			</div>
		</div>
		<hr style="border-color:#fe5502; margin-top:10px; margin-bottom:10px">
		<div class="row">
			<div class="col-md-6">
				<div class="box-body truck-lanes-form"><strong>Total fare</strong></div>
			</div>
			<div class="col-md-6 text-right" id="total">
				<div class="box-body truck-lanes-form"><strong id="overall_total">00.00</strong></div>
			</div>
		</div>
		<hr style="border-color:#fe5502; margin-top:10px; margin-bottom:10px">
		<span style="color:red" id="note_text">If priority delivery option is choosen then 2 % of 0.00 i.e 0.00 will be charged extra.</span><hr/>
</div>
</div>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $('#fareDiv').hide();
    $('#trucklanes-truck_category').change(function(){
        var siteUrl = '?r=vehicles/gettrucks';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type},
            success: function(msg){
                $('#trucklanes-type').html(msg);
            }
        });
    });
    
    $('#trucklanes-type').on('change', function(){
        var siteUrl = '?r=vehicles/gettruckload';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type},
            success: function(msg){
                $('#trucklanes-loading_capacity').val(msg);
            }
        });
    });
    
    $('#trucklanes-truck_lane').change(function(){
        var siteUrl = '?r=trucklanes/getlanedata';
        var type = $(this).val();
		$('#trucklanes-distance').val('');
		$('#trucklanes-today_price').val('');
		$('#trucklanes-weight').val('');
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            dataType: 'JSON',
            data: {type:type},
            success: function(msg){
                $('#trucklanes-distance').val(msg.distance);
                $('#trucklanes-today_price').val(msg.price_per_ton);
            }
        });
    });    

    $('#trucklanes-term').change(function(){
        var siteUrl = '?r=trucklanes/getterms';
        var type = $(this).val();
        $.ajax({
            type: "POST",
            url: siteUrl,
            dataType: 'JSON',
            data: {type:type},
            success: function(msg){
                var res = JSON.stringify(msg);
                $('#trucklanes-charges').val(res);
            }
        });
    });
  
	$('#calculationForm').submit(function(event){
		event.preventDefault();
		var distance = $('#trucklanes-distance').val();
		var weight = $('#trucklanes-weight').val();
		if(distance != '' && weight != ''){
			var siteUrl = '?r=trucklanes/farecalculation';
			var formData = $("#calculationForm").serialize();
			$.ajax({
				type: "POST",
				url: siteUrl,
				dataType: 'JSON',
				data: formData,
				success: function(msg){
					var html ='';
					for(var i in msg){
						html ='';
						$("#"+i).html(msg[i]);
						for(var j in msg.taxes){
							html+='<div class="row"><div class="col-md-6">';
							html+='<div class="box-body truck-lanes-form">'+msg.taxes[j].name+'</div>';
							html+='</div><div class="col-md-6 text-right" id="total">';
							html+='<div class="box-body truck-lanes-form">'+msg.taxes[j].rate+'</div></div></div>';
						}
					}
					$('#fareDiv').show();
					$("#taxes-cont").html(html);
				}
			});
		}
    });
    
    /*var form = $( this );
    var formData = $("#calculationForm").serialize();
    url = form.attr( "action" );
    alert(url);
    var posting = $.post( url, { formData: formData } );
    posting.done(function( data ) {
        alert(data);
        var content = $( data ).find( "#content" );
        $( "#result" ).empty().append( content );
    });*/

 
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>
   </div>
</section>