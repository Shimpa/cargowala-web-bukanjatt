<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\TruckLanes */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'CargoWala | Fare Calculation');
$title = Yii::t('app', 'Fare Calculation');
$this->params['breadcrumbs'][] = $title;
?>
<section class="content truck-lanes-create">
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"><?= $title;?></h3>
      <!--<div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
      </div>-->
    </div><!-- /.box-header -->
<div class="box-body truck-lanes-form">
  <?php $form = ActiveForm::begin(['id' => 'calculationForm']); ?>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'truck_lane')->dropDownList(Common::getCustomList($model,['status'=>1],['_id','source','destination']),['class'=>'form-control', 'prompt'=>'Select Truck Lane']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'distance') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price_per_km') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'price_per_ton') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'truck_category')->dropDownList(Common::getTruckCategory(),['class'=>'form-control', 'prompt'=>'Select Truck Category']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'type')->dropDownList(['' => 'Select Truck Category First']); ?>
        </div>
    </div>    
    <?= $form->field($model, 'loading_capacity')->hiddenInput(['readonly' => 'true' ])->label(false); ?>
    <?= $form->field($model, 'charges')->hiddenInput(['readonly' => 'true' ])->label(false); ?>
    <h4>Choose Tax and other charges</h4>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'term')->dropDownList(['0' => 'Insurance', '1' => 'Tax', '2' => 'Both'],['prompt'=>'Choose Tax Term']); ?>
        </div>
    </div>
    <div class="col-md-6">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Calculate') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){

    $('#trucklanes-truck_category').change(function(){
        var siteUrl = '?r=vehicles/gettrucks';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type},
            success: function(msg){
                $('#trucklanes-type').html(msg);
            }
        });
    });
    
    $('#trucklanes-type').on('change', function(){
        var siteUrl = '?r=vehicles/gettruckload';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type},
            success: function(msg){
                $('#trucklanes-loading_capacity').val(msg);
            }
        });
    });
    
    $('#trucklanes-truck_lane').change(function(){
        var siteUrl = '?r=trucklanes/getlanedata';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            dataType: 'JSON',
            data: {type:type},
            success: function(msg){
                $('#trucklanes-distance').val(msg.distance);
                $('#trucklanes-price_per_km').val(msg.price_per_km);
                $('#trucklanes-price_per_ton').val(msg.price_per_ton);
            }
        });
    });    

    $('#trucklanes-term').change(function(){
        var siteUrl = '?r=trucklanes/getterms';
        var type = $(this).val();
        $.ajax({
            type: "POST",
            url: siteUrl,
            dataType: 'JSON',
            data: {type:type},
            success: function(msg){
                var res = JSON.stringify(msg);
                $('#trucklanes-charges').val(res);
            }
        });
    });
  
   /* $('#calculationForm').submit(function(event){
        event.preventDefault();
        var siteUrl = '?r=trucklanes/farecalculation';
        var formData = $("#calculationForm").serialize();
       // alert(formData);
        $.ajax({
            type: "POST",
            url: siteUrl,
            dataType: 'JSON',
            data: {formData:formData},
            success: function(msg){
            alert(msg);
                var res = JSON.stringify(msg);
                $('#trucklanes-charges').val(res);
            }
        });
    });*/
    
    /*var form = $( this );
    var formData = $("#calculationForm").serialize();
    url = form.attr( "action" );
    alert(url);
    var posting = $.post( url, { formData: formData } );
    posting.done(function( data ) {
        alert(data);
        var content = $( data ).find( "#content" );
        $( "#result" ).empty().append( content );
    });*/

 
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>
   </div>
</section>