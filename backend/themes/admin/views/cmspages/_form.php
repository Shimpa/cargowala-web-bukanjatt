<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\CmsPages */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body cms-pages-form">
    <?php $form = ActiveForm::begin();?>
    <?php 
    if(!empty($languages)){
        foreach($languages as $lang){
            $arrData[] =  [
                'label' => $lang->language_name,
                'content' => $this->render('_editor', ['model' => $model,'lang'=>$lang->language_code]),
                'active' => '',
            ];
        }?>
        <?= Tabs::widget([
            'items' => $arrData,
        ]);
    }?>
    <div class="col-md-6">
        <br/>
        <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
    </div>  
    <div class="col-md-12">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>
</div>