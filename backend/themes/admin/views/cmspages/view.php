<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model common\models\CmsPages */
$this->title = 'CargoWala | Manage Content Pages | View Content Page : '.$model->en_title;
$title = $model->en_title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Cms Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
	[
		'label' => Yii::t('app', 'Create  Cms Pages'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
	[
		'label' => Yii::t('app', 'Update  Cms Pages'),
		'url'   => ['update','id' => (string)$model->_id],
		'wrap'=>true,
		'icon'=>'fa-edit',
	],
	[
		'label' => Yii::t('app', 'Manage  Cms Pages'),
		'url'   =>['index'],
		'wrap'=>true,
		'icon'=>'fa-list',
	],
	[
		'label' => Yii::t('app', 'Delete  Cms Pages'),
		'url'   => ['delete','id' => (string)$model->_id],
		'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
		'wrap'=>true,
		'icon'=>'fa-times',
	],
];
?>
<section class="content cms-pages-view">
    <div class="box box-default">
        <div class="box-header mtb10">
            <h3 class="box-title"><?= Html::encode($title) ?></h3>
            <div class="box-tools pull-right">
                <p><?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
                'class' => 'btn btn-danger',
                'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
                ],]) ?></p>
            </div>
        </div><!-- /.box-header -->
        <div class="row">
            <div class="col-md-12">
                <?php foreach($languages as $lang){
                    $arrData[] =  [
                    'label' => $lang->language_name,
                    'content' => $this->render('_view', ['model' => $model,'lang'=>$lang->language_code]),
                    'active' => '',
                    ];
                }?>
                <?= Tabs::widget([
                'items' => $arrData,
                ]);?>
            </div>  
        </div>
    </div>
</section>