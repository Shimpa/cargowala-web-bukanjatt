<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
?>
<?php //echo '<pre>'; print_r($model);?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        '_id',
        ['attribute'=> 'title', 'format' => 'raw', 'value' => empty($model->title[$lang]) ? '' : $model->title[$lang]],
		// 'alias',
		// "content.$lang",
        ['attribute' => 'content', 'format' => 'raw', 'value' => empty($model->content[$lang]) ? '' : $model->content[$lang]],
        // 'image',
        'created_by',
        'status',
        // 'created_on',
        // 'modified_on',
    ],
]) ?>