<?php
use yii\helpers\Html; ?>
<script src="//cdn.ckeditor.com/4.5.7/full/ckeditor.js"></script>
    <div class="col-md-6">
        <br/>
        <label for="cmspages-title" class="control-label">Title</label>
        <?php echo HTML::input ( 'text', $name = 'CmsPages[title]['.$lang.']', empty($model->title[$lang]) ? '' : $model->title[$lang], $options = ['class'=>'form-control'] ) ?>
    </div>
    <div class="col-md-12">
        <br/>
        <label for="cmspages-content" class="control-label">Content</label>
        <?php echo HTML::textarea ($name = 'CmsPages[content]['.$lang.']', empty($model->content[$lang])?'':$model->content[$lang], $options = ['class'=>'form-control', 'maxlength' => 300, 'rows' => 6, 'cols' => 50] ) ?>  
    </div>
<script type= 'text/javascript'>
CKEDITOR.replace('CmsPages[content][<?=$lang?>]', {
	toolbar : 'basic',
    uiColor : '#FE5502',
    enterMode : CKEDITOR.ENTER_BR,
    shiftEnterMode : CKEDITOR.ENTER_P
});
</script>