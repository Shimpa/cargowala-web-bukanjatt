<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CmsPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'CargoWala | Manage Content Pages');
$title = Yii::t('app', 'Manage Content Pages');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
    [
        'label' => Yii::t('app', 'Create  Cms Pages'),
        'url'   => ['create'],
        'wrap'=>true,
        'icon'=>'fa-plus',
    ],       
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box cms-pages-index">
    <div class="box-header mtb10">
        <h3 class="box-title"><?= Html::encode($title) ?> List</h3>
        <div class="box-tools pull-right">
            <p><?= Html::a(Yii::t('app', 'Create Cms Pages'), ['create'], ['class' => 'btn btn-success']) ?></p>
        </div>    
    </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'_id',
                ['attribute' => 'title', 'format' => 'html', 'value' => function($m){ return empty($m->title['en'])?'':$m->title['en'];  }],
                'alias',
                ['attribute' => 'content', 'format' => 'html', 'value' => function($m){ return empty($m->content['en']) ? '' :$m->content['en']; }],
                //['attribute' => 'status', 'filter'=> [0 => 'Inactive', 1 => 'Active']],
				['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
				   return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
				}],					
                //'image',
                // 'created_by',
                // 'created_on',
                // 'modified_on',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
 </div>
</div>
</section>
<script>
/* $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
}); */
</script>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=cmspages/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>