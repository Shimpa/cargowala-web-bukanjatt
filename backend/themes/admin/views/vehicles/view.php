<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Vehicles;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */
$this->title = 'CargoWala | Manage Vehicles | View Vehicle | '.$model->vehicle_number;
$title = $model->vehicle_number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Vehicles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'View';
$this->params['breadcrumbs'][] = $title;
$this->menu = [
          [
           'label' => Yii::t('app', 'Create  Vehicles'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Update  Vehicles'),
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => Yii::t('app', 'Manage  Vehicles'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => Yii::t('app', 'Delete  Vehicles'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content vehicles-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title">View Vehicle <?= Html::encode($title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?php /*echo  Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])*/ ?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'truck_category',
            'truck_type',
            'owner_id',
            'vehicle_number',
			['attribute' => 'rc_pic', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'rc_expiry_date',
            'pnumber',
			['attribute' => 'pic', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'permit_expiry_date',
            'renew_date',
			['attribute' => 'image', 'format' => ['image',['width'=>'120','height'=>'120']]],
            'status',
            'truck_status',
            'is_available',			
            // 'created_on',
            // 'modified_on',
        ],
    ]) ?>
  </div>  
 </div>
 </div>
</section>   
