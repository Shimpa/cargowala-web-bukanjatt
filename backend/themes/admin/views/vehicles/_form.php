<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Common;
use common\models\Users;
/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //echo '<pre>'; print_r($model->getErrors()); ?>

<div class="box-body vehicles-form">
  <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
     <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'truck_category')->dropDownList(Common::getTruckTypes(),['class'=>'form-control', 'prompt'=>'Select Truck Category']); ?>
        </div>
        
        <div class="col-md-6">
            <?php echo $form->field($model, 'truck_type')->dropDownList(['' => 'Select truck category first']); ?>
        </div>
     </div>
    <label>Registration Details</label> <hr/>
    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'rc_pic')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
            $img = Common::getMediaPath($model, $model["registration"]["rc_pic"]);
			$file_info = pathinfo($img);
			if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?>
		</div>	
		<div class="col-md-6">
			<?php if(Yii::$app->controller->action->id == "update"){
				echo $form->field($model, 'owner_id')->textInput(['readonly' => 'true']);
			}else{ ?>			
				<?php $userModel = new Users() ?>
				<?php echo $form->field($model, 'owner_id')->dropDownList(Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12]],['_id', 'name', 'firstname', 'lastname']),['class' => 'form-control', 'prompt' => 'Select Owner']); 
			}?>
		</div>	  
    </div><br/>
    
    <div class="row">
      <div class="col-md-6">
          <?= $form->field($model, 'vehicle_number') ?>
      </div>

      <div class="col-md-6">
          <?= $form->field($model, 'rc_expiry_date')->textInput(['readonly'=>'true']); ?>
      </div>
    </div>
    <label>Insurance & Permit Details</label> <hr/>    
    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'pic')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
            $img = Common::getMediaPath($model, $model["permit"]["pic"]);
			$file_info = pathinfo($img);
			if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?>
		</div>		
    </div><br/>    
    
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'pnumber') ?>
        </div>

        <div class="col-md-6">
            <?php echo $form->field($model, 'permit_expiry_date')->textInput(['readonly'=>'true']); ?>
        </div>        
    </div>    
    
    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'image')->fileInput(); ?><?php if(Yii::$app->controller->action->id == "update"){
			$img = Common::getMediaPath($model, $model["insurance"]["image"]);
			$file_info = pathinfo($img);
			if(!empty($file_info['extension'])) echo '<image src='.$img.' height="150px" width="150px" data-toggle="lightbox" />';}?>
		</div>
        <div class="col-md-6">
            <?= $form->field($model, 'renew_date')->textInput(['readonly'=>'true']); ?>
        </div>		
    </div><br/>

    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php 
// echo '<pre>'; print_r($model); die('update');
$image = $model->image;
$pic = $model->pic;
$rc_pic = $model->rc_pic;
$JS = <<<SCRIPT
 var vehicle = '{$model->truck_type}';
$(document).ready(function(){
    // alert('asd');
    $('#vehicles-truck_category').change(function(){
        var siteUrl = '?r=vehicles/gettrucks';
        var type = $(this).val();
        //alert(type);
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type,truck:vehicle},
            success: function(msg){
                $('#vehicles-truck_type').html(msg);
            }
        });
    });
    
 console.log(vehicle);
    if(vehicle !== null)
	  $('#vehicles-truck_category').trigger('change');
      
/*    $('#vehicles-truck_type').on('change', function(){
        var siteUrl = '?r=vehicles/gettruckload';
        var type = $(this).val();
        $.ajax({ 
            type: "POST",
            url: siteUrl,
            data: {type:type},
            success: function(msg){
                $('#vehicles-loading_capacity').val(msg);
            }
        });
    });
    */
        
    var availableStates = [
        "Punjab",
        "Haryana",
        "Himachal Pradesh",
        "Jammu & Kashmir",
        "Rajsthan",
        "Uttar Pradesh",
    ];
    
    function split( val ) {
      return val.split( /,\s*/ );
    }
    
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#vehicles-state" ).bind( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      }).autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableStates, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });    
    
	var image ='$image';
	if(image){
		$("#vehicles-image").prev('input').val(image);
	}
	
	var rc_pic ='$rc_pic';
	if(rc_pic){
		$("#vehicles-rc_pic").prev('input').val(rc_pic);
	}
	
	var pic ='$pic';
	if(pic){
		$("#vehicles-pic").prev('input').val(pic);
	}	
	
    $('#vehicles-renew_date').daterangepicker({
        singleDatePicker: true,
		"minDate": new Date(Date.now()),
		"maxDate": new Date(Date.now()+(1000*60*60*60*12*12*5)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });
	$('#vehicles-rc_expiry_date').daterangepicker({
        singleDatePicker: true,
		"minDate": new Date(Date.now()),
		"maxDate": new Date(Date.now()+(1000*60*60*60*12*12*15)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });
	 $('#vehicles-permit_expiry_date').daterangepicker({
        singleDatePicker: true,
		"minDate": new Date(Date.now()),
		"maxDate": new Date(Date.now()+(1000*60*60*60*12*12*5)),
		//"startDate": new Date(),
        autoclose: true,
		showDropdowns: true
    });
    
    /*jQuery(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        //console.log(this);
        jQuery(this).lightbox();
    });*/
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>