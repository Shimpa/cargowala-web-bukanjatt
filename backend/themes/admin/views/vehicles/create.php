<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Vehicles */
$this->title = Yii::t('app', 'CargoWala | Manage Vehicles | Create Vehicle');
$title = Yii::t('app', 'Create Vehicle');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Vehicles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [  
	[
		'label' => Yii::t('app', 'Manage  Vehicles'),
		'url'   =>['index'],
		'wrap'=>true,
		'icon'=>'fa-list',
	],
];
?>
<section class="content vehicles-create">
	<div class="box box-default">
		<div class="box-header with-border">
			<h3 class="box-title"><?= Html::encode($title) ?></h3>
			<!--<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
			</div>-->
		</div><!-- /.box-header -->
		<?= $this->render('_form', [ 'model' => $model]) ?>
	</div>
</section>