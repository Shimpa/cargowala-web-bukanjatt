<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Vehicles;
use common\models\Trucks;
use common\models\Common;
use common\models\Users;
/* @var $this yii\web\View */
/* @var $searchModel common\models\VehiclesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CargoWala | Manage Vehicles');
$title = Yii::t('app', 'Manage Vehicles');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
    [
        'label' => Yii::t('app', 'Create  Vehicles'),
        'url'   => ['create'],
        'wrap'=>true,
        'icon'=>'fa-plus',
    ],
];
$userModel = new Users();
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box vehicles-index">
                <div class="box-header mtb10">
                    <h3 class="box-title"><?= Html::encode($title) ?> List</h3>
                    <div class="box-tools pull-right">
                        <p><?= Html::a(Yii::t('app', 'Create Vehicles'), ['create'], ['class' => 'btn btn-success']) ?></p>
                    </div>    
                </div><!-- /.box-header -->
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <?php $dataModel = new Trucks(); ?>
                <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
						['attribute'=>'truck_category', 'filter'=>Common::getTruckTypes()], 
                        ['attribute'=>'truck_type', 'filter'=>Common::getList($dataModel,['status'=>1], ['_id', 'name'])], 
						['attribute'=>'owner_id', 'filter'=>Common::getOwnerList($userModel,['status'=>1, 'role'=>[11,12]],['_id', 'name', 'firstname', 'lastname'])],
                        'vehicle_number',
						/* ['attribute'=>'compliance', 'filter' => ['0'=> 'No', '1'=> 'Yes'], 'format'=>'html', 'value'=>function($data){
							return '<div> Compliance <title><i class="fa fa-files-o"></i>'.Common::getCompliance($data->compliance).'</title></div>';
						}], */
						['attribute'=>'truck_status', 'filter' => [ 0 => 'Not Assigned', 1 => 'Assigned', 2 => 'In-Transit']],
						['attribute'=>'is_available', 'filter' => [ 0 => 'Not Available',  1 => 'Available']],
						['attribute'=> 'status', 'filter' => [0 => 'Inactive', 1 => 'Active'], 'format' => 'raw', 'value' => function($data){
						   return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
						}],	
                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
 /*  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  }); */
</script>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=vehicles/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
	$(".glyphicon-trash").hide(); // hides the delete button from Action listing	
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>