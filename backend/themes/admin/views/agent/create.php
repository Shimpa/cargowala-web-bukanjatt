<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = Yii::t('app', 'Create Trucker');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Trucker'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [  

          [
           'label' => Yii::t('app', 'Manage  Trucker'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],         
];
?>
 <section class="content users-create">

        
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){ 
    $('#users-checkbox').click(function() {
		 if($(this).val() == 1){ 
				 $('#users-checkbox').val(0);
				 $('#users-office_address').val($('#users-registered_address').val());
				 $('#users-office_city').val($('#users-register_city').val());
				 $('#users-office_state').val($('#users-register_state').val());
				 $('#users-office_pin').val($('#users-register_pin').val());
		 }else{
			     $('#users-checkbox').val(1);
			     $('#users-office_address').val('');
				 $('#users-office_city').val('');
				 $('#users-office_state').val('');
				 $('#users-office_pin').val(''); 
		 }
    })
});
SCRIPT;
$this->registerJs($JS,$this::POS_END);