<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = 'Create Users';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
           [
           'label' => 'Operations',
           'url'   => 'javascript:;',
           'wrap'=>true,
           'icon'=>'',
           'active'=>true,
           
          ],       

          [
           'label' => 'Manage  Users',
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],         
];
?>
 <section class="content users-create">

        
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->
<div class="nav-tabs-custom">
   
<?=Tabs::widget([
    'items' => [
        [
            'label' => 'Profile',
            'content' => $this->render('_form', ['model' => $model]),
            'active' => true,
        ],
        [
            'label' => 'Jobs & Experience',
            'content' => 'Anim pariatur cliche...dasdasdasdasdasd',
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Schooling Detail',
            'content' => 'Anim pariatur cliche...dasdasdasdasdasd',
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Colleges Detail',
            'content' => 'Anim pariatur cliche...dasdasdasdasdasd',
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Proffesional Certifications',
            'content' => 'Anim pariatur cliche...dasdasdasdasdasd',
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Skill Sets',
            'content' => 'Anim pariatur cliche...dasdasdasdasdasd',
            'headerOptions' => ['class'=>'disabled'],

        ],
    ],
]);
 ?>
 </div> 
</div>
</section>
