<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'avatar') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'mobile_no') ?>

    <?php // echo $form->field($model, 'otp') ?>

    <?php // echo $form->field($model, 'hash_token') ?>

    <?php // echo $form->field($model, 'about') ?>

    <?php // echo $form->field($model, 'is_ip') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'device') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'jobs') ?>

    <?php // echo $form->field($model, 'schools') ?>

    <?php // echo $form->field($model, 'colleges') ?>

    <?php // echo $form->field($model, 'certifications') ?>

    <?php // echo $form->field($model, 'interests') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
