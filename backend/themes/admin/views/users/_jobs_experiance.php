<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Countries;
use common\models\Common;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body users-form">
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-3 pull-right mtb10"> 
     <a href="#jobsModal" type="button"   data-toggle="modal" id="jobsexperience-model" class="btn btn-success" >Add New Job & Experience
              <i class="fa  fa-plus"></i>
          </a>
        
      </div>
       <div id="grid-list-ip"  > 
      <?php Pjax::begin();?>       
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'_id',
            'title',
            'designation',
            'orgnization',
            'from',
            'to',
            'call_charges',
            // 'otp',
            // 'hash_token',
            // 'about',
            // 'status',
            // 'device',
            // 'address',
            // 'jobs',
            // 'schools',
            // 'colleges',
            // 'certifications',
            // 'interests',

            ['class' => 'yii\grid\ActionColumn',
             'template'=>'{delete}{update}',
             'buttons'=>[
                'update'=>function ($url, $model) {
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Update'),
                        'data-update'=>$model['_id'],
                        'onclick' =>'updateForm(event,this);', 
            ]);
        },
        'delete'=>function ($url, $model) { 
            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Delete'),
                        'data-_id'=>$model['_id'],
                        'data-uid'=>$model['user_id'],
                        'onclick' =>'deleteRow(event,this);', 
            ]);
        },         
        ],
         'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'update') {
            $url = Url::to(['jobs/load-job']); // your own url generation logic
            return $url;
        }
        if ($action === 'delete') {
            $url = Url::to(['jobs/delete']); // your own url generation logic
            return $url;
        }     
    }     
             ]     
             
             
        ],
    ]); ?>
    <?php Pjax::end();?>       
    </div>       
   </div>           
  </div>
</div>
<!-- Modal -->
        <div class="example-modal">
          <div id="jobsModal" class="modal fade design-model" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Job & Experience</h4>
                  </div>
                  <div class="modal-body" >
                   <?=$this->render('add_job',['model'=>$model])?>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->

