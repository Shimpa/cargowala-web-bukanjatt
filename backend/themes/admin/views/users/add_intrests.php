<?php
use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Countries;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body users-form">
  <div class="box-body users-form">
  <div class="row">
    <?php $form = ActiveForm::begin(['action'=>Url::to(['users/intrests']),'id'=>'addupdate-intrests']); ?>
        
    <div class="col-md-12">
  
     <?= Html::hiddenInput('user_id',(string)$model->_id)?>    
     <?= $form->field($model, 'intrests')->hiddenInput()->label(false) ?>    
     <div class="mp-tags">
        <label>Interests (Free consultation)</label> 
        <div class="mp-tag-cont form-control" id="mp-tag_cont" data-control="#users-intrests">
        <span class="editable"></span> 
        </div> 
    </div>   
    </div>     
          
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','onclick'=>'submitForm(this);']) ?>
       <button type="button" class="btn btn-default " id="dismiss-model" data-dismiss="modal">Close</button>
    </div>

    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
<!-- Modal -->
<?php
$JS='';
$JS.= <<<SCRIPT
 loadMPTags();
SCRIPT;
$this->registerJs($JS,$this::POS_END);


