<?php
use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Countries;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body users-form">
  <div class="box-body users-form">
  <div class="row">
    <?php $form = ActiveForm::begin(['action'=>Url::to(['jobs/add-update']),'id'=>'addupdate-job']); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'title')->label('Title') ?>
     <?= Html::hiddenInput('user_id',$model->user_id)?>
     <?= Html::hiddenInput('update',(string)$model->_id)?>
     <?= $form->field($model, 'designation') ?>
     <?= $form->field($model, 'orgnization') ?>
     
        
    </div>  
     
    <div class="col-md-6">
     <?= $form->field($model, 'from') ?>    
     <?= $form->field($model, 'to') ?>
     <?= $form->field($model, 'call_charges') ?>    
    </div>         
    <div class="col-md-12">
     <?= $form->field($model, 'skills')->hiddenInput()->label(false) ?>    
     <div class="mp-tags">
        <label>Skills</label> 
        <div class="mp-tag-cont form-control" id="mp-tag_cont" data-control="#jobsexperience-skills">
        <span class="editable"></span> 
        </div> 
    </div>
    </div>    
    <div class="col-md-12"> <p>&nbsp;</p>   
    <?= $form->field($model, 'detail')->textarea() ?>     
    </div>    
 
    <div class="col-md-12">
    <h3>Address</h3>
      </div>  
    <div class="col-md-6 pop-form">
     <?= $form->field($model, 'country')->dropdownList(ArrayHelper::map(Countries::find()->all(),'countryId','name'),['prompt'=>'--Select Country--','onchange'=>'loadStates(this)','data-state_id'=>'#jobsexperience-state','data-state_val'=>$model->state]) ?>
    <?= $form->field($model, 'state')->dropdownList([],['prompt'=>'--Select Country First --']) ?>
    </div>
    <div class="col-md-6">
     <?= $form->field($model, 'city')?>
     <?= $form->field($model, 'zipcode')?>
    </div>  
      
          
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','onclick'=>'submitForm(this);']) ?>
       <button type="button" class="btn btn-default " id="dismiss-model" data-dismiss="modal">Close</button>
    </div>

    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
<!-- Modal -->

<?php
$JS='';
$JS.= <<<SCRIPT
$("#jobsexperience-country").select2();   
$("#jobsexperience-state").select2();
//Datemask2 mm/dd/yyyy
$("#jobsexperience-from").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
$("#jobsexperience-to").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
