<?php

use yii\helpers\Html;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = 'Update Users: ' . ' ' . $model->name['firstname'];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
$this->menu = [
           [
           'label' => 'Operations',
           'url'   => 'javascript:;',
           'wrap'=>true,
           'icon'=>'',
           'active'=>true,
           
          ],
          [
           'label' => 'Create  Users',
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => 'Manage  Users',
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => 'View  Users',
           'url'   => ['view','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-eye',
            
          ],
          [
           'label' => 'Delate  Users',
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"'Are you sure you want to delete this item?'",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content users-update">

          
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

<?=Tabs::widget([
    'items' => [
        [
            'label' => 'Profile',
            'content' => $this->render('_form', ['model' => $model]),
            'active' => true,
        ],
        [
            'label' => 'Jobs & Experience',
            'content' => $this->render('_jobs_experiance', ['model'=>$jobs[0],'dataProvider' => $jobs[1],'searchModel'=>$jobs[2]]),
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Schooling Detail',
            'content' => $this->render('_qualification', ['model'=>$school[0],'dataProvider' => $school[1],'searchModel'=>$school[2],'type'=>'schools']),
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Colleges Detail',
            'content' => $this->render('_colleges', ['model'=>$college[0],'dataProvider' => $college[1],'searchModel'=>$college[2],'type'=>'colleges']),
            'headerOptions' => ['class'=>'disabled'],

        ],
        [
            'label' => 'Proffesional Certifications',
            'content' => $this->render('_certifications', ['model'=>$certificate[0],'dataProvider' => $certificate[1],'searchModel'=>$certificate[2]]),
            'headerOptions' => ['class'=>'disabled'],

        ],
    ],
]);
 ?>

 </div>
</section>
