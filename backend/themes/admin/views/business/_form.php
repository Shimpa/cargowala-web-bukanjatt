<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Business */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body business-form">
    <?php $form = ActiveForm::begin(); ?>
     
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name') ?>
        </div>         
    </div>         
     
    <div class="row">
        <div class="col-md-6">
		  <?php echo $form->field($model, 'desc')->textarea(array('maxlength' => 555, 'rows' => 6, 'cols' => 50)); ?>
        </div>         
    </div>         

    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>       
    </div>       

    <div class="col-md-6">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>
</div>