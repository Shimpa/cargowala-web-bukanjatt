<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TruckersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manage Truckers');
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [

          [
           'label' => Yii::t('app', 'Create  Truckers'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],       


];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box truckers-index">
        <div class="box-header mtb10">
         
                  <h3 class="box-title"><?= Html::encode($this->title) ?> List</h3>
                 <div class="box-tools pull-right">
                  <p>
        <?= Html::a(Yii::t('app', 'Create Truckers'), ['create'], ['class' => 'btn btn-success']) ?>

         </p>
    
                </div>    
                </div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

   
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'name',
            'contact',
            'password',
            'image',
            // 'created_by',
            // 'status',
            // 'created_on',
            // 'modified_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>