<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TruckLanes */

$this->title = Yii::t('app', 'Upload Truck Lanes CSV');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Truck Lanes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [  
    [
        'label' => Yii::t('app', 'Manage  Truck Lanes'),
        'url'   =>['index'],
        'wrap'=>true,
        'icon'=>'fa-list',
    ],         
];
?>
<section class="content truck-lanes-create">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <!--<div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>-->
        </div><!-- /.box-header -->
        <?= $this->render('_csvform', [
        'model' => $model,
        ]) ?>
    </div>
</section>