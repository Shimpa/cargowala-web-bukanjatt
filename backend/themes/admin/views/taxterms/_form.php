<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TruckLanes */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body truck-lanes-form">
  <?php $form = ActiveForm::begin(); ?>
    
    <?php /*<div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'term')->dropDownList(['0' => 'Insurance', '1' => 'Tax'],['prompt'=>'Choose Tax Term']); ?>
        </div>
    </div>*/?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'ttype')->dropDownList(['0' => 'Fixed', '1' => 'Percentage'],['prompt'=>'Choose Tax Type']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'rate') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'priority') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($model, 'status')->dropDownList(['1' => 'Active', '0' => 'Inactive'],['prompt'=>'Choose Option']); ?>
        </div>
    </div>
    
    <div class="col-md-6">      
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div> 
    <?php ActiveForm::end(); ?>
</div>