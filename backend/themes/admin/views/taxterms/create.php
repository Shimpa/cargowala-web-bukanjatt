<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaxTerms */

$this->title = Yii::t('app', 'Create Tax Terms');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Tax Terms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [  

          [
           'label' => Yii::t('app', 'Manage  Tax Terms'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],         
];
?>
 <section class="content tax-terms-create">

        
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
