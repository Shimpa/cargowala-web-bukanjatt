<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\Trucks */
$this->title = Yii::t('app', 'CargoWala | Manage Truck Types | Create Truck');
$title = Yii::t('app', 'Create Truck');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Truck Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [  
      [
       'label' => Yii::t('app', 'Manage Truck Types'),
       'url'   =>['index'],
       'wrap'=>true,
       'icon'=>'fa-list',

      ],         
];
?>
<section class="content trucks-create">
<div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($title) ?></h3>
      <!--<div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
      </div>-->
    </div><!-- /.box-header -->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</section>