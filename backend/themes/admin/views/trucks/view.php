<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Trucks */

$this->title = 'CargoWala | Manage Truck Types | View Truck : '.$model->name;
$title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Truck Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
$this->menu = [
    [
        'label' => Yii::t('app', 'Create  Trucks'),
        'url'   => ['create'],
        'wrap'=>true,
        'icon'=>'fa-plus',
    ],
    [
        'label' => Yii::t('app', 'Update  Trucks'),
        'url'   => ['update','id' => (string)$model->_id],
        'wrap'=>true,
        'icon'=>'fa-edit',
    ],
    [
        'label' => Yii::t('app', 'Manage  Trucks'),
        'url'   =>['index'],
        'wrap'=>true,
        'icon'=>'fa-list',
    ],
    /*[
        'label' => Yii::t('app', 'Delete  Trucks'),
        'url'   => ['delete','id' => (string)$model->_id],
        'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
        'wrap'=>true,
        'icon'=>'fa-times',
    ],*/
];
?>
<section class="content trucks-view">
    <div class="box box-default">
        <div class="box-header mtb10">
            <h3 class="box-title"><?= Html::encode($title) ?></h3>
            <div class="box-tools pull-right">
                <p>
                    <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                    <?php /* echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])*/ ?>
                </p>
            </div>
        </div><!-- /.box-header -->
        <div class="row">
            <div class="col-md-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        '_id',
                        'truck_type',
                        'name',
                        'desc',
                        'loading_capacity',
                        'created_by',
                        'status',
                        // 'created_on',
                        // 'modified_on',
                    ],
                ]) ?>
            </div>  
        </div>
    </div>
</section>