<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Trucks */

$this->title = Yii::t('app', 'CargoWala | Manage Truck Types | Update {modelClass} : ', [
    'modelClass' => 'Truck',
]) . ' ' . $model->name;
$title = Yii::t('app', 'Update {modelClass} : ', [
    'modelClass' => 'Truck Types',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manage Truck Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update Truck');
$this->params['breadcrumbs'][] = $model->name;
$this->menu = [

          [
           'label' => Yii::t('app', 'Create  Trucks'),
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => Yii::t('app', 'Manage  Trucks'),
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => Yii::t('app', 'View  Trucks'),
           'url'   => ['view','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-eye',
            
          ],
          [
           'label' => Yii::t('app', 'Delete  Trucks'),
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"Yii::t('app', 'Are you sure you want to delete this item?')",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content trucks-update">

          
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($title) ?></h3>
              <!--<div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>-->
            </div><!-- /.box-header -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

 </div>
</section>
