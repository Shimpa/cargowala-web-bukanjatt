<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TopicsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'CargoWala | Manage Topics');
$title = Yii::t('app', 'Manage Topics');
$this->params['breadcrumbs'][] = $title;
$this->menu = [
	[
		'label' => Yii::t('app', 'Create  Topics'),
		'url'   => ['create'],
		'wrap'=>true,
		'icon'=>'fa-plus',
	],
];
?>
<section class="content">
<div class="row">
<div class="col-xs-12">
<div class="box topics-index">
	<div class="box-header mtb10">
		<h3 class="box-title"><?= Html::encode($title) ?> List</h3>
		<div class="box-tools pull-right">
			<p> <?= Html::a(Yii::t('app', 'Create Topics'), ['create'], ['class' => 'btn btn-success']) ?> </p>
		</div>    
	</div><!-- /.box-header -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute'=>'title', 'label' => 'English Title', 'value'=>function($m){ return empty($m->title['en'])?'':$m->title['en'];  }],
			['attribute'=>'description', 'label' => 'English Description', 'value'=>function($m){ return empty($m->description['en']) ? '' :$m->description['en']; }],
            ['attribute'=>'title', 'label' => 'Hindi Title', 'value'=>function($m){ return empty($m->title['hi'])?'':$m->title['hi'];  }],
			['attribute'=>'description', 'label' => 'Hindi Description', 'value'=>function($m){ return empty($m->description['hi']) ? '' :$m->description['hi']; }],
            // 'created_on',
            // 'modified_on',
			['attribute'=> 'status', 'filter' => ['0'=> 'Inactive', '1'=> 'Active'], 'format' => 'raw', 'value' => function($data){
			   return '<input class="changeStatus" type="checkbox" data-size="mini" name="status-checkbox" value='.$data->_id->__toString().' '.$a =($data->status == 'Active') ? "checked='checked'" : "" .' >';
			}],				
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
 </div>
</div>
 </div>
</div>
</section>
<?php 
$JS = <<<SCRIPT
$(document).ready(function(){
    $(".changeStatus").bootstrapSwitch({
        'onText' : 'Active',
        'offText' : 'Inactive',
    });
      
    $('input[name="status-checkbox"]').on('switchChange.bootstrapSwitch', function(e, state) {
        e.preventDefault();
        var id = $(this).val();
        $.ajax({
			type: "POST",
            url: '?r=topics/changestatus',
			data: {id:id}, 
            success: function(msg){}
        });
    });
});
	$(".filters").hide(); // hides the delete button from Action listing	

SCRIPT;
$this->registerJs($JS,$this::POS_END);
?>	