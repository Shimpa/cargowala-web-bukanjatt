<?php
use yii\helpers\Html; ?>
<div class="row">
	<div class="col-md-6">
		<br/>
		<label for="topics-title" class="control-label">Title</label>
		<?php echo HTML::input ( 'text', $name = 'Topics[title]['.$lang.']', empty($model->title[$lang]) ? '' : $model->title[$lang], $options = ['class'=>'form-control'] ) ?>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<br/>
		<label for="topics-description" class="control-label">Description</label>
		<?php echo HTML::textarea ($name = 'Topics[description]['.$lang.']', empty($model->description[$lang])?'':$model->description[$lang], $options = ['class'=>'form-control', 'rows' => 6, 'cols' => 50] ) ?>  
	</div>
</div>