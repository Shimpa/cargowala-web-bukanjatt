<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;
?>
<?php //echo '<pre>'; print_r($model);?>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        '_id',
        ['attribute'=>"title",'value'=>$model->title[$lang]],
       // 'alias',
        // "description.$lang",
        ['attribute'=>"description",'value'=>$model->description[$lang]],
        // 'image',
        'status',
        'created_on',
        'modified_on',
    ],
]) ?>