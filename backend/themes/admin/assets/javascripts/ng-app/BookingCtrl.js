cWala.filter('cstmFilter',function(){
	 return function(x) { console.log(x)
        return x['$id'] ||null;
    };
})
cWala.controller('Booking',['$scope','$http',function($scope,$http){
	$scope.truckers = [];
	
	
    $scope.loadModel = function(shipment_id){
		$('[name="shipment_id"]').val(shipment_id);
		$("#changetrucker").trigger('click');
	}
	
	$scope.refineSearch  = function(term){
		angular.element('.success1').addClass('hidden');
		var data = getData();
		data['term']    = term;
		var data         = $.param(data);
		if((term && term.length<2) || acall)return;
		acall=true;
		$http.post($("#readTruckersUrl").val(),data,getConfig()).success(function(res){
		acall=false;
		$scope.truckers = res;
			
	});
	}
	function getConfig(){
		return {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
        };
	}
	var data = {_csrf:_csrf};
	function getData(){
		return data;
	}
	if($("#readTruckersUrl").length>0){
	var dat1 = $.param(data);
		$http.post($("#readTruckersUrl").val(),dat1,getConfig()).success(function(res){
		acall=false;
		$scope.truckers = res;
			
	});
	}
	$scope.assignShipment = function(tid,regular){
		angular.element('.success1').addClass('hidden');
		var cnf = confirm('Are you sure you want to assign current shipment');
		if(!cnf)return;
		var data  =getData()
		    data['__sid'] =  $("[name='shipment_id']").val();
		    data['__tid'] =  tid;
		    if(regular){
		       data['regular-board'] =  1;
	        }
		var dat1 = $.param(data);
			$http.post($("#asp2").val(),dat1,getConfig()).success(function(res){
                if(res.status==200){
				   angular.element('#i'+res.id).replaceWith(res.msg);
                   var sucele =angular.element('.success1').removeClass('hidden');
					   sucele.html(res.successMsg);	
				} 			
	        });
	}
	// view bids
	
	$scope.loadBids = function(shipment_id){
		$("#loadbids-model").click();
		angular.element('.success1').addClass('hidden');
		var data  =getData();
		    data['_s_'] =shipment_id;
		var dat1 = $.param(data);
			$http.post($("#sbids").val(),dat1,getConfig()).success(function(res){
                if(res.status && res.status==200){
                   $scope.bids = res.data;
				   angular.element('#bids-loader').css('display','none');
				} 			
	        });
	}
	// cancel(reject) bid
	$scope.cancelBid = function(e){
		var conf  = confirm('Are you sure');
		if(!conf)return;
		var bid_id = e.target.getAttribute('data-bid');
		var data  =getData();
		    data['_bd___'] =bid_id;
		var dat1 = $.param(data);
			$http.post($("#cnbid").val(),dat1,getConfig()).success(function(res){
                if(res.status && res.status==200){
				   angular.element('#b'+bid_id).html('Bid Rejected');
				} 			
	        });
	}
	// post a new bid
	$scope.postNewBid =  function(shipment_id){
		$("#shipment_id").val(shipment_id);
		$scope.select_trucker  = "Select Trucker";
		$("#postnewbid-model").click();
		
	}
	$scope.chooseTrucker = function(model){
		$("#"+model).click();
		$scope.loading_please_wait = "Loading Please wait..";
		$http.post($("#readTruckersUrl").val(),dat1,getConfig()).success(function(res){
				   $scope.bidtruckers = res;
	    });
		
	};
	// selet trucker ...
	$scope.selectTrucker =  function(e){
		$scope.truckerId = e.target.getAttribute('data-trucker');
		$scope.select_trucker  = e.target.getAttribute('data-trucker-name') + " || Change";
		$scope.errors = false;
		var data  =getData();
		    data['_tid___'] =e.target.getAttribute('data-trucker');
		    data['_sid___'] =$("#shipment_id").val();
		var dat1 = $.param(data);
			$http.post($("#isbidpostedb4").val(),dat1,getConfig()).success(function(res){
                if(res.status && res.status==1){
				  $scope.loading_please_wait = null; 
				  if(res.data){
					  $scope.lastBid = res.data;						 
					// }); 
				  }	
				} 			
	        });
		$("#postnewbidtruker").modal('toggle');
	};
	// post new bid...
	$scope.submitNewBid = function(e){ e.preventDefault();
		
		var dat1 = $(e.target).serialize();
		           $scope.errors = false;							  
			$http.post($(e.target).attr('action'),dat1,getConfig()).success(function(res){
                if(res.status && res.status==200){
					var data = res.data;
				    var bc = angular.element('#bc'+data.shipment_id); 
	                    bc.html(parseInt(bc.html())+1);
					$("[name='Bids[price]']").val('');
					$("[name='Bids[description]']").val('');
					$("[name='Bids[_id]']").val('');
					$scope.lastBid.price = false;
                    $scope.select_trucker  = "Select Trucker";
					$scope.msg = res.msg;
				}else if(res.status && res.status==400){
				    $scope.errors = res.errors;
				}
	        });
	}
	
	/* post regular shipment on bid load board */
	$scope.changeShipmentType = function (shipment_id){
	   	var data  =getData();
		    data['_sid___'] =shipment_id;
		var dat1 = $.param(data);
			$http.post($("#postonbidbrd").val(),dat1,getConfig()).success(function(res){
                if(res.status && res.status==200){
				   location.reload();	
				} 			
	        });	
	};
	
}]);