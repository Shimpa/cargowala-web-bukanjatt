"use strict";
var userIsIp=false;
$(".nav-tabs a[data-toggle=tab]").on("click", function(e) {
  if ($(this).parent().hasClass("disabled")) {
    e.preventDefault();
    alert('User must be IP and profile must be completed to activate these tabs.');  
    return false;
  }
});
// in model avatar  selecttion 
$("#list-avatars").on('click','a',function(){ 
   $("#avatar_pic").attr('src',$(this).data('url'));
   $("#users-avatar_image").val($(this).data('url'));
   $("#avatar_model button").click();    
    
});
$("#users-is_ip").on('change',function(){
    ipisTabs($(this).val()=='IP');  
})
 $("#users-call_charges").on('blur',function(){
    if($("#users-is_ip").val()=='IP'){
       var call_charges = $(this).val();    
       $("#schools-call_charges").val(call_charges);    
       $("#colleges-call_charges").val(call_charges);    
    } 
 });
// activate/deactivate tabs according user types...
// IP or IS 
function ipisTabs(isIP){
 $('.nav-tabs li a').each(function(k,v){
    $("#useris_ip").css('display',isIP?'block':'none');    
    if(!isNewRecord && isIP && k!=0){
      $(v).parent().removeClass('disabled');
      $(v).attr('style','color:#444;');
    }
    else if(k!=0){
      $(v).parent().addClass('disabled'); 
      $(v).attr('style','color:#999');
    } 
    
  });      
}

Object.defineProperty(this,'_csrf',{
    enumerable:false,
    configrable:false,
    get:function (){ return csrf() }
});
function csrf(){
    return document.querySelector('[name="_csrf"]').value;
}
function Jsonify(str){ var $_json = str.replace(/((for\(;;\);))/ig,'').replace(/\r/,'\\r').replace(/\n/,'\\n').replace(/\t/,'\\t');
                 if (typeof($_json)=='object') return $_json;
                    return JSON.parse( $_json );
}
/* update form load data and fill form data
*/
function updateForm(e,obj){ e.preventDefault();
   var url   = obj.href,data={};
       data['update'] = obj.getAttribute('data-update');                      
       data['type']   = obj.getAttribute('data-type');                      
       data['user_id']= $("[name='user_id']").val();                      
       data['_csrf']  = _csrf;                      
   $("[name='update']").val(obj.getAttribute('data-update'));
   $("[name='type']").val(obj.getAttribute('data-type'));                           
   //console.log($("input[type='hidden']"));                                        
   $.post(url,data,function(res){ var res = Jsonify(res);
        var d =res.data;                                 
        for(var i in d){ 
            if($('[name="'+res.form+'['+i+']"]').length && $('[name="'+res.form+'['+i+']"]').get(0).tagName=="SELECT"){
               if(i=='state')
                 $('[name="'+res.form+'[country]"]').get(0).setAttribute('data-state_val',d['state']);   
               $('[name="'+res.form+'['+i+']"]').val(d[i]).change();
            }
            else{ 
                 $('[name="'+res.form+'['+i+']"]').val(d[i]);
                if(i=="skills")loadMPTags();
            }
        }
        var form = res.list;                                                      
         $("#"+form.toLowerCase()+"-model").click();    
    
    },'html');
                                 
}
/* delete form row */
function deleteRow(e,obj){ e.preventDefault();
    if(!confirm('Are you sure want to delete this?'))return;                      
    var data = obj.dataset;data['_csrf'] =_csrf;                      
    $.post(obj.href,data,function(res){ var res = Jsonify(res);
        refreshList(res.form,res.type);
    },'html');                              
}
/* show form errors after submitting form data...
*/
function showErrors(errors,form){ 
    var err = errors.errors;
    $('.required').removeClass('has-error').removeClass('has-success');
    $('.required').find('.help-block').each(function(k,v){ $(v).html('');})
    for(var i in err){
        $('.field-'+form+'-'+i).addClass('has-error').find('.help-block').html(err[i]);
    }
}


/* refersh ip user list data */
function refreshList(listId,type){
  $.post(load_ip_data,{'_csrf':_csrf,'type':type,'uid':$("[name='user_id']").val()},function(res){
    $(listId).html($(res).find(listId));  
  },'html');    
  
}
/* create new tag*/
function newNode(tag){ 
  return document.createElement(tag);
}
/* tags script*/
$('body').on({
      'click':function(){
        $(this).find('span.editable').prop('contenteditable',true).focus();  
    }
},"div#mp-tag_cont");
$('body').on({
    'keyup':function(e){ e.preventDefault();
            var kCode = e.which || e.keyCode,
                txt =this.textContent.match(/^([a-zA-Z0-9]+[\s?]*[a-zA-Z0-9]*[,]*$)$/);
            if(!txt) return this.innerHTML = ""; ;   
            if(kCode==13) return createTag(this,this.textContent);

    }   
  },"div#mp-tag_cont span.editable");
/* create mp tag control*/
function createTag(obj,dta,update_case){ 
    var span = newNode('span'),a = newNode('a'),
        update_case = update_case || false;
    span.innerHTML = '<em>'+dta+'</em>';
    a.innerHTML    = 'X';
    a.onclick      = function(){ removeTag(a)};
    span.appendChild(a);
    obj.parentNode.insertBefore(span,obj);
    obj.innerHTML  = "";
    add2Control(obj,update_case);
    return;
    //obj.removeAttribute('contenteditable');
    //obj.
    //obj.classList.remove('editable');
}
/* add tags to control */
function add2Control(obj,update_case){ 
    var prnt = obj.parentNode,
        control=prnt.getAttribute('data-control'),
        arr = new Array(); 
    $(prnt.querySelectorAll('span > em')).each(function(k,v){
        arr.push(v.textContent);
    });
    if(!update_case)
       $(control).val(arr.join(','));
}
/* remove tag from tag list */
function removeTag(a){
  var prnt = a.parentNode.parentNode;
    prnt.removeChild(a.parentNode);
    add2Control($(prnt).find("span.editable").get(0));    
}
/* create array to  tags*/
function arr2Tag(arr,prnt,update_case){
    for(var i in arr){
       createTag($(prnt).find("span.editable").get(0),arr[i],update_case);
    }
}
function initMPTag(){
   $("div#mp-tag_cont").each(function(k,v){
    if(!$(v).find('span.editable').length){
     var span = newNode('span');
         span.classList.add('editable');
         span.setAttribute('contenteditable',true);
         v.appendChild(span);
         span.focus();
   }
   })
}
function loadMPTags(){
   var control  = "";
   $("div#mp-tag_cont").each(function(k,v){
       control =v.getAttribute('data-control');
       if($(control).val()){ v.innerHTML = '';initMPTag();
          var tag_arr =$(control).val().split(',');
          arr2Tag(tag_arr,v,true); // loaded for form update..          
       }      
   })    
}
/* clear form input|select|textarea control value */
function clearForm(form,skip){
    var skip = skip || {'user_id':1,'type':1},
        ele =  ['select','input','textarea','editable'];
    for(var i in ele){
        if(ele[i]=="editable")
           __clearForm(form,'div#mp-tag_cont','html',skip);
        __clearForm(form,ele[i],'value',skip);
            
    
    }
    
}
function __clearForm(form,tag,type,skip){
    $(form).find(tag).each(function(k,e){ var name = $(e).get(0).name || null;
       if(!(name in skip)){
          if(type=='value'){
             if($(e).get(0).tagName=='SELECT')
                $(e).val('').change();
              $(e).val('');
          }
          else if(type == 'html'){ 
                $(e).html('');   
                initMPTag();
            }
       }
    });    
}
$('body').on('click','[data-clear_form]',function(){
    var form = this.getAttribute('data-clear_form');
    console.log(form);
    clearForm(form);
})
