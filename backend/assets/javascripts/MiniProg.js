
this.mp = function (){
    
   "use strict;"
 var self = this.mp.prototype;   
// xhr objects...
function xhrObject(){
return window.XMLHttpRequest
   ?new XMLHttpRequest()
   :new ActiveXObject("Microsoft.XMLHTTP");    
}
// parse response...
function parseResponse(){
    
}
// parse post get data...
function requestify(data,isFile){
    return new FormData(data);
}
function transport(params,callback){
  var route = params.route || document.href,
      method= params.type  || 'GET',
      isFile= params.file  || false,
      data  = params.data  || null,
  xhr = xhrObject();
  xhr.onreadystatechange = function (){
    if(xhr.status==200 && xhr.readyState==4){
       callback.call(this,xhr.responseText);    
    }  
  };
  xhr.open(method,route,true);
  xhr.send(requestify(data,isFile));    
}
self.http = {
     transport:function (params,callback){
        transport(params,callback); 
     }
};
  
 return self;    
};

