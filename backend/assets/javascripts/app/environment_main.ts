import { NgModule }  from angular2/core;
import { BrowserModule }  from angular2/platform-browser;
/* App Root */
import {AppComponent} from "./environment_app.component"

@NgModule({
  imports:      [
    BrowserModule,
 
  ],
  providers:    [ ],
  declarations: [ AppComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
