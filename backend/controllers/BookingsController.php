<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Bookings;
use common\models\Users;
use common\models\Payments;
use common\models\BookingsSearch;
use common\models\Common;
use common\models\Bids;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
* MembershipController implements the CRUD actions for Bookings model.
*/
class BookingsController extends Controller{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus','getbp_','asp2__','invoice','genratebuility'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    //'invoice' => ['post'],
                    'getbp_' => ['post'],
                    'asp2__' => ['post'],
                ],
            ],
        ];
    }	

	/**
	* Lists all Bookings models.
	* @return mixed
	*/
    public function actionIndex(){
        $searchModel = new BookingsSearch();
		$title = "All Orders";
		$params = Yii::$app->request->queryParams;
		$type = empty($_REQUEST['type'])?null:$_REQUEST['type'];
		switch($type){
			case "new":
			    $title = "Pending Orders";
			    $params['BookingsSearch']['status'] = [0];	
			break;
			case "active":
			    $params['BookingsSearch']['status'] = [6,8];
			    $title = "Active Orders";	
			break;
			case "past":
				$params['BookingsSearch']['status'] = [1,2,3,5];
			    $title = "Past Orders";
			break;
			case "intransit":
				$params['BookingsSearch']['status'] = [4];
			    $title = "In-Transit Orders";
			break;
			case "cancelled":
				$params['BookingsSearch']['status'] = [1,2];
			    $title = "Cancelled Orders";
			break;
			case "bidboard":
				$params['BookingsSearch']['shipment_type'] = 5;
			    $title = "Bid Load Board Orders";
			break;
			case "regular":
				$params['BookingsSearch']['shipment_type'] = 1;
			    $title = "Regular Load Board Orders";
			break;			
			default:
				$params['BookingsSearch']['status'] = [0,1,2,3,4,5,6,7,8];
			    $title = "All Orders";
			break;	
				
		}

        $dataProvider = $searchModel->search($params);
		 // Sorting on the bases of creation date
		// echo '<pre>'; print_r($dataProvider); echo '</pre>';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => $title,
        ]);
    }

  	/**
	* Lists all Bookings models.
	* @return mixed
	*/
    public function actionPendingOrders(){
        $searchModel = new BookingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bookings model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bookings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bookings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bookings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bookings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Bookings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Bookings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = Bookings::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/* get active truckers who can get orders */
	public function actionGetbp_(){ // find just truckers
		$where = ['status'=>1,'role'=>['$in'=>[11,12]]];
		if(Yii::$app->request->post('term'))
		  $where['$or']=[
			              [
				           'name.firstname'=>['$regex'=>new \MongoRegex('/'.Yii::$app->request->post('term').'/'),'$options'=>'i']						    	  
						  ],
			              [
				           'name.lastname'=>['$regex'=>new \MongoRegex('/'.Yii::$app->request->post('term').'/'),'$options'=>'i']						    	  
						  ],
			              [
				           'contact.mobile_number'=>Yii::$app->request->post('term')   	  
						  ], 
			          
		                ];
		$data = Users::find()->select(['_id','name','contact.mobile_number'])
			                ->where($where)->asArray()->all();
		Common::encodeJSON($data,false);		
	}
	/* assign shipement to trucker...*/
	public function actionAsp2__(){
		if(empty(Yii::$app->request->post('__tid')) || empty(Yii::$app->request->post('__sid')))die(json_encode(['status'=>400,'error'=>'invalid data']));
		$shipment = Bookings::findOne(new \MongoId(Yii::$app->request->post('__sid')));
		$shipment->trucker_id =new \MongoId(Yii::$app->request->post('__tid'));
		$shipment->status = Bookings::ACCEPTED;
		$shipment->save(false);
		$trucker = Users::findOne($shipment->trucker_id);
		$shipper = Users::findOne($shipment->shipper_id);
		if(!Yii::$app->request->post('regular-board')){
		$bid = Bids::findOne(['shipment_id'=>$shipment->_id,'trucker_id'=>$shipment->trucker_id]);
		if($bid instanceof Bids){
			$bid->price = $shipment->shipment_detail['load_fare'];
		}else{
			$bid = new Bids();
			$bid->price = $shipment->shipment_detail['load_fare'];
		}
		$bid->status= Bids::ACCEPTED;
		$bid->save();
		}
		
		//print_r($shipment);die;
		// shipper message
		$shippersubject = Common::getLocalMessage('subject_trucker_asigned',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<shipper_name>>'=>$shipper->getName(),
			'<<amount>>'      =>$shipment->shipment_detail['overall_total'],
			'<<date>>'        =>$shipment->transit['t_date'],
			'<<time>>'        =>$shipment->transit['t_time'],
			'<<support_no>>'  =>Common::getSupportNo(),
			
		]);
		$shippermessage = Common::getLocalMessage('message4_shipper_shipment_assigned',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<shipper_name>>'=>$shipper->getName(),
			'<<amount>>'      =>$shipment->shipment_detail['overall_total'],
			'<<date>>'        =>$shipment->transit['t_date'],
			'<<time>>'        =>$shipment->transit['t_time'],
			'<<order_id>>'    =>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		]);
		// trucker message
		$truckersubject = Common::getLocalMessage('subject_shipment_assigned2trucker');
		$truckermessage = Common::getLocalMessage('message4_trucker_newshipment_assigned',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<shipper_name>>'=>$shipper->getName(),
			'<<amount>>'      =>$shipment->shipment_detail['overall_total'],
			'<<date>>'        =>$shipment->transit['t_date'],
			'<<time>>'        =>$shipment->transit['t_time'],
			'<<order_id>>'    =>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		]);
		
		
		//notify trucker...
		if(!empty($trucker->contact['email']))
		   Common::saveNotification($trucker->email, $truckersubject, $truckermessage, 'email');
		if(!empty($trucker->contact['mobile_number']))
		   Common::saveNotification($trucker->contact['mobile_number'], $truckersubject, $truckermessage, 'mobile');
		if(!empty($trucker->device['token']))
		   Common::saveNotification($trucker->device['token'], $truckersubject, $truckermessage, 'push');
		// norify sms push email to shipper
		if(!empty($shipper->contact['email']))
		   Common::saveNotification($shipper->email, $shippersubject, $shippermessage, 'email');
		if(!empty($shipper->contact['mobile_number']))
		   Common::saveNotification($shipper->contact['mobile_number'], $shippersubject, $shippermessage, 'mobile');
		if(!empty($shipper->device['token']))
		   Common::saveNotification($shipper->device['token'], $shippersubject, $shippermessage, 'push');
		
		Common::encodeJSON([
			'status'=>Common::HTTP_SUCCESS,
			'msg'=>'Assigned '. $trucker->loadUser($shipment->trucker_id)->assignTrucker(), 
			'id'=>(string)$shipment->_id,
			'successMsg'=>"Shipment having order id " . $shipment->shipment_id ." assigned to " . $trucker->loadUser($shipment->trucker_id)->getName(),
			
		],false);
		
		
	}
	public function actionInvoice($id){
		
		if(!Yii::$app->request->get('id')){
			Yii::$app->session->setFlash('error','Invalid Parameters');
		    return $this->redirect(['view', 'id' => Yii::$app->request->get('id')]);
		}
		$shipment  = $this->findModel(Yii::$app->request->get('id'));				
		
		$trucker   = $shipment->truckers;
		$shipper   = $shipment->shipper;
		$emails    = [];
		if($trucker){
			array_push($emails,$trucker->contact['email']);
		}
		if($shipper) array_push($emails,$shipper->contact['email']);
		$payments  = Payments::findOne(['shipment_id'=>$shipment->_id]);
		$html = $this->renderPartial('invoice',['shipment'=>$shipment,'payments'=>$payments]);
		$subject = 'Cargowala Order Invoice';
		    Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['no-reply'])
                ->setTo($emails)
                ->setSubject($subject)
                // ->setTextBody('Plain text content')
                ->setHtmlBody($html)
                ->send();
		Yii::$app->session->setFlash('error','Invoice emailed successfully to shipper trucker');
		$this->redirect(['view','id'=>Yii::$app->request->get('id')]);
	}
	public function actionGenratebuility($id){
		
		if(!Yii::$app->request->get('id')){
			Yii::$app->session->setFlash('error','Invalid Parameters');
		    return $this->redirect(['view', 'id' => Yii::$app->request->get('id')]);
		}
		$shipment  = $this->findModel(Yii::$app->request->get('id'));	
		$shipper   = $shipment->shipper;
		$emails    = [];
		/*if($trucker){
			array_push($emails,$trucker->contact['email']);
		}*/
		//if($shipper) array_push($emails,$shipper->contact['email']);
		
		
		return  $this->render('buility',['shipment'=>$shipment]);
		/*$subject = 'Cargowala Order Invoice';
		    Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['no-reply'])
                ->setTo($emails)
                ->setSubject($subject)
                // ->setTextBody('Plain text content')
                ->setHtmlBody($html)
                ->send();
		Yii::$app->session->setFlash('error','Invoice emailed successfully to shipper trucker');
		$this->redirect(['view','id'=>Yii::$app->request->get('id')]);*/
	}
	
}
