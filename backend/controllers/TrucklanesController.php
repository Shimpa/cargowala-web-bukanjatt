<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\TruckLanes;
use common\models\TruckLanesSearch;
use common\models\TaxTerms;
use common\models\AdminSettings;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
* TrucklanesController implements the CRUD actions for TruckLanes model.
*/
class TrucklanesController extends Controller{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'uploadcsv', 'farecalculation', 'getlanedata', 'getterms', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all TruckLanes models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new TruckLanesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single TruckLanes model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new TruckLanes model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new TruckLanes();
        $model->scenario = TruckLanes::SCENARIO_CREATE;
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Creates a new TruckLanes model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionUploadcsv(){
        $model = new TruckLanes();
        $model->scenario = TruckLanes::SCENARIO_CSV_UPLOAD;
        if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'csvfile');
            if(!empty($file->tempName)){
                $columns  = ['source','source_state','destination','destination_state','distance','price_per_km','price_per_ton'];
                $suffix   = ['created_by'=>Yii::$app->user->getId(), 'status'=>1, 'created_on'=>Common::currentTimeStamp()]; 
                $csv = array_map(function($arr) use($columns,$suffix){
                    return array_combine($columns,str_getcsv($arr))+$suffix;
                },file($file->tempName));
                array_shift($csv);               
                Yii::$app->mongodb->getCollection('truck_lanes')->batchInsert($csv);    
            }
            return $this->redirect(['index']);
        }
        return $this->render('uploadcsv',['model'=>$model]);
    }

    /**
    * Calculate fare from two points according to the vehicle.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionFarecalculation_prevoius(){
        $model = new TruckLanes();
        $model->scenario = TruckLanes::SCENARIO_CALCULATION;
        if($model->load(Yii::$app->request->post())){
			echo '<pre>'; print_r($_POST); die('here');
            //II. Calculation (truck ton X current ton rate + distance X per KM rate + other charges)
            $subTotal = $_POST['TruckLanes']['loading_capacity'] * $_POST['TruckLanes']['price_per_ton'] + $_POST['TruckLanes']['distance'] * $_POST['TruckLanes']['price_per_km'];
            $charges = json_decode($_POST['TruckLanes']['charges']);
            $details = [];
            if($charges){
                foreach($charges as $charge){
                    if($charge->type == 'Fixed'){
                        $total = $charge->rate;
                    }else{
                        $total = $subTotal * $charge->rate / 100;
                    }
                    $details[] = [
                        'surchargeType' => $charge->type,
                        'surchargeTerm' => $charge->term,
                        'surchargeName' => $charge->name,
                        'surchargeRate' => $charge->rate,
                        'subTotal'      => $subTotal,
                        'surcharge'     => $total,
                        'totalAmount'   => $subTotal + $total,
                        'message'       => 'Total Amount is '.$subTotal + $total,
                    ];
                }
            }else{
                $details[] = [
                    'message' => 'Please choose different term type, your choosen term type is either inactive or not found in the records !',
                ];                
            }
            die(json_encode($details));
        }
        return $this->render('farecalculation',['model'=>$model]);
    }

    /**
    * Calculate fare from two points according to the vehicle.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionFarecalculation(){
        $model = new TruckLanes();
        $model->scenario = TruckLanes::SCENARIO_CALCULATION;
        if($model->load(Yii::$app->request->post())){
			$totaltax = [];
			$lane_rate = $_POST['TruckLanes']['today_price'];
			$adminsettings = AdminSettings::find()->orderBy('created_on')->one();
			if(empty($lane_rate))	$lane_rate = $adminsettings->lane_rate;
			$sub_total = number_format((float)$_POST['TruckLanes']['weight'] * $lane_rate * ceil($_POST['TruckLanes']['distance']), 2, '.', '');
			$priority_delivery_charge = number_format((float)$sub_total * $adminsettings->priority_delivery_rate / 100, 2, '.', '');
			$note = '<span style="color:red,">Note : If priority delivery option is choosen then '.$adminsettings->priority_delivery_rate.' % of '.$sub_total.' i.e '.$priority_delivery_charge.' will be charged extra.</span>';
			$overall_total = $sub_total;
			if(isset($_POST['taxes']))	$taxes = TaxTerms::find()->where(['status' => 1])->orderBy('priority')->all();
			if(!empty($taxes) && ($sub_total>0)){
				$count = 0;
				foreach($taxes as $tax){
					if($tax->ttype == 'Percentage'){
						$totaltax[] = [
							'name' =>  $tax->name. ' @ rate of '.$tax->rate. ' %',
							'rate' => number_format((float)$sub_total * $tax->rate/100, 2, '.', ''),
						];
						$overall_total +=((float)$sub_total * $tax->rate/100);
					}else{
						$totaltax[] = [
							'name' =>  $tax->name.' - '.$tax->rate.' fixed amount',
							'rate' => number_format((float)$tax->rate, 2, '.', ''),
						];
						$overall_total += (float)$tax->rate;
					}
					$count++;
				}
			}
			$total['sub_total'] =number_format((float)$sub_total, 2, '.', '');
			$total['overall_total'] =number_format((float)$overall_total, 2, '.', '');
			$total['taxes'] = $totaltax;
			$total['note_text'] = $note;
			die(json_encode($total));
		}
        return $this->render('farecalculation',['model'=>$model]);
    }

    /**
    * Updates an existing TruckLanes model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->scenario = TruckLanes::SCENARIO_UPDATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
           // echo '<pre>'; print_r($model->getErrors()); echo '</pre>';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing TruckLanes model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Finds the TruckLanes model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return TruckLanes the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = TruckLanes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionGetlanedata(){
        $type = Yii::$app->request->post('type');
        $res = TruckLanes::find()->where(['_id' => $type, 'status' => 1])->one();
        die(json_encode($res->attributes));
    }
    
    public function actionGetterms(){
        $type = Yii::$app->request->post('type');
        $res = TaxTerms::find()->where(['term' => $type, 'status' => 1])->orderBy('priority ASC')->all();
        if($type == 2) $res = TaxTerms::find()->where(['status' => 1])->orderBy('priority ASC')->all();
        $data = [];
        foreach($res as $key => $val){
            $data[] = [
                'term' => $val->term,
                'name' => $val->name,
                'rate' => $val->rate,
                'type' => $val->type,
                'priority' => $val->priority
            ];
        }
        die(json_encode($data));
    }

	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active') $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }
	
}