<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Vehicles;
use common\models\VehiclesSearch;
use common\models\Common;
use common\models\Trucks;
use common\models\Queue;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
/**
 * VehiclesController implements the CRUD actions for Vehicles model.
 */
class VehiclesController extends Controller{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'gettrucks', 'gettruckload', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all Vehicles models.
    * @return mixed
    */
	public function actionIndex(){
		$searchModel = new VehiclesSearch();
		$params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
		// echo '<pre>'; print_r($dataProvider); echo '</pre>';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
    /**
    * Displays a single Vehicles model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vehicles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Vehicles();
        $model->scenario = Vehicles::SCENARIO_ADMIN_CREATE;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
			$model->truck_status = 0;
			$model->is_available = 1;
            $model->save(false);
            $this->uploadImage($model, true);
			$notificationModel = Vehicles::find()->where( ['_id' => new \MongoId($model->_id), 'owner_id' => new \MongoId($model->owner_id) ])->with('owner')->one();
			// $name = empty($notificationModel['owner']->firstname) ? '' : $notificationModel['owner']->firstname.' '.empty($notificationModel['owner']->lastname) ? '' : $notificationModel['owner']->lastname;
			$name = $notificationModel['owner']->firstname.' '.$notificationModel['owner']->lastname;
			$subject = "CargoWala ! New vehicle with registration number ".$model->registration['vehicle_number']." is added under your account !";
			$message = "Hello ".$name.",\nA new Vehicle has been added successfully under your account with registration number ".$model->registration['vehicle_number']." on ".date('j M Y').". You can now manage the same under My Vehicles section on your mobile/ web app. For queries/support contact us at Helpline Number ".Common::getAdmin_contact();
			if(!empty($notificationModel['owner']->email)) Common::saveNotification($notificationModel['owner']->email, $subject, $message, 'email');
			if(!empty($notificationModel['owner']->mobile_number)) Common::saveNotification($notificationModel['owner']->mobile_number, $subject, $message, 'mobile');
			if(!empty($notificationModel['owner']['device']['token'])) Common::saveNotification($notificationModel['owner']['device']['token'], $subject, $message, 'push');				
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            // echo '<pre>'; print_r($model->getErrors()); echo '</pre>';
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Vehicles model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->scenario = Vehicles::SCENARIO_ADMIN_UPDATE;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $this->uploadImage($model, false); // call to upload function.
            $model->save();
			$notificationModel = Vehicles::find()->where( ['_id' => new \MongoId($model->_id), 'owner_id' => new \MongoId($model->owner_id) ])->with('owner')->one();
			$name = $notificationModel['owner']->firstname.' '.$notificationModel['owner']->lastname;
			// echo $name = empty($notificationModel['owner']->firstname) ? '' : $notificationModel['owner']->firstname.' '.empty($notificationModel['owner']->lastname) ? '' : $notificationModel['owner']->lastname;
			$subject = "CargoWala ! Vehicle details with registration number ".$model->registration['vehicle_number']." are updated under your account !";
			$message = "Hello ".$name.",\nVehicle details with registration number ".$model->registration['vehicle_number']." has been updated successfully under your account on ".date('j M Y').". You can now manage the same under My Vehicles section on your mobile/ web app. For queries/support contact us at Helpline Number ".Common::getAdmin_contact();
			if(!empty($notificationModel['owner']->email)) Common::saveNotification($notificationModel['owner']->email, $subject, $message, 'email');
			if(!empty($notificationModel['owner']->mobile_number)) Common::saveNotification($notificationModel['owner']->mobile_number, $subject, $message, 'mobile');
			if(!empty($notificationModel['owner']['device']['token'])) Common::saveNotification($notificationModel['owner']['device']['token'], $subject, $message, 'push');			
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Deletes an existing Drivers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
		return $this->redirect(['index']); // commented functionality for now
        $record = $this->findModel($id);
        $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            Common::DelAllMedia($path);
        $record->delete();
    }	
    /**
    * Finds the Vehicles model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Vehicles the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Vehicles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false){
		$path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = '';
        if(!empty($_FILES['Vehicles']['name'])){
			// echo '<pre>'; print_r($_FILES['Vehicles']['name']); die;
            foreach($_FILES['Vehicles']['name'] as $key => $val){
                $prefix = 'registration';
                if($key == 'pic'){
                    $prefix = 'permit';
                }else if($key == 'image'){
                    $prefix = 'insurance';
                }
                if(!empty($val))
                    $image = UploadedFile::getInstance($model, $key);
                if(!empty($image)){
                    if(!$stat){
                        $filename = $model->oldAttributes[$prefix][$key];
                        // if($image) Common::DelFile($path.DS.$filename);
                    }
                    $model->$prefix  = $prefix.'_'.$key.".".$image->extension;
					$filename = $model->$prefix;
                    $image->saveAs($path.DS.$filename);
                    // Common::SiteStardardImageSizesResize($path.DS.$model->image);
                    $model->$prefix = [$key => empty($filename) ? NULL : $filename];
					$model->$key = $filename;
                    if($stat) $model->save(false);
                }elseif(empty($image) && !$stat){ //if images are not Changed/Uploaded again
                    if(!empty($model->oldAttributes[$prefix][$key])) $filename = $model->oldAttributes[$prefix][$key];
                    $model->$prefix = [$key => empty($filename) ? NULL : $filename];
					$model->$key = $filename;
                    $model->save(false);
                }
            }
        }
		// echo '<pre>'; print_r($model); die('upload');
        return true;
    }
    
    public function actionGettrucks(){
        $type  = Yii::$app->request->post('type');
        $truck = Yii::$app->request->post('truck');
        $res = Trucks::find()->where(['truck_type' => $type, 'status' => 1])->orderBy('loading_capacity ASC')->all();
        $data = '';
        $data .= "<option value = ''>Select Truck</option>";
        foreach($res as $key => $val){
            $id = $val->_id->__toString();
            $name = $val->name;
			$selected = $truck==$id?"selected='selected'":null;
            $data .= "<option {$selected} value= ".$id.">".$name."   ( ".$val->loading_capacity." ton ) </option>";
        }
        die($data);
        //Common::encodeJSON($data,false);
    }
    
    public function actionGettruckload(){
        $type = Yii::$app->request->post('type');
        $res = Trucks::find()->where(['_id' => $type])->one();
        die($res->attributes['loading_capacity']);
        //Common::encodeJSON($res->attributes['loading_capacity'], false);
    }
    
    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus_(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
		$model = $model->load($model->oldAttributes);
		echo '<pre>'; print_r($model); die('changestatus');
        if($model->status)
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }
	
	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
		$model->truck_category = $model->oldAttributes['truck_category'];
		$model->truck_type = $model->oldAttributes['truck_type'];
		$model->truck_status = $model->oldAttributes['truck_status'];
		$model->is_available = $model->oldAttributes['is_available'];
        return $model->save(false);
    }	
}