<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Users;
use common\models\UsersSearch;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ShipperController implements the CRUD actions for Users model.
 */
class ShipperController extends Controller{
    
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus', 'changecp'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
    * Lists all Users models.
    * @return mixed
    */
    public function actionIndex(){
		$searchModel = new UsersSearch();
		$params = Yii::$app->request->queryParams;
		if(empty($params['UsersSearch']['role'] ))
		   $params['UsersSearch']['role'] = [21,22]; // to fetch Shipper users;
		// $params['UsersSearch']['status'] = [0,1];
        $dataProvider = $searchModel->search($params);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('created_on DESC'); // Sorting on the bases of creation date
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Users model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView_($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
    public function actionView($id){
		$model= $this->findModel($id);
	    $model->scenario = Users::SCENARIO_TRUCKER_VIEW;
        return $this->render('view', [
            'model' =>$model,
        ]);
    }

    /**
    * Creates a new Users model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
		return $this->redirect(['index']); // returns to the index function
        $model = new Users();
        $compArr = [];
        $model->scenario = Users::SCENARIO_CREATE;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            //$model->status = 1;
            $model->role = 2;
            $model->business_name = empty($_POST['Users']['business_name']) ? '' : $_POST['Users']['business_name'];
            $model->registered_address = empty($_POST['Users']['registered_address']) ? '' : $_POST['Users']['registered_address'];
            $model->office_address = empty($_POST['Users']['office_address']) ? '' : $_POST['Users']['office_address'];
            $model->landline_number = empty($_POST['Users']['landline']) ? '' : $_POST['Users']['landline'];
            $model->pancard = empty($_POST['Users']['pancard']) ? '' : $_POST['Users']['pancard'];
            $model->save();
            $model->sendNotificationMail( $this, 'Shipper', $_POST['Users']['password'] );
            $this->uploadImage($model, true);
            if(empty($model->business['name'])){
                $compArr[] = 'business_name';
            }
            if(empty($model->business['registered_address'])){
                $compArr[] = 'business_registered_address';
            }
            if(empty($model->business['office_address'])){
                $compArr[] = 'business_office_address';
            }
            if(empty($model->business['landline'])){
                $compArr[] = 'business_landline_number';
            }
            if(empty($model->pancard)){
                $compArr[] = 'pancard';
            }
            if(empty($model->image)){
                $compArr[] = 'image';
            }
            $compliance = Common::setCommonCompliance($compArr);
            $model->compliance = $compliance;
            $model->save(false);
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Users model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
		$model = $this->findModel($id);
        $compArr = [];
        $model->scenario = Users::SCENARIO_UPDATE;
        $oldPassword = $model->password; //Capture the old password.
        $model->password = '';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->uploadImage($model, false); // call to upload function.
			if(isset($_POST['Users']['role'])) $model->role = $model->account_type = $_POST['Users']['role'];
            $model->business_name = empty($_POST['Users']['business_name']) ? '' : $_POST['Users']['business_name'];
            $model->registered_address = empty($_POST['Users']['registered_address']) ? '' : $_POST['Users']['registered_address'];
            $model->registered_city = empty($_POST['Users']['registered_city']) ? '' : $_POST['Users']['registered_city'];
            $model->registered_state = empty($_POST['Users']['registered_state']) ? '' : $_POST['Users']['registered_state'];
            $model->registered_pincode = empty($_POST['Users']['registered_pincode']) ? '' : $_POST['Users']['registered_pincode'];
            $model->office_address = empty($_POST['Users']['office_address']) ? '' : $_POST['Users']['office_address'];
            $model->office_city = empty($_POST['Users']['office_city']) ? '' : $_POST['Users']['office_city'];
            $model->office_state = empty($_POST['Users']['office_state']) ? '' : $_POST['Users']['office_state'];
            $model->office_pincode = empty($_POST['Users']['office_pincode']) ? '' : $_POST['Users']['office_pincode'];
            $model->landline = empty($_POST['Users']['landline']) ? '' : $_POST['Users']['landline'];
            $model->pancard = empty($_POST['Users']['pancard']) ? '' : $_POST['Users']['pancard'];
            $model->b_type = empty($_POST['Users']['business_type']) ? '' : $_POST['Users']['business_type'];
            $model->c_type = empty($_POST['Users']['company_type']) ? '' : $_POST['Users']['company_type'];
	
          /* $model->save();
          if(empty($model->business['name'])){
                $compArr[] = 'business_name';
            }
            if(empty($model->business['registered_address'])){
                $compArr[] = 'business_registered_address';
            }
            if(empty($model->business['office_address'])){
                $compArr[] = 'business_office_address';
            }
            if(empty($model->business['landline'])){
                $compArr[] = 'business_landline_number';
            }
            if(empty($model->pancard)){
                $compArr[] = 'pancard';
            }
            if(empty($model->image)){
                $compArr[] = 'image';
            }
            $compliance = Common::setCommonCompliance($compArr);
            $model->compliance = $compliance; */
            $model->save(false);            
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }

    /**
    * Deletes an existing Users model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete_previous($id){
        $record = $this->findModel($id);
        if(!empty($record->image)){
            $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            $oldImage = $record->oldAttributes['image'];
            Common::DelFile($path.DS.$oldImage);
        }
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionDelete($id){
        return $this->redirect(['index']); // returns to the index function
		$model = $this->findModel($id);
        $model->status = 0; // Deleted
		if(isset($model->has_cp))
		$model->has_cp = $model->oldAttributes['has_cp'];
        $model->save(false);
    }

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
		$model->has_cp = $model->oldAttributes['has_cp'];
        return $model->save(false);
    }

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangecp(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->has_cp == 'Yes')
            $model->has_cp = 0;
        else $model->has_cp = 1;
		$model->status = $model->oldAttributes['status'];
        return $model->save(false);
    }

    /**
    * Finds the Users model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Users the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if(($model = Users::findOne($id)) !== null){
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, 'image');
        if(!empty($image)){
            if(!$stat){
                $oldImage = empty($model->oldAttributes['image']) ? '' : $model->oldAttributes['image'];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->image  = 'shipper_dp'.".".$image->extension; //die('as');
            $image->saveAs($path.DS.$model->image);
            // Common::SiteStardardImageSizesResize($path.DS.$model->image);
            //echo '<pre>'; print_r($model); die;
            $model->save(false); return true;
        }elseif(!$image && !$stat) $model->image = empty($model->oldAttributes['image']) ? '' : $model->oldAttributes['image'] ; return true;
    }
    
}