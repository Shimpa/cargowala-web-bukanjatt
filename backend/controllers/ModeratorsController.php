<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Common;
use common\models\Moderators;
use common\models\ModeratorsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
* ModeratorsController implements the CRUD actions for Moderators model.
*/

class ModeratorsController extends Controller{
   
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all Moderators models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new ModeratorsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Moderators model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new Moderators model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new Moderators();
        $model->scenario = Moderators::SCENARIO_CREATE;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->isAdmin = 0;
            $model->save();
            $this->uploadImage($model, true);
            $model->sendNotificationMail( $this, 'Moderators', $_POST['Moderators']['password'] );
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Moderators model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        //$model->scenario = 'update';
        $model->scenario = Moderators::SCENARIO_UPDATE;
        $oldPassword = $model->password;//Capture the old password.
        $model->password = '';		
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            $this->uploadImage($model, false); // call to upload function.
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing Moderators model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Finds the Moderators model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Moderators the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Moderators::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, 'image');
        if(!empty($image)){
            if(!$stat){
                $oldImage = empty($model->oldAttributes['image']) ? '' : $model->oldAttributes['image'];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->image  = 'moderator_dp'.".".$image->extension; //die('as');
            $image->saveAs($path.DS.$model->image);
            // Common::SiteStardardImageSizesResize($path.DS.$model->image);
            $model->save(false); return true;
        }elseif(!$image && !$stat) $model->image = empty($model->oldAttributes['image']) ? '' : $model->oldAttributes['image'] ; return true;
    }
	
    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }
	
}