<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Users;
use common\models\UsersSearch;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ShipperController implements the CRUD actions for Users model.
 */
class ShipperController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
    * Lists all Users models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Users model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new Users model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new Users();
        $model->scenario = Users::SCENARIO_CREATE;
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            $this->uploadImage($model, true);        
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Users model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->scenario = Users::SCENARIO_UPDATE;
        $oldPassword = $model->password;//Capture the old password.
        $model->password = '';        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->uploadImage($model, false); // call to upload function.
            $model->save();        
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing Users model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        $record = $this->findModel($id);
        if(!empty($record->image)){
            $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            $oldImage = $record->oldAttributes['image'];
            Common::DelFile($path.DS.$oldImage);
        }
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Finds the Users model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Users the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, 'image');
        if(!empty($image)){
            if(!$stat){
                $oldImage = $model->oldAttributes['image'];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->image  = 'shipper_dp'.".".$image->extension;
            $image->saveAs($path.DS.$model->image);
            // Common::SiteStardardImageSizesResize($path.DS.$model->image);
            if($stat) $model->save(); return true;
        }elseif(!$image && !$stat) $model->image = $model->oldAttributes['image']; return true;
    }
    
}