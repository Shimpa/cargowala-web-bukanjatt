<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Vehicles;
use common\models\VehiclesSearch;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * VehiclesController implements the CRUD actions for Vehicles model.
 */
class VehiclesController extends Controller{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vehicles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VehiclesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vehicles model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vehicles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Vehicles();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
            if(!empty($_FILES['Vehicles']['name'])){
                foreach($_FILES['Vehicles']['name'] as $key => $val){
                    $this->uploadImage($model, true , $key); // call to upload function.
                }
            }
           // echo '<pre>'; print_r($_POST); print_r($_FILES); echo '</pre>'; die('create');
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }        
    }

    /**
     * Updates an existing Vehicles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vehicles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Vehicles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Vehicles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if (($model = Vehicles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image of volumn
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $stat=false, $type){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        //$innerPath  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id."-".$type, 0755, true);
        $image = UploadedFile::getInstance($model, $type);
        if($image){
            if(!$stat){
                $oldImage = $model->oldAttributes[$type];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->$type  = $image->baseName .".".$image->extension;          
            $image->saveAs($path.DS.$model->$type);
            // Common::SiteStardardImageSizesResize($path.DS.$model->image);
            if($stat) $model->save(); return true;
        }elseif(!$image && !$stat) $model->$type =$model->oldAttributes[$type]; return true;
    }    
}
