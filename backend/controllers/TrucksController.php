<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Trucks;
use common\models\TrucksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
/**
* TrucksController implements the CRUD actions for Trucks model.
*/
class TrucksController extends Controller{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all Trucks models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new TrucksSearch();
		$params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
		// echo '<pre>'; print_r($dataProvider); 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Trucks model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new Trucks model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new Trucks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Trucks model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing Trucks model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
		return $this->redirect(['index']); // returns to the index function
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Finds the Trucks model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Trucks the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Trucks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
		
	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
		$model->oldAttributes['truck_type'];
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
		$model->truck_type = $model->oldAttributes['truck_type'];
        return $model->save(false);
    }
	
}