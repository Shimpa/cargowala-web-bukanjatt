<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Faq;
use common\models\FaqSearch;
use common\models\CmsPages;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
* MembershipController implements the CRUD actions for Faqs model.
*/
class FaqController extends Controller{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

	/**
	* Lists all Faq models.
	* @return mixed
	*/
	public function actionIndex(){
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
		// echo '<pre>'; print_r($dataProvider); echo '</pre>';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	* Displays a single Faq model.
	* @param integer $_id
	* @return mixed
	*/
	public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
			'languages' => CmsPages::getLanguages(),
        ]);
    }

	/**
	* Creates a new Faq model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	* @return mixed
	*/
    public function actionCreate(){
        $model = new Faq();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
				'languages' => CmsPages::getLanguages(),
            ]);
        }
    }

	/**
	* Updates an existing Faq model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $_id
	* @return mixed
	*/
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
				'languages' => CmsPages::getLanguages(),				
            ]);
        }
    }

	/**
	* Deletes an existing Faq model.
	* If deletion is successful, the browser will be redirected to the 'index' page.
	* @param integer $_id
	* @return mixed
	*/
	public function actionDelete($id){
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	/**
	* Finds the Faq model based on its primary key value.
	* If the model is not found, a 404 HTTP exception will be thrown.
	* @param integer $_id
	* @return Faq the loaded model
	* @throws NotFoundHttpException if the model cannot be found
	*/
    protected function findModel($id) {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }
	
	
}
