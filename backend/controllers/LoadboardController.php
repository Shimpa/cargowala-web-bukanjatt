<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Bookings;
use common\models\Bids;
use common\models\Common;
use common\models\Users;
use common\models\BookingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LoadboardController implements the CRUD actions for Bookings model.
 */
class LoadboardController extends Controller{
	
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus','b_ds__','b_rjc_','postnbid','isbidostedb4','changeshipmenttype'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'b_ds__' => ['post'],
                    'b_rjc_' => ['post'],
                    'postnbid' => ['post'],
                    'isbidostedb4' => ['post'],
                ],
            ],
        ];
    }	

    /**
     * Lists all Bookings models.
     * @return mixed
     */
    public function actionIndex(){
        $searchModel = new BookingsSearch();
		$title = "All Loads";
		$params = Yii::$app->request->queryParams;
		$title = "All Orders";
		$params = Yii::$app->request->queryParams;
		$type = empty($_REQUEST['type'])?null:$_REQUEST['type'];
		$view = 'index';
		switch($type){	
			case "bidboard":
				$params['BookingsSearch']['shipment_type'] = [5];
				//$params['BookingsSearch']['status'] = [0,6];
			    $title = "Bid Load Board Orders";
				
			break;
			case "regular":
				$params['BookingsSearch']['shipment_type'] = [1];
				//$params['BookingsSearch']['status'] = [0,6];
			    $title = "Regular Load Board Orders";
				$view = 'index-regular';
			break;			
			default:
				$params['BookingsSearch']['shipment_type'] = [5,1];
			    $title = "All Orders";
			break;	
				
		}

        $dataProvider = $searchModel->search($params);
	
        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => $title,
        ]);
    }	

    /**
     * Displays a single Bookings model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('../bookings/view', [
            'model' => $this->findModel($id),
			'regular_board'=>true,
        ]);
    }

    /**
     * Creates a new Bookings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Bookings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Bookings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Bookings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

	/**
	* Finds the Bookings model based on its primary key value.
	* If the model is not found, a 404 HTTP exception will be thrown.
	* @param integer $_id
	* @return Bookings the loaded model
	* @throws NotFoundHttpException if the model cannot be found
	*/
	protected function findModel($id){
		$model = Bookings::find()->where(['_id'=>$id])->with(['username','bids'])->one();
		if ($model !== null) return $model;
		throw new NotFoundHttpException('The requested page does not exist.');
	}
	/* load bids by shipments*/
	public function actionB_ds__(){
		$bids  = Bids::find()->select(['price','trucker_id','description','status'])
			                 ->with(['user'=>function ($query) {
								    $query->select(['name','contact.mobile_number']);
								    }])
			                 ->where([
			                          'shipment_id'=>new\MongoId(Yii::$app->request->post('_s_')),
								      'status'=>Bids::POSTED
		                            ])->all();
		$data = [];
		foreach($bids as $bid){
			$data[] = [
				       '_id'=>(string)$bid->_id, 
				       'price'  => $bid->price,
				       'trucker'=> $bid->user['name']['firstname'] . " " . $bid->user['name']['lastname'], 
				       'contact_no'  =>$bid->user['contact']['mobile_number'],
				       'status'      =>Bids::getStatusText($bid->status),
				       'description' =>$bid->description, 
			          ];   
		}
		Common::encodeJSON(['data'=>$data,'status'=>Common::HTTP_SUCCESS],false);
		
		
	}
	/* reject bid posted by trucker by id */
	public function actionB_rjc_(){
		$bid =Bids::find()->with([
								  'user'=>function($query){
			                                $query->select(['name','contact','device']);
		                           },
								  'shipment'=>function($query){
									        $query->select([
												'loading.lp_city',
												'unloading.up_city',
												'shipper_id',
												'shipment_id',
												'transit.t_date',
												'transit.t_time',					
								            ]);
								  }])->where([
			'_id'=>new \MongoId(Yii::$app->request->post('_bd___'))])->one();
		$bid->status =Bids::REJECTED;
		$bid->save();
		$trucker  = $bid->user;
		$shipment = $bid->shipment;
		$shipper  = Users::findOne(['_id'=>new \MongoId($shipment->shipper_id)]); 
		
		// shipper message
		$truckersubject = Common::getLocalMessage('subjecttrucker_bid_rejected',[],'bids');
		$truckermessage = Common::getLocalMessage('notifytrucker_bid_rejected',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<order_ref_number>>'=>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bids');
		// trucker message
		$shippersubject = Common::getLocalMessage('notify_shipper_bidrejected',[],'bids');
		$shippermessage = Common::getLocalMessage('notifyshipper_bid_rejected',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<shipper_name>>'=>$shipper->getName(),
			'<<order_ref_number>>'=>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bids');
		
		
		//notify trucker...
		if(!empty($trucker->contact['email']))
		   Common::saveNotification($trucker->email, $truckersubject, $truckermessage, 'email');
		if(!empty($trucker->contact['mobile_number']))
		   Common::saveNotification($trucker->contact['mobile_number'], $truckersubject, $truckermessage, 'mobile');
		if(!empty($trucker->device['token']))
		   Common::saveNotification($trucker->device['token'], $truckersubject, $truckermessage, 'push');
		// norify sms push email to shipper
		if(!empty($shipper->contact['email']))
		   Common::saveNotification($shipper->email, $shippersubject, $shippermessage, 'email');
		if(!empty($shipper->contact['mobile_number']))
		   Common::saveNotification($shipper->contact['mobile_number'], $shippersubject, $shippermessage, 'mobile');
		if(!empty($shipper->device['token']))
		   Common::saveNotification($shipper->device['token'], $shippersubject, $shippermessage, 'push');
		
		Common::encodeJSON(['status'=>Common::HTTP_SUCCESS],false);
	}
	/* post new bid on shipment */
	public function actionPostnbid(){
		$bid = new Bids();
		$data= Yii::$app->request->post('Bids');
		if(!empty($data['_id'])){// if bid id found update bid...
			$bid = Bids::findOne(['_id'=>new \MongoId($data['_id'])]);
		}
		$data['status'] = Bids::POSTED;				
		$bid->load(['Bids'=>$data]);
		if($bid->save()==false){
			$errors = $bid->getErrors();
			$err=[];
		   foreach($errors as $key=>$error){
			   if(!empty($error[0]))
				   $err[] = $error[0];
		   } Common::encodeJSON(['status'=>Common::HTTP_BAD_REQUEST,'data'=>false,'errors'=>$err],false);
		}
		$shipment = $bid->shipment;
		$trucker  = $bid->user; 
		$shipper  = Users::findOne(['_id'=>$shipment->shipper_id]); 
		// shipper message
		$truckersubject = Common::getLocalMessage('notify_shipper_bidposted',[],'bids');
		$truckermessage = Common::getLocalMessage('notifytruker_bid_posted',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<order_ref_number>>'=>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bids');
		// trucker message
		$shippersubject = Common::getLocalMessage('notify_shipper_bidposted',[],'bids');
		$shippermessage = Common::getLocalMessage('notifyshipper_bid_posted',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<shipper_name>>'=>$shipper->getName(),
			'<<order_ref_number>>'=>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bids');
		
		
		//notify trucker...
		if(!empty($trucker->contact['email']))
		   Common::saveNotification($trucker->email, $truckersubject, $truckermessage, 'email');
		if(!empty($trucker->contact['mobile_number']))
		   Common::saveNotification($trucker->contact['mobile_number'], $truckersubject, $truckermessage, 'mobile');
		if(!empty($trucker->device['token']))
		   Common::saveNotification($trucker->device['token'], $truckersubject, $truckermessage, 'push');
		// norify sms push email to shipper
		if(!empty($shipper->contact['email']))
		   Common::saveNotification($shipper->email, $shippersubject, $shippermessage, 'email');
		if(!empty($shipper->contact['mobile_number']))
		   Common::saveNotification($shipper->contact['mobile_number'], $shippersubject, $shippermessage, 'mobile');
		if(!empty($shipper->device['token']))
		   Common::saveNotification($shipper->device['token'], $shippersubject, $shippermessage, 'push');
		
		Common::encodeJSON(['status'=>Common::HTTP_SUCCESS,'data'=>$data,'msg'=>['success'=>'Bid Posted Successfully']],false);
		
	}
	/* get bid data if bid posted by trucker on current shipment */
	public function actionIsbidostedb4(){
		$bid =Bids::find()->select(['price','description'])->where(['shipment_id'=>new \MongoId(Yii::$app->request->post('_sid___')),'trucker_id'=>new \MongoId(Yii::$app->request->post('_tid___'))])->asArray()->one();
		Common::encodeJSON(['status'=>!empty($bid),'data'=>$bid],false);
		
	}
	/* change regular load board shipment to bid load board*/
	public function actionChangeshipmenttype(){
		if(!Yii::$app->request->post('_sid___'))return;
		$booking = Bookings::findOne(['_id'=>new \MongoId(Yii::$app->request->post('_sid___'))]);
		if(!empty($booking->trucker_id)) return;
		$booking->shipment_type = [
			'code'=>Bookings::BID_BOARD,
			'text'=>'Bid Load Board',
		];
		$booking->save(false);
		
	    $shipper = $booking->shipper;
		$trucker = [];//$booking->getTruckers();
		
		
			// shipper message
		/*$truckersubject = Common::getLocalMessage('notify_shipper_shipbid2regular',[],'bids');
		$truckermessage = Common::getLocalMessage('notifytruker_bid_posted',[
			'<<trucker_name>>'=>$trucker->getName(),
			'<<order_ref_number>>'=>$shipment->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bids');*/
		// shipper message
		$shippersubject = Common::getLocalMessage('subject_shipper_shipbid2regular',[],'bookings');
		$shippermessage = Common::getLocalMessage('notify_shipper_shipbid2regular',[
			'<<shipper_name>>'=>$shipper->getName(),
			'<<order_id>>'=>$booking->shipment_id,
			'<<support_no>>'  =>Common::getSupportNo(),
			
		],'bookings');		
		
		
		//notify trucker...
		if($trucker && !empty($trucker->contact['email']))
		   Common::saveNotification($trucker->email, $truckersubject, $truckermessage, 'email');
		if($trucker && !empty($trucker->contact['mobile_number']))
		   Common::saveNotification($trucker->contact['mobile_number'], $truckersubject, $truckermessage, 'mobile');
		if($trucker && !empty($trucker->device['token']))
		   Common::saveNotification($trucker->device['token'], $truckersubject, $truckermessage, 'push');
		// norify sms push email to shipper
		if($shipper && !empty($shipper->contact['email']))
		   Common::saveNotification($shipper->email, $shippersubject, $shippermessage, 'email');
		if($shipper && !empty($shipper->contact['mobile_number']))
		   Common::saveNotification($shipper->contact['mobile_number'], $shippersubject, $shippermessage, 'mobile');
		if($shipper &&!empty($shipper->device['token']))
		   Common::saveNotification($shipper->device['token'], $shippersubject, $shippermessage, 'push');
		
		Common::encodeJSON(['status'=>Common::HTTP_SUCCESS],false);
	}

}
