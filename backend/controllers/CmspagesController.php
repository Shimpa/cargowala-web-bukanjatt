<?php

namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\CmsPages;
use common\models\CmsPagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
* CmspagesController implements the CRUD actions for CmsPages model.
*/
class CmspagesController extends Controller{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all CmsPages models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new CmsPagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('created_on DESC'); // Sorting on the bases of creation date
// echo '<pre>'; print_r($dataProvider); die('index');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single CmsPages model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
             'languages' => CmsPages::getLanguages(),
        ]);
    }

    /**
    * Creates a new CmsPages model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new CmsPages();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'languages' => CmsPages::getLanguages(),
            ]);
        }
    }

    /**
    * Updates an existing CmsPages model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'languages' => CmsPages::getLanguages(),
            ]);
        }
    }

    /**
    * Deletes an existing CmsPages model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
    * Finds the CmsPages model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return CmsPages the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = CmsPages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }
	
}