<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Categories;
use common\models\Common;
use common\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
* CategoriesController implements the CRUD actions for Categories model.
*/
class CategoriesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Lists all Categories models.
    * @return mixed
    */
    public function actionIndex(){
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		// echo '<pre>'; print_r($dataProvider); die('categories');
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Displays a single Categories model.
    * @param integer $_id
    * @return mixed
    */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
    * Creates a new Categories model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate(){
        $model = new Categories();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $this->uploadImage($model, true); // call to upload function.
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Updates an existing Categories model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->uploadImage($model); // call to upload function.
            $model->save();
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Deletes an existing Categories model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionDelete($id){
        return $this->redirect(['index']);
        $record = $this->findModel($id);
        if(!empty($record->image)){
            $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            $oldImage = $record->oldAttributes['image'];
            Common::DelFile($path.DS.$oldImage);
        }
        $this->findModel($id)->delete();
    }

    /**
    * Finds the Categories model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Categories the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id){
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * upload image of volumn
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model,$stat=false){
        $path = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id,0755,true);
        $image  = UploadedFile::getInstance($model, 'image');
        if($image){
            if(!$stat){
                $oldImage = $model->oldAttributes['image'];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->image  = $image->baseName .".".$image->extension;          
            $image->saveAs($path.DS.$model->image);
            // Common::SiteStardardImageSizesResize($path.DS.$model->image);
            if($stat) $model->save(); return true;
        }elseif(!$image && !$stat) $model->image = $model->oldAttributes['image']; return true;
    }
	
	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }	
    
}