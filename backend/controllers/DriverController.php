<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Drivers;
use common\models\DriversSearch;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DriverController implements the CRUD actions for Drivers model.
 */
class DriverController extends Controller{
	
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	

    /**
     * Lists all Drivers models.
     * @return mixed
     */
    public function actionIndex_pre(){
        $searchModel = new DriversSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

   public function actionIndex(){
		$searchModel = new DriversSearch();
		$params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($params);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('_id DESC'); // Sorting on the bases of creation date
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
    /**
     * Displays a single Drivers model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Drivers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Drivers();
		$model->scenario = Drivers::SCENARIO_ADMINCREATE;
        if($model->load(Yii::$app->request->post())) {
			$model->password = md5($_POST['Drivers']['password']);
			$model->driver_status = 0;
			$model->is_available = 1;
			$model->save(false);
			$this->uploadImage($model,'image');
			$this->uploadImage($model,'licence_image');
			$this->uploadImage($model,'adhaar_image');
			$notificationModel = Drivers::find()->where( ['_id' => new \MongoId($model->_id), 'owner_id' => new \MongoId($model->owner_id) ])->with('owner')->one();
			// $name = empty($notificationModel['owner']->firstname) ? '' : $notificationModel['owner']->firstname.' '.empty($notificationModel['owner']->lastname) ? '' : $notificationModel['owner']->lastname;
			$name = $notificationModel['owner']->firstname.' '.$notificationModel['owner']->lastname;
			$subject = "CargoWala ! New driver with mobile number ".$model->mobile_no." is added under your account !";
			$message = "Hello ".$name.",\nA new Driver has been added successfully under your account with mobile number ".$model->mobile_no." on ".date('j M Y').". You can now manage the same under My Drivers section on your mobile/ web app. For queries/support contact us at Helpline Number ".Common::getAdmin_contact();
			if(!empty($notificationModel['owner']->email)) Common::saveNotification($notificationModel['owner']->email, $subject, $message, 'email');
			if(!empty($notificationModel['owner']->mobile_number)) Common::saveNotification($notificationModel['owner']->mobile_number, $subject, $message, 'mobile');
			if(!empty($notificationModel['owner']['device']['token'])) Common::saveNotification($notificationModel['owner']['device']['token'], $subject, $message, 'push');				
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Drivers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id){
        $model = $this->findModel($id);
		$model->scenario = Drivers::SCENARIO_ADMINUPDATE;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->uploadImage($model,'image');
			$this->uploadImage($model,'licence_image');
			$this->uploadImage($model,'adhaar_image');
			$notificationModel = Drivers::find()->where( ['_id' => new \MongoId($model->_id), 'owner_id' => new \MongoId($model->owner_id) ])->with('owner')->one();
			// $name = empty($notificationModel['owner']->firstname) ? '' : $notificationModel['owner']->firstname.' '.empty($notificationModel['owner']->lastname) ? '' : $notificationModel['owner']->lastname;
			$name = $notificationModel['owner']->firstname.' '.$notificationModel['owner']->lastname;			
			$subject = "CargoWala ! Driver details with mobile number ".$model->mobile_no." are updated under your account !";
			$message = "Hello ".$name.",\nDriver details with mobile number ".$model->mobile_no." has been updated successfully under your account on ".date('j M Y').". You can now manage the same under My Drivers section on your mobile/ web app. For queries/support contact us at Helpline Number ".Common::getAdmin_contact();
			if(!empty($notificationModel['owner']->email)) Common::saveNotification($notificationModel['owner']->email, $subject, $message, 'email');
			if(!empty($notificationModel['owner']->mobile_number)) Common::saveNotification($notificationModel['owner']->mobile_number, $subject, $message, 'mobile');
			if(!empty($notificationModel['owner']['device']['token'])) Common::saveNotification($notificationModel['owner']['device']['token'], $subject, $message, 'push');
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

	 /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model, $index){
        $path = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $index);
		$this->moveFiles($model);
        if(!empty($image)){
		   $filename = empty($model->$index) ? time() . "{$index}.".$image->extension : basename($model->$index);	
           $model->$index  = $filename;
           $image->saveAs($path.DS.$model->$index);
           $model->save(false);
	    }
	}
	
	private function moveFiles($model){ 
		$path     = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true)."/";
		$tmpath   = Common::Mediapath()."/tmp/";
		foreach(['image','licence_image'] as $image){
			if(file_exists($tmpath.$model->$image)){
				if(@copy($tmpath.$model->$image,$path.$model->$image))
				@unlink($tmpath.$model->$image);  
			}
		}
	}

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
		$model->load($model->oldAttributes);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }	
	
    /**
     * Deletes an existing Drivers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
		return $this->redirect(['index']); // commented functionality for now
        $record = $this->findModel($id);
        $path = Common::mkdir($record::PARENTDIR.$record::CHILDDIR."-".$record->_id,0755,true);
            Common::DelAllMedia($path);
        $record->delete();
    }	

    /**
     * Finds the Drivers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Drivers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Drivers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
