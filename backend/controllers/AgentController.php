<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Users;
use common\models\UsersSearch;
use common\models\Common;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TruckerController implements the CRUD actions for Users model.
 */
class AgentController extends Controller{

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus', 'changecp'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }	

    /**
    * Lists all Users models.
    * @return mixed
    */
    public function actionIndex(){
		$searchModel = new UsersSearch();
		$params = Yii::$app->request->queryParams;
		$params['UsersSearch']['role'] = [3]; // to fetch only Agents;
		// $params['UsersSearch']['status'] = [0,1];
        $dataProvider = $searchModel->search($params);
		if(empty(Yii::$app->request->queryParams['sort'])) $dataProvider->query->orderBy('created_on DESC'); // Sorting on the bases of creation date
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
		$model= $this->findModel($id);
	    $model->scenario = Users::SCENARIO_TRUCKER_VIEW;
        return $this->render('view', [
            'model' =>$model,
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		return $this->redirect(['index']); // returns to the index function
        $model = new Users();
        $model->scenario = Users::SCENARIO_TRUCKER_CREATE;
        if($model->load(Yii::$app->request->post())){
			// $model->role = $_POST['Users']['account_type'];
            $model->save(false);
			$this->uploadImage($model,'image', true);
			$this->uploadImage($model,'business_logo', true);
            return $this->redirect(['index']);
            // return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$model = $this->findModel($id);
		$model->scenario = Users::SCENARIO_AGENT_UPDATE;
		$oldPassword = $model->password; //Capture the old password.
		$model->password = '';
         if ($model->load(Yii::$app->request->post())) {
			 // $model->role = $_POST['Users']['account_type'];
            $model->save(false);
			$this->uploadImage($model,'image', true);
			$this->uploadImage($model,'business_logo', true);
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id){
		return $this->redirect(['index']); // returns to the index function
        $model = $this->findModel($id);
        $model->status = 0; // Deleted
		if(isset($model->has_cp))
		$model->has_cp = $model->oldAttributes['has_cp'];
        $model->save(false);
    }
	
    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
			// echo "<pre>"; print_r($model); exit;
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
        /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
		// die('ankur');
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
		//$model->has_cp = $model->oldAttributes['has_cp'];
        return $model->save(false);
    }

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangecp(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if($model->has_cp == 'Yes')
            $model->has_cp = 0;
        else $model->has_cp = 1;
		$model->status = $model->oldAttributes['status'];
        return $model->save(false);
    }	
	

	    /**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model,$colum, $stat=false){
        $path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
        $image = UploadedFile::getInstance($model, $colum);
        if(!empty($image)){
            if(!$stat){
                $oldImage = empty($model->oldAttributes[$colum]) ? '' : $model->oldAttributes[$colum];
                Common::DelFile($path.DS.$oldImage);
            }
            $model->$colum  = $colum.'_'.time().".".$image->extension; //die('as');
            $image->saveAs($path.DS.$model->$colum);
            $model->save(false); return true;
        }elseif(!$image && !$stat) $model->$colum = empty($model->oldAttributes[$colum]) ? '' : $model->oldAttributes[$colum] ; return true;
    }
}
