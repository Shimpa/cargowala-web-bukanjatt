<?php
namespace backend\controllers;

use Yii;
use yii\base\Behavior;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\CmsPages;
use common\models\Moderators;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'savedetails', 'forgotpassword', 'changestatus', 'changecp'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'dashboard'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
         return $this->render('dashboard');
    }

    public function actionLogin(){
		$this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm;
        $model->isAdmin = true;
        if($model->load(Yii::$app->request->post()) && $model->login()){
			return $this->redirect(['site/dashboard']);
        }else{
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
	
	public function actionDashboard(){
        //	print_r(Yii::$app->user);
        return $this->render('dashboard');
	}

    public function actionSavedetails(){
		$this->layout = 'login';
		$model = new CmsPages();
		$model->title = 'Home';
		$model->alias = 'home';
		$model->content = "Home page content Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
		$model->created_by = 2;
		$model->status = 1;
		$model->created_on = date('Y:m:d H:i:s');
		$model->modified_on = date('Y:m:d H:i:s');
		$model->save();
	}

    public function actionForgotpassword($email){
		$model = new Moderators();
        $model->save();
	}
    
	public function actionforgetpassword(){
		//$email= 'vivek.sood@trigma.co.in';
		$email= Yii::app()->request->getParam('emailid');
		$json = array();
		if(isset($email) && !empty($email)){
			$user_email_exists = Users::model()->findByAttributes(array("email"=>$email)); 			
			if(count($user_email_exists)>0){
				$token = md5(time());
				$user_email_exists->token = $token;
				if($user_email_exists->save(false)){
					$uniqueMail= $email;
					$from = 'admin@h2o.com';
					$subject = 'Reset password';
					$reporttxt = Yii::app()->request->getBaseUrl(true) .'/index.php?r=users/resetpassword&token ='.$token.'&id='.$user_email_exists->id;
					$message = "Hello User,<br/><br/>
					Please reset your password, click below link:-<br/>
					<a href='".$reporttxt."'>".$reporttxt."</a><br/><br/>
					Regards,<br/>
					Admin";
					Rahcommon::Obj()->SendMail($uniqueMail,$from,$message,$subject);
					$json = array(
						'result'=> 'true',
						'response'=> 'Reset password email has been sent to your email account.',
					);
				}else{
					$json = array(
						'result'=> 'false',
						'response'=> 'Internal Error(Db Error).',
					);
				}
			}else{
				$json = array(
					'result'=> 'false',
					'response'=> 'Email does not exists.',
				);
			}
		}else{
			$json = array(
				'result'=> 'false',
				'response'=> 'Pass the required parameters'					
			);			
		}
		echo json_encode($json);die();
	}
    
    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }
	
	/**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangestatus(){
        $dataModel  = Yii::$app->request->post('model');
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($dataModel, $id);
		echo '<pre>'; print_r($model); die('as');
        if($model->status == 'Active')
            $model->status = 0;
        else $model->status = 1;
        return $model->save(false);
    }

    /**
    * Change the Status 
    * @param integer $_id
    * @return true/false
    */
    public function actionChangecp(){
        $id  = Yii::$app->request->post('id');
        $model = $this->findModel($model, $id);
        if($model->has_cp == 'Yes')
            $model->has_cp = 0;
        else $model->has_cp = 1;
        $model->status = $model->status ;
        return $model->save(false);
    }
	
	/**
    * Finds the Users model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $_id
    * @return Users the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($dataModel, $id){
		die($dataModel);
		$model = new $dataModel();
        if(($result = $model::findOne($id)) !== null){
            return $result;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
}