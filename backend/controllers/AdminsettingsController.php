<?php
namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use common\models\Common;
use common\models\AdminSettings;
use common\models\AdminSettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * AdminsettingsController implements the CRUD actions for AdminSettings model.
 */
class AdminsettingsController extends Controller{
	
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'changestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	/**
	* Lists all AdminSettings models.
	* @return mixed
	*/
    public function actionIndex(){
        $searchModel = new AdminSettingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	* Displays a single AdminSettings model.
	* @param integer $_id
	* @return mixed
	*/
	public function actionView($id){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	/**
	* Creates a new AdminSettings model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	* @return mixed
	*/
    public function actionCreate(){
        $model = new AdminSettings();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
			$this->uploadImage($model, 'company_logo', false); // call to upload function.
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }	

    /**
    * Updates an existing AdminSettings model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $_id
    * @return mixed
    */
    public function actionUpdate($id){
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->save();
			$this->uploadImage($model, 'company_logo', true); // call to upload function.
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        }else{
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }	

	/**
	* Deletes an existing AdminSettings model.
	* If deletion is successful, the browser will be redirected to the 'index' page.
	* @param integer $_id
	* @return mixed
	*/
	public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

	/**
	* Finds the AdminSettings model based on its primary key value.
	* If the model is not found, a 404 HTTP exception will be thrown.
	* @param integer $_id
	* @return AdminSettings the loaded model
	* @throws NotFoundHttpException if the model cannot be found
	*/
	protected function findModel($id){
        if (($model = AdminSettings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	/**
    * upload image to the specified directory
    * @params $model Volumn Model , $stat must be boolean true=Create,false=Update
    **/ 
    private function uploadImage($model,$colum, $stat=false){
        $image = UploadedFile::getInstance($model, $colum);
        if(!empty($image)){
			$path  = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id, 0755, true);
			Common::DelAllMedia($path);
			$path = Common::mkdir($model::PARENTDIR.$model::CHILDDIR."-".$model->_id,0755,true);
            $model->$colum  = $colum.'_'.time().".".$image->extension;
            $image->saveAs($path.DS.$model->$colum);
            $model->save(false); return true;
        }elseif(!$image && !$stat) $model->$colum = empty($model->oldAttributes[$colum]) ? '' : $model->oldAttributes[$colum] ; return true;
    }
	
}