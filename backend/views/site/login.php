<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login | Guidance club';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php $form = ActiveForm::begin(['id' => 'signin-form_id','options'=>['class'=>'panel admin-login']]); ?>
          <div class="form-group has-feedback">
            <?= $form->field($model, 'username')->textInput(['class'=>'form-control','placeholder'=>'Email'])->label(false) ?>  
    
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control','placeholder'=>'Password'])->label(false) ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="LoginForm[rememberMe]"  > Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>

            </div><!-- /.col -->
          </div>
        <?php ActiveForm::end(); ?>



      </div><!-- /.login-box-body -->











