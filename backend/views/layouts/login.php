<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use backend\themes\Theme;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->

    <head>
        <?= $this->render('//layouts/loginhead') ?>
        <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->    
        
        
    </head>
  
<body class="hold-transition login-page">
    <?php $this->beginBody(); ?>
    <div class="login-box">
      <div class="login-logo">
        <a href="<?=Url::home()?>"><b>Guidance</b>Club</a>
      </div><!-- /.login-logo -->
     <?= $content ?> 
    </div><!-- /.login-box -->




</body>
<?php $this->endBody(); ?>

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</html>
 <?php $this->endPage(); ?>   
    