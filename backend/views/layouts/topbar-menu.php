<?php

/**
 * Topbar menu layout.
 *
 * @var \yii\web\View $this View 
 */

use backend\themes\admin\widgets\Menu;
use yii\helpers\Url;
echo Menu::widget(
    [
        'options' => [
            'class' => 'nav navbar-nav'
        ],
        'items' => [
            [
                'label' => Yii::t('app', 'Dashboard'),
                'url' => ['site/dashboard'],
                /*'icon' => 'fa-dashboard',*/
                'active' => Yii::$app->request->url === Url::to(['site/dashboard'])
            ],           
            [
                'label' => Yii::t('app', 'Users'),
                'url' => 'javascript:;',
                'options'=>['class'=>'dropdown'],
                'submenuClass'=>'dropdown-menu',
                'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels') || Yii::$app->user->can('BViewComments'),
                'items' => [
                    
                    [
                        'label' => Yii::t('app', 'Manage IP/IS'),
                        'url' => ['users/index'],
                        //'options'=>['class'=>'dropdown'],
                        //'submenuClass'=>'dropdown-menu',
                        //'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                        /*'items' => [
                    
                    [
                        'label' => Yii::t('app', 'Users'),
                        'url' => ['users/index'],
                        'options'=>['class'=>'dropdown'],
                        'submenuClass'=>'dropdown-menu',
                        'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                     ]
                    ]*/
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                  /*  [
                        'label' => Yii::t('app', 'Add/Edit Publishers'),
                        'url' => ['publishers/index'],
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                    [
                        'label' => Yii::t('app', 'Comments'),
                        'url' => ['/comment/index'],
                       // 'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels'),
                    ] */
                    
                   
                ]
              
            ],
         /*   [
                'label' => Yii::t('app', 'Masters'),
                'url' => 'javascript:;',
                'options'=>['class'=>'dropdown'],
                'submenuClass'=>'dropdown-menu',
                'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels') || Yii::$app->user->can('BViewComments'),
                'items' => [
                    
                    [
                        'label' => Yii::t('app', 'Manage Avatars'),
                        'url' => ['avatars/index'],
                        //'options'=>['class'=>'dropdown'],
                        //'submenuClass'=>'dropdown-menu',
                        //'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                        /*'items' => [
                    
                    [
                        'label' => Yii::t('app', 'Users'),
                        'url' => ['users/index'],
                        'options'=>['class'=>'dropdown'],
                        'submenuClass'=>'dropdown-menu',
                        'itemOptions'=>['class'=>'dropdown-toggle','data-toggle'=>'dropdown'],
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                     ]
                    ]
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                    [
                        'label' => Yii::t('app', 'Add/Edit Publishers'),
                        'url' => ['publishers/index'],
                        //'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                    [
                        'label' => Yii::t('app', 'Comments'),
                        'url' => ['/comment/index'],
                       // 'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels'),
                    ]
                    
                   
                ]
              
            ], */
   
           
        ]
    ]
);