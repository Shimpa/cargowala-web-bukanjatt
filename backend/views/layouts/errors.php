<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use web\themes\Theme;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="gt-ie8 gt-ie9 not-ie" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->

    <head>
        <?= $this->render('//layouts/loginhead') ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&amp;subset=latin" rel="stylesheet" type="text/css">
        <!--[if lt IE 9]>
		<script src="assets/javascripts/ie.min.js"></script>
	<![endif]-->
    </head>
    <body class="theme-default page-signin-alt">
    <?php $this->beginBody(); ?>
<script>var init = [];</script>
<!-- Demo script --> <script src="<?= Yii::$app->view->theme->baseUrl ?>/assets/demo/demo.js"></script> <!-- / Demo script -->

<div class="signin-header">
		<a href="<?= Yii::$app->homeUrl ?>" class="logo">
			<strong><?= Yii::$app->name ?></strong>
		</a> <!-- / .logo -->
		
	</div> <!-- / .header -->

	<h1 class="form-header">Sign in to your Account</h1> <!-- / #main-wrapper -->
         <?= $content ?> 
<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?= Yii::$app->view->theme->baseUrl ?>/assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->





</body>
<?php $this->endBody(); ?>
</html>
 <?php $this->endPage(); ?>   
    