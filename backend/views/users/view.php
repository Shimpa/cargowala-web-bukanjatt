<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = $model->name['firstname'];
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->menu = [
           [
           'label' => 'Operations',
           'url'   => 'javascript:;',
           'wrap'=>true,
           'icon'=>'',
           'active'=>true,
           
          ],
          [
           'label' => 'Create  Users',
           'url'   => ['create'],
           'wrap'=>true,
           'icon'=>'fa-plus',
           
          ],
          [
           'label' => 'Update  Users',
           'url'   => ['update','id' => (string)$model->_id],
           'wrap'=>true,
           'icon'=>'fa-edit',
            
          ],
          [
           'label' => 'Manage  Users',
           'url'   =>['index'],
           'wrap'=>true,
           'icon'=>'fa-list',
            
          ],
          [
           'label' => 'Delate  Users',
           'url'   => ['delete','id' => (string)$model->_id],
           'itemOptions'=>['data-confirm'=>"'Are you sure you want to delete this item?'",'data-method'=>'post'],
           'wrap'=>true,
           'icon'=>'fa-times',
            
          ],
];
?>
 <section class="content users-view">

          
          <div class="box box-default">
            <div class="box-header mtb10">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
              <div class="box-tools pull-right">
                  <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
                </div>
            </div><!-- /.box-header -->
  <div class="row">
 <div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            ['attribute'=>'avatar','format'=>'html','value'=>"{$model->getAvatar()} {$model->avatar_name}"],
            ['attribute'=>'name','value'=>"{$model->firstname} {$model->lastname}"],
            'email',
            'mobile_no',
            //'otp',
            //'hash_token',
            //'device',
            ['attribute'=>'address','format'=>'html','value'=>"<ul>
            <li><strong>Country : </strong>{$model->country_name}</li>
            <li><strong>State : </strong>{$model->state_name}</li>
            <li><strong>City : </strong>{$model->city}</li>
            <li><strong>Zipcode : </strong>{$model->zipcode}</li>
            </ul>"],
            ['attribute'=>'is_ip','value'=>$model->userIs],
            'about',
            //'jobs',
            //'schools',
            //'colleges',
            //'certifications',
            'intrests',
            ['attribute'=>'status','value'=>$model->status?"Active":"Inactive"],
        ],
    ]); 
 ?>
   
  </div>  
 </div>
 </div>
</section>   
