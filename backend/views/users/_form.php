<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Countries;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body users-form">
  <div class="row">
    <?php $form = ActiveForm::begin(); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'firstname')->label('Firstname <i>(hidden publicaly no one user can see it)</i>') ?>
     <?= $form->field($model, 'lastname')->label('Lastname <i>(hidden publicaly no one user can see it)</i>') ?>
     <?= $form->field($model, 'email') ?>
     <?= $form->field($model, 'password') ?>
     <?= $form->field($model, 'mobile_no') ?>
     <?= $form->field($model, 'rating') ?>
     <?= $form->field($model, 'users_count') ?>
     <?= Html::hiddenInput('user_id',(string)$model->_id) ?>
     <?= $form->field($model, 'status')->dropDownList([0=>"Inactive",1=>"Active"],['prompt'=>'--Select--']) ?>
     <?= $form->field($model, 'is_ip')->dropDownList(['IS'=>'Information Seeker','IP'=>'Infomation Provider'],['prompt'=>'--Select--']) ?>    
    </div>         
    
     
    <div class="col-md-6">    
      <div class="col-sm-12">
        <label class="control-label" for="users-avatar">Avatar</label>
        <div class="row">
        <div class="col-sm-4 border-right">
        <span class="avatar-pic">
        <center>
            <img src="<?=Common::avatar($model->avatar_image)?>" id="avatar_pic" />
        </center>  
        </span>
           
        <center <?php if(empty($model->avatar_image)):?> style="visibility:hidden;" <?php endif?>>
          <a href="#myModal" type="button"  data-toggle="modal"  >Change Avatar 
              <i class="fa  fa-edit"></i>
          </a>
        </center>            
        <span>        
        </span>    
        </div>
        <div class="col-sm-8">
        <?=$form->field($model, 'avatar_name')->label('Avatar Name <i>Visible to all users</i>')->textInput(['placeholder'=>'Avatar Name','readonly'=>true]) ?>
            
        <?= $form->field($model, 'avatar_image')->label(false)->hiddenInput() ?>    
        </div>
       </div>
       </div>        
      </div>         
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'about')->textarea(['rows'=>8.5]) ?>

    </div>         
    <div class="col-md-12"  id="useris_ip" style="display:<?=$model->is_ip=='IP'?'block':'none'?>;"  >
    <h3>Information Provider Basic Info</h3>
    <div class="col-md-6">
    <?= $form->field($model, 'call_charges') ?>
    </div>
    <div class="col-md-12">    
    <?= $form->field($model, 'intrests')->hiddenInput()->label(false) ?>    
     <div class="mp-tags">
        <label>Interests (Free consultation)</label> 
        <div class="mp-tag-cont form-control" id="mp-tag_cont" data-control="#users-intrests">
        <span class="editable"></span> 
        </div> 
    </div>
    </div>
    <div class="col-md-12">    
    <?= $form->field($model, 'paid_consultancy')->hiddenInput()->label(false) ?>    
     <div class="mp-tags">
        <label>Paid Consultancy</label> 
        <div class="mp-tag-cont form-control" id="mp-tag_cont" data-control="#users-paid_consultancy">
        <span class="editable"></span> 
        </div> 
    </div>
    </div>     
    </div>  
 
    <div class="col-md-12">
    <h3>Address</h3>
      </div>  
    <div class="col-md-6">
     <?= $form->field($model, 'country')->dropdownList(ArrayHelper::map(Countries::find()->all(),'countryId','name'),['prompt'=>'--Select Country--','onchange'=>'loadStates(this)','data-state_id'=>'#users-state','data-state_val'=>$model->state]) ?>
    <?= $form->field($model, 'state')->dropdownList([],['prompt'=>'--Select Country First --']) ?>
    </div>
    <div class="col-md-6">
     <?= $form->field($model, 'city')?>
     <?= $form->field($model, 'zipcode')?>
    </div>  
      
          
    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>
<!-- Modal -->
        <div class="example-modal">
          <div id="myModal" class="modal fade design-model" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Choose Avatar</h4>
                  </div>
                  <div class="modal-body" >
                    <p id="list-avatars">
                       <div class="col-md-3">
              <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Loading state</h3>
                </div>
                <div class="box-body">
                  The body of the box
                </div><!-- /.box-body -->
                <!-- Loading (remove the following to stop the loading)-->
                <div class="overlay">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <!-- end loading -->
              </div><!-- /.box -->
            </div>
                    </p>
                  </div>
                  <div class="modal-footer" id="avatar_model">
                    <button type="button" class="btn btn-default pull-left dnone" data-dismiss="modal">Close</button>  
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div><!-- /.example-modal -->









<?php
  $load_states   = Url::to(['users/load-states']);
  $load_avatars  = Url::to(['users/load-avatars']);
  $load_ip_data  = Url::to(['users/loadip-data']);
  $state = $model->state?$model->state:0;
  $isIP  = (int)$model->is_ip == 'IP';
$JS='';

$JS.= <<<SCRIPT
var load_ip_data = '{$load_ip_data}',type=false;
$('#myModal').on('shown.bs.modal', function () { loadAvatars();});
function loadAvatars(){ 
$.post('{$load_avatars}',{'_csrf':_csrf},function(res){
    var res= Jsonify(res),html='';
    res.forEach(function(ele){    
       html+='<a href="javascript:;" data-url="'+ele+'"><img src="'+ele+'" /></a>';
    });
       $("#list-avatars").html(html);
  },'html');
}
$("#users-country").select2();   
$("#users-state").select2();   
// load states by country id
function loadStates(obj){
    var country_id = $(obj).val(),stateObj = $(obj).data('state_id');
    $.post('{$load_states}',{cid:country_id,'_csrf':_csrf},function(options){
      $(stateObj).html('');
      options.forEach(function(v){
       $(stateObj).select2({'data':[{'id':v.id,'text':v.name}]});      
      });
      var state_val = $(obj).data('state_val');
      if(state_val)
         $(stateObj).val(state_val).change();
    });
  }
function submitForm(obj){  
 var form  = $(obj).parents('form').attr('id'); 
$('body').off('submit','form#'+form);
$('body').on('submit', 'form#'+form, function (e) {
    e.preventDefault();
     var form = $(this);
     // return false if form still have some validation errors
     if (form.find('.has-error').length) {
          return false;
     }
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          dataType:'html',
          success: function (response) { var res = Jsonify(response);
          if(res.success){
             $('button#dismiss-model').click();
            if(res.type) 
               clearForm(form);
          if(res.listId)     
             refreshList(res.listId,res.type);
           }
           if(res.error)
              showErrors(res.error,res.form);               
          }
     });
     return false;
});
}
SCRIPT;
if(!empty($model->country)){
$JS.= <<<SCRIPT
    loadStates($("#users-country"));
SCRIPT;
}
$JS.= <<<SCRIPT
    var isNewRecord = '{$model->isNewRecord}';
    var userIsIp    =  {$isIP};
    var isIP        = '{$isIP}';
SCRIPT;

if($model->is_ip=='IP'){
$JS.= <<<SCRIPT
    ipisTabs(true);
    loadMPTags();
SCRIPT;
}
$this->registerJs($JS,$this::POS_END);