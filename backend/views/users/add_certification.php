<?php
use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Countries;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box-body users-form">
  <div class="box-body users-form">
  <div class="row">
    <?php $form = ActiveForm::begin(['action'=>Url::to(['certification/add-update']),'id'=>'addupdate-certification']); ?>

     
    
     
    <div class="col-md-6">
     <?= $form->field($model, 'title')->label('Title') ?>
     <?= Html::hiddenInput('user_id',$model->user_id)?>
     <?= Html::hiddenInput('update',(string)$model->_id)?>
     <?= $form->field($model, 'certification') ?>
     <?= $form->field($model, 'orgnization') ?>
     
        
    </div>  
     
    <div class="col-md-6">
     <?= $form->field($model, 'from') ?>    
     <?= $form->field($model, 'to') ?>
    </div>         

    <div class="col-md-12"><p>&nbsp;</p>
     <?= $form->field($model, 'detail') ?>
    </div>     

      
          
 <br/>    
 <div class="col-md-6">      
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','onclick'=>'submitForm(this);']) ?>
       <button type="button" class="btn btn-default " id="dismiss-model" data-dismiss="modal">Close</button>
    </div>

    </div> 

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
<!-- Modal -->

<?php
$JS='';
$JS.= <<<SCRIPT
$("#certification-country").select2();   
$("#certification-state").select2();
//Datemask2 mm/dd/yyyy
$("#certification-from").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
$("#certification-to").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
SCRIPT;
$this->registerJs($JS,$this::POS_END);
